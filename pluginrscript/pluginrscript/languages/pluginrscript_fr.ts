<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>RSC_StepUseRScript</name>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="52"/>
        <source>Excexute R script</source>
        <translation>Exécuter un script R</translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="57"/>
        <source>Cette étape permet d&apos;exécuter un script R en tant qu&apos;étape Computree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="72"/>
        <source>Script R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="72"/>
        <source>Fichier R (*.*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="81"/>
        <location filename="../step/rsc_stepuserscript.cpp" line="89"/>
        <source>--------------------------------------</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="82"/>
        <source>Informations sur le script R spécifié.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="83"/>
        <source>Nom          : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="84"/>
        <source>Auteur       : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="85"/>
        <source>Organisation : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="86"/>
        <source>Version      : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="87"/>
        <source>Description  :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="502"/>
        <source>Erreur : Paramètre de type indéfini : %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="574"/>
        <source>Erreur : champs défini hors d&apos;une déclaration de table.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="577"/>
        <source>Erreur : Déclaration inconnue : %1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="587"/>
        <source>Erreur : Le fichier script R spécifié n&apos;existe pas : %1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/rsc_stepuserscript.cpp" line="589"/>
        <source>Erreur : Aucun fichier script R spécifié.</source>
        <translation></translation>
    </message>
</context>
</TS>
