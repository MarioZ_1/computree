#ifndef RSC_PLUGINENTRY_H
#define RSC_PLUGINENTRY_H

#include "pluginentryinterface.h"

class RSC_PluginManager;

class RSC_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    RSC_PluginEntry();
    ~RSC_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    RSC_PluginManager *_pluginManager;
};

#endif // RSC_PLUGINENTRY_H
