CT_PREFIX = ../../computreev6

COMPUTREE += ctlibplugin ctlibclouds ctlibstep ctlibstepaddon


include($${CT_PREFIX}/plugin_shared.pri)

TARGET = plug_rscript

HEADERS += \
    $$CT_LIB_PREFIX/ctlibplugin/pluginentryinterface.h \
    rsc_pluginentry.h \
    rsc_pluginmanager.h \
    tools/infield.h \
    tools/intable.h \
    tools/outfield.h \
    tools/outtable.h \
    tools/tools.h \
    step/rsc_stepuserscript.h
SOURCES += \
    rsc_pluginentry.cpp \
    rsc_pluginmanager.cpp \
    tools/infield.cpp \
    tools/intable.cpp \
    tools/outfield.cpp \
    tools/outtable.cpp \
    tools/tools.cpp \
    step/rsc_stepuserscript.cpp

TRANSLATIONS += languages/pluginrscript_en.ts \
                languages/pluginrscript_fr.ts
