/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#ifndef RSC_STEPUSERSCRIPT_H
#define RSC_STEPUSERSCRIPT_H

#include "ct_step/abstract/ct_abstractstep.h"

class RSC_StepUseRScript : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    struct Description {
        QString _name;
        QString _description;
        QString _author;
        QString _organization;
        QString _version;
    };


    struct Parameter {

        QString         _name;
        QString         _type;
        QString         _beforelab;
        QString         _afterlab;
        double          _min;
        double          _max;
        int             _dec;
        int             _maxsize;
        QStringList     _vals;

        bool            _boolValue;
        double          _doubleValue;
        int             _intValue;
        QString         _characterValue;
    };

    struct Field {
        QString _name;
        QString _type;
        QString _desc;
        bool    _multi;
        bool    _opt;

        QString                 _modelName;
        CT_AutoRenameModels*    _autoRename;
    };

    struct Table {
        QString                 _name;
        QString                 _desc;
        bool                    _cpy;
        QString                 _res_modelName;
        QString                 _grp_modelName;
        QString                 _item_modelName;
        QString                 _id_modelName;
        CT_AutoRenameModels*    _res_autoRename;
        CT_AutoRenameModels*    _grp_autoRename;
        CT_AutoRenameModels*    _item_autoRename;
        CT_AutoRenameModels*    _id_autoRename;

        QList<Field>    _fields;
    };

    RSC_StepUseRScript();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void finalizePreSettings();

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void compute() final;

private:

    QStringList _fileName;

    Description         _description;
    QList<Parameter>    _parameters;
    QList<Table>        _inTables;
    QList<Table>        _outTables;

    Table*              _currentTable;

//    CT_AutoRenameModels     _outSceneModelName;

    void processHeader();
};


#endif // RSC_STEPUSERSCRIPT_H
