/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "rsc_stepuserscript.h"
#include "ct_log/ct_logmanager.h"

#include "tools/tools.h"

#include <QFile>
#include <QTextStream>

#include <limits>

RSC_StepUseRScript::RSC_StepUseRScript() : SuperClass()
{
}

QString RSC_StepUseRScript::description() const
{
    return tr("Excexute R script");
}

QString RSC_StepUseRScript::detailledDescription() const
{
    return tr("Cette étape permet d'exécuter un script R en tant qu'étape Computree");
}

CT_VirtualAbstractStep* RSC_StepUseRScript::createNewInstance() const
{
    // cree une copie de cette etape
    return new RSC_StepUseRScript();
}

//////////////////// PROTECTED //////////////////

void RSC_StepUseRScript::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addFileChoice(tr("Script R"), CT_FileChoiceButton::OneExistingFile, tr("Fichier R (*.*)"), _fileName);
}

void RSC_StepUseRScript::finalizePreSettings()
{
    processHeader();

    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("--------------------------------------"));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Informations sur le script R spécifié."));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Nom          : %1").arg(_description._name));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Auteur       : %1").arg(_description._author));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Organisation : %1").arg(_description._organization));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Version      : %1").arg(_description._version));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Description  :"));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, _description._description);
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("--------------------------------------"));
}


void RSC_StepUseRScript::declareInputModels(CT_StepInModelStructureManager& manager)
{
    if (_inTables.isEmpty())
    {
        manager.setNotNeedInputResult();
    } else {

        // initialisation
        for (int t = 0 ; t < _outTables.size() ; t++)
        {
            Table &outtable = _outTables[t];
            outtable._cpy = false;
        }

        for (int i = 0 ; i < _inTables.size() ; i++)
        {
            Table &intable = _inTables[i];
            intable._cpy = false;

            QString tablename = intable._name;

            intable._res_modelName = QString("RESIN_%1").arg(tablename);
            intable._grp_modelName = QString("GRPIN_%1").arg(tablename);
            intable._item_modelName = QString("ITEMIN_%1").arg(tablename);
            intable._id_modelName = QString("IDCTIN_%1").arg(tablename);

            intable._res_autoRename = NULL;
            intable._item_autoRename = NULL;
            intable._id_autoRename = NULL;

            for (int t = 0 ; t < _outTables.size() ; t++)
            {
                Table &outtable = _outTables[t];

                outtable._res_autoRename = NULL;
                outtable._grp_autoRename = NULL;
                outtable._item_autoRename = NULL;
                outtable._id_autoRename = NULL;

                if (outtable._name == tablename)
                {
                    intable._cpy = true;
                    outtable._cpy = true;

                    outtable._res_modelName = intable._res_modelName;
                    outtable._item_modelName = intable._item_modelName;
                    outtable._id_modelName = intable._id_modelName;
                }
            }

            CT_InResultModelGroup *resultModel = NULL;
            if (intable._cpy)
            {
                resultModel = createNewInResultModelForCopy(intable._res_modelName, intable._desc);
            } else {
                resultModel = createNewInResultModel(intable._res_modelName, intable._desc);
            }

            resultModel->setZeroOrMoreRootGroup();
            resultModel->addGroupModel("", intable._grp_modelName, CT_AbstractItemGroup::staticGetType(), "", "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
            resultModel->addItemModel(intable._grp_modelName, intable._item_modelName, CT_AbstractSingularItemDrawable::staticGetType(), "", "", CT_InAbstractModel::C_ChooseMultipleIfMultiple);

            if (intable._cpy) // en cas de copie, ID nécessaire
            {
                resultModel->addItemAttributeModel(intable._item_modelName, intable._id_modelName, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::ANY, "IDCT", "ID Computree", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
            }

            QList<Field> &fields = intable._fields;
            for (int j = 0 ; j < fields.size() ; j++)
            {
                Field& field = fields[j];
                QString fieldName = field._name;

                field._modelName =  QString("ATTIN_%1_%2").arg(tablename).arg(fieldName);
                field._autoRename =  NULL;

                CT_InAbstractModel::ChoiceMode choiceMode = CT_InAbstractModel::C_ChooseOneIfMultiple;
                if (field._multi) {choiceMode = CT_InAbstractModel::C_ChooseMultipleIfMultiple;}

//                CT_InAbstractModel::FinderMode finderMode = CT_InAbstractModel::F_IsOptional;
                CT_InAbstractModel::FinderMode finderMode = CT_InAbstractModel::F_IsObligatory;
                if (field._opt) {finderMode = CT_InAbstractModel::F_IsOptional;}

                if (field._type == "logical")
                {
                    resultModel->addItemAttributeModel(intable._item_modelName, field._modelName, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::BOOLEAN, field._name, field._desc, choiceMode, finderMode);
                } else if (field._type == "integer")
                {
                    resultModel->addItemAttributeModel(intable._item_modelName, field._modelName, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER_INT, field._name, field._desc, choiceMode, finderMode);
                } else if (field._type == "numeric")
                {
                    resultModel->addItemAttributeModel(intable._item_modelName, field._modelName, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, field._name, field._desc, choiceMode, finderMode);
                } else if (field._type == "character")
                {
                    resultModel->addItemAttributeModel(intable._item_modelName, field._modelName, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, field._name, field._desc, choiceMode, finderMode);
                } else if (field._type == "factor")
                {
                    resultModel->addItemAttributeModel(intable._item_modelName, field._modelName, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, field._name, field._desc, choiceMode, finderMode);
                }
            }
        }
    }
}

// Création et affiliation des modèles OUT
void RSC_StepUseRScript::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
//    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

//    if (resultModel != NULL)
//    {
//        resultModel->addItemModel(DEF_SearchInGroup, _outSceneModelName, new CT_Scene(), tr("Scène extraite"));
//    }
}


void RSC_StepUseRScript::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    if (_parameters.size() > 0)
    {
        for (int i = 0 ; i < _parameters.size() ; i++)
        {
            Parameter &p = (Parameter&) _parameters.at(i);

            if (p._type == "logical")
            {
                postInputConfigDialog->addBool(p._beforelab, p._afterlab, "", p._boolValue);
            } else if (p._type == "numeric")
            {
                if (p._dec == 0)
                {
                    postInputConfigDialog->addInt(p._beforelab, p._afterlab, p._min, p._max, p._intValue);
                } else {
                    postInputConfigDialog->addDouble(p._beforelab, p._afterlab, p._min, p._max, p._dec, p._doubleValue);
                }

            } else if (p._type == "character")
            {
                postInputConfigDialog->addString(p._beforelab, p._afterlab, p._characterValue);
            } else if (p._type == "list")
            {
                postInputConfigDialog->addStringChoice(p._beforelab, p._afterlab, p._vals, p._characterValue);
            }
        }
    }
}


void RSC_StepUseRScript::compute()
{

    for (int i = 0 ; i < _inTables.size() ; i++)
    {
        Table &intable = _inTables[i];

        QVector<QString> fieldVals(intable._fields.size());

        QList<Field> &fields = intable._fields;
        for (int j = 0 ; j < fields.size() ; j++)
        {
            Field& field = fields[j];
            fieldVals[j] = field._name;
        }

        QString val;
        for (int fv = 0 ; fv < fieldVals.size() ; fv++)
        {
            val.append(fieldVals[fv]);
            val.append("\t");
        }
        qDebug() << val;


        CT_ResultGroup* inresult = getInputResultsForModel(intable._res_modelName).first();

        CT_ResultGroupIterator it(inresult, this, intable._grp_modelName);
        while (!isStopped() && it.hasNext())
        {
            const CT_AbstractItemGroup* group = it.next();
            fieldVals.fill("NA");

            QList<CT_AbstractSingularItemDrawable*> items = group->itemsByINModelName(this, intable._item_modelName);

            for (int k = 0 ; k < items.size() ; k++)
            {
                const CT_AbstractSingularItemDrawable* item = items.at(k);

                if (item != NULL)
                {
                    QList<Field> &fields = intable._fields;
                    for (int j = 0 ; j < fields.size() ; j++)
                    {
                        Field& field = fields[j];

                        CT_AbstractItemAttribute* att = item->firstItemAttributeByINModelName(inresult, this, field._modelName);

                        if (att != NULL)
                        {
                            bool ok;
                            fieldVals[j] = att->toString(item, &ok);
                            if (!ok) {fieldVals[j] = "NA";}
                        }
                    }

                }
            }

            QString val;
            for (int fv = 0 ; fv < fieldVals.size() ; fv++)
            {
                val.append(fieldVals[fv]);
                val.append("\t");
            }
            qDebug() << val;
        }
    }

    // récupération du résultats IN et OUT
//    CT_ResultGroup *outResult = getOutResultList().first();

//    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
//    while(!isStopped() && it.hasNext())
//    {
//        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

//        if (group != NULL)
//        {
//            const CT_Scene *in_scene = (CT_Scene*) group->firstItemByINModelName(this, DEF_SearchInScene);
//            const CT_AbstractPointCloudIndex *pointCloudIndex = in_scene->getPointCloudIndex();


//        }
//    }




//    qDebug() << "description";
//    qDebug() << "name= " << _description._name;
//    qDebug() << "author= " << _description._author;
//    qDebug() << "organization= " << _description._organization;
//    qDebug() << "version= " << _description._version;
//    qDebug() << "description= " << _description._description;

//    qDebug();
//    qDebug() << "_parameters";

//    for (int i = 0 ; i < _parameters.size() ; i++)
//    {
//        const Parameter &param = _parameters.at(i);
//        qDebug() << "_name= " << param._name;
//        qDebug() << "_type= " << param._type;
//        qDebug() << "_beforelab= " << param._beforelab;
//        qDebug() << "_afterlab= " << param._afterlab;
//        qDebug() << "_min= " << param._min;
//        qDebug() << "_max= " << param._max;
//        qDebug() << "_dec= " << param._dec;
//        qDebug() << "_maxsize= " << param._maxsize;
//        qDebug() << "_vals= " << param._vals;
//        qDebug() << "_boolValue= " << param._boolValue;
//        qDebug() << "_doubleValue= " << param._doubleValue;
//        qDebug() << "_intValue= " << param._intValue;
//        qDebug() << "_characterValue= " << param._characterValue;
//        qDebug() << "name= " << param._name;
//    }


//    qDebug();
//    qDebug() << "_inTables";
//    for (int i = 0 ; i < _inTables.size() ; i++)
//    {
//        const Table &table = _inTables.at(i);
//        qDebug() << "_name= " << table._name;

//        for (int j = 0 ; table._fields.size() ; j++)
//        {
//            const Field &field = table._fields.at(j);
//            qDebug() << "Field name= " << field._name << " ; type= " << field._type << " ; descr= " << field._desc;
//        }
//    }


//    qDebug();
//    qDebug() << "_outTables";
//    for (int i = 0 ; i < _outTables.size() ; i++)
//    {
//        const Table &table = _outTables.at(i);
//        qDebug() << "_name= " << table._name;

//        for (int j = 0 ; table._fields.size() ; j++)
//        {
//            const Field &field = table._fields.at(j);
//            qDebug() << "Field name= " << field._name << " ; type= " << field._type << " ; descr= " << field._desc;
//        }
//    }




}

void RSC_StepUseRScript::processHeader()
{
    if(_fileName.size() > 0 && QFile::exists(_fileName.first()))
    {
        QFile f(_fileName.first());

        if (f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&f);

            bool beginParse = false;
            bool endParse = false;

            _description._name = "";
            _description._description = "";
            _description._author = "";
            _description._organization= "";
            _description._version= "";

            _currentTable = NULL;

            while (!stream.atEnd() && !endParse)
            {
                QString line = stream.readLine();
                QString testLine = line;
                testLine.replace(QRegExp("[# ]"), "");
                testLine = testLine.toUpper();

                if (testLine == "COMPUTREE_HEADER_END") {beginParse = false;endParse = true;}
                if (beginParse)
                {
                    QMultiMap<QString, QString> lineTable;
                    QString code = Tools::parseLine(line, lineTable);

                    if (code == "name") {_description._name = lineTable.value("name");_currentTable = NULL;}
                    else if (code == "description") {_description._description += lineTable.value("description");_description._description += "\n";_currentTable = NULL;}
                    else if (code == "author") {_description._author = lineTable.value("author");_currentTable = NULL;}
                    else if (code == "organization") {_description._organization = lineTable.value("organization");_currentTable = NULL;}
                    else if (code == "version") {_description._version = lineTable.value("version");_currentTable = NULL;}
                    else if (code == "param")
                    {
                        _currentTable = NULL;
                        Parameter p;
                        p._name = lineTable.value("param");
                        p._type = lineTable.value("type");
                        p._beforelab = lineTable.value("beforelab", "");
                        p._afterlab = lineTable.value("afterlab", "");
                        p._dec = 0;
                        p._min = 0;
                        p._max = 0;
                        p._maxsize = 0;
                        p._vals.clear();

                        p._boolValue = false;
                        p._doubleValue = 0;
                        p._intValue = 0;
                        p._characterValue = "";


                        if (p._type == "logical")
                        {
                            if (lineTable.value("default").toLower() == "true")
                            {
                                p._boolValue = true;
                            } else {
                                p._boolValue = false;
                            }
                        }
                        if (p._type == "numeric")
                        {
                            p._dec = lineTable.value("dec").toInt();
                            p._min = lineTable.value("min").toDouble();
                            p._max = lineTable.value("max").toDouble();

                            if (p._dec == 0)
                            {
                                p._intValue = lineTable.value("default", 0).toInt();
                            } else {
                                p._doubleValue = lineTable.value("default", 0).toDouble();
                            }
                        }
                        if (p._type == "character")
                        {
                            p._maxsize = lineTable.value("maxsize").toInt();
                            p._characterValue = lineTable.value("default", "");
                        }
                        if (p._type == "list")
                        {
                            p._vals = lineTable.values("val");
                            p._characterValue = lineTable.value("default", "");
                        } else {
                            PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Erreur : Paramètre de type indéfini : %1")).arg(p._type));
                        }
                        _parameters.append(p);

                    }
                    else if (code == "intable")
                    {
                        QString name = lineTable.value("intable");
                        if (!name.isEmpty()) // existance et unicité obligatoires
                        {
                            bool alreadyInserted = false;
                            for (int t = 0 ; t < _inTables.size() && ! alreadyInserted; t++)
                            {
                                if (_inTables.at(t)._name == name) {alreadyInserted = true;}
                            }

                            if (!alreadyInserted)
                            {
                                _inTables.append(Table());
                                _currentTable = &(_inTables.last());
                                _currentTable->_name = name;
                                _currentTable->_desc = lineTable.value("desc");
                            }
                        }
                    }
                    else if (code == "outtable")
                    {
                        QString name = lineTable.value("outtable");
                        if (!name.isEmpty()) // existance et unicité obligatoires
                        {
                            bool alreadyInserted = false;
                            for (int t = 0 ; t < _outTables.size() && ! alreadyInserted; t++)
                            {
                                if (_outTables.at(t)._name == name) {alreadyInserted = true;}
                            }

                            if (!alreadyInserted)
                            {
                                _outTables.append(Table());
                                _currentTable = &(_outTables.last());
                                _currentTable->_name = name;
                                _currentTable->_desc = lineTable.value("desc");
                            }
                        }
                    }
                    else if (code == "field")
                    {
                        if (_currentTable != NULL)
                        {
                            Field f;
                            f._name = lineTable.value("field");

                            bool alreadyinserted = false;
                            for (int fi = 0 ; fi < _currentTable->_fields.size() && ! alreadyinserted ; fi++)
                            {
                                if (_currentTable->_fields.at(fi)._name == f._name) {alreadyinserted = true;}
                            }

                            if (!alreadyinserted)
                            {
                                f._desc = lineTable.value("desc");
                                f._type = lineTable.value("type");

                                QString ismulti = lineTable.value("multi", "false");
                                f._multi = (ismulti == "true");

                                QString isopt = lineTable.value("opt", "false");
                                f._opt = (isopt == "true");

                                _currentTable->_fields.append(f);
                            }
                        } else {
                            PS_LOG->addMessage(LogInterface::error, LogInterface::step, tr("Erreur : champs défini hors d'une déclaration de table."));
                        }
                    } else if (!code.isEmpty()) {
                        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Erreur : Déclaration inconnue : %1.")).arg(code));
                    }
                }
                if (testLine == "COMPUTREE_HEADER_BEGIN") {beginParse = true;endParse = false;}

            }
        }
    } else {
        if (_fileName.size() > 0)
        {
            PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Erreur : Le fichier script R spécifié n'existe pas : %1.")).arg(_fileName.first()));
        } else {
            PS_LOG->addMessage(LogInterface::error, LogInterface::step, tr("Erreur : Aucun fichier script R spécifié."));
        }
    }
}
