#ifndef RSC_PLUGINMANAGER_H
#define RSC_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class RSC_PluginManager : public CT_AbstractStepPlugin
{
public:
    RSC_PluginManager();
    ~RSC_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-r-script/wiki");}

    virtual QString getPluginOfficialName() const;
    virtual QString getPluginRISCitation() const;

    virtual bool init();

protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // RSC_PLUGINMANAGER_H
