CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

TARGET = plug_rscript

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    rsc_pluginentry.h \
    rsc_pluginmanager.h \
    tools/tools.h \
    step/rsc_stepuserscript.h
SOURCES += \
    rsc_pluginentry.cpp \
    rsc_pluginmanager.cpp \
    tools/tools.cpp \
    step/rsc_stepuserscript.cpp

TRANSLATIONS += languages/pluginrscript_en.ts \
                languages/pluginrscript_fr.ts
