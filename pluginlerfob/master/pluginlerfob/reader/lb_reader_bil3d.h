#ifndef LB_READER_BIL3D_H
#define LB_READER_BIL3D_H

#include "ct_reader/abstract/ct_abstractreader.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"

#define DEF_PB_Reader_Bil3D_rootGroup   "root"

#define DEF_PB_Reader_Bil3D_sceneOut            "outScene"
#define DEF_PB_Reader_Bil3D_elementGroup        "elementGroup"
#define DEF_PB_Reader_Bil3D_elementCircleGroup  "eCircleGroup"
#define DEF_PB_Reader_Bil3D_elementCircle       "circle"

class LB_Reader_Bil3D : public CT_AbstractReader
{
    Q_OBJECT

public:
    LB_Reader_Bil3D();

    bool setFilePath(const QString &filepath);

    CT_AbstractReader* copy() const;

    READER_COPY_FULL_IMP(LB_Reader_Bil3D)

protected:
    void protectedInit();
    void protectedCreateOutItemDrawableModelList();
    bool protectedReadFile();

private:
    CT_OutStdGroupModel *_rootGroupModel;
    CT_OutStdSingularItemModel *_sceneOutModel;
    CT_OutStdGroupModel *_elementGroupModel;
    CT_OutStdGroupModel *_elementCircleGroupModel;
    CT_OutStdSingularItemModel *_elementCircleModel;

    QString     _fileName_moe;
    QString     _fileName_bil;
    QString     _fileName_ext;
    QString     _fileName_ama;
    QString     _fileName_mes;

    bool     _moe;
    bool     _bil;
    bool     _ext;
    bool     _ama;
    bool     _mes;

};

#endif // LB_READER_BIL3D_H
