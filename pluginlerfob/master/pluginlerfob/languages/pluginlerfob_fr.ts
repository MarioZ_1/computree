<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>LB_Reader_Bil3D</name>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="72"/>
        <source>Fichiers Bil 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="79"/>
        <source>Billon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="80"/>
        <source>Enveloppe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="81"/>
        <source>Element</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="82"/>
        <source>Cercle (grp)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="83"/>
        <source>Cercle</source>
        <translation>Cercle</translation>
    </message>
</context>
<context>
    <name>LB_StepCharacteriseCurvature</name>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="79"/>
        <source>Calcule les courbures d&apos;une section</source>
        <translation>Calcule les courbures d&apos;une section</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="85"/>
        <source>Cette étape permet de caractériser l&apos;évolution de la courbure de la polyligne centrale de  billons à partir des cercles ajustés sur les clusters horizontaux.On définit comme paramètres:&lt;ul&gt;&lt;li&gt; un nombre de cercles &lt;b&gt;n&lt;/b&gt; qui seront pris en compte de part et d&apos;autre de chaque centre pour calculer les vecteurs tangents et normaux à la ligne des centres en faisant appel à une méthode de régression linéaire. (N.B.: l&apos;épaisseur des clusters horizontaux impacte aussi le résultat.)&lt;/li&gt;&lt;li&gt; un &lt;b&gt;rayon maximal&lt;/b&gt; des cercles osculateurs à afficher.&lt;br&gt;&lt;/ul&gt;En sortie, cette étape fournit:&lt;ul&gt;&lt;li&gt; l&apos;évolution du trièdre de Frenet le long de la polyligne composée des centres de chaque cercle, et dont se déduisent les cercles osculateurs caractérisant la courbure, en amplitude et en orientation. Chaque trièdre est composé des vecteurs tangent, normal, et binormal. Leurs normes est fixée à 10cm pour la représentation. Le cercle osculateur associé à un point de la polyligne est dans le plan défini par les vecteurs tangent et normal associés au point.&lt;br /&gt;Les attributs permettent de relier les rayons de courbure à la hauteur, et à l&apos;abscisse curviligne du point de la polyligne.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>Cette étape permet de caractériser l&apos;évolution de la courbure de la polyligne centrale de  billons à partir des cercles ajustés sur les clusters horizontaux.On définit comme paramètres:&lt;ul&gt;&lt;li&gt; un nombre de cercles &lt;b&gt;n&lt;/b&gt; qui seront pris en compte de part et d&apos;autre de chaque centre pour calculer les vecteurs tangents et normaux à la ligne des centres en faisant appel à une méthode de régression linéaire. (N.B.: l&apos;épaisseur des clusters horizontaux impacte aussi le résultat.)&lt;/li&gt;&lt;li&gt; un &lt;b&gt;rayon maximal&lt;/b&gt; des cercles osculateurs à afficher.&lt;br&gt;&lt;/ul&gt;En sortie, cette étape fournit:&lt;ul&gt;&lt;li&gt; l&apos;évolution du trièdre de Frenet le long de la polyligne composée des centres de chaque cercle, et dont se déduisent les cercles osculateurs caractérisant la courbure, en amplitude et en orientation. Chaque trièdre est composé des vecteurs tangent, normal, et binormal. Leurs normes est fixée à 10cm pour la représentation. Le cercle osculateur associé à un point de la polyligne est dans le plan défini par les vecteurs tangent et normal associés au point.&lt;br /&gt;Les attributs permettent de relier les rayons de courbure à la hauteur, et à l&apos;abscisse curviligne du point de la polyligne.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="109"/>
        <source>Billons</source>
        <translation>Billons</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="110"/>
        <source>Groupe Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="111"/>
        <source>Cluster(groupe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="111"/>
        <source>Groupe contenant les clusters contenues dans la section</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="112"/>
        <source>Cercle</source>
        <translation>Cercle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="119"/>
        <source>Courbures</source>
        <translation>Courbures</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="120"/>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="122"/>
        <source>Courbure</source>
        <translation>Courbure</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="120"/>
        <source>groupe contenant les infos de courbure d&apos;un billon </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="123"/>
        <source>tangente</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="123"/>
        <source>tangente à la ligne neutre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="124"/>
        <source>normale</source>
        <translation>normale</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="124"/>
        <source>normale à la ligne neutre</source>
        <translation>normale à la ligne neutre</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="125"/>
        <source>binormale</source>
        <translation>binormale</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="125"/>
        <source>binormale à la ligne neutre</source>
        <translation>binormale à la ligne neutre</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="126"/>
        <source>Cercle osculateur</source>
        <translation>Cercle osculateur</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="126"/>
        <source>Cercle tangent à la ligne neutre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="127"/>
        <source>Attributs courbure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="131"/>
        <source>Rayon de Courbure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="134"/>
        <source>Hauteur de référence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="137"/>
        <source>Abscisse Curviligne</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="146"/>
        <source>Nombre n de cercles pour lissage sur 2n +1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="147"/>
        <source>Rayon de courbure maximal à afficher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="316"/>
        <source> Courbures Section %1 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="464"/>
        <source> Courbures Section %1 non calculées car trop peu de points </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LB_StepCharacterizeLog</name>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="152"/>
        <source>resSections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="153"/>
        <source>Section(Billon)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="154"/>
        <source>Cluster(groupe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="154"/>
        <source>Groupe contenant les clusters contenues dans la section</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="156"/>
        <source>Circle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="165"/>
        <source>Amplitude de l&apos;inclinaison</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="105"/>
        <source>Caractérisation du billon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="111"/>
        <source>Cette étape permet de caractériser le(s) billon(s) présent(s) dans la scène &lt;br&gt;&lt;br /&gt;En entrée, les données utilisées proviennent de nuages 3D décrivant des billons décomposés en  clusters horizontaux contigus sur lesquels des cercles ont été ajustés.&lt;br /&gt;En sortie, certaines caractéristiques sont rendues visibles à travers des objets les figurant, mais d&apos;autres sont seulement numériques et sont accessibles via l&apos;objet attribut et/ou sont affichées dans la fenêtre Log.&lt;br /&gt;Ces caractéristiques concernent:&lt;br /&gt; &lt;ul&gt;&lt;li&gt; l&apos;&lt;b&gt;inclinaison&lt;/b&gt; globale est établie à partir des centres des cercles d&apos;extrémité dits de fin bout et gros bout. Trois visualisations lui sont associés : &lt;/li&gt;&lt;ul&gt;&lt;li&gt; Un segment incliné &lt;/li&gt;&lt;li&gt; Un segment horizontal, correspondant à la projection horizontale du précédent indiquant la direction et l&apos;amplitude de l&apos;inclinaison au niveau de la base du billon &lt;/li&gt;&lt;li&gt; Un cercle horizontal à la base du billon dont le rayon correspond à la longueur du segment précédent &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; la &lt;b&gt;flexuosité&lt;/b&gt; définie à partir de la polyligne joignant les centres des cercles et exprimée par deux valeurs &lt;li&gt;&lt;ul&gt;&lt;li&gt; la longueur de la polyligne à rapporter à la longueur du segment représentant l&apos;inclinaison globale&apos; &lt;/li&gt;&lt;li&gt; l&apos;aire de la surface gauche définie par la polyligne et le segment représentant l&apos;inclinaison globale, calculée par triangulation&apos; &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; le &lt;b&gt;volume de bois rond &lt;/b&gt; calculé de deux manières différentes, à partir de la somme des volumes de: &lt;/li&gt;&lt;ul&gt;&lt;li&gt;  troncs de cône définis à partir de deux cercles consécutifs&apos; &lt;/li&gt;&lt;li&gt;  cylindres définis à partir de chaque cercle et de la distance le séparant du voisin&apos; &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; le &lt;b&gt;défilement &lt;/b&gt; calculé à partir de la distance séparant deux cercles consécutifs, et de leurs rayons respectifs. Les valeurs moyenne, minimale et maximale sont restituées dana la liste d&apos;attributs  &lt;li&gt;&lt;li&gt; le &lt;b&gt;volume de bois scié  &lt;/b&gt; calculé en tenant compte de la position de la section la plus décalée par rapport à l&apos;&apos;inclinaison globale. Ce volume est calculé à partir la longueur du billon et de l&apos;intersection du cercle associé à cette section avec les deux cercles correspondant aux extrémités du billon. Cette intersection peut aller de la section de fin bout à une valeur nulle, le cas général étant la réunion de quatre 1/4 d&apos;ellipse. Chaque quart d&apos;ellipse est définie pas les longueurs de ses demi-axes a et b, desquels il est possible de définir le rectangle d&apos;aire maximal s&apos;inscrivant à l&apos;intérieur et qui a pour côtés a et b divisés par la racine carrée de 2. &lt;li&gt;&lt;/ul&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="152"/>
        <source>résultat comportant des sections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="156"/>
        <source>Cercle ajusté sur un cluster horizontal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="163"/>
        <source>Caractéristiques des Billons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="163"/>
        <source>Caractéristiques Sections )
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="164"/>
        <source>Caractéristiques Billon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="164"/>
        <source>groupe contenant les characteristiques d&apos;un billon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="165"/>
        <source>Cercle représentant l&apos;amplitude de l&apos;inclinaison, centré sur la base du billon.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="166"/>
        <source>Projection horizontale inclinaison</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="166"/>
        <source>Segment horizontal dont l&apos;origine est le centre du bas du billon. Il indique l&apos;amplitude de l&apos;inclinaison totale par sa longueur, et sa direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="167"/>
        <source>segment inclinaison totale </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="167"/>
        <source>Inclinaison gloable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="168"/>
        <source>cylindre équivalent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="168"/>
        <source>cylindre de même volume que le billon et incliné de la même facon </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="169"/>
        <source>Demi-diamètre a1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="169"/>
        <source>Demi-diamètre a1 d1/4 ellipse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="170"/>
        <source>Demi-diamètre a2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="170"/>
        <source>Demi-diamètre a2 d1/4 ellipse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="171"/>
        <source>Demi-diamètre b1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="171"/>
        <source>Demi-diamètre b1 d1/4 ellipse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="172"/>
        <source>Demi-diamètre b2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="172"/>
        <source>Demi-diamètre b2 d1/4 ellipse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="173"/>
        <source>Longueur1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="173"/>
        <source>Longueur du tronçon inférieur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="174"/>
        <source>Longueur2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="174"/>
        <source>Longueur du tronçon supérieur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="175"/>
        <source>Cercle fin bout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="176"/>
        <source>Cercle flèche max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="176"/>
        <source>Cercle où la flèche par rapport à  l&apos;inclinaison globale est maximale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="177"/>
        <source>Cercle gros bout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="178"/>
        <source>Attributs du Billon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="182"/>
        <source>Volume1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="185"/>
        <source>Volume2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="188"/>
        <source>Longueur Courbe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="191"/>
        <source>Aire flexuosité</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="194"/>
        <source>Défilement Moyen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="197"/>
        <source>Défilement Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="200"/>
        <source>Défilement Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="568"/>
        <source> Section %1 Attention Problème Volume Sciage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="571"/>
        <source> Section %1 Volume Sciage %2 [m3] ( l: %3 a1: %4  a2: %5 b1: %6 b2: %7 m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="418"/>
        <source> Section %1 Volume /Cone %2 m3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="419"/>
        <source> Section %1 Volume /Cylindre %2 m3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="420"/>
        <source> Section %1 Défilement [m/m] Moyen %2 Min %3 Max %4 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="506"/>
        <source> Section %1 Critere lineaire de flexuosite %2 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="507"/>
        <source> Section %1 Critere surfacique de flexuosite %2 m2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="508"/>
        <source> Section %1 Fleche maximale %2 m</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lb_pluginmanager.cpp" line="36"/>
        <location filename="../lb_pluginmanager.cpp" line="37"/>
        <source>Qualité des bois</source>
        <translation></translation>
    </message>
</context>
</TS>
