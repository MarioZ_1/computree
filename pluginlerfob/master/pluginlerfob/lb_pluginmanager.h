#ifndef LB_PLUGINMANAGER_H
#define LB_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class LB_PluginManager : public CT_AbstractStepPlugin
{
public:
    LB_PluginManager();
    ~LB_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-lerfob/wiki");}

    QString getPluginRISCitation() const;

protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // LB_PLUGINMANAGER_H
