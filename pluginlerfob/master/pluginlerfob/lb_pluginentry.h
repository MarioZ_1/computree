#ifndef LB_PLUGINENTRY_H
#define LB_PLUGINENTRY_H

#include "interfaces.h"

class LB_PluginManager;

class LB_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    LB_PluginEntry();
    ~LB_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    LB_PluginManager *_pluginManager;
};

#endif // LB_PLUGINENTRY_H