#include "seg_pluginentry.h"
#include "seg_pluginmanager.h"

SEG_PluginEntry::SEG_PluginEntry()
{
    _pluginManager = new SEG_PluginManager();
}

SEG_PluginEntry::~SEG_PluginEntry()
{
    delete _pluginManager;
}

QString SEG_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* SEG_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_segma, SEG_PluginEntry)
#endif
