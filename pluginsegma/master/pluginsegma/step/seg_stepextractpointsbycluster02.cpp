#include "seg_stepextractpointsbycluster02.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/ct_scene.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_resSc "resSc"
#define DEFin_grpSc "grpSc"
#define DEFin_scene "scene"

#define DEFin_res "res"
#define DEFin_mainGrp "maingrp"
#define DEFin_clusters "clusters"
#define DEFin_grp "grp"
#define DEFin_itemWithIdCluster "heights"
#define DEF_inAttIDcluster "idcluster"


// Constructor : initialization of parameters
SEG_StepExtractPointsByCluster02::SEG_StepExtractPointsByCluster02(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

// Step description (tooltip of contextual menu)
QString SEG_StepExtractPointsByCluster02::getStepDescription() const
{
    return tr("8- Extraire les points par couronne (v2)");
}

// Step detailled description
QString SEG_StepExtractPointsByCluster02::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepExtractPointsByCluster02::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepExtractPointsByCluster02::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepExtractPointsByCluster02(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepExtractPointsByCluster02::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_resSc = createNewInResultModel(DEFin_resSc, tr("Scène(s)"), "", true);
    resIn_resSc->setZeroOrMoreRootGroup();
    resIn_resSc->addGroupModel("", DEFin_grpSc, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_resSc->addItemModel(DEFin_grpSc, DEFin_scene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène(s)"));


    CT_InResultModelGroupToCopy *res = createNewInResultModelForCopy(DEFin_res, tr("Couronnes"));
    res->setZeroOrMoreRootGroup();
    res->addGroupModel("", DEFin_mainGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res->addItemModel(DEFin_mainGrp, DEFin_clusters, CT_Image2D<qint32>::staticGetType(), tr("Couronnes segmentées"));
    res->addGroupModel(DEFin_mainGrp, DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Couronne"));
    res->addItemModel(DEFin_grp, DEFin_itemWithIdCluster, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
    res->addItemAttributeModel(DEFin_itemWithIdCluster, DEF_inAttIDcluster, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::ANY, tr("ID Cluster"));
}

// Creation and affiliation of OUT models
void SEG_StepExtractPointsByCluster02::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _outScene_ModelName, new CT_Scene(), tr("Scène extraite"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepExtractPointsByCluster02::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void SEG_StepExtractPointsByCluster02::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resInScene = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut = outResultList.at(0);

    CT_ResultGroupIterator itMainGrp(resOut, this, DEFin_mainGrp);
    while (itMainGrp.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* mainGroup = itMainGrp.next();
        if (mainGroup != NULL)
        {
            CT_Image2D<qint32>* clusters = (CT_Image2D<qint32>*) mainGroup->firstItemByINModelName(this, DEFin_clusters);
            if (clusters != NULL)
            {
                qint32 maxIDCluster = clusters->dataMax();
                QVector<CT_StandardItemGroup*> groups(maxIDCluster + 1);
                QVector<CT_PointCloudIndexVector*> clouds(maxIDCluster + 1);
                groups.fill(NULL);

                for (int i = 0 ; i <= maxIDCluster ; i++)
                {
                    clouds[i] = new CT_PointCloudIndexVector();
                }

                CT_GroupIterator grpIt(mainGroup, this, DEFin_grp);
                while (grpIt.hasNext()  && !isStopped())
                {
                    CT_StandardItemGroup* group = (CT_StandardItemGroup*) grpIt.next();

                    CT_AbstractSingularItemDrawable* item = group->firstItemByINModelName(this, DEFin_itemWithIdCluster);
                    if (item != NULL)
                    {
                        qint32 clusterID = -1;
                        CT_AbstractItemAttribute* att = item->firstItemAttributeByINModelName(resOut, this, DEF_inAttIDcluster);
                        if (att != NULL) {clusterID = att->toInt(item, NULL);}

                        if (clusterID >= 0)
                        {
                            groups[clusterID] = group;
                        }
                    }
                }

                setProgress(5);


                CT_ResultItemIterator itScenes(resInScene, this, DEFin_scene);
                while (itScenes.hasNext() && !isStopped())
                {
                    const CT_AbstractItemDrawableWithPointCloud *scene = (const CT_AbstractItemDrawableWithPointCloud*) itScenes.next();
                    const CT_AbstractPointCloudIndex *inCloudIndex = scene->getPointCloudIndex();
                    if (scene != NULL && inCloudIndex != NULL)
                    {
                        size_t cpt = 0;
                        float sceneSize = inCloudIndex->size();

                        CT_PointIterator itP(inCloudIndex);
                        while(itP.hasNext() && (!isStopped()))
                        {
                            const CT_Point &point = itP.next().currentPoint();
                            size_t index = itP.currentGlobalIndex();
                            qint32 clusterForPoint = clusters->valueAtCoords(point(0), point(1));
                            if (clusterForPoint >= 0 && clusterForPoint != clusters->NA())
                            {
                                clouds[clusterForPoint]->addIndex(index);
                            }
                            if (++cpt % 100 == 0) {setProgress(90.0*(float)cpt/sceneSize + 5.0);}
                        }
                    }
                }

                for (int i = 0 ; i <= maxIDCluster ; i++)
                {
                    CT_PointCloudIndexVector* cloud = clouds[i];
                    if (cloud->size() > 0)
                    {
                        CT_Scene* scene = new CT_Scene(_outScene_ModelName.completeName(), resOut, PS_REPOSITORY->registerPointCloudIndex(cloud));
                        scene->updateBoundingBox();

                        if (groups[i] != NULL)
                        {
                            groups[i]->addItemDrawable(scene);
                        } else {
                            qDebug() << "Problème cluster i = " << i;
                        }
                    } else {
                        delete cloud;
                    }
                }
            }
        }
    }
}

