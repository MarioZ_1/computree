#ifndef SEG_STEPGAUSSIANFILTER_H
#define SEG_STEPGAUSSIANFILTER_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

class SEG_StepGaussianFilter: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepGaussianFilter();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _sigma;


    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inRaster;
    CT_HandleOutSingularItem<CT_Image2D<float> >                    _outRaster;

};

#endif // SEG_STEPGAUSSIANFILTER_H
