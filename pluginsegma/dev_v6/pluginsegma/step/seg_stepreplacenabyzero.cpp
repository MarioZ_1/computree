#include "seg_stepreplacenabyzero.h"

SEG_StepReplaceNAByZero::SEG_StepReplaceNAByZero() : SuperClass()
{
}

QString SEG_StepReplaceNAByZero::description() const
{
    return tr("0- Remplacer les valeurs NA par Zéro");
}

QString SEG_StepReplaceNAByZero::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepReplaceNAByZero::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepReplaceNAByZero::createNewInstance() const
{
    return new SEG_StepReplaceNAByZero();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepReplaceNAByZero::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inRaster, tr("Raster"));
}

void SEG_StepReplaceNAByZero::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outRaster, tr("Raster sans NA"));
}

void SEG_StepReplaceNAByZero::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* inGrid : grp->singularItems(_inRaster))
        {
            if (isStopped()) {return;}

            CT_Image2D<float> *outGrid = new CT_Image2D<float>(inGrid->minX(), inGrid->minY(), inGrid->xdim(), inGrid->ydim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());

            outGrid->getMat() = const_cast<CT_Image2D<float>*>(inGrid)->getMat().clone();

            for (int xx = 0 ; xx < inGrid->xdim() ; xx++)
            {
                for (int yy = 0 ; yy < inGrid->ydim() ; yy++)
                {
                    float val = inGrid->value(xx, yy);
                    if (qFuzzyCompare(val, inGrid->NA()) || val < 0)
                    {
                        outGrid->setValue(xx, yy, 0);
                    }
                }
            }

            outGrid->computeMinMax();

            grp->addSingularItem(_outRaster, outGrid);
        }
    }
}
