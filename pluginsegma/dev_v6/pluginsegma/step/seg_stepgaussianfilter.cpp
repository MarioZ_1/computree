#include "seg_stepgaussianfilter.h"

#include "opencv2/imgproc/imgproc.hpp"

SEG_StepGaussianFilter::SEG_StepGaussianFilter() : SuperClass()
{
    _sigma = 0.40;
}

QString SEG_StepGaussianFilter::description() const
{
    return tr("2- Filtre Gaussien");
}

QString SEG_StepGaussianFilter::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepGaussianFilter::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepGaussianFilter::createNewInstance() const
{
    return new SEG_StepGaussianFilter();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepGaussianFilter::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inRaster, tr("Raster"));
}

void SEG_StepGaussianFilter::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outRaster, tr("Lissage gaussien"));
}

void SEG_StepGaussianFilter::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Sigma (Ecart-type)"), tr("en mètres"), 0, 1000, 2, _sigma, 1);
    postInputConfigDialog->addEmpty();
    postInputConfigDialog->addTitle(tr("N.B. : Portée du filtre = 7.7 x Sigma (en mètres)"));
}

void SEG_StepGaussianFilter::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* imageIn : grp->singularItems(_inRaster))
        {
            if (isStopped()) {return;}

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<float>* filteredImage = new CT_Image2D<float>(min(0), min(1), imageIn->xdim(), imageIn->ydim(), imageIn->resolution(), imageIn->level(), imageIn->NA(), imageIn->NA());
            grp->addSingularItem(_outRaster, filteredImage);

            double sigma = _sigma / imageIn->resolution();
            cv::GaussianBlur(const_cast<CT_Image2D<float>*>(imageIn)->getMat(), filteredImage->getMat(), cv::Size2d(0, 0), sigma);
            filteredImage->computeMinMax();
        }
    }
}
