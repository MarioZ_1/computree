#ifndef SEG_STEPEXTRACTPOINTSBYCLUSTER02_H
#define SEG_STEPEXTRACTPOINTSBYCLUSTER02_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_scene.h"

class SEG_StepExtractPointsByCluster02: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepExtractPointsByCluster02();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:
    CT_HandleInResultGroup<>                                                        _inResultSc;
    CT_HandleInStdZeroOrMoreGroup                                                   _inZeroOrMoreRootGroupSc;
    CT_HandleInStdGroup<>                                                           _inGroupSc;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                  _inScene;

    CT_HandleInResultGroupCopy<>                                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                           _inMainGroup;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                    _inClusters;
    CT_HandleInStdGroup<>                                                           _inGroup;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                        _inItemWithID;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER> _inAttID;

    CT_HandleOutSingularItem<CT_Scene>                                              _outScene;
};

#endif // SEG_STEPEXTRACTPOINTSBYCLUSTER02_H
