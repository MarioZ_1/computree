#ifndef SEG_STEPFILTERMAXIMABYEXCLUSIONRADIUS_H
#define SEG_STEPFILTERMAXIMABYEXCLUSIONRADIUS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_referencepoint.h"

class SEG_StepFilterMaximaByExclusionRadius: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepFilterMaximaByExclusionRadius();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    QStringList _fileName;
    bool        _createMaximaPts;


    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inHeights;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                    _inMaxima;

    CT_HandleOutSingularItem<CT_Image2D<qint32> >                   _outMaxima;
    CT_HandleOutStdGroup                                            _outGroupMaximaPoint;
    CT_HandleOutSingularItem<CT_ReferencePoint>                     _outMaximaPoint;

    double getRadius(double height, const QMap<double, double> &radii);
};

#endif // SEG_STEPFILTERMAXIMABYEXCLUSIONRADIUS_H
