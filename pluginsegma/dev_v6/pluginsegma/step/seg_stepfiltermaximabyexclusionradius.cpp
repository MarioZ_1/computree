#include "seg_stepfiltermaximabyexclusionradius.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

SEG_StepFilterMaximaByExclusionRadius::SEG_StepFilterMaximaByExclusionRadius() : SuperClass()
{
    _createMaximaPts = true;
}

QString SEG_StepFilterMaximaByExclusionRadius::description() const
{
    return tr("4- Filtrer les maxima par des rayons d'exclusion");
}

QString SEG_StepFilterMaximaByExclusionRadius::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepFilterMaximaByExclusionRadius::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepFilterMaximaByExclusionRadius::createNewInstance() const
{
    return new SEG_StepFilterMaximaByExclusionRadius();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepFilterMaximaByExclusionRadius::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Maxima)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inHeights, tr("Image (hauteurs)"));
    manager.addItem(_inGroup, _inMaxima, tr("Maxima"));
}

void SEG_StepFilterMaximaByExclusionRadius::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outMaxima, tr("Maxima filtrés"));

    if (_createMaximaPts)
    {
        manager.addGroup(_inGroup, _outGroupMaximaPoint, tr("Maxima filtrés (Pts)"));
        manager.addItem(_outGroupMaximaPoint, _outMaximaPoint, tr("Maximum"));
    }
}

void SEG_StepFilterMaximaByExclusionRadius::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool("", "", tr("Créer des points pour les maxima"), _createMaximaPts);
}

void SEG_StepFilterMaximaByExclusionRadius::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
   // TODOV6 - le paramètre _fileName pose un problème lors du chargement du script
    postInputConfigDialog->addFileChoice(tr("Fichier de paramètres"),CT_FileChoiceButton::OneExistingFile , "Fichier de paramètres (*.*)", _fileName);
}


void SEG_StepFilterMaximaByExclusionRadius::compute()
{
    QMap<double, double> radii;

    if (_fileName.size() > 0)
    {
        QFile parameterFile(_fileName.first());
        if (parameterFile.exists() && parameterFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&parameterFile);

            while (!stream.atEnd())
            {
                QString line = stream.readLine();
                if (!line.isEmpty())
                {
                    QStringList values = line.split("\t");

                    if (values.size() > 1)
                    {
                        bool ok1, ok2;
                        double height = values.at(0).toDouble(&ok1);
                        double radius = values.at(1).toDouble(&ok2);

                        if (ok1 && ok2)
                        {
                            radii.insert(height, radius);
                        }
                    }
                }
            }
            parameterFile.close();
        }
    }

    if (!radii.contains(0)) {radii.insert(0, radii.first());}
    radii.insert(std::numeric_limits<double>::max(), radii.last());

    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* imageIn : grp->singularItems(_inHeights))
        {
            if (isStopped()) {return;}

            const CT_Image2D<qint32>* maximaIn = grp->singularItem(_inMaxima);


            if (maximaIn != nullptr)
            {
                Eigen::Vector2d min;
                maximaIn->getMinCoordinates(min);
                CT_Image2D<qint32>* filteredMaxima = new CT_Image2D<qint32>(min(0), min(1), maximaIn->xdim(), maximaIn->ydim(), maximaIn->resolution(), maximaIn->level(), maximaIn->NA(), 0);
                grp->addSingularItem(_outMaxima, filteredMaxima);

                filteredMaxima->getMat() = const_cast<CT_Image2D<qint32>*>(maximaIn)->getMat().clone();

                setProgress(20);

                // Get maxima coordinates list
                QMultiMap<qint32, Eigen::Vector3d*> maximaCoords;
                QMultiMap<double, qint32> maximaHeights;

                for (int xx = 0 ; xx < maximaIn->xdim() ; xx++)
                {
                    for (int yy = 0 ; yy < maximaIn->ydim() ; yy++)
                    {
                        qint32 maximaID = maximaIn->value(xx, yy);

                        if (maximaID > 0 && maximaID != maximaIn->NA())
                        {
                            Eigen::Vector3d* coords = new Eigen::Vector3d();
                            if (maximaIn->getCellCenterCoordinates(xx, yy, *coords))
                            {
                                (*coords)(2) = double(imageIn->value(xx, yy));
                                maximaCoords.insert(maximaID, coords);
                                maximaHeights.insert((*coords)(2), maximaID);
                            }
                        }
                    }
                }

                setProgress(25);

                // Compute ordered vector of maxima ids
                QList<qint32> validMaxima;

                QMapIterator<double, qint32> itH(maximaHeights);
                itH.toBack();
                while (itH.hasPrevious())
                {
                    itH.previous();
                    qint32 cl = itH.value();
                    if (!validMaxima.contains(cl)) {validMaxima.append(cl);}
                }

                QVector<qint32> orderedMaxima = validMaxima.toVector();
                int mxSize = orderedMaxima.size();
                validMaxima.clear();

                // Create maxima coords vector
                QVector<Eigen::Vector3d> coords(mxSize);
                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 id = orderedMaxima.at(i);

                    QList<Eigen::Vector3d*> coordinates = maximaCoords.values(id);
                    coords[i] = *(coordinates.at(0));

                    // Compute position of the current maxima if more than one pixel
                    int size = coordinates.size();
                    if (size > 1)
                    {
                        for (int j = 1 ; j < size ; j++)
                        {
                            Eigen::Vector3d* pos = coordinates.at(j);
                            coords[i](0) += (*pos)(0);
                            coords[i](1) += (*pos)(1);
                            if ((*pos)(2) > coords[i](2)) {coords[i](2) = (*pos)(2);}
                        }

                        coords[i](0) /= size;
                        coords[i](1) /= size;
                    }
                }

                setProgress(30);

                // For each radius, test others
                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 id = orderedMaxima.at(i);

                    if (id > 0)
                    {
                        double x = coords[i](0);
                        double y = coords[i](1);
                        double z = coords[i](2);
                        double radius = getRadius(z, radii);

                        // detect the maximum to remove
                        for (int j = i + 1 ; j < mxSize ; j++)
                        {
                            qint32 idTested = orderedMaxima.at(j);

                            if (idTested > 0)
                            {
                                double dist = sqrt(pow(x - coords[j](0), 2) + pow(y - coords[j](1), 2));
                                                       if (dist < radius)
                                {
                                    orderedMaxima[j] = 0;
                                }
                            }
                        }
                    }

                    setProgress(float(29.0*i / mxSize + 30.0));
                }

                setProgress(60);

                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 cl = orderedMaxima.at(i);
                    if (cl > 0)
                    {
                        validMaxima.append(cl);

                        double x = coords[i](0);
                        double y = coords[i](1);
                        double z = coords[i](2);

                        if (_createMaximaPts)
                        {
                            CT_StandardItemGroup* grpPt = new CT_StandardItemGroup();
                            CT_ReferencePoint* refPoint = new CT_ReferencePoint(x, y, z, 0);
                            grp->addGroup(_outGroupMaximaPoint, grpPt);
                            grpPt->addSingularItem(_outMaximaPoint, refPoint);
                        }
                    }
                }

                setProgress(70);

                QMap<qint32, qint32> newIds;
                qint32 cpt = 1;
                // effectively delete toRemove maximum and numbers them in a continuous way
                for (int xx = 0 ; xx < filteredMaxima->xdim() ; xx++)
                {
                    for (int yy = 0 ; yy < filteredMaxima->ydim() ; yy++)
                    {
                        qint32 maximaID = filteredMaxima->value(xx, yy);

                        if (maximaID > 0)
                        {
                            if (validMaxima.contains(maximaID))
                            {
                                qint32 newId = newIds.value(maximaID, 0);
                                if (newId == 0)
                                {
                                    newId = cpt++;
                                    newIds.insert(maximaID, newId);
                                }
                                filteredMaxima->setValue(xx, yy, newId);
                            } else {
                                filteredMaxima->setValue(xx, yy, 0);
                            }
                        }
                    }
                }
                newIds.clear();
                setProgress(90);

                filteredMaxima->computeMinMax();

                qDeleteAll(maximaCoords.values());
                setProgress(99);
            }
        }
    }
    setProgress(100);
}

double SEG_StepFilterMaximaByExclusionRadius::getRadius(double height, const QMap<double, double> &radii)
{
    double radius = 0;
    bool stop = false;
    QMapIterator<double, double> it(radii);
    while (it.hasNext() && !stop)
    {
        it.next();
        double h = it.key();
        double r = it.value();

        if (height >= h)
        {
            radius = r;
        } else {
            stop = true;
        }
    }

    return radius;
}
