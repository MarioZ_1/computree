#include "seg_pluginmanager.h"

//#include "metric/seg_metricrastersegma.h"
#include "step/seg_stepanalyzeandfitcrowns.h"
#include "step/seg_stepcavityfill.h"
#include "step/seg_stepcomputewatershed.h"
#include "step/seg_stepdetectmaxima.h"
#include "step/seg_stepextractpointsbycluster02.h"
#include "step/seg_stepfiltermaximabyclusterarea.h"
#include "step/seg_stepfiltermaximabyexclusionradius.h"
#include "step/seg_stepgaussianfilter.h"
#include "step/seg_stepreplacenabyzero.h"
#include "step/seg_stepseparateclusters.h"

SEG_PluginManager::SEG_PluginManager() : CT_AbstractStepPlugin()
{
}

SEG_PluginManager::~SEG_PluginManager()
{
}

QString SEG_PluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin SEGMA for Computree\n"
           "AU  - Saint-Onge, Benoit\n"
           "PB  - Université du Québec à Montréal\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/segma/wiki\n"
           "ER  - \n";
}

bool SEG_PluginManager::loadGenericsStep()
{        
    addNewRastersStep<SEG_StepAnalyzeAndFitCrowns>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepCavityFill>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepComputeWatershed>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepDetectMaxima>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepExtractPointsByCluster02>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepFilterMaximaByClusterArea>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepFilterMaximaByExclusionRadius>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepGaussianFilter>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepReplaceNAByZero>(QObject::tr("SEGMA"));
    addNewRastersStep<SEG_StepSeparateClusters>(QObject::tr("SEGMA"));

    return true;
}

bool SEG_PluginManager::loadOpenFileStep()
{
    return true;
}

bool SEG_PluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool SEG_PluginManager::loadActions()
{
    return true;
}

bool SEG_PluginManager::loadExporters()
{
    return true;
}

bool SEG_PluginManager::loadReaders()
{
    return true;
}

bool SEG_PluginManager::loadMetrics()
{
    //addNewMetric(new SEG_MetricRasterSegma());
    return true;

}

