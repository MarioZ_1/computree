#include "seg_stepextractpointsbycluster.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/ct_scene.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"
#define DEFin_mask "maxima"
#define DEFin_resSc "resSc"
#define DEFin_grpSc "grpSc"
#define DEFin_scene "scene"


// Constructor : initialization of parameters
SEG_StepExtractPointsByCluster::SEG_StepExtractPointsByCluster(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

// Step description (tooltip of contextual menu)
QString SEG_StepExtractPointsByCluster::getStepDescription() const
{
    return tr("8- Extraire les points par couronne");
}

// Step detailled description
QString SEG_StepExtractPointsByCluster::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepExtractPointsByCluster::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepExtractPointsByCluster::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepExtractPointsByCluster(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepExtractPointsByCluster::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_resSc = createNewInResultModel(DEFin_resSc, tr("Scène(s)"), "", true);
    resIn_resSc->setZeroOrMoreRootGroup();
    resIn_resSc->addGroupModel("", DEFin_grpSc, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_resSc->addItemModel(DEFin_grpSc, DEFin_scene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène(s)"));


    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_mask, CT_Image2D<quint8>::staticGetType(), tr("Masque"));
}

// Creation and affiliation of OUT models
void SEG_StepExtractPointsByCluster::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _outScene_ModelName, new CT_Scene(), tr("Scène extraite"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepExtractPointsByCluster::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void SEG_StepExtractPointsByCluster::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resInScene = inResultList.at(0);


    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut = outResultList.at(0);

    QMap<CT_Image2D<quint8>*, CT_StandardItemGroup*> groups;
    QMap<CT_Image2D<quint8>*, CT_PointCloudIndexVector*> pointsClouds;

    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(resOut, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        CT_Image2D<quint8>* mask = (CT_Image2D<quint8>*)grp->firstItemByINModelName(this, DEFin_mask);

        if (mask != NULL)
        {
            groups.insert(mask, grp);
            pointsClouds.insert(mask, new CT_PointCloudIndexVector());
        }
    }

    setProgress(5.0);

    // IN results browsing
    QMapIterator<CT_Image2D<quint8>*, CT_PointCloudIndexVector*> it(pointsClouds);

    CT_ResultItemIterator itScenes(resInScene, this, DEFin_scene);
    while (itScenes.hasNext() && !isStopped())
    {
        const CT_AbstractItemDrawableWithPointCloud *scene = (const CT_AbstractItemDrawableWithPointCloud*) itScenes.next();
        const CT_AbstractPointCloudIndex *inCloudIndex = scene->getPointCloudIndex();
        if (scene != NULL && inCloudIndex != NULL)
        {
            size_t cpt = 0;
            float sceneSize = inCloudIndex->size();

            CT_PointIterator itP(inCloudIndex);
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                bool found = false;
                it.toFront();
                while (!found && it.hasNext())
                {
                    it.next();
                    CT_Image2D<quint8>* mask = it.key();
                    if (point(0) >= mask->minX() && point(0) <= mask->maxX() &&
                        point(1) >= mask->minY() && point(1) <= mask->maxY() &&
                        mask->valueAtCoords(point(0), point(1)) > 0)
                    {
                        CT_PointCloudIndexVector* outCloudIndex = it.value();
                        outCloudIndex->addIndex(index);
                        found = true;
                    }
                }
                if (++cpt % 1000 == 0) {setProgress(90.0*(float)cpt/sceneSize + 5.0);}
            }

        }
    }

    QMapIterator<CT_Image2D<quint8>*, CT_StandardItemGroup*> itGrp(groups);
    while (itGrp.hasNext())
    {
        itGrp.next();
        CT_Image2D<quint8>* mask = itGrp.key();
        CT_PointCloudIndexVector* outCloudIndex = pointsClouds.value(mask);
        CT_StandardItemGroup* grp = itGrp.value();

        if (outCloudIndex->size() > 0)
        {
            outCloudIndex->setSortType(CT_AbstractCloudIndex::SortedInAscendingOrder);

            CT_Scene* scene = new CT_Scene(_outScene_ModelName.completeName(), resOut, PS_REPOSITORY->registerPointCloudIndex(outCloudIndex));
            scene->updateBoundingBox();

            grp->addItemDrawable(scene);
        } else {
            delete outCloudIndex;
        }
    }

    setProgress(100);
}

