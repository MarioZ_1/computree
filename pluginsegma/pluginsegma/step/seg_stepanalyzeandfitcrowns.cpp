#include "seg_stepanalyzeandfitcrowns.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/ct_attributeslist.h"
#include "ct_itemdrawable/ct_profile.h"
#include "ct_iterator/ct_groupiterator.h"

#include "ct_shape2ddata/ct_polygon2ddata.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_mainGrp "maingrp"
#define DEFin_clusters "clusters"
#define DEFin_grp "grp"
#define DEFin_heights "heights"
#define DEFin_mask "mask"
#define DEF_inAttIDcluster "idcluster"

// Constructor : initialization of parameters
SEG_StepAnalyzeAndFitCrowns::SEG_StepAnalyzeAndFitCrowns(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _scoreThreshold = 80.0;
    _resolutionOTSU = 0.1;
}

// Step description (tooltip of contextual menu)
QString SEG_StepAnalyzeAndFitCrowns::getStepDescription() const
{
    return tr("7- Analyser / Rogner les couronnes");
}

// Step detailled description
QString SEG_StepAnalyzeAndFitCrowns::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepAnalyzeAndFitCrowns::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepAnalyzeAndFitCrowns::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepAnalyzeAndFitCrowns(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepAnalyzeAndFitCrowns::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *res = createNewInResultModelForCopy(DEFin_res, tr("Couronnes"));
    res->setZeroOrMoreRootGroup();
    res->addGroupModel("", DEFin_mainGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res->addItemModel(DEFin_mainGrp, DEFin_clusters, CT_Image2D<qint32>::staticGetType(), tr("Clusters"));
    res->addGroupModel(DEFin_mainGrp, DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Couronne"));
    res->addItemModel(DEFin_grp, DEFin_heights, CT_Image2D<float>::staticGetType(), tr("Image (hauteurs)"));
    res->addItemAttributeModel(DEFin_heights, DEF_inAttIDcluster, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::ANY, tr("ID Cluster"));
    res->addItemModel(DEFin_grp, DEFin_mask, CT_Image2D<quint8>::staticGetType(), tr("Masque"));
}

// Creation and affiliation of OUT models
void SEG_StepAnalyzeAndFitCrowns::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_res);

    if (res != NULL)
    {
        res->addItemModel(DEFin_mainGrp, _outClusters_ModelName, new CT_Image2D<qint32>(), tr("Clusters modifiés"));
        res->addItemModel(DEFin_grp, _outImage_ModelName, new CT_Image2D<float>(), tr("Image Modifiée"));
        res->addItemModel(DEFin_grp, _outMask_ModelName, new CT_Image2D<quint8>(), tr("Masque Modifié"));

        res->addItemModel(DEFin_grp, _attributes_ModelName, new CT_AttributesList(), tr("Attributs des couronnes"));
        res->addItemAttributeModel(_attributes_ModelName, _attIDCluster_ModelName, new CT_StdItemAttributeT<qint32>(CT_AbstractCategory::DATA_ID), tr("IDCluster"));
        res->addItemAttributeModel(_attributes_ModelName, _attMaxHeight_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("MaxHeight"));
        res->addItemAttributeModel(_attributes_ModelName, _attMaxHeight_X_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_X), tr("XMaxHeight"));
        res->addItemAttributeModel(_attributes_ModelName, _attMaxHeight_Y_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_Y), tr("YMaxHeight"));
        res->addItemAttributeModel(_attributes_ModelName, _attMinHeight_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("MinHeight"));
        res->addItemAttributeModel(_attributes_ModelName, _attCrownArea_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("CrownArea"));
        res->addItemAttributeModel(_attributes_ModelName, _attCentroidX_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_X), tr("XCentroid"));
        res->addItemAttributeModel(_attributes_ModelName, _attCentroidY_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_Y), tr("YCentroid"));
        res->addItemAttributeModel(_attributes_ModelName, _attEccentricity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Eccentricity"));
        res->addItemAttributeModel(_attributes_ModelName, _attSolidity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Solidity"));
        res->addItemAttributeModel(_attributes_ModelName, _attHtoAratio_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("HtoAratio"));
        res->addItemAttributeModel(_attributes_ModelName, _attCVmax_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("CVmax"));
        res->addItemAttributeModel(_attributes_ModelName, _attCentroidShift_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("CentroidShift"));
        res->addItemAttributeModel(_attributes_ModelName, _attVextent_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Vextent"));
        res->addItemAttributeModel(_attributes_ModelName, _attCrRatio_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("CrRatio"));
        res->addItemAttributeModel(_attributes_ModelName, _attDiameter_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Diameter"));
        res->addItemAttributeModel(_attributes_ModelName, _attCircularity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Circularity"));

        res->addItemAttributeModel(_attributes_ModelName, _attScoreCentroidShift_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("ScoreCentroidShift"));
        res->addItemAttributeModel(_attributes_ModelName, _attScoreEccentricity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("ScoreEccentricity"));
        res->addItemAttributeModel(_attributes_ModelName, _attScoreSolidity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("ScoreSolidity"));
        res->addItemAttributeModel(_attributes_ModelName, _attScoreCVmax_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("ScoreCVmax"));
        res->addItemAttributeModel(_attributes_ModelName, _attScore_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score"));
        res->addItemAttributeModel(_attributes_ModelName, _attOtsuThreshold_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("OtsuThreshold"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepAnalyzeAndFitCrowns::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Seuil en-dessous duquel la couronne est amputée"), "%", 0, 100, 0, _scoreThreshold);
    configDialog->addDouble(tr("Résolution du profil pour le seuillage d'OTSU"), "m", 0.01, 100, 2, _resolutionOTSU);
}



void SEG_StepAnalyzeAndFitCrowns::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    // COPIED results browsing
    CT_ResultGroupIterator itCpy(res, this, DEFin_mainGrp);
    while (itCpy.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* mainGrp = (CT_StandardItemGroup*) itCpy.next();
        CT_Image2D<qint32>* clustersIn = (CT_Image2D<qint32>*)mainGrp->firstItemByINModelName(this, DEFin_clusters);

        if (clustersIn != NULL)
        {
            CT_Image2D<qint32>* clustersOut = new CT_Image2D<qint32>(_outClusters_ModelName.completeName(), res, clustersIn->minX(), clustersIn->minY(), clustersIn->colDim(), clustersIn->linDim(), clustersIn->resolution(), clustersIn->level(), -1, -1);
            mainGrp->addItemDrawable(clustersOut);

            size_t cpt = 0;
            size_t maxCpt = clustersIn->dataMax();
            CT_GroupIterator itCpy(mainGrp, this, DEFin_grp);
            while (itCpy.hasNext() && !isStopped())
            {

                CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy.next();

                CT_Image2D<float>* heightsIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_heights);
                CT_Image2D<quint8>* maskIn = (CT_Image2D<quint8>*)grp->firstItemByINModelName(this, DEFin_mask);

                qint32 cluster = 0;
                CT_AbstractItemAttribute* att = heightsIn->firstItemAttributeByINModelName(res, this, DEF_inAttIDcluster);
                if (att != NULL) {cluster = att->toInt(heightsIn, NULL);}


                if (heightsIn != NULL && maskIn != NULL)
                {

                    CT_Image2D<quint8>* maskOut = new CT_Image2D<quint8>(_outMask_ModelName.completeName(), res, maskIn->minX(), maskIn->minY(), maskIn->colDim(), maskIn->linDim(), maskIn->resolution(), maskIn->level(), maskIn->NA(), 0);
                    grp->addItemDrawable(maskOut);
                    maskOut->getMat() = maskIn->getMat().clone();

                    CT_Image2D<float>* heightsOut = new CT_Image2D<float>(_outImage_ModelName.completeName(), res, heightsIn->minX(), heightsIn->minY(), heightsIn->colDim(), heightsIn->linDim(), heightsIn->resolution(), heightsIn->level(), heightsIn->NA(), 0);
                    grp->addItemDrawable(heightsOut);
                    heightsOut->getMat() = heightsIn->getMat().clone();

                    heightsOut->computeMinMax();
                    maskOut->computeMinMax();

                    // Calcul des indicateurs
                    computeIndicators(heightsOut, maskOut);

                    // Compute scores
                    double attScoreCentroidShift = standardize(_attCentroidShift, 2.0, 15.1, true);
                    double attScoreEccentricity = standardize(_attEccentricity, 0.3, 0.8, true);
                    double attScoreSolidity = standardize(_attSolidity, 0.3, 0.8, false);
                    double attScoreCVmax = standardize(_attCVmax, 0.8, 0.12, true);

                    double OTSUvalue = -1;
                    double attScore = (attScoreCentroidShift + attScoreEccentricity + attScoreSolidity + attScoreCVmax) / 4.0;

                    if (attScore < _scoreThreshold)
                    {
                        // Création du profil de distribution de hauteur
                        CT_Profile<float>* profile = CT_Profile<float>::createProfileFromSegment(NULL, NULL, 0, 0, _attMinHeight, 0, 0, _attMaxHeight, _resolutionOTSU, -1, 0);

                        for (size_t xx = 0 ; xx < heightsOut->colDim() ; xx++)
                        {
                            for (size_t yy = 0 ; yy < heightsOut->linDim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    float height = heightsOut->value(xx, yy);
                                    if (height != heightsOut->NA())
                                    {
                                        profile->addValueForXYZ(0, 0, height, 1);
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        // Calcul du seuil d'OTSU
                        OTSUvalue = profile->getOtsuThreshold(NULL, NULL) + _attMinHeight;

                        // Filtrage du masque
                        for (size_t xx = 0 ; xx < heightsOut->colDim() ; xx++)
                        {
                            for (size_t yy = 0 ; yy < heightsOut->linDim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    float height = heightsOut->value(xx, yy);
                                    if (height != heightsOut->NA() && height < OTSUvalue)
                                    {
                                        maskOut->setValue(xx, yy, 0);
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        // Remplissage des trous dans le masque
                        std::vector<std::vector<cv::Point> > contours2;
                        cv::findContours(maskOut->getMat(), contours2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                        cv::Scalar color(1);
                        cv::drawContours(maskOut->getMat(), contours2, -1, color, -1);

                        // Ne conserve d'un segment (le plus gros)
                        cv::Mat labels;
                        cv::Mat stats;
                        cv::Mat centroids;
                        int Nsegs = cv::connectedComponentsWithStats(maskOut->getMat(), labels, stats, centroids, 4);
                        if (Nsegs > 2)
                        {
                            int maxArea = 0;
                            int keptLabel = 1;
                            for (int i = 1 ; i < Nsegs ; i++)
                            {
                                int area = stats.at<int>(i, cv::CC_STAT_AREA);
                                if (area > maxArea)
                                {
                                   maxArea = area;
                                   keptLabel = i;
                                }
                            }

                            for (size_t xx = 0 ; xx < maskOut->colDim() ; xx++)
                            {
                                for (size_t yy = 0 ; yy < maskOut->linDim() ; yy++)
                                {
                                    size_t index;
                                    if (maskOut->index(xx, yy, index))
                                    {
                                        if (labels.at<int>(yy, xx) != keptLabel)
                                        {
                                            maskOut->setValue(xx, yy, maskOut->NA());
                                        }
                                    } else {
                                        qDebug() << "Problème";
                                    }
                                }
                            }
                        }


                        // Création du raster final de hauteur
                        for (size_t xx = 0 ; xx < heightsOut->colDim() ; xx++)
                        {
                            for (size_t yy = 0 ; yy < heightsOut->linDim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    if (maskOut->value(xx, yy) == 0 || maskOut->value(xx, yy) == maskOut->NA())
                                    {
                                        heightsOut->setValue(xx, yy, heightsOut->NA());
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        heightsOut->computeMinMax();
                        maskOut->computeMinMax();

                        // Mise à jour des indicateurs
                        computeIndicators(heightsOut, maskOut);
                    }

                    // Mise à jour du raster final des clusters
                    for (size_t xx = 0 ; xx < maskOut->colDim() ; xx++)
                    {
                        for (size_t yy = 0 ; yy < maskOut->linDim() ; yy++)
                        {
                            size_t index;
                            if (maskOut->index(xx, yy, index))
                            {
                                if (maskOut->value(xx, yy) >0)
                                {
                                    double x = maskOut->getCellCenterColCoord(xx);
                                    double y = maskOut->getCellCenterLinCoord(yy);
                                    clustersOut->setValueAtCoords(x, y, cluster);
                                }
                            } else {
                                qDebug() << "Problème";
                            }
                        }
                    }

                    // Ajout des valeurs d'attributs dans le résultat de sortie
                    CT_AttributesList *attributes = new CT_AttributesList(_attributes_ModelName.completeName(), res);
                    grp->addItemDrawable(attributes);

                    attributes->addItemAttribute(new CT_StdItemAttributeT<qint32>(_attIDCluster_ModelName.completeName(),           CT_AbstractCategory::DATA_ID   ,    res, cluster));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attMaxHeight_ModelName.completeName(),           CT_AbstractCategory::DATA_VALUE,    res, _attMaxHeight));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attMaxHeight_X_ModelName.completeName(),         CT_AbstractCategory::DATA_VALUE,    res, _attMaxHeight_X));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attMaxHeight_Y_ModelName.completeName(),         CT_AbstractCategory::DATA_VALUE,    res, _attMaxHeight_Y));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attMinHeight_ModelName.completeName(),           CT_AbstractCategory::DATA_VALUE,    res, _attMinHeight));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCrownArea_ModelName.completeName(),           CT_AbstractCategory::DATA_VALUE,    res, _attCrownArea));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCentroidX_ModelName.completeName(),           CT_AbstractCategory::DATA_X,        res, _attCentroidX));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCentroidY_ModelName.completeName(),           CT_AbstractCategory::DATA_Y,        res, _attCentroidY));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attEccentricity_ModelName.completeName(),        CT_AbstractCategory::DATA_VALUE,    res, _attEccentricity));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attSolidity_ModelName.completeName(),            CT_AbstractCategory::DATA_VALUE,    res, _attSolidity));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attHtoAratio_ModelName.completeName(),           CT_AbstractCategory::DATA_VALUE,    res, _attHtoAratio));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCVmax_ModelName.completeName(),               CT_AbstractCategory::DATA_VALUE,    res, _attCVmax));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCentroidShift_ModelName.completeName(),       CT_AbstractCategory::DATA_VALUE,    res, _attCentroidShift));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attVextent_ModelName.completeName(),             CT_AbstractCategory::DATA_VALUE,    res, _attVextent));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCrRatio_ModelName.completeName(),             CT_AbstractCategory::DATA_VALUE,    res, _attCrRatio));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attDiameter_ModelName.completeName(),            CT_AbstractCategory::DATA_VALUE,    res, _attDiameter));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attCircularity_ModelName.completeName(),         CT_AbstractCategory::DATA_VALUE,    res, _attCircularity));

                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attScoreCentroidShift_ModelName.completeName(),  CT_AbstractCategory::DATA_VALUE,    res, attScoreCentroidShift));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attScoreEccentricity_ModelName.completeName(),   CT_AbstractCategory::DATA_VALUE,    res, attScoreEccentricity));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attScoreSolidity_ModelName.completeName(),       CT_AbstractCategory::DATA_VALUE,    res, attScoreSolidity));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attScoreCVmax_ModelName.completeName(),          CT_AbstractCategory::DATA_VALUE,    res, attScoreCVmax));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attScore_ModelName.completeName(),               CT_AbstractCategory::DATA_VALUE,    res, attScore));
                    attributes->addItemAttribute(new CT_StdItemAttributeT<double>(_attOtsuThreshold_ModelName.completeName(),       CT_AbstractCategory::DATA_VALUE,    res, OTSUvalue));

                }
                setProgress(((float)cpt++ / (float)maxCpt) * 90.0);
            }
            clustersOut->computeMinMax();
        }
        setProgress(100);
    }
}

void SEG_StepAnalyzeAndFitCrowns::computeIndicators(CT_Image2D<float>* heightsOut, CT_Image2D<quint8>* maskOut)
{
    _attMinHeight = heightsOut->dataMin();
    _attMaxHeight = heightsOut->dataMin();
    _attMaxHeight_X = 0;
    _attMaxHeight_Y = 0;
    _attCrownArea = 0;
    _attCentroidX = 0;
    _attCentroidY = 0;
    _attEccentricity = 0;
    _attSolidity = 0;
    _attHtoAratio = 0;
    _attCVmax = 0;
    _attCentroidShift = 0;
    _attVextent = 0;
    _attCrRatio = 0;
    _attDiameter = 0;
    _attCircularity = 0;


    double weightedCentroidX = 0;
    double weightedCentroidY = 0;
    double sdHeights = 0;
    double heightsSum = 0;
    double crownAreaPix = 0;
    double crownAreaPixWithHeights = 0;
    double heightsMean = 0;

    // Compute eccentricity, solidity circularty and perimeter
    cv::Mat_<quint8> contours = maskOut->getMat().clone();
    std::vector<std::vector<cv::Point> > contoursPoints;

    cv::findContours(contours, contoursPoints, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    if (contoursPoints.size() > 0 && (contoursPoints[0].size() >= 5))
    {
        double areaContour = cv::contourArea(contoursPoints[0]);
        double perimeter = 0;

        cv::RotatedRect ellipseRect = cv::fitEllipse(contoursPoints[0]);
        cv::Size2f ellipseAxis = ellipseRect.size;

        double demiGrandAxe = std::max(ellipseAxis.height, ellipseAxis.width) / 2.0;
        double demiPetitAxe = std::min(ellipseAxis.height, ellipseAxis.width) / 2.0;
        _attEccentricity = sqrt(1 - (demiPetitAxe*demiPetitAxe/(demiGrandAxe*demiGrandAxe)));


        //cv::Moments moments = cv::moments(contoursPoints[0]);
        //int cx = int(moments.m10/moments.m00); // centroid image
        //int cy = int(moments.m01/moments.m00); // centroid image

        perimeter = cv::arcLength(contoursPoints[0],true) * heightsOut->resolution();

        std::vector<cv::Point> convexHullPoints;
        cv::convexHull(contoursPoints[0], convexHullPoints);

        _attSolidity = areaContour / cv::contourArea(convexHullPoints);

        _attCircularity = 1;
        if (perimeter != 0) {_attCircularity = sqrt(areaContour* heightsOut->resolution()* heightsOut->resolution() / M_PI) * 2.0 * M_PI / perimeter;}

    } else {
        _attEccentricity = 1;
        _attSolidity = 1;
        _attCircularity = 1;
    }


    // Compute attCentroidX, attCentroidY, weightedCentroidX, weightedCentroidY, attCrownArea, sdHeights
    for (size_t xx = 0 ; xx < heightsOut->colDim() ; xx++)
    {
        for (size_t yy = 0 ; yy < heightsOut->linDim() ; yy++)
        {
            size_t index;
            if (heightsOut->index(xx, yy, index))
            {
                Eigen::Vector3d cellCenter;
                heightsOut->getCellCenterCoordinates(index, cellCenter);

                if (maskOut->valueAtIndex(index) != maskOut->NA())
                {
                    _attCentroidX += cellCenter(0);
                    _attCentroidY += cellCenter(1);

                    crownAreaPix += 1;
                }

                float height = heightsOut->value(xx, yy);
                if (height != heightsOut->NA())
                {
                    heightsSum += height;

                    if (height > _attMaxHeight)
                    {
                        _attMaxHeight = height;
                        _attMaxHeight_X = heightsOut->getCellCenterColCoord(xx);
                        _attMaxHeight_Y = heightsOut->getCellCenterLinCoord(yy);;
                    }

                    weightedCentroidX += cellCenter(0) * height;
                    weightedCentroidY += cellCenter(1) * height;

                    sdHeights += height*height;
                    crownAreaPixWithHeights += 1;
                }
            } else {
                qDebug() << "Problème";
            }
        }
    }

    if (crownAreaPix > 0)
    {
        //_attMaxHeight = heightsOut->dataMax();
        _attVextent = _attMaxHeight - _attMinHeight;
        if (_attMaxHeight > 0) {_attCrRatio = _attVextent / _attMaxHeight;}

        if (crownAreaPixWithHeights > 0) {heightsMean = heightsSum / crownAreaPixWithHeights;}

        _attCentroidX /= crownAreaPix;
        _attCentroidY /= crownAreaPix;

        if (heightsSum > 0)
        {
            weightedCentroidX /= heightsSum;
            weightedCentroidY /= heightsSum;
        }

        sdHeights = sqrt((1/crownAreaPix)*sdHeights - heightsMean*heightsMean);

        _attCrownArea = crownAreaPix * heightsOut->resolution() * heightsOut->resolution();

        _attHtoAratio = _attMaxHeight / _attCrownArea;

        if (_attMaxHeight > 0) {_attCVmax = sdHeights / _attMaxHeight;}

        _attDiameter = 2.0*sqrt(_attCrownArea / M_PI);

        _attCentroidShift = sqrt(pow(_attCentroidX - weightedCentroidX, 2) + pow(_attCentroidY - weightedCentroidY, 2)) / _attDiameter * 10000.0;
    }
}


double SEG_StepAnalyzeAndFitCrowns::standardize(double val, double min, double max, bool invert)
{
    double result = val;
    if (result > max) {result = max;}
    if (result < min) {result = min;}

    result = 100.0 * (result - min) / (max - min);
    if (invert) {result  = 100.0 - result;}
    return result;
}
