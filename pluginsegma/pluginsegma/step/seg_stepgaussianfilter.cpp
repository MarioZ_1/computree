#include "seg_stepgaussianfilter.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "opencv2/imgproc/imgproc.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"



// Constructor : initialization of parameters
SEG_StepGaussianFilter::SEG_StepGaussianFilter(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _sigma = 0.40;
}

// Step description (tooltip of contextual menu)
QString SEG_StepGaussianFilter::getStepDescription() const
{
    return tr("2- Filtre Gaussien");
}

// Step detailled description
QString SEG_StepGaussianFilter::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepGaussianFilter::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepGaussianFilter::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepGaussianFilter(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepGaussianFilter::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_image, CT_Image2D<float>::staticGetType(), tr("Image"));

}

// Creation and affiliation of OUT models
void SEG_StepGaussianFilter::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _filteredImage_ModelName, new CT_Image2D<float>(), tr("Image filtrée"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepGaussianFilter::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Sigma (Ecart-type)"), tr("en mètres"), 0, 1000, 2, _sigma, 1);
    configDialog->addEmpty();
    configDialog->addTitle(tr("N.B. : Portée du filtre = 7.7 x Sigma (en mètres)"));
}

void SEG_StepGaussianFilter::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        CT_Image2D<float>* imageIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_image);
        if (imageIn != NULL)
        {
            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<float>* filteredImage = new CT_Image2D<float>(_filteredImage_ModelName.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), imageIn->NA(), imageIn->NA());
            grp->addItemDrawable(filteredImage);

            double sigma = _sigma / imageIn->resolution();
            cv::GaussianBlur(imageIn->getMat(), filteredImage->getMat(), cv::Size2d(0, 0), sigma);
            filteredImage->computeMinMax();
        }
    }
}
