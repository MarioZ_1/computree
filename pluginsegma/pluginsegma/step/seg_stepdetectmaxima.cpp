#include "seg_stepdetectmaxima.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/types_c.h"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"

#define DEFin_resDTM "resdtm"
#define DEFin_DTMGrp "dtmgrp"
#define DEFin_DTM "dtm"



// Constructor : initialization of parameters
SEG_StepDetectMaxima::SEG_StepDetectMaxima(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minHeight = 2.0;
}

// Step description (tooltip of contextual menu)
QString SEG_StepDetectMaxima::getStepDescription() const
{
    return tr("3- Détecter les maxima");
}

// Step detailled description
QString SEG_StepDetectMaxima::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepDetectMaxima::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepDetectMaxima::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepDetectMaxima(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepDetectMaxima::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_image, CT_Image2D<float>::staticGetType(), tr("Image"));

    CT_InResultModelGroup *resultDTM = createNewInResultModel(DEFin_resDTM, tr("MNT"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", DEFin_DTMGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(DEFin_DTMGrp, DEFin_DTM, CT_Image2D<float>::staticGetType(), tr("MNT"));
    resultDTM->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);

}

// Creation and affiliation of OUT models
void SEG_StepDetectMaxima::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);

    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _maximaImage_ModelName, new CT_Image2D<qint32>(), tr("Maxima"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepDetectMaxima::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Ne pas détécter de maxima en dessous de"), tr("m"), 0, 99999, 2, _minHeight);
}

void SEG_StepDetectMaxima::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    CT_Image2D<float>* mnt = NULL;
    if (getInputResults().size() > 1)
    {
        CT_ResultGroup* resin_DTM = getInputResults().at(1);
        CT_ResultItemIterator it(resin_DTM, this, DEFin_DTM);
        if (it.hasNext())
        {
            mnt = (CT_Image2D<float>*) it.next();
        }
    }


    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {

        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        CT_Image2D<float>* imageIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_image);
        if (imageIn != NULL)
        {

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<qint32>* maximaImage = new CT_Image2D<qint32>(_maximaImage_ModelName.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), imageIn->NA(), 0);
            grp->addItemDrawable(maximaImage);

            setProgress(10);


            cv::Mat_<float> dilatedMat(imageIn->linDim(), imageIn->colDim());
            cv::Mat maximaMat = cv::Mat::zeros(imageIn->linDim(), imageIn->colDim(), CV_32F); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!

            cv::Mat_<float> &imageMat = imageIn->getMat();
            // Détéction des maxima
            cv::dilate(imageMat, dilatedMat, cv::getStructuringElement(cv::MORPH_RECT, cv::Size2d(3,3)));

            cv::compare(imageMat, dilatedMat, maximaMat, cv::CMP_EQ);

            setProgress(30);

            // numérotation des maxima
            cv::Mat labels = cv::Mat::zeros(imageIn->linDim(), imageIn->colDim(), CV_32S); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!
            cv::connectedComponents(maximaMat, labels);

            cv::Mat_<qint32> labs = labels;
            for (int col = 0 ; col < maximaMat.cols ; col++)
            {
                for (int lin = 0 ; lin < maximaMat.rows ; lin++)
                {
                    float zval = imageMat(lin, col);

                    if (mnt != NULL)
                    {
                        float zmnt = mnt->valueAtCoords(imageIn->getCellCenterColCoord(col), imageIn->getCellCenterLinCoord(lin));
                        if (zmnt != mnt->NA())
                        {
                            zval -= zmnt;
                        }
                    }

                    if (zval < _minHeight)
                    {
                        labs(lin, col) = 0;
                    }
                }
            }

            setProgress(60);

            maximaImage->getMat() = labels;

            setProgress(80);

            maximaImage->computeMinMax();

            setProgress(99);

        }
    }
}
