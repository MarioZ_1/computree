#ifndef SEG_PLUGINENTRY_H
#define SEG_PLUGINENTRY_H

#include "interfaces.h"

class SEG_PluginManager;

class SEG_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    SEG_PluginEntry();
    ~SEG_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    SEG_PluginManager *_pluginManager;
};

#endif // SEG_PLUGINENTRY_H