CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

CONFIG += c++14
MUST_USE_OPENCV = 1

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

COMPUTREE += ctlibmetrics

include($${CT_PREFIX}/include_ct_library.pri)

TARGET = plug_segma

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    seg_pluginentry.h \
    seg_pluginmanager.h \
    step/seg_stepcavityfill.h \
    tools/cavityfillmain.h \
    tools/list_croissance.h \
    tools/commontools.h \
    step/seg_stepgaussianfilter.h \
    step/seg_stepdetectmaxima.h \
    step/seg_stepcomputewatershed.h \
    step/seg_stepextractpointsbycluster.h \
    step/seg_stepseparateclusters.h \
    step/seg_stepanalyzeandfitcrowns.h \
    step/seg_stepfiltermaximabyexclusionradius.h \
    step/seg_stepreplacenabyzero.h \
    step/seg_stepextractpointsbycluster02.h \
    step/seg_stepfiltermaximabyclusterarea.h \
    metric/seg_metricrastersegma.h \
    step/seg_stepdetectmaxima02.h
SOURCES += \
    seg_pluginentry.cpp \
    seg_pluginmanager.cpp \
    step/seg_stepcavityfill.cpp \
    tools/cavityfillmain.cpp \
    tools/list_croissance.cpp \
    tools/commontools.cpp \
    step/seg_stepgaussianfilter.cpp \
    step/seg_stepdetectmaxima.cpp \
    step/seg_stepcomputewatershed.cpp \
    step/seg_stepextractpointsbycluster.cpp \
    step/seg_stepseparateclusters.cpp \
    step/seg_stepanalyzeandfitcrowns.cpp \
    step/seg_stepfiltermaximabyexclusionradius.cpp \
    step/seg_stepreplacenabyzero.cpp \
    step/seg_stepextractpointsbycluster02.cpp \
    step/seg_stepfiltermaximabyclusterarea.cpp \
    metric/seg_metricrastersegma.cpp \
    step/seg_stepdetectmaxima02.cpp

TRANSLATIONS += languages/pluginsegma_en.ts \
                languages/pluginsegma_fr.ts

