CT_PREFIX = ../../computreev5
DEFINES += COMPUTREE_V5

CHECK_CAN_USE_PCL = 1
CHECK_CAN_USE_OPENCV = 1
MUST_USE_USE_PCL = 1
MUST_USE_USE_GSL = 1
MUST_USE_OPENCV = 1

COMPUTREE += ctlibpcl ctliblas ctlibfilters ctlibmetrics ctlibstdactions

include($${CT_PREFIX}/shared.pri)
include($${CT_PREFIX}/include_ct_library.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

greaterThan(QT_MAJOR_VERSION, 4):
QT += concurrent
QT +=  testlib
QT -= gui
CONFIG += c++17
CONFIG += testcase
CONFIG += console
CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += \
    tests/sf_unittestBuildingbrick.cpp \
    tests/sf_unittestGnCircle.cpp \
    tests/sf_unittestLineFitWithIntercept.cpp \
    tests/sf_unittestQsmMerge.cpp \
    tests/sf_unittestRansacLineFitWithIntercept.cpp \
    tests/sf_unittestSegment.cpp \
    tests/sf_unittestcylinder.cpp \
    tests/sf_unittestmain.cpp \
    tests/sf_unittestqsmsearch.cpp \
    tests/sf_unittestqsmsort.cpp
   # tests/sf_unittesttransferfeature.cpp
HEADERS += \
    tests/sf_unittestBuildingbrick.h \
    tests/sf_unittestGnCircle.h \
    tests/sf_unittestLineFitWithIntercept.h \
    tests/sf_unittestQsmMerge.h \
    tests/sf_unittestRansacLineFitWithIntercept.h \
    tests/sf_unittestSegment.h \
    tests/sf_unittestcylinder.h \
    tests/sf_unittestqsmsearch.h \
    tests/sf_unittestqsmsort.h
   # tests/sf_unittesttransferfeature.h


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../ComputreeInstallRelease/plugins/ -lplug_simpleforest
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../ComputreeInstallRelease/plugins/ -lplug_simpleforest
else:unix: LIBS += -L$$PWD/../../ComputreeInstallRelease/plugins/ -lplug_simpleforest

unix:!macx: LIBS += -L/usr/lib/ -lgslcblas

INCLUDEPATH += $$PWD/../../ComputreeInstallRelease/
INCLUDEPATH += $$PWD/../../ComputreeInstallRelease/plugins/
INCLUDEPATH += $${BOOST_INC_PATH}
DEPENDPATH += $$PWD/../../ComputreeInstallRelease/
DEPENDPATH += $$PWD/../../ComputreeInstallRelease/plugins/
DEPENDPATH += $${BOOST_LIBS_PATH}

MOC_DIR = build
OBJECTS_DIR = build
UI_DIR = build

DESTDIR = build
