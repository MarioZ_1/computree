/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITPLAIN_H
#define SF_FITPLAIN_H

#include <pcl/features/normal_3d.h>

#include "pcl/cloud/feature/sf_abstractFeature.h"

template<typename PointType>
class SF_FitPlain
{
private:
  typename pcl::PointCloud<PointType>::Ptr m_cloud;
  pcl::PointXYZ m_pointOnPlane;
  pcl::Normal m_planeNormal;

public:
  SF_FitPlain(typename pcl::PointCloud<PointType>::Ptr cloud_in);
  void compute();
  pcl::PointXYZ pointOnPlane();
  pcl::Normal planeNormal();
};

#include "sf_fitPlain.hpp"

#endif // SF_FITPLAIN_H
