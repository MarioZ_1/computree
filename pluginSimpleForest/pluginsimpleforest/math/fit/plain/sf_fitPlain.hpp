/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITPLAIN_HPP
#define SF_FITPLAIN_HPP

#include "sf_fitPlain.h"
template<typename PointType>
SF_FitPlain<PointType>::SF_FitPlain(typename pcl::PointCloud<PointType>::Ptr cloud_in) : m_cloud(cloud_in)
{
}

template<typename PointType>
void
SF_FitPlain<PointType>::compute()
{
  Eigen::Vector4f xyz_centroid(0, 0, 0, 1);
  pcl::compute3DCentroid(*m_cloud, xyz_centroid);
  m_pointOnPlane.x = xyz_centroid.x();
  m_pointOnPlane.y = xyz_centroid.y();
  m_pointOnPlane.z = xyz_centroid.z();

  Eigen::Matrix3f covariance_matrix;
  pcl::computeCovarianceMatrix(*m_cloud, xyz_centroid, covariance_matrix);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eig(covariance_matrix);
  const auto smallestEigenVec = eig.eigenvectors().col(0);
  m_planeNormal.normal_x = smallestEigenVec[0];
  m_planeNormal.normal_y = smallestEigenVec[1];
  m_planeNormal.normal_z = smallestEigenVec[2];
}

template<typename PointType>
pcl::PointXYZ
SF_FitPlain<PointType>::pointOnPlane()
{
  return m_pointOnPlane;
}

template<typename PointType>
pcl::Normal
SF_FitPlain<PointType>::planeNormal()
{
  return m_planeNormal;
}

#endif // SF_FITPLAIN_HPP
