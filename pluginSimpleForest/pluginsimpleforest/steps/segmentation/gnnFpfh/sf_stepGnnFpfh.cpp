/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepGnnFpfh.h"

#include <converters/CT_To_PCL/sf_converterCTToPCL.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>

#include "pcl/cloud/classification/SF_GnnFpfh.h"
#include "pcl/cloud/feature/fpfh/sf_fpfh.h"

#include <QDirIterator>

SF_StepGnnFpfh::SF_StepGnnFpfh(CT_StepInitializeData& dataInit) : SF_AbstractStepSegmentation(dataInit), m_maxNumberClusters(10)
{
  for (auto i = 0; i < m_maxNumberClusters; ++i) {
    m_outGrpClusters.push_back(CT_AutoRenameModels());
    m_outClusters.push_back(CT_AutoRenameModels());
  }
}

SF_StepGnnFpfh::~SF_StepGnnFpfh() {}

QString
SF_StepGnnFpfh::getStepDescription() const
{
  return tr("Gaussian Mixture Models with Fast Point Feature Histograms");
}

QString
SF_StepGnnFpfh::getStepDetailledDescription() const
{
  return tr("For an input cloud the fast point feature histogram is computed for a given range. Gassian mixture model classification "
            "is performed to create 10 output clusters.");
}

QString
SF_StepGnnFpfh::getStepURL() const
{
  return tr("https://www.simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepGnnFpfh::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepGnnFpfh(dataInit);
}

QStringList
SF_StepGnnFpfh::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepGnnFpfh::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Point Cloud"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Cloud Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Cloud"));
}

void
SF_StepGnnFpfh::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("[<b>Only one input cloud allowed otherwise no manual classification is possible</b>].");
  configDialog->addText("[<b>Crash of this step under Linux was observed</b>].");
  configDialog->addDouble("Run the FPFH estimation with [<em><b>range</b></em>] of  ", "(m).", 0.05, 0.5, 3, m_range);
  configDialog->addText("Normals used of FPFH are computed with [<em><b>range</b></em>] / 2.");
  configDialog->addText(
    "The cloud is downscaled before all computations with [<em><b>range</b></em>] / 6. Later classification is backTransformed");
  configDialog->addInt(
    "Choose the [<em><b>number</b></em>] of gaussian mixture clusters", "", 2, m_maxNumberClusters, m_numberClusters);
  configDialog->addInt("Downscale the 33 dimensional FPFH matrix to [<em><b>dimensions</b></em>]", "", 1, 33, m_dimension);
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepGnnFpfh::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString grpLa = "Cluster grp number ";
    QString cloud = "Cluster number ";
    for (auto i = 0; i < m_maxNumberClusters; ++i) {
      QString grpLa2 = grpLa;
      grpLa2.append(QString::number(i));
      QString cloud2 = cloud;
      cloud2.append(QString::number(i));
      resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, m_outGrpClusters[i], new CT_StandardItemGroup(), grpLa2);
      resModelw->addItemModel(m_outGrpClusters[i], m_outClusters[i], new CT_Scene(), cloud2);
    }
  }
}

void
SF_StepGnnFpfh::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);

    Sf_ConverterCTToPCL<SF_PointNormal> converter;
    converter.setItemCpyCloudInDeprecated(ctCloud);
    converter.compute();
    SF_CloudNormal::Ptr cloud = converter.cloudTranslated();

    SF_CloudNormal::Ptr cloudDownscaled(new SF_CloudNormal());
    pcl::VoxelGrid<SF_PointNormal> sor;
    sor.setInputCloud(cloud);
    float voxelRange = static_cast<float>(m_range / 6.);
    sor.setLeafSize(voxelRange, voxelRange, voxelRange);
    sor.filter(*cloudDownscaled);

    pcl::NormalEstimation<SF_PointNormal, SF_PointNormal> ne;

    ne.setInputCloud(cloudDownscaled);
    pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>());
    ne.setSearchMethod(tree);
    ne.setRadiusSearch(static_cast<float>(m_range / 2.));
    ne.compute(*cloudDownscaled);

    pcl::PointCloud<pcl::FPFHSignature33>::Ptr features(new pcl::PointCloud<pcl::FPFHSignature33>());
    SF_FPFH fpfh(cloudDownscaled, features);
    fpfh.setParameters(static_cast<float>(m_range), 7);
    fpfh.computeFeatures();
    features = fpfh.getFeatures();

    SF_GNNFPFH gnnFpfh(features);
    gnnFpfh.setParameters(m_numberClusters, m_dimension);
    gnnFpfh.compute();
    std::vector<int> indicesClusters = gnnFpfh.getIndices();

    pcl::search::KdTree<pcl::PointXYZINormal>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZINormal>);
    kdtree->setInputCloud(cloudDownscaled);
    std::vector<CT_PointCloudIndexVector*> outIndices;
    for (std::int32_t i = 0; i < m_maxNumberClusters; ++i) {
      outIndices.push_back(new CT_PointCloudIndexVector());
    }
    const CT_AbstractPointCloudIndex* pointCloudIndex = ctCloud->getPointCloudIndex();
    CT_PointIterator itP(pointCloudIndex);
    std::uint32_t indexPCL = 0;
    while (itP.hasNext() && !isStopped()) {
      std::uint32_t indexCt = itP.currentGlobalIndex();
      pcl::PointXYZINormal point = cloud->points[indexPCL];
      indexPCL++;
      std::vector<int> pointIdxNKNSearch(1);
      std::vector<float> pointNKNSquaredDistance(1);
      if (kdtree->nearestKSearch(point, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) {
        int indexPoint = pointIdxNKNSearch[0];
        int indexCluster = indicesClusters[indexPoint];
        outIndices[indexCluster]->addIndex(indexCt);
      }
      itP.next();
    }

    for (std::int32_t i = 0; i < m_maxNumberClusters; ++i) {
      CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpClusters[i].completeName(), outResult);
      group->addGroup(filterGrp);
      CT_Scene* outScene = new CT_Scene(
        m_outClusters[i].completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(outIndices[i]));
      outScene->updateBoundingBox();
      filterGrp->addItemDrawable(outScene);
    }
  }
}
