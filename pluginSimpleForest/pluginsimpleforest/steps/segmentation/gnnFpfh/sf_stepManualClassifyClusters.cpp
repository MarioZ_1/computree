/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepManualClassifyClusters.h"

#include <converters/CT_To_PCL/sf_converterCTToPCL.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>

#include "pcl/sf_math.h"

SF_StepManualClassifyClusters::SF_StepManualClassifyClusters(CT_StepInitializeData& dataInit)
  : SF_AbstractFilterBinaryStep(dataInit), m_maxNumberClusters(10)
{
  for (auto i = 0; i < m_maxNumberClusters; ++i) {
    QString grp = "UnqiueGroup";
    grp.append(QString::number(i));
    QString cluster = "UnqiueCluster";
    cluster.append(QString::number(i));
    m_inGrpClusters.push_back(grp);
    m_inClusters.push_back(cluster);
    m_clusterClassification.push_back(m_unclassified);
  }
}

SF_StepManualClassifyClusters::~SF_StepManualClassifyClusters() {}

QString
SF_StepManualClassifyClusters::getStepDescription() const
{
  return tr("Manual classification of Gaussian Mixture Models");
}

QString
SF_StepManualClassifyClusters::getStepDetailledDescription() const
{
  return tr("Output clusters of the step Gaussian Mixture Models with Fast Point Feature Histograms should be inspected before "
            "executing this step. Each cluster needs to be classified as good, noise or unclassified. All good clusters are merged as "
            "well as all noise clusters. Points in unclassified clusters are allocated afterwards regarding their distance to their "
            "nearest good and noise neighbors.");
}

QString
SF_StepManualClassifyClusters::getStepURL() const
{
  return tr("https://www.simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepManualClassifyClusters::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepManualClassifyClusters(dataInit);
}

QStringList
SF_StepManualClassifyClusters::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepManualClassifyClusters::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Point Cloud"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Cloud Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Cloud"));
  for (auto i = 0; i < m_maxNumberClusters; ++i) {
    QString clustergrpNameString = "Cluster Group number";
    clustergrpNameString.append(QString::number(i));
    QString clusterNameString = "Cluster number";
    clusterNameString.append(QString::number(i));
    resModel->addGroupModel(DEF_IN_GRP_CLUSTER,
                            m_inGrpClusters[i],
                            CT_AbstractItemGroup::staticGetType(),
                            clustergrpNameString,
                            "",
                            CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resModel->addItemModel(m_inGrpClusters[i], m_inClusters[i], CT_Scene::staticGetType(), clusterNameString);
  }
}

void
SF_StepManualClassifyClusters::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addInt(
    "Compute for each unclassfied point the average distance of [<em><b>k</b></em>] nearest neighbors within noise and good points",
    "",
    1,
    999,
    m_k);
  for (auto i = 0; i < m_maxNumberClusters; ++i) {
    QString description = "[<em><b>Classification</b></em>] of the cluster number ";
    description.append(QString::number(i));
    configDialog->addStringChoice(description, " ", m_classifyChoices, m_clusterClassification[i]);
  }

  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepManualClassifyClusters::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != nullptr) {
    resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, _outGrp, new CT_StandardItemGroup(), tr("Classified gaussian mixtures"));
    resModelw->addItemModel(_outGrp, _outCloud, new CT_Scene(), tr("Cloud"));
    resModelw->addItemModel(_outGrp, _outNoise, new CT_Scene(), tr("Noise"));
  }
}

void
SF_StepManualClassifyClusters::compute()
{
  m_computationsDone = 0;
  CT_ResultGroup* outResult = getOutResultList().at(0);
  Eigen::Vector3d translation;

  CT_ResultGroupIterator outResIt2(outResult, this, DEF_IN_GRP_CLUSTER);
  SF_CloudNormal::Ptr cloud(new SF_CloudNormal);
  CT_AbstractItemDrawableWithPointCloud* ctCloud = nullptr;
  CT_StandardItemGroup* group = nullptr;
  while (!isStopped() && outResIt2.hasNext()) {
    group = const_cast<CT_StandardItemGroup*>(static_cast<const CT_StandardItemGroup*>(outResIt2.next()));
    ctCloud = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_IN_CLOUD_SEED);

    Sf_ConverterCTToPCL<SF_PointNormal> converter;
    converter.setItemCpyCloudInDeprecated(ctCloud);
    converter.compute();
    cloud = converter.cloudTranslated();
    translation = converter.translation();
  }
  SF_CloudNormal::Ptr goodCloud(new SF_CloudNormal);
  SF_CloudNormal::Ptr noiseCloud(new SF_CloudNormal);
  for (std::uint32_t i = 0; i < static_cast<std::uint32_t>(m_maxNumberClusters); ++i) {
    CT_ResultGroupIterator outResIt(outResult, this, m_inGrpClusters[i]);
    while (!isStopped() && outResIt.hasNext()) {
      CT_StandardItemGroup* group = const_cast<CT_StandardItemGroup*>(static_cast<const CT_StandardItemGroup*>(outResIt.next()));
      const CT_AbstractItemDrawableWithPointCloud* ctCloud = static_cast<const CT_AbstractItemDrawableWithPointCloud*>(
        group->firstItemByINModelName(this, m_inClusters[i]));
      Sf_ConverterCTToPCL<SF_PointNormal> converter;
      converter.setItemCpyCloudInDeprecated(ctCloud);
      converter.compute(translation);
      SF_CloudNormal::Ptr cloud = converter.cloudTranslated();
      if (m_clusterClassification[i] == m_good) {
        *goodCloud += *cloud;
      } else if (m_clusterClassification[i] == m_noise) {
        *noiseCloud += *cloud;
      }
    }
  }

  pcl::search::KdTree<pcl::PointXYZINormal>::Ptr kdtreeGood(new pcl::search::KdTree<pcl::PointXYZINormal>);
  kdtreeGood->setInputCloud(goodCloud);
  pcl::search::KdTree<pcl::PointXYZINormal>::Ptr kdtreeBad(new pcl::search::KdTree<pcl::PointXYZINormal>);
  kdtreeBad->setInputCloud(noiseCloud);
  CT_PointCloudIndexVector* goodCtIndices = new CT_PointCloudIndexVector();
  CT_PointCloudIndexVector* badCtIndices = new CT_PointCloudIndexVector();

  auto getAverageDistance = [this](pcl::search::KdTree<pcl::PointXYZINormal>::Ptr& kdtree, pcl::PointXYZINormal& point) {
    float distance = std::numeric_limits<float>::max();
    std::vector<int> pointIdxNKNSearch(static_cast<std::uint32_t>(m_k));
    std::vector<float> pointNKNSquaredDistance(static_cast<std::uint32_t>(m_k));
    if (kdtree->nearestKSearch(point, m_k, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) { // TODO
      std::vector<float> distances;
      for (auto sqrdDist : pointNKNSquaredDistance) {
        distances.push_back(std::sqrt(sqrdDist));
      }
      distance = SF_Math<float>::getMean(distances);
    }
    return distance;
  };
  const CT_AbstractPointCloudIndex* pointCloudIndex = ctCloud->getPointCloudIndex();
  CT_PointIterator itP(pointCloudIndex);
  std::uint32_t indexPCL = 0;
  if (noiseCloud->empty()) {
    while (itP.hasNext() && !isStopped()) {
      std::uint32_t indexCt = static_cast<std::uint32_t>(itP.currentGlobalIndex());
      goodCtIndices->addIndex(indexCt);
      itP.next();
    }
  } else if (goodCloud->empty()) {
    while (itP.hasNext() && !isStopped()) {
      std::uint32_t indexCt = static_cast<std::uint32_t>(itP.currentGlobalIndex());
      badCtIndices->addIndex(indexCt);
      itP.next();
    }
  } else {
    while (itP.hasNext() && !isStopped()) {
      std::uint32_t indexCt = static_cast<std::uint32_t>(itP.currentGlobalIndex());
      pcl::PointXYZINormal point = cloud->points[indexPCL];
      indexPCL++;
      auto distGood = getAverageDistance(kdtreeGood, point);
      auto distBad = getAverageDistance(kdtreeBad, point);
      if (distGood < distBad) {
        goodCtIndices->addIndex(indexCt);
      } else {
        badCtIndices->addIndex(indexCt);
      }
      itP.next();
    }
  }

  {
    CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(_outGrp.completeName(), outResult);
    group->addGroup(filterGrp);
    addSceneToFilterGrp(filterGrp, outResult, goodCtIndices, _outCloud.completeName());
    addSceneToFilterGrp(filterGrp, outResult, badCtIndices, _outNoise.completeName());
  }
}
