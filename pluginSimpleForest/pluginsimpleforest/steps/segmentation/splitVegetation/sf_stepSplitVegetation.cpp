/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepSplitVegetation.h"

SF_StepSplitVegetation::SF_StepSplitVegetation(CT_StepInitializeData& dataInit) : SF_AbstractStepSegmentation(dataInit) {}

SF_StepSplitVegetation::~SF_StepSplitVegetation() {}

QString
SF_StepSplitVegetation::getStepDescription() const
{
  return tr("Height based tree identification from segmented clouds.");
}

QString
SF_StepSplitVegetation::getStepDetailledDescription() const
{
  return tr("Splits segmented segmentation clouds into undergrowth and trees."
            " The decision is done by the height of the segmented cluser.");
}

QString
SF_StepSplitVegetation::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepSplitVegetation::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepSplitVegetation(dataInit);
}

QStringList
SF_StepSplitVegetation::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepSplitVegetation::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Vegetation result"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_SCENE,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Segmented vegetation group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_SCENE, DEF_IN_SCENE_CLOUD, CT_Scene::staticGetType(), tr("Segmented vegetation cloud"));
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Root group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
}

void
SF_StepSplitVegetation::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addDouble("The vegetation is split by the [<em><b>threshold height</b></em>]  ", " (m)", 0.1, 100, 1, m_splitHeight);
  configDialog->addText(" into undergrowth and trees.");
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepSplitVegetation::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, m_outGrpUndergrowth, new CT_StandardItemGroup(), tr("Undergrowth group"));
    resModelw->addItemModel(m_outGrpUndergrowth, m_outCloudUndergrowth, new CT_Scene(), tr("Undergrowth cloud"));
    resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, m_outGrpTrees, new CT_StandardItemGroup(), tr("Tree group"));
    resModelw->addItemModel(m_outGrpTrees, m_outCloudTrees, new CT_Scene(), tr("Tree cloud"));
  }
}

void
SF_StepSplitVegetation::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_StandardItemGroup* groupRoot{};
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    groupRoot = (CT_StandardItemGroup*)outResIt.next();
  }

  CT_ResultGroupIterator resultGrpIterator(outResult, this, DEF_IN_SCENE);
  while (!isStopped() && resultGrpIterator.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)resultGrpIterator.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_SCENE_CLOUD);
    Eigen::Vector3d min{ 0, 0, 0 }, max{ 0, 0, 0 };
    ctCloud->getBoundingBox(min, max);
    auto height = max[2] - min[2];
    const CT_AbstractPointCloudIndex* index = ctCloud->getPointCloudIndex();
    CT_PointCloudIndexVector* indexvector = new CT_PointCloudIndexVector;
    if (!index || index->size() == 0u) {
      continue;
    }
    const CT_AbstractPointCloudIndex* cloudIndex = ctCloud->getPointCloudIndex();
    CT_PointIterator itP(cloudIndex);
    while (itP.hasNext() && !isStopped()) {
      const size_t index = itP.next().currentGlobalIndex();
      indexvector->addIndex(index);
    }
    indexvector->setSortType(CT_AbstractCloudIndex::SortType::SortedInAscendingOrder);
    if (height > m_splitHeight) {
      CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpTrees.completeName(), outResult);
      groupRoot->addGroup(filterGrp);
      CT_Scene* outScene = new CT_Scene(
        m_outCloudTrees.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexvector));
      outScene->updateBoundingBox();
      filterGrp->addItemDrawable(outScene);
    } else {
      CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpUndergrowth.completeName(), outResult);
      groupRoot->addGroup(filterGrp);
      CT_Scene* outScene = new CT_Scene(
        m_outCloudUndergrowth.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexvector));
      outScene->updateBoundingBox();
      filterGrp->addItemDrawable(outScene);
    }
  }
}
