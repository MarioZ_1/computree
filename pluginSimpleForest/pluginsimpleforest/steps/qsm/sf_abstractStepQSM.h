/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_ABSTRACTSTEPQSM_H
#define SF_ABSTRACTSTEPQSM_H

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "steps/sf_abstractStep.h"

class SF_AbstractStepQSM : public SF_AbstractStep
{
  Q_OBJECT
public:
  SF_AbstractStepQSM(CT_StepInitializeData& dataInit);

protected:
  CT_TTreeGroup* constructTopology(CT_AbstractResult* result, std::shared_ptr<SF_ModelQSM> qsm);
  void constructRecursively(const CT_AbstractResult* result, CT_TNodeGroup* stemNode, std::shared_ptr<SF_ModelSegment> segment);
  CT_CylinderData* constructCylinderData(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick);
  void setCylinders(const CT_AbstractResult* result, CT_TNodeGroup* stemNode, std::shared_ptr<SF_ModelSegment> segment);

  template<typename T>
  void addQSM(CT_ResultGroup* outResult, QList<T> paramList, QString outResultGrpName, QString outSFQSMName);
  template<typename T>
  void addQSM(CT_ResultGroup* outResult, QList<T> paramList, QString outResultGrpName, QString outSFQSMName, QString outParamName);
  void addQSMToOutResult(CT_OutResultModelGroupToCopyPossibilities* resModelw, QString header, QString group);
  void addAttributes(const CT_AbstractResult* result, CT_Cylinder* cylinder, std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick);
  bool hasSameSize(const std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>>& qsms,
                   const std::vector<const CT_AbstractItemDrawableWithPointCloud*>& clouds) const;

  void addInputQSMs();
  void addInputClouds();
  void createVisualizeCylinders(CT_StepConfigurableDialog* dialog);

  std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>> getInputQSMs();
  std::vector<const CT_AbstractItemDrawableWithPointCloud*> getIputClouds();

  CT_AutoRenameModels _treeGroup;
  CT_AutoRenameModels _stemNodeGroup;
  CT_AutoRenameModels _stemNodeCylinders;
  CT_AutoRenameModels _QSMCloud;
  CT_AutoRenameModels _QSMGrp;

  CT_AutoRenameModels _QSMGrp2;

  CT_AutoRenameModels m_growthVolume;
  CT_AutoRenameModels m_growthLength;
  CT_AutoRenameModels m_branchID;
  CT_AutoRenameModels m_reversePipeOrder;
  CT_AutoRenameModels m_branchOrder;
  CT_AutoRenameModels m_reverseBranchOrder;
  CT_AutoRenameModels m_fittingType;
  CT_AutoRenameModels m_mean;

  bool m_onlyGoodFit = false;

  double m_stemPercentage = 0.2;
};

template<typename T>
void
SF_AbstractStepQSM::addQSM(CT_ResultGroup* outResult, QList<T> paramList, QString outResultGrpName, QString outSFQSMName)
{
  CT_ResultGroupIterator outResIt(outResult, this, outResultGrpName);
  size_t index = 0;
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* qsmGrp = new CT_StandardItemGroup(_QSMGrp.completeName(), outResult);
    T& params = paramList[index++];
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();

    std::shared_ptr<SF_ModelQSM> qsm = params._qsm;
    if (!qsm) {
      _groupsToBeRemoved.push_back(group);
      continue;
    }
    qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);

    CT_TTreeGroup* tree = constructTopology(outResult, qsm);
    group->addGroup(qsmGrp);
    qsmGrp->addGroup(tree);

    SF_QSMItem* qsmItem = new SF_QSMItem(outSFQSMName, outResult, qsm);
    qsmGrp->addItemDrawable(qsmItem);
  }
  paramList.clear();
  removeCorruptedScenes();
}

template<typename T>
void
SF_AbstractStepQSM::addQSM(CT_ResultGroup* outResult,
                           QList<T> paramList,
                           QString outResultGrpName,
                           QString outSFQSMName,
                           QString outParamName)
{
  CT_ResultGroupIterator outResIt(outResult, this, outResultGrpName);
  size_t index = 0;
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* qsmGrp = new CT_StandardItemGroup(_QSMGrp.completeName(), outResult);
    T& params = paramList[index++];
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    std::shared_ptr<SF_ModelQSM> qsm = params._qsm;
    if (!qsm) {
      _groupsToBeRemoved.push_back(group);
      continue;
    }
    qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);

    CT_TTreeGroup* tree = constructTopology(outResult, qsm);
    group->addGroup(qsmGrp);
    qsmGrp->addGroup(tree);
    if (outParamName != "") {
      SF_SphereFollowingParametersItem* paramItem = new SF_SphereFollowingParametersItem(outParamName, outResult, params);
      qsmGrp->addItemDrawable(paramItem);
    }
    SF_QSMItem* qsmItem = new SF_QSMItem(outSFQSMName, outResult, qsm);
    qsmGrp->addItemDrawable(qsmItem);
  }
  paramList.clear();
  removeCorruptedScenes();
}

#endif // SF_ABSTRACTSTEPQSM_H
