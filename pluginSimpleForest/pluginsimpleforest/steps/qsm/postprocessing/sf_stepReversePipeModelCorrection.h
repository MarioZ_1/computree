#ifndef SF_STEPREVERSEPIPEMODELCORRECTION_H
#define SF_STEPREVERSEPIPEMODELCORRECTION_H

#include "steps/qsm/sf_abstractStepQSM.h"

#include <QString>

class SF_StepReversePipeModelCorrection : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepReversePipeModelCorrection(CT_StepInitializeData& dataInit);
  ~SF_StepReversePipeModelCorrection();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void createParamList(CT_ResultGroup* out_result);
  virtual void createPreConfigurationDialog() {}
  virtual void createPostConfigurationDialog();
  void compute();
  QList<SF_ParamReversePipeModelCorrection> _paramList;
  void createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog);

private:
  CT_AutoRenameModels m_outCloudItem;
  CT_AutoRenameModels _outCylinderGroup;
  CT_AutoRenameModels _outCylinders;
  CT_AutoRenameModels _outSFQSM;
  int toStringSFMethod();
  SF_CLoudToModelDistanceMethod toStringCMDMethod();

  double _minRadius = 0.0025;
  double _range = 0.25;
  bool m_doSplit = true;
  int m_minPts = 5;
  double m_inlierDistance = 0.015;
  int m_ransacIterations = 1000;
};

#endif // SF_STEPREVERSEPIPEMODELCORRECTION_H
