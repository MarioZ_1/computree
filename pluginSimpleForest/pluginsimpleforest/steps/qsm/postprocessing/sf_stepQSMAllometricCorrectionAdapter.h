/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPQSMALLOMETRICCHECKADAPTER_H
#define SF_STEPQSMALLOMETRICCHECKADAPTER_H

#include <QThreadPool>

#include "math/fit/line/sf_fitLineWithIntercept.h"
#include "qsm/algorithm/postprocessing/sf_QSMAllometricCorrectionParameterEstimation.h"
#include "qsm/algorithm/postprocessing/sf_qsmAllometricCorrection.h"
#include "steps/param/sf_paramAllSteps.h"

class SF_QSMAllometricCheckAdapter
{
public:
  SF_QSMAllometricCheckAdapter();
  SF_QSMAllometricCheckAdapter(const SF_QSMAllometricCheckAdapter& obj);
  ~SF_QSMAllometricCheckAdapter() = default;

  void operator()(SF_ParamAllometricCorrectionNeighboring& params);

private:
  std::shared_ptr<QMutex> mMutex;
};

#endif // SF_STEPQSMALLOMETRICCHECKADAPTER_H
