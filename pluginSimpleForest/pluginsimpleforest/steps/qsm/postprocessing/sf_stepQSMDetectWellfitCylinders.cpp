/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepQSMDetectWellFitCylinders.h"

#include "steps/item/sf_spherefollowingParametersItem.h"
#include "steps/qsm/postprocessing/SF_stepQSMDetectWellFitCylindersAdapter.h"

#include <QtConcurrent/QtConcurrent>

#include <ct_itemdrawable/ct_cylinder.h>

SF_StepQSMDetectWellFitCylinders::SF_StepQSMDetectWellFitCylinders(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit)
{
  m_onlyGoodFit = true;
}

SF_StepQSMDetectWellFitCylinders::~SF_StepQSMDetectWellFitCylinders() {}

QString
SF_StepQSMDetectWellFitCylinders::getStepDescription() const
{
  return tr("QSM Detect Wellfit Cylinders");
}

QString
SF_StepQSMDetectWellFitCylinders::getStepDetailledDescription() const
{
  return tr("The steps detects well fitted cylinders. It imports a qsm and its point cloud. Points are allocated to cylinders, for "
            "each cylinder a sub cloud is generated."
            "The average distance of each subcloud to its cylinder is computed. Only the cylinders with small enough distance are "
            "considered well fit. Additionally "
            " cylinders which are close to the tips are by default considered bad fitted. Each cylinder receives a flag about the "
            "good or bad fitted class, which can be"
            " used during allometric correction.");
}

QString
SF_StepQSMDetectWellFitCylinders::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepQSMDetectWellFitCylinders::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepQSMDetectWellFitCylinders(dataInit);
}

QStringList
SF_StepQSMDetectWellFitCylinders::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationSphereFollowing());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepQSMDetectWellFitCylinders::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();

  configDialog->addText("<b>Cylinder quality estimation</b>:");
  configDialog->addText(
    "The reverse branch order for a segment is the number of branch junctions passed from this segment to its furthest leave.", "");
  configDialog->addText("Tip segments have reverse branch order 1, the maximum reverse branch order is found at the root.", "");
  configDialog->addInt(
    "Cylinders from segments having an reverse branch order smaller than [<em><b>minimumReverseBranchOrder</b></em>] ",
    " are considered noise. ",
    1,
    99,
    m_minReverseBranchOrder);
  configDialog->addEmpty();
  configDialog->addText("For the remaining cylinders points are allocated and their average distance to the cylinder is computed.",
                        "");
  configDialog->addDouble(" Only the best  "
                          " [<em><b>percentageFitQuality</b></em>] ",
                          " cylinders will not be considered noise. ",
                          0.01,
                          1.00,
                          2,
                          m_percentageFitQuality);
  configDialog->addText(
    "Computree does scale the color code of an attached attribute of a cylinder by the min and max of that attribute.", "");
  configDialog->addText(
    "Min and max change between different models and to the next flag can let you unify the colorpallete between different QSMs.", "");
  configDialog->addBool(
    "Check for [<em><b>unifiedVisualization</b></em>], uncheck if you want to analyse the error statistically with exported QSMs. ",
    "",
    "",
    m_normalizeErrorVisualization,
    "");
  configDialog->addEmpty();
  createVisualizeCylinders(configDialog);
  configDialog->addEmpty();
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepQSMDetectWellFitCylinders::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Result"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("QSM item"));

  CT_InResultModelGroup* resModel2 = createNewInResultModel(DEF_IN_RESULT2, tr("Input Result Cloud"));
  resModel2->setZeroOrMoreRootGroup();
  resModel2->addGroupModel("",
                           DEF_IN_GRP_CLUSTER2,
                           CT_AbstractItemGroup::staticGetType(),
                           tr("Tree Group"),
                           "",
                           CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel2->addItemModel(DEF_IN_GRP_CLUSTER2, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Tree Cloud"));
}

void
SF_StepQSMDetectWellFitCylinders::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);

  if (resModelw != NULL) {
    QString name = tr("QSM cylinders with fit quality");
    QString sfCylinders = name;
    sfCylinders.append(tr("SF QSM plugin internal"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepQSMDetectWellFitCylinders::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  createParamList(outResult);
  writeLogger();
  QFuture<void> future = QtConcurrent::map(_paramList, SF_StepQSMDetectWellfitCylindersAdapter());
  while (!future.isFinished()) {
    setProgressByCounter(10.0f, 85.0f);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamQSMDetectWellfitCylinders<SF_Point>>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}

void
SF_StepQSMDetectWellFitCylinders::createParamList(CT_ResultGroup* outResult)
{
  CT_ResultGroupIterator outResItCloud(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResItCloud.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud.next();
    const SF_QSMItem* QSM_Item = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    SF_ParamQSMDetectWellfitCylinders<SF_Point> params;
    auto qsm = QSM_Item->getQsm()->clone();
    params._qsm = qsm;
    params.m_percentageFitQuality = static_cast<float>(m_percentageFitQuality);
    params.m_minReverseBranchOrder = m_minReverseBranchOrder;
    params.m_normalizeErrorVisualization = m_normalizeErrorVisualization;
    _paramList.append(std::move(params));
  }
  CT_ResultGroup* outResult2 = getInputResults().at(1);
  CT_ResultGroupIterator outResIt2(outResult2, this, DEF_IN_GRP_CLUSTER2);
  size_t index = 0;
  while (!isStopped() && outResIt2.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt2.next();
    if (index >= static_cast<size_t>(_paramList.size())) {
      QString errorMsg = "SF_StepQSMRefitCylinders More trees than clouds - Please reconfigure this step";
      PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
      return;
    }
    index = std::min(index, static_cast<size_t>(_paramList.size()));
    SF_ParamQSMDetectWellfitCylinders<SF_Point>& param = _paramList[index];
    index++;
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);
    param._itemCpyCloudIn = ctCloud;
  }
  m_computationsTotal = _paramList.size();
}
