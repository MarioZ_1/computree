/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepQSMAllometricCorrectionManual.h"

#include "steps/item/sf_spherefollowingParametersItem.h"
#include "steps/qsm/postprocessing/sf_stepQSMAllometricCorrectionManualAdapter.h"

#include <QtConcurrent/QtConcurrent>

#include <ct_itemdrawable/ct_cylinder.h>

SF_StepQSMAllometricCorrectionManual::SF_StepQSMAllometricCorrectionManual(CT_StepInitializeData& dataInit)
  : SF_AbstractStepQSM(dataInit)
{
  m_growthParameters.push_back(m_growthVolume);
  m_growthParameters.push_back(m_growthLength);
}

SF_StepQSMAllometricCorrectionManual::~SF_StepQSMAllometricCorrectionManual() {}

QString
SF_StepQSMAllometricCorrectionManual::getStepDescription() const
{
  return tr("QSM Allometric Correction Manual");
}

QString
SF_StepQSMAllometricCorrectionManual::getStepDetailledDescription() const
{
  return tr(
    "In Xu et al 2007 an early QSM radius predicting method using based on metabolic scaling theory is presented."
    "There the radius is predicted from the length to the tip. During the devlopment of Simpletree much more powerful parameters"
    " than traditional volume or length were invented. See Hackenberg 2015 for the recursive definition of the volume named "
    "growthvolume."
    "The two parameter equation (radius = a*growthlength^b) or (radius = a*growthVolume^b)."
    " showed more accuracy with the invented recursive defined parameters applied as independent variable in the power model."
    "Here we will apply the forumla of 2007 with the new parameter growthlength for QSM radius correction. The power parameters"
    " a and b need to be computed externally in this step and are user intput.");
}

QString
SF_StepQSMAllometricCorrectionManual::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepQSMAllometricCorrectionManual::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepQSMAllometricCorrectionManual(dataInit);
}

QStringList
SF_StepQSMAllometricCorrectionManual::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationSphereFollowing());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepQSMAllometricCorrectionManual::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>Allometric correction</b>:");

  configDialog->addStringChoice("[<em><b>growth Parameter</b></em>] ", " ", m_growthParameters, m_growthParameterSelected);

  configDialog->addDouble(
    "The lower the [<em><b>correction range</b></em>] , a percentage number between 0 and 1, the more strict is the check",
    " (m). ",
    0.001,
    0.999,
    3,
    _range);

  configDialog->addDouble("Each cylinder has a "
                          " [<em><b>min Radius</b></em>] ",
                          " (m). ",
                          0.0001,
                          0.01,
                          5,
                          _minRadius);

  configDialog->addDouble("Use as power parameter [<em><b>a</b></em>]", " . ", 0.00000001, 10, 8, m_a);

  configDialog->addDouble("Use as power parameter [<em><b>b</b></em>]", " . ", 0.1, 0.8, 8, m_b);
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepQSMAllometricCorrectionManual::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT,
                                                                        tr("Input result Step Allometric Correction Manual"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("internal QSM"));
}

void
SF_StepQSMAllometricCorrectionManual::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString name = tr("Manual allometric corrected ");
    QString sfCylinders = name;
    sfCylinders.append(tr("QSM internal"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepQSMAllometricCorrectionManual::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  createParamList(outResult);
  writeLogger();
  QFuture<void> future = QtConcurrent::map(_paramList, SF_QSMAllometricCorrectionManualAdapter());
  while (!future.isFinished()) {
    setProgressByCounter(10.0f, 85.0f);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamAllometricCorrectionNeighboring>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}

void
SF_StepQSMAllometricCorrectionManual::createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText(
    QObject::tr(
      "For For the introduction of using defined parameters in such correction and autoestimating the power parameter cite:"),
    "Hackenberg, J.; Spiecker, H.; Calders, K.; Disney, M.; Raumonen, P.");
  configDialog->addText(" (2.2. Tree Modeling - Allometric improvement)",
                        "<em>SimpleTree - An Efficient Open Source Tool to "
                        "Build Tree Models from TLS Clouds.</em>");
  configDialog->addText("", "Forests <b>2015</b>, 6, 4245-4294.");

  configDialog->addEmpty();
  configDialog->addText(
    QObject::tr("For this step please you can cite in addition an early method using allometric prediction with length and radius:"),
    "Xu, H.; Gossett, N.; Chen B.");
  configDialog->addText("(section 5.1 Tree Allometry)", "<em>Knowledge and heuristic-based modeling of laser-scanned trees.</em>");
  configDialog->addText("", "ACM Transactions on Graphics <b>2007</b>, 19-es.");
}

void
SF_StepQSMAllometricCorrectionManual::createParamList(CT_ResultGroup* outResult)
{
  CT_ResultGroupIterator outResItCloud(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResItCloud.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud.next();
    const SF_QSMItem* QSM_Item = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    SF_ParamAllometricCorrectionNeighboring params;
    auto qsm = QSM_Item->getQsm()->clone();

    if (m_growthParameterSelected == m_growthLength) {
      params.m_useGrowthLength = true;
    } else {
      params.m_useGrowthLength = false;
    }
    if (params.m_useGrowthLength) {
      qsm->setA(m_a);
      qsm->setB(m_b);
    } else {
      qsm->setA(m_a);
      qsm->setB(m_b);
    }
    params._qsm = qsm;
    params._range = _range;
    params._minRadius = _minRadius;
    _paramList.append(std::move(params));
  }
  m_computationsTotal = _paramList.size();
}
