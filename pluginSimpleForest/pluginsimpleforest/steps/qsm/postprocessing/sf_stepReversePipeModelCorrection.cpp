/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepReversePipeModelCorrection.h"

#include "steps/item/sf_spherefollowingParametersItem.h"
#include "steps/qsm/postprocessing/sf_stepReversePipeModelCorrectionAdapter.h"

#include <QtConcurrent/QtConcurrent>

#include <ct_itemdrawable/ct_cylinder.h>

SF_StepReversePipeModelCorrection::SF_StepReversePipeModelCorrection(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepReversePipeModelCorrection::~SF_StepReversePipeModelCorrection() {}

QString
SF_StepReversePipeModelCorrection::getStepDescription() const
{
  return tr("QSM RBOPR Filter");
}

QString
SF_StepReversePipeModelCorrection::getStepDetailledDescription() const
{
  return tr("The RBOPA is defined as the number of supporting twigs. As this is a proxy for the cross-sectional area of the sapwood "
            "and furthermore the sapwood area is a proxy for the total cross sectional we conclude about its squareroot, the RBOPR: "
            "The RPOBR is a proxy for the radius of the cylinder. We build a simple linear model of the first order to have a radius "
            "predictor which we can use inside a filter.");
}

QString
SF_StepReversePipeModelCorrection::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepReversePipeModelCorrection::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepReversePipeModelCorrection(dataInit);
}

QStringList
SF_StepReversePipeModelCorrection::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationSphereFollowing());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepReversePipeModelCorrection::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>RBOPR filter</b>:");
  configDialog->addText(
    "We build a simple filter which predicts the <b>radius</b> from the <b>RPOBR</b>:  <b>radius</b> = <b>RPOBR</b> x <b>a</b>.");
  configDialog->addText("First we fit an estamator for <b>a</b> and this we can use to predict a radius.");
  configDialog->addText("We can adjust the allowed variation between modeled radius and predicted radius.");
  configDialog->addText("If the deviation is too large we overwrite with the predicted radius:");
  configDialog->addDouble(
    "The lower the [<em><b>correction range</b></em>] , a percentage number between 0 and 1, the more strict is the check",
    " (m). ",
    0.01,
    1,
    2,
    _range);
  configDialog->addDouble(
    "Use as inlier distance for the radius [<em><b>inlierDistance</b></em>] ", " (m). ", 0.01, 0.1, 3, m_inlierDistance);
  configDialog->addBool("Split stem from other cylinders and fit individually", "", "", m_doSplit);
  configDialog->addDouble("The twigs have a [<em><b>min Radius</b></em>] of ", " (m). ", 0.001, 0.1, 4, _minRadius);
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepReversePipeModelCorrection::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT,
                                                                        tr("Input result Step Reverse Pipe Model Filter"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("internal QSM"));
}

void
SF_StepReversePipeModelCorrection::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString name = tr("QSM RBOPR filter ");
    QString sfCylinders = name;
    sfCylinders.append(tr("internal"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepReversePipeModelCorrection::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  createParamList(outResult);
  writeLogger();
  QFuture<void> future = QtConcurrent::map(_paramList, SF_QSMReversePipeModelCorrectionAdapter());
  while (!future.isFinished()) {
    setProgressByCounter(10.0f, 85.0f);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamReversePipeModelCorrection>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}

void
SF_StepReversePipeModelCorrection::createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText(
    QObject::tr(
      "For the importance of pipemodel theory already an early example (predict a pipemodel parameter, but not use it) was given in:"),
    "Hackenberg, J.; Spiecker, H.; Calders, K.; Disney, M.; Raumonen, P.");
  configDialog->addText(" Figure 20. Improvement possibilities of the pipe model theory [43] with a Prunus avium model",
                        "<em>SimpleTree - An Efficient Open Source Tool to "
                        "Build Tree Models from TLS Clouds.</em>");
  configDialog->addText("", "Forests <b>2015</b>, 6, 4245-4294.");
  configDialog->addText(
    " apart from this the here used parameter was already introduced as output parameter in SimpleTree 4.x versions", "");
}

void
SF_StepReversePipeModelCorrection::createParamList(CT_ResultGroup* outResult)
{
  CT_ResultGroupIterator outResItCloud(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResItCloud.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud.next();
    const SF_QSMItem* QSM_Item = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    SF_ParamReversePipeModelCorrection params;
    auto qsm = QSM_Item->getQsm()->clone();
    params._qsm = qsm;
    params.doSplit = m_doSplit;
    params.m_minPts = m_minPts;
    params.m_minRadius = _minRadius;
    params.m_range = _range;
    params.m_inlierDistance = m_inlierDistance;
    params.m_ransacIterations = m_ransacIterations;
    _paramList.append(std::move(params));
  }
  m_computationsTotal = _paramList.size();
}
