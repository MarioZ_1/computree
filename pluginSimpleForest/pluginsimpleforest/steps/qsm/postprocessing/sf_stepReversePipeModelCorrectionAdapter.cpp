/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "steps/qsm/postprocessing/sf_stepReversePipeModelCorrectionAdapter.h"

SF_QSMReversePipeModelCorrectionAdapter::SF_QSMReversePipeModelCorrectionAdapter(const SF_QSMReversePipeModelCorrectionAdapter& obj)
{
  mMutex = obj.mMutex;
}

SF_QSMReversePipeModelCorrectionAdapter::SF_QSMReversePipeModelCorrectionAdapter()
{
  mMutex.reset(new QMutex);
}

SF_QSMReversePipeModelCorrectionAdapter::~SF_QSMReversePipeModelCorrectionAdapter() {}

void
SF_QSMReversePipeModelCorrectionAdapter::operator()(SF_ParamReversePipeModelCorrection& params)
{
  SF_ParamReversePipeModelCorrection paramsCpy;
  {
    QMutexLocker m1(&*mMutex);
    paramsCpy = params;
  }
  {
    QMutexLocker m1(&*mMutex);
    {
      std::unordered_map<std::uint32_t, std::vector<float>> histogramMap;
      SF_FitLineWithIntercept<float> lineFit;
      auto cylinders = paramsCpy._qsm->getBuildingBricks();
      auto filter = [&](const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& cylinders) {
        for (auto& cylinder : cylinders) {
          if (cylinder->getGoodQuality()) {
            const auto x = static_cast<std::uint32_t>(std::max(1., std::ceil(cylinder->getSegment()->getRBOPR())) - 1.);
            histogramMap[x].push_back(cylinder->getRadius());
          }
        }
        std::vector<float> x;
        std::vector<float> y;
        for (auto& pair : histogramMap) {
          x.push_back(static_cast<float>(pair.first) - 0.5f);
          y.push_back(SF_Math<float>::getMedian(pair.second));
        }
        lineFit.setX(x);
        lineFit.setY(y);
        lineFit.setInlierDistance(paramsCpy.m_inlierDistance);
        lineFit.setIterations(100);
        lineFit.setMinPts(paramsCpy.m_minPts);
        lineFit.setUseWeight(true);
        lineFit.setIntercept(paramsCpy.m_minRadius);
        lineFit.compute();
        auto equation = lineFit.equation();
        for (auto& cylinder : cylinders) {
          if (cylinder->getSegment()->getID() <= 1) {
            continue;
          }
          const auto radius = cylinder->getRadius();
          auto x = static_cast<std::uint32_t>(std::max(1., std::ceil(cylinder->getSegment()->getRBOPR())) - 1.);
          const auto radiusModel = equation.first * x + equation.second;
          const auto deviation = radiusModel * paramsCpy.m_range;
          const auto min = radiusModel - deviation;
          const auto max = cylinder->getSegment()->isStem() ? radiusModel + deviation : radiusModel;
          if (min > radius || max < radius) {
            cylinder->setRadius(radiusModel, FittingType::REVERSEPIPEMODEL);
          }
        }
      };
      if (paramsCpy.doSplit) {
        auto split = splitIntoStemAndNoStem(cylinders);
        filter(split.first);
        filter(split.second);
      } else {
        filter(cylinders);
      }
    }
  }
  {
    QMutexLocker m1(&*mMutex);
    paramsCpy._qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);
    params = paramsCpy;
  }
}
