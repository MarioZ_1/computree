/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPEXPORTQSMLIST_H
#define SF_STEPEXPORTQSMLIST_H

#include "steps/qsm/sf_abstractStepQSM.h"

#include <converters/CT_To_PCL/sf_converterCTIDToPCLCloud.h>
#include <converters/CT_To_PCL/sf_converterCTToPCL.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>

class SF_StepExportQSMList : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepExportQSMList(CT_StepInitializeData& dataInit);
  ~SF_StepExportQSMList();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  virtual void createPreConfigurationDialog() {}
  virtual void createPostConfigurationDialog();
  void compute();

private:
  int toStringSFMethod();
  SF_CLoudToModelDistanceMethod toStringCMDMethod();

  bool m_writePlyGrowthVolume = true;
  bool m_smoothQSM = false;
  bool m_writePlyGrowthLengthPly = false;
  bool m_writePlyStem = false;
  bool m_writeCloudRadius = false;
  bool m_writeCloudGrowthLength = true;
  bool m_writeCloudFitQuality = false;
  bool m_writeCloud = true;
  bool m_downScaleCloud = true;
  bool m_doTranslation = true;
  double m_voxelSize = 0.04;
  QStringList m_filePath;
};

#endif // SF_STEPEXPORTQSMLIST_H
