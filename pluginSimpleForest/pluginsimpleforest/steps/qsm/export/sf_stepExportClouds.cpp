/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepExportClouds.h"

#include "file/export/cloud/sf_exportCloud.h"
#include "file/export/ply/sf_exportPly.h"
#include "file/export/qsm/sf_exportQSM.h"
#include "steps/item/sf_spherefollowingParametersItem.h"
#include "steps/qsm/postprocessing//sf_stepQSMAllometricCorrectionAdapter.h"

#include <ct_itemdrawable/ct_cylinder.h>
#include <ct_itemdrawable/ct_fileheader.h>

SF_StepExportClouds::SF_StepExportClouds(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepExportClouds::~SF_StepExportClouds() {}

QString
SF_StepExportClouds::getStepDescription() const
{
  return tr("Cloud list exporter");
}

QString
SF_StepExportClouds::getStepDetailledDescription() const
{
  return tr("For each cloud an Ascii readbale format with x,y,z will be generated.");
}

QString
SF_StepExportClouds::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepExportClouds::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepExportClouds(dataInit);
}

QStringList
SF_StepExportClouds::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationSphereFollowing());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepExportClouds::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>Cloud List exporter</b>:");
  configDialog->addFileChoice(tr("Select an export folder"), CT_FileChoiceButton::OneExistingFolder, "", m_filePath);
}

void
SF_StepExportClouds::createInResultModelListProtected()
{
  CT_InResultModelGroup* resModelName = createNewInResultModelForCopy(DEF_IN_RESULT2, tr("Input for name"), "", true);
  resModelName->setZeroOrMoreRootGroup();
  resModelName->addGroupModel("",
                              DEF_IN_GRP_CLUSTER3,
                              CT_AbstractItemGroup::staticGetType(),
                              tr("Name Group"),
                              "",
                              CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModelName->addItemModel(DEF_IN_GRP_CLUSTER3, DEF_IN_NAME, CT_FileHeader::staticGetType(), tr("Name"));

  CT_InResultModelGroup* resModelName2 = createNewInResultModel(DEF_IN_RESULT3, tr("Input for Cloud"), "", true);
  resModelName2->setZeroOrMoreRootGroup();
  resModelName2->addGroupModel("",
                               DEF_IN_GRP_CLUSTER2,
                               CT_AbstractItemGroup::staticGetType(),
                               tr("Cloud Group"),
                               "",
                               CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModelName2->addItemModel(DEF_IN_GRP_CLUSTER2, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Cloud"));
}

void
SF_StepExportClouds::createOutResultModelListProtected()
{
}

void
SF_StepExportClouds::compute()
{
  if (m_filePath.empty() || m_filePath.first() == "") {
    QString errorMsg = "SF_StepExportClouds please give a path to export the result. Cloud export skipped.";
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
    return;
  }
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResultName = getInputResults().at(0);
  CT_ResultGroupIterator outResItName(outResultName, this, DEF_IN_GRP_CLUSTER3);
  std::vector<QString> names;
  while (!isStopped() && outResItName.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItName.next();
    CT_FileHeader* header = (CT_FileHeader*)group->firstItemByINModelName(this, DEF_IN_NAME);
    QString str = header->getFileName();
    QString cropedFilename = str.split(".", QString::SkipEmptyParts).at(0);
    //    cropedFilename.append("_");
    names.push_back(cropedFilename);
  }

  std::vector<const CT_AbstractItemDrawableWithPointCloud*> cloudCTs;
  CT_ResultGroup* outResultCloud = getInputResults().at(1);
  CT_ResultGroupIterator outResItCloud2(outResultCloud, this, DEF_IN_GRP_CLUSTER2);
  while (!isStopped() && outResItCloud2.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud2.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);
    cloudCTs.push_back(ctCloud);
  }
  CT_Point firstPoint;
  bool doOnce = true;
  for (size_t i = 0; i < cloudCTs.size(); i++) {
    auto cloudCT = cloudCTs[i];
    QString name;
    if (names.size() == 1) {
      name = names.at(0);
      name.append("_");
      std::string indexString = std::to_string(i);
      while (indexString.length() < 6) {
        indexString = "0" + indexString;
      }
      name.append(QString::fromStdString(indexString));
    } else if (names.size() == cloudCTs.size()) {
      name = names.at(i);
    } else {
      QString errorMsg = "SF_StepExportQSMList please reconfigure input results.";
      PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
      return;
    }
    QString path = m_filePath.first();
    std::ofstream myfile;
    auto full = path.toStdString() + "/" + name.toStdString() + ".csv";
    std::cout << full << std::endl;
    myfile.open(full);

    const CT_AbstractPointCloudIndex* indices = cloudCT->getPointCloudIndex();
    CT_PointIterator it(indices);
    while (it.hasNext()) {
      const CT_Point& internalPoint = it.next().currentPoint();
      if (doOnce) {
        firstPoint = internalPoint;
        doOnce = false;
      }
      myfile << internalPoint[0] - firstPoint[0] << " ," << internalPoint[1] - firstPoint[1] << " ,"
             << internalPoint[2] - firstPoint[2] << "\n";
    }
    myfile.close();
  }
  writeLogger();
}
