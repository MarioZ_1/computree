/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPDIJKSTRA_H
#define SF_STEPDIJKSTRA_H

#include "steps/qsm/sf_abstractStepQSM.h"

class SF_StepDijkstra : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepDijkstra(CT_StepInitializeData& dataInit);
  ~SF_StepDijkstra();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void adaptParametersToExpertLevel();
  void createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog);
  void createParamList(CT_ResultGroup* out_result);
  virtual void createPreConfigurationDialog();
  virtual void createPostConfigurationDialog();

  void compute();
  QList<SF_ParamDijkstra<SF_PointNormal>> _paramList;

  QList<SF_ParamQSM<SF_PointNormal>> paramList();
  CT_AutoRenameModels m_outCloudItem;
  CT_AutoRenameModels _outCylinderGroup;
  CT_AutoRenameModels _outCylinders;
  CT_AutoRenameModels _outSFQSM;
  CT_AutoRenameModels _outParams;
  int toStringSFMethod();
  SF_CLoudToModelDistanceMethod toStringCMDMethod();
  double _PP_voxelSize = 0.02;
  double _PP_euclideanClusteringDistance = 0.1;
  double _SF_OPT_edgeConnectionDistance = 0.04;
  double _SF_OPT_clusteringDistance = 0.1;
  double _SF_OPT_binningDistance = 0.1;
  int _SF_RANSACIiterations = 100;
  double _SF_inlierDistance = 0.03;
  int _SF_minPtsGeometry = 5;
  int _CMD_fittingMethod = toStringCMDMethod();
  int _CMD_k = 9;
  int _CMD_numClstrs = 1;
  double _CMD_cropDistance = 0.15;
  double _CMD_inlierDistance = 0.05;
  // todo add m_clusterDownScale
  QString _PARAMETERS_1 = "100%";
  QString _PARAMETERS_3 = "75%; 100%; 150%";
  QString _PARAMETERS_5 = "75%; 100%; 150%; 200%; 250%";
  QString _PARAMETERS_7 = "75%; 90%; 100%; 140%; 190%; 250%; 300%";
  QString _PARAMETERS_9 = "75%; 90%; 100%; 140%; 180%; 240%; 300%; 370; 450";
  QString _PARAMETERS_11 = "75%; 90%; 100%; 140%; 190%; 240%; 300%; 400%; 500%; 600%; 700%";
  QString _PARAMETERS_CHOICE_EDGE_CONNECTION_DISTANCE = _PARAMETERS_5;
  QString _PARAMETERS_CHOICE_CLUSTERING_DISTANCE = _PARAMETERS_5;
  QString _PARAMETERS_CHOICE_BINNING_DISTANCE = _PARAMETERS_5;
  QStringList _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE;
  QStringList _PARAMETERS_LIST_CLUSTERING_DISTANCE;
  QStringList _PARAMETERS_LIST_BINNING_DISTANCE;

  std::vector<double> paramsStringToNumber(const QString& UISelection);

  void configDialogAddDijkstraHyperParameters(CT_StepConfigurableDialog* configDialog);
  void configDialogGuruAddPreProcessing(CT_StepConfigurableDialog* configDialog);
  void configDialogGuruAddGridSearchCloudToModelDistance(CT_StepConfigurableDialog* configDialog);
};

#endif // SF_STEPDIJKSTRA_H
