/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepDijkstra.h"

#include "sf_stepDijkstraAdapter.h"
#include "steps/item/sf_spherefollowingParametersItem.h"

#include <QtConcurrent/QtConcurrent>

#include <ct_itemdrawable/ct_cylinder.h>

SF_StepDijkstra::SF_StepDijkstra(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit)
{
  _pointDensities.append(_lowDensity);
  _pointDensities.append(_mediumDensity);
  _pointDensities.append(_highDensity);

  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_1);
  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_3);
  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_5);
  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_7);
  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_9);
  _PARAMETERS_LIST_EDGE_CONNECTION_DISTANCE.push_back(_PARAMETERS_11);

  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_1);
  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_3);
  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_5);
  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_7);
  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_9);
  _PARAMETERS_LIST_CLUSTERING_DISTANCE.push_back(_PARAMETERS_11);

  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_1);
  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_3);
  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_5);
  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_7);
  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_9);
  _PARAMETERS_LIST_BINNING_DISTANCE.push_back(_PARAMETERS_11);
}

SF_StepDijkstra::~SF_StepDijkstra() {}

QString
SF_StepDijkstra::getStepDescription() const
{
  return tr("Dijkstra Modeling");
}

QString
SF_StepDijkstra::getStepDetailledDescription() const
{
  return tr("Dijkstra Modeling - This implementation of the Dijkstra method utilizes an "
            "unsegmented tree cloud. On the modling parameters a downhill simplex search. "
            "is performed.");
}

QString
SF_StepDijkstra::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepDijkstra::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepDijkstra(dataInit);
}

QStringList
SF_StepDijkstra::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationDijkstra());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepDijkstra::configDialogAddDijkstraHyperParameters(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText("<b>Dijkstra Method Hyper Parameters</b>:");
  configDialog->addText("Hyper Parameters are set. Those parameters will "
                        "never change during optimization.");
  configDialog->addStringChoice("[<em><b>SAC Model Consensus method</b></em>] ", " ", _SF_methodList, _SF_methodChoice);
  configDialog->addDouble("The [<em><b>inlier distance</b></em>] for the"
                          " selected <em>SAC Model Consensus method</em> is ",
                          " (m). ",
                          0.005,
                          0.05,
                          3,
                          _SF_inlierDistance);
  configDialog->addInt("For fitting a geometry with the selected <em>SAC Model Consensus "
                       "method</em> at minimum [<em><b>minPts</b></em>]",
                       " points are needed.",
                       3,
                       100,
                       _SF_minPtsGeometry);
  configDialog->addInt("For the selected <em>SAC Model Consensus method</em> "
                       "[<em><b>iterations</b></em>]",
                       " are performed.",
                       3,
                       100,
                       _SF_RANSACIiterations);
  configDialog->addEmpty();
}

void
SF_StepDijkstra::configDialogGuruAddPreProcessing(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText("<b>Pre Processing</b>:");
  configDialog->addDouble("To even out the distribution and speed things up the cloud is "
                          "downscaled first to [<em><b>voxel size</b></em>] ",
                          " (m). ",
                          0.005,
                          0.1,
                          3,
                          _PP_voxelSize);
  configDialog->addDouble("Only the largest cluster will be processed with "
                          "[<em><b>clustering range</b></em>]  ",
                          " (m). ",
                          0.03,
                          0.9,
                          2,
                          _PP_euclideanClusteringDistance);
  configDialog->addEmpty();
}

void
SF_StepDijkstra::configDialogGuruAddGridSearchCloudToModelDistance(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText("<b>Cloud To Model Distance</b>:");
  configDialog->addText("For the downhill simplex search evaluation the parameter set with "
                        "the smallest qsm to cloud distance is chosen.");
  configDialog->addStringChoice("For the cloud to model distance we choose "
                                "[<em><b>distance method</b></em>] ",
                                " .",
                                _CMD_methodList,
                                _CMD_methodChoice);
  configDialog->addInt("Test for each point its nearest [<em><b>k</b></em>] "
                       "cylinders to get best nearest neighbor",
                       " .",
                       3,
                       9,
                       _CMD_k);
  configDialog->addDouble("For MSAC and inlier methods the distance is cropped "
                          "at [<em><b>crop distance</b></em>] ",
                          " (m).",
                          0.05,
                          0.5,
                          2,
                          _CMD_cropDistance);
  configDialog->addDouble("The [<em><b>inlier distance</b></em>]", " (m).", 0.01, 0.1, 2, _CMD_inlierDistance);
  configDialog->addEmpty();
}

void
SF_StepDijkstra::createPreConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPreConfigurationDialog();
  configDialog->addBool("Uncheck to deactivate parameterization possibilities "
                        "of this step. Only recommended for beginners",
                        "",
                        "expert",
                        _isExpert);
  configDialog->addStringChoice("In beginner mode select point cloud quality", "", _pointDensities, _choicePointDensity);

  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepDijkstra::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialogGuruAddPreProcessing(configDialog);
  configDialogAddDijkstraHyperParameters(configDialog);
  configDialogGuruAddGridSearchCloudToModelDistance(configDialog);
}

void
SF_StepDijkstra::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString name = tr("Dijkstra ");
    QString sfCylinders = name;
    sfCylinders.append(tr(" QSM"));
    QString sfParameters = name;
    sfParameters.append(tr(" parameters"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepDijkstra::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Input Result QSM Dijkstra"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Cloud Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Input Cloud QSM Dijkstra"));
}

void
SF_StepDijkstra::adaptParametersToExpertLevel()
{
  if (!_isExpert) {
    _SF_inlierDistance = 0.03;
    _SF_minPtsGeometry = 3;
    _PARAMETERS_CHOICE_EDGE_CONNECTION_DISTANCE = _PARAMETERS_7;
    _PARAMETERS_CHOICE_CLUSTERING_DISTANCE = _PARAMETERS_7;
    _PARAMETERS_CHOICE_BINNING_DISTANCE = _PARAMETERS_7;
    _PP_voxelSize = 0.01;
    if (_choicePointDensity == _lowDensity) {
      _PP_voxelSize = 0.03;
      _SF_OPT_edgeConnectionDistance = 0.09;
      _SF_OPT_clusteringDistance = 0.2;
      _SF_OPT_binningDistance = 0.2;
    } else if (_choicePointDensity == _mediumDensity) {
      _PP_voxelSize = 0.02;
      _SF_OPT_edgeConnectionDistance = 0.05;
      _SF_OPT_clusteringDistance = 0.12;
      _SF_OPT_binningDistance = 0.12;
    } else {
      _PP_voxelSize = 0.01;
      _SF_OPT_edgeConnectionDistance = 0.025;
      _SF_OPT_clusteringDistance = 0.05;
      _SF_OPT_binningDistance = 0.05;
    }
  }
}

void
SF_StepDijkstra::createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addEmpty();
  configDialog->addText(
    QObject::tr("For this step please cite in addition the early method using Dijkstra for predicting tree skeleton:"),
    "Xu, H.; Gossett, N.; Chen B.");
  configDialog->addText("", "<em>Knowledge and heuristic-based modeling of laser-scanned trees.</em>");
  configDialog->addText("", "ACM Transactions on Graphics <b>2007</b>, 19-es.");
  configDialog->addEmpty();
  configDialog->addText(
    QObject::tr("And this presenting the automatic parameter search by using cloud to model distance for QSM modeling:"),
    "Hackenberg, J.; Spiecker, H.; Calders, K.; Disney, M.; Raumonen, P.");
  configDialog->addText("(section 2.2. Tree Modeling - Parameter Optimization)",
                        "<em>SimpleTree - An Efficient Open Source Tool to "
                        "Build Tree Models from TLS Clouds.</em>");
  configDialog->addText("", "Forests <b>2015</b>, 6, 4245-4294.");
  configDialog->addEmpty();
}

void
SF_StepDijkstra::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  identifyAndRemoveCorruptedScenes(outResult);
  createParamList(outResult);
  writeLogger();
  QFuture<void> future = QtConcurrent::map(_paramList, SF_DijkstraAdapter());
  while (!future.isFinished()) {
    setProgressByCounter(10.0f, 85.0f);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamDijkstra<SF_PointNormal>>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}

QList<SF_ParamQSM<SF_PointNormal>>
SF_StepDijkstra::paramList()
{
  QList<SF_ParamQSM<SF_PointNormal>> paramList;
  std::for_each(_paramList.begin(), _paramList.end(), [&paramList](SF_ParamDijkstra<SF_PointNormal>& params) {
    SF_ParamQSM<SF_PointNormal> param;
    param._qsm = params._qsm;
    param._translation = params._translation;
    paramList.push_back(param);
  });
  return paramList;
}

int
SF_StepDijkstra::toStringSFMethod()
{
  int type = -1;
  if (_SF_methodChoice == _RANSAC)
    type = pcl::SAC_RANSAC;
  if (_SF_methodChoice == _LMEDS)
    type = pcl::SAC_LMEDS;
  if (_SF_methodChoice == _MSAC)
    type = pcl::SAC_MSAC;
  if (_SF_methodChoice == _RRANSAC)
    type = pcl::SAC_RRANSAC;
  if (_SF_methodChoice == _RMSAC)
    type = pcl::SAC_RMSAC;
  if (_SF_methodChoice == _MLESAC)
    type = pcl::SAC_MLESAC;
  if (_SF_methodChoice == _PROSAC)
    type = pcl::SAC_PROSAC;
  return type;
}

SF_CLoudToModelDistanceMethod
SF_StepDijkstra::toStringCMDMethod()
{
  if (_CMD_methodChoice == _ZEROMOMENTUMORDER)
    return SF_CLoudToModelDistanceMethod::ZEROMOMENTUMORDER;
  if (_CMD_methodChoice == _FIRSTMOMENTUMORDERMSAC)
    return SF_CLoudToModelDistanceMethod::FIRSTMOMENTUMORDERMSAC;
  if (_CMD_methodChoice == _FIRSTMOMENTUMORDER)
    return SF_CLoudToModelDistanceMethod::FIRSTMOMENTUMORDER;
  if (_CMD_methodChoice == _SECONDMOMENTUMORDERMSAC)
    return SF_CLoudToModelDistanceMethod::SECONDMOMENTUMORDERMSAC;
  if (_CMD_methodChoice == _SECONDMOMENTUMORDER)
    return SF_CLoudToModelDistanceMethod::SECONDMOMENTUMORDER;
  return SF_CLoudToModelDistanceMethod::ZEROMOMENTUMORDER;
}

std::vector<double>
SF_StepDijkstra::paramsStringToNumber(const QString& UISelection)
{
  // TODO can be removed?
  std::vector<double> paramVec;
  if (UISelection == _PARAMETERS_1) {
    paramVec.push_back(1.0);
  } else if (UISelection == _PARAMETERS_3) {
    paramVec.push_back(0.75);
    paramVec.push_back(1.0);
    paramVec.push_back(1.5);
  } else if (UISelection == _PARAMETERS_5) {
    paramVec.push_back(0.75);
    paramVec.push_back(1.0);
    paramVec.push_back(1.50);
    paramVec.push_back(2.0);
    paramVec.push_back(2.5);
  } else if (UISelection == _PARAMETERS_7) {
    paramVec.push_back(0.75);
    paramVec.push_back(0.9);
    paramVec.push_back(1.0);
    paramVec.push_back(1.4);
    paramVec.push_back(1.9);
    paramVec.push_back(2.5);
    paramVec.push_back(3.0);
  } else if (UISelection == _PARAMETERS_9) {
    paramVec.push_back(0.75);
    paramVec.push_back(0.9);
    paramVec.push_back(1.0);
    paramVec.push_back(1.4);
    paramVec.push_back(1.8);
    paramVec.push_back(2.4);
    paramVec.push_back(3.0);
    paramVec.push_back(3.7);
    paramVec.push_back(4.5);
  } else if (UISelection == _PARAMETERS_11) {
    paramVec.push_back(0.75);
    paramVec.push_back(0.9);
    paramVec.push_back(1.0);
    paramVec.push_back(1.4);
    paramVec.push_back(1.9);
    paramVec.push_back(2.4);
    paramVec.push_back(3.0);
    paramVec.push_back(4.0);
    paramVec.push_back(5.0);
    paramVec.push_back(6.0);
    paramVec.push_back(7.0);
  } else {
    throw("SF_StepSpherefollowingRoot: Invalid grid parameter selected.");
  }
  return paramVec;
}

void
SF_StepDijkstra::createParamList(CT_ResultGroup* outResult)
{
  // TODO clean up here
  SF_DijsktraParameters sphereFollowingParams;
  SF_DijkstraOptimizationParameters sfOptimizationParameters;
  sfOptimizationParameters.m_binningDistance = _SF_OPT_binningDistance;
  sfOptimizationParameters.m_binningDistanceMultiplier = paramsStringToNumber(_PARAMETERS_CHOICE_BINNING_DISTANCE);
  sfOptimizationParameters.m_clusteringDistance = _SF_OPT_clusteringDistance;
  sfOptimizationParameters.m_clusteringDistanceMultiplier = paramsStringToNumber(_PARAMETERS_CHOICE_CLUSTERING_DISTANCE);
  sfOptimizationParameters.m_edgeConnectionDistance = _SF_OPT_edgeConnectionDistance;
  sfOptimizationParameters.m_edgeConnectionDistanceMultiplier = paramsStringToNumber(_PARAMETERS_CHOICE_EDGE_CONNECTION_DISTANCE);
  sphereFollowingParams.m_optimizationParams = sfOptimizationParameters;
  sphereFollowingParams._minPtsGeometry = _SF_minPtsGeometry;
  sphereFollowingParams._inlierDistance = _SF_inlierDistance;
  sphereFollowingParams._RANSACIterations = _SF_RANSACIiterations;
  sphereFollowingParams._fittingMethod = toStringSFMethod();

  SF_CloudToModelDistanceParameters distanceParams;
  distanceParams._method = toStringCMDMethod();
  distanceParams._k = _CMD_k;
  distanceParams._cropDistance = _CMD_cropDistance;
  distanceParams._inlierDistance = _CMD_inlierDistance;
  distanceParams.m_useAngle = false;

  // adaptParametersToExpertLevel();
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);
    SF_ParamDijkstra<SF_PointNormal> param;
    param._sphereFollowingParams._RANSACIterations = _SF_RANSACIiterations;
    param._sphereFollowingParams._inlierDistance = _SF_inlierDistance;
    param._sphereFollowingParams._minPtsGeometry = _SF_minPtsGeometry;

    param._stepProgress = _stepProgress;
    param._distanceParams = distanceParams;
    param.m_dijkstraParameters = sphereFollowingParams;
    param.m_clusterDownScale = _PP_voxelSize;
    param._clusteringDistance = _PP_euclideanClusteringDistance;

    param._modelCloudError = 1337;
    param._fittedGeometries = 0;
    param._log = PS_LOG;
    param._itemCpyCloudIn = ctCloud;
    param._grpCpyGrp = group;

    // TODO those two guys are important
    param.m_djikstraRange = param.m_clusterDownScale * 2.5;
    param.m_slice = param.m_clusterDownScale * 2;
    param.m_clusterSlize = param.m_clusterDownScale * 2;
    _paramList.append(param);
  }
  int numberClouds = _paramList.size();
  int numberComputationsPerCloud = 100; // TODO here are params missing
  m_computationsTotal = numberClouds * numberComputationsPerCloud;
}
