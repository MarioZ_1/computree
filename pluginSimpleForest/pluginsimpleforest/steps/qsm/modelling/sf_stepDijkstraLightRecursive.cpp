/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepDijkstraLightRecursive.h"

#include "sf_stepDijkstraLightRecursiveAdapter.h"
#include "steps/item/sf_spherefollowingParametersItem.h"

#include <QString>
#include <QtConcurrent/QtConcurrent>

#include <ct_itemdrawable/ct_cylinder.h>

SF_StepDijkstraLightRecursive::SF_StepDijkstraLightRecursive(CT_StepInitializeData& dataInit) : SF_StepSpherefollowingBasic(dataInit)
{
}

SF_StepDijkstraLightRecursive::~SF_StepDijkstraLightRecursive() {}

QString
SF_StepDijkstraLightRecursive::getStepDescription() const
{
  return tr("SphereFollowing Dijkstra Recursive");
}

QString
SF_StepDijkstraLightRecursive::getStepDetailledDescription() const
{
  return tr("SphereFollowing Dijkstra Recursive - First from an input cloud and an input qsm"
            " unfitted points are estimated. Those points are clustered. Each cluster large enough"
            " gets processed with the dijkstra algorthim routine after being downscaled."
            " The Dijkstra pathes are used as tree skeleton.");
}

QString
SF_StepDijkstraLightRecursive::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepDijkstraLightRecursive::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepDijkstraLightRecursive(dataInit);
}

QStringList
SF_StepDijkstraLightRecursive::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationSphereFollowing());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepDijkstraLightRecursive::configRecursion(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble("To even out the distribution and speed things up the cloud is "
                          "downscaled first to [<em><b>voxel size</b></em>] ",
                          " (m). ",
                          0.005,
                          0.1,
                          3,
                          m_clusterDownScale);
  configDialog->addDouble("All points of the downscaled cloud with a [<em><b>point to model distance</b></em>] larger ",
                          " (m) are extracted.",
                          0.02,
                          0.9,
                          2,
                          m_unfittedDistance);
  configDialog->addDouble("Those unfitted points are clustered with a  "
                          "[<em><b>clustering range</b></em>]  ",
                          " (m). ",
                          0.03,
                          0.1,
                          2,
                          m_clusteringDistance);
  configDialog->addText("On each cluster a Dijkstra based tree segmentation is performed. Each point pair with a distance smaller "
                        "[<em><b>clustering range</b></em>] builds a Dijkstra edge.");
  configDialog->addEmpty();
}

void
SF_StepDijkstraLightRecursive::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configRecursion(configDialog);
}

void
SF_StepDijkstraLightRecursive::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT2);
  if (resModelw != NULL) {
    QString name = tr("Dijkstra recursive");
    QString sfCylinders = name;
    sfCylinders.append(tr(" QSM"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER2));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepDijkstraLightRecursive::createInResultModelListProtected()
{
  addInputQSMs();
  addInputClouds();
}

void
SF_StepDijkstraLightRecursive::createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addEmpty();
  configDialog->addText(
    QObject::tr("For this step please cite in addition the early method using Dijkstra for predicting tree skeleton:"),
    "Xu, H.; Gossett, N.; Chen B.");
  configDialog->addText("(section 5.1 Tree Allometry)", "<em>Knowledge and heuristic-based modeling of laser-scanned trees.</em>");
  configDialog->addText("", "ACM Transactions on Graphics <b>2007</b>, 19-es.");
  configDialog->addEmpty();
  configDialog->addEmpty();
}

void
SF_StepDijkstraLightRecursive::compute()
{
  m_computationsDone = 0;
  createParamList();
  writeLogger();
  QFuture<void> future = QtConcurrent::map(_paramList, SF_SpherefollowingDijkstraLightRecursiveAdapter());
  while (!future.isFinished()) {
    setProgressByCounter(10.0f, 85.0f);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamSpherefollowingRecursive<SF_PointNormal>>(
    getOutResultList().front(), _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER2), _outSFQSM.completeName(), "");
  _paramList.clear();
}

void
SF_StepDijkstraLightRecursive::createParamList()
{
  SF_SphereFollowingParameters sphereFollowingParams;
  SF_SphereFollowingOptimizationParameters sfOptimizationParameters;
  sfOptimizationParameters._epsilonSphere = _SF_OPT_sphereEpsilon;
  sfOptimizationParameters._epsilonSphereMultiplier = paramsStringToNumber(_PARAMETERS_CHOICE_SPHERE_EPSILON);
  sfOptimizationParameters._euclideanClusteringDistance = _SF_OPT_euclideanClusteringDistance;
  sfOptimizationParameters._euclideanClusteringDistanceMultiplier = paramsStringToNumber(
    _PARAMETERS_CHOICE_EUCLIDEAN_CLUSTERING_DISTANCE);
  sfOptimizationParameters._sphereRadiusMultiplier = _SF_OPT_sphereRadiusMultiplier;
  sfOptimizationParameters._sphereRadiusMultiplierMultiplier = paramsStringToNumber(_PARAMETERS_CHOICE_SPHERE_RADIUS_MULTIPLIER);
  std::vector<SF_SphereFollowingOptimizationParameters> optimizationParametersVector;
  optimizationParametersVector.push_back(sfOptimizationParameters);
  sphereFollowingParams.m_optimizationParams = optimizationParametersVector;
  sphereFollowingParams._minPtsGeometry = _SF_minPtsGeometry;
  sphereFollowingParams._inlierDistance = _SF_inlierDistance;
  sphereFollowingParams._RANSACIterations = _SF_RANSACIiterations;
  sphereFollowingParams._heightInitializationSlice = _SF_heightInitializationSlice;
  sphereFollowingParams._minGlobalRadius = _SF_minRadiusGlobal;
  sphereFollowingParams._fittingMethod = toStringSFMethod();

  SF_CloudToModelDistanceParameters distanceParams;
  distanceParams._method = toStringCMDMethod();
  distanceParams._k = _CMD_k;
  distanceParams._cropDistance = _CMD_cropDistance;
  distanceParams._inlierDistance = _CMD_inlierDistance;

  adaptParametersToExpertLevel();

  std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>> qsms = getInputQSMs();
  std::vector<const CT_AbstractItemDrawableWithPointCloud*> clouds = getIputClouds();
  if (!hasSameSize(qsms, clouds)) {
    return;
  }

  for (size_t index = 0; index < clouds.size(); ++index) {
    SF_ParamSpherefollowingRecursive<SF_PointNormal> param;
    param.m_maxPercentage = m_maxPercentage;
    param.m_clusterDownScale = m_clusterDownScale;
    param.m_slice = m_clusteringDistance;
    param.m_clusteringDistance = m_clusteringDistance;
    param.m_djikstraRange = m_clusteringDistance;
    param.m_minPercentage = m_minPercentage;
    param.m_unfittedDistance = m_unfittedDistance;
    param._stepProgress = _stepProgress;
    param._distanceParams = distanceParams;
    param._sphereFollowingParams = sphereFollowingParams;
    param._voxelSize = m_clusteringDistance;
    param._clusteringDistance = _PP_euclideanClusteringDistance;
    param.m_numClstrs = _CMD_numClstrs;
    param._modelCloudError = 1337;
    param._fittedGeometries = 0;
    param._log = PS_LOG;
    param._itemCpyCloudIn = clouds[index];
    param._qsm = qsms[index].first;
    param._grpCpyGrp = qsms[index].second;
    _paramList.append(param);
  }
  m_computationsTotal = _paramList.size();
}
