/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepBufferFilter.h"

#include "steps/filter/binary/statistical_outlier_filter/sf_statisticalOutlierRemovalAdapter.h"

#include "pcl/cloud/segmentation/dijkstra/sf_dijkstra.h"

#define DEF_IN_GRP1 "grp1"
#define DEF_IN_GRP2 "grp2"
#define DEF_IN_CLOUD1 "cloud1"
#define DEF_IN_CLOUD2 "cloud2"

SF_StepBufferFilter::SF_StepBufferFilter(CT_StepInitializeData& dataInit) : SF_AbstractFilterBinaryStep(dataInit) {}

SF_StepBufferFilter::~SF_StepBufferFilter() {}

QString
SF_StepBufferFilter::getStepDescription() const
{
  return tr("Buffers a filtered cloud.");
}

QString
SF_StepBufferFilter::getStepDetailledDescription() const
{
  return tr("A filter operation has two clouds as input. The orginal tree cloud and the filtered cloud. All points of the original "
            "cloud within a given range to the filtered cloud are merged to the filtered cloud.");
}

QString
SF_StepBufferFilter::getStepURL() const
{
  return tr("www.simpleForest.org");
}

CT_VirtualAbstractStep*
SF_StepBufferFilter::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepBufferFilter(dataInit);
}

QStringList
SF_StepBufferFilter::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  return _risCitationList;
}

void
SF_StepBufferFilter::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Filtered Group result"));
  assert(resModel != NULL);
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel(
    "", DEF_IN_GRP1, CT_AbstractItemGroup::staticGetType(), tr("Filtered Group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP1, DEF_IN_CLOUD1, CT_Scene::staticGetType(), tr("Filtered Cloud"));
  CT_InResultModelGroup* resModel2 = createNewInResultModel(DEF_IN_RESULT2, tr("Original group result"));
  resModel2->setZeroOrMoreRootGroup();
  resModel2->addGroupModel(
    "", DEF_IN_GRP2, CT_AbstractItemGroup::staticGetType(), tr("Original Group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel2->addItemModel(DEF_IN_GRP2, DEF_IN_CLOUD2, CT_Scene::staticGetType(), tr("Original Cloud"));
}

void
SF_StepBufferFilter::createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble("Run the buffering with [<em><b>range</b></em>] of  ", "(m).", 0.02, 0.5, 3, m_range);
}

void
SF_StepBufferFilter::createPostConfigurationDialogBeginner(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble("Run the buffering with [<em><b>range</b></em>] of  ", "(m).", 0.02, 0.5, 3, m_range);
}

void
SF_StepBufferFilter::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != nullptr) {
    resModelw->addItemModel(DEF_IN_GRP1, _outCloud, new CT_Scene(), tr("Filtered and Buffer extended Clouds"));
  }
}

void
SF_StepBufferFilter::adaptParametersToExpertLevel()
{
}

void
SF_StepBufferFilter::writeOutputPerScence(CT_ResultGroup*, size_t)
{
}

void
SF_StepBufferFilter::writeOutput(CT_ResultGroup*)
{
}

void
SF_StepBufferFilter::compute()
{
  m_computationsDone = 0;
  constexpr float goodPointIntensity = 0.f;
  constexpr float noisePointIntensity = 1.f;
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_ResultGroup* inResult2 = getInputResults().at(1);
  CT_ResultGroupIterator outResIt1(outResult, this, DEF_IN_GRP1);
  CT_ResultGroupIterator outResIt2(inResult2, this, DEF_IN_GRP2);
  std::vector<CT_AbstractItemDrawableWithPointCloud*> clouds1;
  std::vector<CT_AbstractItemDrawableWithPointCloud*> clouds2;
  while (!isStopped() && outResIt1.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt1.next();
    CT_AbstractItemDrawableWithPointCloud* ct_cloud = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD1);
    clouds1.push_back(ct_cloud);
  }
  while (!isStopped() && outResIt2.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt2.next();
    CT_AbstractItemDrawableWithPointCloud* ct_cloud = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD2);
    clouds2.push_back(ct_cloud);
  }
  if (clouds1.size() == clouds2.size()) {
    CT_ResultGroupIterator outResIt3(outResult, this, DEF_IN_GRP1);
    for (size_t i = 0; i < clouds1.size(); i++) {
      CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt3.next();
      CT_AbstractItemDrawableWithPointCloud* cloud1 = clouds1[i];
      Sf_ConverterCTToPCL<SF_PointNormal> converter;
      converter.setItemCpyCloudInDeprecated(cloud1);
      converter.compute();
      const auto translation = converter.translation();
      SF_CloudNormal::Ptr cloudFiltered = converter.cloudTranslated();
      CT_AbstractItemDrawableWithPointCloud* cloud2 = clouds2[i];
      Sf_ConverterCTToPCL<SF_PointNormal> converter2;
      converter2.setItemCpyCloudInDeprecated(cloud2);
      converter2.compute(translation);
      SF_CloudNormal::Ptr cloudOriginal = converter2.cloudTranslated();

      pcl::search::KdTree<pcl::PointXYZINormal>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZINormal>);
      kdtree->setInputCloud(cloudFiltered);
      std::vector<int> pointIdxNKNSearch(1);
      std::vector<float> pointNKNSquaredDistance(1);
      for (auto& p : cloudOriginal->points) {
        p.intensity = noisePointIntensity;
      }
      for (auto& p : cloudOriginal->points) {
        if (kdtree->nearestKSearch(p, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) {
          if (std::sqrt(pointNKNSquaredDistance[0]) < m_range) {
            p.intensity = goodPointIntensity;
          }
        }
      }

      std::vector<size_t> newIndices;
      CT_PointCloudIndexVector* vec = new CT_PointCloudIndexVector();
      if (cloud2 != nullptr) {
        const CT_AbstractPointCloudIndex* indices2 = cloud2->getPointCloudIndex();
        std::uint32_t index = 0;
        CT_PointIterator it1(indices2);
        while (it1.hasNext()) {
          auto n = it1.next();
          float thisIntensity = cloudOriginal->points[index].intensity;
          if (thisIntensity == goodPointIntensity) {
            newIndices.push_back(n.currentGlobalIndex());
          }
          ++index;
        }
      }
      std::sort(newIndices.begin(), newIndices.end());
      std::for_each(newIndices.begin(), newIndices.end(), [vec](size_t index) { vec->addIndex(index); });
      CT_Scene* outScene = new CT_Scene(_outCloud.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(vec));
      outScene->updateBoundingBox();
      group->addItemDrawable(outScene);
    }
  } else {
    QString str = "You tried to merge ";
    str.append(QString::number(clouds1.size()));
    str.append(" clouds with ");
    str.append(QString::number(clouds2.size()));
    str.append(" clouds. This is not possible, the number of clouds has to be the same. Producing an empty result.");
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, str);
  }
}

void
SF_StepBufferFilter::writeLogger()
{
}

void
SF_StepBufferFilter::createParamList(CT_ResultGroup*)
{
}
