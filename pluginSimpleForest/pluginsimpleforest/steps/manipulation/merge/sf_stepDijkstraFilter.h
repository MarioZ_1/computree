/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPDIJKSTRAFILTER_H
#define SF_STEPDIJKSTRAFILTER_H

#include "steps/filter/binary/sf_abstractFilterBinaryStep.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include <steps/param/sf_paramAllSteps.h>

class SF_StepDijkstraFilter : public SF_AbstractFilterBinaryStep
{
  Q_OBJECT

public:
  SF_StepDijkstraFilter(CT_StepInitializeData& dataInit);
  ~SF_StepDijkstraFilter();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void adaptParametersToExpertLevel();
  void createPostConfigurationDialogBeginner(CT_StepConfigurableDialog* configDialog);
  void createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog);
  void compute();
  virtual void writeLogger();

private:
  double m_range = 0.1;

  void writeOutputPerScence(CT_ResultGroup*, size_t);
  void writeOutput(CT_ResultGroup* outResult);
  void createParamList(CT_ResultGroup* outResult);
};

#endif // SF_STEPDIJKSTRAFILTER_H
