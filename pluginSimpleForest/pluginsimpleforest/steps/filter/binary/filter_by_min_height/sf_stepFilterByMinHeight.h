/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPFILTERBYMINHEIGHT_H
#define SF_STEPFILTERBYMINHEIGHT_H

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "steps/segmentation/sf_AbstractStepSegmentation.h"

#include "steps/param/sf_paramAllSteps.h"

class SF_StepFilterByMinHeight : public SF_AbstractStepSegmentation
{
  Q_OBJECT

public:
  SF_StepFilterByMinHeight(CT_StepInitializeData& dataInit);
  ~SF_StepFilterByMinHeight();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  QList<SF_ParamGroundFilter<SF_PointNormal>> _paramList;
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void adaptParametersToExpertLevel(){};
  void createPostConfigurationDialogBeginner(CT_StepConfigurableDialog*){};
  void createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog);
  void compute();
  virtual void writeLogger();

private:
  double m_minHeight = 1.25;
  CT_AutoRenameModels m_outGrpTrees;
  CT_AutoRenameModels m_outGrpUndergrowth;
  CT_AutoRenameModels m_outCloudTrees;
  CT_AutoRenameModels m_outCloudUndergrowth;
};

#endif // SF_STEPFILTERBYMINHEIGHT_H
