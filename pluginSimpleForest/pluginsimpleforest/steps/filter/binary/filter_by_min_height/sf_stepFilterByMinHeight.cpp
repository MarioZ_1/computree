/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepFilterByMinHeight.h"

#include "converters/CT_To_PCL/sf_converterCTToPCLDTM.h"

SF_StepFilterByMinHeight::SF_StepFilterByMinHeight(CT_StepInitializeData& dataInit) : SF_AbstractStepSegmentation(dataInit) {}

SF_StepFilterByMinHeight::~SF_StepFilterByMinHeight() {}

QString
SF_StepFilterByMinHeight::getStepDescription() const
{
  return tr("Filter By Min Height");
}

QString
SF_StepFilterByMinHeight::getStepDetailledDescription() const
{
  return tr("Ground - This filter imports a DTM and input clouds. For each input cloud"
            " the point with minimal z coordinate is estimated. If this point has a distance"
            " larger than a user given treshold from the DTM, the cloud is removed.");
}

QString
SF_StepFilterByMinHeight::getStepURL() const
{
  return tr("https://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepFilterByMinHeight::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepFilterByMinHeight(dataInit);
}

QStringList
SF_StepFilterByMinHeight::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  return _risCitationList;
}

void
SF_StepFilterByMinHeight::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Point Cloud result"));
  assert(resModel != NULL);
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Group to be filtered"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(
    DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Cloud to be filtered"));
  resModel->addGroupModel(
    "", DEF_IN_SCENE, CT_AbstractItemGroup::staticGetType(), tr("Root group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);

  CT_InResultModelGroup* resModelDTM = createNewInResultModel(DEF_IN_RESULT_DTM, tr("DTM result"));
  assert(resModelDTM != NULL);
  resModelDTM->setZeroOrMoreRootGroup();
  resModelDTM->addGroupModel(
    "", DEF_IN_DTMGRP, CT_AbstractItemGroup::staticGetType(), tr("DTM group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModelDTM->addItemModel(DEF_IN_DTMGRP, DEF_IN_DTM, CT_Image2D<float>::staticGetType(), tr("DTM model"));
}

void
SF_StepFilterByMinHeight::createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble("If a clouds lowest point is further away from the DTM "
                          "than [<em><b>maximal MinHeight</b></em>] of  ",
                          "  (m), the cloud is removed",
                          0.1,
                          100,
                          2,
                          m_minHeight);
}

void
SF_StepFilterByMinHeight::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    resModelw->addGroupModel(DEF_IN_SCENE, m_outGrpUndergrowth, new CT_StandardItemGroup(), tr("Noise group"));
    resModelw->addItemModel(m_outGrpUndergrowth, m_outCloudUndergrowth, new CT_Scene(), tr("Noise cloud"));
    resModelw->addGroupModel(DEF_IN_SCENE, m_outGrpTrees, new CT_StandardItemGroup(), tr("Tree group"));
    resModelw->addItemModel(m_outGrpTrees, m_outCloudTrees, new CT_Scene(), tr("Tree cloud"));
  }
}

void
SF_StepFilterByMinHeight::compute()
{
  CT_ResultGroup* inDTMResult = getInputResults().at(1);
  CT_ResultItemIterator iterDTM(inDTMResult, this, DEF_IN_DTM);
  CT_Image2D<float>* dtm = (CT_Image2D<float>*)iterDTM.next();

  CT_ResultGroup* outResult = getOutResultList().at(0);
  _groupsToBeRemoved.clear();
  CT_StandardItemGroup* groupRoot{};
  CT_ResultGroupIterator outResIt2(outResult, this, DEF_IN_SCENE);
  while (!isStopped() && outResIt2.hasNext()) {
    groupRoot = (CT_StandardItemGroup*)outResIt2.next();
  }
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    if (group != NULL) {
      const CT_AbstractItemDrawableWithPointCloud* ctCloud =
        (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_IN_CLOUD_SEED);
      if (ctCloud != NULL) {
        if (ctCloud->getPointCloudIndex()->size() <= 0) {
          _groupsToBeRemoved.push_back(group);
        } else {
          const CT_AbstractPointCloudIndex* indices = ctCloud->getPointCloudIndex();
          CT_PointCloudIndexVector* indexvector = new CT_PointCloudIndexVector;
          CT_PointIterator it(indices);
          CT_Point minPoint;
          double minZ = std::numeric_limits<double>::max();
          while (it.hasNext()) {
            const CT_Point& internalPoint = it.next().currentPoint();
            const size_t index = it.currentGlobalIndex();
            indexvector->addIndex(index);
            if (internalPoint.z() < minZ) {
              minZ = internalPoint.z();
              minPoint = internalPoint;
            }
          }
          indexvector->setSortType(CT_AbstractCloudIndex::SortType::SortedInAscendingOrder);
          auto dtmHeight = dtm->valueAtCoords(minPoint.x(), minPoint.y());
          auto dist = minZ - dtmHeight;
          if (dist < m_minHeight) {
            CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpTrees.completeName(), outResult);
            groupRoot->addGroup(filterGrp);
            CT_Scene* outScene = new CT_Scene(
              m_outCloudTrees.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexvector));
            outScene->updateBoundingBox();
            filterGrp->addItemDrawable(outScene);
          } else {
            CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpUndergrowth.completeName(), outResult);
            groupRoot->addGroup(filterGrp);
            CT_Scene* outScene = new CT_Scene(
              m_outCloudUndergrowth.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexvector));
            outScene->updateBoundingBox();
            filterGrp->addItemDrawable(outScene);
          }
        }
      } else {
        _groupsToBeRemoved.push_back(group);
      }
    } else {
      _groupsToBeRemoved.push_back(group);
    }
  }
  removeCorruptedScenes();
}

void
SF_StepFilterByMinHeight::writeLogger()
{
}
