/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepEuclideanClusteringFilter.h"

#include "sf_stepEuclideanClusteringFilterAdapter.h"

#include <QtConcurrent/QtConcurrent>

SF_StepEuclideanClusteringFilter::SF_StepEuclideanClusteringFilter(CT_StepInitializeData& dataInit)
  : SF_AbstractFilterBinaryStep(dataInit)
{
}

SF_StepEuclideanClusteringFilter::~SF_StepEuclideanClusteringFilter() {}

QString
SF_StepEuclideanClusteringFilter::getStepDescription() const
{
  return tr("Euclidean Clustering Filter");
}

QString
SF_StepEuclideanClusteringFilter::getStepDetailledDescription() const
{
  return tr("Euclidean clustering filter - This filter clusters an input cloud with"
            " a user given clustering range. Only a user specified number of clusters"
            " containing more than min and less than max points are merged into"
            " one output, the other points are considered noise.");
}

QString
SF_StepEuclideanClusteringFilter::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepEuclideanClusteringFilter::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepEuclideanClusteringFilter(dataInit);
}

QStringList
SF_StepEuclideanClusteringFilter::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepEuclideanClusteringFilter::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* res_model = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Point Cloud"));
  assert(res_model != NULL);
  res_model->setZeroOrMoreRootGroup();
  res_model->addGroupModel("",
                           DEF_IN_GRP_CLUSTER,
                           CT_AbstractItemGroup::staticGetType(),
                           tr("Group to be denoised"),
                           "",
                           CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  res_model->addItemModel(
    DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Cloud to be denoised"));
}

void
SF_StepEuclideanClusteringFilter::createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble(
    "First the cloud is downscaled to a [<em><b>voxel size</b></em>] of  ", " (m). ", 0.01, 0.1, 3, params.m_voxelSize);
  configDialog->addDouble("Then an euclidean clustering routine is performed with"
                          " a [<em><b>range</b></em>] of  ",
                          " (m).",
                          0.025,
                          5,
                          3,
                          params.m_clusterRange);

  configDialog->addDouble("A good cluster has to contain more than  "
                          " [<em><b>minimum percentage</b></em>] ",
                          "  .",
                          0,
                          1,
                          3,
                          params.m_minPtsRel);
  configDialog->addDouble("A good cluster has to contain less than  "
                          " [<em><b>maximum percentage</b></em>] ",
                          "  .",
                          0,
                          1,
                          3,
                          params.m_maxPtsRel);
  configDialog->addDouble("A good cluster has to have a larger extension than  "
                          " [<em><b>minimum extension</b></em>] ",
                          " (m).",
                          0,
                          1,
                          3,
                          params.m_minLength);
  configDialog->addInt(
    "Only the first [<em><b>n</b></em>]", " largest clusters are merged into the output.", 1, 99999, params.m_numberClusters);
}

void
SF_StepEuclideanClusteringFilter::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != nullptr) {
    resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, _outGrp, new CT_StandardItemGroup(), tr("Euclidean Clustering Filter"));
    resModelw->addItemModel(_outGrp, _outCloud, new CT_Scene(), tr("Filtered Cloud"));
    resModelw->addItemModel(_outGrp, _outNoise, new CT_Scene(), tr("Noise"));
  }
}

void
SF_StepEuclideanClusteringFilter::writeOutputPerScence(CT_ResultGroup* outResult, size_t i)
{
  SF_ParamEuclideanClusteringFilter<SF_PointNormal> param = _paramList.at(static_cast<int>(i));
  std::vector<CT_PointCloudIndexVector*> outputIndexList = createOutputVectors(static_cast<size_t>(param._sizeOutput));
  createOutputIndices(outputIndexList, param._outputIndices, param._itemCpyCloudIn);
  CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(_outGrp.completeName(), outResult);
  param._grpCpyGrp->addGroup(filterGrp);
  addSceneToFilterGrp(filterGrp, outResult, outputIndexList[0], _outCloud.completeName());
  addSceneToFilterGrp(filterGrp, outResult, outputIndexList[1], _outNoise.completeName());
}

void
SF_StepEuclideanClusteringFilter::writeOutput(CT_ResultGroup* outResult)
{
  int size = _paramList.size();
  for (int i = 0; i < size; i++) {
    writeOutputPerScence(outResult, static_cast<size_t>(i));
  }
}

void
SF_StepEuclideanClusteringFilter::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  identifyAndRemoveCorruptedScenes(outResult);
  createParamList(outResult);
  QFuture<void> future = QtConcurrent::map(_paramList, SF_StepEuclideanClsteringFilterAdapter());
  setProgressByFuture(future, 10, 85);
  writeOutput(outResult);
  writeLogger();
  _paramList.clear();
}

void
SF_StepEuclideanClusteringFilter::writeLogger()
{
  if (!_paramList.empty()) {
    auto strList = _paramList[0].toStringList();
    for (auto& str : strList) {
      PS_LOG->addMessage(LogInterface::info, LogInterface::step, str);
    }
    size_t filtered = 0;
    size_t total = 0;
    for (auto const& param : _paramList) {
      auto vector = param._outputIndices;
      for (auto i : vector) {
        total++;
        filtered += static_cast<size_t>(i);
      }
    }
    auto str2 = _paramList[0].toFilterString(total, filtered);
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, str2);
  }
}

void
SF_StepEuclideanClusteringFilter::createParamList(CT_ResultGroup* outResult)
{
  adaptParametersToExpertLevel();
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const CT_AbstractItemDrawableWithPointCloud* ct_cloud =
      (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_IN_CLOUD_SEED);
    SF_ParamEuclideanClusteringFilter<SF_PointNormal> param = params;
    param._log = PS_LOG;
    param._sizeOutput = 2;
    param._itemCpyCloudIn = ct_cloud;
    param._grpCpyGrp = group;
    _paramList.append(param);
  }
}
