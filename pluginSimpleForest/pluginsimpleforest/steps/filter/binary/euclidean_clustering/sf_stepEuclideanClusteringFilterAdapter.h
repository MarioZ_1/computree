/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPEUCLIDEANCLUSTERINGFILTERADAPTER_H
#define SF_STEPEUCLIDEANCLUSTERINGFILTERADAPTER_H

#include <QThreadPool>
#include <iostream>
#include <pcl/cloud/filter/binary/stem/sf_stemFilter.h>

#include "converters/CT_To_PCL/sf_converterCTToPCL.h"
#include "steps/param/sf_paramAllSteps.h"

#include <pcl/filters/voxel_grid.h>

class SF_StepEuclideanClsteringFilterAdapter
{
public:
  std::shared_ptr<QMutex> mMutex;

  SF_StepEuclideanClsteringFilterAdapter(const SF_StepEuclideanClsteringFilterAdapter& obj) { mMutex = obj.mMutex; }

  SF_StepEuclideanClsteringFilterAdapter() { mMutex.reset(new QMutex); }

  ~SF_StepEuclideanClsteringFilterAdapter() {}

  void operator()(SF_ParamEuclideanClusteringFilter<SF_PointNormal>& params)
  {
    Sf_ConverterCTToPCL<SF_PointNormal> converter;
    {
      QMutexLocker m1(&*mMutex);
      converter.setItemCpyCloudInDeprecated(params._itemCpyCloudIn);
    }
    converter.compute();

    SF_CloudNormal::Ptr cloud = converter.cloudTranslated();
    pcl::VoxelGrid<SF_PointNormal> vg;
    SF_CloudNormal::Ptr cloudFiltered(new SF_CloudNormal);
    vg.setInputCloud(cloud);
    vg.setLeafSize(
      static_cast<float>(params.m_voxelSize), static_cast<float>(params.m_voxelSize), static_cast<float>(params.m_voxelSize));
    vg.filter(*cloudFiltered);

    std::vector<pcl::PointIndices> clusterIndicesDownscaled;
    std::vector<int> downScaledIndices(cloudFiltered->points.size(), 1);
    pcl::EuclideanClusterExtraction<SF_PointNormal> ec;
    {
      QMutexLocker m1(&*mMutex);
      ec.setClusterTolerance(params.m_clusterRange);
    }
    int size = static_cast<int>(cloudFiltered->points.size());
    ec.setMinClusterSize(1);
    ec.setMaxClusterSize(size);
    pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>);
    tree->setInputCloud(cloudFiltered);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloudFiltered);
    ec.extract(clusterIndicesDownscaled);

    int j = 0;
    int numberClusters;
    size_t min;
    size_t max;
    {
      QMutexLocker m1(&*mMutex);
      min = static_cast<size_t>(params.m_minPtsRel * size);
      max = static_cast<size_t>(params.m_maxPtsRel * size);
      numberClusters = params.m_numberClusters;
    }
    for (std::vector<pcl::PointIndices>::const_iterator it = clusterIndicesDownscaled.begin(); it != clusterIndicesDownscaled.end();
         ++it) {
      if (j >= numberClusters) {
        break;
      }
      if (it->indices.size() >= min && it->indices.size() <= max) {
        j++;
        SF_CloudNormal cluster;
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
          cluster.push_back(cloudFiltered->points[static_cast<size_t>(*pit)]);

        SF_PointNormal min_pt, max_pt;
        const auto minIt = std::min_element(
          cluster.points.begin(), cluster.points.end(), [](const auto& p1, const auto& p2) { return p1.z < p2.z; });
        const auto maxIt = std::max_element(
          cluster.points.begin(), cluster.points.end(), [](const auto& p1, const auto& p2) { return p1.z < p2.z; });
        auto distance = std::abs(maxIt->z - minIt->z);
        if (distance > params.m_minLength) {
          for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
            downScaledIndices[static_cast<size_t>(*pit)] = 0;
        }
      }
    }
    std::vector<int> indices;
    pcl::KdTreeFLANN<SF_PointNormal> kdtree2;
    kdtree2.setInputCloud(cloudFiltered);
    for (auto point : cloud->points) {
      std::vector<int> pointIdxNKNSearch(1);
      std::vector<float> pointNKNSquaredDistance(1);
      if (kdtree2.nearestKSearch(point, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) {
        indices.push_back(downScaledIndices[static_cast<size_t>(pointIdxNKNSearch[0u])]);
      } else {
        indices.push_back(1);
      }
    }
    {
      QMutexLocker m1(&*mMutex);
      params._outputIndices = std::move(indices);
    }
  }
};

#endif // SF_STEPEUCLIDEANCLUSTERINGFILTERADAPTER_H
