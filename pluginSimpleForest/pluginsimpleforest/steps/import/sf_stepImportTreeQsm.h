/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPIMPORTTREEQSM_H
#define SF_STEPIMPORTTREEQSM_H

#include <ct_itemdrawable/ct_scene.h>
#include <ct_iterator/ct_pointiterator.h>
#include <ct_pointcloudindex/ct_pointcloudindexvector.h>
#include <ct_result/ct_resultgroup.h>
#include <ct_result/model/inModel/ct_inresultmodelgrouptocopy.h>
#include <ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h>
#include <ct_step/abstract/ct_abstractstep.h>
#include <ct_view/ct_stepconfigurabledialog.h>

#include "steps/qsm/sf_abstractStepQSM.h"

#define DEF_IN_RESULT "ires"
#define DEF_IN_GRP_CLUSTER "igrp"

#define DEF_IN_RESULT "ires"
#define DEF_IN_RESULT2 "ires2"
#define DEF_IN_RESULT3 "ires3"
#define DEF_IN_RESULT4 "ires4"
#define DEF_IN_GRP_CLUSTER "igrp"
#define DEF_IN_GRP_CLUSTER2 "igrp2"
#define DEF_IN_GRP_CLUSTER3 "igrp3"
#define DEF_IN_NAME "iname"
#define DEF_IN_FILE "ifile"
#define DEF_IN_RESULT_DTM "idtmres"
#define DEF_IN_DTMGRP "igrpdtm"
#define DEF_IN_DTM "idtm"
#define DEF_IN_CLOUD_SEED "icloud"
#define DEF_IN_QSM "iqsm"
#define DEF_IN_SCENE "igrp2"
#define DEF_IN_SCENE_CLOUD "icloud2"
#define DEF_IN_GRP_TREES "iTrees"
#define DEF_IN_GRP_UNDERGROWTH "iUndergrowth"
#define DEF_IN_CLOUD_TREES "iCloudTrees"
#define DEF_IN_CLOUD_UNDERGROWTH "iCloudUndergrowth"
#define DEF_IN_DTM_GRP "idtmGrp"
#define DEF_IN_DTM "idtm"

class SF_StepImportTreeQsm : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepImportTreeQsm(CT_StepInitializeData& dataInit);
  ~SF_StepImportTreeQsm();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  virtual void createPostConfigurationDialog();
  void compute();

private:
  QStringList m_filePath;
  bool m_originalRadius = false;
  CT_AutoRenameModels m_outGroup;
  CT_AutoRenameModels m_outScene;
};

#endif // SF_STEPIMPORTTREEQSM_H
