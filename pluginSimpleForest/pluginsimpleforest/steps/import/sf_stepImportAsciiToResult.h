/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPIMPORTASCIITORESULT_H
#define SF_STEPIMPORTASCIITORESULT_H

#include <ct_itemdrawable/ct_scene.h>
#include <ct_iterator/ct_pointiterator.h>
#include <ct_pointcloudindex/ct_pointcloudindexvector.h>
#include <ct_result/ct_resultgroup.h>
#include <ct_result/model/inModel/ct_inresultmodelgrouptocopy.h>
#include <ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h>
#include <ct_step/abstract/ct_abstractstep.h>
#include <ct_view/ct_stepconfigurabledialog.h>

#define DEF_IN_RESULT "ires"
#define DEF_IN_GRP_CLUSTER "igrp"

class SF_StepImportAsciiToResult : public CT_AbstractStep
{
  Q_OBJECT

public:
  SF_StepImportAsciiToResult(CT_StepInitializeData& dataInit);
  ~SF_StepImportAsciiToResult();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  virtual void createPostConfigurationDialog();
  void compute();

private:
  QStringList m_filePath;
  CT_AutoRenameModels m_outGroup;
  CT_AutoRenameModels m_outScene;
};

#endif // SF_STEPIMPORTASCIITORESULT_H
