/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepImportAsciiToResult.h"

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <sstream>
#include <string>

#include <ct_itemdrawable/ct_fileheader.h>

SF_StepImportAsciiToResult::SF_StepImportAsciiToResult(CT_StepInitializeData& dataInit) : CT_AbstractStep(dataInit) {}

SF_StepImportAsciiToResult::~SF_StepImportAsciiToResult() {}

QString
SF_StepImportAsciiToResult::getStepDescription() const
{
  return tr("Imports xyz Ascii files without generating a new result.");
}

QString
SF_StepImportAsciiToResult::getStepDetailledDescription() const
{
  return tr("Computree importers generate a new result making it impossible to process to imported clouds in the same processing "
            "chain. This step simply lets you choose a x,y,z formatted clouds and generates ct clouds out of it. This step was "
            "designed to import the leave points in addition to a denoised tree architecture cloud.");
}

QString
SF_StepImportAsciiToResult::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepImportAsciiToResult::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepImportAsciiToResult(dataInit);
}

void
SF_StepImportAsciiToResult::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>Ascii importer</b>:");
  configDialog->addFileChoice(tr("Select the Ascii files"), CT_FileChoiceButton::OneOrMoreExistingFiles, "", m_filePath);
}

void
SF_StepImportAsciiToResult::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Input"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Tree Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
}

void
SF_StepImportAsciiToResult::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != nullptr) {
    resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, m_outGroup, new CT_StandardItemGroup(), tr("Leave group"));
    resModelw->addItemModel(m_outGroup, m_outScene, new CT_Scene(), tr("Leave Cloud"));
  }
}

void
SF_StepImportAsciiToResult::compute()
{
  using namespace std;
  using namespace boost;
  using boost::lexical_cast;
  if (m_filePath.empty() || m_filePath.first() == "") {
    QString errorMsg = "SF_StepImportAsciiToResult please give a path to import the cloud.";
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
    return;
  }
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* root = const_cast<CT_StandardItemGroup*>(static_cast<const CT_StandardItemGroup*>(outResIt.next()));
    for (const auto& path : m_filePath) {
      CT_StandardItemGroup* group = new CT_StandardItemGroup(m_outGroup.completeName(), outResult);
      CT_AbstractUndefinedSizePointCloud* mpcir = PS_REPOSITORY->createNewUndefinedSizePointCloud();
      std::ifstream infile(path.toStdString());
      std::string line;
      std::getline(infile, line);
      while (std::getline(infile, line)) {
        std::vector<double> myVec;
        char_separator<char> sep(" ");
        tokenizer<char_separator<char>> tokens(line, sep);
        CT_Point point;
        for (const auto& t : tokens) {
          std::string input = t.c_str();
          input.erase(std::remove(input.begin(), input.end(), '\n'), input.end());
          input.erase(std::remove(input.begin(), input.end(), '\r'), input.end());
          myVec.push_back(lexical_cast<double>(input));
        }
        point[0] = myVec[0];
        point[1] = myVec[1];
        point[2] = myVec[2];
        mpcir->addPoint(point);
      }
      CT_NMPCIR pcir = PS_REPOSITORY->registerUndefinedSizePointCloud(mpcir);
      CT_Scene* scene = new CT_Scene(m_outScene.completeName(), outResult, pcir);
      scene->updateBoundingBox();
      group->addItemDrawable(scene);
      root->addGroup(group);
    }
    break;
  }
}
