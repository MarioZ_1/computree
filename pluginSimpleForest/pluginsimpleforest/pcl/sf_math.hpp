/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_MATH_HPP
#define SF_MATH_HPP

#include "sf_math.h"

#include <numeric>
#include <unordered_map>

template<typename T>
const T SF_Math<T>::_PI = 3.1415926;

template<typename T>
const T SF_Math<T>::_RAD_TO_DEG = 180.0 / SF_Math::_PI;

template<typename T>
const T SF_Math<T>::_DEG_TO_RAD = SF_Math::_PI / 180.0;

template<typename T>
const T SF_Math<T>::_ALLOMETRIC_POWER = 1.0 / 2.59;

template<typename T>
T
SF_Math<T>::getAngleBetweenDegf(Eigen::Vector3f axis1, Eigen::Vector3f axis2)
{
  axis1.normalize();
  axis2.normalize();
  float angleBetween0And180 = acos(axis1.dot(axis2)) * SF_Math::_RAD_TO_DEG;
  return std::min(angleBetween0And180, static_cast<T>(180.0f - angleBetween0And180));
}

template<typename T>
void
SF_Math<T>::histogramDownscale(std::vector<T>& xInOut, std::vector<T>& yInOut, std::uint32_t nBins)
{
  const auto maxX = *std::max_element(xInOut.cbegin(), xInOut.cend());
  const auto minX = *std::min_element(xInOut.cbegin(), xInOut.cend());
  const auto xCellSize = (maxX - minX) / static_cast<T>(nBins);
  std::unordered_map<std::uint32_t, std::vector<T>> histogramMapX;
  std::unordered_map<std::uint32_t, std::vector<T>> histogramMapY;
  for (std::uint32_t index = 0; index < xInOut.size(); ++index) {
    const auto mapIndex = static_cast<std::uint32_t>((xInOut[index] - minX) / xCellSize);
    histogramMapX[mapIndex].push_back(xInOut[index]);
    histogramMapY[mapIndex].push_back(yInOut[index]);
  }
  xInOut.clear();
  yInOut.clear();
  for (auto& pair : histogramMapX) {
    xInOut.push_back(SF_Math<T>::getMedian(pair.second));
  }
  for (auto& pair : histogramMapY) {
    yInOut.push_back(SF_Math<T>::getMedian(pair.second));
  }
}

template<typename T>
T
SF_Math<T>::distancef(const Eigen::Vector3f& pointA, const Eigen::Vector3f& pointB)
{
  T distance = (pointA - pointB).norm();
  return distance;
}

template<typename T>
T
SF_Math<T>::getAngleBetweenRadf(Eigen::Vector3f axis1, Eigen::Vector3f axis2)
{
  axis1.normalize();
  axis2.normalize();
  return acos(axis1.dot(axis2));
}

template<typename T>
T
SF_Math<T>::getAngleBetweenDeg(Eigen::Vector3d axis1, Eigen::Vector3d axis2)
{
  axis1.normalize();
  axis2.normalize();
  T angleBetween0And180 = acos(axis1.dot(axis2)) * SF_Math::_RAD_TO_DEG;
  return std::min(angleBetween0And180, static_cast<T>(180.0 - angleBetween0And180));
}

template<typename T>
T
SF_Math<T>::distance(const Eigen::Vector3d& pointA, const Eigen::Vector3d& pointB)
{
  T distance = (pointA - pointB).norm();
  return distance;
}

template<typename T>
T
SF_Math<T>::getAngleBetweenRad(Eigen::Vector3d axis1, Eigen::Vector3d axis2)
{
  axis1.normalize();
  axis2.normalize();
  return acos(axis1.dot(axis2));
}

template<typename T>
T
SF_Math<T>::getMedian(std::vector<T>& vec)
{
  const auto size = vec.size();
  if (vec.size() == 0) {
    return static_cast<T>(0);
  }
  if (vec.size() == 1) {
    return vec[0];
  }
  std::sort(vec.begin(), vec.end());
  if (size % 2 == 0) {
    return (vec[size / 2 - 1] + vec[size / 2]) / static_cast<T>(2);
  } else {
    return vec[size / 2];
  }
}

template<typename T>
T
SF_Math<T>::getQuantile(std::vector<T>& vec, float quantile)
{
  if (vec.size() == 0) {
    return static_cast<T>(0);
  }
  size_t n = vec.size() * quantile;
  std::nth_element(vec.begin(), vec.begin() + n, vec.end());
  return vec[n];
}

template<typename T>
T
SF_Math<T>::getMean(const std::vector<T>& vec)
{
  if (vec.size() == 0) {
    return 0;
  }
  auto lambda = [&vec](double a, double b) { return a + b; };
  return (std::accumulate(vec.cbegin(), vec.cend(), static_cast<T>(0), lambda) / vec.size());
}

template<typename T>
T
SF_Math<T>::getStandardDeviation(const std::vector<T>& vec)
{
  if (vec.size() == 0) {
    return 0;
  }
  return getStandardDeviation(vec, getMean(vec));
}

template<typename T>
T
SF_Math<T>::getStandardDeviation(const std::vector<T>& vec, T mean)
{
  if (vec.size() == 0) {
    return 0;
  }
  const size_t sz = vec.size();
  if (sz == 1) {
    return 0.0;
  }
  // Now calculate the variance
  auto variance_func = [&mean, &sz](T accumulator, const T& val) { return accumulator + ((val - mean) * (val - mean) / (sz - 1)); };
  auto var = std::accumulate(vec.cbegin(), vec.cend(), 0.0, variance_func);
  return std::sqrt(var);
}

template<typename T>
std::vector<T>
SF_Math<T>::getLog(const std::vector<T>& vector)
{
  std::vector<T> logarithms;
  logarithms.reserve(vector.size());
  std::transform(vector.cbegin(), vector.cend(), std::back_inserter(logarithms), [](const T& value) { return std::log(value); });
  return logarithms;
}

template<typename T>
std::vector<T>
SF_Math<T>::getExp(const std::vector<T>& vector)
{
  std::vector<T> exponents;
  exponents.reserve(vector.size());
  std::transform(vector.cbegin(), vector.cend(), std::back_inserter(exponents), [](const T& value) { return std::exp(value); });
  return exponents;
}

template<typename T>
std::vector<T>
SF_Math<T>::normalize(const std::vector<T>& vector)
{
  if (vector.size() == 0) {
    return vector;
  }
  const auto minMaxItPair = std::minmax_element(vector.cbegin(), vector.cend(), [](const T& a, const T& b) { a < b; });
  const auto min = *(minMaxItPair.first);
  const auto max = *(minMaxItPair.second);
  const auto divisor = max - min;
  if (divisor == static_cast<T>(0)) {
    divisor = 1;
  }
  std::vector<T> normalized;
  normalized.reserve(vector.size());
  std::transform(
    vector.cbegin(), vector.cend(), std::back_inserter(normalized), [&min, &divisor](const T& a) { return (a - min) / divisor; });
  return normalized;
}

template<typename T>
std::vector<T>
SF_Math<T>::statisticalOutlierFilter(const std::vector<T>& vector, std::uint32_t iterations, T multiplier)
{
  auto vec = vector;
  for (std::uint32_t i = 0; i < iterations; ++i) {
    std::vector<T> vectorNew;
    vectorNew.reserve(vector.size());
    const auto mean = getMean(vec);
    const auto sd = getStandardDeviation(vec, mean);
    const auto threshold = mean + multiplier * sd;
    std::copy_if(
      vector.cbegin(), vector.cend(), std::back_inserter(vectorNew), [&threshold](const T& entry) { return entry <= threshold; });
    vec = std::move(vectorNew);
  }
  return vec;
}

#endif // SF_MATH_HPP
