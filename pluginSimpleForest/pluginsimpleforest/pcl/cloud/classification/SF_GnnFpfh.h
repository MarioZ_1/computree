/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_GNNFPFH_H
#define SF_GNNFPFH_H

#include "pcl/cloud/sf_abstractCloud.h"

#include <pcl/features/fpfh.h>

class SF_GNNFPFH : public SF_AbstractCloud<pcl::FPFHSignature33>
{
  std::uint32_t m_numberClusters;

  void createIndices() override;
  void createIndex(pcl::FPFHSignature33, float) override;
  void reset() override;

public:
  SF_GNNFPFH(pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloudIn);
  void setParameters(std::uint32_t numberClusters, int dimension);
  void compute();
  int m_dimension = 5;
};

#endif // SF_GNNFPFH_H
