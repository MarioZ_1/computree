/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_gnn.h"

#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/ml.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>

void
SF_Gnn::createIndices()
{
}

void
SF_Gnn::createIndex(pcl::FPFHSignature33, float)
{
}

void
SF_Gnn::reset()
{
}

SF_Gnn::SF_Gnn(pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloudIn) : SF_AbstractCloud<pcl::FPFHSignature33>(cloudIn) {}

void
SF_Gnn::setParameters(std::uint32_t numberClusters, std::uint32_t reducedDimension)
{
  m_numberClusters = numberClusters;
  m_dimension = reducedDimension;
}

void
SF_Gnn::compute(std::vector<std::vector<float>> features)
{
  if (features.empty()) {
    std::cerr << "SF_Gnn::compute features.empty()" << std::endl;
  }
  auto columns = features.front().size();
  auto rows = features.size();
  Eigen::MatrixXf matEigen(rows, columns);
  for (std::uint32_t r = 0; r < features.size(); ++r) {
    const std::vector<float>& feature = features[r];
    for (std::uint32_t c = 0; c < feature.size(); ++c) {
      matEigen(r, c) = feature[c];
    }
  }

  Eigen::RowVectorXf maxCoeffs = matEigen.colwise().maxCoeff();
  Eigen::RowVectorXf minCoeffs = matEigen.colwise().minCoeff();
  Eigen::RowVectorXf scalingFactors = maxCoeffs - minCoeffs;
  matEigen = (matEigen.rowwise() - minCoeffs).array().rowwise() / scalingFactors.array();

  // Mean centering data.
  Eigen::VectorXf featureMeans = matEigen.colwise().mean();
  Eigen::MatrixXf centered = matEigen.rowwise() - featureMeans.transpose();

  // Compute the covariance matrix.
  Eigen::MatrixXf cov = centered.adjoint() * centered;
  cov = cov / (static_cast<float>(matEigen.rows()) - 1.f);

  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eig(cov);

  // Get the two major eigenvectors and omit the others.
  Eigen::MatrixXf evecs = eig.eigenvectors();
  Eigen::MatrixXf pcaTransform = evecs.rightCols(m_dimension);
  matEigen = matEigen * pcaTransform;

  std::vector<std::int32_t>& indices = SF_AbstractCloud<pcl::FPFHSignature33>::_indices;
  indices.clear();
  cv::Mat mat(static_cast<std::int32_t>(matEigen.rows()), static_cast<std::int32_t>(matEigen.cols()), CV_32FC(1));
  for (std::uint32_t i = 0; i < matEigen.rows(); ++i) {
    for (std::uint32_t j = 0; j < matEigen.cols(); ++j) {
      mat.at<float>(static_cast<std::int32_t>(i), static_cast<std::int32_t>(j)) = matEigen(i, j);
    }
  }

  {
    using namespace cv;
    using namespace cv::ml;
    cv::Ptr<cv::ml::EM> model = cv::ml::EM::create();
    model->setClustersNumber(static_cast<int>(m_numberClusters));
    Mat logs, labels, probs;
    model->trainEM(mat, logs, labels, probs);
    for (std::int32_t i = 0; i < mat.rows; ++i) {
      const auto row = mat.row(i);
      Mat probs(1, static_cast<std::int32_t>(m_numberClusters), CV_64FC1);
      auto response = model->predict2(row, probs);
      indices.push_back(static_cast<std::int32_t>(cvRound(response[1])));
    }
  }
}

#include "sf_gnn.h"
