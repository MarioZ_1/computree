/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_GNN_H
#define SF_GNN_H

#include "pcl/cloud/sf_abstractCloud.h"

#include <pcl/features/fpfh.h>

class SF_Gnn : public SF_AbstractCloud<pcl::FPFHSignature33>
{
  std::uint32_t m_numberClusters;
  std::uint32_t m_dimension;

  void createIndices() override;
  void createIndex(pcl::FPFHSignature33, float) override;
  void reset() override;

public:
  SF_Gnn(pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloudIn);
  void setParameters(std::uint32_t numberClusters, std::uint32_t reducedDimension);
  void compute(std::vector<std::vector<float>> features);
};

#endif // SF_GNN_H
