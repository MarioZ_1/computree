/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "SF_GnnFpfh.h"

#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/ml.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>

void
SF_GNNFPFH::createIndices()
{
}

void
SF_GNNFPFH::createIndex(pcl::FPFHSignature33, float)
{
}

void
SF_GNNFPFH::reset()
{
}

SF_GNNFPFH::SF_GNNFPFH(pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloudIn) : SF_AbstractCloud<pcl::FPFHSignature33>(cloudIn) {}

void
SF_GNNFPFH::setParameters(uint32_t numberClusters, int32_t dimension)
{
  m_dimension = dimension;
  m_numberClusters = numberClusters;
}

void
SF_GNNFPFH::compute()
{
  pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloud = SF_AbstractCloud<pcl::FPFHSignature33>::m_cloudIn;
  std::vector<int>& indices = SF_AbstractCloud<pcl::FPFHSignature33>::_indices;
  indices.clear();
  cv::Mat mat(cloud->points.size(), 33, CV_32FC(1));
  Eigen::MatrixXf eigenMat(cloud->points.size(), 33);
  const auto size = cloud->points.size();
  for (std::uint32_t i = 0; i < size; ++i) {
    const pcl::FPFHSignature33& fpfh = cloud->at(i);
    for (std::uint32_t j = 0; j < 33u; ++j) {
      eigenMat(i, j) = fpfh.histogram[j];
    }
  }
  // Normalize
  for (int i = 0; i < eigenMat.cols(); i++) {
    eigenMat.col(i).normalize();
  }

  {
    // Mean centering data.
    auto featureMeans = eigenMat.colwise().mean().eval();
    Eigen::MatrixXf centered = eigenMat.rowwise() - featureMeans;

    // Compute the covariance matrix.
    Eigen::MatrixXf cov = centered.adjoint() * centered;
    cov = cov / (eigenMat.rows() - 1);

    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eig(cov);
    // Normalize eigenvalues to make them represent percentages.
    Eigen::VectorXf normalizedEigenValues = eig.eigenvalues() / eig.eigenvalues().sum();

    // Get the two major eigenvectors and omit the others.
    Eigen::MatrixXf evecs = eig.eigenvectors();
    Eigen::MatrixXf pcaTransform = evecs.rightCols(m_dimension);

    // Map the dataset in the new two dimensional space.
    eigenMat = eigenMat * pcaTransform;
  }

  // Normalize again as we reduced dimensionality
  for (int i = 0; i < eigenMat.cols(); i++) {
    eigenMat.col(i).normalize();
  }

  for (std::uint32_t i = 0; i < size; ++i) {
    const pcl::FPFHSignature33& fpfh = cloud->at(i);
    for (std::uint32_t j = 0; j < static_cast<std::uint32_t>(m_dimension); ++j) {
      mat.at<float>(i, j) = eigenMat(i, j);
    }
  }
  {
    using namespace cv;
    using namespace cv::ml;
    Ptr<EM> model = EM::create();
    model->setClustersNumber(static_cast<int>(m_numberClusters));
    Mat logs, labels, probs;
    model->trainEM(mat, logs, labels, probs);

    for (std::uint32_t i = 0; i < size; ++i) {
      const auto row = mat.row(i);
      Mat probs(1, m_dimension, CV_64FC1);
      auto response = model->predict2(row, probs);
      indices.push_back(static_cast<std::uint32_t>(cvRound(response[1])));
    }
  }
}
