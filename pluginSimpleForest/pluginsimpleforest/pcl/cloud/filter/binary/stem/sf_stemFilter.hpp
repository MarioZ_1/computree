/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEM_FILTER_HPP
#define SF_STEM_FILTER_HPP

#include <pcl/segmentation/extract_clusters.h>

#include "ct_colorcloud/ct_colorcloudstdvector.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "pcl/cloud/feature/growth_direction/sf_growthDirection.h"
#include "pcl/cloud/filter/binary/stem/sf_stemFilter.h"
#include "pcl/cloud/segmentation/dijkstra/sf_dijkstra.h"

template<typename PointType>
void
SF_StemFilter<PointType>::transferStem(const SF_ParamStemFilter<PointType>& params,
                                       typename pcl::PointCloud<PointType>::Ptr cloudWithGrowthDirection)
{
  if (cloudWithGrowthDirection->empty()) {
    *SF_StemFilter<PointType>::_cloudOutFilteredNoise = *SF_StemFilter<PointType>::m_cloudIn;
    return;
  }
  //  _colors = new CT_ColorCloudStdVector(SF_StemFilter<PointType>::_cloudIn->points.size());
  float minZ = 10000000;
  float maxZ = -100000;
  for (size_t i = 0; i < SF_StemFilter<PointType>::m_cloudIn->points.size(); i++) {
    PointType p = SF_StemFilter<PointType>::m_cloudIn->points.at(i);
    if (p.z > maxZ)
      maxZ = p.z;
    if (p.z < minZ)
      minZ = p.z;
  }
  pcl::KdTreeFLANN<PointType> kdtree;
  kdtree.setInputCloud(cloudWithGrowthDirection);
  for (size_t i = 0; i < SF_StemFilter<PointType>::m_cloudIn->points.size(); i++) {
    //    CT_Color& col = _colors->colorAt(i);
    PointType p = SF_StemFilter<PointType>::m_cloudIn->points.at(i);
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    if (kdtree.nearestKSearch(p, 1, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0) {
      PointType gdPoint = cloudWithGrowthDirection->points[pointIdxRadiusSearch[0]];
      Eigen::Vector3f vec(gdPoint.normal_x, gdPoint.normal_y, gdPoint.normal_z);
      if (gdPoint.normal_z < 0) {
        vec = Eigen::Vector3f(-gdPoint.normal_x, -gdPoint.normal_y, -gdPoint.normal_z);
      }
      Eigen::Vector3d axis1;
      axis1[0] = params._x;
      axis1[1] = params._y;
      axis1[2] = params._z;
      Eigen::Vector3d axis2;
      axis2[0] = gdPoint.normal_x;
      axis2[1] = gdPoint.normal_y;
      axis2[2] = gdPoint.normal_z;
      double deg = SF_Math<double>::getAngleBetweenDeg(axis1, axis2);
      if (deg < params._angle || deg > (180 - params._angle)) {
        SF_StemFilter<PointType>::_cloudOutFiltered->points.push_back(p);
      } else {
        SF_StemFilter<PointType>::_cloudOutFilteredNoise->points.push_back(p);
      }
    }
  }
}

template<typename PointType>
void
SF_StemFilter<PointType>::setParams(SF_ParamStemFilter<PointType>& params)
{
  _params = params;
}

template<typename PointType>
typename pcl::PointCloud<PointType>::Ptr
SF_StemFilter<PointType>::lowestSlice(typename pcl::PointCloud<PointType>::Ptr cloud)
{
  typename pcl::PointCloud<PointType>::Ptr slice(new pcl::PointCloud<PointType>);
  float minZ = std::numeric_limits<float>::max();
  for (auto point : cloud->points) {
    if (point.z < minZ)
      minZ = point.z;
  }
  for (auto point : cloud->points) {
    if (point.z < minZ + 0.1) {
      point.intensity = 0;
      slice->push_back(point);
    }
  }
  return slice;
}

template<typename PointType>
void
SF_StemFilter<PointType>::compute()
{
  SF_StemFilter<PointType>::_cloudOutFilteredNoise.reset(new typename pcl::PointCloud<PointType>);
  SF_StemFilter<PointType>::_cloudOutFiltered.reset(new typename pcl::PointCloud<PointType>);
  typename pcl::PointCloud<PointType>::Ptr downScaledCloud(new typename pcl::PointCloud<PointType>);
  typename pcl::PointCloud<PointType>::Ptr processedCloud(new typename pcl::PointCloud<PointType>);
  pcl::VoxelGrid<SF_PointNormal> sor;
  sor.setInputCloud(SF_StemFilter<PointType>::m_cloudIn);
  {
    sor.setLeafSize(_params._voxelSize, _params._voxelSize, _params._voxelSize);
  }
  sor.filter(*downScaledCloud);

  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>);
  tree->setInputCloud(downScaledCloud);
  std::vector<pcl::PointIndices> clusterIndices;
  pcl::EuclideanClusterExtraction<PointType> ec;
  ec.setClusterTolerance(_params._voxelSize * 2.5);
  ec.setMinClusterSize(2);
  ec.setMaxClusterSize(std::numeric_limits<int>::max());
  ec.setSearchMethod(tree);
  ec.setInputCloud(downScaledCloud);
  ec.extract(clusterIndices);

  for (std::vector<pcl::PointIndices>::const_iterator it = clusterIndices.begin(); it != clusterIndices.end(); ++it) {
    pcl::PointCloud<SF_PointNormal>::Ptr cloudCluster(new pcl::PointCloud<SF_PointNormal>);
    for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit) {
      cloudCluster->points.push_back(downScaledCloud->points[*pit]);
    }
    auto seeds = lowestSlice(cloudCluster);
    SF_Dijkstra(cloudCluster, seeds, _params._voxelSize * 2.5);
    *processedCloud += *cloudCluster;
  }

  transferStem(_params, processedCloud);
  SF_StemFilter<PointType>::createIndices();
}

template<typename PointType>
SF_StemFilter<PointType>::SF_StemFilter()
{
  Sf_AbstractBinaryFilter<PointType>::reset();
}

#endif // SF_STEM_FILTER_HPP
