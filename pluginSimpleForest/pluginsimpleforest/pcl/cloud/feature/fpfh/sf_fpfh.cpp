/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_fpfh.h"
#include "pcl/features/impl/fpfh_omp.hpp"

void
SF_FPFH::createIndices()
{
}

void
SF_FPFH::createIndex(SF_PointNormal, float)
{
}

void
SF_FPFH::reset()
{
  SF_AbstractFeature<SF_PointNormal, pcl::FPFHSignature33>::m_features.reset(new pcl::PointCloud<pcl::FPFHSignature33>);
}

SF_FPFH::SF_FPFH(SF_CloudNormal::Ptr cloudIn, pcl::PointCloud<pcl::FPFHSignature33>::Ptr featuresOut)
  : SF_AbstractFeature<SF_PointNormal, pcl::FPFHSignature33>(cloudIn, featuresOut)
{
  reset();
}

void
SF_FPFH::setParameters(float range, uint32_t numberOfThreads)
{
  m_rangePFH = range;
  m_numberOfThreads = numberOfThreads;
}

void
SF_FPFH::computeFeatures()
{
  SF_CloudNormal::Ptr cloud = SF_AbstractCloud<SF_PointNormal>::m_cloudIn;
  pcl::PointCloud<pcl::FPFHSignature33>::Ptr fpfhs = SF_AbstractFeature<SF_PointNormal, pcl::FPFHSignature33>::m_features;
  pcl::FPFHEstimationOMP<SF_PointNormal, SF_PointNormal, pcl::FPFHSignature33> fpfh;
  fpfh.setInputCloud(cloud);
  fpfh.setInputNormals(cloud);
  fpfh.setNumberOfThreads(m_numberOfThreads);
  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>);
  fpfh.setSearchMethod(tree);
  fpfh.setRadiusSearch(m_rangePFH);
  fpfh.compute(*fpfhs);
}
