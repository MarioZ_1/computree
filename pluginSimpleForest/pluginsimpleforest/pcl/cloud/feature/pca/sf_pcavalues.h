/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_PCAVALUES_H
#define SF_PCAVALUES_H

struct SF_PCAValues
{
  Eigen::Vector3f lambdas;
  inline void check_valid()
  {
    if (lambdas[0] > lambdas[1] || lambdas[0] > lambdas[2] || lambdas[1] > lambdas[2]) {
      throw std::runtime_error("Lambdas have not been computed during PCA.");
    }
  }

  inline float getLambda(const int i)
  {
    const auto sum = lambdas[0] + lambdas[1] + lambdas[2];
    if (sum != 0) {
      return (lambdas[i] / sum);
    }
    return 0;
  }

  inline float getLambda1()
  {
    check_valid();
    return getLambda(0);
  }

  inline float getLambda2()
  {
    check_valid();
    return getLambda(1);
  }

  inline float getLambda3()
  {
    check_valid();
    return getLambda(2);
  }

  inline std::vector<float> getDescription()
  {
    constexpr float min = 0.00001f;
    std::vector<float> description;
    const auto l1 = std::max(min, getLambda1());
    const auto l2 = std::max(min, getLambda2());
    const auto l3 = std::max(min, getLambda3());
    description.push_back(l1);
    description.push_back(l2);
    description.push_back(l3);
    description.push_back(l2 - l1);
    description.push_back(l3 - l2);
    description.push_back(l1 / l2);
    description.push_back(l1 / l3);
    description.push_back(l2 / l3);
    return description;
  }

  Eigen::Matrix3f vectors;

  inline Eigen::Vector3f getVector1()
  {
    check_valid();
    Eigen::Vector3f vec = vectors.col(0);
    vec.normalize();
    return vec;
  }

  inline Eigen::Vector3f getVector2()
  {
    check_valid();
    Eigen::Vector3f vec = vectors.col(1);
    vec.normalize();
    return vec;
  }

  inline Eigen::Vector3f getVector3()
  {
    check_valid();
    Eigen::Vector3f vec = vectors.col(2);
    vec.normalize();
    return vec;
  }
};
#endif // SF_PCAVALUES_H
