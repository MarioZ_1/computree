/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_NORMAL_HPP
#define SF_NORMAL_HPP

#include <pcl/features/normal_3d.h>

#include "sf_normal.h"

template<typename PointType, typename FeatureType>
SF_Normal<PointType, FeatureType>::SF_Normal(typename pcl::PointCloud<PointType>::Ptr cloudIn, pcl::PointCloud::Ptr featuresOut)
  : m_cloudIn(cloudIn), m_features(featuresOut)
{
}

template<typename PointType, typename FeatureType>
void
SF_Normal<PointType, FeatureType>::setParameters(const SF_ParamNormal& params)
{
  m_params = params;
}

template<typename PointType, typename FeatureType>
void
SF_Normal<PointType, FeatureType>::computeFeaturesRange()
{
  pcl::NormalEstimation<PointType, FeatureType> ne;
  ne.setInputCloud(m_cloudIn);
  pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>());
  ne.setSearchMethod(tree);
  ne.setRadiusSearch(m_params.m_radius);
  ne.compute(*SF_Normal<PointType, FeatureType>::m_features);
  ;
}

template<>
void
SF_Normal<SF_PointNormal, SF_PointNormal>::computeFeaturesRange()
{
  pcl::NormalEstimation<SF_PointNormal, SF_PointNormal> ne;
  ne.setInputCloud(m_cloudIn);
  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>());
  ne.setSearchMethod(tree);
  ne.setRadiusSearch(m_params.m_radius);
  ne.compute(*SF_Normal<SF_PointNormal, SF_PointNormal>::m_cloudIn);
  for (const auto& point : m_cloudIn->points) {
    SF_Normal<PointType, FeatureType>::m_features->points.emplace_back(point.normal);
  }
}

template<typename PointType, typename FeatureType>
void
SF_Normal<PointType, FeatureType>::computeFeaturesKnn()
{
  pcl::NormalEstimation<PointType, FeatureType> ne;
  ne.setInputCloud(m_cloudIn);
  pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>());
  ne.setSearchMethod(tree);
  ne.setKSearch(m_params.m_numberOfNeighbors);
  ne.compute(*SF_Normal<PointType, FeatureType>::m_features);
}

template<>
void
SF_Normal<SF_PointNormal, SF_PointNormal>::computeFeaturesKnn()
{
  pcl::NormalEstimation<SF_PointNormal, SF_PointNormal> ne;
  ne.setInputCloud(m_cloudIn);
  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>());
  ne.setSearchMethod(tree);
  ne.setKSearch(m_params.m_numberOfNeighbors);
  ne.compute(*SF_Normal<SF_PointNormal, SF_PointNormal>::m_cloudIn);
  for (const auto& point : m_cloudIn->points) {
    SF_Normal<PointType, FeatureType>::m_features->points.emplace_back(point.normal);
  }
}

template<typename PointType, typename FeatureType>
void
SF_Normal<PointType, FeatureType>::computeFeatures()
{
  SF_Normal<PointType, FeatureType>::m_features.reset(new pcl::PointCloud<FeatureType>);
  if (m_params.m_useRange) {
    computeFeaturesRange();
  } else {
    computeFeaturesKnn();
  }
}

#endif // SF_NORMAL_HPP
