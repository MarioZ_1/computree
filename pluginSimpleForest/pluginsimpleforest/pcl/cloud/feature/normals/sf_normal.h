/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_NORMAL_H
#define SF_NORMAL_H

#include "pcl/cloud/feature/sf_abstractFeature.h"
#include "sf_paramNormal.h"

template<typename PointType, typename FeatureType>
class SF_Normal : public SF_AbstractFeature<PointType, FeatureType>
{
private:
  SF_ParamNormal m_params;
  virtual void computeFeaturesRange();
  template<>
  virtual void SF_Normal<SF_PointNormal, SF_PointNormal>::computeFeaturesKnn();
  virtual void computeFeaturesKnn();
  template<>
  virtual void SF_Normal<SF_PointNormal, SF_PointNormal>::computeFeaturesRange();

public:
  SF_Normal(typename pcl::PointCloud<PointType>::Ptr cloudIn, typename pcl::PointCloud<FeatureType>::Ptr featuresOut);
  void computeFeatures() override;
  void setParameters(const SF_ParamNormal& params);
}

#include "sf_normal.hpp"

#endif // SF_NORMAL_H
