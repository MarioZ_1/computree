/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_importSfQsm.h"

#include "qsm/build/sf_qsmBuildTopology.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

std::shared_ptr<SF_ModelQSM>
SF_ImportSfQsm::import(const QString& path)
{
  m_parentIndices.clear();
  m_indices.clear();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
  QFile inputFile(path);
  if (inputFile.open(QIODevice::ReadOnly)) {
    QTextStream in(&inputFile);
    if (!in.atEnd()) {
      // process header
      initIndices(in.readLine());
    }
    while (!in.atEnd()) {
      QString line = in.readLine();
      bricks.push_back(createBrick(line));
    }
    inputFile.close();
  }
  SF_QSMBuildTopology topologyBuilder = SF_QSMBuildTopology(bricks, m_parentIndices, m_indices);
  topologyBuilder.compute();
  auto qsm = topologyBuilder.qsm();
  qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_LENGTH);
  return qsm;
}

void
SF_ImportSfQsm::initIndices(const QString& header)
{
  QStringList list = header.split(", ", QString::SkipEmptyParts);
  m_indexStartX = list.indexOf("startX");
  m_indexStartY = list.indexOf("startY");
  m_indexStartZ = list.indexOf("startZ");
  m_indexEndX = list.indexOf("endX");
  m_indexEndY = list.indexOf("endY");
  m_indexEndZ = list.indexOf("endZ");
  // Unsure why +1 is needed, but it is :(.
  m_indexTranslateX = list.indexOf("translateX") + 1;
  m_indexTranslateY = list.indexOf("translateY") + 1;
  m_indexTranslateZ = list.indexOf("translatez") + 1;
  m_indexRadius = list.indexOf("radius");
  m_indexFittingType = list.indexOf("FittingType");
  m_indexIndex = list.indexOf("ID");
  m_indexParentIndex = list.indexOf("parentID");
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_ImportSfQsm::createBrick(const QString& line)
{
  QStringList list = line.split(", ", QString::SkipEmptyParts);
  if (list.size() < 10) {
    QString errorMessage = "[SF_ImportSfQsm] : line has not the correct size: \n";
    errorMessage.append(list.join(" , "));
    throw std::runtime_error(errorMessage.toStdString());
  } else {
    Eigen::Vector3d translation;
    translation[0] = list[m_indexTranslateX].toDouble();
    translation[1] = list[m_indexTranslateY].toDouble();
    translation[2] = list[m_indexTranslateZ].toDouble();

    Eigen::Vector3d start;
    start[0] = list[m_indexStartX].toDouble();
    start[1] = list[m_indexStartY].toDouble();
    start[2] = list[m_indexStartZ].toDouble();
    start -= translation;

    Eigen::Vector3d end;
    end[0] = list[m_indexEndX].toDouble();
    end[1] = list[m_indexEndY].toDouble();
    end[2] = list[m_indexEndZ].toDouble();
    end -= translation;

    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick);
    double radius = list[m_indexRadius].toDouble();
    std::string fittingTypeString = list[m_indexFittingType].toStdString();
    FittingType fittingType = cylinder->getFittingTypeFromString(fittingTypeString);
    if (fittingTypeString != cylinder->getStringFromFittingType(fittingType)) {
      QString errorMessage = "[SF_ImportSfQsm] : somthing went wrong with converting the FittingTypeStrng: \n";
      errorMessage.append(QString::fromStdString(fittingTypeString));
      throw std::runtime_error(errorMessage.toStdString());
    }
    cylinder->setStartEndRadius(start, end, radius, fittingType);
    m_indices.push_back(list[m_indexIndex].toInt());
    m_parentIndices.push_back(list[m_indexParentIndex].toInt());
    return cylinder;
  }
}
