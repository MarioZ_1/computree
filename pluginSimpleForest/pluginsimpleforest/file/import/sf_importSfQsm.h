/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_IMPORTSFQSM_H
#define SF_IMPORTSFQSM_H

#include "file/import/sf_abstractImportQsm.h"

class SF_ImportSfQsm : public SF_AbstractImportQsm
{
private:
  std::uint32_t m_indexStartX = 3;
  std::uint32_t m_indexStartY = 4;
  std::uint32_t m_indexStartZ = 5;

  std::uint32_t m_indexEndX = 6;
  std::uint32_t m_indexEndY = 7;
  std::uint32_t m_indexEndZ = 8;

  std::uint32_t m_indexTranslateX = 6;
  std::uint32_t m_indexTranslateY = 7;
  std::uint32_t m_indexTranslateZ = 8;

  std::uint32_t m_indexRadius = 9;
  std::uint32_t m_indexFittingType = 16;

  std::uint32_t m_indexIndex = 1;
  std::uint32_t m_indexParentIndex = 2;

  std::vector<std::int32_t> m_parentIndices;
  std::vector<std::int32_t> m_indices;

  void initIndices(const QString& header);
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> createBrick(const QString& line);

public:
  SF_ImportSfQsm() = default;
  std::shared_ptr<SF_ModelQSM> import(const QString& path) override;
};

#endif // SF_IMPORTSFQSM_H
