/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_exportDtm.h"

int
SF_ExportDTM::getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  return m_dtm->colDim() * m_dtm->linDim();
}

int
SF_ExportDTM::getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  return (m_dtm->colDim() - 1) * (m_dtm->linDim() - 1) * 2;
}

void
SF_ExportDTM::writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  size_t cols = m_dtm->xArraySize();
  size_t rows = m_dtm->yArraySize();
  for (size_t r = 0; r < rows - 1; ++r) {
    for (size_t c = 0; c < cols - 1; ++c) {
      size_t index11{}, index12{}, index21{}, index22{};
      m_dtm->index(c, r, index11);
      m_dtm->index(c, r + 1, index12);
      m_dtm->index(c + 1, r, index21);
      m_dtm->index(c + 1, r + 1, index22);
      outStream << "3 ";
      outStream << QString::number(index11);
      outStream << " ";
      outStream << QString::number(index12);
      outStream << " ";
      outStream << QString::number(index22);
      outStream << "\n";

      outStream << "3 ";
      outStream << QString::number(index22);
      outStream << " ";
      outStream << QString::number(index21);
      outStream << " ";
      outStream << QString::number(index11);
      outStream << "\n";
    }
  }
}

void
SF_ExportDTM::exportDTM(const QString& path,
                        const QString& qsmName,
                        CT_Image2D<float>* dtm,
                        const Eigen::Vector3d& translation,
                        SF_ExportPlyPolicy exportPolicy)
{
  m_dtm = dtm;
  m_translation = translation;
  m_exportPolicy = exportPolicy;
  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  QString fullPath = getFullPath(path);
  QDir dir(fullPath);
  if (!dir.exists())
    dir.mkpath(".");
  QString fullName = getFullName(qsmName);
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
  fullPath.append(fullName);
  QFile file(fullPath);
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream outStream(&file);
    writeHeader(outStream, bricks);
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = dtmToCloud();
    writeCloud(outStream, cloud);
    writeFaces(outStream, bricks);
    file.close();
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr
SF_ExportDTM::dtmToCloud()
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
  size_t cols = m_dtm->xArraySize();
  size_t rows = m_dtm->yArraySize();
  for (size_t r = 0; r < rows; ++r) {
    for (size_t c = 0; c < cols; ++c) {
      pcl::PointXYZI point;
      Eigen::Vector3d center;
      m_dtm->getCellCenterCoordinates(c, r, center);
      center = center + m_translation;
      point.x = center[0];
      point.y = center[1];
      point.z = m_dtm->value(c, r) + m_translation[2];
      cloud->push_back(point);
    }
  }
  return cloud;
}

SF_ExportDTM::SF_ExportDTM()
{
  m_firstColor = SF_ColorFactory::Color::BROWN;
  m_secondColor = SF_ColorFactory::Color::BROWN;
}
