/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_exportPlySmoothed.h"

int
SF_ExportPlySmoothed::getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  int sum = 0;
  for (const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& branch : m_branchStructure) {
    sum += 2;
    sum += branch.size() * m_resolution + m_resolution;
  }
  return sum;
}

int
SF_ExportPlySmoothed::getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  int sum = 0;
  for (const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& branch : m_branchStructure) {
    sum += 2 * m_resolution;
    sum += (branch.size() * m_resolution) * 2;
  }
  return sum;
}

void
SF_ExportPlySmoothed::writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  size_t currentIndex = 0;
  for (int i = 0; i < static_cast<int>(m_branchStructure.size()); i++) {
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> branch = m_branchStructure.at(i);
    writeFacesEnd(outStream, currentIndex, currentIndex + 1, false);
    currentIndex++;
    auto superIndex = currentIndex;
    for (int x = 0; x < static_cast<int>(branch.size()); x++) {
      for (int j = 0; j < m_resolution; j++) {
        outStream << "3 ";
        outStream << QString::number(currentIndex + x * m_resolution + j % m_resolution);
        outStream << " ";
        outStream << QString::number(currentIndex + x * m_resolution + (j + 1) % m_resolution);
        outStream << " ";
        outStream << QString::number(currentIndex + (x + 1) * m_resolution + (j + 1) % m_resolution);
        outStream << "\n";

        outStream << "3 ";
        outStream << QString::number(currentIndex + (x + 1) * m_resolution + (j + 1) % m_resolution);
        outStream << " ";
        outStream << QString::number(currentIndex + (x + 1) * m_resolution + j % m_resolution);
        outStream << " ";
        outStream << QString::number(currentIndex + x * m_resolution + j % m_resolution);
        outStream << "\n";
      }
      superIndex += m_resolution;
    }
    writeFacesEnd(outStream, superIndex + m_resolution, superIndex, true);
    superIndex += m_resolution;
    currentIndex = superIndex + 1;
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr
SF_ExportPlySmoothed::bricksToCloud(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
  for (auto& branch : m_branchStructure) {
    auto startEigen = branch.front()->getStart();
    pcl::PointXYZI start;
    start.x = startEigen[0];
    start.y = startEigen[1];
    start.z = startEigen[2];
    auto endEigen = branch.back()->getEnd();
    pcl::PointXYZI end;
    end.x = endEigen[0];
    end.y = endEigen[1];
    end.z = endEigen[2];
    for (size_t i = 0; i < branch.size(); i++) {
      std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick = branch.at(i);
      pcl::PointCloud<pcl::PointXYZI>::Ptr brickCloud = brickToCloud(brick);
      if (i == 0) {
        start.intensity = brickCloud->points.at(0).intensity;
        cloud->push_back(start);
        for (int k = 0; k < m_resolution; k++) {
          cloud->push_back(brickCloud->points.at(k));
        }
      }
      if (i == branch.size() - 1) {
        for (int k = 0; k < m_resolution; k++) {
          cloud->push_back(brickCloud->points.at(m_resolution + k));
        }
        end.intensity = brickCloud->points.at(0).intensity;
        cloud->push_back(end);
      } else {
        std::shared_ptr<Sf_ModelAbstractBuildingbrick> brickChild = branch.at(i + 1);
        pcl::PointCloud<pcl::PointXYZI>::Ptr childCloud = brickToCloud(brickChild);
        for (int k = 0; k < m_resolution; k++) {
          pcl::PointXYZI parent = brickCloud->points.at(m_resolution + k);
          pcl::PointXYZI child = childCloud->points.at(k);
          pcl::PointXYZI merge;
          merge.x = (parent.x + child.x) / 2;
          merge.y = (parent.y + child.y) / 2;
          merge.z = (parent.z + child.z) / 2;
          merge.intensity = (parent.intensity + child.intensity) / 2;
          cloud->push_back(merge);
        }
      }
    }
  }
  return cloud;
}

void
SF_ExportPlySmoothed::SF_ExportPlySmoothed::writeFacesEnd(QTextStream& outStream,
                                                          size_t centerIndex,
                                                          size_t firstHullIndex,
                                                          bool reverse)
{
  for (int j = 0; j < m_resolution; j++) {
    outStream << "3 ";
    outStream << QString::number(centerIndex);
    outStream << " ";
    if (!reverse) {
      outStream << QString::number(firstHullIndex + j % m_resolution);
      outStream << " ";
      outStream << QString::number(firstHullIndex + (j + 1) % m_resolution);
    } else {
      outStream << QString::number(firstHullIndex + (j + 1) % m_resolution);
      outStream << " ";
      outStream << QString::number(firstHullIndex + j % m_resolution);
    }
    outStream << "\n";
  }
  outStream << "\n";
}

SF_ExportPlySmoothed::SF_ExportPlySmoothed() : SF_AbstractExportPly() {}

void
SF_ExportPlySmoothed::exportQSM(QString path,
                                QString qsmName,
                                std::shared_ptr<SF_ModelQSM> qsm,
                                SF_ExportPlyPolicy exportPolicy,
                                int resolution)
{
  m_qsm = qsm;
  m_exportPolicy = exportPolicy;
  m_resolution = resolution;
  m_branchStructure = m_qsm->getBranchBuildingBricks();
  auto bricks = m_qsm->getBuildingBricks();
  getMinMax(bricks);
  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  QString fullPath = getFullPath(path);
  QDir dir(fullPath);
  if (!dir.exists())
    dir.mkpath(".");
  QString fullName = getFullName(qsmName);
  fullPath.append(fullName);
  QFile file(fullPath);
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream outStream(&file);
    writeHeader(outStream, bricks);
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
    writeCloud(outStream, cloud);
    writeFaces(outStream, bricks);
    file.close();
  }
}
