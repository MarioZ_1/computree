/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "themewidgetstem.h"

#include <QtCharts/QAbstractBarSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QPercentBarSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCore/QTime>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

#include <pcl/sf_math.h>

ThemeWidgetStem::ThemeWidgetStem(std::shared_ptr<SF_ModelQSM> qsm, QWidget* parent)
  : QWidget(parent)
  , m_qsm(qsm)
  , m_listCount(3)
  , m_valueMax(10)
  , m_valueCount(7)
  , m_dataTable(generateRandomData(m_listCount, m_valueMax, m_valueCount))
{
  connectSignals();
  // create layout
  QGridLayout* baseLayout = new QGridLayout();
  QHBoxLayout* settingsLayout = new QHBoxLayout();
  QLabel* warning = new QLabel;
  warning->setStyleSheet("font-weight: bold; color: black");
  warning->setText("Stem quality");
  settingsLayout->addWidget(warning);
  settingsLayout->addWidget(new QLabel(" - by Dr. Jan Hackenberg - www.simpleForest.org:"));
  settingsLayout->addStretch();
  baseLayout->addLayout(settingsLayout, 0, 0, 1, 3);

  // create charts

  QChartView* chartView;

  chartView = new QChartView(createScatterChartStemTaperGood());
  baseLayout->addWidget(chartView, 1, 0);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartStemTaper());
  baseLayout->addWidget(chartView, 1, 1);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartBranchHeight());
  baseLayout->addWidget(chartView, 2, 0);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartBranchHeightBin());
  baseLayout->addWidget(chartView, 2, 1);
  m_charts << chartView;

  chartView = new QChartView(createStemAngle());
  baseLayout->addWidget(chartView, 3, 0);
  m_charts << chartView;

  //  chartView = new QChartView(createScatterChartGrowthLengthAll());
  //  baseLayout->addWidget(chartView, 3, 1);
  //  m_charts << chartView;

  setLayout(baseLayout);

  // Set defaults
  updateUI();
}

ThemeWidgetStem::~ThemeWidgetStem() {}

void
ThemeWidgetStem::connectSignals()
{
}

DataTable
ThemeWidgetStem::generateRandomData(int listCount, int valueMax, int valueCount) const
{
  DataTable dataTable;
  auto minZ = m_qsm->getRootBuildingBrick()->getStart()[2];
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks = m_qsm->getBuildingBricks();
  auto maxIt = std::max_element(
    bricks.begin(), bricks.end(), [](auto brick1, auto brick2) { return brick1->getEnd()[2] < brick2->getEnd()[2]; });
  auto maxZ = (*maxIt)->getEnd()[2];
  auto height = maxZ - minZ;

  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getGoodQuality()) {
        if (brick->getSegment()->getBranchOrder() != 0) {
          continue;
        }
        QPointF value(brick->getStart()[2] - minZ, brick->getRadius());
        QString label = "Stem taper Good Quality";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getSegment()->getBranchOrder() != 0) {
        continue;
      }
      QPointF value(brick->getStart()[2] - minZ, brick->getRadius());
      QString label = "Stem taper";
      dataList << Data(value, label);
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    QString label = "Branch Height";
    QPointF value(0.f, 0.f);
    dataList << Data(value, label);
    for (auto& brick : bricks) {
      if (brick->getSegment()->getBranchOrder() != 1) {
        continue;
      }
      if (brick->getParent()->getSegment()->getBranchOrder() == 0) {
        QPointF value(brick->getStart()[2] - minZ, brick->getGrowthVolume());
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    QString label = "Branch Height Heistogram";
    for (std::int32_t i = 0; i <= static_cast<std::int32_t>(height * 2); ++i) {
      QPointF value(static_cast<float>(i) / 2.f, 0.f);
      dataList << Data(value, label);
    }
    for (auto& brick : bricks) {
      if (brick->getSegment()->getBranchOrder() != 1) {
        continue;
      }
      if (brick->getParent()->getSegment()->getBranchOrder() == 0) {
        auto h = brick->getEnd()[2] - minZ;
        auto index = static_cast<std::int32_t>(h * 2.f);
        index = std::min(dataList.size() - 1, index);
        QPointF& value = dataList[index].first;
        value.setY(value.y() + brick->getGrowthVolume());
      }
    }
    dataTable << dataList;
  }

  {
    DataList dataList;
    QString label = "Stem Angle Heistogram";
    for (std::int32_t i = 0; i <= 12; ++i) {
      QPointF value(i * 5.f, 0.f);
      dataList << Data(value, label);
    }
    Eigen::Vector3d zAxis(0, 0, 1);
    for (auto& brick : bricks) {
      if (brick->getSegment()->getBranchOrder() != 0) {
        continue;
      }
      auto angle = SF_Math<float>::getAngleBetweenDeg(zAxis, brick->getAxis());
      angle = std::min(angle, 59.99f);
      auto index = static_cast<std::int32_t>(angle / 5.f);
      index = std::min(dataList.size() - 1, index);
      QPointF& value = dataList[index].first;
      value.setY(value.y() + brick->getVolume());
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getGoodQuality()) {
        QPointF value(brick->getGrowthLength(), brick->getRadius());
        QString label = "GrowthLength Good Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (!brick->getGoodQuality()) {
        QPointF value(brick->getGrowthLength(), brick->getRadius());
        QString label = "GrowthLength Bad Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  return dataTable;
}

QChart*
ThemeWidgetStem::createAreaChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Area chart");

  // The lower series initialized to zero values
  QLineSeries* lowerSeries = 0;
  QString name("Series ");
  int nameIndex = 0;
  for (int i(0); i < m_dataTable.count(); i++) {
    QLineSeries* upperSeries = new QLineSeries(chart);
    for (int j(0); j < m_dataTable[i].count(); j++) {
      Data data = m_dataTable[i].at(j);
      if (lowerSeries) {
        const QVector<QPointF>& points = lowerSeries->pointsVector();
        upperSeries->append(QPointF(j, points[i].y() + data.first.y()));
      } else {
        upperSeries->append(QPointF(j, data.first.y()));
      }
    }
    QAreaSeries* area = new QAreaSeries(upperSeries, lowerSeries);
    area->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(area);
    chart->createDefaultAxes();
    lowerSeries = upperSeries;
  }

  return chart;
}

QChart*
ThemeWidgetStem::createBarChart(int valueCount) const
{
  Q_UNUSED(valueCount);
  QChart* chart = new QChart();
  chart->setTitle("Bar chart");

  QStackedBarSeries* series = new QStackedBarSeries(chart);
  for (int i(0); i < m_dataTable.count(); i++) {
    QBarSet* set = new QBarSet("Bar set " + QString::number(i));
    for (const Data& data : m_dataTable[i])
      *set << data.first.y();
    series->append(set);
  }
  chart->addSeries(series);
  chart->createDefaultAxes();

  return chart;
}

QChart*
ThemeWidgetStem::createLineChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Line chart");

  QString name("Series ");
  int nameIndex = 0;
  for (const DataList& list : m_dataTable) {
    QLineSeries* series = new QLineSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(series);
  }
  chart->createDefaultAxes();

  return chart;
}

QChart*
ThemeWidgetStem::createPieChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Pie chart");

  qreal pieSize = 1.0 / m_dataTable.count();
  for (int i = 0; i < m_dataTable.count(); i++) {
    QPieSeries* series = new QPieSeries(chart);
    for (const Data& data : m_dataTable[i]) {
      QPieSlice* slice = series->append(data.second, data.first.y());
      if (data == m_dataTable[i].first()) {
        slice->setLabelVisible();
        slice->setExploded();
      }
    }
    qreal hPos = (pieSize / 2) + (i / (qreal)m_dataTable.count());
    series->setPieSize(pieSize);
    series->setHorizontalPosition(hPos);
    series->setVerticalPosition(0.5);
    chart->addSeries(series);
  }

  return chart;
}

QChart*
ThemeWidgetStem::createSplineChart() const
{
  // spine chart
  QChart* chart = new QChart();
  chart->setTitle("Spline chart");
  QString name("Series ");
  int nameIndex = 0;
  for (const DataList& list : m_dataTable) {
    QSplineSeries* series = new QSplineSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  return chart;
}

QChart*
ThemeWidgetStem::createStemAngle() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("Stem Angle to zAxis");
  const DataList& list = m_dataTable[4];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  series->setBorderColor(series->color());
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Volume");
  chart->axisX()->setTitleText("Angle");
  return chart;
}

QChart*
ThemeWidgetStem::createScatterChartGrowthVolumeAll() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("GrowthVolume");
  {
    const DataList& list = m_dataTable[2];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Good Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  {
    const DataList& list = m_dataTable[3];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  return chart;
}

QChart*
ThemeWidgetStem::createScatterChartStemTaperGood() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("Stem Taper Good Cylinders");
  const DataList& list = m_dataTable[0];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  series->setBorderColor(series->color());
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("Height");
  return chart;
}

QChart*
ThemeWidgetStem::createScatterChartBranchHeightBin() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("Branch HeightBin Distribution");
  {
    const DataList& list = m_dataTable[3];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    series->setPen(QPen(Qt::PenStyle::NoPen));
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Volume Branch");
  chart->axisX()->setTitleText("Height Branch");
  return chart;
}

QChart*
ThemeWidgetStem::createScatterChartStemTaper() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("Stem Taper All Cylinders");
  const DataList& list = m_dataTable[1];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  series->setBorderColor(series->color());
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("Height");
  return chart;
}

QChart*
ThemeWidgetStem::createScatterChartBranchHeight() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("Branch Height Distribution");
  {
    const DataList& list = m_dataTable[2];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    series->setPen(QPen(Qt::PenStyle::NoPen));
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Volume Branch");
  chart->axisX()->setTitleText("Height Branch");
  return chart;
}

void
ThemeWidgetStem::updateUI()
{
  QChart::ChartTheme theme(QChart::ChartThemeBrownSand);

  const auto charts = m_charts;
  if (m_charts.at(0)->chart()->theme() != theme) {
    for (QChartView* chartView : charts) {
      chartView->chart()->setTheme(QChart::ChartThemeBrownSand);
      constexpr bool checked = true;
      chartView->setRenderHint(QPainter::Antialiasing, checked);
      chartView->chart()->setAnimationOptions(QChart::NoAnimation);
      chartView->chart()->legend()->setAlignment(Qt::AlignLeft);
      chartView->chart()->legend()->hide();
      chartView->chart()->setDropShadowEnabled(false);
      QList<QAbstractSeries*> allSeries = chartView->chart()->series();
      for (auto series : allSeries) {
        reinterpret_cast<QScatterSeries*>(series)->setPen(QPen(Qt::PenStyle::NoPen));
      }
    }

    QPalette pal = window()->palette();
    pal.setColor(QPalette::Window, QRgb(0x9e8965));
    pal.setColor(QPalette::WindowText, QRgb(0x404044));
    window()->setPalette(pal);
  }
}
