/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMFITQUALITY_HPP
#define SF_QSMFITQUALITY_HPP

#include "sf_qsmFitQuality.h"

#include "pcl/sf_math.h"

template<typename PointType>
SF_QsmFitQuality<PointType>::SF_QsmFitQuality(std::shared_ptr<SF_ModelQSM> qsm,
                                              SF_CloudToModelDistanceParameters& params,
                                              typename pcl::PointCloud<PointType>::Ptr cloud)
  : SF_AllocatePointsToSkeleton<PointType>(qsm, params, cloud)
{
  compute();
}

template<typename PointType>
void
SF_QsmFitQuality<PointType>::compute()
{
  constexpr float maxDistance = 0.2f;
  if (SF_AllocatePointsToSkeleton<PointType>::m_buildingBricks.size() !=
      SF_AllocatePointsToSkeleton<PointType>::m_brickClusters.size()) {
    throw std::runtime_error("[SF_QsmFitQuality] went wrong.");
  }

  std::vector<double> distancesCylinders;
  for (std::uint32_t i = 0; i < SF_AllocatePointsToSkeleton<PointType>::m_buildingBricks.size(); ++i) {
    const auto brick = SF_AllocatePointsToSkeleton<PointType>::m_buildingBricks[i];
    const auto cloud = SF_AllocatePointsToSkeleton<PointType>::m_brickClusters[i];
    std::vector<double> distances;
    distances.reserve(cloud->points.size());
    std::transform(cloud->points.cbegin(), cloud->points.cend(), std::back_inserter(distances), [&brick](const auto& point) {
      const Eigen::Vector3d p(static_cast<double>(point.x), static_cast<double>(point.y), static_cast<double>(point.z));
      return brick->getDistance(p);
    });
    constexpr std::uint32_t minNumberOfPointsForCylinderFit = 10u;
    const auto averageDistance = distances.size() < minNumberOfPointsForCylinderFit ? maxDistance
                                                                                    : SF_Math<double>::getMean(distances);
    if (averageDistance > 0. && averageDistance < maxDistance) {
      brick->setMean(averageDistance);
      distancesCylinders.push_back(averageDistance);
    } else {
      brick->setMean(maxDistance);
    }
  }
  constexpr std::uint32_t iterations = 5;
  constexpr double multiplier = 3.0;
  auto filteredDistances = SF_Math<double>::statisticalOutlierFilter(distancesCylinders, iterations, multiplier);
  const auto maxError = SF_Math<double>::getMedian(filteredDistances);
  for (auto brick : SF_AllocatePointsToSkeleton<PointType>::m_buildingBricks) {
    brick->setMaxFitQualityError(maxError);
  }
}

#endif // SF_QSMFITQUALITY_HPP
