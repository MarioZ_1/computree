/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_QSMAllometricCorrectionParameterEstimation.h"

#include "math/fit/power/sf_fitgnpower.h"
#include "pcl/sf_math.h"

void
SF_QSMAllometricCorrectionParameterEstimation::setParams(const SF_ParamAllometricCorrectionNeighboring& params)
{
  m_params = params;
}

SF_ParamAllometricCorrectionNeighboring
SF_QSMAllometricCorrectionParameterEstimation::params() const
{
  return m_params;
}

SF_QSMAllometricCorrectionParameterEstimation::SF_QSMAllometricCorrectionParameterEstimation() {}

bool
SF_QSMAllometricCorrectionParameterEstimation::isUnCorrectedRadiusFit(
  const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& buildingBrick) const
{
  return buildingBrick && buildingBrick->isMeasuredRadius();
}

float
SF_QSMAllometricCorrectionParameterEstimation::getA(const std::vector<float>& x, const std::vector<float>& y, float b) const
{
  std::vector<float> bValues(x.size());
  for (size_t i = 0; i < x.size(); i++) {
    bValues[i] = y[i] / std::pow(x[i], b);
  }
  return SF_Math<float>::getMedian(bValues);
}

void
SF_QSMAllometricCorrectionParameterEstimation::getTwigRadius(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks)
{
  auto setRadius = [&]() {
    if (m_params.m_useGrowthLength) {
      m_params._qsm->setC(m_twigRadius);
    } else {
      m_params._qsm->setC(m_twigRadius);
    }
  };
  if (!m_params.m_estimateMinRadius) {
    m_twigRadius = 0;
    setRadius();
    return;
  }
  if (bricks.empty()) {
    m_twigRadius = m_params._minRadius;
    setRadius();
    return;
  }
  std::vector<double> radii;
  auto getArea = [](double radius) -> double { return radius * radius * SF_Math<double>::_PI; };
  auto getRadius = [](double area) -> double { return std::sqrt(area / SF_Math<double>::_PI); };
  std::transform(
    bricks.begin(), bricks.end(), std::back_inserter(radii), [&](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) -> double {
      const auto numberTwigs = brick->getSegment()->getRBOPA();
      if (numberTwigs == 0) {
        return m_twigRadius;
      }
      const auto brickArea = getArea(brick->getSegment()->getRadius());
      return getRadius(brickArea / static_cast<double>(numberTwigs));
    });
  std::sort(radii.begin(), radii.end());
  auto last = forestUnique(radii.begin(), radii.end());
  radii.erase(last, radii.end());
  m_twigRadius = SF_Math<double>::getMean(radii);
  m_twigRadius = std::max(m_twigRadius, static_cast<float>(m_params._minRadius));
  setRadius();
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMAllometricCorrectionParameterEstimation::chooseBestBricks(
  const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks) const
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingbricks;
  for (auto brick : bricks) {
    if (brick->getGoodQuality() && !brick->isLowerStem()) {
      buildingbricks.push_back(brick);
    }
  }
  return buildingbricks;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMAllometricCorrectionParameterEstimation::removeStem(
  const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks) const
{
  auto crownStart = m_params._qsm->crownStartBrick();
  float height = crownStart->getCenter()[2];

  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> noStemBricks;
  std::for_each(bricks.begin(), bricks.end(), [&height, &noStemBricks](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) {
    auto cylinderHeight = brick->getCenter()[2];
    if (cylinderHeight > height || brick->getSegment()->getBranchOrder() != 0) {
      noStemBricks.push_back(brick);
    }
  });
  return noStemBricks;
}

void
SF_QSMAllometricCorrectionParameterEstimation::compute()
{
  std::vector<float> y;
  std::vector<float> x;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bestBricks = chooseBestBricks(m_params._qsm->getBuildingBricks());
  getTwigRadius(bestBricks);

  std::transform(bestBricks.begin(),
                 bestBricks.end(),
                 std::back_inserter(y),
                 [rad = m_twigRadius](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getRadius() - rad; });
  if (m_params.m_useGrowthLength) {
    std::transform(bestBricks.begin(),
                   bestBricks.end(),
                   std::back_inserter(x),
                   [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getGrowthLength(); });
  } else if (m_params.m_useVesselVolume) {
    std::transform(bestBricks.begin(),
                   bestBricks.end(),
                   std::back_inserter(x),
                   [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getVesselVolume(); });
  } else {
    std::transform(bestBricks.begin(),
                   bestBricks.end(),
                   std::back_inserter(x),
                   [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getGrowthVolume(); });
  }
  if (bestBricks.size() < 5) {
    m_params.m_power = SF_Math<double>::_ALLOMETRIC_POWER;
    m_params._qsm->setA(getA(x, y, m_params.m_power));
    m_params._qsm->setB(m_params.m_power);
    return;
  }
  SF_Math<float>::histogramDownscale(x, y, 50u);
  SF_FitGNPower<float> powerFit;
  powerFit.setX(x);
  powerFit.setY(y);
  powerFit.setMinPts(m_params.m_minPts);
  powerFit.setInlierDistance(m_params.m_inlierDistance);
  powerFit.setRansacIterations(m_params.m_ransacIterations);
  powerFit.setGaussNewtonIterations(m_params.m_gaussNewtonIterations);
  powerFit.setFitWithIntercept(m_params.m_withIntercept);
  float oldA = powerFit.a();
  try {
    powerFit.compute();
    if (powerFit.b() > 0.1 && powerFit.b() < 0.8) {
      m_params.m_power = powerFit.b();
      m_params._qsm->setA(powerFit.a());
      m_params._qsm->setB(powerFit.b());
    } else {
      m_params.m_power = SF_Math<double>::_ALLOMETRIC_POWER;
      m_params._qsm->setA(getA(x, y, m_params.m_power));
      m_params._qsm->setB(m_params.m_power);
      m_params._qsm->setC(0);
    }
  } catch (...) {
    if (powerFit.a() != oldA && powerFit.b() > 0.1 && powerFit.b() < 0.8) {
      m_params.m_power = powerFit.b();
      m_params._qsm->setA(powerFit.a());
      m_params._qsm->setB(powerFit.b());
    } else {
      m_params.m_power = SF_Math<double>::_ALLOMETRIC_POWER;
      m_params._qsm->setA(getA(x, y, m_params.m_power));
      m_params._qsm->setB(m_params.m_power);
    }
  }
}
