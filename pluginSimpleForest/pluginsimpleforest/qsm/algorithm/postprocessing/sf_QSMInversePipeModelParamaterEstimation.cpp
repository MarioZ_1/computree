/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_QSMInversePipeModelParamaterEstimation.h"
/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "math/fit/line/sf_fitLineWithIntercept.h"
#include "sf_QSMInversePipeModelParamaterEstimation.h"

void
SF_QSMReversePipeModelParamaterEstimation::setParams(const SF_ParamReversePipeModelCorrection& params)
{
  m_params = params;
}

SF_ParamReversePipeModelCorrection
SF_QSMReversePipeModelParamaterEstimation::params() const
{
  return m_params;
}

std::pair<float, float>
SF_QSMReversePipeModelParamaterEstimation::equation() const
{
  return m_equation;
}

SF_QSMReversePipeModelParamaterEstimation::SF_QSMReversePipeModelParamaterEstimation() {}

bool
SF_QSMReversePipeModelParamaterEstimation::isUnCorrectedRadiusFit(std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick)
{
  return ((buildingBrick->getFittingType() == SPHEREFOLLOWING || buildingBrick->getFittingType() == CYLINDERCORRECTION ||
           buildingBrick->getFittingType() == TRUNCATEDCONECORRECTION));
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMReversePipeModelParamaterEstimation::chooseBestBricks(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bestBricks;
  float maxY = 0;
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
    float y = brick->getSegment()->getRBOPR();
    if (y > maxY) {
      maxY = y;
    }
  }
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
    float y = brick->getSegment()->getRBOPR();
    if (y < maxY * MINPERCENTAGE)
      continue;
    if (y > maxY * MAXPERCENTAGE)
      continue;
    bestBricks.push_back(brick);
  }
  return bestBricks;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMReversePipeModelParamaterEstimation::unCorrectedBuildingBricks()
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingbricks;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks = m_params._qsm->getBuildingBricks();
  for (auto brick : bricks) {
    if (isUnCorrectedRadiusFit(brick)) {
      for (int i = 0; i < brick->getLength() * 100; i++) {
        buildingbricks.push_back(brick);
      }
    }
  }
  return buildingbricks;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMReversePipeModelParamaterEstimation::removeStem(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  auto crownStart = m_params._qsm->crownStartBrick();
  float height = crownStart->getCenter()[2];

  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> noStemBricks;
  std::for_each(bricks.begin(), bricks.end(), [&height, &noStemBricks](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) {
    auto cylinderHeight = brick->getCenter()[2];
    if (cylinderHeight > height || brick->getSegment()->getBranchOrder() != 0) {
      noStemBricks.push_back(brick);
    }
  });
  return noStemBricks;
}

void
SF_QSMReversePipeModelParamaterEstimation::compute()
{
  std::vector<float> y;
  std::vector<float> x;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> uncorrectedBricks = unCorrectedBuildingBricks();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bestBricks = chooseBestBricks(uncorrectedBricks);
  std::transform(bestBricks.begin(),
                 bestBricks.end(),
                 std::back_inserter(y),
                 [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getRadius(); });
  std::transform(bestBricks.begin(),
                 bestBricks.end(),
                 std::back_inserter(x),
                 [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getSegment()->getRBOPR(); });
  SF_FitLineWithIntercept<float> lineFit;
  lineFit.setX(x);
  lineFit.setY(y);
  lineFit.setIntercept(m_params.m_minRadius);
  lineFit.setInlierDistance(m_params.m_inlierDistance);
  lineFit.setIterations(m_params.m_ransacIterations);
  lineFit.setMinPts(m_params.m_minPts);
  try {
    lineFit.compute();
    m_equation = lineFit.equation();
  } catch (const std::exception& e) {
    std::string eStr = std::string(e.what());
    eStr.append("Could not fit reverse pipemodel parameters.");
    std::cout << eStr << std::endl;
    m_equation = lineFit.errorEquation();
  } catch (...) {
    throw("Could not fit reverse pipemodel parameters.");
  }
}
