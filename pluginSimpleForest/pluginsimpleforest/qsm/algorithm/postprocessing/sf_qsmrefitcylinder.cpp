#include "sf_qsmrefitcylinder.h"

#include "pcl/sf_math.h"
#include "qsm/algorithm/postprocessing/sf_mergeCylindersInsideSegment.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

SF_QSMRefitCylinder::SF_QSMRefitCylinder() {}

void
SF_QSMRefitCylinder::setParams(const SF_ParamRefitCylinders& params)
{
  m_params = params;
}

void
SF_QSMRefitCylinder::setCloud(const SF_CloudNormal::Ptr& cloud)
{
  m_cloud = cloud;
}

void
SF_QSMRefitCylinder::initialize()
{
  m_kdtreeQSM.reset(new typename pcl::KdTreeFLANN<SF_PointNormal>());
  SF_CloudNormal::Ptr centerCloud(new SF_CloudNormal());
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = m_params._qsm->getBuildingBricks();
  for (size_t i = 0; i < buildingBricks.size(); i++) {
    Eigen::Vector3d pointEigen = buildingBricks[i]->getCenter();
    SF_PointNormal point;
    point.getVector3fMap() = pointEigen.cast<float>();
    point.intensity = i;
    centerCloud->push_back(std::move(point));
    SF_CloudNormal::Ptr cloud(new SF_CloudNormal());
    m_cylinderClusters.push_back(cloud);
  }
  m_kdtreeQSM->setInputCloud(centerCloud);

  for (size_t i = 0; i < m_cloud->points.size(); i++) {
    SF_PointNormal point = m_cloud->points[i];
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    if (m_kdtreeQSM->nearestKSearch(point, m_params.m_knn, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0) {
      float minDistance = 5 * m_params.m_inlierDistance;
      int index = -1;
      for (size_t j = 0; j < pointIdxRadiusSearch.size(); ++j) {
        std::shared_ptr<Sf_ModelAbstractBuildingbrick> neighboringBrick = buildingBricks[pointIdxRadiusSearch[j]];
        Eigen::Vector3d p(point.x, point.y, point.z);
        float distance = std::abs(neighboringBrick->getDistance(std::move(p)));
        if (distance < minDistance) {
          minDistance = distance;
          index = pointIdxRadiusSearch[j];
        }
      }
      if (index > -1) {
        m_cylinderClusters[index]->push_back(point);
      }
    }
  }
}

void
SF_QSMRefitCylinder::compute()
{
  SF_MergeCylindersInsideSegment merge;
  merge.compute(m_params._qsm);
  initialize();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = m_params._qsm->getBuildingBricks();
  for (size_t index = 0; index < buildingBricks.size(); index++) {
    pcl::SACSegmentationFromNormals<SF_PointNormal, SF_PointNormal> seg;
    SF_CloudNormal::Ptr cloud = m_cylinderClusters[index];
    if (cloud->points.size() > m_params.m_minPts) {
      pcl::PointIndices::Ptr inliersCylinder(new pcl::PointIndices);
      pcl::ModelCoefficients::Ptr coefficientsCylinder(new pcl::ModelCoefficients);
      seg.setOptimizeCoefficients(true);
      seg.setModelType(pcl::SACMODEL_CYLINDER);
      seg.setMethodType(pcl::SAC_MLESAC);
      seg.setOptimizeCoefficients(false);
      seg.setMaxIterations(m_params.m_ransacIterations);
      seg.setDistanceThreshold(m_params.m_inlierDistance);
      seg.setInputCloud(cloud);
      seg.setInputNormals(cloud);
      seg.segment(*inliersCylinder, *coefficientsCylinder);
      if (coefficientsCylinder->values.size() == 7) {
        auto newAxis = Eigen::Vector3d(
          coefficientsCylinder->values[3], coefficientsCylinder->values[4], coefficientsCylinder->values[5]);
        auto oldAxis = buildingBricks[index]->getAxis();
        auto angle = SF_Math<double>::getAngleBetweenDeg(newAxis, oldAxis);
        if (angle < 30. && coefficientsCylinder->values[6] < (buildingBricks[index]->getRadius() + m_params.m_inlierDistance)) {
          buildingBricks[index]->setCoefficients(coefficientsCylinder);
          buildingBricks[index]->setFittingType(FittingType::CYLINDERCORRECTION);
        }
      }
    }
  }
  for (const auto& segment : m_params._qsm->getSegments()) {
    for (const auto& brick : segment->getBuildingBricks()) {
      if (!brick->getChildren().empty()) {
        auto child = brick->getChildren().front();
        Eigen::Vector3d end = brick->getEnd();
        Eigen::Vector3d start = child->getStart();
        Eigen::Vector3d newP = (start + end) / 2.;
        brick->setStartEndRadius(brick->getStart(), newP, brick->getRadius(), brick->getFittingType());
        child->setStartEndRadius(newP, child->getEnd(), child->getRadius(), child->getFittingType());
      }
    }
  }
}
