/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DETECTSHOOTS_H
#define SF_DETECTSHOOTS_H

#include "sf_detectCrown.h"

class SF_DetectShoots
{
  std::vector<std::shared_ptr<SF_ModelSegment>> firstOrderShoots;
  std::vector<std::shared_ptr<SF_ModelSegment>> secondOrderShoots;

public:
  SF_DetectShoots();
  void compute(std::shared_ptr<SF_ModelQSM> qsm);
  std::vector<std::shared_ptr<SF_ModelSegment>> getFirstOrderShoots() const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getSecondOrderShoots() const;
};

#endif // SF_DETECTSHOOTS_H
