/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DIJKSTRAPARAMETERS_H
#define SF_DIJKSTRAPARAMETERS_H

#include <algorithm>
#include <pcl/segmentation/sac_segmentation.h>
#include <vector>

struct SF_DijkstraOptimizationParameters
{
  double m_edgeConnectionDistance = 0.04;
  double m_clusteringDistance = 0.1;
  double m_binningDistance = 0.1;
  std::vector<double> m_edgeConnectionDistanceMultiplier{ 1.0 };
  double _sphereRadiusMultiplier = 2.0f;
  std::vector<double> m_clusteringDistanceMultiplier{ 1.0 };
  double _epsilonSphere = 0.035f;
  std::vector<double> m_binningDistanceMultiplier{ 1.0 };
  SF_DijkstraOptimizationParameters() {}
  SF_DijkstraOptimizationParameters(double edgeConnectionDistance, double clusteringDistance, double binningDistance)
    : m_edgeConnectionDistance(edgeConnectionDistance), m_clusteringDistance(clusteringDistance), m_binningDistance(binningDistance)
  {
  }
};

struct SF_DijsktraParameters
{
  SF_DijkstraOptimizationParameters m_optimizationParams;
  int _fittingMethod = pcl::SAC_MLESAC;
  float _heapDelta = 0.1f;
  float _inlierDistance = 0.03f;
  double _medianRadiusMultiplier = 3.5f;
  int _minPtsGeometry = 3;
  int _RANSACIterations = 100;
  float _heightInitializationSlice = 0.1f;
  SF_DijsktraParameters() {}
};

#endif // SF_DIJKSTRAPARAMETERS_H
