/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DIJKSTRALIGHTRECURSIVE_H
#define SF_DIJKSTRALIGHTRECURSIVE_H

#include "qsm/algorithm/detection/sf_idetection.h"
#include "qsm/algorithm/optimization/gridsearch/sf_spherefollowingrastersearch.h"
#include "qsm/algorithm/postprocessing/sf_mergeQsm.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

#include <pcl/kdtree/kdtree_flann.h>

class SF_DijkstraLightRecursive : public SF_IDetection
{
  const float MAXDISTANCE = 100.f;
  SF_ParamSpherefollowingRecursive<SF_PointNormal> m_params;
  SF_CloudNormal::Ptr m_cloud;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> m_bricks;
  SF_MergeQsm m_mergeQsm;
  pcl::KdTreeFLANN<SF_PointNormal>::Ptr _kdtreeQSM;
  pcl::PointCloud<SF_PointNormal>::Ptr m_centerCloud;
  SF_CloudNormal::Ptr extractUnfittedPoints();
  std::vector<SF_CloudNormal::Ptr> clusters(SF_CloudNormal::Ptr cloud, size_t minSize, size_t maxSize, float clusterDistance);
  pcl::ModelCoefficients::Ptr coefficients(const SF_PointNormal& p);
  SF_PointNormal closestPoint(const SF_PointNormal& searchPoint, const std::vector<SF_PointNormal>& cloud);
  std::vector<SF_CloudNormal::Ptr> sortClusters(std::vector<SF_CloudNormal::Ptr> clusters);
  void processClusters(std::vector<SF_CloudNormal::Ptr>& clusters);
  void connectQSM(std::shared_ptr<SF_ModelQSM> childQSM);
  void getMinMax(size_t& min, size_t& max, SF_CloudNormal::Ptr cluster, Eigen::Vector3d& closestQSM);
  Eigen::Vector3f cloudVector(SF_CloudNormal::Ptr& cloud, const size_t minIndex, const size_t maxIndex);
  Eigen::Vector3d translateCloud(SF_CloudNormal::Ptr& cloud, const size_t index);
  void initializeKdTree();
  std::vector<SF_CloudNormal::Ptr> distanceClusters(SF_CloudNormal::Ptr cloud, const std::vector<float>& distances);
  std::vector<std::vector<SF_CloudNormal::Ptr>> clusterClusters(std::vector<SF_CloudNormal::Ptr>& clusters);
  std::vector<std::vector<SF_PointNormal>> centerOfMass(std::vector<std::vector<SF_CloudNormal::Ptr>>& clusterClusters);
  void mergeFirstTwoClusters(std::vector<std::vector<SF_CloudNormal::Ptr>>& clusterClusters);
  float minDistance(SF_CloudNormal::Ptr cloud) const;
  void setFittingType(std::shared_ptr<SF_ModelQSM> qsm);
  void computeRadiusAtRoot();

public:
  SF_DijkstraLightRecursive();
  void compute() override;
  void setParams(const SF_ParamSpherefollowingRecursive<SF_PointNormal>& params);
  void setCloud(const SF_CloudNormal::Ptr& cloud);
  void setQsm(const std::shared_ptr<SF_ModelQSM>& qsm);
  std::shared_ptr<SF_ModelQSM> getQSM() override;
  float error() override;
};

#endif // SF_DIJKSTRALIGHTRECURSIVE_H
