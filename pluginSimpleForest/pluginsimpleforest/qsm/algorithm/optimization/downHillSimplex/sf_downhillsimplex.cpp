/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_downhillsimplex.h"

#include "cloud/filter/unary/maxIntensity/sf_maxintensity.h"
#include "qsm/algorithm/cloudQSM/sf_clustercloudbyqsm.h"
#include "qsm/algorithm/distance/sf_extractFittedPoints.h"

#include <cstdint>
#include <stdio.h>

#include <functional>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_ieee_utils.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_multifit_nlinear.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_test.h>
#include <vector>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>

SF_ParamSpherefollowingAdvanced<SF_PointNormal>
SF_DownHillSimplex::params() const
{
  return m_params;
}

void
SF_DownHillSimplex::setParams(const SF_ParamSpherefollowingAdvanced<SF_PointNormal>& params)
{
  m_params = params;
}

SF_DownHillSimplex::SF_DownHillSimplex()
{
  m_numberOfMedian = 0;
  m_numberOfRansacIterations = 0;
}

void
SF_DownHillSimplex::serializeVec(gsl_vector* x,
                                 double fac,
                                 size_t numberClusters,
                                 SF_ParamSpherefollowingAdvanced<SF_PointNormal>& params)
{
  size_t index = 0;
  for (size_t i = 0; i < numberClusters; i++) {
    gsl_vector_set(x, index++, static_cast<double>(params._sphereFollowingParams.m_optimizationParams[i]._epsilonSphere * fac));
    gsl_vector_set(
      x, index++, static_cast<double>(params._sphereFollowingParams.m_optimizationParams[i]._euclideanClusteringDistance * fac));
    gsl_vector_set(
      x, index++, static_cast<double>(params._sphereFollowingParams.m_optimizationParams[i]._sphereRadiusMultiplier * fac));
  }
}

void
SF_DownHillSimplex::computeDHS(SF_ParamSpherefollowingAdvanced<SF_PointNormal>& params, size_t numClusters)
{
  params._modelCloudError = std::numeric_limits<float>::max();
  m_params._modelCloudError = std::numeric_limits<float>::max();
  std::unique_ptr<SF_ParamSpherefollowingAdvanced<SF_PointNormal>> paramSmartPtr(
    new SF_ParamSpherefollowingAdvanced<SF_PointNormal>(params));
  paramSmartPtr->m_numberOfMedian = 0;
  paramSmartPtr->m_numberOfRansacIterations = 0;
  std::uintptr_t par[1] = { reinterpret_cast<std::uintptr_t>(paramSmartPtr.get()) };
  const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer* s = nullptr;
  gsl_vector *ss, *x;
  x = gsl_vector_alloc(numClusters * 3);
  ss = gsl_vector_alloc(numClusters * 3);
  gsl_multimin_function minex_func;
  size_t iter = 0;
  int status;
  double size;
  serializeVec(x, 1.0, numClusters, params);
  serializeVec(ss, 0.3, numClusters, params);
  minex_func.n = numClusters * 3;
  minex_func.f = downhillSimplex;
  minex_func.params = par;
  s = gsl_multimin_fminimizer_alloc(T, numClusters * 3);
  gsl_multimin_fminimizer_set(s, &minex_func, x, ss);

  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;
    size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, params._fitQuality * 1e-3);
    float maxf = std::numeric_limits<float>::max();
    double maxd = static_cast<double>(maxf);
    float error = (s->fval >= maxd) ? maxf : static_cast<float>(s->fval);
    if (params._modelCloudError > error) {
      params._modelCloudError = error;
      size_t index = 0;
      std::vector<SF_SphereFollowingOptimizationParameters> paramVec = params._sphereFollowingParams.m_optimizationParams;
      gsl_vector* v = s->x;
      for (size_t i = 0; i < numClusters; i++) {
        paramVec[i]._epsilonSphere = gsl_vector_get(v, index++);
        paramVec[i]._euclideanClusteringDistance = gsl_vector_get(v, index++);
        paramVec[i]._sphereRadiusMultiplier = gsl_vector_get(v, index++);
      }
      params._sphereFollowingParams.m_optimizationParams = paramVec;
      m_params = params;
    }
    m_params._stepProgress->fireComputation();
  } while (status == GSL_CONTINUE && iter < static_cast<size_t>(params._iterations));
  while (iter < static_cast<size_t>(params._iterations)) {
    iter++;
    m_params._stepProgress->fireComputation();
  }
  m_numberOfMedian += paramSmartPtr->m_numberOfMedian;
  m_numberOfRansacIterations += paramSmartPtr->m_numberOfRansacIterations;
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(s);

  SF_SphereFollowing sphereFollowing;
  sphereFollowing.setParams(m_params);
  sphereFollowing.setCloud(params.m_cloudSphereFollowing);
  sphereFollowing.compute();
  m_numberOfMedian += sphereFollowing.getNumberOfMedian();
  m_numberOfRansacIterations += sphereFollowing.getNumberOfRansacIterations();

  params._qsm = sphereFollowing.getQSM();
  m_params = params; // TODO those pesky hidden assignments to members :/
  m_params.m_cloudSphereFollowing = sphereFollowing.cloud();
}

long long
SF_DownHillSimplex::numberOfRansacIterations() const
{
  return m_numberOfRansacIterations;
}

long long
SF_DownHillSimplex::numberOfMedian() const
{
  return m_numberOfMedian;
}

void
SF_DownHillSimplex::compute()
{
  auto size = m_params.m_numClstrs;
  m_params._modelCloudError = std::numeric_limits<float>::max();
  m_params._sphereFollowingParams.m_optimizationParams.resize(1);
  m_params._sphereFollowingParams.m_optimizationParams.resize(size, m_params._sphereFollowingParams.m_optimizationParams.back());
  SF_ParamSpherefollowingAdvanced<SF_PointNormal> paramCpy = m_params;
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloudCpy = m_params.m_cloudSphereFollowing;

  paramCpy._modelCloudError = std::numeric_limits<float>::max();
  paramCpy.m_numClstrs = size;
  computeDHS(paramCpy, size);
}

double
downhillSimplex(const gsl_vector* v, void* params)
{
  std::uintptr_t* p = reinterpret_cast<std::uintptr_t*>(params);
  SF_ParamSpherefollowingAdvanced<pcl::PointXYZINormal>* paramsBasic =
    reinterpret_cast<SF_ParamSpherefollowingAdvanced<pcl::PointXYZINormal>*>(p[0]);
  size_t index = 0;
  std::vector<SF_SphereFollowingOptimizationParameters> paramsOptim = paramsBasic->_sphereFollowingParams.m_optimizationParams;
  for (size_t i = 0; i < paramsBasic->m_numClstrs; i++) {
    paramsOptim[i]._epsilonSphere = gsl_vector_get(v, index++);
    paramsOptim[i]._euclideanClusteringDistance = gsl_vector_get(v, index++);
    paramsOptim[i]._sphereRadiusMultiplier = gsl_vector_get(v, index++);
  }
  paramsBasic->_sphereFollowingParams.m_optimizationParams = paramsOptim;
  SF_SphereFollowing sphereFollowing;
  sphereFollowing.setParams(*paramsBasic);
  sphereFollowing.setCloud(paramsBasic->m_cloudSphereFollowing);
  try {
    sphereFollowing.compute();
    paramsBasic->m_numberOfMedian += sphereFollowing.getNumberOfMedian();
    paramsBasic->m_numberOfRansacIterations += sphereFollowing.getNumberOfRansacIterations();
    paramsBasic->_modelCloudError = sphereFollowing.error();
  } catch (...) {
    paramsBasic->_modelCloudError = std::numeric_limits<float>::max();
  }
  double res = static_cast<double>(paramsBasic->_modelCloudError);
  return res;
}
