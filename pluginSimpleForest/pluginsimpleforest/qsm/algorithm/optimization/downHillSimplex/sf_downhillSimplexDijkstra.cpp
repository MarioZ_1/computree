/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_downhillSimplexDijkstra.h"

SF_ParamDijkstra<SF_PointNormal>
SF_DownhillSimplexDijkstra::params() const
{
  return m_params;
}

void
SF_DownhillSimplexDijkstra::setParams(const SF_ParamDijkstra<SF_PointNormal>& params)
{
  m_params = params;
}

SF_DownhillSimplexDijkstra::SF_DownhillSimplexDijkstra() {}

void
SF_DownhillSimplexDijkstra::serializeVec(gsl_vector* x, double fac, SF_ParamDijkstra<SF_PointNormal>& params)
{
  size_t index = 0;
  gsl_vector_set(x, index++, static_cast<double>(params.m_djikstraRange * fac));
  gsl_vector_set(x, index++, static_cast<double>(params.m_slice * fac));
  gsl_vector_set(x, index++, static_cast<double>(params.m_clusterSlize * fac));
}

void
SF_DownhillSimplexDijkstra::compute()
{
  m_params._modelCloudError = std::numeric_limits<float>::max();
  auto params = m_params;
  std::unique_ptr<SF_ParamDijkstra<SF_PointNormal>> paramSmartPtr(new SF_ParamDijkstra<SF_PointNormal>(params));
  std::uintptr_t par[1] = { reinterpret_cast<std::uintptr_t>(paramSmartPtr.get()) };
  const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer* s = nullptr;
  gsl_vector *ss, *x;
  x = gsl_vector_alloc(3);
  ss = gsl_vector_alloc(3);
  gsl_multimin_function minex_func;
  size_t iter = 0;
  int status;
  double size;
  serializeVec(x, 1.0, params);
  serializeVec(ss, 0.2, params);
  minex_func.n = 3;
  minex_func.f = downhillSimplexDijkstra;
  minex_func.params = par;
  s = gsl_multimin_fminimizer_alloc(T, 3);
  gsl_multimin_fminimizer_set(s, &minex_func, x, ss);

  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;
    size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, 0.0001); // TODO params._fitQuality * 1e-3
    float maxf = std::numeric_limits<float>::max();
    double maxd = static_cast<double>(maxf);
    float error = (s->fval >= maxd) ? maxf : static_cast<float>(s->fval);
    if (params._modelCloudError > error) {
      params._modelCloudError = error;
      size_t index = 0;
      gsl_vector* v = s->x;
      params.m_djikstraRange = gsl_vector_get(v, index++);
      params.m_slice = gsl_vector_get(v, index++);
      params.m_clusterSlize = gsl_vector_get(v, index++);
      m_params = params;
    }
    m_params._stepProgress->fireComputation();
  } while (status == GSL_CONTINUE && iter < static_cast<size_t>(100));
  while (iter < static_cast<size_t>(100)) { // TODO here param
    iter++;
    m_params._stepProgress->fireComputation(); // TODO this as well
  }
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(s);

  SF_DijkstraLightRecursive dijkstra;
  dijkstra.setParams(m_params);
  dijkstra.setCloud(params._cloudIn);
  dijkstra.compute();

  params._qsm = dijkstra.getQSM();
  m_params = params;
  // m_params.m_cloudSphereFollowing = sphereFollowing.cloud();
}

double
downhillSimplexDijkstra(const gsl_vector* v, void* params)
{
  std::uintptr_t* p = reinterpret_cast<std::uintptr_t*>(params);
  SF_ParamDijkstra<pcl::PointXYZINormal>* paramsBasic = reinterpret_cast<SF_ParamDijkstra<pcl::PointXYZINormal>*>(p[0]);
  size_t index = 0;
  paramsBasic->m_djikstraRange = gsl_vector_get(v, index++);
  paramsBasic->m_slice = gsl_vector_get(v, index++);
  paramsBasic->m_clusterSlize = gsl_vector_get(v, index++);

  if (paramsBasic->m_djikstraRange <= 0.01 || paramsBasic->m_slice <= 0.01 || paramsBasic->m_clusterSlize <= 0.01) {
    return static_cast<double>(std::numeric_limits<float>::max());
  }

  SF_DijkstraLightRecursive dijkstra;
  dijkstra.setParams(*paramsBasic);
  dijkstra.setCloud(paramsBasic->_cloudIn);
  try {
    dijkstra.compute();
    paramsBasic->_modelCloudError = dijkstra.error();
  } catch (...) {
    paramsBasic->_modelCloudError = std::numeric_limits<float>::max();
  }
  double res = static_cast<double>(paramsBasic->_modelCloudError);
  return res;
}
