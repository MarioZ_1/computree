/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmBuildTopology.h"

SF_QSMBuildTopology::SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks,
                                         std::vector<int32_t> parentIndices,
                                         std::vector<int32_t> indices)
  : m_bricks(bricks), m_parentIndices(parentIndices), m_indices(indices)
{
}

SF_QSMBuildTopology::SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks,
                                         std::vector<int32_t> parentIndices)
{
  m_bricks = bricks;
  std::vector<std::int32_t> indices(parentIndices.size());
  std::iota(indices.begin(), indices.end(), 0);
  m_indices = std::move(indices);
  m_parentIndices = std::move(parentIndices);
}

SF_QSMBuildTopology::SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks)
{
  m_bricks = bricks;
  std::vector<std::int32_t> parentIndices(bricks.size());
  std::iota(parentIndices.begin(), parentIndices.end(), -1);
  m_parentIndices = std::move(parentIndices);
  std::vector<std::int32_t> indices(bricks.size());
  std::iota(indices.begin(), indices.end(), 0);
  m_indices = std::move(indices);
}

void
SF_QSMBuildTopology::compute()
{
  setIndices();
  m_qsm.reset(new SF_ModelQSM);
  std::shared_ptr<SF_ModelSegment> rootSegment(new SF_ModelSegment);
  auto rootBrick = getRoot();
  rootSegment->addBuildingBrick(rootBrick);
  m_qsm->setRootSegment(rootSegment);
  buildRecursively(rootBrick);
}

std::shared_ptr<SF_ModelQSM>
SF_QSMBuildTopology::qsm() const
{
  return m_qsm;
}

void
SF_QSMBuildTopology::setIndices()
{
  if (m_bricks.size() != m_indices.size()) {
    throw std::runtime_error("[SF_QSMBuildTopology] : indices and bricks do not have same size.");
  }
  for (std::uint32_t i = 0; i < m_bricks.size(); ++i) {
    m_bricks[i]->setID(m_indices[i]);
  }
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_QSMBuildTopology::getBricksWithParentIndex(std::int32_t index)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> cylinders;
  for (std::uint32_t i = 0; i < m_bricks.size(); ++i) {
    auto& brick = m_bricks[i];
    auto parentIndex = m_parentIndices[i];
    if (parentIndex == index) {
      cylinders.push_back(brick);
    }
  }
  return cylinders;
}
void
SF_QSMBuildTopology::buildRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick)
{
  auto children = getBricksWithParentIndex(lastAddedBrick->getID());
  if (children.size() == 1) {
    addRecursively(lastAddedBrick, children.front());
  } else if (children.size() > 1) {
    addRecursively(lastAddedBrick, children);
  }
}

void
SF_QSMBuildTopology::addRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick,
                                    std::shared_ptr<Sf_ModelAbstractBuildingbrick>& childBrick)
{
  lastAddedBrick->getSegment()->addBuildingBrick(childBrick);
  buildRecursively(childBrick);
}
void
SF_QSMBuildTopology::addRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick,
                                    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& childBricks)
{
  for (auto& childBrick : childBricks) {
    std::shared_ptr<SF_ModelSegment> childSegment(new SF_ModelSegment);
    childSegment->addBuildingBrick(childBrick);
    lastAddedBrick->getSegment()->addChild(childSegment);
    buildRecursively(childBrick);
  }
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_QSMBuildTopology::getRoot()
{
  auto bricks = getBricksWithParentIndex(-1);
  if (bricks.empty()) {
    throw std::runtime_error("[SF_QSMBuildTopology] : no root exists.");
  }
  if (bricks.size() > 1) {
    throw std::runtime_error("[SF_QSMBuildTopology] : more than one root exists.");
  }
  return bricks.front();
}
