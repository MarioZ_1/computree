/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMBUILDTOPOLOGY_H
#define SF_QSMBUILDTOPOLOGY_H

#include "pcl/sf_math.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"
#include "qsm/model/sf_modelQSM.h"

class SF_QSMBuildTopology
{
private:
  std::shared_ptr<SF_ModelQSM> m_qsm;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> m_bricks;
  std::vector<std::int32_t> m_parentIndices;
  std::vector<std::int32_t> m_indices;
  void setIndices();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> getBricksWithParentIndex(std::int32_t index);
  void buildRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick);
  void addRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick,
                      std::shared_ptr<Sf_ModelAbstractBuildingbrick>& childBrick);
  void addRecursively(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& lastAddedBrick,
                      std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& childBricks);
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> getRoot();

public:
  SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks,
                      std::vector<int32_t> parentIndices,
                      std::vector<int32_t> indices);
  SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks,
                      std::vector<std::int32_t> parentIndices);
  SF_QSMBuildTopology(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks);

  void compute();
  std::shared_ptr<SF_ModelQSM> qsm() const;
};

#endif // SF_QSMBUILDTOPOLOGY_H
