/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <Eigen/Dense>

#include "pcl/sf_math.h"
#include "sf_modelAbstractBuildingbrick.h"
#include "sf_modelCylinderBuildingbrick.h"
#include "sf_modelSegment.h"
#include <clocale>

double
Sf_ModelCylinderBuildingbrick::getRadius()
{
  return m_radius;
}

void
Sf_ModelCylinderBuildingbrick::transform(const Eigen::Affine3f& transform)
{
  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::PointXYZ p1;
  p1.x = m_start[0];
  p1.y = m_start[1];
  p1.z = m_start[2];
  pcl::PointXYZ p2;
  p2.x = m_end[0];
  p2.y = m_end[1];
  p2.z = m_end[2];
  cloud.points.push_back(p1);
  cloud.points.push_back(p2);

  pcl::PointCloud<pcl::PointXYZ> transformed;
  pcl::transformPointCloud(cloud, transformed, transform);
  p1 = transformed.points.at(0);
  p2 = transformed.points.at(1);

  m_start[0] = p1.x;
  m_start[1] = p1.y;
  m_start[2] = p1.z;
  m_end[0] = p2.x;
  m_end[1] = p2.y;
  m_end[2] = p2.z;
}

void
Sf_ModelCylinderBuildingbrick::setRadius(double radius, FittingType type)
{
  m_fittingType = type;
  m_radius = radius;
}

void
Sf_ModelCylinderBuildingbrick::translate(Eigen::Vector3d translation)
{
  m_start = m_start + translation;
  m_end = m_end + translation;
}

double
Sf_ModelCylinderBuildingbrick::getVolume()
{
  double volume = SF_Math<double>::_PI * m_radius * m_radius * getLength();
  return volume;
}

double
Sf_ModelCylinderBuildingbrick::getLength()
{
  Eigen::Vector3d c = m_end - m_start;
  return c.norm();
}

double
Sf_ModelCylinderBuildingbrick::getDistance(const Eigen::Vector3d& point)
{
  double distToAxis = getDistanceToAxis(point);
  double distToSegment = getProjectedDistanceToSegment(point);
  double distToHull = distToAxis - m_radius;
  double distance = (std::sqrt((distToHull * distToHull) + (distToSegment * distToSegment)));
  return distance;
}

double
Sf_ModelCylinderBuildingbrick::getDistanceToAxis(const Eigen::Vector3d& point)
{
  auto projection = getProjectionOnAxis(point);
  return (point - projection).norm();
}

double
Sf_ModelCylinderBuildingbrick::getProjectedDistanceToSegment(const Eigen::Vector3d& point)
{
  Eigen::Vector3d projection = getProjectionOnAxis(point);
  double distToStart = SF_Math<double>::distance(m_start, projection);
  double distToEnd = SF_Math<double>::distance(m_end, projection);
  double length = getLength();
  if (distToStart <= length && distToEnd <= length) {
    return 0;
  }
  return std::min(distToStart, distToEnd);
}

double
Sf_ModelCylinderBuildingbrick::getDistanceToInfinitHull(const Eigen::Vector3d& point)
{
  return getDistanceToAxis(point) - m_radius;
}

Eigen::Vector3d
Sf_ModelCylinderBuildingbrick::getProjectionOnAxis(const Eigen::Vector3d& point)
{
  Eigen::Vector3d a = point - m_start;
  Eigen::Vector3d b = m_end - m_start;
  return (m_start + (a.dot(b) / b.dot(b)) * b);
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
Sf_ModelCylinderBuildingbrick::clone()
{
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick);
  cylinder->setStartEndRadius(m_start, m_end, m_radius, m_fittingType);
  cylinder->setMean(m_mean);
  cylinder->setMaxFitQualityError(m_maxFitQualityError);
  cylinder->setGoodQuality(m_wasGoodFitted);
  return cylinder;
}

Eigen::Vector3d
Sf_ModelCylinderBuildingbrick::getCenter()
{
  return ((m_start + m_end) / 2);
}

Eigen::Vector3d
Sf_ModelCylinderBuildingbrick::getAxis()
{
  return (m_end - m_start);
}

void
Sf_ModelCylinderBuildingbrick::setStartEndRadius(const Eigen::Vector3d& start,
                                                 const Eigen::Vector3d& end,
                                                 double radius,
                                                 FittingType type)
{
  m_start = start;
  m_end = end;
  m_radius = radius;
  m_fittingType = type;
}

void
Sf_ModelCylinderBuildingbrick::setCoefficients(pcl::ModelCoefficients::Ptr coefficients)
{
  if (coefficients->values.size() == 7) {
    auto oldStart = m_start;
    auto oldEnd = m_end;
    auto newAxis = Eigen::Vector3d(coefficients->values[3], coefficients->values[4], coefficients->values[5]);
    auto newPoint = Eigen::Vector3d(coefficients->values[0], coefficients->values[1], coefficients->values[2]);
    Eigen::Vector3d a = oldStart - newPoint;
    Eigen::Vector3d b = newAxis;
    auto newStart = (newPoint + (a.dot(b) / b.dot(b)) * b);
    a = oldEnd - newPoint;
    auto newEnd = (newPoint + (a.dot(b) / b.dot(b)) * b);
    setStartEndRadius(newStart, newEnd, coefficients->values[6], FittingType::CYLINDERCORRECTION);
  }
}

Sf_ModelCylinderBuildingbrick::Sf_ModelCylinderBuildingbrick(pcl::ModelCoefficients::Ptr circleA, pcl::ModelCoefficients::Ptr circleB)
{
  assert(circleA->values.size() == 4 && circleB->values.size() == 4);
  m_start[0] = circleA->values[0];
  m_start[1] = circleA->values[1];
  m_start[2] = circleA->values[2];
  m_end[0] = circleB->values[0];
  m_end[1] = circleB->values[1];
  m_end[2] = circleB->values[2];
  m_radius = circleB->values[3];
  m_fittingType = FittingType::SPHEREFOLLOWING;
}

Sf_ModelCylinderBuildingbrick::Sf_ModelCylinderBuildingbrick(Eigen::Vector3d start, Eigen::Vector3d end, double radius)
{
  m_start = start;
  m_end = end;
  m_radius = radius;
}

std::string
Sf_ModelCylinderBuildingbrick::toString()
{
  std::setlocale(LC_NUMERIC, "C");
  std::string str("cylinder");
  str.append(", ");
  str.append(std::to_string(m_id));
  str.append(", ");
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> parent = getParent();
  if (parent == nullptr) {
    str.append("-1");
  } else {
    str.append(std::to_string(parent->getID()));
  }
  str.append(", ");
  str.append(std::to_string(m_start[0]));
  str.append(", ");
  str.append(std::to_string(m_start[1]));
  str.append(", ");
  str.append(std::to_string(m_start[2]));
  str.append(", ");
  str.append(std::to_string(m_end[0]));
  str.append(", ");
  str.append(std::to_string(m_end[1]));
  str.append(", ");
  str.append(std::to_string(m_end[2]));
  str.append(", ");
  str.append(std::to_string(m_radius));
  str.append(", ");
  str.append(std::to_string(getVolume()));
  str.append(", ");
  str.append(std::to_string(getGrowthVolume()));
  str.append(", ");
  str.append(std::to_string(getLength()));
  str.append(", ");
  str.append(std::to_string(getGrowthLength()));
  str.append(", ");
  str.append(std::to_string(getVesselVolume()));
  str.append(", ");
  str.append(std::to_string(getDistanceToTwig()));
  str.append(", ");
  str.append(m_enumMap.at(m_fittingType));
  str.append(", ");
  str.append(std::to_string(getMean()));
  str.append(", ");
  str.append(std::to_string(getMaxFitQualityError()));
  str.append(", ");
  str.append(std::to_string(getGoodQuality()));
  str.append(", ");
  std::shared_ptr<SF_ModelSegment> segment = getSegment();
  str.append(segment->toString());
  return str;
}

std::string
Sf_ModelCylinderBuildingbrick::toHeaderString()
{
  std::string str(
    "type, ID, parentID, startX, startY, startZ, endX, endY, "
    "endZ, radius, volume, growthVolume, length, growthLength, vesselVolume, distanceToTwig, FittingType, averagePointDistance, "
    "maxPointDistance, qualityFlag,");
  std::shared_ptr<SF_ModelSegment> segment = getSegment();
  str.append(segment->toHeaderString());
  return str;
}
