/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_MODEL_ABSTRACT_SEGMENT_H
#define SF_MODEL_ABSTRACT_SEGMENT_H

#include "sf_modelAbstractBuildingbrick.h"
#include "vector"

class SF_ModelQSM;

class SF_ModelSegment : public std::enable_shared_from_this<SF_ModelSegment>
{
  std::weak_ptr<SF_ModelSegment> m_parent;
  std::weak_ptr<SF_ModelQSM> m_tree;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> m_buildingBricks;
  std::vector<std::shared_ptr<SF_ModelSegment>> m_children;

protected:
  std::int32_t m_ID;
  std::int32_t m_branchOrder;
  std::int32_t m_reverseBranchOrder;
  std::int32_t m_RBOPA;
  std::int32_t m_numberOfBranches;
  double m_RBOPR;
  int m_branchID;
  int getParentID();
  std::shared_ptr<SF_ModelSegment> getFirstChild();

public:
  enum class SF_SORTTYPE
  {
    GROWTH_LENGTH,
    GROWTH_VOLUME,
    RADIUS,
    ANGLE
  };
  SF_ModelSegment() = default;
  std::shared_ptr<SF_ModelSegment> clone();

  SF_ModelSegment(std::shared_ptr<SF_ModelQSM> tree);
  void addChild(std::shared_ptr<SF_ModelSegment> child);
  void addBuildingBrick(std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick);
  virtual std::string toString();
  virtual std::string toHeaderString();
  Eigen::Vector3d getStart() const;
  Eigen::Vector3d getEnd() const;
  Eigen::Vector3d getAxis() const;
  bool isLeave() const;
  void remove();
  double getRadius() const;
  double getVolume() const;
  double getGrowthVolume() const;
  double getGrowthLength() const;
  double getLength() const;
  bool isRoot() const;
  bool isStem() const;
  void computeBranchOrder(int branchOrder = 0);
  void computeReverseBranchOrder(int branchOrder = 1);
  double getVolume();
  void computeReverseSummedBranchOrder(int branchOrder = 1);
  void computeNumberOfBranches(int numberOfBranches = 0);
  void initializeOrder();
  std::shared_ptr<SF_ModelSegment> getParent() const;
  void setParent(const std::weak_ptr<SF_ModelSegment>& parent);
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> getBuildingBricks() const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getChildren() const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getSiblings();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> getChildBuildingBricks(const size_t index);
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> getParentBuildingBrick(const size_t index);
  std::shared_ptr<SF_ModelQSM> getTree() const;
  bool isStartOfBranchOrder(const std::int32_t branchOrder) const;
  void setChildSegments(const std::vector<std::shared_ptr<SF_ModelSegment>> childSegments);
  void setBuildingBricks(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& buildingBricks);
  void sort(const SF_SORTTYPE sortType);
  std::int32_t getBranchOrder() const;
  void setBranchOrder(std::int32_t branchOrder);
  std::int32_t getReverseBranchOrder() const;
  void setReverseBranchOrder(std::int32_t reverseBranchOrder);
  double getRBOPR() const;
  void setRBPOBR(std::int32_t rbopr);
  std::int32_t getID() const;
  void setID(std::int32_t ID);
  std::int32_t getRBOPA() const;
  void setRBOPA(std::int32_t rbopa);
  void computeBranchID(std::int32_t branchID);
  std::int32_t getBranchID() const;
  void setTree(const std::weak_ptr<SF_ModelQSM>& tree);
};

#endif // SF_MODEL_ABSTRACT_SEGMENT_H
