/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_MODEL_ABSTRACT_BUILDINGBRICK_H
#define SF_MODEL_ABSTRACT_BUILDINGBRICK_H

#include <Eigen/Core>
#include <memory.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/common/transforms.h>
#include <pcl/point_types.h>
#include <unordered_map>

enum FittingType
{
  UNKNOWN,
  SPHEREFOLLOWING,
  CYLINDERCORRECTION,
  TRUNCATEDCONECORRECTION,
  MEDIAN,
  ALLOMETRICGROWTHVOLUME,
  ALLOMETRICGROWTHLENGTH,
  ALLOMETRICVESSELVOLUME,
  CORRECTBRANCHJUNCTIONS,
  MINRADIUS,
  CONNECTQSM,
  DIJKSTRA,
  DIJKSTRALIGHT,
  REVERSEPIPEMODEL,
  EXTRAPOLATETODTM,
  SHOOTCORRECTION,
  TREEQSM,
  TAPERFIT
};

class SF_ModelSegment;

class Sf_ModelAbstractBuildingbrick : public std::enable_shared_from_this<Sf_ModelAbstractBuildingbrick>
{
protected:
  std::int32_t m_id;
  size_t m_index;
  std::weak_ptr<SF_ModelSegment> m_segment;
  Eigen::Vector3d m_start;
  Eigen::Vector3d m_end;
  bool goodQuality = false;

  double m_maxFitQualityError = 0.1;
  double m_mean = m_maxFitQualityError;
  FittingType m_fittingType;
  double m_minAllometricReturn = 0.00001;
  bool m_wasGoodFitted = true;
  virtual double getDistanceToAxis(const Eigen::Vector3d& point) = 0;
  double getDistanceIfOnAxis(const Eigen::Vector3d& point);
  virtual Eigen::Vector3d getProjectionOnAxis(const Eigen::Vector3d& point) = 0;
  virtual double getBoundingSphereRadius();
  static const std::unordered_map<FittingType, std::string> m_enumMap;

public:
  Sf_ModelAbstractBuildingbrick();
  virtual std::shared_ptr<Sf_ModelAbstractBuildingbrick> clone() = 0;
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> getParent() const;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> getChildren();
  FittingType getFittingTypeFromString(const std::string& fittingTypeString) const;
  std::string getStringFromFittingType(const FittingType& fittingTpye) const;
  bool isLeastSquaresFitted() const;
  bool isMeasuredRadius() const;
  bool isPredictedRadius() const;
  bool isRoot() const;
  bool isLowerStem() const;
  bool startsBranchOrder() const;

  double getGrowthLength();
  double getGrowthVolume();
  double getVesselVolume();
  double getDistanceToTwig();

  virtual double getLength() = 0;
  virtual double getVolume() = 0;
  virtual double getRadius() = 0;
  virtual void setStartEndRadius(const Eigen::Vector3d& start, const Eigen::Vector3d& end, double radius, FittingType type) = 0;
  virtual void setRadius(double radius, FittingType type) = 0;
  virtual void setCoefficients(pcl::ModelCoefficients::Ptr coefficients) = 0;
  virtual double getDistance(const Eigen::Vector3d& point) = 0;
  virtual void translate(Eigen::Vector3d translation) = 0;
  virtual void transform(const Eigen::Affine3f& transform) = 0;

  void setGoodQuality(bool goodQuality);
  bool getGoodQuality() const;
  void remove();
  void splitSegmentAfter();
  double getDistance(const pcl::PointXYZ& point);
  double getDistance(const pcl::PointXYZINormal& point);
  double getDistanceIfOnAxis(const pcl::PointXYZ& point);
  double getDistanceIfOnAxis(const pcl::PointXYZINormal& point);
  double getDistanceToRoot();
  virtual Eigen::Vector3d getCenter() = 0;
  virtual Eigen::Vector3d getAxis() = 0;
  virtual std::string toString() = 0;
  virtual std::string toHeaderString() = 0;
  bool operator<(const Sf_ModelAbstractBuildingbrick& other);

  double getMaxFitQualityError() const;
  void setMaxFitQualityError(const double error);
  double getMean() const;
  void setMean(const double mean);

  size_t getIndex() const;
  void setIndex(const size_t& index);
  std::int32_t getID() const;
  void setID(const int32_t& ID);
  Eigen::Vector3d getStart() const;
  Eigen::Vector3d getEnd() const;
  std::shared_ptr<SF_ModelSegment> getSegment() const;
  void setSegment(std::shared_ptr<SF_ModelSegment> segment);
  void setFittingType(FittingType fittingType);
  FittingType getFittingType() const;
  void getBranchBuildingBricks(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& currentBranch,
                               std::shared_ptr<Sf_ModelAbstractBuildingbrick> currentBrick);
};

std::pair<std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>>
splitIntoStemAndNoStem(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks);

#endif // SF_MODEL_ABSTRACT_BUILDINGBRICK_H
