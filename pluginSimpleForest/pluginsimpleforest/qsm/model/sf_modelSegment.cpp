/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_modelSegment.h"

#include "pcl/sf_math.h"
#include "sf_modelCylinderBuildingbrick.h"
#include "sf_modelQSM.h"

std::shared_ptr<SF_ModelSegment>
SF_ModelSegment::clone()
{
  std::shared_ptr<SF_ModelSegment> segment(new SF_ModelSegment);
  auto bricks = getBuildingBricks();
  for (auto brick : bricks) {
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> brickCpy = brick->clone();
    segment->addBuildingBrick(brickCpy);
  }
  for (auto child : getChildren()) {
    std::shared_ptr<SF_ModelSegment> childCpy = child->clone();
    segment->addChild(childCpy);
  }
  return segment;
}

void
SF_ModelSegment::setParent(const std::weak_ptr<SF_ModelSegment>& parent)
{
  m_parent = parent;
}

std::shared_ptr<SF_ModelSegment>
SF_ModelSegment::getParent() const
{
  return m_parent.lock();
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_ModelSegment::getBuildingBricks() const
{
  return m_buildingBricks;
}

std::string
SF_ModelSegment::toString()
{
  std::string str(std::to_string(m_ID));
  str.append(", ");
  std::shared_ptr<SF_ModelSegment> parent = getParent();
  if (parent == nullptr) {
    str.append("-1");
  } else {
    str.append(std::to_string(parent->getID()));
  }
  str.append(", ");
  str.append(std::to_string(getRadius()));
  str.append(", ");
  str.append(std::to_string(m_buildingBricks[m_buildingBricks.size() / 2]->getGrowthVolume()));
  str.append(", ");
  str.append(std::to_string(m_buildingBricks[m_buildingBricks.size() / 2]->getGrowthLength()));
  str.append(", ");
  str.append(std::to_string(m_branchOrder));
  str.append(", ");
  str.append(std::to_string(m_reverseBranchOrder));
  str.append(", ");
  str.append(std::to_string(m_RBOPR));
  str.append(", ");
  str.append(std::to_string(m_RBOPA));
  str.append(", ");
  str.append(std::to_string(m_numberOfBranches));
  str.append(", ");
  str.append(std::to_string(m_branchID));
  str.append(", ");
  str.append(getTree()->toString());
  return str;
}

std::string
SF_ModelSegment::toHeaderString()
{
  std::string str("segmentID, parentSegmentID, segmentMedianRadius, "
                  "segmentGrowthVolume, segmentGrowthLength, branchOrder, "
                  "reverseBranchOrder, reverseBranchorderPipeRadius, reverseBranchorderPipeArea, numberOfBranches, branchID, ");
  str.append(getTree()->toHeaderString());
  return str;
}

Eigen::Vector3d
SF_ModelSegment::getStart() const
{
  Eigen::Vector3d start;
  if (!m_buildingBricks.empty()) {
    start = m_buildingBricks[0]->getStart();
  }
  return start;
}

Eigen::Vector3d
SF_ModelSegment::getEnd() const
{
  Eigen::Vector3d end;
  if (!m_buildingBricks.empty()) {
    end = m_buildingBricks[m_buildingBricks.size() - 1]->getEnd();
  }
  return end;
}

Eigen::Vector3d
SF_ModelSegment::getAxis() const
{
  return (getEnd() - getStart());
}

bool
SF_ModelSegment::isLeave() const
{
  return getChildren().empty();
}

void
SF_ModelSegment::remove()
{
  if (!isRoot()) {
    std::shared_ptr<SF_ModelSegment> parent = getParent();
    std::vector<std::shared_ptr<SF_ModelSegment>> parentsChildren = parent->getChildren();
    std::vector<std::shared_ptr<SF_ModelSegment>>::iterator position = std::find(
      parentsChildren.begin(), parentsChildren.end(), shared_from_this());
    if (position != parentsChildren.end()) {
      parentsChildren.erase(position);
      parent->setChildSegments(parentsChildren);
    }
    m_parent.reset();
    m_tree.reset();
  }
}

double
SF_ModelSegment::getRadius() const
{
  std::vector<double> radii;
  for (size_t i = 0; i < m_buildingBricks.size(); i++) {
    const auto type = m_buildingBricks[i]->getFittingType();
    if (type == FittingType::SPHEREFOLLOWING || type == FittingType::CYLINDERCORRECTION ||
        type == FittingType::ALLOMETRICGROWTHVOLUME || type == FittingType::ALLOMETRICGROWTHLENGTH ||
        type == FittingType::REVERSEPIPEMODEL) {
      radii.push_back(m_buildingBricks[i]->getRadius());
    }
  }
  return SF_Math<double>::getMedian(radii);
}

double
SF_ModelSegment::getVolume() const
{
  double volume = 0;
  for (size_t i = 0; i < m_buildingBricks.size(); i++) {
    volume += m_buildingBricks[i]->getVolume();
  }
  return volume;
}

double
SF_ModelSegment::getGrowthVolume() const
{
  if (!m_buildingBricks.empty()) {
    size_t index = m_buildingBricks.size() / 2;
    return (m_buildingBricks[index]->getGrowthVolume());
  }
  return 0;
}

double
SF_ModelSegment::getGrowthLength() const
{
  if (!m_buildingBricks.empty()) {
    size_t index = m_buildingBricks.size() / 2;
    return (m_buildingBricks[index]->getGrowthLength());
  }
  return 0;
}

double
SF_ModelSegment::getLength() const
{
  double length = 0;
  for (size_t i = 0; i < m_buildingBricks.size(); i++) {
    length += m_buildingBricks[i]->getLength();
  }
  return length;
}

bool
SF_ModelSegment::isRoot() const
{
  return m_parent.lock() ? false : true;
}

bool
SF_ModelSegment::isStem() const
{
  return m_branchOrder == 0;
}

void
SF_ModelSegment::computeBranchOrder(int branchOrder)
{
  m_branchOrder = branchOrder;
  if (m_children.size() == 0) {
    return;
  }
  if (m_children.size() == 1) {
    m_children[0]->computeBranchOrder(branchOrder);
    return;
  }
  if (m_children.size() > 1) {
    m_children[0]->computeBranchOrder(branchOrder);
    std::for_each(std::next(m_children.begin()), m_children.end(), [branchOrder](std::shared_ptr<SF_ModelSegment> child) {
      child->computeBranchOrder(branchOrder + 1);
    });
  }
}

void
SF_ModelSegment::computeReverseBranchOrder(int branchOrder)
{
  m_reverseBranchOrder = std::max(branchOrder, m_reverseBranchOrder);
  if (!isRoot()) {
    auto parent = getParent();
    parent->computeReverseBranchOrder(branchOrder + 1);
  }
}

double
SF_ModelSegment::getVolume()
{
  if (getBuildingBricks().front() == nullptr) {
    return 0;
  }
  return getBuildingBricks().front()->getGrowthVolume();
}

void
SF_ModelSegment::computeReverseSummedBranchOrder(int branchOrder)
{
  m_RBOPA += branchOrder;
  m_RBOPR = std::sqrt(static_cast<double>(m_RBOPA));
  if (!isRoot()) {
    auto parent = getParent();
    parent->computeReverseSummedBranchOrder(branchOrder);
  }
}

void
SF_ModelSegment::computeNumberOfBranches(int numberOfBranches)
{
  m_numberOfBranches = std::max(numberOfBranches, m_numberOfBranches);
  if (!isRoot()) {
    auto parent = getParent();
    parent->computeNumberOfBranches(numberOfBranches + parent->getChildren().size() - 1);
  }
}

void
SF_ModelSegment::initializeOrder()
{
  m_reverseBranchOrder = -1;
  m_RBOPA = 0;
  m_numberOfBranches = 0;
  m_RBOPR = -1;
  m_branchOrder = -1;
  m_branchID = 0;
  for (const auto& child : m_children) {
    child->initializeOrder();
  }
}

std::int32_t
SF_ModelSegment::getID() const
{
  return m_ID;
}

void
SF_ModelSegment::setID(std::int32_t ID)
{
  m_ID = ID;
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_ModelSegment::getChildren() const
{
  return m_children;
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_ModelSegment::getSiblings()
{
  if (!isRoot()) {
    auto parent = getParent();
    return parent->getChildren();
  }
  std::vector<std::shared_ptr<SF_ModelSegment>> segments;
  segments.push_back(shared_from_this());
  return segments;
}

std::shared_ptr<SF_ModelQSM>
SF_ModelSegment::getTree() const
{
  return m_tree.lock();
}

bool
SF_ModelSegment::isStartOfBranchOrder(const std::int32_t branchOrder) const
{
  return (m_branchOrder == branchOrder && getParent() && getParent()->getBranchOrder() != branchOrder);
}

void
SF_ModelSegment::setChildSegments(const std::vector<std::shared_ptr<SF_ModelSegment>> childSegments)
{
  m_children.clear();
  std::for_each(
    childSegments.begin(), childSegments.end(), [this](std::shared_ptr<SF_ModelSegment> childSegment) { addChild(childSegment); });
}

void
SF_ModelSegment::setBuildingBricks(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& buildingBricks)
{
  m_buildingBricks.clear();
  std::for_each(buildingBricks.begin(), buildingBricks.end(), [this](std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick) {
    addBuildingBrick(buildingBrick);
  });
}

void
SF_ModelSegment::sort(const SF_ModelSegment::SF_SORTTYPE sortType)
{
  std::sort(m_children.begin(),
            m_children.end(),
            [sortType, this](std::shared_ptr<SF_ModelSegment> child1, std::shared_ptr<SF_ModelSegment> child2) {
              switch (sortType) {
                case SF_SORTTYPE::GROWTH_LENGTH:
                  return (child1->getGrowthLength() > child2->getGrowthLength());
                  break;
                case SF_SORTTYPE::GROWTH_VOLUME:
                  return (child1->getGrowthVolume() > child2->getGrowthVolume());
                  break;
                case SF_SORTTYPE::RADIUS:
                  return (child1->getRadius() > child2->getRadius());
                  break;
                case SF_SORTTYPE::ANGLE:
                  return (SF_Math<double>::getAngleBetweenDeg(child1->getAxis(), getAxis()) <
                          SF_Math<double>::getAngleBetweenDeg(child2->getAxis(), getAxis()));
                  break;
                default:
                  throw("Tree sorting with undefined sort type called.");
                  break;
              }
              return true;
            });
}

std::int32_t
SF_ModelSegment::getBranchOrder() const
{
  return m_branchOrder;
}

void
SF_ModelSegment::setBranchOrder(std::int32_t branchOrder)
{
  m_branchOrder = branchOrder;
}

std::int32_t
SF_ModelSegment::getReverseBranchOrder() const
{
  return m_reverseBranchOrder;
}

void
SF_ModelSegment::setReverseBranchOrder(std::int32_t reverseBranchOrder)
{
  m_reverseBranchOrder = reverseBranchOrder;
}

double
SF_ModelSegment::getRBOPR() const
{
  return m_RBOPR;
}

void
SF_ModelSegment::setRBPOBR(std::int32_t rbopr)
{
  m_RBOPR = rbopr;
}

std::int32_t
SF_ModelSegment::getRBOPA() const
{
  return m_RBOPA;
}

void
SF_ModelSegment::setRBOPA(std::int32_t rbopa)
{
  m_RBOPA = rbopa;
}

void
SF_ModelSegment::computeBranchID(std::int32_t branchID)
{
  m_branchID = branchID;
  auto children = getChildren();
  for (auto child : children) {
    child->computeBranchID(branchID);
  }
}

std::int32_t
SF_ModelSegment::getBranchID() const
{
  return m_branchID;
}

void
SF_ModelSegment::setTree(const std::weak_ptr<SF_ModelQSM>& tree)
{
  m_tree = tree;
}

int
SF_ModelSegment::getParentID()
{
  std::shared_ptr<SF_ModelSegment> parent = getParent();
  if (parent == nullptr) {
    return -1;
  } else {
    return parent->getID();
  }
}

void
SF_ModelSegment::addChild(std::shared_ptr<SF_ModelSegment> child)
{
  child->setParent(shared_from_this());
  child->setTree(m_tree);
  m_children.push_back(child);
}

void
SF_ModelSegment::addBuildingBrick(std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick)
{
  buildingBrick->setIndex(m_buildingBricks.size());
  buildingBrick->setSegment(shared_from_this());
  m_buildingBricks.push_back(buildingBrick);
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_ModelSegment::getChildBuildingBricks(const size_t index)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children;
  if (index < m_buildingBricks.size() - 1) {
    children.push_back(m_buildingBricks.at(index + 1));
  } else if (index == m_buildingBricks.size() - 1) {
    for (size_t i = 0; i < m_children.size(); i++) {
      std::shared_ptr<Sf_ModelAbstractBuildingbrick> child = m_children.at(i)->getBuildingBricks()[0];
      children.push_back(child);
    }
  }
  return children;
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_ModelSegment::getParentBuildingBrick(const size_t index)
{
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> parent;
  if (index == 0) {
    std::shared_ptr<SF_ModelSegment> parentSegment = getParent();
    if (parentSegment != nullptr) {
      parent = parentSegment->getBuildingBricks()[parentSegment->getBuildingBricks().size() - 1];
    }
  } else {
    parent = m_buildingBricks[index - 1];
  }
  return parent;
}

SF_ModelSegment::SF_ModelSegment(std::shared_ptr<SF_ModelQSM> tree) : m_tree(tree)
{
  m_ID = -1;
  m_branchOrder = -1;
  m_reverseBranchOrder = -1;
  m_RBOPR = -1;
  m_branchID = 0;
}
