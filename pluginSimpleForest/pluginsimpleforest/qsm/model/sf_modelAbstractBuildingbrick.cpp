/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_modelAbstractBuildingbrick.h"
#include "pcl/sf_math.h"
#include "sf_modelQSM.h"
#include "sf_modelSegment.h"

const std::unordered_map<FittingType, std::string> Sf_ModelAbstractBuildingbrick::m_enumMap{
  { UNKNOWN, "UNKNOWN" },
  { SPHEREFOLLOWING, "SPHEREFOLLOWING" },
  { CYLINDERCORRECTION, "CYLINDERCORRECTION" },
  { TRUNCATEDCONECORRECTION, "TRUNCATEDCONECORRECTION" },
  { MEDIAN, "MEDIAN" },
  { ALLOMETRICGROWTHVOLUME, "ALLOMETRICGROWTHVOLUME" },
  { ALLOMETRICGROWTHLENGTH, "ALLOMETRICGROWTHLENGTH" },
  { ALLOMETRICVESSELVOLUME, "ALLOMETRICVESSELVOLUME" },
  { CORRECTBRANCHJUNCTIONS, "CORRECTBRANCHJUNCTIONS" },
  { MINRADIUS, "MINRADIUS" },
  { CONNECTQSM, "CONNECTQSM" },
  { DIJKSTRA, "DIJKSTRA" },
  { DIJKSTRALIGHT, "DIJKSTRALIGHT" },
  { REVERSEPIPEMODEL, "REVERSEPIPEMODEL" },
  { EXTRAPOLATETODTM, "EXTRAPOLATETODTM" },
  { SHOOTCORRECTION, "SHOOTCORRECTION" },
  { TREEQSM, "TREEQSM" },
  { TAPERFIT, "TAPERFIT" }
};

FittingType
Sf_ModelAbstractBuildingbrick::getFittingTypeFromString(const std::string& fittingTypeString) const
{
  for (auto const& pair : m_enumMap) {
    if (pair.second == fittingTypeString) {
      return pair.first;
    }
  }
  throw std::runtime_error("[Sf_ModelAbstractBuildingbrick::getFittingTypeFromString] : invalid string received.");
}

std::string
Sf_ModelAbstractBuildingbrick::getStringFromFittingType(const FittingType& fittingTpye) const
{
  auto it = m_enumMap.find(fittingTpye);
  if (it == m_enumMap.end()) {
    throw std::runtime_error("[Sf_ModelAbstractBuildingbrick::getStringFromFittingType] : invalid fittingType received.");
  }
  return it->second;
}

bool
Sf_ModelAbstractBuildingbrick::getGoodQuality() const
{
  return m_wasGoodFitted;
}

void
Sf_ModelAbstractBuildingbrick::setGoodQuality(bool goodQuality)
{
  m_wasGoodFitted = goodQuality;
}

double
Sf_ModelAbstractBuildingbrick::getDistanceToRoot()
{
  auto distance = getLength();
  if (!isRoot()) {
    distance += getParent()->getDistanceToRoot();
  }
  return distance;
}

double
Sf_ModelAbstractBuildingbrick::getVesselVolume()
{
  double vesselVolume = getLength() * getSegment()->getRBOPA();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children = getChildren();
  for (size_t i = 0; i < children.size(); i++) {
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> child = children.at(i);
    vesselVolume += child->getVesselVolume();
  }
  return std::max(m_minAllometricReturn, vesselVolume);
}

double
Sf_ModelAbstractBuildingbrick::getMaxFitQualityError() const
{
  return m_maxFitQualityError;
}

void
Sf_ModelAbstractBuildingbrick::setMaxFitQualityError(const double error)
{
  m_maxFitQualityError = error;
}

double
Sf_ModelAbstractBuildingbrick::getMean() const
{
  return m_mean;
}

void
Sf_ModelAbstractBuildingbrick::setMean(const double mean)
{
  m_mean = mean;
}

double
Sf_ModelAbstractBuildingbrick::getDistanceToTwig()
{
  double distance = getLength();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children = getChildren();
  if (!children.empty()) {
    distance += children.front()->getDistanceToTwig();
  }
  return distance;
}

double
Sf_ModelAbstractBuildingbrick::getGrowthLength()
{
  double growthLength = getLength();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children = getChildren();
  for (size_t i = 0; i < children.size(); i++) {
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> child = children.at(i);
    growthLength += child->getGrowthLength();
  }
  return std::max(m_minAllometricReturn, growthLength);
}

double
Sf_ModelAbstractBuildingbrick::getGrowthVolume()
{
  double growthVolume = getVolume();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children = getChildren();
  for (size_t i = 0; i < children.size(); i++) {
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> child = children.at(i);
    growthVolume += child->getGrowthVolume();
  }
  return std::max(m_minAllometricReturn, growthVolume);
}

void
Sf_ModelAbstractBuildingbrick::remove()
{
  if (m_segment.lock()) {
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = m_segment.lock()->getBuildingBricks();
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>::iterator position = std::find(
      buildingBricks.begin(), buildingBricks.end(), shared_from_this());
    if (position != buildingBricks.end()) {
      buildingBricks.erase(position);
      m_segment.lock()->setBuildingBricks(buildingBricks);
    }
    m_segment.reset();
  }
}

void
Sf_ModelAbstractBuildingbrick::splitSegmentAfter()
{
  auto segment = getSegment();
  auto bricks = segment->getBuildingBricks();
  if (bricks.size() == (m_index + 1)) {
    return;
  }
  std::shared_ptr<SF_ModelSegment> seg1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> seg2(new SF_ModelSegment);
  for (size_t i = 0; i < bricks.size(); ++i) {
    (i <= m_index) ? seg1->addBuildingBrick(bricks[i]) : seg2->addBuildingBrick(bricks[i]);
  }
  const auto tree = segment->getTree();
  seg1->setTree(tree);
  seg2->setTree(tree);
  seg1->addChild(seg2);
  auto children = segment->getChildren();
  for (auto child : children) {
    child->remove();
    seg2->addChild(child);
  }
  if (segment->isRoot()) {
    if (tree) {
      tree->setRootSegment(seg1);
    }
    return;
  }
  segment->getParent()->addChild(seg1);
  segment->remove();
}

size_t
Sf_ModelAbstractBuildingbrick::getIndex() const
{
  return m_index;
}

double
Sf_ModelAbstractBuildingbrick::getDistance(const pcl::PointXYZ& point)
{
  Eigen::Vector3d p(point.x, point.y, point.z);
  return getDistance(p);
}

double
Sf_ModelAbstractBuildingbrick::getDistance(const pcl::PointXYZINormal& point)
{
  Eigen::Vector3d p(point.x, point.y, point.z);
  return getDistance(p);
}

double
Sf_ModelAbstractBuildingbrick::getDistanceIfOnAxis(const pcl::PointXYZ& point)
{
  Eigen::Vector3d p(point.x, point.y, point.z);
  return getDistanceIfOnAxis(p);
}

double
Sf_ModelAbstractBuildingbrick::getDistanceIfOnAxis(const pcl::PointXYZINormal& point)
{
  Eigen::Vector3d p(point.x, point.y, point.z);
  return getDistanceIfOnAxis(p);
}

bool
Sf_ModelAbstractBuildingbrick::operator<(const Sf_ModelAbstractBuildingbrick& other)
{
  bool less = true;
  if (m_start[0] > other.m_start[0])
    less = false;
  else if (m_start[1] > other.m_start[1])
    less = false;
  else if (m_start[2] > other.m_start[2])
    less = false;
  else if (m_end[0] > other.m_end[0])
    less = false;
  else if (m_end[1] > other.m_end[1])
    less = false;
  else if (m_end[2] > other.m_end[2])
    less = false;
  return less;
}

void
Sf_ModelAbstractBuildingbrick::setIndex(const size_t& index)
{
  m_index = index;
}

int32_t
Sf_ModelAbstractBuildingbrick::getID() const
{
  return m_id;
}

std::shared_ptr<SF_ModelSegment>
Sf_ModelAbstractBuildingbrick::getSegment() const
{
  return m_segment.lock();
}

void
Sf_ModelAbstractBuildingbrick::setSegment(std::shared_ptr<SF_ModelSegment> segment)
{
  m_segment = segment;
}

void
Sf_ModelAbstractBuildingbrick::setFittingType(FittingType fittingType)
{
  m_fittingType = fittingType;
}

void
Sf_ModelAbstractBuildingbrick::setID(const std::int32_t& ID)
{
  m_id = ID;
}

Eigen::Vector3d
Sf_ModelAbstractBuildingbrick::getStart() const
{
  return m_start;
}

Eigen::Vector3d
Sf_ModelAbstractBuildingbrick::getEnd() const
{
  return m_end;
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
Sf_ModelAbstractBuildingbrick::getParent() const
{
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> parent = getSegment()->getParentBuildingBrick(m_index);
  return parent;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
Sf_ModelAbstractBuildingbrick::getChildren()
{
  std::shared_ptr<SF_ModelSegment> segment = getSegment();
  return segment->getChildBuildingBricks(m_index);
}

bool
Sf_ModelAbstractBuildingbrick::isMeasuredRadius() const
{
  if ((m_fittingType == FittingType::SPHEREFOLLOWING) || (m_fittingType == FittingType::CYLINDERCORRECTION) ||
      (m_fittingType == FittingType::TRUNCATEDCONECORRECTION) || (m_fittingType == FittingType::CORRECTBRANCHJUNCTIONS) ||
      (m_fittingType == FittingType::TREEQSM)) {
    return true;
  }
  return false;
}

bool
Sf_ModelAbstractBuildingbrick::isPredictedRadius() const
{
  return !isMeasuredRadius();
}

bool
Sf_ModelAbstractBuildingbrick::isRoot() const
{
  return getParent() ? false : true;
}

bool
Sf_ModelAbstractBuildingbrick::isLowerStem() const
{
  return ((getSegment()->getBranchOrder() == 0) && (getStart()[2] < getSegment()->getTree()->getCrownStartZ()));
}

bool
Sf_ModelAbstractBuildingbrick::startsBranchOrder() const
{
  return (isRoot() || (getParent()->getSegment()->getBranchOrder() != getSegment()->getBranchOrder()));
}

FittingType
Sf_ModelAbstractBuildingbrick::getFittingType() const
{
  return m_fittingType;
}

void
Sf_ModelAbstractBuildingbrick::getBranchBuildingBricks(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& currentBranch,
                                                       std::shared_ptr<Sf_ModelAbstractBuildingbrick> currentBrick)
{
  currentBranch.push_back(currentBrick);
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> children = currentBrick->getChildren();
  if (!children.empty()) {
    getBranchBuildingBricks(currentBranch, children.at(0));
  }
}

double
Sf_ModelAbstractBuildingbrick::getDistanceIfOnAxis(const Eigen::Vector3d& point)
{
  Eigen::Vector3d projection = getProjectionOnAxis(point);
  double distToStart = SF_Math<double>::distance(m_start, projection);
  double distToEnd = SF_Math<double>::distance(m_end, projection);
  double length = getLength();
  if (distToStart > length || distToEnd > length) {
    return std::numeric_limits<double>::max();
  }
  return SF_Math<double>::distance(projection, point);
}

double
Sf_ModelAbstractBuildingbrick::getBoundingSphereRadius()
{
  double halfLength = getLength() / 2;
  double radius = getRadius();
  return std::sqrt(halfLength * halfLength + radius * radius);
}

Sf_ModelAbstractBuildingbrick::Sf_ModelAbstractBuildingbrick()
{
  m_id = -1;
}

std::pair<std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>>
splitIntoStemAndNoStem(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> stem;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> noStem;
  std::copy_if(bricks.cbegin(), bricks.cend(), std::back_inserter(stem), [&](const auto& brick) { return brick->isLowerStem(); });
  std::copy_if(bricks.cbegin(), bricks.cend(), std::back_inserter(noStem), [&](const auto& brick) { return !brick->isLowerStem(); });
  return std::make_pair(stem, noStem);
}
