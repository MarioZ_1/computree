/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestLineFitWithIntercept.h"

void
SF_UnitTestLineFitWithIntercept::lineFitWorks()
{
  std::vector<float> x{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
  std::vector<float> y{ 11, 21, 31, 41, 51, 161, 171, 181, 191, 201, 211, 221 };
  SF_FitLineWithIntercept<float> lineFit;
  lineFit.setX(x);
  lineFit.setY(y);
  lineFit.setMinPts(4);
  lineFit.setIterations(10);
  lineFit.setInlierDistance(1.f);
  lineFit.setIntercept(1.f);
  lineFit.compute();
  auto equation = lineFit.equation();
  QCOMPARE(equation.first, 10.f);
  QCOMPARE(equation.second, 1.f);
}

void
SF_UnitTestLineFitWithIntercept::lineFitWorksWithOutliers()
{
  std::vector<float> x{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
  std::vector<float> y{ 11, 22, 30, 41, 51, 161, 171, 181, 191, 201, 211, 221 };
  SF_FitLineWithIntercept<float> lineFit;
  lineFit.setX(x);
  lineFit.setY(y);
  lineFit.setMinPts(4);
  lineFit.setIterations(10);
  lineFit.setInlierDistance(2.f);
  lineFit.setIntercept(1.f);
  lineFit.compute();
  auto equation = lineFit.equation();
  QCOMPARE(equation.first, 9.981818f);
  QCOMPARE(equation.second, 1.f);
}
