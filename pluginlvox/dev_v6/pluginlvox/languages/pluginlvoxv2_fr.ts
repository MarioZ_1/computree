<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ComputeProfilesConfiguration</name>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="26"/>
        <source>Configuration de la génération de profils</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="34"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="536"/>
        <source>Pas entre deux profils</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="71"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="241"/>
        <source>Valeurs en pourcentage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="58"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="92"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="198"/>
        <source>et &lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="78"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;La configuration par défaut calcule le profil sur toute la grille suivant l&apos;axe Z&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="45"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="136"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="50"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="141"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="85"/>
        <source>Axe principal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="102"/>
        <source>N&apos;utiliser que les cellules dont la valeur est &gt;=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="146"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="154"/>
        <source>Axe de génération</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="112"/>
        <source>Restreindre le calcul sur une portion de la grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="177"/>
        <source>Borne de l&apos;axe de génération                    &gt;=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="184"/>
        <source>Bornes de l&apos;axe des ordonnées                 &gt;=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="191"/>
        <source>Défaut (RAZ)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="221"/>
        <source>Bornes de l&apos;axe des abscisses                   &gt;=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="208"/>
        <source>Sur toute la grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="228"/>
        <source>Personnalisée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="248"/>
        <source>et &lt;=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="258"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="265"/>
        <source>Coordonnées</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="270"/>
        <source>Voxel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="278"/>
        <source>Unité de calcul</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="439"/>
        <source>Axe principal du profil (ordonnée)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="455"/>
        <source>Axe de génération des profils (normal)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="471"/>
        <source>Axe abscisses</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="484"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="510"/>
        <source>Inférieure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="497"/>
        <location filename="../mk/view/computeprofilesconfiguration.ui" line="523"/>
        <source>Supérieure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/computeprofilesconfiguration.cpp" line="147"/>
        <source>Les valeurs minimum et maximum doivent être négatives lors du calcul de codes d&apos;erreurs.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GenericComputeGridsConfiguration</name>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="20"/>
        <source>Configuration de(s) grille(s) de sortie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="37"/>
        <source>Fonction prédéfinie :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="47"/>
        <source>Si vous appliquez la fonction prédéfinie : toutes les grilles de sorties que vous avez mise en place jusqu&apos;à présent seront supprimées et remplacées par celles de la fonction prédéfinie.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="57"/>
        <source>Si vous ajoutez une fonction prédéfinie : toutes les grilles de sorties mise en place jusqu&apos;à présent seront conservées.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="60"/>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="140"/>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="192"/>
        <source>Ajouter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="165"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Grilles d&apos;entrées&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Grilles de sorties&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="147"/>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="199"/>
        <source>Supprimer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="50"/>
        <source>Valider</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="82"/>
        <source>Variable de la grille de sortie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="92"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Configuration de la grille de sortie&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="105"/>
        <source>Valeur de chaque case de la grille si aucune condition ci-dessous n&apos;est satisfaite :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="122"/>
        <source> Vérification et code d&apos;erreur </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Définissez ici des formules conditionnelles. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;Elles seront executées dans l&apos;ordre dans lequel vous les avez définies et si l&apos;une d&apos;entre elles est vérifié ni les autres ni la formule finale ne seront exécutés.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Pseudo code (exemple) :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Si condition[1] Alors&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = codeErreur[1]&lt;br/&gt;Sinon Si condition[2] Alors&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = codeErreur[2]&lt;br/&gt;Sinon&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = formuleFinal&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span&gt;Si vous voulez vous référer à la grille de sortie écrivez dans la formule le mot clé &quot;&lt;b&gt;this&lt;/b&gt;&quot;.&lt;/span&gt;&lt;p&gt;Exemple : &lt;i&gt;this &amp;lt; -1&lt;/i&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="99"/>
        <source> Résultat </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.ui" line="112"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Définissez ici la formule finale&lt;/span&gt;&lt;/p&gt;&lt;p&gt;La formule finale est executée seulement si aucune condition ci-dessus n&apos;est satisfaite.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Pseudo code (exemple) :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Si condition[1] Alors&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = codeErreur[1]&lt;br/&gt;Sinon Si condition[2] Alors&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = codeErreur[2]&lt;br/&gt;Sinon&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;case(i) = formuleFinal&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span&gt;Si vous voulez vous référer à la grille de sortie écrivez dans la formule le mot clé &quot;&lt;b&gt;this&lt;/b&gt;&quot;.&lt;/span&gt;&lt;p&gt;Exemple : &lt;i&gt;this + a*2&lt;/i&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="114"/>
        <source>Aucune condition ou formule définie pour la grille de sortie %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="122"/>
        <source>Une condition de la grille de sortie %1 ne contient aucune formule</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="130"/>
        <source>La grille de sortie %1 porte le même nom de variable qu&apos;une autre grille !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="357"/>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="358"/>
        <source>Variable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="357"/>
        <source>Nom et Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="358"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="358"/>
        <source>Nom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="358"/>
        <source>NA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="358"/>
        <source>Défaut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="359"/>
        <source>Si</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="359"/>
        <source>Formule d&apos;erreur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="389"/>
        <source>Confirmer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="389"/>
        <source>Attention ! Votre configuration actuelle sera perdu, voulez-vous quand même continuer ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="414"/>
        <source>Erreur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="414"/>
        <source>La fonction prédéfinie ne peut être %4 car elle a besoin de la grille d&apos;entrée %1 non présente.

Grille(s) nécessaire : 

%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="418"/>
        <source>ajoutée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="418"/>
        <source>appliquée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="503"/>
        <source>Grille calculée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="578"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;/br&gt;&lt;br&gt;&lt;/br&gt;&lt;i&gt;%2&lt;/i&gt;&lt;br&gt;&lt;/br&gt;&lt;br&gt;&lt;/br&gt;Grille(s) nécessaire(s) en entrée :%3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="586"/>
        <source>Aucun calcul prédéfini disponible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="687"/>
        <source>&lt;b&gt;Configuration de la grille de sortie&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/genericcomputegridsconfiguration.cpp" line="692"/>
        <source> &lt;b&gt;&quot;%1&quot;&lt;/b&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepCombineDensityGrids</name>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="54"/>
        <source>3- Combiner les grilles LVOX des différents points de vues</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="68"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="105"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="73"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="106"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="75"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="78"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="107"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="80"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="108"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="82"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="109"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="84"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="110"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="86"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="87"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="89"/>
        <source>delta th.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="103"/>
        <source>Grilles combinées</source>
        <translation>Grilles combinées</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="111"/>
        <source>density - %1</source>
        <translation>densité - %1</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="112"/>
        <source>delta theoretical</source>
        <translation>delta théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="113"/>
        <source>scanId</source>
        <translation>Id scan</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="140"/>
        <source>Mode de combinaison</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="140"/>
        <source>des grilles de densité :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="141"/>
        <source>max (densité)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="145"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="147"/>
        <source>max (nt - nb)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="147"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="155"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="162"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="170"/>
        <source>Non disponible : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="147"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="155"/>
        <source>grille(s) nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="152"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="155"/>
        <source>max (nt - nb)/nt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="153"/>
        <source>    -&gt; Ignorer les cas à densité nulle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="160"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="162"/>
        <source>max (ni)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="162"/>
        <source>grille ni manquante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="167"/>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="170"/>
        <source>sum(ni) / sum(nt - nb)</source>
        <translation>somme(ni) / somme (nt - nb)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="168"/>
        <source>    -&gt; (nt - nb) minimum</source>
        <translation>    -&gt; (nt - nb) minimum</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="170"/>
        <source>grille(s) ni/nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="228"/>
        <source>Grille ni manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="229"/>
        <source>Grille nt manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="230"/>
        <source>Grille nb manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinedensitygrids.cpp" line="231"/>
        <source>Grille delta theorique manquante !</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepCombineLvoxGrids</name>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="62"/>
        <source>3bis- Combiner les grilles LVOX des différents points de vues</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="76"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="119"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="81"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="120"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="83"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="86"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="121"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="88"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="122"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="90"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="123"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="92"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="124"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="94"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="95"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="97"/>
        <source>delta th.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="100"/>
        <source>MNT (Raster)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="103"/>
        <source>Modèle Numérique de Terrain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="117"/>
        <source>Grilles combinées</source>
        <translation>Grilles combinées</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="125"/>
        <source>density - %1</source>
        <translation>densité - %1</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="126"/>
        <source>delta theoretical</source>
        <translation>delta théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="127"/>
        <source>scanId</source>
        <translation>Id scan</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="154"/>
        <source>Mode de combinaison</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="154"/>
        <source>des grilles de densité :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="155"/>
        <source>max (densité)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="159"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="161"/>
        <source>max (nt - nb)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="161"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="169"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="176"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="184"/>
        <source>Non disponible : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="161"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="169"/>
        <source>grille(s) nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="166"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="169"/>
        <source>max (nt - nb)/nt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="167"/>
        <source>    -&gt; Ignorer les cas à densité nulle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="174"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="176"/>
        <source>max (ni)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="176"/>
        <source>grille ni manquante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="181"/>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="184"/>
        <source>sum(ni) / sum(nt - nb)</source>
        <translation>somme(ni) / somme (nt - nb)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="182"/>
        <source>    -&gt; (nt - nb) minimum</source>
        <translation>    -&gt; (nt - nb) minimum</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="184"/>
        <source>grille(s) ni/nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="259"/>
        <source>Grille ni manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="260"/>
        <source>Grille nt manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="261"/>
        <source>Grille nb manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcombinelvoxgrids.cpp" line="262"/>
        <source>Grille delta theorique manquante !</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepComputeHeightProfile</name>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="42"/>
        <source>Créer un Profil Vertical de phytovolume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="56"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="59"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="62"/>
        <source>MNT (Raster)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="65"/>
        <source>Modèle Numérique de Terrain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="77"/>
        <source>Profil</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="80"/>
        <source>Threshold</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="83"/>
        <source>Strata 1 Vol</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="92"/>
        <source>Seuil Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="93"/>
        <source>Seuil Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="94"/>
        <source>Pas du seuil</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputeheightprofile.cpp" line="95"/>
        <source>Limite haute de la strate 1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepComputeLvoxGrids</name>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="90"/>
        <source>2- Calculer les grilles LVOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="101"/>
        <source>Scène(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="104"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="105"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="106"/>
        <source>Scanner</source>
        <translation>Scanner</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="107"/>
        <source>Flag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="121"/>
        <source>Hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="122"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="126"/>
        <source>Theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="127"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="130"/>
        <source>ActualTotal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="131"/>
        <source>isNta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="133"/>
        <source>Before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="134"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="136"/>
        <source>Density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="137"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="142"/>
        <source>DeltaIn</source>
        <translation>DeltaIn</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="143"/>
        <source>DeltaOut</source>
        <translation>DeltaOut</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="146"/>
        <source>DeltaTheoretical</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="150"/>
        <source>DeltaActualTotal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="152"/>
        <source>DeltaBefore</source>
        <translation>DeltaBefore</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="166"/>
        <source>Resolution of the grids</source>
        <translation>Résolution des grilles</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="166"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="167"/>
        <source>Minimum number of effective ray in a voxel to take it into account</source>
        <translation>Nombre minimum de rayons effectifs dans un voxel pour qu&apos;ils soient pris en compte</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="168"/>
        <source>Compute Distances</source>
        <translation>Calcul des distances</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="171"/>
        <source>Computation of total beam number :</source>
        <translation>Calcul du nombre total de rayons:</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="173"/>
        <source>Default mode : Theoretical number</source>
        <translation>Mode par défaut: nombre théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="174"/>
        <source>Actual beam number (a full point cloud is required)</source>
        <translation>Véritable nombre de rayons (un nuage de points complet est nécessaire)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="175"/>
        <source>Both are computed</source>
        <translation>Les deux sont calculés</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="179"/>
        <source>Reference for (minX, minY, minZ) corner of the grid :</source>
        <translation>Référence pour la coordonnée (minX, minY, minZ) de la grille :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="182"/>
        <source>Default mode : Bounding box of the scene</source>
        <translation>Mode par défaut: Boite englobante de la scène</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="183"/>
        <source>Custom mode : Relative to following coordinates:</source>
        <translation>Mode personnalisé: par rapport aux coordonnées suivantes:</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="184"/>
        <source>Custom mode : Relative to following coordinates + custom dimensions:</source>
        <translation>Mode personnalisé: par rapport aux coordonnées suivantes + dimensions personnalisées:</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="185"/>
        <source>Custom mode : centered on following coordinates + custom dimensions:</source>
        <translation>Mode personnalisé: centrée sur les coordonnées suivantes + dimensions personnalisées:</translation>
    </message>
    <message>
        <source>Custom mode : Relative to folowing coordinates:</source>
        <translation type="vanished">Mode personnalisé : relatif aux coordonnées suivantes:</translation>
    </message>
    <message>
        <source>Custom mode : Relative to folowing coordinates + custom dimensions:</source>
        <translation type="vanished">Mode personnalisé : Relatif aux coordonnées ET utilisant les dimensions suivantes :</translation>
    </message>
    <message>
        <source>Custom mode : centered on folowing coordinates + custom dimensions:</source>
        <translation type="vanished">Mode personnalisé : Centré sur les coordonnées et dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="188"/>
        <source>X coordinate:</source>
        <translation>Coordonnée X :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="189"/>
        <source>Y coordinate:</source>
        <translation>Coordonnée Y :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="190"/>
        <source>Z coordinate:</source>
        <translation>Coordonnée Z :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="192"/>
        <source>X dimension:</source>
        <translation>Dimension X :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="193"/>
        <source>Y dimension:</source>
        <translation>Dimension Y :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="194"/>
        <source>Z dimension:</source>
        <translation>Dimension Z :</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="196"/>
        <source>Placette cylindrique</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepcomputelvoxgrids.cpp" line="357"/>
        <source>Dimensions spécifiées ne contenant pas les positions de scans : la grille a du être élargie !</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepExportComputedGrids</name>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="34"/>
        <source>2a - Export computed grids</source>
        <translation>2a - Export des grilles calculées</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="40"/>
        <source>Export all the computed grids into a folder</source>
        <translation>Export de toutes les grilles calculées dans un répertoire</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="61"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="66"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="68"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="70"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="72"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="74"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="76"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="78"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="79"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepexportcomputedgrids.cpp" line="93"/>
        <source>Choisir la destination</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepFilterGridByRadius</name>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="63"/>
        <source>Filter une grille 3D par un rayon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="80"/>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="84"/>
        <source>Grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="93"/>
        <source>Grille filtrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="101"/>
        <source>X centre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="102"/>
        <source>Y centre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_stepfiltergridbyradius.cpp" line="103"/>
        <source>Rayon</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX2_StepPreparePointCloud</name>
    <message>
        <location filename="../urfm/step/lvox2_steppreparepointcloud.cpp" line="34"/>
        <source>Préparer la scène de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_steppreparepointcloud.cpp" line="48"/>
        <location filename="../urfm/step/lvox2_steppreparepointcloud.cpp" line="51"/>
        <source>Scene</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../urfm/step/lvox2_steppreparepointcloud.cpp" line="61"/>
        <source>Flag</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_Grid3DExporter</name>
    <message>
        <location filename="../mk/tools/exporter/lvox3_grid3dexporter.cpp" line="23"/>
        <source>Grilles 3D, ACSII LVOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/exporter/lvox3_grid3dexporter.cpp" line="34"/>
        <source>Fichiers Grilles 3D (ASCII) LVOX3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/exporter/lvox3_grid3dexporter.cpp" line="36"/>
        <source>Export des Grilles 3D au format ASCII, ré-implementé pour LVOX3 avec résolution cellule x,y,z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/exporter/lvox3_grid3dexporter.cpp" line="59"/>
        <source>Aucun ItemDrawable du type LVOX3_AbstractGrid3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/exporter/lvox3_grid3dexporter.cpp" line="70"/>
        <source>Erreur</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_Reader_AsciiGrid3D</name>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="48"/>
        <source>Grille 3D, Fichiers ascii GRD3DLVOX</source>
        <translation>Grille 3D, Fichiers ascii GRD3D LVOX</translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="114"/>
        <source>Fichiers grille 3D LVOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="116"/>
        <source>Chargement d&apos;une grille 3D depuis un fichier ASCII ré-implementée pour LVOX résolution x,y,z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="124"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="213"/>
        <source>Read header of file &quot;%1&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="250"/>
        <source>Data type &quot;%1&quot; founded in header</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="252"/>
        <source>Unable to read the data type &quot;%1&quot; founded at line %2. Create a grid of &quot;float&quot; by default.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/reader/lvox3_reader_asciigrid3d.cpp" line="264"/>
        <source>Data type defined as &quot;%1&quot; (by intuition). If you want to force the data type you must insert the string &quot;datatype XXXX&quot; at line %2 (line after the NODATA_value). XXXX must be replaced by &quot;%3&quot; for int, &quot;%4&quot; for float, &quot;%5&quot; for bool, etc...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepAddGridToNext</name>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="27"/>
        <source>Ni</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="29"/>
        <source>Nt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="30"/>
        <source>Nb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="24"/>
        <source>3D Grid</source>
        <translation>Grille 3D</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="26"/>
        <source>2D Grid (MNT)</source>
        <translation>Grille 2D(MNT)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="35"/>
        <source>Ajouter une grille 2D(MNT)/3D(Ni/Nt/Nb) au résultat d&apos;une étape précédente</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="41"/>
        <source>Cette étape permet de rajouter une grille 2D ou 3D à un résultat précédent. L&apos;on peut ensuite s&apos;en servir pour le calcul de caractéristiques des grilles. À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="56"/>
        <source>Type de grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="63"/>
        <source>Étape d&apos;entrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="65"/>
        <source>Groupe de référence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="67"/>
        <source>Grille à affilier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="72"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="74"/>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="110"/>
        <source>MNT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="84"/>
        <source>Type de grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="100"/>
        <source>Grille 3D(Ni)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="101"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="103"/>
        <source>Grille 3D(Nt)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="104"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="106"/>
        <source>Grille 3D(Nb)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepaddgridtonext.cpp" line="107"/>
        <source>isNb</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepComputeLvoxGrids</name>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="80"/>
        <source>2 - Calculer les grilles LVOX (LVOX 3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="89"/>
        <source>Cette étape permet de créer une grille 3D pour les bons tirs, les tirs théoriques et les tirs interceptés à partir d&apos;un taille de voxel défini.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="111"/>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="114"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="113"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="117"/>
        <source>Scanner</source>
        <translation>Scanner</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="117"/>
        <source>The scanner is used to simulate the shooting pattern. If it was not present choose at least a shooting pattern !</source>
        <translation>Le scanner permet de simuler le schéma de tir. Si ce n&apos;était pas présent, choisissez au moins un modèle de tir !</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="118"/>
        <source>Shooting pattern</source>
        <translation>Modèle de tir</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="118"/>
        <source>The shooting pattern is used to simulate it. If it was not present choose at least a scanner !</source>
        <translation>Le modèle de tir est utilisé pour le simuler. Si ce n&apos;était pas présent, choisissez au moins un scanner!</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="120"/>
        <source>MNT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="121"/>
        <source>MNC</source>
        <translation>MNS</translation>
    </message>
    <message>
        <source>Resolution of the grids</source>
        <translation type="vanished">Résolution des grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="65"/>
        <source>Needle (from Length and Diameter)</source>
        <translation>Aiguille (à partir de Longueur et Diamètre)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="66"/>
        <source>BroadLeaf (from Extension1 and Extension2)</source>
        <translation>Feuille (à partir de Extension1 et Extension2)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="67"/>
        <source>Needle Area (from flat scan)</source>
        <translation>Aire d&apos;aiguille (à partir d&apos;un scan plat)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="68"/>
        <source>BroadLead Area (from flat scan)</source>
        <translation>Aire de feuille (à partir d&apos;un scan plat)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="176"/>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="177"/>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="178"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="176"/>
        <source>Resolution of the grids x axis</source>
        <translation>Resolution x de la grille</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="177"/>
        <source>Resolution of the grids y axis</source>
        <translation>Resolution y de la grille</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="178"/>
        <source>Resolution of the grids z axis</source>
        <translation>Resolution z de la grille</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="179"/>
        <source>Compute Distances</source>
        <translation>Calcul des distances</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="181"/>
        <source>Reference for (minX, minY, minZ) corner of the grid :</source>
        <translation>Référence pour la coordonnée (minX, minY, minZ) de la grille :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="184"/>
        <source>Default mode : Bounding box of the scene</source>
        <translation>Mode par défaut: Boite englobante de la scène</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="185"/>
        <source>Custom mode : Relative to folowing coordinates:</source>
        <translation>Mode personnalisé : relatif aux coordonnées suivantes:</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="186"/>
        <source>Custom mode : Relative to folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Relatif aux coordonnées ET utilisant les dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="187"/>
        <source>Custom mode : centered on folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Centré sur les coordonnées et dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="190"/>
        <source>Automatic mode : use grid paramters from grid file</source>
        <translation>Mode automatique: utilise des paramètres de grille à partir du fichier de grille</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="191"/>
        <source>Choisir le fichier .grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="211"/>
        <source>&lt;b&gt;Attention: une résolution x,y,z de 1 cm pour une zone de 10m^3 prend au moins 16GB de mémoire. Un manque d&apos;espace causera un arrêt forcé de CompuTree.&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="214"/>
        <source>Estimation of vegetation element area (for PAD computation)</source>
        <translation>Estimation de l&apos;aire des éléments végétation (pour calcul de PAD)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="215"/>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="216"/>
        <source>Length/Extension1 (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="217"/>
        <source>Diameter/Extension2 (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="218"/>
        <source>Projected Area (from flat scan, cm^2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Length/Extension1 (m)</source>
        <translation type="vanished">Longueur/extension1 (m)</translation>
    </message>
    <message>
        <source>Diameter/Extension2 (m)</source>
        <translation type="vanished">Diamètre/Extension2 (m)</translation>
    </message>
    <message>
        <source>Projected Area (from flat scan, m^2)</source>
        <translation type="vanished">Aire projetée (à partir d&apos;un scan plat, m^2)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="197"/>
        <source>Parameters for custom modes:</source>
        <translation>Paramètres pour les modes personnalisées:</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="70"/>
        <source>0,5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="71"/>
        <source>0,9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="72"/>
        <source>0,95</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="198"/>
        <source>X coordinate:</source>
        <translation>Coordonnée X :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="199"/>
        <source>Y coordinate:</source>
        <translation>Coordonnée Y :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="200"/>
        <source>Z coordinate:</source>
        <translation>Coordonnée Z :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="202"/>
        <source>X dimension:</source>
        <translation>Dimension X :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="203"/>
        <source>Y dimension:</source>
        <translation>Dimension Y :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="204"/>
        <source>Z dimension:</source>
        <translation>Dimension Z :</translation>
    </message>
    <message>
        <source>Parameters for the Pad Preleminaries computations</source>
        <translation type="vanished">Paramètres préliminaires pour le calcul du PAD</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="236"/>
        <source>Hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="237"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="241"/>
        <source>Theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="242"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="244"/>
        <source>Before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputelvoxgrids.cpp" line="245"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <source>DeltaIn</source>
        <translation type="vanished">DeltaIn</translation>
    </message>
    <message>
        <source>DeltaOut</source>
        <translation type="vanished">DeltaOut</translation>
    </message>
    <message>
        <source>Deltatheoretical</source>
        <translation type="vanished">DeltaThéorique</translation>
    </message>
    <message>
        <source>DeltaBefore</source>
        <translation type="vanished">DeltaBefore</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepComputePAD</name>
    <message>
        <source>Grilles</source>
        <translation type="vanished">Grilles</translation>
    </message>
    <message>
        <source>density</source>
        <translation type="vanished">densité</translation>
    </message>
    <message>
        <source>Grilles combinées</source>
        <translation type="vanished">Grilles combinées</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="67"/>
        <source>3b - Estimation du Plant Area Density</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="82"/>
        <source>Grids</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="85"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="87"/>
        <source>Stats hits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="88"/>
        <source>Stats th.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="89"/>
        <source>Stats before</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="100"/>
        <source>PAD-ContactFreq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="102"/>
        <source>PAD-MLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="104"/>
        <source>PAD-BiasCorMLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="106"/>
        <source>PAD-BeerLamb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="108"/>
        <source>PAD-BiasCorBeerLamb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="110"/>
        <source>PAD-UnequalPathBeerLamb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="113"/>
        <source>FreePath Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="114"/>
        <source>Effective FreePath Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="115"/>
        <source>Delta Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="116"/>
        <source>Effective Delta Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="117"/>
        <source>Effective Delta Square Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="118"/>
        <source>1_zleDelta Effective Sum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="131"/>
        <source>Attenuation coefficient estimators :</source>
        <translation>Estimateurs du coefficient d&apos;attenuation :</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="132"/>
        <source>Contact Frequency</source>
        <translation>Fréquence de contact</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="133"/>
        <source>MLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="134"/>
        <source>Bias-corrected MLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="135"/>
        <source>Beer-Lambert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="136"/>
        <source>Bias-corrected Beer-Lambert (equal path)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="137"/>
        <source>Bias-corrected Beer-Lambert (unequal path)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="139"/>
        <source>Configuration options :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="140"/>
        <source>Compute estimators for N larger than</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputepad.cpp" line="141"/>
        <source>Create Grids Corresponding to the different summations</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepComputeProfiles</name>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="38"/>
        <source>Coordonnées</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="56"/>
        <source>6 - Créer des profils à partir de voxels (LVOX 3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="62"/>
        <source>Cette étape permet de créer un histogramme de densité pour toute la grille 3D ou une partie de celle-ci.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="136"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="140"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="167"/>
        <source>Profil</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputeprofiles.cpp" line="172"/>
        <source>Profile Grid</source>
        <translation>Grille de profil</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepComputeSky</name>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="33"/>
        <source>1a - Calcul un raster ciel (LVOX 3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="39"/>
        <source>Cette étape permet de créer une grille 3D à partir d&apos;une taille de voxel défini et également de créer un raster pour le ciel.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="53"/>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="56"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="55"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="63"/>
        <source>Nombre minimum de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="64"/>
        <source>Resolution de la grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="64"/>
        <source>mètre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="73"/>
        <source>MNC</source>
        <translation>MNS</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="74"/>
        <source>Sky hit grid</source>
        <translation>Gille de hits ciel</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="75"/>
        <source>Z level</source>
        <translation>Niveau Z</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcomputesky.cpp" line="75"/>
        <source>Z level of the sky</source>
        <translation>Niveau Z du ciel</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepCreateMesh</name>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="49"/>
        <source>Avec maillage 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="51"/>
        <source>Sans maillage 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="52"/>
        <source>Maillage convexe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="54"/>
        <source>Maillage concave</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="59"/>
        <source>Estimation de fraction d&apos;écart de couronnes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="65"/>
        <source>Cette étape prend en entrée un nuage de points et une grille 3D LVOX pour créer une maille convexe visant à extraire les caractéristiques d&apos;une couronne.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="79"/>
        <source>Utilisation de maillage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="80"/>
        <source>Si vous n&apos;utilisez pas de maillage, vous ne pourrez pas avoir le gap fraction de la couronne.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="87"/>
        <source>Grille d&apos;entrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="89"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="90"/>
        <source>Grille de hits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="100"/>
        <source>Type de maillage 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="101"/>
        <source>À noter que le maillage concave prend un temps de calcul considérable en comparaison au maillage convexe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="113"/>
        <source>Grille de masque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="114"/>
        <location filename="../mk/step/lvox3_stepcreatemesh.cpp" line="116"/>
        <source>Grille de surface</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepExportCrownStats</name>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="39"/>
        <source>Exporter des statistiques de couronnes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="45"/>
        <source>Cette étape prend une grille masque, une grille avec de la matière et un nom de fichier en entrée. Cette étape calcule: - Surface projetée de la couronne- Surface exposée de la couronne(accessible pour la photosynthèse)- Volume de la couronne- Densité de la couronne- Profondeur de la couronne- Hauteur de l&apos;arbre(si normalisé)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="64"/>
        <source>Grille d&apos;entrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="66"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="67"/>
        <source>Grille de masque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="68"/>
        <source>Grille de surface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="69"/>
        <source>Grille de hits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="70"/>
        <source>Entete de fichier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="71"/>
        <source>Nom du fichier de nuage de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="78"/>
        <source>Dossier de sortie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="79"/>
        <source>Nom du fichier de sortie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="80"/>
        <source>Retirer le dessous de la canopé </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepexportcrownstats.cpp" line="82"/>
        <source>Retirer le dessous de la canopé avec une trop grande résolution de grille peut causer des résultats abérrants.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepExtractCircularGrid</name>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="34"/>
        <source>Extraction d&apos;une grille circulaire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="40"/>
        <source>Cette étape permet d&apos;extraire une grille 3D circulaire a partir d&apos;une grille d&apos;entrée, le but étant d&apos;offrir un outil se rapprochant de la réalité forestière.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="53"/>
        <source>Grille d&apos;entrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="55"/>
        <source>Groupe de référence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="56"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="64"/>
        <source>Coordonnée X du centre de la grille à extraire:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="65"/>
        <source>Coordonnée Y du centre de la grille à extraire :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="66"/>
        <source>Rayon de début de la grille à extraire :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="67"/>
        <source>Rayon de la grille à extraire (maximum) :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="68"/>
        <source>Niveau Z minimum :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="69"/>
        <source>Niveau Z maximum :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepextractcirculargrid.cpp" line="81"/>
        <source>Grille extraite</source>
        <translation></translation>
    </message>
    <message>
        <source>Extracted Grid</source>
        <translation type="vanished">Grille extraite</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepFilterVoxelAtDTM</name>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="29"/>
        <source>Filtrage de voxels au niveau du MNT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="35"/>
        <source>Cette étape permet de filtrer tous les voxels se situant au niveau du MNT ou sous le MNT.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="48"/>
        <source>Grille d&apos;entrée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="52"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="53"/>
        <source>DTM</source>
        <translation>MTN</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="64"/>
        <source>Grille 3D(filtrée)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="138"/>
        <source>La grille d&apos;entrée comporte %1 voxels.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepfiltervoxelatdtm.cpp" line="139"/>
        <source>La grille de sortie comporte %1 voxels.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepFlatGrid</name>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="24"/>
        <source>Summation of all the value of a Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="25"/>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="26"/>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="27"/>
        <source>Number of voxel with value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="32"/>
        <source>Flat 3D Grid to a single level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="38"/>
        <source>Cette étape permet de rajouter une grille 2D ou 3D à un résultat précédent. L&apos;on peut ensuite s&apos;en servir pour le calcul de caractéristiques des grilles. À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="57"/>
        <source>Grids</source>
        <translation type="unfinished">Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="60"/>
        <source>Group</source>
        <translation type="unfinished">Groupe</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="62"/>
        <source>Grid to Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="69"/>
        <source>Type of Flatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepflatgrid.cpp" line="79"/>
        <source>Flatted Grid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepGenericComputeGrids</name>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="48"/>
        <source>3a - Calcul personnalisé sur des grilles (LVOX 3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="54"/>
        <source>Cette étape permet de créer une grille 3D de densité à partir des trois grilles de l&apos;étape précédente (Hits, Theoritical, Before). En plus, cette étape donne une valeur d&apos;erreur à des voxels ayant trop d&apos;occlusion.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="283"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="290"/>
        <source>Grille A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="294"/>
        <source>Grille %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="294"/>
        <source>Grille optionnel à utiliser si vous voulez faire des calculs sur plus d&apos;une grille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="302"/>
        <source>Attention</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="302"/>
        <source>Vous avez déjà définie des grilles de sorties vous ne pouvez donc pas changer les grilles d&apos;entrées. Voulez vous supprimer votre configuration des grilles de sorties pour modifier les grilles d&apos;entrées ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="365"/>
        <source>Grille calculée</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepGridNormalisation</name>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="30"/>
        <source>Normalisation des hauteur d&apos;une grille (en developpement)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="36"/>
        <source>Cette étape permet diminuer la hauteur des voxel d&apos;une grille selon un roster de hauteur. À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="53"/>
        <source>Grids</source>
        <translation type="unfinished">Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="56"/>
        <source>Group</source>
        <translation type="unfinished">Groupe</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="58"/>
        <source>Grid to Reduce</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgridnormalisation.cpp" line="76"/>
        <source>Flatted Grid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LVOX3_StepInterpolateDistance</name>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="41"/>
        <source>Trust</source>
        <translation>Confiance</translation>
    </message>
    <message>
        <source>4 - NODATA interpolation (LVOX 3)</source>
        <translation type="vanished">4 - Interpolation des NODATA (LVOX 3)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="86"/>
        <source>Grids</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="93"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="95"/>
        <source>Density grid</source>
        <translation>Grille de densité</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="122"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="125"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="128"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="131"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <source>Common parameters</source>
        <translation type="vanished">Paramètres communs</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="155"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="162"/>
        <source>Minimum density</source>
        <translation>Densité minimum</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="156"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="163"/>
        <source>Minimum neighbor value to consider the voxel in the average</source>
        <translation>Valeur minimale du voisin pour considérer le voxel dans la moyenne</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="157"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="164"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="177"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="183"/>
        <source>Interpolation radius</source>
        <translation>Rayon d&apos;interpolation</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="157"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="164"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="177"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="183"/>
        <source>meter</source>
        <translation>mètres</translation>
    </message>
    <message>
        <source>Parameter for Z axis average interpolation</source>
        <translation type="vanished">Parametre pour l&apos;interpolation en moyenne de Z</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="73"/>
        <source>Interpolation type</source>
        <translation>Type d&apos;interpolation</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="43"/>
        <source>Z Axis Average</source>
        <translation>Moyenne de Z</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="40"/>
        <source>Distance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="44"/>
        <source>Kriging Binomial (In Development)</source>
        <translation>Krigeage binomial (en développement)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="45"/>
        <source>Kriging Normal (In Development)</source>
        <translation>Krigeage normal (En développement)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="52"/>
        <source>5 - NODATA interpolation (LVOX 3)</source>
        <translation>4 - Interpolation des NODATA (LVOX 3)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="58"/>
        <source>Cette étape permet d&apos;interpolation les valeurs NODATA et les codes d&apos;erreurs(toutes valeurs négatives). Elle contient deux interpolations basées sur le voisinage (Distance et Trust) et une interpolation basée sur la moyenne des Z, plus rapide, mais moins précise.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="116"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="119"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="154"/>
        <source>Parameter for distance interpolation</source>
        <translation>Paramètres pour l&apos;interpolation de distance</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="158"/>
        <source>Decay power</source>
        <translation>Puissance de décomposition</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="161"/>
        <source>Parameters for trust interpolation</source>
        <translation>Paramètres d&apos;interpolation de confiance</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="165"/>
        <source>Lower bound</source>
        <translation>Limite basse</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="165"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="166"/>
        <source>effective rays (Nt - Nb)</source>
        <translation>Rayons effectifs (Nt - Nb)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="165"/>
        <source>Voxel with lower effective rays are not trusted</source>
        <translation>Les voxels avec un nombre de rayons efficaces inférieur ne sont pas fiables</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="166"/>
        <source>Higher bound</source>
        <translation>Limite haute</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="166"/>
        <source>Voxel with higher effective rays are fully trusted</source>
        <translation>Les voxels avec un nombre de rayons efficaces supérieur sont totalement fiables</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="169"/>
        <source>Parameters for Z axis average interpolation</source>
        <translation>Paramètres pour l&apos;interpolation par moyenne de Z</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="170"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="176"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="182"/>
        <source>effective rays (Nt - Nb) Threshold</source>
        <translation>Limite de rayons effectifs(Nt-Nb)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="170"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="176"/>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="182"/>
        <source>Voxel with lower effective rays are considered as having a density of 0</source>
        <translation>Les voxels sous la limite sont considérés comme ayant une densité nulle</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="171"/>
        <source>number of height Z axis average calculated on</source>
        <translation>Nombre de tranches de Z à prendre pour la moyenne</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepinterpolatedistance.cpp" line="207"/>
        <source>Density - interpolated</source>
        <translation>Densité - interpolée</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepLoadFiles</name>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="70"/>
        <source>1 - Charger des fichiers pour LVOX (LVOX 3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="76"/>
        <source>Cette étape permet de charger des fichiers à l&apos;aide de différents readers pour pouvoir créer des grilles de densité.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="376"/>
        <source>Entête de fichier</source>
        <translation>File header</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="378"/>
        <source>Scanner LVOX</source>
        <translation>Scanneur LVOX</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="378"/>
        <source>Shooting Pattern/Position/Direction/Angle forced by user</source>
        <translation>Modele de tirs/Position/Direction/Angle forcé par l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="471"/>
        <source>CT_Scene object not found or loaded object has no scene.</source>
        <translation>objet CT_Scene non trouvé ou chargement d&apos;un objet sans points.</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="505"/>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="521"/>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="529"/>
        <source>The scanner position is over 300m for the center of the scene. It as now been set to values at the minimum of the scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="513"/>
        <source>The scanner position is over 300m for the center of the scene. It as now been set to values center over the scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="544"/>
        <source>New Scanner X : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="545"/>
        <source>New Scanner Y : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="546"/>
        <source>New Scanner Z : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="621"/>
        <source>Restriction de %1 points.</source>
        <translation></translation>
    </message>
    <message>
        <source>Scanner forced</source>
        <translation type="vanished">Scanner forcé</translation>
    </message>
    <message>
        <source>Position / Angle of the scanner forced by user</source>
        <translation type="vanished">Position / Angle du scanner forcé par l&apos;utilisateur</translation>
    </message>
    <message>
        <source>CT_Scene object not found</source>
        <translation type="vanished">Objet scène non trouvé</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepMergeGrids</name>
    <message>
        <source>6 - merge grids from multiple scans (LVOX 3)</source>
        <translation type="vanished">6 - Fusionner des grilles à partir de scans multiples (LVOX 3)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="39"/>
        <source>4 - merge grids from multiple scans (LVOX 3)</source>
        <translation>6 - Fusionner des grilles à partir de scans multiples (LVOX 3)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="45"/>
        <source>Cette étape permet de fusionner les grilles de tous les scanneurs sur une scène d&apos;un nuage de points. On obtient en sortie une nouvelle version des quatres grilles(merged).</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="113"/>
        <source>Grids</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="116"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="124"/>
        <source>Density grid</source>
        <translation>Grille de densité</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="160"/>
        <source>Hits (merged)</source>
        <translation>Hits (fusionnés)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="164"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="169"/>
        <source>Theoretic (merged)</source>
        <translation>Théoriques (fusionnés)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="173"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="178"/>
        <source>Before (merged)</source>
        <translation>Before (fusionnés)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="182"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepmergegrids.cpp" line="187"/>
        <source>Density (merged)</source>
        <translation>Densité (fusionnés)</translation>
    </message>
</context>
<context>
    <name>LVOX3_StepUnifyScene</name>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="41"/>
        <source>Unification des scènes pour le calcul du DTM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="47"/>
        <source>Cette étape permet d&apos;extraire une grille 3D circulaire a partir d&apos;une grille d&apos;entrée, le but étant d&apos;offrir un outil se rapprochant de la réalité forestière.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="60"/>
        <source>Groupe de scènes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="62"/>
        <source>Groupe de référence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="63"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="75"/>
        <location filename="../mk/step/lvox3_stepunifyscene.cpp" line="78"/>
        <source>MergedScene</source>
        <translation>Scène fusionnée</translation>
    </message>
</context>
<context>
    <name>LVOX_Grid3DExporter</name>
    <message>
        <location filename="../tools/lvox_grid3dexporter.cpp" line="23"/>
        <source>Grilles 3D, ACSII</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tools/lvox_grid3dexporter.cpp" line="34"/>
        <source>Fichiers Grilles 3D (ASCII)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tools/lvox_grid3dexporter.cpp" line="36"/>
        <source>Export des Grilles 3D au format ASCII, inspiré du format ASCII ESRI GRID pour les rasters (1 fichier par grille)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tools/lvox_grid3dexporter.cpp" line="59"/>
        <source>Aucun ItemDrawable du type CT_AbstractGrid3D</source>
        <translation>Aucun Item du type CT_AbstractGrid3D</translation>
    </message>
    <message>
        <location filename="../tools/lvox_grid3dexporter.cpp" line="70"/>
        <source>Erreur</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepCombineDensityGrids</name>
    <message>
        <source>Combinaison de grilles 3D de différentes points de vues</source>
        <translation type="vanished">Combinaison de grilles 3D de différentes points de vues</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="68"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="105"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="73"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="106"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <source>theorical</source>
        <translation type="vanished">theorical (théoriques)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="78"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="107"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="82"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="109"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="89"/>
        <source>delta th.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="86"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="54"/>
        <source>3- Combiner les grilles LVOX des différents points de vues</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="75"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="80"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="108"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="84"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="110"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="87"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="103"/>
        <source>Grilles combinées</source>
        <translation>Grilles combinées</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="111"/>
        <source>density - %1</source>
        <translation>densité - %1</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="112"/>
        <source>delta theoretical</source>
        <translation>delta théorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="113"/>
        <source>scanId</source>
        <translation>Id scan</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="140"/>
        <source>Mode de combinaison</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="140"/>
        <source>des grilles de densité :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="141"/>
        <source>max (densité)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="145"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="147"/>
        <source>max (nt - nb)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="147"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="155"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="162"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="170"/>
        <source>Non disponible : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="147"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="155"/>
        <source>grille(s) nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="152"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="155"/>
        <source>max (nt - nb)/nt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="153"/>
        <source>    -&gt; Ignorer les cas à densité nulle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="160"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="162"/>
        <source>max (ni)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="162"/>
        <source>grille ni manquante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="167"/>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="170"/>
        <source>sum(ni) / sum(nt - nb)</source>
        <translation>somme(ni) / somme (nt - nb)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="168"/>
        <source>    -&gt; (nt - nb) minimum</source>
        <translation>    -&gt; (nt - nb) minimum</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="170"/>
        <source>grille(s) ni/nt/nb manquante(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="226"/>
        <source>Grille ni manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="227"/>
        <source>Grille nt manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="228"/>
        <source>Grille nb manquante !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcombinedensitygrids.cpp" line="229"/>
        <source>Grille delta theorique manquante !</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepCompareGrids</name>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="25"/>
        <source>7 - Compare two grids</source>
        <translation>7 - Comparer deux grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="31"/>
        <source>Takes two grids and outputs a new one containing difference on a voxel by voxel basis</source>
        <translation>A partir de deux grilles, calcul d&apos;une grille contenant leur différence voxel par voxel</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="52"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="57"/>
        <source>nb (before)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="58"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="61"/>
        <source>nt (theoretical)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="62"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomparegrids.cpp" line="73"/>
        <source>nb/nt</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepComputeLvoxGrids</name>
    <message>
        <source>Calcul de grilles 3D densités corrigées</source>
        <translation type="vanished">Calcul de grilles 3D densités corrigées</translation>
    </message>
    <message>
        <source>Scène(s)</source>
        <translation type="vanished">Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="127"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="125"/>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="128"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="129"/>
        <source>Scanner</source>
        <translation>Scanner</translation>
    </message>
    <message>
        <source>Grille de référence</source>
        <translation type="vanished">Grille de référence</translation>
    </message>
    <message>
        <source>OPTIONNEL</source>
        <translation type="vanished">OPTIONNEL</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="156"/>
        <source>Hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <source>Theorical</source>
        <translation type="vanished">theorical (théoriques)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="162"/>
        <source>Before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="165"/>
        <source>Density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="171"/>
        <source>DeltaIn</source>
        <translation>DeltaIn</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="172"/>
        <source>DeltaOut</source>
        <translation>DeltaOut</translation>
    </message>
    <message>
        <source>DeltaTheorical</source>
        <translation type="vanished">DeltaTheorical</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="159"/>
        <source>Theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="173"/>
        <source>Deltatheoretical</source>
        <translation>DeltaThéorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="174"/>
        <source>DeltaBefore</source>
        <translation>DeltaBefore</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="109"/>
        <source>2- Calculer les grilles LVOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="157"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="160"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="163"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="166"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="194"/>
        <source>Reference for (minX, minY, minZ) corner of the grid :</source>
        <translation>Référence pour la coordonnée (minX, minY, minZ) de la grille :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="197"/>
        <source>Default mode : Bounding box of the scene</source>
        <translation>Mode par défaut: Boite englobante de la scène</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="198"/>
        <source>Custom mode : Relative to folowing coordinates:</source>
        <translation>Mode personnalisé : relatif aux coordonnées suivantes:</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="199"/>
        <source>Custom mode : Relative to folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Relatif aux coordonnées ET utilisant les dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="200"/>
        <source>Custom mode : centered on folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Centré sur les coordonnées et dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="202"/>
        <source>Automatic mode : use grid paramters from grid file</source>
        <translation>Mode automatique: utilise des paramètres de grille à partir du fichier de grille</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="203"/>
        <source>Choisir le fichier .grid</source>
        <translation>Choose .grid file</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="206"/>
        <source>X coordinate:</source>
        <translation>Coordonnée X :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="207"/>
        <source>Y coordinate:</source>
        <translation>Coordonnée Y :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="208"/>
        <source>Z coordinate:</source>
        <translation>Coordonnée Z :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="210"/>
        <source>X dimension:</source>
        <translation>Dimension X :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="211"/>
        <source>Y dimension:</source>
        <translation>Dimension Y :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="212"/>
        <source>Z dimension:</source>
        <translation>Dimension Z :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="351"/>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="376"/>
        <source>Dimensions spécifiées ne contenant pas les positions de scans : la grille a du être élargie !</source>
        <translation></translation>
    </message>
    <message>
        <source>Use reference grid resolution</source>
        <translation type="vanished">Utiliser la résolution de la grille de référence</translation>
    </message>
    <message>
        <source>(if reference grid has been specified...)</source>
        <translation type="vanished">(si une grille de référence a été séléctionnée)</translation>
    </message>
    <message>
        <source>If not : resolution of the grids</source>
        <translation type="vanished">Sinon : résolution des grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="188"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="188"/>
        <source>Resolution of the grids</source>
        <translation>Résolution des grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="189"/>
        <source>Minimum number of effective ray in a voxel to take it into account</source>
        <translation>Nombre minimum de rayons effectifs dans un voxel pour qu&apos;ils soient pris en compte</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="190"/>
        <source>Compute Distances</source>
        <translation>Calcul des distances</translation>
    </message>
</context>
<context>
    <name>LVOX_StepComputeOcclusionsSpace</name>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="76"/>
        <source>Compute occlusions space</source>
        <translation>Calcul de l&apos;espace occlus</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="87"/>
        <source>Scène(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="90"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="92"/>
        <source>Scanner</source>
        <translation>Scanner</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="105"/>
        <source>Occlusion space</source>
        <translation>Espace occlus</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="117"/>
        <source>Resolution of the grids</source>
        <translation>Résolution des grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="117"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="118"/>
        <source>Distance Threshold</source>
        <translation>Seuil de distance</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="122"/>
        <source>Reference for (minX, minY, minZ) corner of the grid :</source>
        <translation>Référence pour la coordonnée (minX, minY, minZ) de la grille :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="125"/>
        <source>Default mode : Bounding box of the scene</source>
        <translation>Mode par défaut: Boite englobante de la scène</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="126"/>
        <source>Custom mode : Relative to folowing coordinates:</source>
        <translation>Mode personnalisé : relatif aux coordonnées suivantes:</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="127"/>
        <source>Custom mode : Relative to folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Relatif aux coordonnées ET utilisant les dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="128"/>
        <source>Custom mode : centered on folowing coordinates + custom dimensions:</source>
        <translation>Mode personnalisé : Centré sur les coordonnées et dimensions suivantes :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="131"/>
        <source>X coordinate:</source>
        <translation>Coordonnée X :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="132"/>
        <source>Y coordinate:</source>
        <translation>Coordonnée Y :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="133"/>
        <source>Z coordinate:</source>
        <translation>Coordonnée Z :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="135"/>
        <source>X dimension:</source>
        <translation>Dimension X :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="136"/>
        <source>Y dimension:</source>
        <translation>Dimension Y :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="137"/>
        <source>Z dimension:</source>
        <translation>Dimension Z :</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="270"/>
        <location filename="../step/lvox_stepcomputeocclusionspace.cpp" line="294"/>
        <source>Dimensions spécifiées ne contenant pas les positions de scans : la grille a du être élargie !</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepComputePAD</name>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="40"/>
        <source>6- Calculer grille Plant Area Density</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="54"/>
        <location filename="../step/lvox_stepcomputepad.cpp" line="68"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="59"/>
        <source>delta th.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="60"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="66"/>
        <source>Grilles combinées</source>
        <translation>Grilles combinées</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="69"/>
        <source>PAD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="77"/>
        <source>    -&gt; Ecraser les valeurs infinies </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="78"/>
        <source>   -&gt; Effacer les valeurs &gt; à</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputepad.cpp" line="175"/>
        <source>Distance Nt moyenne = 0 [PAD infini !!] </source>
        <translation>Mean Nt distance = 0 [infinite PAD !!]</translation>
    </message>
</context>
<context>
    <name>LVOX_StepComputeProfile</name>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="33"/>
        <source>5- Créer un Profil Vertical à partir de Voxels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="47"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="51"/>
        <source>Grille 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="61"/>
        <source>Profil</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="70"/>
        <source>Ne prendre en compte que les valeurs &gt; à</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepcomputeprofile.cpp" line="72"/>
        <source>Ne prendre en compte que les valeurs &lt; à</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepExportComputedGrids</name>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="34"/>
        <source>2a - Export computed grids</source>
        <translation>2a - Export des grilles calculées</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="40"/>
        <source>Export all the computed grids into a folder</source>
        <translation>Export de toutes les grilles calculées dans un répertoire</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="61"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="66"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="68"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="70"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="72"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="74"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="76"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="78"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="79"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportcomputedgrids.cpp" line="93"/>
        <source>Choisir la destination</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepExportMergedGrids</name>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="34"/>
        <source>3a - Export merged grids</source>
        <translation>3a - Export des grilles fusionnées</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="40"/>
        <source>Export all the merged grids into a folder</source>
        <translation>Export de toute les grilles fusionnées dans un répertoire</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="61"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="66"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="68"/>
        <source>isNi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="70"/>
        <source>theoretical</source>
        <translation>Théorique</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="72"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="74"/>
        <source>before</source>
        <translation>before (intercéptés avant)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="76"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="78"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="79"/>
        <source>isDensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepexportmergedgrids.cpp" line="93"/>
        <source>Choisir la destination</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepInterpolateDensityGrids</name>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="32"/>
        <source>4 - Interpolation par moyenne de Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="38"/>
        <source>Cette étape fait une interpolation des valeurs de la grille de densité.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="52"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="56"/>
        <source>hits</source>
        <translation>hits (retours)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="57"/>
        <source>density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="66"/>
        <source>density (interpolated)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="74"/>
        <source>effective rays (Nt - Nb) Threshold</source>
        <translation>Limite de rayons effectifs(Nt-Nb)</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepinterpolatedensitygrid.cpp" line="74"/>
        <source>Voxel with lower effective rays are considered as having no density.</source>
        <translation>Les voxels avec un nombre de rayons effectifs plus bas sont considérés comme ayant une densité nulle.</translation>
    </message>
</context>
<context>
    <name>LVOX_StepLoadInFile</name>
    <message>
        <source>Chargement de nuages de points / fichier .in</source>
        <translation type="vanished">Chargement de nuages de points / fichier .in</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="51"/>
        <source>1- Charger un fichier .in LVOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="71"/>
        <source>IndividualScenes</source>
        <translation>Scènes individuelles</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="74"/>
        <source>IndividualScene</source>
        <translation>Scène individuelle</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="75"/>
        <source>IndividualSceneColors</source>
        <translation>Couleurs de scène individuelle</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="76"/>
        <source>IndividualSceneIntensity</source>
        <translation>Intensité de scène individuelle</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="77"/>
        <source>ScanPosition</source>
        <translation>Position de scan</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="80"/>
        <location filename="../step/lvox_steploadinfile.cpp" line="83"/>
        <source>MergedScene</source>
        <translation>Scène fusionnée</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="91"/>
        <source>Choisir le fichier .in</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="92"/>
        <source>Extraire une placette de rayon fixé</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="93"/>
        <source>Rayon de la placette</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="94"/>
        <source>Z min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="95"/>
        <source>Z max</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LVOX_StepMergeInputs</name>
    <message>
        <source>Grilles</source>
        <translation type="obsolete">Grilles</translation>
    </message>
    <message>
        <source>Hits</source>
        <translation type="obsolete">hits (retours)</translation>
    </message>
    <message>
        <source>Theoretical</source>
        <translation type="obsolete">Théorique</translation>
    </message>
    <message>
        <source>Before</source>
        <translation type="obsolete">before (intercéptés avant)</translation>
    </message>
    <message>
        <source>Density</source>
        <translation type="obsolete">densité</translation>
    </message>
</context>
<context>
    <name>LVOX_StepNdNtGrids</name>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="25"/>
        <source>4.5 - Compute nb/nt grid</source>
        <translation>4.5 - Calcul de la grille nb/nt</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="31"/>
        <source>Compute nb/nt grid</source>
        <translation>Calcul de la grille nb/nt</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="52"/>
        <source>Grilles</source>
        <translation>Grilles</translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="57"/>
        <source>nb (before)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="58"/>
        <source>isNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="61"/>
        <source>nt (theoretical)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="62"/>
        <source>isNt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lvox_stepndntgrids.cpp" line="73"/>
        <source>nb/nt</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LoadFileConfigUtil</name>
    <message>
        <location filename="../mk/view/loadfileconfigutil.cpp" line="59"/>
        <source>Error parsing line %1</source>
        <translation>Erreur de décodage ligne %1</translation>
    </message>
</context>
<context>
    <name>LoadFileConfiguration</name>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="14"/>
        <source>Chargement de fichiers pour LVOX</source>
        <translation>Loading files for LVOX</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="22"/>
        <source> Type de fichier </source>
        <translation>File type</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="28"/>
        <source>Choix du lecteur :</source>
        <translation>Choix du reader :</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="41"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="50"/>
        <source>Supprimer les points dont les coordonnées sont (0,0,0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="57"/>
        <source>Restreindre la scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="75"/>
        <source>Rayon de la scène restreinte (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="97"/>
        <source> Chemin des fichiers </source>
        <translation>Files path</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="103"/>
        <source>Ajouter les chemins de fichiers à charger :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="112"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;Ajouter&lt;/span&gt; un chemin de fichier en cliquant sur le bouton &amp;quot;Ajouter&amp;quot;.&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;Modifier&lt;/span&gt; un chemin de fichier en sélectionnant la ligne et en appuyant sur le bouton &amp;quot;Modifier&amp;quot;&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;Supprimer&lt;/span&gt; un chemin de fichier en sélectionnant la ligne et en appuyant sur le bouton &amp;quot;Supprimer&amp;quot;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="124"/>
        <source>Ajouter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="131"/>
        <source>Modifier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="138"/>
        <source>Importer .in</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="145"/>
        <source>Exporter en .in</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="152"/>
        <source>Supprimer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="159"/>
        <source>Tout supprimer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="196"/>
        <source>Configuration du scanner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="202"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Aucun fichier sélectionné&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="209"/>
        <source>Type de scanner :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="216"/>
        <source>Type de scanner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="223"/>
        <source>Position (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="234"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la coordonnée Z du centre du scanner. Si le scanneur est planaire, rajouter une marge d&apos;au moins 20 metres du plus haut point de la scene.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="513"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la coordonnée Z du centre du scanner&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="250"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="529"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="257"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="536"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="270"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="565"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="277"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="572"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la coordonnée X du centre du scanner&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="293"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="543"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la coordonnée Y du centre du scanner&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="314"/>
        <source>Angles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="320"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cochez la case si votre appareil scanne la scène dans le sens des aiguilles d&apos;une montre (à cocher lors de l&apos;utilisation d&apos;un scanner FARO ou Z+F).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="323"/>
        <source>Angles dans le sens horaire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="330"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cochez la case si vous souhaitez écrire les résolutions et angles en radians&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="333"/>
        <source>Valeurs en radians</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="340"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résolution angulaire&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="352"/>
        <source>Horizontale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="359"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la résolution angulaire horizontale du scanner&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="372"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indiquez la résolution angulaire verticale du scanner&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="385"/>
        <source>Verticale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="394"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Angles d&apos;ouverture&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="403"/>
        <source>Horizontaux</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="425"/>
        <location filename="../mk/view/loadfileconfiguration.ui" line="475"/>
        <source>à</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="447"/>
        <source>Verticaux</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="502"/>
        <source>Direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="593"/>
        <source>Appliquer au fichier suivant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.ui" line="600"/>
        <source>Appliquer à tous les fichiers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="156"/>
        <source>Aucun fichier sélectionné !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="202"/>
        <source>&lt;i&gt;%1&lt;/i&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="202"/>
        <source>Aucun fichier sélectionné</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="309"/>
        <source>Scene (*.in)</source>
        <translation>Scène (*.in)</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="320"/>
        <source>&lt;b&gt;Failed to load file %1&lt;/b&gt;&lt;/br&gt;&lt;p&gt;%2&lt;p&gt;</source>
        <translation>&lt;b&gt;Failed to load file %1&lt;/b&gt;&lt;/br&gt;&lt;p&gt;%2&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="322"/>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="337"/>
        <source>Import error</source>
        <translation>Erreur d&apos;importation</translation>
    </message>
    <message numerus="yes">
        <location filename="../mk/view/loadfileconfiguration.cpp" line="334"/>
        <source>&lt;b&gt;%n Files(s) not found:&lt;/b&gt;&lt;/br&gt;&lt;p&gt;%1&lt;p&gt;</source>
        <translation>
            <numerusform>&lt;b&gt;%n fichier(s) non trouvé(s):&lt;/b&gt;&lt;/br&gt;&lt;p&gt;%1&lt;p&gt;</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="347"/>
        <source>Save File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="349"/>
        <source>In File (*.in)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="404"/>
        <source>Erreur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="404"/>
        <source>Aucun lecteur de fichier disponible !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="422"/>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="424"/>
        <source>Choisir un fichier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="422"/>
        <location filename="../mk/view/loadfileconfiguration.cpp" line="424"/>
        <source>Fichiers compatibles (%1)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MergeGridsConfiguration</name>
    <message>
        <location filename="../mk/view/mergegridsconfiguration.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/mergegridsconfiguration.ui" line="20"/>
        <source>Merge method</source>
        <translation>Méthode de fusion</translation>
    </message>
    <message>
        <location filename="../mk/view/mergegridsconfiguration.ui" line="49"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/mergegridsconfiguration.ui" line="55"/>
        <source>Ignore zero density voxel</source>
        <translation>Ignorer le voxel de densité nulle</translation>
    </message>
    <message>
        <location filename="../mk/view/mergegridsconfiguration.ui" line="64"/>
        <source>Minimum effective rays (nt - nb):</source>
        <translation>Nomnbre de rayons effectifs minimal (nt-nb):</translation>
    </message>
</context>
<context>
    <name>PredefinedMapperConfiguration</name>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="26"/>
        <source>Mettre en correspondance les entrées</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="49"/>
        <source> Par nom de variable </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="62"/>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="91"/>
        <source>Grille(s) disponible(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="67"/>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="96"/>
        <source>Issu de la fonction prédéfinie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="78"/>
        <source> Par nom et type </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.ui" line="109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Veuillez mettre en correspondances les grilles de la fonction prédéfinie avec les grilles d&apos;entrées disponibles.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.cpp" line="61"/>
        <source>La grille &quot;%1&quot; a été choisie deux fois !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/view/predefinedmapperconfiguration.cpp" line="73"/>
        <source>Toutes les grilles nécessaires n&apos;ont pas été renseignées</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Les dimensions de la grille ont été élargies grille pour inclure les scanneurs</source>
        <translation type="vanished">Les dimensions de la grille ont été élargies pour inclure les scanneurs</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_computelvoxgridspreparator.cpp" line="108"/>
        <source>Les dimensions de la grille ont été élargies grille pour inclure les scanneurs et/ou leurs portées</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_computelvoxgridspreparator.cpp" line="135"/>
        <source>Voxel bounding box: (%1,%2,%3), (%4,%5,%6)</source>
        <translation>Bounding box des Voxels : (%1,%2,%3), (%4,%5,%6)</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_computelvoxgridspreparator.cpp" line="159"/>
        <location filename="../step/lvox_stepcomputelvoxgrids.cpp" line="393"/>
        <location filename="../step/lvox_steploadinfile.cpp" line="146"/>
        <source>File reader created</source>
        <translation>Lecteur de fichier créé</translation>
    </message>
    <message>
        <location filename="../step/lvox_steploadinfile.cpp" line="154"/>
        <source>Wrong file extension</source>
        <translation>Mauvaise extension de fichier</translation>
    </message>
    <message>
        <location filename="../lvox_steppluginmanager.cpp" line="119"/>
        <location filename="../lvox_steppluginmanager.cpp" line="120"/>
        <location filename="../lvox_steppluginmanager.cpp" line="121"/>
        <location filename="../lvox_steppluginmanager.cpp" line="122"/>
        <location filename="../lvox_steppluginmanager.cpp" line="123"/>
        <location filename="../lvox_steppluginmanager.cpp" line="124"/>
        <location filename="../lvox_steppluginmanager.cpp" line="125"/>
        <location filename="../lvox_steppluginmanager.cpp" line="126"/>
        <location filename="../lvox_steppluginmanager.cpp" line="127"/>
        <location filename="../lvox_steppluginmanager.cpp" line="128"/>
        <source>LVOX3</source>
        <translation></translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="vanished">Filtre</translation>
    </message>
    <message>
        <location filename="../lvox_steppluginmanager.cpp" line="133"/>
        <location filename="../lvox_steppluginmanager.cpp" line="135"/>
        <location filename="../lvox_steppluginmanager.cpp" line="136"/>
        <source>Statistiques</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="179"/>
        <source>Indice de densité relative (IDR)</source>
        <translation>Relative density index (RDI)</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="180"/>
        <source>Calcul d&apos;une grille de densité à partir des grilles &quot;Nt&quot;, &quot;Nb&quot; et &quot;Ni&quot;</source>
        <translation>Calculation of a density grid from the grids &quot;Nt&quot;, &quot;Nb&quot; and &quot;Ni&quot;</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="185"/>
        <source>Hits [%1]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="190"/>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="244"/>
        <source>Theoretical [%1]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="195"/>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="249"/>
        <source>Before [%1]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="202"/>
        <source>Density</source>
        <translation>densité</translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="238"/>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="256"/>
        <source>Taux d&apos;occlusion</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_stepgenericcomputegrids.cpp" line="239"/>
        <source>Calcul d&apos;une grille contenant le taux d&apos;occlusion à partir des grilles &quot;Nt&quot; et &quot;Nb&quot;</source>
        <translation>Computation of a grid containing the occlusion rate from the grids &quot;Nt&quot; and &quot;Nb&quot;</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_genericconfiguration.h" line="33"/>
        <source>&quot;%1&quot; = %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_genericconfiguration.h" line="98"/>
        <source>&quot;%1&quot; [%2]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_gridmode.h" line="26"/>
        <source>Bounding box of the scene</source>
        <translation>Bounding box de la scène</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_gridmode.h" line="28"/>
        <source>Relative to coordinates</source>
        <translation>Par rapport aux coordonnées</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_gridmode.h" line="30"/>
        <source>Relative to coordinates and custom dimensions</source>
        <translation>Par rapport aux coordonnées et aux dimensions personnalisées</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_gridmode.h" line="32"/>
        <source>Centered to coordinates and custom dimensions</source>
        <translation>Centré sur les coordonnées et la dimension personnalisée</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_gridmode.h" line="34"/>
        <source>From .grid file</source>
        <translation>A partir du fichier .grid</translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="14"/>
        <source>Tirs réels - Sphérique</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="15"/>
        <source>Tirs réels - Planaire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="16"/>
        <source>Tirs théoriques à partir du scanner - Sphérique</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="16"/>
        <source>Les tirs sont générés à partir des données du scanner contenue dans chaque fichier chargé. En plus d&apos;offrir un scanner forced pour générés tirs à partir des paramètres utilisateur renseignés.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="17"/>
        <source>Les tirs sont générés à partir des paramètres utilisateur renseignés ci-dessous.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="17"/>
        <source>Tirs théoriques - Sphérique customisé</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="14"/>
        <source>La position du scanner représente l&apos;origine des tirs. La direction d&apos;un tir est orientée vers un point du nuage. Ceci pour chaque point du nuage.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/lvox3_scannerutils.cpp" line="15"/>
        <source>La position du scanner et la direction représentent respectivement un point du plan et son vecteur normal. L&apos;origine d&apos;un tir est le point du nuage projeté sur le plan, la direction est le point du nuage. Ceci pour chaque point du nuage.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/step/lvox3_steploadfiles.cpp" line="593"/>
        <source>Cancelling scene narrowing : radius of 0 meter</source>
        <translation>Rayon de 0 mètres. Arrêt de restriction.</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="622"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="466"/>
        <source>X dimension</source>
        <translation>Dimension X</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="623"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="467"/>
        <source>Y dimension</source>
        <translation>Dimension Y</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="624"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="468"/>
        <source>Z dimension</source>
        <translation>Dimension Z</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="625"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="472"/>
        <source>X Resolution</source>
        <translation>Résolution X</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="626"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="473"/>
        <source>Y Resolution</source>
        <translation>Résolution Y</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="627"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="474"/>
        <source>Z Resolution</source>
        <translation>Résolution Z</translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="628"/>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="475"/>
        <source>NA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="629"/>
        <source>Min Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d.h" line="630"/>
        <source>Max Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="469"/>
        <source>X min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="470"/>
        <source>Y min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/3dgrid/lvox3_grid3d_sparse.h" line="471"/>
        <source>Z min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="155"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="148"/>
        <source>Mode filaire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="156"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="149"/>
        <source>Forcer limite basse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="157"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="150"/>
        <source>Limite basse (forcée)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="158"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="151"/>
        <source>Forcer limite haute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="159"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="152"/>
        <source>Limite haute (forcée)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="160"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="153"/>
        <source>Coef. de reduction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="161"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="154"/>
        <source>Utiliser couleurs pré-définies</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="162"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="155"/>
        <source>Valeur de transparence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="163"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="156"/>
        <source>Nb. Plans masqués X-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="164"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="157"/>
        <source>Nb. Plans masqués X+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="165"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="158"/>
        <source>Nb. Plans masqués Y-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="166"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="159"/>
        <source>Nb. Plans masqués Y+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="167"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="160"/>
        <source>Nb. Plans masqués Z-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3d_sparsedrawmanager.hpp" line="168"/>
        <location filename="../mk/tools/drawmanager/lvox3_standardgrid3ddrawmanager.hpp" line="161"/>
        <source>Nb. Plans masqués Z+</source>
        <translation></translation>
    </message>
</context>
</TS>
