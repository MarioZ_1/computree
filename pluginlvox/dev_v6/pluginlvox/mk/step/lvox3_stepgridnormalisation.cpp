#include "lvox3_stepgridnormalisation.h"

//In/Out dependencies
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools dependencies
#include "ct_view/ct_stepconfigurabledialog.h"
#include "mk/tools/lvox3_gridtype.h"

//Source models(result where the grid will go)
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInSourceItem        "its"

//Target models(result of the grid)
#define DEF_SearchInTargetResult      "rt"
#define DEF_SearchInTargetGroup       "gt"
#define DEF_SearchInGrid              "grid"
#define DEF_SearchInRoster            "ros"

LVOX3_StepGridNormalisation::LVOX3_StepGridNormalisation(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

QString LVOX3_StepGridNormalisation::getStepDescription() const
{
    return tr("Normalisation des hauteur d'une grille (en developpement)");
}

// Step detailed description
QString LVOX3_StepGridNormalisation::getStepDetailledDescription() const
{
    return tr("Cette étape permet diminuer la hauteur des voxel d'une grille selon un roster de hauteur. "
              "À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)");
}

CT_VirtualAbstractStep* LVOX3_StepGridNormalisation::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepGridNormalisation(dataInit);
}

void LVOX3_StepGridNormalisation::createPreConfigurationDialog()
{

}

void LVOX3_StepGridNormalisation::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInTargetResult, tr("Grids"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInTargetGroup, CT_AbstractItemGroup::staticGetType(), tr("Group"));

    resultModel->addItemModel(DEF_SearchInTargetGroup, DEF_SearchInGrid, LVOX3_AbstractGrid3D::staticGetType(), tr("Grid to Reduce"));
    //resultModel->addItemModel(DEF_SearchInTargetGroup,DEF_SearchInRoster, CT_Image2D<float>(),tr("Roster des hauteurs"));
    //TODO Ajuster le CT_image2D comme itemAttendu, un cas trouble de groupe devrait apparaitre

}

void LVOX3_StepGridNormalisation::createPostConfigurationDialog()
{

}

void LVOX3_StepGridNormalisation::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel =
            createNewOutResultModelToCopy(DEF_SearchInTargetResult);

    if(resultModel != NULL)
    {
        resultModel->addItemModel(DEF_SearchInTargetGroup,_NORM_ModelName,new lvox::Grid3Df(),tr("Flatted Grid"));
    }
}

void LVOX3_StepGridNormalisation::compute()
{
    //QList<CT_ResultGroup*> outResult =
    CT_ResultGroup* resultOut_grids = getOutResultList().first();

    CT_ResultGroupIterator itGrp(resultOut_grids, this, DEF_SearchInTargetGroup);
    while(itGrp.hasNext() && !isStopped()) {
        /*
         * Two casts are required, the group iterator returns const objects and
         * because addItemDrawable() is called, it has to be non const.
         */
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itGrp.next());
        LVOX3_AbstractGrid3D* InGrid = (LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGrid);
        //CT_Image2D* MNT = (CT_Image2D*) group->firstItemByINModelName(this, DEF_SearchInRoster);

        float xres = InGrid->xresolution();
        float yres = InGrid->yresolution();
        float zres = InGrid->zresolution();
        float NAd = 0;

        size_t xdim = InGrid->xdim();
        size_t ydim = InGrid->ydim();
        size_t zdim = InGrid->zdim();

        lvox::Grid3Df* itemOuti_CONTEUR = new lvox::Grid3Df(_NORM_ModelName.completeName(), resultOut_grids,
                                                             InGrid->minX(), InGrid->minY(), InGrid->minZ(),
                                                             xdim, ydim, 1,
                                                             xres,yres,zres, NAd, NAd);
        group->addItemDrawable(itemOuti_CONTEUR);

        for(size_t xx = 0;xx < xdim; xx++)
        {
            for(size_t yy = 0; yy < ydim; yy++)
            {
                //nombre de voxels
                //int nbValeur = div(MNT->valueAtCoords(xx,yy),zres).quot;

                for(size_t zz = 0; zz < zdim; zz++)
                {
                    //attribuer la valeur du voxel à l'un sous
                    size_t index = 0;
                    InGrid->index(xx,yy,zz,index);
                    float currentValue = InGrid->valueAtIndexAsDouble(index);
                    //itemOuti_CONTEUR->setValue(xx,yy,zz-nbValeur,currentValue);
                }
            }
        }

        itemOuti_CONTEUR->computeMinMax();
    }

}
