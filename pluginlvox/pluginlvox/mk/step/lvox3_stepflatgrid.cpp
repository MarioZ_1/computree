#include "lvox3_stepflatgrid.h"

//In/Out dependencies
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools dependencies
#include "ct_view/ct_stepconfigurabledialog.h"
#include "mk/tools/lvox3_gridtype.h"

//Source models(result where the grid will go)
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInSourceItem        "its"

//Target models(result of the grid)
#define DEF_SearchInTargetResult      "rt"
#define DEF_SearchInTargetGroup       "gt"
#define DEF_SearchInGrid              "grid"

LVOX3_StepFlatGrid::LVOX3_StepFlatGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    m_SumTypeCollection.insert(tr("Summation of all the value of a Z"), Summation);
    m_SumTypeCollection.insert(tr("Number of voxel with value"), Number);
    _GridTypeCollection.insert(tr("Number of voxel with value"), Int);
    _GridTypeCollection.insert(tr("Number of voxel with value"), Float);
}

QString LVOX3_StepFlatGrid::getStepDescription() const
{
    return tr("Flat 3D Grid to a single level");
}

// Step detailed description
QString LVOX3_StepFlatGrid::getStepDetailledDescription() const
{
    return tr("Cette étape permet de rajouter une grille 2D ou 3D à un résultat précédent. L'on peut ensuite s'en servir pour le calcul de caractéristiques des grilles. "
              "À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)");
}

CT_VirtualAbstractStep* LVOX3_StepFlatGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepFlatGrid(dataInit);
}

// Choose the grid type to give a little more flexibility in grid choice MNT or 3D Grid
void LVOX3_StepFlatGrid::createPreConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

}

void LVOX3_StepFlatGrid::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInTargetResult, tr("Grids"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInTargetGroup, CT_AbstractItemGroup::staticGetType(), tr("Group"));

    resultModel->addItemModel(DEF_SearchInTargetGroup, DEF_SearchInGrid, LVOX3_AbstractGrid3D::staticGetType(), tr("Grid to Flat"));

}

void LVOX3_StepFlatGrid::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addStringChoice(tr("Type of Flatting"), "", m_SumTypeCollection.keys(), m_SumType);
}

void LVOX3_StepFlatGrid::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel =
            createNewOutResultModelToCopy(DEF_SearchInTargetResult);

    if(resultModel != NULL)
    {
        resultModel->addItemModel(DEF_SearchInTargetGroup,_FLAT_ModelName,new lvox::Grid3Df(),tr("Flatted Grid"));
    }
}

void LVOX3_StepFlatGrid::compute()
{
    //QList<CT_ResultGroup*> outResult =
    CT_ResultGroup* resultOut_grids = getOutResultList().first();

    CT_ResultGroupIterator itGrp(resultOut_grids, this, DEF_SearchInTargetGroup);
    while(itGrp.hasNext() && !isStopped()) {
        /*
         * Two casts are required, the group iterator returns const objects and
         * because addItemDrawable() is called, it has to be non const.
         */
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itGrp.next());
        LVOX3_AbstractGrid3D* InGrid = (LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGrid);

        float xres = InGrid->xresolution();
        float yres = InGrid->yresolution();
        float zres = InGrid->zresolution();
        float NAd = 0;

        size_t xdim = InGrid->xdim();
        size_t ydim = InGrid->ydim();
        size_t zdim = InGrid->zdim();




        lvox::Grid3Df* itemOuti_CONTEUR = new lvox::Grid3Df(_FLAT_ModelName.completeName(), resultOut_grids,
                                                             InGrid->minX(), InGrid->minY(), InGrid->minZ(),
                                                             xdim, ydim, 1,
                                                             xres,yres,zres, NAd, NAd);
        group->addItemDrawable(itemOuti_CONTEUR);
        //qDebug() << xdim << ydim << zdim;

        //std::vector<float> outData;
        for(size_t xx = 0;xx < xdim; xx++)
        {
            for(size_t yy = 0; yy < ydim; yy++)
            {
                int nbValeur = 0;
                for(size_t zz = 0; zz < zdim; zz++)
                {
                    size_t index = 0;
                    InGrid->index(xx,yy,zz,index);
                    float currentValue = InGrid->valueAtIndexAsDouble(index);
                    //qDebug() << index << currentValue;
                    //qDebug() << InGrid->valueAtXYZ(xx,yy,zz);
                    if(currentValue>0)
                    {
                        if(m_SumTypeCollection.value(m_SumType) == Summation)
                        {
                            nbValeur += currentValue;
                        }
                        else
                        {
                            nbValeur ++;
                        }
                    }
                }
                itemOuti_CONTEUR->setValue(xx,yy,0,nbValeur);
                //itemOuti_CONTEUR->setValueAtXYZ(xx,yy,1,nbValeur);
                //outData.push_back(nbValeur);
                //qDebug() << nbValeur << itemOut_CONTEUR->setValueAtXYZ(xx,yy,0,nbValeur);
            }
        }
        //qDebug() << itemOut_CONTEUR->xdim() << itemOut_CONTEUR->ydim() <<itemOut_CONTEUR->zdim()<< itemOut_CONTEUR->sizeX() << itemOut_CONTEUR->sizeY() << itemOut_CONTEUR->sizeZ();
        //qDebug() << data.size();

        //for(int i = 0; i < outData.size();i++)
        /*{
            //qDebug() << data.at(i);
            itemOuti_CONTEUR->setValueAtIndex(i,outData.at(i));
        }*/

        itemOuti_CONTEUR->computeMinMax();
    }

}
