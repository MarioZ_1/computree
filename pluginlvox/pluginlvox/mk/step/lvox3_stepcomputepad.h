#ifndef LVOX3_STEPCOMPUTEPAD_H
#define LVOX3_STEPCOMPUTEPAD_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"
#include <QStack>

/*!
 * \class LVOX_StepComputePAD
 * \ingroup Steps_LVOX
 * \brief <b>Compute LAD grid from ni, nt, nb and deltaT grids.</b>
 *
 */

class LVOX3_StepComputePAD: public CT_AbstractStep
{
    Q_OBJECT

public:


    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    LVOX3_StepComputePAD(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    static float computePAD(float density, float xres,float yres, float zres, float D_Nt_mean, float gFunction);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    /*!
     * \brief computeVegetation
     *
     * Estimate the mean element cross section assuming simple element shape and size
     */
    void computeVegetation();

    /*!
     * \brief computeAttenuation
     *
     * Compute the attenuation coefficient of a single vegetation element;
     */
    void computeAttenuation(double volume);

    /*!
     * \brief computeStatistique
     * \param i index for the coputation of the statistics
     *
     * Not yep used
     */
    void computeStatistique(int i);

    /*!
     * \brief computeVariance
     *
     * Not yet used
     */
    void computeVariance();

    /*!
     * \brief computeIntervals
     *
     * Not yet used
     */
    void computeIntervals();

    /*!
     * \brief computeBias
     *
     * Not yep used
     */
    void computeBias();

private :

    QString _confidenceLevel;
    double _Lambda1;// attenuation coefficient of a single vegetation element
    bool _BCMLE;
    bool _MLE;
    bool _BCBL; // equal path
    bool _BL;
    bool _CF;
    bool _BC2BL;// unequal path
    bool    _EraseInfinity;
    double   _PADlimit;
    int _NThreshold;
    bool _grilleSup;

    CT_AutoRenameModels _PADCF_ModelName;
    CT_AutoRenameModels _PADBCMLE_ModelName;
    CT_AutoRenameModels _PADMLE_ModelName;
    CT_AutoRenameModels _PADBCBL_ModelName;
    CT_AutoRenameModels _PADBL_ModelName;
    CT_AutoRenameModels _PADBC2BL_ModelName;
    CT_AutoRenameModels _FreeSum_ModelName;
    CT_AutoRenameModels _FreeEffSum_ModelName;
    CT_AutoRenameModels _DeltaSum_ModelName;
    CT_AutoRenameModels _DeltaEffSum_ModelName;
    CT_AutoRenameModels _DeltaEffSquareSum_ModelName;
    CT_AutoRenameModels _1ZleDeltaEffSum_ModelName;


    QStack<size_t> _InfinityIndexStack;
    double _MeanElementCrossSection;
    double _I;
    double _deltaMean;
    double _freeMean;
    double _effectiveFreePath;
    double _effectiveDeltaPath;
    double _effectiveFreePath_Hits;
    double _estimatedAttenuationCoefficient;
    double _estimatedVariance;
    double _alpha;

    int _N = 0;

};

#endif // LVOX3_STEPCOMPUTEPAD_H
