/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginToolKit library.

 PluginToolKit is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginToolKit is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginToolKit.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "tk_stepmergeclouds.h"

#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_itemdrawable/ct_scene.h"

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_view/ct_buttongroup.h"


#include <math.h>
#include <iostream>
#include <QList>
#include <limits>

#define DEF_SearchInResult "rin"
#define DEF_SearchInGroup   "gin"
#define DEF_SearchInScene   "scin"

#define DEF_SearchOutResult "rout"
#define DEF_SearchOutGroup  "gout"
#define DEF_SearchOutScene  "scout"

TK_StepMergeClouds::TK_StepMergeClouds(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

QString TK_StepMergeClouds::getStepDescription() const
{
    return tr("Fusionner des nuages de points");
}

QString TK_StepMergeClouds::getStepDetailledDescription() const
{
    return tr("Cette étape permet de fusionner plusieurs nuages de points");
}

CT_VirtualAbstractStep* TK_StepMergeClouds::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new TK_StepMergeClouds(dataInit);
}

//////////////////// PROTECTED //////////////////

void TK_StepMergeClouds::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_SearchInResult, tr("Scène(s) à fusionner"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène à fusionner"));
}

// Création et affiliation des modèles OUT
void TK_StepMergeClouds::createOutResultModelListProtected()
{    
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_SearchOutResult, tr("Scène fusionnée"));

    resultModel->setRootGroup(DEF_SearchOutGroup);
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutScene, new CT_Scene(), tr("Scène fusionnée"));
}

void TK_StepMergeClouds::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void TK_StepMergeClouds::compute()
{
    // récupération du résultats IN et OUT
    CT_ResultGroup *inResult = getInputResults().first();
    CT_ResultGroup *outResult = getOutResultList().first();

    size_t n_points = 0;
    size_t i = 0;

    CT_ResultItemIterator it0(inResult, this, DEF_SearchInScene);
    while(!isStopped() && it0.hasNext())
    {
        const CT_AbstractItemDrawableWithPointCloud *in_scene = (CT_AbstractItemDrawableWithPointCloud*) it0.next();
        const CT_AbstractPointCloudIndex *pointCloudIndex = in_scene->getPointCloudIndex();
        n_points += pointCloudIndex->size();
    }

        CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
    resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

    CT_ResultItemIterator it(inResult, this, DEF_SearchInScene);
    while(!isStopped() && it.hasNext())
    {
        const CT_AbstractItemDrawableWithPointCloud *in_scene = (CT_AbstractItemDrawableWithPointCloud*) it.next();
        const CT_AbstractPointCloudIndex *pointCloudIndex = in_scene->getPointCloudIndex();

        // Extraction des points de la placette

        CT_PointIterator itP(pointCloudIndex);
        while(itP.hasNext() && (!isStopped()))
        {
            size_t index = itP.next().currentGlobalIndex();

            resPointCloudIndex->addIndex(index);

            // progres de 0 à 100
            if (i % 10000 == 0) {setProgress(90.0*i/n_points);}
            ++i;
        }
    }

    if (resPointCloudIndex->size() > 0)
    {
        resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

        // creation du groupe
        CT_StandardItemGroup *outGroup = new CT_StandardItemGroup(DEF_SearchOutGroup, outResult);

        // creation et ajout de la scene
        CT_Scene *outScene = new CT_Scene(DEF_SearchOutScene, outResult, PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));
        outScene->updateBoundingBox();
        outGroup->addItemDrawable(outScene);

        // ajout au résultat
        outResult->addGroup(outGroup);

        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène fusionnée comporte %1 points.")).arg(resPointCloudIndex->size()));
    } else {
        delete resPointCloudIndex;
        PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Aucun point à fusionner"));
    }

}
