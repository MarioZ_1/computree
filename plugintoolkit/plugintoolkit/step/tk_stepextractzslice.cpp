#include "tk_stepextractzslice.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include <assert.h>
#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"

#include <QDebug>

// Alias for indexing in models
#define DEF_inputResult "inputResult"
#define DEF_inGrp "inputGroup"
#define DEF_inScene "inputScene"

#define DEF_inItem "inItem"
#define DEF_inATTBot "inATTBot"
#define DEF_inATTTop "inATTTop"


// Constructor : initialization of parameters
TK_StepExtractZSlice::TK_StepExtractZSlice(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _botZ = 0.5;
    _topZ = 7.0;
}

// Step description (tooltip of contextual menu)
QString TK_StepExtractZSlice::getStepDescription() const
{
    return tr("Extraire les points dans une tranche horizontale");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepExtractZSlice::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepExtractZSlice(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractZSlice::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Z Minimum"), "m", -1e+10, 1e+10, 4, _botZ);
    configDialog->addDouble(tr("Z Maximum"), "m", -1e+10, 1e+10, 4, _topZ);
}

// Creation and affiliation of IN models
void TK_StepExtractZSlice::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_inGrp, DEF_inScene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_StepExtractZSlice::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_inputResult);

    if(res != NULL)
        res->addItemModel(DEF_inGrp, _outScene_ModelName, new CT_Scene(), tr("Extracted Scene"));
}

void TK_StepExtractZSlice::compute()
{

    CT_ResultGroup* res = getOutResultList().first();

    CT_ResultGroupIterator it(res, this, DEF_inGrp);
    while (it.hasNext() && !isStopped())
    {        
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) it.next();
        const CT_Scene* scene = (const CT_Scene*)grp->firstItemByINModelName(this, DEF_inScene);
        const CT_AbstractSingularItemDrawable* item = NULL;

        if (scene != NULL)
        {
            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector(); // 1) cretation of output pointcloud index
            extractedCloud->setSortType(CT_PointCloudIndexVector::NotSorted);

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext() && !isStopped())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(2) <= _topZ &&
                     point(2) >= _botZ)
                {
                    extractedCloud->addIndex(index); // 2) adding kept indices
                }

                setProgress( 99.0*i++ /nbPoints );
            }
            extractedCloud->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);


            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(_outScene_ModelName.completeName(), res, PS_REPOSITORY->registerPointCloudIndex(extractedCloud)); // 3) create scene, registering the pointcloudindex
                outScene->updateBoundingBox(); // 4) don't forget to update the bounding box, to be fitted to filtered points

                grp->addItemDrawable(outScene);
            }
        }
    }

    setProgress(100);
}
