#ifndef TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H
#define TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class TK_StepExtractZSliceFromAttributes : public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    TK_StepExtractZSliceFromAttributes(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPreConfigurationDialog();

    void createPostConfigurationDialog();


    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _botZ;
    double    _topZ;
    bool      _useFixedBot;
    bool      _useFixedTop;

    CT_AutoRenameModels    _outScene_ModelName;

};

#endif // TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H
