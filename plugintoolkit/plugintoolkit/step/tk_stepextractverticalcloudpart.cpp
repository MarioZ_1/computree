#include "tk_stepextractverticalcloudpart.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include <assert.h>
#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"

// Alias for indexing in models
#define DEF_inputResult "inputResult"
#define DEF_inGrp "inputGroup"
#define DEF_inScene "inputScene"

#define UPPER_MODE 0
#define LOWER_MODE 1
#define ABSOLUTE_TYPE 0
#define RELATIVE_TYPE 1

// Constructor : initialization of parameters
TK_StepExtractVerticalCloudPart::TK_StepExtractVerticalCloudPart(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _limitType = UPPER_MODE;
    _limitMethod = RELATIVE_TYPE;

    _absThickness = 5.0;
    _relThickness = 0.25;
}

// Step description (tooltip of contextual menu)
QString TK_StepExtractVerticalCloudPart::getStepDescription() const
{
    return tr("Extraire les points dans une tranche haute/basse");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepExtractVerticalCloudPart::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepExtractVerticalCloudPart(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepExtractVerticalCloudPart::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_inGrp, DEF_inScene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_StepExtractVerticalCloudPart::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_inputResult);

    if(res != NULL)
        res->addItemModel(DEF_inGrp, _outScene_ModelName, new CT_Scene(), tr("Extracted Scene"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepExtractVerticalCloudPart::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();


    CT_ButtonGroup &bgSliceType = configDialog->addButtonGroup(_limitType);
    configDialog->addExcludeValue(tr("Tranche haute"), "", "", bgSliceType, UPPER_MODE);
    configDialog->addExcludeValue(tr("Tranche basse"), "", "", bgSliceType, LOWER_MODE);

    configDialog->addEmpty();

    CT_ButtonGroup &bgMethod = configDialog->addButtonGroup(_limitMethod);
    configDialog->addExcludeValue(tr("Limite absolue"), "", "", bgMethod, ABSOLUTE_TYPE);
    configDialog->addDouble      (tr("    Seuil en valeur absolue"), "m", -1e+10, 1e+10, 2, _absThickness);

    configDialog->addEmpty();

    configDialog->addExcludeValue(tr("Limite relative"), "", "", bgMethod, RELATIVE_TYPE);
    configDialog->addDouble      (tr("    Seuil en valeur relative"), "%", 0, 100, 2, _relThickness, 100);

}

void TK_StepExtractVerticalCloudPart::compute()
{

    CT_ResultGroup* res = getOutResultList().first();

    CT_ResultGroupIterator it(res, this, DEF_inGrp);
    while (it.hasNext() && !isStopped())
    {        
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) it.next();
        const CT_Scene* scene = (const CT_Scene*)grp->firstItemByINModelName(this, DEF_inScene);

        if (scene != NULL)
        {
            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();
            extractedCloud->setSortType(CT_AbstractCloudIndex::NotSorted);

            // Cloud bounding box
            Eigen::Vector3d min;
            Eigen::Vector3d max;
            scene->getBoundingBox(min, max);

            double thickness = _absThickness;
            if (_limitMethod == RELATIVE_TYPE)
            {
                thickness = (max(2) - min(2))*_relThickness;
            }

            double zmin = -std::numeric_limits<double>::max();
            double zmax = std::numeric_limits<double>::max();

            if (_limitType == UPPER_MODE)
            {
                zmin = max(2) - thickness;
            } else {
                zmax = min(2) + thickness;
            }

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext() && !isStopped())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if (point(2) <= zmax && point(2) >= zmin)
                {
                    extractedCloud->addIndex(index);
                }

                setProgress( 100.0*i++ /nbPoints );
                }

            if (extractedCloud->size() > 0)
            {
                extractedCloud->setSortType(CT_AbstractCloudIndex::SortedInAscendingOrder);
                CT_Scene* outScene = new CT_Scene(_outScene_ModelName.completeName(), res, PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                grp->addItemDrawable(outScene);
            }
        }
    }
}
