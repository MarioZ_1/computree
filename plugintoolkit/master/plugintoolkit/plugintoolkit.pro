CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

# In file "include.pri" it will test if pcl was found or not. If it was not found a warning message will be displayed but no error.
CHECK_CAN_USE_PCL = 1

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

# if PCL was found
contains(DEFINES, USE_PCL) {
    # we use the pcl library of computree
    COMPUTREE += ctlibpcl
}

include($${CT_PREFIX}/include_ct_library.pri)

TARGET = plug_toolkit

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    tk_pluginentry.h \
    tk_steppluginmanager.h \
    step/tk_translatecloud.h \
    step/tk_stepscalecloud.h \
    step/tk_steprotatecloud.h \
    step/tk_stepcentercloud.h \
    step/tk_stepextractbox.h \
    step/tk_stepextractcylinder.h \
    step/tk_stepextractsphere.h \
    step/tk_stepcolorbyaxis.h \
    step/tk_stepcolorrandom.h \
    step/tk_stepestimatenormalsrandom.h \
    step/tk_stepfacecolorrandom.h \
    step/tk_stepextractbboxpart.h \
    step/tk_stepextractzslicefromattributes.h \
    step/tk_stepsplitscene.h \
    step/tk_stepnormalestimator.h \
    step/tk_stepextractzslice.h \
    step/tk_stepextractverticalcloudpart.h \
    step/tk_stepmergeclouds.h \
    step/tk_stepfacenormalestimator.h \
    step/tk_stepfilterpointsbyattribute.h
SOURCES += \
    tk_pluginentry.cpp \
    tk_steppluginmanager.cpp \
    step/tk_translatecloud.cpp \
    step/tk_stepscalecloud.cpp \
    step/tk_steprotatecloud.cpp \
    step/tk_stepcentercloud.cpp \
    step/tk_stepextractbox.cpp \
    step/tk_stepextractcylinder.cpp \
    step/tk_stepextractsphere.cpp \
    step/tk_stepcolorbyaxis.cpp \
    step/tk_stepcolorrandom.cpp \
    step/tk_stepestimatenormalsrandom.cpp \
    step/tk_stepfacecolorrandom.cpp \
    step/tk_stepextractbboxpart.cpp \
    step/tk_stepextractzslicefromattributes.cpp \
    step/tk_stepsplitscene.cpp \
    step/tk_stepnormalestimator.cpp \
    step/tk_stepextractzslice.cpp \
    step/tk_stepextractverticalcloudpart.cpp \
    step/tk_stepmergeclouds.cpp \
    step/tk_stepfacenormalestimator.cpp \
    step/tk_stepfilterpointsbyattribute.cpp

TRANSLATIONS += languages/plugintoolkit_en.ts \
                languages/plugintoolkit_fr.ts

