#ifndef TK_PLUGINENTRY_H
#define TK_PLUGINENTRY_H

#include "interfaces.h"

class TK_StepPluginManager;

class TK_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    TK_PluginEntry();
    ~TK_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    TK_StepPluginManager *_stepPluginManager;
};

#endif // TK_PLUGINENTRY_H
