#include "tk_stepfacenormalestimator.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractitemdrawable.h"
#include "ct_itemdrawable/accessibility/ct_iaccessfacecloud.h"

#include "ct_itemdrawable/ct_faceattributesnormal.h"
#include "ct_normalcloud/ct_normalcloudstdvector.h"

#include "ct_accessor/ct_pointaccessor.h"
#include "ct_iterator/ct_faceiterator.h"
#include "ct_mesh/ct_face.h"
#include "ct_mesh/ct_edge.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

TK_StepFaceNormalEstimator::TK_StepFaceNormalEstimator(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

QString TK_StepFaceNormalEstimator::getStepDescription() const
{
    return tr("Calcul des normales des faces");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepFaceNormalEstimator::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepFaceNormalEstimator(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepFaceNormalEstimator::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_AbstractItemDrawable::staticGetType(), tr("Item(s)"), tr("Item(s) contenant des faces"));
}

// Creation and affiliation of OUT models
void TK_StepFaceNormalEstimator::createOutResultModelListProtected()
{
    // ****************************************************
    // On sort un resultats correspondant a l'attribut de hauteur que l'on va pouvoir normalier :
    //  - un groupe d'item (groupe de hauteur)
    //      - un item (attribut hauteurs)
    // ****************************************************

    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if(resultModel != NULL)
        resultModel->addItemModel(DEF_groupIn_inputScene, m_itemOutNormalModelName, new CT_FaceAttributesNormal(), tr("Random Normals"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepFaceNormalEstimator::createPostConfigurationDialog()
{
    // No dialog box
}

void TK_StepFaceNormalEstimator::compute()
{
    CT_ResultGroup* resultOut_normal = getOutResultList().first();

    CT_ResultGroupIterator it(resultOut_normal, this, DEF_groupIn_inputScene);

    CT_PointAccessor pAccess;
    CT_Point p1;
    CT_Point p2;
    CT_Point p3;

    while(it.hasNext())
    {
        CT_AbstractItemGroup* groupOut = (CT_AbstractItemGroup*)it.next();

        const CT_IAccessFaceCloud* itemIn_scene = dynamic_cast<const CT_IAccessFaceCloud*>(groupOut->firstItemByINModelName(this, DEF_itemIn_scene));

        if(itemIn_scene != NULL)
        {
            const CT_AbstractFaceCloudIndex* faceIndexes = itemIn_scene->getFaceCloudIndex();

            // On declare un nuage de normales que l'on va remplir
            CT_NormalCloudStdVector *normalCloud = new CT_NormalCloudStdVector( faceIndexes->size() );

            size_t index = 0;

            CT_FaceIterator fIt(faceIndexes);

            while(fIt.hasNext()) {
                fIt.next();
                const CT_Face& face = fIt.cT();

                pAccess.pointAt(face.iPointAt(0), p1);
                pAccess.pointAt(face.iPointAt(1), p2);
                pAccess.pointAt(face.iPointAt(2), p3);

                const CT_Point vu = p2-p1;
                const CT_Point vv = p3-p1;
                const CT_Point normal = vu.normalized().cross(vv).normalized();

                CT_Normal normalTmp(normal[0], normal[1], normal[2], 0);
                normalCloud->replaceNormal(index, normalTmp);
                index++;
            }

            // On cree les attributs que l'on met dans le groupe
            CT_FaceAttributesNormal* normalAttribute = new CT_FaceAttributesNormal(m_itemOutNormalModelName.completeName(), resultOut_normal, itemIn_scene->getFaceCloudIndexRegistered(), normalCloud);
            groupOut->addItemDrawable( normalAttribute );
        }
    }
}
