#ifndef TK_STEPNORMALESTIMATOR_H
#define TK_STEPNORMALESTIMATOR_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include "ct_normalcloud/tools/iestimatorobserverandcontroller.h"

/*!
 * \class TK_StepNormalEstimator
 * \ingroup Steps_TK
 * \brief <b>Estimation de normales dans un nuage de point.</b>
 *
 * No detailled description for this step
 *
 * \param _nbNeigbours 
 *
 */

class TK_StepNormalEstimator: public CT_AbstractStep, public IEstimatorObserverAndController
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    TK_StepNormalEstimator(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    void setEstimationProgress(int p);
    bool mustStopEstimation() const;
protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    int                     _nbNeigbours;
    double                  _precisionThreshold;
    CT_AutoRenameModels     m_autoRenameNormalsModel;
    CT_AutoRenameModels     m_autoRenameCurvatureModel;

};

#endif // TK_STEPNORMALESTIMATOR_H
