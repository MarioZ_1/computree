#include "tk_translatecloud.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_mutablepointiterator.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

#include <QDebug>

// Constructor : initialization of parameters
TK_TranslateCloud::TK_TranslateCloud(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _x = 0;
    _y = 0;
    _z = 0;

    // Cette etape est CT_debugable
    setDebuggable( true );
}

// Step description (tooltip of contextual menu)
QString TK_TranslateCloud::getStepDescription() const
{
    return tr("Translation des points");
}

// Step copy method
CT_VirtualAbstractStep* TK_TranslateCloud::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_TranslateCloud(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_TranslateCloud::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_TranslateCloud::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_groupIn_inputScene, _outSceneModelName, new CT_Scene(), tr("Translated Scene"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void TK_TranslateCloud::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Translation selon X"), "m", -1e+10, 1e+10, 4, _x);
    configDialog->addDouble(tr("Translation selon Y"), "m", -1e+10, 1e+10, 4, _y);
    configDialog->addDouble(tr("Translation selon Z"), "m", -1e+10, 1e+10, 4, _z);
}

void TK_TranslateCloud::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_groupIn_inputScene);
    while(!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Scene* itemIn_scene = (CT_Scene*) group->firstItemByINModelName(this, DEF_itemIn_scene);
            const CT_AbstractPointCloudIndex *cloudIndex = itemIn_scene->getPointCloudIndex();
            CT_PointIterator itP(cloudIndex);

            // On Cree un nouveau nuage qui sera le translate
            CT_NMPCIR translatedCloud = PS_REPOSITORY->createNewPointCloud(cloudIndex->size());
            CT_MutablePointIterator itPM(translatedCloud);

            // On cree le vecteur de translation
            Eigen::Vector3d translateVector(_x, _y, _z);

            size_t i = 0;
            // On applique la translation a tous les points du nuage
            while (itP.hasNext() && itPM.hasNext())
            {
                itP.next();
                itPM.next().replaceCurrentPoint(itP.currentPoint() + translateVector);

                // Barre de progression
                setProgress(100.0*i++ /cloudIndex->size());

                // On regarde si on est en debug mode
                waitForAckIfInDebugMode();
            }

            CT_Scene* itemOut_scene = new CT_Scene(_outSceneModelName.completeName(), outResult, translatedCloud);
            itemOut_scene->setBoundingBox( itemIn_scene->minX() + _x, itemIn_scene->minY() + _y, itemIn_scene->minZ() + _z, itemIn_scene->maxX() + _x, itemIn_scene->maxY() + _y, itemIn_scene->maxZ() + _z);

            group->addItemDrawable(itemOut_scene);
        }
    }
}
