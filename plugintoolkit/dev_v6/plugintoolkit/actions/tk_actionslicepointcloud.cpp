#include "actions/tk_actionslicepointcloud.h"

#include "documentinterface.h"
#include "painterinterface.h"

#include "ct_global/ct_context.h"
#include <QMouseEvent>
#include <QKeyEvent>
#include <QIcon>
#include <QPainter>

#include "math.h"


TK_ActionSlicePointCloud::TK_ActionSlicePointCloud_dataContainer::TK_ActionSlicePointCloud_dataContainer()
{
    _thickness = 0;
    _spacing = 0;
}

TK_ActionSlicePointCloud::TK_ActionSlicePointCloud(QList<CT_AbstractItemDrawableWithPointCloud *> *sceneList,
                                                   double xmin, double ymin, double zmin, double xmax, double ymax, double zmax,
                                                   TK_ActionSlicePointCloud_dataContainer *dataContainer) : CT_AbstractActionForGraphicsView()
{
    _sceneList = sceneList;
    _xmin = xmin;
    _ymin = ymin;
    _zmin = zmin;

    _xmax = xmax;
    _ymax = ymax;
    _zmax = zmax;

    _xwidth = std::abs(_xmax - _xmin);
    _ywidth = std::abs(_ymax - _ymin);

    _dataContainer = dataContainer;
}

TK_ActionSlicePointCloud::~TK_ActionSlicePointCloud()
{
}

QString TK_ActionSlicePointCloud::uniqueName() const
{
    return "TK_ActionSlicePointCloud";
}

QString TK_ActionSlicePointCloud::title() const
{
    return "Action Slicing";
}

QString TK_ActionSlicePointCloud::description() const
{
    return "Action Slicing";
}

QIcon TK_ActionSlicePointCloud::icon() const
{
    return QIcon(":/icons/select_rectangular.png");
}

QString TK_ActionSlicePointCloud::type() const
{
    return CT_AbstractAction::TYPE_MODIFICATION;
}

void TK_ActionSlicePointCloud::init()
{
    CT_AbstractActionForGraphicsView::init();

    if(nOptions() == 0)
    {
        // create the option widget if it was not already created
        TK_ActionSlicePointCloudOptions *option = new TK_ActionSlicePointCloudOptions(this);

        // add the options to the graphics view
        graphicsView()->addActionOptions(option);

        option->setThickness(_dataContainer->_thickness);
        option->setSpacing(_dataContainer->_spacing);

        connect(option, SIGNAL(parametersChanged()), this, SLOT(update()));

        // register the option to the superclass, so the hideOptions and showOptions
        // is managed automatically
        registerOption(option);
        for (int i = 0 ; i < _sceneList->size() ; i++)
        {
            document()->addItemDrawable(*(_sceneList->at(i)));
        }
        document()->redrawGraphics();
        dynamic_cast<GraphicsViewInterface*>(document()->views().first())->camera()->fitCameraToVisibleItems();
    }
}

void TK_ActionSlicePointCloud::update()
{
    TK_ActionSlicePointCloudOptions *option = static_cast<TK_ActionSlicePointCloudOptions*>(optionAt(0));

    _dataContainer->_thickness = option->getThickness();
    _dataContainer->_spacing = option->getSpacing();

    redrawOverlayAnd3D();
}

void TK_ActionSlicePointCloud::redrawOverlay()
{
    document()->redrawGraphics();
}

void TK_ActionSlicePointCloud::redrawOverlayAnd3D()
{
    setDrawing3DChanged();
    document()->redrawGraphics();
}

bool TK_ActionSlicePointCloud::mousePressEvent(QMouseEvent *e)
{
    TK_ActionSlicePointCloudOptions *option = static_cast<TK_ActionSlicePointCloudOptions*>(optionAt(0));

    if ((e->modifiers() & Qt::ControlModifier) || (e->modifiers()  & Qt::ShiftModifier))
    {
        if (e->buttons() & Qt::LeftButton)
        {
            option->decreaseIncrement();
            update();
            return true;
        } else if (e->buttons() & Qt::RightButton)
        {
            option->increaseIncrement();
            update();
            return true;
        }
    }

    return false;
}

bool TK_ActionSlicePointCloud::mouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    return false;
}

bool TK_ActionSlicePointCloud::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    return false;
}

bool TK_ActionSlicePointCloud::wheelEvent(QWheelEvent *e)
{
    TK_ActionSlicePointCloudOptions *option = static_cast<TK_ActionSlicePointCloudOptions*>(optionAt(0));

    if (e->modifiers()  & Qt::ControlModifier)
    {
        if (e->delta() > 0)
        {
            option->setThickness(option->getThickness() + option->getIncrement());
        } else if (e->delta() < 0)
        {
            option->setThickness(option->getThickness() - option->getIncrement());
        }
        update();
        return true;
    } else if (e->modifiers()  & Qt::ShiftModifier)
    {
        if (e->delta() > 0)
        {
            option->setSpacing(option->getSpacing() + option->getIncrement());
        } else if (e->delta() < 0)
        {
            option->setSpacing(option->getSpacing() - option->getIncrement());
        }
        update();
        return true;
    }

    return false;
}

bool TK_ActionSlicePointCloud::keyPressEvent(QKeyEvent *e)
{
    Q_UNUSED(e);
    return false;

}

bool TK_ActionSlicePointCloud::keyReleaseEvent(QKeyEvent *e)
{
    Q_UNUSED(e);
    return false;
}

void TK_ActionSlicePointCloud::draw(GraphicsViewInterface &view, PainterInterface &painter)
{
    Q_UNUSED(view)

    if (qFuzzyCompare(_dataContainer->_thickness, 0.0)) {return;}

    painter.save();

    QColor oldColor = painter.getColor();
    painter.setColor(QColor(0,125,0,100));

    double z_current = _zmin;
    while (z_current <= _zmax)
    {
        painter.fillRectXY(Eigen::Vector2d(_xmin, _ymin), Eigen::Vector2d(_xmin+_xwidth, _ymin+_ywidth), z_current);
        painter.fillRectXY(Eigen::Vector2d(_xmin, _ymin), Eigen::Vector2d(_xmin+_xwidth, _ymin+_ywidth), z_current + _dataContainer->_thickness);
        painter.drawRectXZ(Eigen::Vector2d(_xmin, z_current), Eigen::Vector2d(_xmin+_xwidth, z_current+_dataContainer->_thickness), _ymin);
        painter.drawRectXZ(Eigen::Vector2d(_xmin, z_current), Eigen::Vector2d(_xmin+_xwidth, z_current+_dataContainer->_thickness), _ymax);
        painter.drawRectYZ(Eigen::Vector2d(_ymin, z_current), Eigen::Vector2d(_ymin+_ywidth, z_current+_dataContainer->_thickness), _xmin);
        painter.drawRectYZ(Eigen::Vector2d(_ymin, z_current), Eigen::Vector2d(_ymin+_ywidth, z_current+_dataContainer->_thickness), _xmax);

        z_current += _dataContainer->_thickness;
        z_current += _dataContainer->_spacing;
    }
    painter.setColor(oldColor);
    painter.restore();
}

void TK_ActionSlicePointCloud::drawOverlay(GraphicsViewInterface &view, QPainter &painter)
{
    Q_UNUSED(view);
    Q_UNUSED(painter);
}

CT_AbstractAction* TK_ActionSlicePointCloud::createInstance() const
{
    return new TK_ActionSlicePointCloud(_sceneList, _xmin, _ymin, _zmin, _xmax, _ymax, _zmax, _dataContainer);
}
