#ifndef TK_STEPEXTRACTPLOTBASEDONDTM_H
#define TK_STEPEXTRACTPLOTBASEDONDTM_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_scene.h"

class TK_StepExtractPlotBasedOnDTM : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepExtractPlotBasedOnDTM();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double _zmin;                         /*!< Z minimum de la placette a extraire*/
    double _zmax;                         /*!< Z maximum de la placette a extraire*/

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;

    CT_HandleInResultGroup<0,1>                                         _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                       _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                               _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float>>                          _inDTM;

};

#endif // TK_STEPEXTRACTPLOTBASEDONDTM_H
