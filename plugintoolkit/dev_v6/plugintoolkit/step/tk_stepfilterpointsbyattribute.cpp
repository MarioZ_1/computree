#include "tk_stepfilterpointsbyattribute.h"

TK_StepFilterPointsByAttribute::TK_StepFilterPointsByAttribute() : SuperClass()
{
    _threshMin = -100;
    _threshMax = 100;
}


QString TK_StepFilterPointsByAttribute::description() const
{
    return tr("Filter les points sur un attribut");
}


CT_VirtualAbstractStep* TK_StepFilterPointsByAttribute::createNewInstance() const
{
    return new TK_StepFilterPointsByAttribute();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepFilterPointsByAttribute::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène à filter"));
    manager.addItem(_inGroup, _inAttribute, tr("Attribut"));
}


void TK_StepFilterPointsByAttribute::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène filtrée"));
}


void TK_StepFilterPointsByAttribute::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Valeur minimum (incluse)"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _threshMin);
    postInputConfigDialog->addDouble(tr("Valeur maximum (incluse)"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _threshMax);
}

void TK_StepFilterPointsByAttribute::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointAttributesScalar* inAttribute = group->singularItem(_inAttribute);

            if (inAttribute != nullptr)
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
                const CT_AbstractPointCloudIndex *attPointCloudIndex = inAttribute->pointCloudIndex();

                if (pointCloudIndex != nullptr && attPointCloudIndex != nullptr)
                {
                    size_t nbPoints = pointCloudIndex->size();

                    CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

                    CT_PointIterator itP(pointCloudIndex);

                    size_t i = 0;
                    while (itP.hasNext())
                    {
                        if (isStopped()) {delete extractedCloud; return;}

                        size_t index = itP.next().currentGlobalIndex();
                        size_t localIndex = attPointCloudIndex->indexOf(index);

                        double attVal = inAttribute->dValueAt(localIndex);

                        if ( attVal <= _threshMax && attVal >= _threshMin )
                        {
                            extractedCloud->addIndex(index);
                        }

                        setProgress(float(100.0*i++ /nbPoints));

                        waitForAckIfInDebugMode();
                    }

                    CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                    outScene->updateBoundingBox();
                    group->addSingularItem(_outScene, outScene);
                }
            }
        }
    }
}
