#include "tk_stepestimatenormalsrandom.h"

#include <time.h>

TK_StepEstimateNormalsRandom::TK_StepEstimateNormalsRandom() : SuperClass()
{
}


QString TK_StepEstimateNormalsRandom::description() const
{
    return tr("Créer normales aléatoires");
}


CT_VirtualAbstractStep* TK_StepEstimateNormalsRandom::createNewInstance() const
{
    return new TK_StepEstimateNormalsRandom();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepEstimateNormalsRandom::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène"));
}


void TK_StepEstimateNormalsRandom::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outNormals, tr("Normales"));
}


void TK_StepEstimateNormalsRandom::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            size_t nbPoints = inScene->pointCloudIndex()->size();

            // On declare un nuage de normales que l'on va remplir aleatoirement
            CT_NormalCloudStdVector *normalCloud = new CT_NormalCloudStdVector( nbPoints );

            srand(uint(time(nullptr)));

            for ( size_t i = 0 ; i < nbPoints ; i++ )
            {
                if (isStopped()) {delete normalCloud; return;}

                for ( int j = 0 ; j < 3 ; j++ )
                {
                    // Tir aleatoire entre -1000 et 1000, ca donnera une precision de 3 chiffres apres la virgule pour la normale
                    normalCloud->normalAt(i)[j] = (rand() % 2001) - 1000;
                }

                normalCloud->normalAt(i)[3] = 0;
                normalCloud->normalAt(i).normalize();

                setProgress(float(100.0*i /nbPoints));
                waitForAckIfInDebugMode();
            }

            // On cree les attributs que l'on met dans un groupe
            CT_PointsAttributesNormal* normalAttribute = new CT_PointsAttributesNormal(inScene->pointCloudIndexRegistered(), normalCloud);
            group->addSingularItem(_outNormals, normalAttribute);
        }
    }
}
