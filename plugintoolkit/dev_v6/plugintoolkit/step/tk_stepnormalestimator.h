#ifndef TK_STEPNORMALESTIMATOR_H
#define TK_STEPNORMALESTIMATOR_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"

#include "ct_normalcloud/tools/iestimatorobserverandcontroller.h"

class TK_StepNormalEstimator: public CT_AbstractStep, public IEstimatorObserverAndController
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepNormalEstimator();

    QString description() const;

    QString detailledDescription() const;

    QString getStepURL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

    void setEstimationProgress(int p);
    bool mustStopEstimation() const;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    int                     _nbNeigbours;
    double                  _precisionThreshold;

	CT_HandleInResultGroupCopy<>                            _inResult;
	CT_HandleInStdZeroOrMoreGroup                           _inZeroOrMoreRootGroup;
	CT_HandleInStdGroup<>                                   _inGroup;
	CT_HandleInItem<CT_AbstractItemDrawableWithPointCloud>  _inScene;
	CT_HandleOutItem<CT_Scene>                              _outScene;

};

#endif // TK_STEPNORMALESTIMATOR_H
