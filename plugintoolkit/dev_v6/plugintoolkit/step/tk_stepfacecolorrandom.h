#ifndef TK_STEPFACECOLORRANDOM_H
#define TK_STEPFACECOLORRANDOM_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_itemdrawable/ct_faceattributescolor.h"

class TK_StepFaceColorRandom : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepFaceColorRandom();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

	CT_HandleInResultGroupCopy<>                            _inResult;
	CT_HandleInStdZeroOrMoreGroup                           _inZeroOrMoreRootGroup;
	CT_HandleInStdGroup<>                                   _inGroup;
    CT_HandleInSingularItem<CT_MeshModel>                   _inMesh;
    CT_HandleOutSingularItem<CT_FaceAttributesColor>        _outFaceColors;

};

#endif // TK_STEPFACECOLORRANDOM_H
