#include "tk_stepslicepointcloud.h"
#include "documentinterface.h"
#include "interfacesforplugin.h"

#include <QMessageBox>

TK_StepSlicePointCloud::TK_StepSlicePointCloud() : SuperClass()
{
    _dataContainer = new TK_ActionSlicePointCloud::TK_ActionSlicePointCloud_dataContainer();
    _dataContainer->_thickness = 1.0;
    _dataContainer->_spacing = 0.0;
    _dataContainer->_zBase = 0;
    _manual = false;

    _xmin = 0;
    _ymin = 0;
    _zmin = 0;
    _xmax = 0;
    _ymax = 0;
    _zmax = 0;

    setManual(true);

    _m_doc = nullptr;

    _sceneList = new QList<CT_AbstractItemDrawableWithPointCloud*>();
}

TK_StepSlicePointCloud::~TK_StepSlicePointCloud()
{
    _sceneList->clear();
    delete _sceneList;
    delete _dataContainer;
}

QString TK_StepSlicePointCloud::description() const
{
    return tr("Découper une scène en Tranches Horizontales");
}

QString TK_StepSlicePointCloud::detailledDescription() const
{
    return tr("Action manuelle permettant de découper une scène d'entrée en tranches horizontales.<br>"
              "Il est possible d'en régler intéractivement :<br>"
              "- L'épaisseur (<b>_thickness</b>, en m)<br>"
              "- L'espacement entre deux tranches (<b>_spacing</b>, en m)<br>"
              "<br>"
              "N.B. : Cette étape peut également fonctionner en mode non interactif, avec les paramètres choisis dans la boite de configuration. ");
}

CT_VirtualAbstractStep* TK_StepSlicePointCloud::createNewInstance() const
{
    return new TK_StepSlicePointCloud();
}

//////////////////// PROTECTED METHODS //////////////////

void TK_StepSlicePointCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène à découper"));
}

void TK_StepSlicePointCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Tranche"));
    manager.addItem(_outGroup, _outCluster, tr("Points de la tranche"));
}

void TK_StepSlicePointCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Epaisseur des tranches :"), tr("m"), 0.001, 100000, 4, _dataContainer->_thickness);
    postInputConfigDialog->addDouble(tr("Espacement des tranches :"), tr("m"), 0, 100000, 4, _dataContainer->_spacing);
    postInputConfigDialog->addBool("","",tr("Choix interactif des paramètres"), _manual);
}

void TK_StepSlicePointCloud::compute()
{
    setManual(_manual);

    _xmin = std::numeric_limits<double>::max();
    _ymin = _xmin;
    _zmin = _xmin;

    _xmax = -_xmin;
    _ymax = -_xmin;
    _zmax = -_xmin;

    int nbSc = 0;


    for (CT_AbstractItemDrawableWithPointCloud* inScene : _inScene.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        _sceneList->append(inScene);
        
        Eigen::Vector3d min, max;
        inScene->boundingBox(min, max);
        
        if (min(0) < _xmin) {_xmin = min(0);}
        if (min(1) < _ymin) {_ymin = min(1);}
        if (min(2) < _zmin) {_zmin = min(2);}
        
        if (max(0) > _xmax) {_xmax = max(0);}
        if (max(1) > _ymax) {_ymax = max(1);}
        if (max(2) > _zmax) {_zmax = max(2);}
        ++nbSc;
    }

    _dataContainer->_zBase = _zmin;

    requestManualMode();

    int cpt = 0;
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();

            QMap<QPair<double, double>, CT_PointCluster*> levels;

            for(double base = _dataContainer->_zBase ; base < _zmax ; base = (base + _dataContainer->_thickness + _dataContainer->_spacing))
            {
                levels.insert(QPair<double, double>(base, base + _dataContainer->_thickness), new CT_PointCluster());
            }

            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext())
            {
                if (isStopped()) {return;}
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                QMapIterator<QPair<double, double>, CT_PointCluster*> it(levels);
                while (it.hasNext())
                {
                    it.next();
                    double zminLevel = it.key().first;
                    double zmaxLevel = it.key().second;
                    CT_PointCluster* cluster = it.value();

                    if ((point(2) >= zminLevel) && (point(2) < zmaxLevel))
                    {
                        cluster->addPoint(index, false);
                    }
                }
            }

            QMapIterator<QPair<double, double>, CT_PointCluster*> it(levels);
            while (it.hasNext())
            {
                it.next();
                CT_PointCluster* cluster = it.value();

                if (cluster->pointCloudIndex()->size() > 0)
                {
                    CT_StandardItemGroup* groupCl = new CT_StandardItemGroup();
                    group->addGroup(_outGroup, groupCl);
                    groupCl->addSingularItem(_outCluster, cluster);
                } else {
                    delete cluster;
                }
            }

            levels.clear();
        }
    }

    setProgress(float(100.0*cpt/nbSc));

    if (_manual)
    {
        _m_doc = nullptr;
    }
}

void TK_StepSlicePointCloud::initManualMode()
{    
    if(_m_doc == nullptr)
    {
//        // create a new 3D document
//        _m_doc = guiContext()->documentManager()->new3DDocument();

        TK_ActionSlicePointCloud* action = new TK_ActionSlicePointCloud(_sceneList, _xmin, _ymin, _zmin, _xmax, _ymax, _zmax, _dataContainer);
//        // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
//        _m_doc->setCurrentAction(action, false);

        GuiContextInterface* context = this->guiContext();
        context->actionsManager()->addAction(action);
         _m_doc = context->documentManager()->new3DDocument();
         _m_doc->setCurrentAction(action);
    }

    QMessageBox::information(nullptr,
                             tr("Mode manuel"),
                             tr("Bienvenue dans le mode manuel de cette étape.\n"
                                "Veuillez sélectionner les paramètres pour réaliser les tranches."),
                             QMessageBox::Ok);
}

void TK_StepSlicePointCloud::useManualMode(bool quit)
{
    Q_UNUSED(quit);
}
