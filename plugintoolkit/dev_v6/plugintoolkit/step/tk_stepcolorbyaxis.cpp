#include "tk_stepcolorbyaxis.h"

#define OX 0
#define OY 1
#define OZ 2

TK_StepColorByAxis::TK_StepColorByAxis() : SuperClass()
{
    _axis = OZ;
}


QString TK_StepColorByAxis::description() const
{
    return tr("Coloriser points selon X/Y/Z");
}


CT_VirtualAbstractStep* TK_StepColorByAxis::createNewInstance() const
{
    return new TK_StepColorByAxis();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepColorByAxis::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Couleurs"));
}


void TK_StepColorByAxis::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    QString chosenAxis;
    switch ( _axis )
    {
    case OX :
    {
        chosenAxis = "Ox";
        break;
    }

    case OY :
    {
        chosenAxis = "Oy";
        break;
    }

    case OZ :
    {
        chosenAxis = "Oz";
        break;
    }

    default :
    {
        break;
    }
    }
    QString itemName = tr("Colored with axis ") + chosenAxis;

    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outPointAttributes, tr("Attributs couleur"));
}


void TK_StepColorByAxis::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText("Colour with axis","","");

    CT_ButtonGroup &axisChoice = postInputConfigDialog->addButtonGroup( _axis );
    postInputConfigDialog->addExcludeValue("", "", tr("Axe X"), axisChoice, OX);
    postInputConfigDialog->addExcludeValue("", "", tr("Axe Y"), axisChoice, OY);
    postInputConfigDialog->addExcludeValue("", "", tr("Axe Z"), axisChoice, OZ);
}

void TK_StepColorByAxis::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *cloudIndex = inScene->pointCloudIndex();
            CT_PointIterator itP(cloudIndex);

            // On declare un tableau d'attributs double que l'on va remplir avec les coordonnéees correspondant a l'axe demandée
            CT_StandardCloudStdVectorT<double> *attribute = new CT_StandardCloudStdVectorT<double>(cloudIndex->size());

            size_t i = 0;
            // On applique la translation a tous les points du nuage
            while (itP.hasNext())
            {
                if (isStopped()) {delete attribute; return;}

                attribute->tAt(i) = itP.next().currentPoint()[_axis];

                setProgress(float(100.0*i++ /cloudIndex->size()));
                waitForAckIfInDebugMode();
            }

            // On recupere le min et max de l'attribut en fonction de l'axe choisi
            double minAttribute, maxAttribute;
            switch ( _axis )
            {
            case OX :
            {
                minAttribute = inScene->minX();
                maxAttribute = inScene->maxX();
                break;
            }

            case OY :
            {
                minAttribute = inScene->minY();
                maxAttribute = inScene->maxY();
                break;
            }

            case OZ :
            {
                minAttribute = inScene->minZ();
                maxAttribute = inScene->maxZ();
                break;
            }

            default :
            {
                break;
            }
            }

            // On cree les attributs que l'on met dans un groupe
            CT_PointsAttributesScalarTemplated<double>*  outAttribute  = new CT_PointsAttributesScalarTemplated<double>(inScene->pointCloudIndexRegistered(), attribute, minAttribute, maxAttribute);

            group->addSingularItem(_outPointAttributes, outAttribute);
        }
    }

}
