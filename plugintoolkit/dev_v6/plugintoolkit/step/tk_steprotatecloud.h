#ifndef TK_STEPROTATECLOUD_H
#define TK_STEPROTATECLOUD_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"

class TK_StepRotateCloud : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepRotateCloud();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private :

    // Step parameters
    double    _alpha;
    double    _axisX;
    double    _axisY;
    double    _axisZ;
    double    _x;
    double    _y;
    double    _z;

    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>  _inScene;
    CT_HandleOutSingularItem<CT_Scene>                              _outScene;

};

#endif // TK_STEPROTATECLOUD_H
