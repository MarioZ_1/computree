/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef TREETOPOLOGY_H
#define TREETOPOLOGY_H

#include <vector>
#include "treesegment.h"
#include "plot/isosurface.h"

typedef struct {
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
    double radius;
    int branchId;
    bool isFirst;
} cylinder;

class treeTopology
{
public:
    treeTopology(){numberOfCylinders=0;}
    void addSegment(treeSegment seg){listSegment.push_back(seg);}
    void refineCylinders();

    std::vector<cylinder>& getListCylinders(){return listCylinders;}

    int getNumberOfCylinders(){return numberOfCylinders;}

    const std::map<unsigned int, pcl::PointXYZ>& getMapPoints() const {return mapPoints;}
    const std::vector<triangle>& getTriangles() const {return listTriangles;}

    void prepareForexport();

private:
    std::vector<treeSegment> listSegment;
    std::vector<cylinder> listCylinders;

    int numberOfCylinders;

    std::map<unsigned int, pcl::PointXYZ> mapPoints;
    std::vector<triangle> listTriangles;
};

#endif // TREETOPOLOGY_H
