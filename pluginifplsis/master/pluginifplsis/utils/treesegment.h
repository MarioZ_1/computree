/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef TREESEGMENT_H
#define TREESEGMENT_H

#include <map>
#include <vector>

class treeSegment
{
public:
    treeSegment(bool isF, int bId);

    void insertProperties(std::string key, double value){properties.insert(make_pair(key,value));}
    double getProperties(std::string key){return properties.at(key);}
    bool isAProperties(std::string key){if ( properties.find(key) == properties.end() ){return false;}else{return true;}}

    bool isFirstSegment(){return isFirst;}
    int getId(){return branchId;}

private:

    bool isFirst;
    int branchId;
    std::map<std::string,double> properties;
};

#endif // TREESEGMENT_H
