/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef VOLUMEDRAWERHELPER_H
#define VOLUMEDRAWERHELPER_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

class volumeDrawerHelper
{
public:
    volumeDrawerHelper(){}
    volumeDrawerHelper(pcl::PointCloud<pcl::PointXYZ>& pts, std::vector<std::vector<int>> inter, std::vector<std::vector<int>> below , std::vector<std::vector<int>> above, std::vector<std::vector<std::vector<int> > > l, std::vector<std::vector<std::vector<int> > > e);
     ~volumeDrawerHelper(){}

    std::vector<std::vector<int>>& getInter(){return inter;}
    std::vector<std::vector<int>>& getBelow(){return below;}
    std::vector<std::vector<int>>& getAbove(){return above;}
    std::vector<std::vector<std::vector<int>>>& getLine(){return lineTop;}
    std::vector<std::vector<std::vector<int>>>& getElements(){return elements;}
    pcl::PointCloud<pcl::PointXYZ>& getPtsCloud(){return pts;}

private:

    pcl::PointCloud<pcl::PointXYZ> pts;
    std::vector<std::vector<int>> inter, below, above;
    std::vector<std::vector<std::vector<int>>> lineTop,elements;

};

#endif // VOLUMEDRAWERHELPER_H
