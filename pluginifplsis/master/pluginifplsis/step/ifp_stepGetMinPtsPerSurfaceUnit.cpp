/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_stepGetMinPtsPerSurfaceUnit.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#define DEF_SearchInResult "rin"
#define DEF_SearchInGroup   "gin"
#define DEF_SearchInScene   "scin"

// On recupere nuage de points et nuages d'index
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"

#include <limits>

#include <fstream>
#include <iostream>

// Constructor : initialization of parameters
IFP_stepGetMinPtsPerSurface::IFP_stepGetMinPtsPerSurface(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _res = 0.2;
}

// Step description (tooltip of contextual menu)
QString IFP_stepGetMinPtsPerSurface::getStepDescription() const
{
    return tr("Filter the point cloud to keep only the minimum points");
}

QString IFP_stepGetMinPtsPerSurface::getStepDetailledDescription() const
{
    return tr("The point cloud is projected into a fine regular 2D (x,y) grid and further are selected the points of minimum elevation in each cell."
              "The resulting set of points serves as input data for the digital terrain model (DTM) algorithm."
              "Generally speaking, the size of the 2D grid should be selected according to the expected DTM accuracy. "
              "The smaller the grid size, the higher the point density and the finer the DTM.");
}

// Step copy method
CT_VirtualAbstractStep* IFP_stepGetMinPtsPerSurface::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new IFP_stepGetMinPtsPerSurface(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

void IFP_stepGetMinPtsPerSurface::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scène"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_Scene::staticGetType(), tr("Scène"));
}

// Création et affiliation des modèles OUT
void IFP_stepGetMinPtsPerSurface::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_SearchInGroup, _outsceneModelName, new CT_Scene(), tr("Scène extraite"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void IFP_stepGetMinPtsPerSurface::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Resolution of the 2d frame", "", 0.001, 100, 3, _res, 0);
}

void IFP_stepGetMinPtsPerSurface::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator itrGr(outResult, this, DEF_SearchInGroup);
    while (itrGr.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itrGr.next();
        const CT_Scene *itemIn_in = (CT_Scene*) group->firstItemByINModelName(this, DEF_SearchInScene);

        if (itemIn_in != NULL)
        {
            const CT_AbstractPointCloudIndex*   indexCloud = itemIn_in->getPointCloudIndex();

            _minx = itemIn_in->minX();
            _miny = itemIn_in->minY();
            _minz = itemIn_in->minZ();

            _dimx = (size_t)floor((itemIn_in->maxX() - _minx)/_res)+1;
            _dimy = (size_t)floor((itemIn_in->maxY() - _miny)/_res)+1;
            _dimz = (size_t)floor((itemIn_in->maxZ() - _minz)/_res)+1;

            size_t n_points = indexCloud->size();
            QMap<size_t, size_t> indexMap;

            // Extraction des points de la placette
            size_t i = 0;

            CT_PointIterator itP(indexCloud);
            CT_PointAccessor pAccess;

            i=0;
            while (!isStopped() && itP.hasNext())
            {
                const CT_Point &point =itP.next().currentPoint();
                size_t pointIndex = itP.cIndex();

                size_t col, row;
                size_t grdIndex = gridIndex(point(0), point(1), col, row);
                size_t previousPointGlobalIndex = indexMap.value(grdIndex, std::numeric_limits<size_t>::max());

                if (previousPointGlobalIndex == std::numeric_limits<size_t>::max())
                {
                    indexMap.insert(grdIndex, pointIndex);
                } else {
                    const CT_Point &previousPoint = pAccess.constPointAt(previousPointGlobalIndex);
                    if (point(2) < previousPoint(2))
                    {
                        indexMap.insert(grdIndex, pointIndex);
                    }
                }
                i++;
                if (i % 10000 == 0) {setProgress(100 * i / n_points);}
            }

            size_t npts = indexMap.size();

            float occupancy = ((float)npts)/((float)_dimx*_dimy);
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("2D grid occupancy rate: %1 %.")).arg((int)(occupancy*100)));

            CT_PointCloudIndexVector *indexCloudMin = new CT_PointCloudIndexVector(npts);
            indexCloudMin->setSortType(CT_AbstractCloudIndex::NotSorted);
            QMapIterator<size_t, size_t> it(indexMap);

            i = 0;
            while (it.hasNext() && (!isStopped()))
            {
                it.next();
                indexCloudMin->replaceIndex(i, it.value());
                if (i % 10000 == 0) {setProgress(50 + 49 * i / npts);}
                ++i;
            }

            // ------------------------------
            // Create OUT groups and items

            if (indexCloudMin->size() > 0)
            {
                // creation et ajout de la scene
                CT_Scene *outScene = new CT_Scene(_outsceneModelName.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexCloudMin));
                outScene->updateBoundingBox();
                group->addItemDrawable(outScene);


                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène de densité réduite comporte %1 points.")).arg(outScene->getPointCloudIndex()->size()));
            } else {

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Aucun point conservé pour cette scène"));
            }
        }
    }
}

size_t IFP_stepGetMinPtsPerSurface::gridIndex(const float &x, const float &y, size_t &colx, size_t &liny) const
{
    colx = (size_t) floor((x - _minx) / _res);
    liny = (size_t) floor((y - _miny) / _res);

    return liny*_dimx + colx;
}
