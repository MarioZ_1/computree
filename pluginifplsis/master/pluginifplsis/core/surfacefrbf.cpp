/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "surfacefrbf.h"
#include "wendlandRbf.h"

#include <pcl/common/common.h>

#include <time.h>

#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>

#include <fstream>

surfaceFrbf::surfaceFrbf(const pcl::PointCloud<pcl::PointXYZ> &pCloud, float pThreshold, int nbrPointsMin, float minSizeLeaf, float histoRange, int histoWindowSizeSmooth, float thresholdErrorBetweenNeighbors)
{
    clock_t t = clock();
    threshold = pThreshold;
    tree = new quadTree(pCloud,threshold, nbrPointsMin, minSizeLeaf, histoRange, histoWindowSizeSmooth, thresholdErrorBetweenNeighbors);
    t = clock() - t;
    std::cout<<"tree : "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    t = clock();

    graphOfLeafNeighborhood = new graph(*tree);
    t = clock() - t;
    std::cout<<"graph : "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    t = clock();

    tree->update();
    t = clock() - t;
    std::cout<<"tree update : "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    t = clock();

    graphOfLeafNeighborhood->updateRadius(*tree);
    t = clock() - t;
    std::cout<<"update radius : "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    t = clock();

    buildSpheres();
    t = clock() - t;
    std::cout<<"spheres : "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(pCloud));
    pcl::getMinMax3D (*cloud_pts_ptr, BBMin, BBMax);
    BBMin.z = tree->getMin().z-0.2;
    BBMax.z = tree->getMax().z+0.2;
}

void surfaceFrbf::buildSpheres()
{
    for(std::size_t i=0;i<tree->getListLeaf().size();i++)
    {
        sphereSupport s(tree->getListLeaf().at(i),tree->getPts());
        listSphere.push_back(s);
    }

}

void surfaceFrbf::fillScalarField(scalarField &field)
{
    pcl::PointXYZ min = field.getBBmin();

    int nbDivX = field.getXNode()-1;
    int nbDivY = field.getYNode()-1;
    int nbDivZ = field.getZNode()-1;

    float sizeCellX = field.getSizeCellX();
    float sizeCellY = field.getSizeCellY();
    float sizeCellZ = field.getSizeCellZ();

    for(std::size_t m=0;m<listSphere.size();m++)
    {
        int coordCenterX, coordCenterY, coordCenterZ;
        coordCenterX = (int)((listSphere.at(m).getCenter().x-min.x)/sizeCellX);
        coordCenterY = (int)((listSphere.at(m).getCenter().y-min.y)/sizeCellY);
        coordCenterZ = (int)((listSphere.at(m).getCenter().z-min.z)/sizeCellZ);
        int stepX = (int)(listSphere.at(m).getRadius()/sizeCellX)+1;
        int stepY = (int)(listSphere.at(m).getRadius()/sizeCellY)+1;
        int stepZ = (int)(listSphere.at(m).getRadius()/sizeCellZ)+1;

        for(int k=coordCenterZ-stepZ;k<coordCenterZ+stepZ;k++)
        {
            for(int j=coordCenterY-stepY;j<coordCenterY+stepY;j++)
            {
                for(int i=coordCenterX-stepX;i<coordCenterX+stepX;i++)
                {
                    if(i>=0 && i<nbDivX+1 && j>=0 && j<nbDivY+1 && k>=0 && k<nbDivZ+1){

                        float x = (float)i*sizeCellX+min.x;
                        float y = (float)j*sizeCellY+min.y;
                        float z = (float)k*sizeCellZ+min.z;

                        float sum = 0.;
                        if(field.getValue(i,j,k) != scalarField::NODATA)
                        {
                            sum = field.getValue(i,j,k);
                        }

                        bool hasBeenUpdated = false;

                        float tmpx = (listSphere.at(m).getCenter().x-x);
                        float tmpy = (listSphere.at(m).getCenter().y-y);
                        float tmpz = (listSphere.at(m).getCenter().z-z);
                        float distR = sqrt(tmpx*tmpx+tmpy*tmpy+tmpz*tmpz);

                        float r = distR/listSphere.at(m).getRadius();

                        if(r<1.)
                        {
                            float res = listSphere.at(m).getLocalRef().getValueOfImplicitFunction(x,y,z);
                            sum += (res)*wendlandRbf::getValueRbfWendland(r);

                            hasBeenUpdated = true;
                        }

                        if(hasBeenUpdated)field.setValue(i,j,k,sum);
                    }
                }
            }
        }
    }
}
