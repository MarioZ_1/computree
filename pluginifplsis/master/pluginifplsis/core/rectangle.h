/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

class rectangle
{
public:

    rectangle(float pXMin, float pXMax, float pYMin, float pYMax);

    float getdeltaX () const {return deltaX;}
    float getdeltaY () const {return deltaY;}

    float getXMin() const {return xMin;}
    float getYMin() const {return yMin;}
    float getXMax() const {return xMax;}
    float getYMax() const {return yMax;}

    void keepPointIn(pcl::PointCloud<pcl::PointXYZ>   pPts,pcl::PointCloud<pcl::PointXYZ>   &pPtsOut);

private:

float xMin,xMax,yMin,yMax;
float deltaX,deltaY;

};

#endif // RECTANGLE_H
