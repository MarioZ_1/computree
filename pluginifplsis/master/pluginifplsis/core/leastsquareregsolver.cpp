/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "leastsquareregsolver.h"

#include <vector>

#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_matrix.h>

//error = weighted sums of squared residuals
void leastSquareRegSolver::solveQuadric(pcl::PointXYZ centroid, float radius, bool useWeight, pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c, *w ;

     //gsl_set_error_handler_off();

    int n = pts.size();
    X = gsl_matrix_alloc (n, NBR_COEFS);
    y = gsl_vector_alloc (n);

    if(useWeight) w = gsl_vector_alloc (n);

    c = gsl_vector_alloc (NBR_COEFS);
    cov = gsl_matrix_alloc (NBR_COEFS, NBR_COEFS);

    for (int i = 0; i < n; i++)
    {
        gsl_matrix_set (X, i, 0, pts.at(i).x*pts.at(i).x);
        gsl_matrix_set (X, i, 1, pts.at(i).x*pts.at(i).y);
        gsl_matrix_set (X, i, 2, pts.at(i).y*pts.at(i).y);
        gsl_matrix_set (X, i, 3, pts.at(i).x);
        gsl_matrix_set (X, i, 4, pts.at(i).y);
        gsl_matrix_set (X, i, 5, 1.0);

        if(useWeight){
            float tmpx = (centroid.x-pts.at(i).x);
            float tmpy = (centroid.y-pts.at(i).y);
            float tmpz = (centroid.z-pts.at(i).z);
            float distR = sqrt(tmpx*tmpx+tmpy*tmpy+tmpz*tmpz);

            float r = distR/radius;
            gsl_vector_set (w, i, std::pow(1-r,4.0)*(1.+4.*r));
        }

        gsl_vector_set (y, i, pts.at(i).z);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, NBR_COEFS);

    if(useWeight){
        gsl_multifit_wlinear(X, w, y, c, cov,error, work);
    }else{
        gsl_multifit_linear (X, y, c, cov,error, work);
    }

    //error normalisation
    (*error) = sqrt((*error)/(double)n);

    gsl_multifit_linear_free (work);

    for(int i=0;i<NBR_COEFS;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

void leastSquareRegSolver::solvePlane(pcl::PointXYZ centroid, pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c;

     //gsl_set_error_handler_off();

    int n = pts.size();
    int nbCoefPlane = 2;
    X = gsl_matrix_alloc (n, nbCoefPlane);
    y = gsl_vector_alloc (n);

    c = gsl_vector_alloc (nbCoefPlane);
    cov = gsl_matrix_alloc (nbCoefPlane, nbCoefPlane);

    for (int i = 0; i < n; i++)
    {
        gsl_matrix_set (X, i, 0, pts.at(i).x-centroid.x);
        gsl_matrix_set (X, i, 1, pts.at(i).y-centroid.y);

        gsl_vector_set (y, i, pts.at(i).z-centroid.z);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nbCoefPlane);

    gsl_multifit_linear (X, y, c, cov,error, work);

    gsl_multifit_linear_free (work);

    for(int i=0;i<nbCoefPlane;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

void leastSquareRegSolver::solveCylinder(pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c;

    int n = pts.size();
    int nbCoefCylinder = 3;
    X = gsl_matrix_alloc (n, nbCoefCylinder);
    y = gsl_vector_alloc (n);

    c = gsl_vector_alloc (nbCoefCylinder);
    cov = gsl_matrix_alloc (nbCoefCylinder, nbCoefCylinder);

    for (int i = 0; i < n; i++)
    {
        gsl_matrix_set (X, i, 0, pts.at(i).x*pts.at(i).x);
        gsl_matrix_set (X, i, 1, pts.at(i).y*pts.at(i).y);
        gsl_matrix_set (X, i, 2, pts.at(i).z*pts.at(i).z);
        gsl_vector_set (y, i, 1.0);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nbCoefCylinder);

    gsl_multifit_linear (X, y, c, cov,error, work);

    gsl_multifit_linear_free (work);

    for(int i=0;i<nbCoefCylinder;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

void leastSquareRegSolver::solveCylinderAngle(pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c;

    int n = pts.size();
    int nbCoefCylinder = 3;
    X = gsl_matrix_alloc (n, nbCoefCylinder);
    y = gsl_vector_alloc (n);

    c = gsl_vector_alloc (nbCoefCylinder);
    cov = gsl_matrix_alloc (nbCoefCylinder, nbCoefCylinder);

    for (int i = 0; i < n; i++)
    {
        gsl_matrix_set (X, i, 0, pts.at(i).x*pts.at(i).x);
        gsl_matrix_set (X, i, 1, pts.at(i).x*pts.at(i).y);
        gsl_matrix_set (X, i, 2, pts.at(i).y*pts.at(i).y);

        gsl_vector_set (y, i, 1.0);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nbCoefCylinder);

    gsl_multifit_linear (X, y, c, cov,error, work);

    gsl_multifit_linear_free (work);

    for(int i=0;i<nbCoefCylinder;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

void leastSquareRegSolver::solveCylinder(pcl::PointXYZ centroid, pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c;

    int n = pts.size();
    int nbCoefCylinder = 3;
    X = gsl_matrix_alloc (n, nbCoefCylinder);
    y = gsl_vector_alloc (n);

    c = gsl_vector_alloc (nbCoefCylinder);
    cov = gsl_matrix_alloc (nbCoefCylinder, nbCoefCylinder);

    for (int i = 0; i < n; i++)
    {
        double xt = pts.at(i).x;
        double yt = pts.at(i).y;
        double zt = pts.at(i).z;
        gsl_matrix_set (X, i, 0, xt*xt);
        gsl_matrix_set (X, i, 1, yt*yt);
        gsl_matrix_set (X, i, 2, zt*zt);
        gsl_vector_set (y, i, 1.0);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nbCoefCylinder);

    gsl_multifit_linear (X, y, c, cov,error, work);

    gsl_multifit_linear_free (work);

    for(int i=0;i<nbCoefCylinder;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

void leastSquareRegSolver::solveWeightedPlane(pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double> weights,std::vector<double>* coeff, double* error)
{
    gsl_matrix *X, *cov;
    gsl_vector *y, *c, *w;

    int n = pts.size();
    int nbCoefCylinder = 3;
    X = gsl_matrix_alloc (n, nbCoefCylinder);
    y = gsl_vector_alloc (n);
    w = gsl_vector_alloc (n);

    c = gsl_vector_alloc (nbCoefCylinder);
    cov = gsl_matrix_alloc (nbCoefCylinder, nbCoefCylinder);

    for (int i = 0; i < n; i++)
    {
        gsl_matrix_set (X, i, 0, pts.at(i).x);
        gsl_matrix_set (X, i, 1, pts.at(i).y);
        gsl_matrix_set (X, i, 2, pts.at(i).z);
        gsl_vector_set (y, i, 1.0);

        gsl_vector_set (w, i, weights.at(i));
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nbCoefCylinder);

    gsl_multifit_wlinear(X, w, y, c, cov,error, work);

    gsl_multifit_linear_free (work);

    for(int i=0;i<nbCoefCylinder;i++)
    {
        coeff->push_back(gsl_vector_get (c,i));
    }

    gsl_matrix_free (X);
    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (cov);
}

/*struct dataquad {
    double  r;
    size_t n;
    double * u;
    double * v;
    double * w;
};

int cylquad_f (const gsl_vector * x, void *data, gsl_vector * f)
{
    double r = ((struct dataquad *) data)->r;
    size_t n = ((struct dataquad *)data)->n;
    double *u = ((struct dataquad *) data)->u;
    double *v = ((struct dataquad *) data)->v;
    double *w = ((struct dataquad *) data)->w;

    double Ai = gsl_vector_get (x, 0);
    double Bi = gsl_vector_get (x, 1);
    double Ci = gsl_vector_get (x, 2);
    double ui = gsl_vector_get (x, 3);
    double vi = gsl_vector_get (x, 4);
    double wi = gsl_vector_get (x, 5);
    //double ri = gsl_vector_get (x, 6);

    for (size_t i = 0; i < n; i++)
    {
        gsl_vector_set (f, i, Ai*(u[i]-ui)*(u[i]-ui)+Bi*(v[i]-vi)*(v[i]-vi)+Ci*(w[i]-wi)*(w[i]-wi)-1.);
    }

    return GSL_SUCCESS;
}

int cylquad_df (const gsl_vector * x, void *data, gsl_matrix * J)
{
    size_t n = ((struct dataquad *)data)->n;
    double *u = ((struct dataquad *) data)->u;
    double *v = ((struct dataquad *) data)->v;
    double *w = ((struct dataquad *) data)->w;

    double Ai = gsl_vector_get (x, 0);
    double Bi = gsl_vector_get (x, 1);
    double Ci = gsl_vector_get (x, 2);
    double ui = gsl_vector_get (x, 3);
    double vi = gsl_vector_get (x, 4);
    double wi = gsl_vector_get (x, 5);
    //double ri = gsl_vector_get (x, 6);

    for (size_t i = 0; i < n; i++)
    {
        gsl_matrix_set (J, i, 0, (u[i]-ui)*(u[i]-ui));
        gsl_matrix_set (J, i, 1, (v[i]-vi)*(v[i]-vi));
        gsl_matrix_set (J, i, 2, (w[i]-wi)*(w[i]-wi));
        gsl_matrix_set (J, i, 3, -2.*Ai*(u[i]-ui));
        gsl_matrix_set (J, i, 4, -2.*Bi*(v[i]-vi));
        gsl_matrix_set (J, i, 5, -2.*Ci*(w[i]-wi));
        //gsl_matrix_set (J, i, 6, -1.);
    }
    return GSL_SUCCESS;
}

int cylquad_fdf (const gsl_vector * x, void *data, gsl_vector * f, gsl_matrix * J)
{
  cylquad_f (x, data, f);
  cylquad_df (x, data, J);

  return GSL_SUCCESS;
}*/

//*void leastSquareRegSolver::solveCylinderQuadNoCenter(pcl::PointXYZ centerInit, double radiusInit, pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff)
//{
//    const gsl_multifit_fdfsolver_type *T;
//    gsl_multifit_fdfsolver *s;
//    int status;
//    unsigned int i, iter = 0;
//    const size_t n = pts.size();
//    const size_t p = 6;

//    gsl_matrix *covar = gsl_matrix_alloc (p, p);
//    double u[n], v[n], w[n];
//    struct dataquad d = { radiusInit, n, u, v, w};
//    gsl_multifit_function_fdf f;
//    double x_init[p] = { 1.0, 1.0, 0. , centerInit.x, centerInit.y, centerInit.z};
//    gsl_vector_view x = gsl_vector_view_array (x_init, p);
//    const gsl_rng_type * type;
//    gsl_rng * r;

//    gsl_rng_env_setup();

//    type = gsl_rng_default;
//    r = gsl_rng_alloc (type);

//    f.f = &cylquad_f;
//    f.df = &cylquad_df;
//    f.fdf = &cylquad_fdf;
//    f.n = n;
//    f.p = p;
//    f.params = &d;

//     This is the data to be fitted
//    for (i = 0; i < n; i++)
//    {
//        u[i]=pts.at(i).x;
//        v[i]=pts.at(i).y;
//        w[i]=pts.at(i).z;
//    };

//    T = gsl_multifit_fdfsolver_lmsder;
//    s = gsl_multifit_fdfsolver_alloc (T, n, p);
//    gsl_multifit_fdfsolver_set (s, &f, &x.vector);

//    do
//    {


//        printf ("iter: %3u Ai = % 15.8f Bi = % 15.8f Ci = % 15.8f  c : % 15.8f % 15.8f % 15.8f |f(x)| = %g\n",
//                  iter,
//                  gsl_vector_get (s->x, 0),
//                  gsl_vector_get (s->x, 1),
//                  gsl_vector_get (s->x, 2),
//                  gsl_vector_get (s->x, 3),
//                  gsl_vector_get (s->x, 4),
//                  gsl_vector_get (s->x, 5),
//                  gsl_blas_dnrm2 (s->f));

//        iter++;
//        status = gsl_multifit_fdfsolver_iterate (s);

//        //printf ("status = %s\n", gsl_strerror (status));

//        if (status)
//            break;

//        status = gsl_multifit_test_delta (s->dx, s->x,0.05, 0.05);

//        if(status==0)int test=0;
//    }
//    while (status == GSL_CONTINUE && iter < 2);

//    //gsl_multifit_covar (s->J, 0.0, covar);

//  #define FIT(i) gsl_vector_get(s->x, i)
//  #define ERR(i) sqrt(gsl_matrix_get(covar,i,i))

//    {
//      double chi = gsl_blas_dnrm2(s->f);
//      double dof = n - p;
//      double c = GSL_MAX_DBL(1, chi / sqrt(dof));

//      printf("chisq/dof = %g\n",  pow(chi, 2.0) / dof);

//      printf ("Ai      = %.5f +/- %.5f\n", FIT(0), c*ERR(0));
//      printf ("Bi      = %.5f +/- %.5f\n", FIT(1), c*ERR(1));
//      printf ("Ci      = %.5f +/- %.5f\n", FIT(2), c*ERR(2));
//      printf ("ui      = %.5f +/- %.5f\n", FIT(3), c*ERR(3));
//      printf ("vi      = %.5f +/- %.5f\n", FIT(4), c*ERR(4));
//      printf ("wi      = %.5f +/- %.5f\n", FIT(5), c*ERR(5));
//      printf ("ri      = %.5f +/- %.5f\n", FIT(6), c*ERR(6));
//    }

//    coeff->push_back(gsl_vector_get(s->x, 0));
//    coeff->push_back(gsl_vector_get(s->x, 1));
//    coeff->push_back(gsl_vector_get(s->x, 2));
//    coeff->push_back(gsl_vector_get(s->x, 3));
//    coeff->push_back(gsl_vector_get(s->x, 4));
//    coeff->push_back(gsl_vector_get(s->x, 5));

//    //printf ("status = %s\n", gsl_strerror (status));

//    gsl_multifit_fdfsolver_free (s);
//    gsl_matrix_free (covar);
//    gsl_rng_free (r);
//}*/

/*struct datacyl {
    size_t n;
    double * u;
    double * v;
};

int cyl_f (const gsl_vector * x, void *data, gsl_vector * f)
{
    size_t n = ((struct datacyl *)data)->n;
    double *u = ((struct datacyl *) data)->u;
    double *v = ((struct datacyl *) data)->v;

    double ui = gsl_vector_get (x, 0);
    double vi = gsl_vector_get (x, 1);
    double ri = gsl_vector_get (x, 2);

    for (size_t i = 0; i < n; i++)
    {
        gsl_vector_set (f, i, (u[i]-ui)*(u[i]-ui)+(v[i]-vi)*(v[i]-vi)-ri*ri);
    }

    return GSL_SUCCESS;
}

int cyl_df (const gsl_vector * x, void *data, gsl_matrix * J)
{
    size_t n = ((struct datacyl *)data)->n;
    double *u = ((struct datacyl *) data)->u;
    double *v = ((struct datacyl *) data)->v;

    double ui = gsl_vector_get (x, 0);
    double vi = gsl_vector_get (x, 1);
    double ri = gsl_vector_get (x, 2);

    for (size_t i = 0; i < n; i++)
    {
        gsl_matrix_set (J, i, 0, -2.*(u[i]-ui));
        gsl_matrix_set (J, i, 1, -2.*(v[i]-vi));
        gsl_matrix_set (J, i, 2, -2.*ri);
    }
    return GSL_SUCCESS;
}

int cyl_fdf (const gsl_vector * x, void *data, gsl_vector * f, gsl_matrix * J)
{
  cyl_f (x, data, f);
  cyl_df (x, data, J);

  return GSL_SUCCESS;
}

void leastSquareRegSolver::solveCylinderNoCenter(pcl::PointXYZ centerInit, double radiusInit, pcl::PointCloud<pcl::PointXYZ>  pts, std::vector<double>* coeff)
{
    const gsl_multifit_fdfsolver_type *T;
    gsl_multifit_fdfsolver *s;
    int status;
    unsigned int i, iter = 0;
    const size_t n = pts.size();
    const size_t p = 3;

    gsl_matrix *covar = gsl_matrix_alloc (p, p);
    double u[n], v[n];
    struct datacyl d = { n, u, v };
    gsl_multifit_function_fdf f;
    double x_init[p] = { centerInit.x, centerInit.y, radiusInit};
    gsl_vector_view x = gsl_vector_view_array (x_init, p);
    const gsl_rng_type * type;
    gsl_rng * r;

    gsl_rng_env_setup();

    type = gsl_rng_default;
    r = gsl_rng_alloc (type);

    f.f = &cyl_f;
    f.df = &cyl_df;
    f.fdf = &cyl_fdf;
    f.n = n;
    f.p = p;
    f.params = &d;

    /* This is the data to be fitted */
   /* for (i = 0; i < n; i++)
    {
        u[i]=pts.at(i).x;
        v[i]=pts.at(i).y;
    };

    T = gsl_multifit_fdfsolver_lmsder;
    s = gsl_multifit_fdfsolver_alloc (T, n, p);
    gsl_multifit_fdfsolver_set (s, &f, &x.vector);

    do
    {


        /*printf ("iter: %3u ui = % 15.8f vi = % 15.8f ri = % 15.8f |f(x)| = %g\n",
                  iter,
                  gsl_vector_get (s->x, 0),
                  gsl_vector_get (s->x, 1),
                  gsl_vector_get (s->x, 2),
                 gsl_blas_dnrm2 (s->f));

        iter++;
        status = gsl_multifit_fdfsolver_iterate (s);

        //printf ("status = %s\n", gsl_strerror (status));

        if (status)
            break;

        status = gsl_multifit_test_delta (s->dx, s->x,0.001, 0.001);

        if(status==0)int test=0;
    }
    while (status == GSL_CONTINUE && iter < 10);

    //gsl_multifit_covar (s->, 0.0, covar);

    coeff->push_back(gsl_vector_get(s->x, 0));
    coeff->push_back(gsl_vector_get(s->x, 1));
    coeff->push_back(gsl_vector_get(s->x, 2));

    //printf ("status = %s\n", gsl_strerror (status));

    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);
    gsl_rng_free (r);
}*/
