/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemfrbfsurface.h"

const itemFrbfsurfaceDrawManager itemFrbfsurface::dem_rbfsurface_DRAW_MANAGER;

itemFrbfsurface::itemFrbfsurface() : CT_AbstractItemDrawableWithoutPointCloud()
{
    //poly = NULL;
    setBaseDrawManager(&dem_rbfsurface_DRAW_MANAGER);
}

itemFrbfsurface::itemFrbfsurface(const CT_OutAbstractSingularItemModel *model,
                                       const CT_AbstractResult *result,
                               const surfaceFrbf *pFrbf) : CT_AbstractItemDrawableWithoutPointCloud(model, result), frbf(*pFrbf)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_rbfsurface_DRAW_MANAGER);
}

itemFrbfsurface::itemFrbfsurface(const QString &modelName,
                                       const CT_AbstractResult *result,
                               const surfaceFrbf *pFrbf) : CT_AbstractItemDrawableWithoutPointCloud(modelName, result), frbf(*pFrbf)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_rbfsurface_DRAW_MANAGER);
}

itemFrbfsurface::~itemFrbfsurface()
{
    //delete poly;
}

QString itemFrbfsurface::getType() const
{
    return staticGetType();
}

QString itemFrbfsurface::staticGetType()
{
    return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/dem_rbfsurface";
}

const surfaceFrbf& itemFrbfsurface::getImplFrbf() const
{
    return frbf;
}

itemFrbfsurface* itemFrbfsurface::copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList)
{
    itemFrbfsurface *t2d = new itemFrbfsurface((const CT_OutAbstractSingularItemModel *)model, result, NULL);
    t2d->setId(id());

    t2d->setAlternativeDrawManager(getAlternativeDrawManager());

    return t2d;
}
