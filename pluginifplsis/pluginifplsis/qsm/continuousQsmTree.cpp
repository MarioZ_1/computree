/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "continuousQsmTree.h"

#include "core/wendlandRbf.h"
#include "utils/treetopology.h"

#include <pcl/common/common.h>
#include <boost/thread.hpp>

void threadProcessCylinder(size_t index , std::vector<continuousQsmSegment> *listSpheres, cylinder &c, continuousQsmSegment &sph)
{
    sph.process(c);
    (*listSpheres)[index]=sph;
}

continuousQsmTree::continuousQsmTree(pcl::PointCloud<pcl::PointXYZ> &inputPts, treeTopology stModel):ptsCloud(inputPts)
{
    pcl::PointXYZ mini,maxi;
    pcl::getMinMax3D (ptsCloud, mini, maxi);
    BBMin.x=mini.x;
    BBMin.y=mini.y;
    BBMin.z=mini.z;
    BBMax.x=maxi.x;
    BBMax.y=maxi.y;
    BBMax.z=maxi.z;

    int nbCylinders = stModel.getNumberOfCylinders();
    int nbCores = static_cast<int>(boost::thread::hardware_concurrency())-1;

    listSphere.resize(nbCylinders);

    std::cout<<"        "<<nbCylinders<<" units to process, "<<nbCores<<" cores available"<<std::endl;

    int m=0;

    while (nbCylinders > 0) {
        std::vector<boost::thread *> threads(std::min(nbCores, nbCylinders));
        for (size_t i = 0; i < threads.size(); ++i) { // Start appropriate number of threads

            cylinder c = stModel.getListCylinders().at(m);

            if(std::find(listBranchIndices.begin(), listBranchIndices.end(), c.branchId) == listBranchIndices.end()){
                listBranchIndices.push_back(c.branchId);
            }

            continuousQsmSegment sph(c,ptsCloud,c.branchId);
            threads[i] = new boost::thread(threadProcessCylinder, m,&listSphere, c ,sph);
            m++;
        }
        for (size_t i = 0; i < threads.size(); ++i) { // Wait for all threads to finish
            threads[i]->join();
            delete threads[i];
            --nbCylinders;
        }
    }

    modelByBranch = true;
}

void continuousQsmTree::fillScalarField(scalarField &field)
{
    if(!modelByBranch){

        pcl::PointXYZ min = field.getBBmin();

        int nbDivx = field.getXNode()-1;
        int nbDivY = field.getYNode()-1;
        int nbDivZ = field.getZNode()-1;

        float sizeCellX = field.getSizeCellX();
        float sizeCellY = field.getSizeCellY();
        float sizeCellZ = field.getSizeCellZ();
        for(std::size_t m=0;m<listSphere.size();m++)
        {
            int coordCenterX, coordCenterY, coordCenterZ;
            coordCenterX = (int)((listSphere.at(m).getCenter().x-min.x)/sizeCellX);
            coordCenterY = (int)((listSphere.at(m).getCenter().y-min.y)/sizeCellY);
            coordCenterZ = (int)((listSphere.at(m).getCenter().z-min.z)/sizeCellZ);
            int stepX = (int)(listSphere.at(m).getRadius()/sizeCellX)+1;
            int stepY = (int)(listSphere.at(m).getRadius()/sizeCellY)+1;
            int stepZ = (int)(listSphere.at(m).getRadius()/sizeCellZ)+1;

            for(int k=coordCenterZ-stepZ;k<coordCenterZ+stepZ;k++)
            {
                for(int j=coordCenterY-stepY;j<coordCenterY+stepY;j++)
                {
                    for(int i=coordCenterX-stepX;i<coordCenterX+stepX;i++)
                    {
                        if(i>=0 && i<nbDivx+1 && j>=0 && j<nbDivY+1 && k>=0 && k<nbDivZ+1){

                            float x = (float)i*sizeCellX+min.x;
                            float y = (float)j*sizeCellY+min.y;
                            float z = (float)k*sizeCellZ+min.z;

                            float sum = 0.;
                            if(field.getValue(i,j,k) != scalarField::NODATA)
                            {
                                sum = field.getValue(i,j,k);
                            }

                            bool hasBeenUpdated = false;

                            float tmpx = (listSphere.at(m).getCenter().x-x);
                            float tmpy = (listSphere.at(m).getCenter().y-y);
                            float tmpz = (listSphere.at(m).getCenter().z-z);
                            float distR = sqrt(tmpx*tmpx+tmpy*tmpy+tmpz*tmpz);

                            float r = distR/listSphere.at(m).getRadius();

                            if(r<1.)
                            {
                                float res2 = listSphere.at(m).getLocalCell().getValueOfImplicitFunction(x,y,z);

                                sum += (res2)*std::pow(1-r,4.0)*(1.+4.*r);

                                hasBeenUpdated = true;
                            }

                            if(hasBeenUpdated)field.setValue(i,j,k,sum);
                        }
                    }
                }
            }
        }

        for(int i=0;i<nbDivx;i++)
        {
            for(int j=0;j<nbDivY;j++)
            {
                field.setValue(i,j,0,0);
                field.setValue(i,j,nbDivZ,0);
            }
        }

    }else{


        clock_t t = clock();

        //std::cout<<"let's fill the scalar field by branch"<<std::endl;

        std::vector<std::string> listFiles;

        pcl::PointXYZ min = field.getBBmin();

        int nbDivx = field.getXNode()-1;
        int nbDivY = field.getYNode()-1;
        int nbDivZ = field.getZNode()-1;

        float sizeCellX = field.getSizeCellX();
        float sizeCellY = field.getSizeCellY();
        float sizeCellZ = field.getSizeCellZ();


        for(int n=0;n<listBranchIndices.size();n++){

            //std::cout<<"    processing branch : "<<n<<std::endl;

            for(std::size_t m=0;m<listSphere.size();m++)
            {
                if(listSphere.at(m).getBranchId()==listBranchIndices.at(n))
                {
                    int coordCenterX, coordCenterY, coordCenterZ;
                    coordCenterX = (int)((listSphere.at(m).getCenter().x-min.x)/sizeCellX);
                    coordCenterY = (int)((listSphere.at(m).getCenter().y-min.y)/sizeCellY);
                    coordCenterZ = (int)((listSphere.at(m).getCenter().z-min.z)/sizeCellZ);
                    int stepX = (int)(listSphere.at(m).getRadius()/sizeCellX)+1;
                    int stepY = (int)(listSphere.at(m).getRadius()/sizeCellY)+1;
                    int stepZ = (int)(listSphere.at(m).getRadius()/sizeCellZ)+1;

                    for(int k=coordCenterZ-stepZ;k<coordCenterZ+stepZ;k++)
                    {
                        for(int j=coordCenterY-stepY;j<coordCenterY+stepY;j++)
                        {
                            for(int i=coordCenterX-stepX;i<coordCenterX+stepX;i++)
                            {
                                if(i>=0 && i<nbDivx+1 && j>=0 && j<nbDivY+1 && k>=0 && k<nbDivZ+1){

                                    float x = (float)i*sizeCellX+min.x;
                                    float y = (float)j*sizeCellY+min.y;
                                    float z = (float)k*sizeCellZ+min.z;

                                    float sum=0.;
                                    if(field.getValue(i,j,k,n) != scalarField::NODATA)
                                    {
                                        sum  = field.getValue(i,j,k,n);
                                    }

                                    bool hasBeenUpdated = false;

                                    float tmpx = (listSphere.at(m).getCenter().x-x);
                                    float tmpy = (listSphere.at(m).getCenter().y-y);
                                    float tmpz = (listSphere.at(m).getCenter().z-z);
                                    float distR = sqrt(tmpx*tmpx+tmpy*tmpy+tmpz*tmpz);

                                    float r = distR/listSphere.at(m).getRadius();

                                    if(r<1.)
                                    {
                                        float res2 = listSphere.at(m).getLocalCell().getValueOfImplicitFunction(x,y,z);

                                        sum += (res2)*std::pow(1-r,4.0)*(1.+4.*r);

                                        hasBeenUpdated = true;
                                    }

                                    if(hasBeenUpdated)field.setValue(i,j,k,sum,n);
                                }
                            }
                        }
                    }
                }
            }
        }

        t = clock() - t;
        std::cout<<"        Branches processing in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;

        field.mergeFields(listBranchIndices.size());

        t = clock() - t;
        std::cout<<"        merging in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    }
}

