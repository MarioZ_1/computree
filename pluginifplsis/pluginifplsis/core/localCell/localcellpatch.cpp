/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "localcellpatch.h"

#include "../leastsquareregsolver.h"

localCellPatch::localCellPatch(const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(pPtsGlobalRef)
{
    computeLeastSquareReg(radius,useWeight);
    getGlobalQuadric();
}
localCellPatch::localCellPatch(pcl::PointXYZ center, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(center,pPtsGlobalRef)
{
    computeLeastSquareReg(radius,useWeight);
    getGlobalQuadric();
}
localCellPatch::localCellPatch(pcl::PointXYZ center, pcl::PointXYZ normal, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(center,normal,pPtsGlobalRef)
{
    computeLeastSquareReg(radius,useWeight);
    getGlobalQuadric();
}

void localCellPatch::computeLeastSquareReg(float radius, bool useWeight)
{
    pcl::PointXYZ cen;
    cen.x = centroid[0];
    cen.y = centroid[1];
    cen.z = centroid[2];
    leastSquareRegSolver::solveQuadric(cen, radius , useWeight, ptsLocalRef, &this->coeff,&this->error);
}

void localCellPatch::getGlobalQuadric()
{
    Eigen::Matrix4f m1Local;
    Eigen::Vector4f m2Local(coeff.at(3),coeff.at(4),-1,coeff.at(5));
    m1Local<<coeff.at(0),0.5*coeff.at(1),0,0,
             0.5*coeff.at(1),coeff.at(2),0,0,
             0,0,0,0,
             0,0,0,0;
    m1 = transfo.matrix().transpose() * m1Local * transfo.matrix();
    m2 = m2Local.transpose() * transfo.matrix();
}
