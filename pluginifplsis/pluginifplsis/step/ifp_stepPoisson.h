/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef IFP_stepPoisson_H
#define IFP_stepPoisson_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_ttreegroup.h"
#include "ct_itemdrawable/ct_topfnodegroup.h"
#include "ct_itemdrawable/ct_opfmeshmodel.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#define DEF_IN_RESULT2 "igrp2"
#define DEF_IN_GRP_CLUSTER3 "igrp3"
#define DEF_IN_NAME "igrp4"


#ifdef USE_PCL
#include <pcl/surface/poisson.h>
#endif

/*!
 * \class DE_StepGetSurface
 * \ingroup Steps_dem
 * \brief <b>compute the dtm.</b>
 *
 * Detailed decription of step purpose.
 * Please also give a general view of the algorithm.
 *
 * \param _error
 * \param _nbx
 * \param _nby
 * \param _nbz
 *
 *
 * <b>Input Models:</b>
 *
 * - CT_ResultGroup (inputPtCloud)\n
 *     - CT_StandardItemGroup (grpIn)...\n
 *         - CT_Scene (inputPtCloud)\n
 *
 * <b>Output Models:</b>
 *
 * - CT_ResultGroup (resOut)\n
 *     - CT_StandardItemGroup (output)...\n
 *         - CT_Triangulation2D (surf)\n
 *
 */

class IFP_stepPoisson: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    IFP_stepPoisson(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    virtual QStringList getStepRISCitations() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    CT_Mesh* buildMesh(pcl::PointCloud<pcl::PointXYZ> &pts, std::vector<pcl::Vertices> &tri, Eigen::Vector3d &bboxMin, Eigen::Vector3d &bboxMax);
    bool loadVertex(pcl::PointXYZ& x0, CT_Point &point);
    bool loadFace(pcl::Vertices tri, const size_t &beginVertexIndex, CT_Mesh *mesh, CT_MutableFaceIterator &itFaces, CT_MutableEdgeIterator &itEdges);
    void updateMinMax(const CT_Point &point, Eigen::Vector3d &bboxMin, Eigen::Vector3d &bboxMax);
    CT_Edge* findHEdgeTwin(const CT_Mesh *mesh, const size_t &p0, const size_t &p1);

    // Step parameters
    bool searchHalfEdges;

    int    _nbNeighborsNormals;
    int    _depth;    /*!<  */
    int    _solverDivide;
    int    _isoDivide;
    double _samplePerNode;
    bool   _confidence;
    double _pointWeight;
    QStringList m_filePath;

    CT_AutoRenameModels     _out_QsmSurfModelName;

};

#endif // IFP_stepPoisson_H
