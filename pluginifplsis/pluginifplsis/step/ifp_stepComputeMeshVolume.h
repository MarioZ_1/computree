/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef IFP_STEPCOMPUTEMESHVOLUME_H
#define IFP_STEPCOMPUTEMESHVOLUME_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#define DEF_IN_RESULT2 "igrp2"
#define DEF_IN_GRP_CLUSTER3 "igrp3"
#define DEF_IN_NAME "igrp4"



class IFP_stepComputeMeshVolume: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    IFP_stepComputeMeshVolume(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;
    QString getStepDetailledDescription() const;
    QStringList getStepRISCitations() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _res;    /*!<  */
    bool    _divideMesh;

    bool isFaceIntersectedByPlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0);
    bool isFaceBelowPlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0);
    bool isFaceAbovePlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0);
    bool fillLineTop(pcl::PointCloud<pcl::PointXYZ>& ptsCloud, std::vector<int> tri, double z0, std::vector<std::vector<int> > &lineTop);
    std::vector<std::vector<std::vector<int>>> findConnexLines(std::vector<std::vector<int>> lineTop);
    bool findConnectedSegments(std::vector<std::vector<int>>& lineTop, std::vector<std::vector<int>>& connectedSegments);

    std::vector<std::vector<std::vector<int> > > findConnexElements(std::vector<std::vector<int>> tris);
    bool findConnectedTris(std::vector<std::vector<int>>& inputTris, std::vector<std::vector<int>>& connexTris);

    double getVolume(std::vector<std::vector<int>>& tris, pcl::PointCloud<pcl::PointXYZ>& pts);
    pcl::PointXYZ getCentroid(std::vector<std::vector<int>>& tris, pcl::PointCloud<pcl::PointXYZ>& pts);

    double getVolumeTetrahedra(pcl::PointXYZ& p0,pcl::PointXYZ& p1,pcl::PointXYZ& p2,pcl::PointXYZ& p3);

    CT_AutoRenameModels     _outVolumeItem_ModelName;
    CT_AutoRenameModels     _outVolumeAttribute_ModelName;
    QStringList m_filePath;


};

#endif // IFP_STEPCOMPUTEMESHVOLUME_H
