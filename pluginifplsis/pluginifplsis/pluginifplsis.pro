CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

MUST_USE_USE_PCL = 1
MUST_USE_USE_GSL = 1

CONFIG += c++14

COMPUTREE += ctlibpcl ctlibgsl ctliblas

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

TARGET = plug_ifplsis

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    ifp_pluginentry.h \
    ifp_steppluginmanager.h \
    step/ifp_stepComputeMnt.h \
    plot/scalarfield.h \
    plot/polygonalizer.h \
    plot/isosurface.h \
    plot/implicitfunction.h \
    plot/hashmapdata.h \
    core/rectangle.h \
    core/quadtree.h \
    core/quadleaf.h \
    core/leastsquareregsolver.h \
    core/constant.h \
    core/graph.h \
    core/histogram.h \
    core/localCell/localCell.h \
    core/localCell/localcellpatch.h \
    core/localCell/localcellcylinder.h \
    core/wendlandRbf.h \
    core/surfacefrbf.h \
    itemdrawable/itemfrbfsurfacedrawmanager.h \
    itemdrawable/itemfrbfsurface.h \
    itemdrawable/itemfrbftreedrawmanager.h \
    itemdrawable/itemfrbftree.h \
    itemdrawable/itemsimpletreedrawmanager.h \
    itemdrawable/itemsimpletree.h \
    core/sphereSupport.h \
    step/ifp_stepGetMinPtsPerSurfaceUnit.h \
    step/ifp_stepSmoothQsm.h \
    qsm/continuousQsmTree.h \
    qsm/continuousQsmSegment.h \
    utils/treetopology.h \
    utils/treesegment.h \
    step/ifp_stepComputeMeshVolume.h \
    itemdrawable/itemvolume.h \
    itemdrawable/itemvolumedrawmanager.h \
    utils/volumedrawerhelper.h \
    step/ifp_stepPoisson.h

SOURCES += \
    ifp_pluginentry.cpp \
    ifp_steppluginmanager.cpp \
    step/ifp_stepComputeMnt.cpp \
    plot/scalarfield.cpp \
    plot/polygonalizer.cpp \
    plot/isosurface.cpp \
    plot/implicitfunction.cpp \
    core/rectangle.cpp \
    core/quadtree.cpp \
    core/quadleaf.cpp \
    core/leastsquareregsolver.cpp \
    core/graph.cpp \
    core/histogram.cpp \
    core/localCell/localCell.cpp \
    core/localCell/localcellpatch.cpp \
    core/localCell/localcellcylinder.cpp \
    core/wendlandRbf.cpp \
    core/surfacefrbf.cpp \
    itemdrawable/itemfrbfsurfacedrawmanager.cpp \
    itemdrawable/itemfrbfsurface.cpp \
    itemdrawable/itemfrbftreedrawmanager.cpp \
    itemdrawable/itemfrbftree.cpp \
    itemdrawable/itemsimpletreedrawmanager.cpp \
    itemdrawable/itemsimpletree.cpp \
    core/sphereSupport.cpp \
    step/ifp_stepGetMinPtsPerSurfaceUnit.cpp \
    step/ifp_stepSmoothQsm.cpp \
    qsm/continuousQsmTree.cpp \
    qsm/continuousQsmSegment.cpp \
    utils/treetopology.cpp \
    utils/treesegment.cpp \
    step/ifp_stepComputeMeshVolume.cpp \
    itemdrawable/itemvolume.cpp \
    itemdrawable/itemvolumedrawmanager.cpp \
    utils/volumedrawerhelper.cpp \
    step/ifp_stepPoisson.cpp

TRANSLATIONS += languages/pluginifplsis_en.ts \
                languages/pluginifplsis_fr.ts

unix:!macx: LIBS += -L/usr/lib/ -lgslcblas
