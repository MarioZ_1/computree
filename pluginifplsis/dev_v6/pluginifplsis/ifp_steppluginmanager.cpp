/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_steppluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"

#include "step/ifp_stepGetMinPtsPerSurfaceUnit.h"
#include "step/ifp_stepComputeMnt.h"
#include "step/ifp_stepSmoothQsm.h"
#include "step/ifp_stepComputeMeshVolume.h"
#include "step/ifp_stepPoisson.h"

IFP_StepPluginManager::IFP_StepPluginManager() : CT_AbstractStepPlugin()
{
}

IFP_StepPluginManager::~IFP_StepPluginManager()
{
}

QString IFP_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin IFP-LSIS for Computree\n"
           "AU  - Morel, Jules\n"
           "PB  - French Institute of Pondicherry, Laboratoire des Sciences de l'Information et des Systèmes\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-ifp-lsis/wiki\n"
           "ER  - \n";
}

bool IFP_StepPluginManager::loadGenericsStep()
{
    addNewPointsStep<IFP_stepGetMinPtsPerSurface>(CT_StepsMenu::LP_Filter);
    addNewRastersStep<IFP_stepComputeMnt>(CT_StepsMenu::LP_DEM);

    addNewMeshesStep<IFP_stepSmoothQSM>(CT_StepsMenu::LP_Create);
    addNewMeshesStep<IFP_stepComputeMeshVolume>(CT_StepsMenu::LP_Analyze);
    addNewMeshesStep<IFP_stepPoisson>(CT_StepsMenu::LP_Create);
    return true;
}

bool IFP_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool IFP_StepPluginManager::loadCanBeAddedFirstStep()
{

    return true;
}

bool IFP_StepPluginManager::loadActions()
{
    return true;
}

bool IFP_StepPluginManager::loadExporters()
{
    return true;
}

bool IFP_StepPluginManager::loadReaders()
{
    return true;
}

