/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "sphereSupport.h"

sphereSupport::sphereSupport(quadLeaf* pLeaf, pcl::PointCloud<pcl::PointXYZ> pCloud)
{
    leaf = pLeaf;

    center.x = leaf->getLocalCell().getCentroid()[0];
    center.y = leaf->getLocalCell().getCentroid()[1];
    center.z = leaf->getLocalCell().getCentroid()[2];

    radius = leaf->getRadius();

    keepPtsInSphere(pCloud);

    if(!leaf->hadBeenRebuild){
        localR = new localCellPatch (pts);
    }else{
        localR = &(leaf->getLocalCell());
    }
}

sphereSupport::~sphereSupport()
{}

sphereSupport::sphereSupport(pcl::PointXYZ pCenter,float pRadius, pcl::PointCloud<pcl::PointXYZ> pCloud)
{
    center = pCenter;
    radius = pRadius;
    keepPtsInSphere(pCloud);
    localR = new localCellPatch (pts);
}

void sphereSupport::keepPtsInSphere(pcl::PointCloud<pcl::PointXYZ> pCloud)
{
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud (pCloud.makeShared());

    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    if ( kdtree.radiusSearch (center, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0 )
    {
        for (size_t i = 0; i < pointIdxRadiusSearch.size (); ++i){
            pcl::PointXYZ pt;
            pt.x = pCloud.at(pointIdxRadiusSearch[i]).x;
            pt.y = pCloud.at(pointIdxRadiusSearch[i]).y;
            pt.z = pCloud.at(pointIdxRadiusSearch[i]).z;
            pts.push_back(pt);
        }
    }
}
