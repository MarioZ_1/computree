/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "histogram.h"

#include <pcl/common/common.h>

histogram::histogram(pcl::PointCloud<pcl::PointXYZ> &ptsCloud, float pRange, int pWinSizeSmooth): winSizeSmooth(pWinSizeSmooth)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(ptsCloud));
    pcl::PointXYZ min,max;
    pcl::getMinMax3D (*cloud_pts_ptr, min, max);

    range = pRange;

    minVal = min.z;
    maxVal = max.z;

    int a=0;

    nbins = (int)((maxVal-minVal)/range)+1;
    //TODO : change the treatment of the first bins
    for(int i=0;i<nbins;i++)
    {
        hist.push_back(0);
    }

    for(size_t i=0;i<ptsCloud.size();i++)
    {
        pcl::PointXYZ  pt = ptsCloud.at(i);
        hist.at((int)((pt.z-minVal)/range))++;
    }

    if(nbins>1)smooth(winSizeSmooth);
}

void histogram::simplefilter(pcl::PointCloud<pcl::PointXYZ> &ptsCloud)
{
    if(nbins>1){
        pcl::PointCloud<pcl::PointXYZ> newPtsCloud;

        bool firstNullBin = false;
        int binNum = 0;
        while(!firstNullBin && binNum<nbins)
        {
            if(hist.at(binNum) == 0)firstNullBin=true;
            binNum++;
        }

        for(std::size_t i=0;i<ptsCloud.size();i++)
        {
            pcl::PointXYZ pt = ptsCloud.at(i);
            if(pt.z-minVal<(float)binNum*range)
            {
                newPtsCloud.push_back(pt);
            }
        }

        ptsCloud.clear();
        ptsCloud = newPtsCloud;
    }
}

void histogram::smooth(int winSize)
{
    int winMidSize = winSize / 2;

    for(int i = 0; i < winMidSize; ++i)
    {
        if(i>0 && i<hist.size()){
            hist.at(i) = hist.at(0);
        }
    }

    for(int i = winMidSize; i < nbins - winMidSize; ++i)
    {
        if(i>0 && i<hist.size()){
            float mean = 0;
            for(int j = i - winMidSize; j <= (i + winMidSize); ++j)
            {
                if(j>0 && j<hist.size()){
                    mean += hist.at(j);
                }
            }
            hist.at(i) = mean / winSize;
        }
    }
}

void histogram::print()
{
    std::cout<<"    min: "<<minVal<<" max: "<<maxVal<<" range: "<<range<<" nb bins: "<<nbins<<std::endl;
    for(size_t i=0;i<hist.size();i++)
    {
        std::cout<<"            "<<i<<" - "<<hist.at(i)<<std::endl;
    }
}


