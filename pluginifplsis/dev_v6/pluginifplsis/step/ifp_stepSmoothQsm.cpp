/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_stepSmoothQsm.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_groupiterator.h"
#include "itemdrawable/itemfrbftree.h"

#include "ct_accessor/ct_pointaccessor.h"

#include "core/surfacefrbf.h"
#include "plot/polygonalizer.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include "ct_itemdrawable/ct_ttreegroup.h"
#include "ct_itemdrawable/ct_tnodegroup.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "ct_itemdrawable/ct_opfmeshmodel.h"
#include "ct_itemdrawable/model/outModel/ct_outopfnodegroupmodel.h"
#include "ct_itemdrawable/tools/iterator/ct_itemiterator.h"

#include "ct_mesh/ct_face.h"
#include "ct_mesh/ct_edge.h"
#include "ct_mesh/tools/ct_meshallocatort.h"


#include "qsm/continuousQsmTree.h"
#include "itemdrawable/itemsimpletree.h"
#include "utils/treetopology.h"

// Alias for indexing in models
#define DEF_resultIn_resIn "resIn"
#define DEF_groupIn_grpIn "grpInQsm"
#define DEF_resultIn_resInQSM "resInQsm"
#define DEF_groupIn_grpInQSM "grpIn"
#define DEF_itemIn_inputPt "inputPt"
#define DEF_itemIn_inputQSM "inputQSM"

#define DEFin_tree "tree"
#define DEFin_grpSegments "grpSegments"
#define DEFin_segment "segment"
#define DEFin_mesh "mesh"

// Alias for indexing out models
#define DEF_resultOut_resOut "resOut"
#define DEF_groupOut_grpOut "grpOut"
#define DEF_itemOut_Qsm "qsm"
#define DEF_itemOut_QsmMesh "qsmMesh"
#define DEF_itemOut_QsmSurf "qsmSurf"

#include <iostream>

// Constructor : initialization of parameters
IFP_stepSmoothQSM::IFP_stepSmoothQSM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    resolution = 0.01;
    branchCounter=0;
    searchHalfEdges=false;
}

// Step description (tooltip of contextual menu)
QString IFP_stepSmoothQSM::getStepDescription() const
{
    return tr("Transform a QSM into a continuous mesh model");
}

QString IFP_stepSmoothQSM::getStepDetailledDescription() const
{
    return tr("This step models the woody part of trees with an implicit surface."
              "The method fits the points and fills surface holes by building upon the provided QSM to model the surface of the tree with local quadratic approximation merged together with radial basis functions used as partition of unity."
              "The result is provided as a polygonal model arising from the polygonization of the resulting implicit function.");
}

QStringList IFP_stepSmoothQSM::getStepRISCitations() const
{
    return QStringList() << tr("TY  - CONF\n"
                               "TI  - Reconstruction of trees with cylindrical quadrics and radial basis functions\n"
                               "T2  - The 9th Symposium on Mobile Mapping Technology, 2015, Sydney, Australia\n"
                               "A1  - Morel, Jules\n"
                               "A2  - Bac, Alexandra\n"
                               "A3  - Vega, Cedric\n"
                               "PY  - 2015\n");
}

// Step copy methodmin
CT_VirtualAbstractStep* IFP_stepSmoothQSM::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new IFP_stepSmoothQSM(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void IFP_stepSmoothQSM::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_resultIn_resIn, tr("Scène"), "", true);
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_grpIn);
    resultModel->addItemModel(DEF_groupIn_grpIn, DEF_itemIn_inputPt, CT_Scene::staticGetType(), tr("Tree Point cloud"));

    CT_InResultModelGroupToCopy *resultModelQsm = createNewInResultModelForCopy(DEF_resultIn_resInQSM, tr("QSM(s)"), "", true);
    resultModelQsm->setZeroOrMoreRootGroup();
    resultModelQsm->addGroupModel("", DEF_groupIn_grpIn);
    resultModelQsm->addGroupModel(DEF_groupIn_grpIn, DEF_groupIn_grpInQSM,CT_TTreeGroup::staticGetType(), tr("Groupe TTree"));
    resultModelQsm->addGroupModel(DEF_groupIn_grpInQSM, DEFin_grpSegments, CT_TNodeGroup::staticGetType(), tr("Groupe segments"));
    resultModelQsm->addItemModel(DEFin_grpSegments, DEFin_segment, CT_AbstractSingularItemDrawable::staticGetType(), tr("Segment"));
}

// Creation and affiliation of OUT models
void IFP_stepSmoothQSM::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resOut, tr("Surfaces extraites"));

    resultModel->setRootGroup(DEF_groupOut_grpOut);
    resultModel->addItemModel(DEF_groupOut_grpOut, DEF_itemOut_Qsm, new itemSimpleTree(), tr("QSM cylinders"));
    resultModel->addItemModel(DEF_groupOut_grpOut, DEF_itemOut_QsmSurf, new CT_MeshModel(), tr("Continuous surface"));
}

// Semi-automatic creation of step parameters DialogBox
void IFP_stepSmoothQSM::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble("Polygonizer resolution", "", 0.001, 100, 3, resolution, 0);
    configDialog->addBool(tr("Research of half edges ? (speed up loading if unchecked)"), "", "", searchHalfEdges);
}

void IFP_stepSmoothQSM::compute()
{
    // récupération du résultats IN et OUT
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* inResultPts = inResultList.at(0);

    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultItemIterator itr(inResultPts, this, DEF_itemIn_inputPt);
    const CT_Scene *itemIn = (CT_Scene*) itr.next();

    //conversion in pcl points
    CT_PointAccessor accessor;
    const CT_AbstractPointCloudIndex*   indexCloud = itemIn->getPointCloudIndex();

    std::cout<<"    1 Computree to PCL point cloud : "<<std::endl;

    pcl::PointCloud<pcl::PointXYZ>  ptsCloud;
    for(size_t i=0;i<indexCloud->size();i++){
        pcl::PointXYZ pt;
        const CT_Point& point_i   = accessor.constPointAt(indexCloud->constIndexAt(i));
        pt.x = point_i[0];
        pt.y = point_i[1];
        pt.z = point_i[2];
        ptsCloud.push_back(pt);
    }

    std::cout << "      "<<ptsCloud.size()<<" points loaded" << std::endl;
    setProgress(10);

    CT_ResultGroup* inResultQsm = inResultList.at(1);
    CT_ResultGroupIterator itr2(inResultQsm, this, DEF_groupIn_grpInQSM);

    CT_TTreeGroup *treeGrp = (CT_TTreeGroup*) itr2.next();

    CT_TNodeGroup *root = treeGrp->rootNode();
    treeTopology topo;
    recursiveSearchQsm(root,0,false,topo);
    topo.refineCylinders();
    setProgress(30);

    PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("Processing of each QSM cylinders ..."));

    clock_t t = clock();
    std::cout<<"    3 Quadrics + RBF "<<std::endl;
    continuousQsmTree h(ptsCloud,topo);
    t = clock() - t;
    std::cout<<"        Qsm smoothed in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    setProgress(50);

    PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("Blending of local approximations into a global model ..."));

    std::cout<<"    4 Polygonization "<<std::endl;
    h.polygonizeLevelSet(resolution,0.);
    t = clock() - t;
    std::cout<<"        surface polygonized in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
    std::cout<<"        Volume = "<<h.getLevelSet().getVolume()<<std::endl;
    setProgress(80);

    std::ostringstream sCompute;
    sCompute<<"Estimated volume: "<<h.getLevelSet().getVolume()<<" m^3";
    PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr(sCompute.str().c_str()));

    PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("Creation of the final mesh model ..."));

    std::cout<<"    5 Mesh creation "<<std::endl;

    Eigen::Vector3d bboxMin;
    bboxMin[0] = std::numeric_limits<double>::max();
    bboxMin[1] = bboxMin[0];
    bboxMin[2] = bboxMin[0];

    Eigen::Vector3d bboxMax;
    bboxMax[0] = -bboxMin[0];
    bboxMax[1] = -bboxMin[0];
    bboxMax[2] = -bboxMin[0];

    const isoSurface iso = h.getLevelSet();
    CT_Mesh* mesh = buildMesh(iso.getMapPoints(),iso.getTriangles(),bboxMin,bboxMax);

    // ------------------------------
    // Create OUT groups and items
    // ----------------------------------------------------------------------------
    // Works on the result corresponding to DEF_resultOut_resOut
    CT_StandardItemGroup* groupOut_grpOut = new CT_StandardItemGroup(DEF_groupOut_grpOut, outResult);

    itemSimpleTree* itemOut_stModel = new itemSimpleTree(DEF_itemOut_Qsm, outResult,&topo);
    groupOut_grpOut->addItemDrawable(itemOut_stModel);

    CT_MeshModel* itemOut_surfMesh = new CT_MeshModel(DEF_itemOut_QsmSurf, outResult,mesh);
    itemOut_surfMesh->setBoundingBox(bboxMin[0], bboxMin[1], bboxMin[2], bboxMax[0], bboxMax[1], bboxMax[2]);
    groupOut_grpOut->addItemDrawable(itemOut_surfMesh);

    outResult->addGroup(groupOut_grpOut);

}

bool IFP_stepSmoothQSM::recursiveSearchQsm(CT_TNodeGroup *node, int idCurrentBranch, bool isFirst, treeTopology &topo)
{
    bool ok = false;

    CT_ItemIterator itI(node);

    while(itI.hasNext())
    {
        CT_AbstractSingularItemDrawable *item = (CT_AbstractSingularItemDrawable*)itI.next();

        QList<CT_AbstractItemAttribute*> l = item->defaultItemAttributes();
        QListIterator<CT_AbstractItemAttribute*> itA(l);

        treeSegment seg(isFirst,idCurrentBranch);
        while(itA.hasNext()){
            CT_AbstractItemAttribute* att = itA.next();
            std::string key = att->model()->displayableName().toStdString();
            std::string valueStr = att->toString(node, NULL).toStdString();
            double value = parseString (valueStr);
            seg.insertProperties(key,value);
        }

        QList<CT_AbstractItemAttribute*> l2 = item->notDefaultItemAttributes();
        QListIterator<CT_AbstractItemAttribute*> itA2(l2);

        while(itA2.hasNext()){
            CT_AbstractItemAttribute* att = itA2.next();
            std::string key = att->model()->displayableName().toStdString();
            std::string valueStr = att->toString(node, NULL).toStdString();
            double value = parseString (valueStr);
            seg.insertProperties(key,value);
        }
        topo.addSegment(seg);
    }

    CT_TNodeGroup *component = node->rootComponent();

    if(component != NULL){
        ok = recursiveSearchQsm(component,idCurrentBranch,true,topo);
    }


    QListIterator<CT_TNodeGroup*> it(node->branches());
    while(it.hasNext())
    {
        branchCounter++;
        ok = recursiveSearchQsm((CT_TNodeGroup*)it.next(),idCurrentBranch+branchCounter,false,topo);
    }

    int componentId=0;
    if(component != NULL)
    {
        while(component->successor() != NULL)
        {
            component = component->successor();
            componentId++;
            ok = recursiveSearchQsm((CT_TNodeGroup*)component,idCurrentBranch,false,topo);
        }
    }

    return ok;
}

double IFP_stepSmoothQSM::parseString(const std::string& str)
{
    double d;
    try {
        d = std::stod(str);
    }catch(std::invalid_argument& e){
        return 0.;
    }catch (const std::out_of_range&) {
        return 0.;
    }
    return d;
}

bool IFP_stepSmoothQSM::loadVertex(pcl::PointXYZ& x0, CT_Point &point)
{
        point(0) = x0.x;
        point(1) = x0.y;
        point(2) = x0.z;
        return true;
}

CT_Mesh* IFP_stepSmoothQSM::buildMesh(const std::map<unsigned int, pcl::PointXYZ>& pts,const std::vector<triangle>& tri,Eigen::Vector3d& bboxMin,Eigen::Vector3d& bboxMax)
{
    CT_Mesh* mesh = new CT_Mesh();

    int nv = pts.size();
    int nf = tri.size();

    CT_MutablePointIterator beginV = CT_MeshAllocator::AddVertices(mesh, nv);
    CT_MutableFaceIterator beginF = CT_MeshAllocator::AddFaces(mesh, nf);
    CT_MutableEdgeIterator beginH = CT_MeshAllocator::AddHEdges(mesh, nf*3);

    size_t beginVertexIndex = beginV.next().cIndex();
    beginV.toFront();

    size_t nfLoaded = 0;
    size_t nvLoaded = 0;
    CT_Point point;

    for(int i=0;i<pts.size();i++)
    {
        pcl::PointXYZ p = pts.at(i);
        if(loadVertex(p, point)) {
            beginV.next().replaceCurrentPoint(point);
            updateMinMax(point, bboxMin, bboxMax);
            ++nvLoaded;
        }
    }
    for(int i=0;i<tri.size();i++)
    {
        triangle t = tri.at(i);
        if(loadFace(t, beginVertexIndex, mesh, beginF, beginH)) {
            ++nfLoaded;
        }
    }
    return mesh;
}

bool IFP_stepSmoothQSM::loadFace(triangle tri, const size_t &beginVertexIndex, CT_Mesh *mesh, CT_MutableFaceIterator &itFaces, CT_MutableEdgeIterator &itEdges)
{
        size_t p0,p1,p2;

        p0 = beginVertexIndex + tri.pointIndex[0];
        p1 = beginVertexIndex + tri.pointIndex[1];
        p2 = beginVertexIndex + tri.pointIndex[2];

        CT_Face &face = itFaces.next().cT();
        size_t faceIndex = itFaces.cIndex();

        CT_Edge *twinE1 = NULL;
        CT_Edge *twinE2 = NULL;
        CT_Edge *twinE3 = NULL;

        if(searchHalfEdges){
            twinE1 = findHEdgeTwin(mesh, p0, p1);
            twinE2 = findHEdgeTwin(mesh, p1, p2);
            twinE3 = findHEdgeTwin(mesh, p2, p0);
        }

        size_t e1Index = itEdges.next().cIndex();
        size_t e2Index;
        size_t e3Index;

        face.setEdge(e1Index);

        CT_Edge &e1 = itEdges.cT();
        e1.setPoint0(p0);
        e1.setPoint1(p1);
        e1.setFace(faceIndex);

        CT_Edge &e2 = itEdges.next().cT();
        e2.setPoint0(p1);
        e2.setPoint1(p2);
        e2.setFace(faceIndex);
        e2Index = itEdges.cIndex();

        CT_Edge &e3 = itEdges.next().cT();
        e3.setPoint0(p2);
        e3.setPoint1(p0);
        e3.setFace(faceIndex);
        e3Index = itEdges.cIndex();

        e1.setNext(e2Index);
        e1.setPrevious(e3Index);
        e2.setNext(e3Index);
        e2.setPrevious(e1Index);
        e3.setNext(e1Index);
        e3.setPrevious(e2Index);

        if(twinE1 != NULL)
        {
            e1.setTwin(twinE1);
            twinE1->setTwin(e1Index);
        }

        if(twinE2 != NULL)
        {
            e2.setTwin(twinE2);
            twinE2->setTwin(e2Index);
        }

        if(twinE3 != NULL)
        {
            e3.setTwin(twinE3);
            twinE3->setTwin(e3Index);
        }

        return true;
}

void IFP_stepSmoothQSM::updateMinMax(const CT_Point &point, Eigen::Vector3d &bboxMin, Eigen::Vector3d &bboxMax)
{
    bboxMin[0] = qMin(point[CT_Point::X], bboxMin[0]);
    bboxMin[1] = qMin(point[CT_Point::Y], bboxMin[1]);
    bboxMin[2] = qMin(point[CT_Point::Z], bboxMin[2]);

    bboxMax[0] = qMax(point[CT_Point::X], bboxMax[0]);
    bboxMax[1] = qMax(point[CT_Point::Y], bboxMax[1]);
    bboxMax[2] = qMax(point[CT_Point::Z], bboxMax[2]);
}

CT_Edge* IFP_stepSmoothQSM::findHEdgeTwin(const CT_Mesh *mesh, const size_t &p0, const size_t &p1)
{
    if(mesh->abstractHedge() == NULL)
        return 0;

    CT_EdgeIterator it(mesh->abstractHedge());

    while(it.hasNext())
    {
        CT_Edge &he = it.next().cT();

        if(((he.iPointAt(0) == p1)
                            && (he.iPointAt(1) == p0)))
            return &he;
    }

    return 0;
}


