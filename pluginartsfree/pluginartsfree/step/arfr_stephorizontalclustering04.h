/****************************************************************************
 Copyright (C) 2010-2012 the Association de Recherche Technologie et Sciences (ARTS), Ecole Nationale Supérieure d'Arts et Métiers (ENSAM), Cluny, France.
                         All rights reserved.

 Contact : Michael.KREBS@ENSAM.EU

 Developers : Michaël KREBS (ARTS/ENSAM)

 This file is part of PluginARTSFREE library.

 PluginARTSFREE is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginARTSFREE is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginARTSFREE.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ARFR_STEPHORIZONTALCLUSTERING04_H
#define ARFR_STEPHORIZONTALCLUSTERING04_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_pointcloud/abstract/ct_abstractpointcloud.h"
#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

#include <QMutex>

/*!
 * \class ARFR_StepHorizontalClustering04
 * \ingroup Steps_OE
 * \brief <b>Create small clusters of points in horizontal thin layers</b>
 *
 * First this step separate the input scene in horizontal layer of thickness _thicknessInMeters;
 * Second, in each layer, created cluster, merging points in the same cluster if their distance is _maxSearchRadiusInMeters or less.
 * If _searchFinely is set to false, only a relatively poor (but very quick) clustering is done.
 * If possible set _searchFinely to true.
 *
 * \param _maxSearchRadiusInMeters Maximum distance beetween two points of the same cluster
 * \param _thicknessInMeters Horizontal layers thickness along Z axis
 * \param _searchFinely If false only (XY) bounding boxes are compared for distances beetween points. Quicker but not so precise. Should be set to true.
 *
 * <b>Input Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup \n
 *          - CT_Scene (Scene) \n
 *
 * <b>Output Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup (Layer)... \n
 *          - CT_StandardItemGroup (ItemGroup)... \n
 *              - CT_PointCluster (Cluster) \n
 *
 */
class ARFR_StepHorizontalClustering04 : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ARFR_StepHorizontalClustering04(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    class MyPoint
    {
    public:

        MyPoint(const CT_Point &point, const size_t &index)
        {
            _point = (CT_Point*)&point;
            _index = index;
        }

        CT_Point *_point;
        size_t    _index;
    };

    double  _maxSearchRadiusInMeters;   /*!< distance d'un point à un groupe */
    double  _thicknessInMeters;         /*!< l'epaisseur des couches */

    int     _sizeMap;
    int     _nLayerFinished;
    QMutex  _mutexUpdateProgress;

    // champs rendus disponibles par context.step()
    CT_ResultGroup                              *_outResult;


    void addLayerFinished();

    static void staticComputeLayer(CT_StandardItemGroup *layer);

    static bool staticAddPointToPointCluster(const CT_Point &point,
                                             const size_t &pointIndex,
                                             CT_PointCluster &pCluster,
                                             ARFR_StepHorizontalClustering04 *ptrClass);

    static CT_PointCluster* staticCombinePointCluster(int id,
                                                      CT_AbstractResult &outResult,
                                                      CT_PointCluster &pClusterToOptimize,
                                                      CT_PointCluster &pClusterToCombine, ARFR_StepHorizontalClustering04 *ptrClass);
};

#endif // ARFR_STEPHORIZONTALCLUSTERING04_H
