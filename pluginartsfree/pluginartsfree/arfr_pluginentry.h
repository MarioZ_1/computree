/****************************************************************************
 Copyright (C) 2010-2012 the Association de Recherche Technologie et Sciences (ARTS), Ecole Nationale Supérieure d'Arts et Métiers (ENSAM), Cluny, France.
                         All rights reserved.

 Contact : Michael.KREBS@ENSAM.EU

 Developers : Michaël KREBS (ARTS/ENSAM)

 This file is part of PluginARTSFREE library.

 PluginARTSFREE is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginARTSFREE is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginARTSFREE.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ARFR_PLUGINENTRY_H
#define ARFR_PLUGINENTRY_H

#include "interfaces.h"

class ARFR_StepPluginManager;

class ARFR_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    ARFR_PluginEntry();
    ~ARFR_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    ARFR_StepPluginManager *_stepPluginManager;
};

#endif // ARFR_PLUGINENTRY_H
