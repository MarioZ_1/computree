/****************************************************************************
 Copyright (C) 2010-2012 the Association de Recherche Technologie et Sciences (ARTS), Ecole Nationale Supérieure d'Arts et Métiers (ENSAM), Cluny, France.
                         All rights reserved.

 Contact : Michael.KREBS@ENSAM.EU

 Developers : Michaël KREBS (ARTS/ENSAM)

 This file is part of PluginARTSFREE library.

 PluginARTSFREE is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginARTSFREE is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginARTSFREE.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "arfr_steppluginmanager.h"

#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"

#include "step/arfr_stephorizontalclustering04.h"
#include "step/arfr_stephorizontalclustering05.h"


ARFR_StepPluginManager::ARFR_StepPluginManager() : CT_AbstractStepPlugin()
{
}

ARFR_StepPluginManager::~ARFR_StepPluginManager()
{
}

QString ARFR_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin ARTS-Free for Computree\n"
           "AU  - Krebs, Michael\n"
           "PB  - AMVALOR, Equipe Bois\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-arts-free/wiki\n"
           "ER  - \n";
}


bool ARFR_StepPluginManager::loadGenericsStep()
{
    addNewPointsStep<ARFR_StepHorizontalClustering04>(CT_StepsMenu::LP_Clusters);
    addNewPointsStep<ARFR_StepHorizontalClustering05>(CT_StepsMenu::LP_Clusters);

    return true;
}

bool ARFR_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool ARFR_StepPluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool ARFR_StepPluginManager::loadActions()
{
    return true;
}

bool ARFR_StepPluginManager::loadExporters()
{
	return true;
}

bool ARFR_StepPluginManager::loadReaders()
{
    return true;
}
