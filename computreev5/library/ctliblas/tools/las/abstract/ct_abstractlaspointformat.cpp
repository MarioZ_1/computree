#include "ct_abstractlaspointformat.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloud/ct_internalpointcloud.h"

#include <QDebug>

CT_AbstractLASPointFormat::CT_AbstractLASPointFormat()
{
    m_lasHeader = NULL;
}

CT_AbstractLASPointFormat::~CT_AbstractLASPointFormat()
{
    clearInfos();
}

void CT_AbstractLASPointFormat::setHeader(const CT_LASHeader *header)
{
    m_lasHeader = (CT_LASHeader*)header;
}

void CT_AbstractLASPointFormat::setAttributes(const QHash<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> &attributes)
{
    m_attributes = attributes;
}

const QHash<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> &CT_AbstractLASPointFormat::attributes() const
{
    return m_attributes;
}

bool CT_AbstractLASPointFormat::initWrite(bool fastButConsumeMoreMemory)
{
    qDebug() << "aa";
    // get type of attribute to search for point format X (from derivated class)
    QList<CT_LasDefine::LASPointAttributesType> types = typesToSearch();
    qDebug() << "bb";

    QListIterator<CT_LasDefine::LASPointAttributesType> itT(types);
    qDebug() << "cc";

    if(itT.hasNext()) {

        qDebug() << "dd";
        if(fastButConsumeMoreMemory)
            m_infosFaster.resize(PS_REPOSITORY->internalConstPointCloud()->size());
        qDebug() << "ee";

        // for each type of attribute
        while(itT.hasNext())
        {
            qDebug() << "ff";
            CT_LasDefine::LASPointAttributesType type = itT.next();
            qDebug() << "gg";

            // get it in the attributes map
            CT_AbstractPointAttributesScalar *scalar = m_attributes.value(type, NULL);
            qDebug() << "hh";

            // if exist
            if(scalar != NULL)
            {
                qDebug() << "ii";
                // get the point cloud index
                const CT_AbstractPointCloudIndex *indexes = scalar->getPointCloudIndex();

                if(indexes != NULL)
                {
                    qDebug() << "jj";
                    size_t pIndex = 0;

                    CT_PointIterator it(indexes);
                    qDebug() << "kk";

                    // for each index
                    while(it.hasNext()) {

                        // get info for this global point index
                        CT_LasPointInfo *info = NULL;

                        if(fastButConsumeMoreMemory) {
                            info = &m_infosFaster[it.next().cIndex()];
                        } else {
                            info = m_infos.value(it.next().cIndex(), NULL);

                            // if info doesn't already exist
                            if(info == NULL)
                            {
                                // create it
                                info = new CT_LasPointInfo();

                                // insert in the info map
                                m_infos.insert(it.cIndex(), info);
                            }
                        }

                        // and set it the attribute
                        info->setAttribute(type, scalar, pIndex);

                        ++pIndex;
                    }
                    qDebug() << "ll";

                }
                qDebug() << "mm";

            }
            qDebug() << "nn";

        }
        qDebug() << "oo";

    }
    qDebug() << "pp";


    return true;
}

void CT_AbstractLASPointFormat::clearInfos()
{
    qDeleteAll(m_infos.begin(), m_infos.end());
    m_infos.clear();

    m_infosFaster.resize(0);
    m_infosFaster.shrink_to_fit();
}
