unix {
    GEOS_BASE_PATH = "/usr/"

    GEOS_INC_PATH = "$${GEOS_BASE_PATH}include"
    GEOS_LIBS_PATH = "$${GEOS_BASE_PATH}lib/x86_64-linux-gnu"
}

windows {
    GEOS_BASE_PATH = "$$PWD/../ComputreeDependencies/geos/"

    GEOS_INC_PATH = $${GEOS_BASE_PATH}include
    GEOS_LIBS_PATH = $${GEOS_BASE_PATH}lib
}
