#ifndef ICONTEXTACCESSOR_HPP
#define ICONTEXTACCESSOR_HPP

#include "icontextaccessor.h"

template<typename RendererContextT>
IContextAccessor<RendererContextT>::~IContextAccessor()
{
    qDeleteAll(m_contexts.begin(), m_contexts.end());
}

template<typename RendererContextT>
void IContextAccessor<RendererContextT>::addedTo(const IGraphicsDocument* doc, const QGLContext* obsoleteContext, const QOpenGLContext* newContext)
{
    if(m_contexts.contains((QGLContext*)obsoleteContext))
        return;

    RendererContextT *c = new RendererContextT(doc, obsoleteContext, newContext);
    m_contexts.insert((QGLContext*)obsoleteContext, c);
}

template<typename RendererContextT>
void IContextAccessor<RendererContextT>::removedFrom(const IGraphicsDocument* doc, const QGLContext *obsoleteContext, const QOpenGLContext* newContext)
{
    Q_UNUSED(doc)
    Q_UNUSED(newContext)

    if(!m_contexts.contains((QGLContext*)obsoleteContext))
        return;

    AMKglEA->contextAboutToBeDeleted(obsoleteContext);

    RendererContextT *c = m_contexts.take((QGLContext*)obsoleteContext);
    delete c;
}

template<typename RendererContextT>
const QHash<QGLContext*,RendererContextT*>& IContextAccessor<RendererContextT>::getContexts() const
{
    return m_contexts;
}

template<typename RendererContextT>
RendererContextT* IContextAccessor<RendererContextT>::getCurrentContext() const
{
    return getContexts().value((QGLContext*)QGLContext::currentContext(), NULL);
}

template<typename RendererContextT>
IGraphicsDocument* IContextAccessor<RendererContextT>::getCurrentDocument() const
{
    if(getCurrentContext() == NULL)
        return NULL;

    return getCurrentContext()->getDocument();
}

#endif // ICONTEXTACCESSOR_HPP
