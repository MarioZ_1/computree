#ifndef CT_ABSTRACTMODIFIABLECLOUDINDEXT_H
#define CT_ABSTRACTMODIFIABLECLOUDINDEXT_H

#include "ct_cloudindex/abstract/ct_abstractcloudindext.h"
#include "ct_cloudindex/tools/ct_modifiablecloudindexiteratort.h"
#include "ct_cloudindex/abstract/ct_abstractmodifiablecloudindex.h"

/**
 * A MODIFIABLE cloud of index (size_t) for T (point, face, etc...)
 */
template<typename T>
class CT_AbstractModifiableCloudIndexT : virtual public CT_AbstractCloudIndexT<T>, public CT_AbstractModifiableCloudIndex
{
public:
    CT_AbstractModifiableCloudIndexT() : CT_AbstractCloudIndexT<T>() {}
};

#endif // CT_ABSTRACTMODIFIABLECLOUDINDEXT_H
