#include "ct_standardcircledrawmanager.h"

#include "ct_itemdrawable/ct_circle.h"

const QString CT_StandardCircleDrawManager::INDEX_CONFIG_DRAW_CIRCLE = CT_StandardCircleDrawManager::staticInitConfigDrawCircle();


CT_StandardCircleDrawManager::CT_StandardCircleDrawManager(QString drawConfigurationName) : CT_StandardAbstractShapeDrawManager(drawConfigurationName.isEmpty() ? CT_Circle::staticName() : drawConfigurationName)
{
}

CT_StandardCircleDrawManager::~CT_StandardCircleDrawManager()
{
}

void CT_StandardCircleDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractShapeDrawManager::draw(view, painter, itemDrawable);

    const CT_Circle &item = dynamic_cast<const CT_Circle&>(itemDrawable);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DRAW_CIRCLE).toBool())
    {
        painter.drawCircle3D(item.getCenter(), item.getDirection(), item.getRadius());
    }
}



CT_ItemDrawableConfiguration CT_StandardCircleDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractShapeDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(CT_StandardCircleDrawManager::staticInitConfigDrawCircle() ,QObject::tr("Dessiner le Cercle"), CT_ItemDrawableConfiguration::Bool, true);

    return item;
}

// PROTECTED //

QString CT_StandardCircleDrawManager::staticInitConfigDrawCircle()
{
    return "CIR_DRW";
}
