#ifndef CT_STANDARDGRID3DDRAWMANAGER_HPP
#define CT_STANDARDGRID3DDRAWMANAGER_HPP

#include "ct_itemdrawable/tools/drawmanager/ct_standardgrid3ddrawmanager.h"
#include "ct_itemdrawable/ct_grid3d.h"
#include <typeinfo>
#include <QObject>

#include "qdebug.h"

template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_3D_MODE_ENABLED = CT_StandardGrid3DDrawManager<DataT>::staticInitConfig3DModeEnabled();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_WIRE_MODE_ENABLED = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigWireModeEnabled();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_LOW_THRESHOLDS_ENABLED = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigLowThresholdsEnabled();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIGH_THRESHOLDS_ENABLED = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigHighThresholdsEnabled();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_LOW_THRESHOLDS_VALUE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigLowThresholdValue();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIGH_THRESHOLDS_VALUE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigHighThresholdValue();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_REDUCTION_COEF = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigReductionCoef();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_USE_PREDEFINED_COLORS = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigUsePredefinedColors();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_TRANSPARENCY_VALUE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigTransparencyValue();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_XINF = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXinf();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_XSUP = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXsup();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_YINF = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYinf();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_YSUP = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYsup();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_ZINF = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigZinf();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_HIDE_PLANE_NB_ZSUP = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigZsup();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_SHOW_TRUES_ONLY = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigShowTrueOnly();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_SLICE_MODE_ENABLED = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigSliceModeEnabled();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_XY_SLICE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXYSlice();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_XZ_SLICE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXZSlice();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_YZ_SLICE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYZSlice();

template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_XY_SLICE_PLANE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXYSlicePlane();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_XZ_SLICE_PLANE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXZSlicePlane();
template< typename DataT > const QString CT_StandardGrid3DDrawManager<DataT>::INDEX_CONFIG_YZ_SLICE_PLANE = CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYZSlicePlane();

template< typename DataT >
CT_StandardGrid3DDrawManager<DataT>::CT_StandardGrid3DDrawManager(QString drawConfigurationName, bool mode3DOn)
    : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? CT_Grid3D<DataT>::staticName() : drawConfigurationName)
{
    _mode3DOn = mode3DOn;
}

template< typename DataT >
CT_StandardGrid3DDrawManager<DataT>::~CT_StandardGrid3DDrawManager()
{
}

template< typename DataT >
void CT_StandardGrid3DDrawManager<DataT>::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const CT_Grid3D<DataT> &item = dynamic_cast<const CT_Grid3D<DataT>&>(itemDrawable);

    // Getting the configuration values
    GLenum  drawingMode;
    bool    mode3D = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_3D_MODE_ENABLED).toBool();
    bool    modeSlice = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SLICE_MODE_ENABLED).toBool();

    bool    wireMode = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_WIRE_MODE_ENABLED).toBool();
    bool    uselowThresh = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_LOW_THRESHOLDS_ENABLED).toBool();
    bool    usehighThresh = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIGH_THRESHOLDS_ENABLED).toBool();
    double  lowThresh = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_LOW_THRESHOLDS_VALUE).toDouble();
    double  highThresh = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIGH_THRESHOLDS_VALUE).toDouble();
    double  reductionCoef = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_REDUCTION_COEF).toDouble();
    int     transparencyValue = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_TRANSPARENCY_VALUE).toInt();
    bool    usePredefinedColors = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_USE_PREDEFINED_COLORS).toBool();

    size_t     nXinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_XINF).toInt();
    size_t     nXsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_XSUP).toInt();
    size_t     nYinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_YINF).toInt();
    size_t     nYsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_YSUP).toInt();
    size_t     nZinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_ZINF).toInt();
    size_t     nZsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_ZSUP).toInt();

    bool    xy = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XY_SLICE).toBool();
    bool    xz = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XZ_SLICE).toBool();
    bool    yz = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_YZ_SLICE).toBool();
    int     xyplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XY_SLICE_PLANE).toInt();
    int     xzplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XZ_SLICE_PLANE).toInt();
    int     yzplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_YZ_SLICE_PLANE).toInt();


    if (nXsup > item.xdim()) {nXsup = item.xdim();}
    if (nYsup > item.ydim()) {nXsup = item.ydim();}
    if (nZsup > item.zdim()) {nXsup = item.zdim();}

    if (xyplane < 0) {xyplane = 0;}
    if (xzplane < 0) {xzplane = 0;}
    if (yzplane < 0) {yzplane = 0;}

    if (xyplane > item.zdim()) {xyplane = item.zdim();}
    if (xzplane > item.ydim()) {xzplane = item.ydim();}
    if (yzplane > item.xdim()) {yzplane = item.xdim();}


    if (transparencyValue > 255) {transparencyValue = 255;}
    if (transparencyValue < 0) {transparencyValue = 0;}

    if ( wireMode ) {drawingMode = GL_LINE;}
    else            {drawingMode = GL_FILL;}

    if (!uselowThresh) {lowThresh =  (double)item.dataMin();}
    if (!usehighThresh) {highThresh =  (double)item.dataMax();}

    bool drawAsMap = true;
    if (lowThresh == highThresh) {drawAsMap = false;} // Si les limites sont égales : une seule couleur

    QColor color = painter.getColor();

    // For each voxel of the grid
    double scaling = 240.0 / (highThresh - lowThresh);
    double offset = - (240*lowThresh)/(highThresh - lowThresh);

    size_t xdim = item.xdim();
    size_t ydim = item.ydim();
    size_t zdim = item.zdim();
    double demiRes = reductionCoef*item.resolution() / 2.0;

    double xmin, ymin, zmin, xmax, ymax, zmax;

    if (mode3D)
    {
        // For each voxel of the grid
        for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
        {
            for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        DataT data = item.valueAtIndex(index);

                        // Draw a cube if the value it contains is between the two thresholds
                        if ( data >= lowThresh && data <= highThresh )
                        {
                            bool predef = false;
                            if (usePredefinedColors && item.colorsDefined())
                            {
                                painter.setColor(item.getColorForValue(data));
                                predef = true;
                            }

                            if (!predef && drawAsMap && !itemDrawable.isSelected())
                            {
                                double h = (int) qRound((data*scaling) + offset);
                                painter.setColor( QColor::fromHsv(h,255,255,transparencyValue) );
                            } else if (!predef){
                                painter.setColor(QColor(255,255,255, transparencyValue));
                            }

                            xmin = item.getCellCenterX(xx) - demiRes;
                            ymin = item.getCellCenterY(yy) - demiRes;
                            zmin = item.getCellCenterZ(zz) - demiRes;
                            xmax = item.getCellCenterX(xx) + demiRes;
                            ymax = item.getCellCenterY(yy) + demiRes;
                            zmax = item.getCellCenterZ(zz) + demiRes;

                            painter.drawCube( xmin, ymin, zmin, xmax, ymax, zmax, GL_FRONT_AND_BACK, drawingMode );
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }
    }

    if (modeSlice)
    {

        if (xy)
        {
            for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
            {
                for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
                {
                    size_t zz = xyplane;
                    double z_val = item.getCellCenterZ(zz);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        DataT data = item.valueAtIndex(index);

                        // Draw a cube if the value it contains is between the two thresholds
                        if ( data >= lowThresh && data <= highThresh )
                        {
                            bool predef = false;
                            if (usePredefinedColors && item.colorsDefined())
                            {
                                painter.setColor(item.getColorForValue(data));
                                predef = true;
                            }

                            if (!predef && drawAsMap && !itemDrawable.isSelected())
                            {
                                double h = (int) qRound((data*scaling) + offset);
                                painter.setColor( QColor::fromHsv(h,255,255,transparencyValue) );
                            } else if (!predef){
                                painter.setColor(QColor(255,255,255, transparencyValue));
                            }

                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterX(xx) - demiRes;
                            minPix(1) = item.getCellCenterY(yy) - demiRes;
                            maxPix(0) = item.getCellCenterX(xx) + demiRes;
                            maxPix(1) = item.getCellCenterY(yy) + demiRes;

                            painter.fillRectXY(minPix, maxPix, z_val);
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }

        if (xz)
        {
            for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t yy = xzplane;
                    double y_val = item.getCellCenterY(yy);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        DataT data = item.valueAtIndex(index);

                        // Draw a cube if the value it contains is between the two thresholds
                        if ( data >= lowThresh && data <= highThresh )
                        {
                            bool predef = false;
                            if (usePredefinedColors && item.colorsDefined())
                            {
                                painter.setColor(item.getColorForValue(data));
                                predef = true;
                            }

                            if (!predef && drawAsMap && !itemDrawable.isSelected())
                            {
                                double h = (int) qRound((data*scaling) + offset);
                                painter.setColor( QColor::fromHsv(h,255,255,transparencyValue) );
                            } else if (!predef){
                                painter.setColor(QColor(255,255,255, transparencyValue));
                            }

                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterX(xx) - demiRes;
                            minPix(1) = item.getCellCenterZ(zz) - demiRes;
                            maxPix(0) = item.getCellCenterX(xx) + demiRes;
                            maxPix(1) = item.getCellCenterZ(zz) + demiRes;

                            painter.fillRectXZ(minPix, maxPix, y_val);
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }

        if (yz)
        {
            for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t xx = yzplane;
                    double x_val = item.getCellCenterX(xx);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        DataT data = item.valueAtIndex(index);

                        // Draw a cube if the value it contains is between the two thresholds
                        if ( data >= lowThresh && data <= highThresh )
                        {
                            bool predef = false;
                            if (usePredefinedColors && item.colorsDefined())
                            {
                                painter.setColor(item.getColorForValue(data));
                                predef = true;
                            }

                            if (!predef && drawAsMap && !itemDrawable.isSelected())
                            {
                                double h = (int) qRound((data*scaling) + offset);
                                painter.setColor( QColor::fromHsv(h,255,255,transparencyValue) );
                            } else if (!predef){
                                painter.setColor(QColor(255,255,255, transparencyValue));
                            }

                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterY(yy) - demiRes;
                            minPix(1) = item.getCellCenterZ(zz) - demiRes;
                            maxPix(0) = item.getCellCenterY(yy) + demiRes;
                            maxPix(1) = item.getCellCenterZ(zz) + demiRes;

                            painter.fillRectYZ(minPix, maxPix, x_val);
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }

    }

    painter.setColor(color);
}

template< typename DataT >
CT_ItemDrawableConfiguration CT_StandardGrid3DDrawManager<DataT>::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addNewConfiguration(staticInitConfig3DModeEnabled(), QObject::tr("Mode 3D"), CT_ItemDrawableConfiguration::Bool, _mode3DOn);             // Draw the grid in 3D Mode
    item.addNewConfiguration(staticInitConfigWireModeEnabled(), QObject::tr("Mode filaire"), CT_ItemDrawableConfiguration::Bool, true);             // Draw the grid in wire mode
    item.addNewConfiguration(staticInitConfigLowThresholdsEnabled(), QObject::tr("Forcer limite basse"), CT_ItemDrawableConfiguration::Bool, true);     // Using thresholds or not
    item.addNewConfiguration(staticInitConfigLowThresholdValue(), QObject::tr("Limite basse (forcée)"), CT_ItemDrawableConfiguration::Double, 0.001 );         // Voxels with value lesser than this threshold will not be drawn
    item.addNewConfiguration(staticInitConfigHighThresholdsEnabled(), QObject::tr("Forcer limite haute"), CT_ItemDrawableConfiguration::Bool, false);     // Using thresholds or not
    item.addNewConfiguration(staticInitConfigHighThresholdValue(), QObject::tr("Limite haute (forcée)"), CT_ItemDrawableConfiguration::Double, 100 );        // Voxels with value greater than this threshold will not be drawn
    item.addNewConfiguration(staticInitConfigReductionCoef(), QObject::tr("Coef. de reduction"), CT_ItemDrawableConfiguration::Double, 1);
    item.addNewConfiguration(staticInitConfigUsePredefinedColors(), QObject::tr("Utiliser couleurs pré-définies"), CT_ItemDrawableConfiguration::Bool, true);             // Draw the grid with pre-defined colors
    item.addNewConfiguration(staticInitConfigTransparencyValue(), QObject::tr("Valeur de transparence"), CT_ItemDrawableConfiguration::Int, 100);
    item.addNewConfiguration(staticInitConfigXinf(), QObject::tr("Nb. Plans masqués X-"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigXsup(), QObject::tr("Nb. Plans masqués X+"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYinf(), QObject::tr("Nb. Plans masqués Y-"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYsup(), QObject::tr("Nb. Plans masqués Y+"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigZinf(), QObject::tr("Nb. Plans masqués Z-"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigZsup(), QObject::tr("Nb. Plans masqués Z+"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigSliceModeEnabled(), QObject::tr("Mode Slice"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigXYSlice(), QObject::tr("XY"), CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigXZSlice(), QObject::tr("XZ"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigYZSlice(), QObject::tr("YZ"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigXYSlicePlane(), QObject::tr("XY Slice"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigXZSlicePlane(), QObject::tr("XZ Slice"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYZSlicePlane(), QObject::tr("YZ Slice"), CT_ItemDrawableConfiguration::Int, 0);

    return item;
}

// PROTECTED //

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfig3DModeEnabled()
{
    return "A3DGD_M3D";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigWireModeEnabled()
{
    return "A3DGD_WME";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigLowThresholdsEnabled()
{
    return "A3DGD_LTE";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigHighThresholdsEnabled()
{
    return "A3DGD_HTE";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigLowThresholdValue()
{
    return "A3DGD_LTV";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigHighThresholdValue()
{
    return "A3DGD_HTV";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigReductionCoef()
{
    return "A3DGD_RDC";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigUsePredefinedColors()
{
    return "A3DGD_PDC";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigTransparencyValue()
{
    return "A3DGD_TRV";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXinf()
{
    return "A3DGD_XIN";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXsup()
{
    return "A3DGD_XSU";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYinf()
{
    return "A3DGD_YIN";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYsup()
{
    return "A3DGD_YSU";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigZinf()
{
    return "A3DGD_ZIN";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigZsup()
{
    return "A3DGD_ZSU";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigShowTrueOnly()
{
    return "A3DGD_STO";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigSliceModeEnabled()
{
    return "A3DGD_MSL";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXYSlice()
{
    return "A3DGD_SXY";
}


template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXZSlice()
{
    return "A3DGD_SXZ";
}


template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYZSlice()
{
    return "A3DGD_SYZ";
}

template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXYSlicePlane()
{
    return "A3DGD_PXY";
}


template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigXZSlicePlane()
{
    return "A3DGD_PXZ";
}


template< typename DataT >
QString CT_StandardGrid3DDrawManager<DataT>::staticInitConfigYZSlicePlane()
{
    return "A3DGD_PYZ";
}




#endif // CT_STANDARDGRID3DDRAWMANAGER_HPP
