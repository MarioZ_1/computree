#include "ct_standardgrid3ddrawmanager.h"

/////////////////////////////////////////////////////////////////////
/// Specialisations for bool type ///////////////////////////////////
/////////////////////////////////////////////////////////////////////

template<>
void CT_StandardGrid3DDrawManager<bool>::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const CT_Grid3D<bool> &item = dynamic_cast<const CT_Grid3D<bool>&>(itemDrawable);

    // Getting the configuration values
    GLenum  drawingMode;
    bool    mode3D = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_3D_MODE_ENABLED).toBool();
    bool    modeSlice = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SLICE_MODE_ENABLED).toBool();

    bool    wireMode = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_WIRE_MODE_ENABLED).toBool();
    double  reductionCoef = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_REDUCTION_COEF).toDouble();
    int     transparencyValue = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_TRANSPARENCY_VALUE).toInt();
    bool    showTrueOnly = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SHOW_TRUES_ONLY).toBool();

    size_t     nXinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_XINF).toUInt();
    size_t     nXsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_XSUP).toUInt();
    size_t     nYinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_YINF).toUInt();
    size_t     nYsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_YSUP).toUInt();
    size_t     nZinf = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_ZINF).toUInt();
    size_t     nZsup = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HIDE_PLANE_NB_ZSUP).toUInt();

    bool    xy = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XY_SLICE).toBool();
    bool    xz = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XZ_SLICE).toBool();
    bool    yz = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_YZ_SLICE).toBool();
    int     xyplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XY_SLICE_PLANE).toInt();
    int     xzplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_XZ_SLICE_PLANE).toInt();
    int     yzplane = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_YZ_SLICE_PLANE).toInt();


    if (nXinf < 0) {nXinf = 0;}
    if (nYinf < 0) {nYinf = 0;}
    if (nZinf < 0) {nZinf = 0;}

    if (nXsup > item.xdim()) {nXsup = item.xdim();}
    if (nYsup > item.ydim()) {nXsup = item.ydim();}
    if (nZsup > item.zdim()) {nXsup = item.zdim();}

    if (xyplane < 0) {xyplane = 0;}
    if (xzplane < 0) {xzplane = 0;}
    if (yzplane < 0) {yzplane = 0;}

    if (xyplane > item.zdim()) {xyplane = item.zdim();}
    if (xzplane > item.ydim()) {xzplane = item.ydim();}
    if (yzplane > item.xdim()) {yzplane = item.xdim();}


    if (transparencyValue > 255) {transparencyValue = 255;}
    if (transparencyValue < 0) {transparencyValue = 0;}

    if ( wireMode ) {drawingMode = GL_LINE;}
    else            {drawingMode = GL_FILL;}

    bool drawAsMap = true;

    QColor color = painter.getColor();

    size_t xdim = item.xdim();
    size_t ydim = item.ydim();
    size_t zdim = item.zdim();
    double demiRes = reductionCoef*item.resolution() / 2.0;

    double xmin, ymin, zmin, xmax, ymax, zmax;

    if (mode3D)
    {

        // For each voxel of the grid
        for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
        {
            for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t index;

                    if (item.index(xx, yy, zz, index))
                    {
                        bool data = item.valueAtIndex(index);

                        if (data || !showTrueOnly)
                        {
                            if (drawAsMap && !itemDrawable.isSelected())
                            {
                                if (data)
                                {
                                    painter.setColor(QColor(0,255,0, transparencyValue));
                                } else {
                                    painter.setColor(QColor(255,0,0, transparencyValue));
                                }


                                xmin = item.getCellCenterX(xx) - demiRes;
                                ymin = item.getCellCenterY(yy) - demiRes;
                                zmin = item.getCellCenterZ(zz) - demiRes;
                                xmax = item.getCellCenterX(xx) + demiRes;
                                ymax = item.getCellCenterY(yy) + demiRes;
                                zmax = item.getCellCenterZ(zz) + demiRes;

                                painter.drawCube( xmin, ymin, zmin, xmax, ymax, zmax, GL_FRONT_AND_BACK, drawingMode );
                            }
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }
    }

    if (modeSlice)
    {

        if (xy)
        {
            for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
            {
                for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
                {
                    size_t zz = xyplane;
                    double z_val = item.getCellCenterZ(zz);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        bool data = item.valueAtIndex(index);

                        if (data || !showTrueOnly)
                        {
                            if (drawAsMap && !itemDrawable.isSelected())
                            {
                                if (data)
                                {
                                    painter.setColor(QColor(0,255,0, transparencyValue));
                                } else {
                                    painter.setColor(QColor(255,0,0, transparencyValue));
                                }
                            }

                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterX(xx) - demiRes;
                            minPix(1) = item.getCellCenterY(yy) - demiRes;
                            maxPix(0) = item.getCellCenterX(xx) + demiRes;
                            maxPix(1) = item.getCellCenterY(yy) + demiRes;

                            painter.fillRectXY(minPix, maxPix, z_val);
                        }

                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }


        if (xz)
        {
            for (size_t xx = nXinf ; xx < (xdim - nXsup) ; xx++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t yy = xzplane;
                    double y_val = item.getCellCenterY(yy);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        bool data = item.valueAtIndex(index);

                        if (data || !showTrueOnly)
                        {
                            if (drawAsMap && !itemDrawable.isSelected())
                            {
                                if (data)
                                {
                                    painter.setColor(QColor(0,255,0, transparencyValue));
                                } else {
                                    painter.setColor(QColor(255,0,0, transparencyValue));
                                }
                            }

                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterX(xx) - demiRes;
                            minPix(1) = item.getCellCenterZ(zz) - demiRes;
                            maxPix(0) = item.getCellCenterX(xx) + demiRes;
                            maxPix(1) = item.getCellCenterZ(zz) + demiRes;

                            painter.fillRectXZ(minPix, maxPix, y_val);
                        }

                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }

        if (yz)
        {
            for (size_t yy = nYinf ; yy < (ydim - nYsup)  ; yy++)
            {
                for (size_t zz = nZinf ; zz < (zdim - nZsup); zz++)
                {
                    size_t xx = yzplane;
                    double x_val = item.getCellCenterX(xx);

                    size_t index;
                    if (item.index(xx, yy, zz, index))
                    {
                        bool data = item.valueAtIndex(index);

                        if (data || !showTrueOnly)
                        {
                            if (drawAsMap && !itemDrawable.isSelected())
                            {
                                if (data)
                                {
                                    painter.setColor(QColor(0,255,0, transparencyValue));
                                } else {
                                    painter.setColor(QColor(255,0,0, transparencyValue));
                                }
                            }


                            Eigen::Vector2d minPix, maxPix;
                            minPix(0) = item.getCellCenterY(yy) - demiRes;
                            minPix(1) = item.getCellCenterZ(zz) - demiRes;
                            maxPix(0) = item.getCellCenterY(yy) + demiRes;
                            maxPix(1) = item.getCellCenterZ(zz) + demiRes;

                            painter.fillRectYZ(minPix, maxPix, x_val);
                        }
                    } else {
                        qDebug() << "Problème d'index (drawmanager)";
                    }
                }
            }
        }

    }

    painter.setColor(color);
}

template<>
CT_ItemDrawableConfiguration CT_StandardGrid3DDrawManager<bool>::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addNewConfiguration(staticInitConfig3DModeEnabled(), QObject::tr("Mode 3D"), CT_ItemDrawableConfiguration::Bool, _mode3DOn);             // Draw the grid in 3D Mode
    item.addNewConfiguration(staticInitConfigWireModeEnabled(), "Mode filaire", CT_ItemDrawableConfiguration::Bool, true);             // Draw the grid in wire mode
    item.addNewConfiguration(staticInitConfigReductionCoef(), "Coef. de reduction", CT_ItemDrawableConfiguration::Double, 1);
    item.addNewConfiguration(staticInitConfigShowTrueOnly(), "Valeurs TRUE seulement", CT_ItemDrawableConfiguration::Bool, 1);
    item.addNewConfiguration(staticInitConfigTransparencyValue(), "Valeur de transparence", CT_ItemDrawableConfiguration::Int, 100);
    item.addNewConfiguration(staticInitConfigXinf(), "Nb. Plans masqués X-", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigXsup(), "Nb. Plans masqués X+", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYinf(), "Nb. Plans masqués Y-", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYsup(), "Nb. Plans masqués Y+", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigZinf(), "Nb. Plans masqués Z-", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigZsup(), "Nb. Plans masqués Z+", CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigSliceModeEnabled(), QObject::tr("Mode Slice"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigXYSlice(), QObject::tr("XY"), CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigXZSlice(), QObject::tr("XZ"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigYZSlice(), QObject::tr("YZ"), CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigXYSlicePlane(), QObject::tr("XY Slice"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigXZSlicePlane(), QObject::tr("XZ Slice"), CT_ItemDrawableConfiguration::Int, 0);
    item.addNewConfiguration(staticInitConfigYZSlicePlane(), QObject::tr("YZ Slice"), CT_ItemDrawableConfiguration::Int, 0);

    return item;
}

