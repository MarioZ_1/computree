#!/bin/bash
for f in $(find /home/drsnuggles/Documents/computree/pluginSimpleForest/pluginsimpleforest/steps/feature/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp'); do clang-format -i $f; done

for f in $(find /home/drsnuggles/Documents/computree/pluginSimpleForest/pluginsimpleforest/steps/manipulation/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp'); do clang-format -i $f; done

for f in $(find /home/drsnuggles/Documents/computree/pluginSimpleForest/pluginsimpleforest/steps/param/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp'); do clang-format -i $f; done

for f in $(find /home/drsnuggles/Documents/computree/pluginSimpleForest/pluginsimpleforest/steps/qsm/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp'); do clang-format -i $f; done

for f in $(find /home/drsnuggles/Documents/computree/pluginSimpleForest/pluginsimpleforest/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp'); do clang-format -i $f; done
