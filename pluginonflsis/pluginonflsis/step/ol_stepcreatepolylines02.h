/****************************************************************************

 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                     and the Laboratoire des Sciences de l'Information et des Systèmes (LSIS), Marseille, France.
                     All rights reserved.

 Contact : alexandre.piboule@onf.fr
           alexandra.bac@esil.univmed.fr

 Developers : Joris Ravaglia (ONF/LSIS)
 With adaptations by : Alexandre PIBOULE (ONF)

 This file is part of PluginONFLSIS library 2.0.

 PluginONFLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginShared is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginShared.  If not, see <http://www.gnu.org/licenses/lgpl.html>.

*****************************************************************************/

#ifndef OL_STEPCREATEPOLYLINES02_H
#define OL_STEPCREATEPOLYLINES02_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"
#include "ct_itemdrawable/abstract/ct_abstractitemgroup.h"


/*!
 * \class OL_StepCreatePolylines02
 * \ingroup Steps_OL
 * \brief <b>Create polylines from point clusters.</b>
 *
 * For each point cluster, adds a polyline aside.
 * A polyline is juste an ordered version of the cluster.
 * The order is obtained by proximity testing in (XY) horizontal projection.
 * So the resulting polylines are 2D polylines (even if they are represented by linking 3D positions of points).
 *
 * <b>Input Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup... \n
 *          - CT_PointCluster (cluster) \n
 *
 * <b>Output Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup... \n
 *          - <em>cpy CT_PointCluster (Cluster)</em> \n
 *          - <em>cpy+ CT_PolyLine (Polyline)</em> \n
 *          - <em>cpy (...)</em> \n
 *
 */
class OL_StepCreatePolylines02 : public CT_AbstractStep
{
    // IMPORTANT pour avoir le nom de l'étape
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    OL_StepCreatePolylines02(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    CT_AutoRenameModels             _outPolylinesModelName;
    CT_ResultGroup*                 _outRes;

    bool                    _multiThread;


    static void staticCreatePolyline(CT_AbstractItemGroup *group);

};

#endif // OL_STEPCREATEPOLYLINES02_H
