#ifndef LIF_PLUGINENTRY_H
#define LIF_PLUGINENTRY_H

#include "interfaces.h"

class LIF_PluginManager;

class LIF_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    LIF_PluginEntry();
    ~LIF_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    LIF_PluginManager *_pluginManager;
};

#endif // LIF_PLUGINENTRY_H