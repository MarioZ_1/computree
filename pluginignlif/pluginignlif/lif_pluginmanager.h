#ifndef LIF_PLUGINMANAGER_H
#define LIF_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class LIF_PluginManager : public CT_AbstractStepPlugin
{
public:
    LIF_PluginManager();
    ~LIF_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-ign-lif/wiki");}

    QString getPluginRISCitation() const;
protected:

    bool loadGenericsStep();
    bool loadMetrics();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // LIF_PLUGINMANAGER_H
