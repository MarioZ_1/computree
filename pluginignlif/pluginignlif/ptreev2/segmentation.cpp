#include "segmentation.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_global/ct_context.h"
#include "ct_math/delaunay2d/ct_delaunaytriangulation.h"

#include <QFile>
#include <QTextStream>

// TODO
// Ajouter le multi-thread pour les segmPPL
// Ajouter l'attribut de Hauteur pour le critère _score_size

Segmentation::Segmentation(CT_AbstractItemDrawableWithPointCloud* scene, Param &param)
{
    _param = &param;
    const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
    size_t n_pts = pointCloudIndex->size();

    // create points
    _points.reserve(n_pts);

    CT_PointIterator it(pointCloudIndex);
    while (it.hasNext())
    {
        it.next();
        const CT_Point& point = it.currentPoint();
        size_t index = it.currentGlobalIndex();

        Point* pt = new Point(_points.size(), index, point(0), point(1), point(2));
        _points.append(pt);

    }

    std::sort(_points.begin(), _points.end(), Point::orderByDecreasingZ);

    // Create grid
    _grid = CT_Grid3D_Points::createGrid3DFromXYZCoords(NULL, NULL,
                                                        scene->minX() - 1.0, scene->minY() - 1.0, scene->minZ() - 1.0,
                                                        scene->maxX() + 1.0, scene->maxY() + 1.0, scene->maxZ() + 1.0,
                                                        param._resolution, false);

    for (int i = 0 ; i < _points.size() ; i++)
    {
        Point* pt = _points.at(i);
        pt->_localIndex = i;
        _grid->addPoint(i, pt->_coord(0), pt->_coord(1), pt->_coord(2));
    }

}

void Segmentation::mergeIncludedHulls(Forest* forest)
{
    bool mergeDone = true;

    while (mergeDone)
    {
        mergeDone = false;
        for (int i = 0 ; i < forest->_trees.size() && !mergeDone; i++)
        {
            Tree* mainTree = forest->_trees.at(i);
            CT_Polygon2DData* mainConvexHull = mainTree->_convexHull;

            if (mainConvexHull != NULL)
            {
                for (int j = i+1 ; j < forest->_trees.size()  && !mergeDone; j++)
                {
                    Tree* otherTree = forest->_trees.at(j);
                    CT_Polygon2DData* otherConvexHull = otherTree->_convexHull;

                    if (otherConvexHull != NULL)
                    {
                        bool merge = true;

                        const QVector<Eigen::Vector2d*>& vertices = otherConvexHull->getVertices();
                        for (int v = 0 ; v < vertices.size() && merge; v++)
                        {
                            Eigen::Vector2d* vert = vertices.at(v);
                            if (!mainConvexHull->contains((*vert)(0), (*vert)(1)))
                            {
                                merge = false;
                            }
                        }

                        if (!merge)
                        {
                            merge = true;
                            const QVector<Eigen::Vector2d*>& vertices2 = mainConvexHull->getVertices();
                            for (int v = 0 ; v < vertices2.size() && merge; v++)
                            {
                                Eigen::Vector2d* vert = vertices2.at(v);
                                if (!otherConvexHull->contains((*vert)(0), (*vert)(1)))
                                {
                                    merge = false;
                                }
                            }
                        }

                        if (merge && (otherTree->_points[0]->_coord(2) >= (mainTree->_minZ - _param->_distDominated)))
                        {
                            QList<Tree*> list;
                            list.append(mainTree);
                            list.append(otherTree);
                            Tree* mergedTree = Tree::mergeTrees(list, *_param);
                            forest->_trees.replace(i, mergedTree);
                            forest->_trees.removeAt(j);

                            for (int l = 0 ; l < forest->_labels.size() ; l++)
                            {
                                int oldLabel = forest->_labels.at(l);
                                if (oldLabel == j) {forest->_labels[l] = i;}
                                else if (oldLabel > j) {forest->_labels[l]--;}
                            }

                            delete mainTree;
                            delete otherTree;

                            mainTree = mergedTree;
                            mergeDone = true;
                        }
                    }
                }
            }
        }
    }
}

Forest* Segmentation::segmPPL(int k)
{
    double radius = sqrt(pow(_grid->maxX() - _grid->minX(), 2) + pow(_grid->maxY() - _grid->minY(), 2) + pow(_grid->maxZ() - _grid->minZ(), 2));
    Forest* forest = new Forest(_points.size());

    for (int pt3d = 0 ; pt3d < _points.size(); pt3d++)
    {
        Point* point3d = _points.at(pt3d);

        // If the point is NOT already labelled
        if (forest->_labels.at(point3d->_localIndex) == -1)
        {
            // Test inclusion in convex hulls of existing trees
            bool stop = false;
            QMultiMap<double, Tree*> containingHulls;
            QMap<Tree*, int> treesIndices;
            for (int i = 0 ; i < forest->_trees.size() && !stop; i++)
            {
                Tree* tree = forest->_trees.at(i);

                if (tree->_convexHull != NULL && tree->_convexHull->contains(point3d->_coord(0), point3d->_coord(1)))
                {
                    if (point3d->_coord(2) >= (tree->_minZ - _param->_distDominated))
                    {
                        double dist = sqrt(pow(tree->_summit->_coord(0) - point3d->_coord(0), 2) + pow(tree->_summit->_coord(1) - point3d->_coord(1), 2));

                        containingHulls.insert(dist, tree);
                        treesIndices.insert(tree, i);
                    }
                }
            }

            if (containingHulls.size() > 0)
            {
                QMapIterator<double, Tree*> itHull(containingHulls);
                if (itHull.hasNext())
                {
                    itHull.next();
                    Tree* tree = itHull.value();
                    forest->_labels[point3d->_localIndex] = treesIndices.value(tree);
                    tree->addPoint(point3d);
                } else {
                    qDebug() << "There is a problem : containingHulls";
                }
            } else
            {
                // Compute K neighbours
                QList<size_t> indexList;
                _grid->getPointIndicesIncludingKNearestNeighbours(point3d->_coord, k+1, radius, indexList);

                QMultiMap<double, int> sortedIndices;
                for (int i = 0 ; i < indexList.size() ; i++)
                {
                    int localIndex = indexList.at(i);

                    if (localIndex != point3d->_localIndex)
                    {
                        Eigen::Vector3d &pt = _points.at(localIndex)->_coord;
                        double squareDist = pow(pt(0) - point3d->_coord(0), 2) + pow(pt(1) - point3d->_coord(1), 2) + pow(pt(2) - point3d->_coord(2), 2);
                        sortedIndices.insert(squareDist, localIndex);
                    }
                }

                indexList.clear();

                double dh3D = 0.0;
                double dh2D = 0.0;
                int kmax3 = k / 3;
                int cpt = 0;
                QMapIterator<double, int> itM(sortedIndices);
                while (cpt <= k && itM.hasNext())
                {
                    itM.next();
                    indexList.append(itM.value());
                    Point* pt = _points.at(itM.value());

                    dh3D += sqrt(pow(pt->_coord(0) - point3d->_coord(0), 2) + pow(pt->_coord(1) - point3d->_coord(1), 2) + pow(pt->_coord(2) - point3d->_coord(2), 2));

                    if (cpt < kmax3)
                    {
                        dh2D += sqrt(pow(pt->_coord(0) - point3d->_coord(0), 2) + pow(pt->_coord(1) - point3d->_coord(1), 2));
                    }
                    cpt++;
                }

                for (int i = 0 ; i < indexList.size() ; i++)
                {
                    Point* pt1 = _points.at(indexList.at(i));
                    for (int j = i + 1 ; j < indexList.size() ; j++)
                    {
                        Point* pt2 = _points.at(indexList.at(j));

                        dh3D += sqrt(pow(pt1->_coord(0) - pt2->_coord(0), 2) + pow(pt1->_coord(1) - pt2->_coord(1), 2) + pow(pt1->_coord(2) - pt2->_coord(2), 2));

                    }
                }


                if (k > 1)      {dh3D = 2.0*dh3D / ((double)k*(double)(k-1));}
                if (kmax3 > 1)  {dh2D = 3.0*dh2D / ((double)(kmax3 - 1));}

                QList<int> indexListFiltered;
                for (int i = 0 ; i < indexList.size() ; i++)
                {
                    Point* pt = _points.at(indexList.at(i));
//                    double dist3D = sqrt(pow(pt->_coord(0) - point3d->_coord(0), 2) + pow(pt->_coord(1) - point3d->_coord(1), 2) + pow(pt->_coord(2) - point3d->_coord(2), 2));
//                    double dist2D = sqrt(pow(pt->_coord(0) - point3d->_coord(0), 2) + pow(pt->_coord(1) - point3d->_coord(1), 2));

//                    if (dist3D < dh3D && dist2D < dh2D)
//                    {
                        indexListFiltered.append(indexList.at(i));
//                    }
                }



                // Detect labels existing in kept neighbours
                QList<int> labelList;
                for (int i = 0 ; i < indexListFiltered.size() ; i++)
                {
                    int index = indexListFiltered.at(i);

                    int currentLabel = forest->_labels.at(index);
                    if (currentLabel > -1 && !labelList.contains(currentLabel))
                    {
                        labelList.append(currentLabel);
                    }
                }



                // Case 1 : no existing label => create new tree with k neighbours
                if (labelList.size() == 0)
                {
                    Tree* newTree = new Tree(k);
                    forest->_trees.append(newTree);
                    int newLabel = forest->_trees.size() - 1;

                    for (int i = 0 ; i < indexListFiltered.size() ; i++)
                    {
                        int localPtIndex = indexListFiltered.at(i);
                        forest->_labels[localPtIndex] = newLabel;
                        newTree->addPoint(_points.at(localPtIndex));
                    }

                    forest->_labels[point3d->_localIndex] = newLabel;
                    newTree->addPoint(point3d);

                }
                // Case 2 : only one existing label => add the point to the tree
                else if (labelList.size() == 1)
                {
                    int existingLabel = labelList.first();

                    Tree* existingTree = forest->_trees.at(existingLabel);
                    forest->_labels[point3d->_localIndex] = existingLabel;
                    existingTree->addPoint(_points.at(point3d->_localIndex));
                    // Case 3 : more than one exiting label
                } else {

                    int keptLabel = -1;
                    Tree* keptTree = NULL;
                    double minRatio = std::numeric_limits<double>::max();

                    for (int i = 0 ; i < labelList.size() ; i++)
                    {
                        int currentLabel = labelList.at(i);
                        Tree* currentTree = forest->_trees.at(currentLabel);
                        double ratio = std::numeric_limits<double>::max();
                        if (currentTree->_convexHull != NULL)
                        {
                            ratio = Segmentation::addNewPointToHullAndComputeArea(currentTree->_convexHull, point3d->_coord(0), point3d->_coord(1), currentTree->_points.size());
                            if (ratio < minRatio)
                            {
                                minRatio = ratio;
                                keptLabel = currentLabel;
                                keptTree = currentTree;
                            }
                        }
                    }

                    if (keptLabel > -1 && keptTree != NULL)
                    {
                        forest->_labels[point3d->_localIndex] = keptLabel;
                        keptTree->addPoint(_points.at(point3d->_localIndex));
                    } else {
                        qDebug() << "There is a problem : point not classified";
                    }
                }
            }
        }
    }

    // If all the convex hull of a tree is included in the convex hull of another one, merge them (except if deltaH is > threshold)
    mergeIncludedHulls(forest);

    return forest;
}


Forest* Segmentation::segmFinale(const QList<Tree*> &keptTrees)
{
    double radius = sqrt(pow(_grid->maxX() - _grid->minX(), 2) + pow(_grid->maxY() - _grid->minY(), 2) + pow(_grid->maxZ() - _grid->minZ(), 2));
    Forest* forest = new Forest(_points.size());

    // Initialise trees, exepts points which are in 2 or more trees
    for (int tr = 0 ; tr < keptTrees.size() ; tr++)
    {
        Tree* tree = keptTrees.at(tr);

        Tree* newTree = new Tree(tree->_k);
        forest->_trees.append(newTree);

        for (int i = 0 ; i < tree->_points.size() ; i++)
        {
            Point* pt = tree->_points.at(i);

            int oldLabel = forest->_labels.at(pt->_localIndex);
            if (oldLabel > 0)
            {
                forest->_labels[pt->_localIndex] = -2;
            } else if (oldLabel == -1) {
                forest->_labels[pt->_localIndex] = tr;
            }
        }
    }

    for (int ptI = 0 ; ptI < forest->_labels.size() ; ptI++)
    {
        int label = forest->_labels.at(ptI);

        if (label >= 0)
        {
            Tree* tree = forest->_trees.at(label);

            Point* pt = _points.at(ptI);
            tree->addPoint(pt);
        }
    }


    // Attribute remaining points
    for (int pt3d = 0 ; pt3d < _points.size(); pt3d++)
    {
        Point* point3d = _points.at(pt3d);

        // If the point is NOT already labelled
        if (forest->_labels.at(point3d->_localIndex) < 0)
        {
            // Test inclusion in convex hulls of existing trees
            bool stop = false;
            for (int i = 0 ; i < forest->_trees.size() && !stop; i++)
            {
                Tree* tree = forest->_trees.at(i);

                if (tree->_convexHull != NULL && tree->_convexHull->contains(point3d->_coord(0), point3d->_coord(1)))
                {
                    if (point3d->_coord(2) >= (tree->_minZ - _param->_distDominated))
                    {
                        forest->_labels[point3d->_localIndex] = i;
                        tree->addPoint(point3d);
                        stop = true;
                    }
                }
            }

            if (!stop)
            {
                QList<int> labelList;
                QList<size_t> indexList;

                int cptScale = _param->_scales.size() - 1;
                int currentK = 0;

                while (cptScale >=0 && labelList.size() == 0)
                {
                    indexList.clear();

                    currentK = _param->_scales.at(cptScale);

                    // Compute K neighbours
                    _grid->getPointIndicesIncludingKNearestNeighbours(point3d->_coord, currentK + 1, radius, indexList);

                    QMultiMap<double, int> sortedIndices;
                    for (int i = 0 ; i < indexList.size() ; i++)
                    {
                        int localIndex = indexList.at(i);

                        if (localIndex != point3d->_localIndex)
                        {
                            Eigen::Vector3d &pt = _points.at(localIndex)->_coord;
                            double dist = pow(pt(0) - point3d->_coord(0), 2) + pow(pt(1) - point3d->_coord(1), 2) + pow(pt(2) - point3d->_coord(2), 2);
                            sortedIndices.insert(dist, localIndex);
                        }
                    }

                    indexList.clear();

                    int cpt = 0;
                    QMapIterator<double, int> itM(sortedIndices);
                    while (cpt <= currentK && itM.hasNext())
                    {
                        itM.next();
                        indexList.append(itM.value());

                        int currentLabel = forest->_labels.at(itM.value());
                        if (currentLabel > -1 && !labelList.contains(currentLabel))
                        {
                            labelList.append(currentLabel);
                        }

                        cpt++;
                    }
                    --cptScale;
                }


                // Case 1 : no existing label => create new tree with k neighbours
                if (labelList.size() == 0)
                {
                    qDebug() << "There is a problem : labelList.size() == 0";
                }

                // Case 2 : only one existing label => add the point to the tree
                else if (labelList.size() == 1)
                {
                    int existingLabel = labelList.first();

                    Tree* existingTree = forest->_trees.at(existingLabel);
                    forest->_labels[point3d->_localIndex] = existingLabel;
                    existingTree->addPoint(_points.at(point3d->_localIndex));
                    // Case 3 : more than one exiting label
                } else {

                    int keptLabel = -1;
                    Tree* keptTree = NULL;
                    double minRatio = std::numeric_limits<double>::max();

                    for (int i = 0 ; i < labelList.size() ; i++)
                    {
                        int currentLabel = labelList.at(i);
                        Tree* currentTree = forest->_trees.at(currentLabel);

                        double ratio = std::numeric_limits<double>::max();
                        if (currentTree->_convexHull != NULL)
                        {
                            ratio = Segmentation::addNewPointToHullAndComputeArea(currentTree->_convexHull,  point3d->_coord(0), point3d->_coord(1), currentTree->_points.size());
                            if (ratio < minRatio)
                            {
                                minRatio = ratio;
                                keptLabel = currentLabel;
                                keptTree = currentTree;
                            }
                        }
                    }

                    if (keptLabel > -1 && keptTree != NULL)
                    {
                        forest->_labels[point3d->_localIndex] = keptLabel;
                        keptTree->addPoint(_points.at(point3d->_localIndex));
                    } else {
                        qDebug() << "There is a problem : point not classified";
                    }
                }
            }
        }
    }

    // If all the convex hull of a tree is included in the convex hull of another one, merge them (except if deltaH is > threshold)
    mergeIncludedHulls(forest);

    for (int tr = 0 ; tr < forest->_trees.size() ; tr++)
    {
        Tree* tree = forest->_trees.at(tr);
        tree->computeScores(*_param);
    }

    return forest;
}


Forest *Segmentation::segmMultiScale()
{       
    QList<Forest*> forests;

    // Create all segmentation forests
    for (int i = 0 ; i < _param->_scales.size() ; i++)
    {
        int scale = _param->_scales.at(i);
        Forest* forest = segmPPL(scale);

        for (int j = 0 ; j < forest->_trees.size() ; j++)
        {
            Tree* tree = forest->_trees.at(j);
            tree->computeScores(*_param);
        }

        forests.append(forest);
        qDebug() << "Segmentation k = " << scale;
    }

    if (forests.size() == 0) {return NULL;}
    else if (forests.size() == 1) {return forests.first();}

    // Create hierachical tree affiliation between forests
    for (int i = 1 ; i < forests.size() ; i++)
    {
        Forest* parentForest = forests.at(i - 1);
        Forest* childForest = forests.at(i);

        for (int ctr = 0 ; ctr < childForest->_trees.size() ; ctr++)
        {
            Tree* cTree = childForest->_trees.at(ctr);
            int cSummitLocalIndex = cTree->_summit->_localIndex;

            int parentSummitId = parentForest->_labels.at(cSummitLocalIndex);
            parentForest->_trees.at(parentSummitId)->_children.append(cTree);
        }
    }


    QList<Tree*> validatedTrees;
    QList<Tree*> toDelete;
    QList<Tree*> toDeleteAtEnd;

    for (int tr = 0 ; tr < forests.first()->_trees.size() ; tr++)
    {
        // Create all combinaisons for all levels
        Tree* tree = forests.first()->_trees.at(tr);

        QList<Tree*> notValidatedTrees;

        notValidatedTrees.append(tree);

        while (!notValidatedTrees.isEmpty())
        {
            QList<Tree*> futureNotValidatedTrees;

            for (int i = 0 ; i < notValidatedTrees.size() ; i++)
            {

                Tree* parentTree = notValidatedTrees.at(i);
                QList<Tree*> &children = parentTree->_children;
                QList<QList<Tree*> > combinations;
                if (children.size() > 0) {combinations.append(children);}

                bool ok = (combinations.size() > 0) && (combinations.last().size() > 1);
                while (ok)
                {
                    ok = createCombination(combinations.last(), combinations, toDelete);
                }

                int bestCombination = -1;
                double bestCombinationScore = parentTree->_score;
                for (int j = 0 ; j < combinations.size() ; j++)
                {

                    const QList<Tree*> &combination = combinations.at(j);

                    double score = 0;
                    for (int k = 0 ; k < combination.size() ; k++)
                    {
                        Tree* childTree = combination.at(k);
                        score += childTree->_score;

//                        if (k == 0 || score > childTree->_score)
//                        {
//                            score = childTree->_score;
//                        }
                    }

                    if (combination.size() > 0) {score /= combination.size();}

                    if (score > bestCombinationScore)
                    {
                        bestCombinationScore = score;
                        bestCombination = j;
                    }
                }

                if (bestCombination == -1)
                {
                    if (children.size() > 1)
                    {
                        Tree* newTree = Tree::mergeTrees(children, *_param);
                        toDelete.append(newTree);
                        futureNotValidatedTrees.append(newTree);
                    } else {
                        validatedTrees.append(parentTree);
                        parentTree->_validated = true;
                    }
                } else {
                    futureNotValidatedTrees.append(combinations.at(bestCombination));
                }

            }
            notValidatedTrees.clear();
            notValidatedTrees.append(futureNotValidatedTrees);
        }

        for (int i = 0 ; i < toDelete.size() ; i++)
        {
            Tree* tree = toDelete.at(i);
            if (tree->_validated)
            {
                toDeleteAtEnd.append(tree);
            } else {
                delete tree;
            }
        }
        toDelete.clear();
    }

    Forest* returnedForest = segmFinale(validatedTrees);
    qDeleteAll(toDeleteAtEnd);
    toDeleteAtEnd.clear();

    return returnedForest;
}

bool Segmentation::createCombination(QList<Tree*> &parentCombination, QList<QList<Tree*> > &combinations, QList<Tree*> &toDelete)
{
    bool ok = true;

    CT_DelaunayTriangulation* triangulation = new CT_DelaunayTriangulation();

    QMap<CT_DelaunayVertex*, Tree*> corresp;


    double minx = std::numeric_limits<double>::max();
    double miny = std::numeric_limits<double>::max();
    double maxx = -std::numeric_limits<double>::max();
    double maxy = -std::numeric_limits<double>::max();

    for (int i = 0 ; i < parentCombination.size() ; i++)
    {
        Tree* tree = parentCombination.at(i);

        if (tree->_summit->_coord(0) < minx) {minx = tree->_summit->_coord(0);}
        if (tree->_summit->_coord(1) < miny) {miny = tree->_summit->_coord(1);}
        if (tree->_summit->_coord(0) > maxx) {maxx = tree->_summit->_coord(0);}
        if (tree->_summit->_coord(1) > maxy) {maxy = tree->_summit->_coord(1);}
    }
    triangulation->init(minx - 1.0, miny - 1.0, maxx + 1.0, maxy + 1.0);


    double worstScore = std::numeric_limits<double>::max();
    CT_DelaunayVertex* worstTreeVertice = NULL;
    for (int i = 0 ; i < parentCombination.size() ; i++)
    {
        Tree* tree = parentCombination.at(i);
        CT_DelaunayVertex* vertice = triangulation->addVertex(new Eigen::Vector3d(tree->_summit->_coord(0), tree->_summit->_coord(1), 0), true);
        vertice->initNeighbors();
        corresp.insert(vertice, tree);

        if (tree->_score < worstScore)
        {
            worstScore = tree->_score;
            worstTreeVertice = vertice;
        }
    }

    if (worstTreeVertice != NULL)
    {
        triangulation->doInsertion();
        triangulation->getVerticesNeighbors();

        double bestScoreRatio = -std::numeric_limits<double>::max();
        Tree* bestMergedTree = NULL;
        Tree* worstTree = corresp.value(worstTreeVertice);
        Tree* neighbTree = NULL;

        QList<CT_DelaunayVertex*> &neighbours = worstTreeVertice->getNeighbors();
        for (int i = 0 ; i < neighbours.size() ; i++)
        {
            QList<Tree*> toMerge;
            Tree* candidateTree = corresp.value(neighbours[i]);

            if (candidateTree != NULL)
            {
                toMerge.append(worstTree);
                toMerge.append(candidateTree);

                Tree* mergedTree = Tree::mergeTrees(toMerge, *_param);

                double candidateScoreRatio = mergedTree->_score / candidateTree->_score;
                if (candidateScoreRatio > bestScoreRatio)
                {
                    bestScoreRatio = candidateScoreRatio;
                    if (bestMergedTree != NULL) {delete bestMergedTree;}
                    bestMergedTree = mergedTree;
                    neighbTree = candidateTree;
                } else {
                    delete mergedTree;
                }
            }
        }

        if (bestMergedTree != NULL)
        {
            QList<Tree*> combination;
            combination.append(bestMergedTree);
            toDelete.append(bestMergedTree);
            for (int i = 0 ; i < parentCombination.size() ; i++)
            {
                Tree* tree = parentCombination.at(i);
                if (tree != worstTree && tree != neighbTree)
                {
                    combination.append(tree);
                }
            }
            combinations.append(combination);
        } else {ok = false;}
    } else {ok = false;}

    delete triangulation;

    return ok;
}


bool Segmentation::isCombinationValid(const QList<Tree*> &combination)
{
    bool valid = true;
    for (int i = 0 ; valid && i < combination.size() ; i++)
    {
        Tree* tree1 = combination.at(i);
        for (int j = 0 ; valid && j < combination.size() ; j++)
        {
            if (i != j)
            {
                Tree* tree2 = combination.at(j);

                if (((tree1->_minZ - tree2->_summit->_coord(2)) < _param->_distDominated) &&  (tree1->_convexHull->contains(tree2->_summit->_coord(0), tree2->_summit->_coord(1))))
                {
                    valid = false;
                }
            }
        }
    }
    return valid;
}

void Segmentation::createAllDualCombinations(const QList<Tree*> &in, QList<QList<Tree*> > &out, QList<Tree*> &toDelete)
{
    for (int i = 0 ; i < in.size() ; i++)
    {
        for (int j = i + 1 ; j < in.size() ; j++)
        {
            QList<Tree*> combinaison;

            QList<Tree*> pair;
            pair.append(in.at(i));
            pair.append(in.at(j));

            Tree* newTree = Tree::mergeTrees(pair, *_param);
            combinaison.append(newTree);
            toDelete.append(newTree);

            for (int k = 0 ; k < in.size() ; k++)
            {
                if (k != i && k != j)
                {
                    combinaison.append(in.at(k));
                }
            }
            out.append(combinaison);
        }
    }
}

void Segmentation::createAllNCombinations(int n, const QList<Tree*> &in, QList<QList<Tree*> > &out, QList<Tree*> &toDelete)
{
    QVector<int> counter(n);
    counter.fill(0);

    bool oneMore = updateCounter(counter, in.size() - 1);
    while (oneMore)
    {
        QList<Tree*> combinaison;
        QList<Tree*> toMerge;
        for(int counterIndex = 0 ; counterIndex < counter.size() ; counterIndex++)
        {
            toMerge.append(in.at(counter[counterIndex]));
        }

        Tree* newTree = Tree::mergeTrees(toMerge, *_param);
        combinaison.append(newTree);
        toDelete.append(newTree);

        for (int inIndex = 0 ; inIndex < in.size() ; inIndex++)
        {
            bool keep = true;

            for (int counterIndex = 0 ; keep && counterIndex < counter.size() ; counterIndex++)
            {
                if (counter[counterIndex] == inIndex) {keep = false;}
            }

            if (keep)
            {
                combinaison.append(in.at(inIndex));
            }
        }

        out.append(combinaison);

        oneMore = incrementCounter(counter, in.size() - 1);
    }

}


bool Segmentation::incrementCounter(QVector<int> &counter, int maxIndex)
{
    bool oneMore = true;

    while (oneMore)
    {
        int cpt = counter.size() - 1;

        int maxVal = maxIndex - (counter.size() - cpt - 1);
        while (cpt >=0 && counter[cpt] >= maxVal) {counter[cpt] = 0; cpt--;}

        if (cpt < 0) {return false;}

        counter[cpt]++;

        oneMore = !updateCounter(counter, maxIndex);
    }

    return true;
}

bool Segmentation::updateCounter(QVector<int> &counter, int maxIndex)
{
    for (int i = 1 ; i < counter.size() ; i++)
    {
        if (counter[i] <= counter[i-1])
        {
            for (int j = i ; j < counter.size() ; j++)
            {
                counter[j] = counter[j-1] + 1;
                if (counter[j] > maxIndex) {return false;}
            }
            return true;
        }
    }

    return true;
}







void Segmentation::getSegmentedScenes(QList<CT_Scene *> &segmentedScenes,
                                      QList<CT_Polygon2D *> &segmentedHulls,
                                      CT_AbstractResult* result,
                                      QString sceneModelName,
                                      QString chModelName,
                                      QString score_size_ModelName,
                                      QString score_regularity_ModelName,
                                      QString score_solidity_ModelName,
                                      QString score_summitOffset_ModelName,
                                      QString score_ModelName,
                                      QString k_ModelName)
{
    Forest* forest = segmMultiScale();

    for (int i  = 0 ; i < forest->_trees.size() ; i++)
    {
        Tree* tree = forest->_trees.at(i);

        CT_PointCloudIndexVector* indexVector = new CT_PointCloudIndexVector();
        indexVector->setSortType(CT_PointCloudIndexVector::NotSorted);

        for (int j = 0 ; j < tree->_points.size() ; j++)
        {
            indexVector->addIndex(tree->_points.at(j)->_globalIndex);
        }

        if (indexVector->size() > 0)
        {
            indexVector->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

            CT_Scene *scene = new CT_Scene(sceneModelName, result, PS_REPOSITORY->registerPointCloudIndex(indexVector));
            scene->updateBoundingBox();

            scene->addItemAttribute(new CT_StdItemAttributeT<double>(score_size_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_score_size));
            scene->addItemAttribute(new CT_StdItemAttributeT<double>(score_regularity_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_score_regularity));
            scene->addItemAttribute(new CT_StdItemAttributeT<double>(score_solidity_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_score_solidity));
            scene->addItemAttribute(new CT_StdItemAttributeT<double>(score_summitOffset_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_score_summitOffset));
            scene->addItemAttribute(new CT_StdItemAttributeT<double>(score_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_score));
            scene->addItemAttribute(new CT_StdItemAttributeT<int>(k_ModelName, CT_AbstractCategory::DATA_VALUE, result, tree->_k));

            segmentedScenes.append(scene);

            CT_Polygon2D *convexHull = NULL;
            if (tree->_convexHull != NULL)
            {
                CT_Polygon2DData* chdata = new CT_Polygon2DData(tree->_convexHull->getVertices());
                convexHull = new CT_Polygon2D(chModelName, result, chdata);
            }
            segmentedHulls.append(convexHull);

        } else {
            delete indexVector;
        }
    }
}

double Segmentation::addNewPointToHullAndComputeArea(CT_Polygon2DData* hull, double x, double y, size_t nPts)
{
    if (hull == NULL) {return 0.0;}

    const QVector<Eigen::Vector2d*>& vertices = hull->getVertices();

    Eigen::Vector2d* newPoint = new Eigen::Vector2d(x, y);

    QList<Eigen::Vector2d*> points2;

    for (int i = 0 ; i < vertices.size() ; i++)
    {
        points2.append(vertices.at(i));
    }
    points2.push_back(newPoint);

    CT_Polygon2DData::orderPointsByXY(points2);
    CT_Polygon2DData* newBound = CT_Polygon2DData::createConvexHull(points2);

    double value = nPts * std::abs(newBound->getArea() - hull->getArea()) / hull->getArea();

    delete newBound;
    delete newPoint;

    return value;
}



