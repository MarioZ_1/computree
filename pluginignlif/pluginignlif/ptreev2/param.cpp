#include "param.h"

Param::Param(const QList<int> &scale, double distDominated, double resSolidityGrid, double resolution)
{
    _scales.append(scale);
    _distDominated = distDominated;
    _resSolidityGrid = resSolidityGrid;
    _resolution = resolution;
}
