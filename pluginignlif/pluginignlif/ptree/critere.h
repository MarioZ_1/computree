#ifndef CRITERE_H
#define CRITERE_H

#include <QtGui>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits>

#include <algorithm>
#include <vector>
#include <cfloat>
#include <fstream>

#include <eigen/Eigen/Core>
#include "tools.h"
#include "param.h"
#include "convexHull.h"


double getScore(std::vector<Eigen::Vector3d> &branche, std::vector<Eigen::Vector3d> &bound, int k, Param &p);
double getScoreT(std::vector<Eigen::Vector3d> &branche,std::vector<Eigen::Vector3d> &bound, int k, Param &p);
double getScoreCH(std::vector<Eigen::Vector3d> &branche, std::vector<Eigen::Vector3d> &bound);
double getScoreEL(std::vector<Eigen::Vector3d> &branche);
double getScoreCG(std::vector<Eigen::Vector3d> &branche, std::vector<Eigen::Vector3d> &bound);



#endif // CRITERE_H
