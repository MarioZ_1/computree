/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/
/****************************************************************************
  History
$Log: sampleplugins.h,v $
Revision 1.2  2006/11/29 00:59:21  cignoni
Cleaned plugins interface; changed useless help class into a plain string

Revision 1.1  2006/09/25 09:24:39  e_cerisoli
add sampleplugins

****************************************************************************/

#ifndef PTREESPLUGIN_H
#define PTREESPLUGIN_H

#include <QObject>
#include <eigen/Eigen/Core>

class QScriptEngine;

template <class S>
class MyPointDistanceFunctor
{
public:
    template <class VERTEXTYPE>
    inline bool operator () (const VERTEXTYPE &p0, const Eigen::Vector3d & p, double & minDist, Eigen::Vector3d & q)
    {
        // Retour de la distance p-p0
        double d = (double) sqrt(pow((p0).P()[0] - p(0), 2) + pow((p0).P()[1] - p(1), 2));
        //double d = (double) Distance((p0).P(), p) ;
        // Atteinte en p
        q = p;
        // Test de d < minDist
        bool ret ;
        if (d < minDist)
            ret = 1 ;
        else
            ret = 0 ;
        // Retour de la distance minimale dans d
        minDist = d ;
        return (ret);
    }
};

class PTREESPlugin : public QObject, public MeshFilterInterface
{
    Q_OBJECT

public:
    enum { FP_PTREES  } ;

    PTREESPlugin();

    virtual QString pluginName(void) const { return "PTREESPlugin"; }

    QString filterName(FilterIDType filter) const;
    QString filterInfo(FilterIDType filter) const;
    void initParameterSet(QAction *,MeshModel &/*m*/, RichParameterSet & /*parent*/);
    bool applyFilter(QAction *filter, MeshDocument &md, RichParameterSet &) ;
    FilterClass getClass(QAction *a);
    QString filterScriptFunctionName(FilterIDType filterID);


};


#endif
