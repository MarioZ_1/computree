#include "convexHull.h"

#include <QDebug>

using namespace std;

ConvexHull::ConvexHull()
{
}

double ConvexHull::det(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r)
{
    return(q[0]*r[1] + p[0]*q[1] + r[0]*p[1]) - (q[0]*p[1] + r[0]*q[1] + p[0]*r[1]);
}


bool ConvexHull::isRightTurn(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r)
{
    bool isRigth = false;
    if (det(p,q,r)<0){isRigth = true;}
    return isRigth;
}

vector<Eigen::Vector3d> ConvexHull::buildUpperConvexHull( vector<Eigen::Vector3d> branche)
{

    vector<Eigen::Vector3d> upperBound;
    sort(branche.begin(), branche.end(), compare3fby1);
    upperBound.push_back(branche[0]);
    upperBound.push_back(branche[1]);

    int i,l;
    int length = branche.size();
    bool isRight;

    for (i=2;i<length;i++){
        upperBound.push_back(branche[i]);
        l=upperBound.size();
        isRight=isRightTurn(upperBound[l-3],upperBound[l-2],upperBound[l-1]);
        while((l > 2) && (!isRight) ){
            upperBound[l-2]=upperBound[l-1];
            upperBound.pop_back();
            l=upperBound.size();
            isRight=isRightTurn(upperBound[l-3],upperBound[l-2],upperBound[l-1]);
        }
    }
    return upperBound;

}

vector<Eigen::Vector3d> ConvexHull::buildLowerConvexHull( vector<Eigen::Vector3d> branche)
{

    std::vector<Eigen::Vector3d> lowerBound;
    int length = branche.size();
    std::sort(branche.begin(), branche.end(), compare3fby1);
    lowerBound.push_back(branche[length-1]);
    lowerBound.push_back(branche[length-2]);

    int i,l;
    bool isRight;
    for (i=length-3;i>=0;i--){
        lowerBound.push_back(branche[i]);
        l=lowerBound.size();
        isRight=isRightTurn(lowerBound[l-3],lowerBound[l-2],lowerBound[l-1]);
        while((l > 2) && (!isRight) ){
            lowerBound[l-2]=lowerBound[l-1];
            lowerBound.pop_back();
            l=lowerBound.size();
            isRight=isRightTurn(lowerBound[l-3],lowerBound[l-2],lowerBound[l-1]);
        }
    }
    return lowerBound;
}

vector<Eigen::Vector3d> ConvexHull::buildConvexHull( vector<Eigen::Vector3d> branche)
{
    std::vector<Eigen::Vector3d> upper = buildUpperConvexHull(branche);
    std::vector<Eigen::Vector3d> lower = buildLowerConvexHull(branche);
    std::vector<Eigen::Vector3d>  bound;
    bound = upper;
    unsigned int i;
    for (i=1;i<lower.size();i++){
        bound.push_back(lower[i]);
    }
    return bound;
}


bool ConvexHull::pointInConvexHull(Eigen::Vector3d p,vector<Eigen::Vector3d> branche)
{
    std::vector<Eigen::Vector3d> upperBound, lowerBound, bound;
    upperBound = buildUpperConvexHull(branche);
    lowerBound = buildLowerConvexHull(branche);
    double x,y;
    double p1x,p2x,p1y,p2y,xinters;
    bound = upperBound;
    unsigned int i;
    for (i=0;i<lowerBound.size();i++){
        bound.push_back(lowerBound[i]);

    }
    int length = bound.size();
    bool inside = false;
    x= p[0];
    y= p[1];
    p1x= bound[0][0];
    p1y= bound[0][1];

    int j;
    for (j=1;j<length+1;j++){
        p2x = bound[j % length][0];
        p2y = bound[j % length][1];
        if (y>std::min(p1y,p2y)){
            if(y<=std::max(p1y,p2y)){

                if(x<=std::max(p1x,p2x)){
                    if (p1y!=p2y){
                        xinters=(y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x;
                        if ((p1x==p2x) || (x<xinters))
                        {
                            inside= !inside;
                        }
                    }
                }
            }
        }
        p1x=p2x;
        p1y=p2y;
    }
    return inside;
}


void ConvexHull::printConvexHull(vector<Eigen::Vector3d> branche)
{
    std::vector<Eigen::Vector3d> upper, lower;
    upper = ConvexHull::buildUpperConvexHull(branche);
    lower = ConvexHull::buildLowerConvexHull(branche);
    unsigned int i;
    for (i=0;i<upper.size();i++)
    {
        qDebug() << "- ( " << upper[i][0] << " , " <<upper[i][1] << " , " << upper[i][2] << " ) ";
    }
    for (i=0;i<lower.size();i++)
    {
        qDebug() << "- ( " << lower[i][0] << " , " << lower[i][1] << " , " << lower[i][2] << " ) ";
    }
}


double ConvexHull::computeArea(vector<Eigen::Vector3d> bound)
{

    double area = 0;

    unsigned int i;
    double x1,x2,y1,y2;
    for (i=0;i<bound.size()-1;i++){
        x1 = bound[i][0];
        x2 = bound[i+1][0];
        y1 = bound[i][1];
        y2 = bound[i+1][1];
        area = area + 0.5*(x1*y2 - y1*x2);
    }
    if (area < 0){area = -area;}

    return area;
}


double ConvexHull::addNewPointAndComputeArea(Eigen::Vector3d p, vector<Eigen::Vector3d > branche)
{
    std::vector<Eigen::Vector3d> oldBound = ConvexHull::buildConvexHull(branche);
    double oldArea = ConvexHull::computeArea(oldBound);
    branche.push_back(p);
    std::vector<Eigen::Vector3d> newBound = ConvexHull::buildConvexHull(branche);
    double newArea = ConvexHull::computeArea(newBound);


    return newArea-oldArea;
}


double ConvexHull::computeDistMax( vector<Eigen::Vector3d> bound, Eigen::Vector3d apex)
{
    unsigned int i;
    double d, distMax = 0;
    for(i=0;i<bound.size(); i++){
        d = sqrt(pow(apex(0) - bound[i](0), 2) + pow(apex(1) - bound[i](1), 2));
        if(d>distMax){
            distMax=d;
        }
    }
    return distMax;
}



