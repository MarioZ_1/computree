#ifndef POINT_H
#define POINT_H

#include <eigen/Eigen/Core>
class Point
{
public:
    Point();
    Point(int indice);
    Point(Eigen::Vector3d coord,double height,int indice);
    const Eigen::Vector3d &  getcoord() const;
    void  setcoord( Eigen::Vector3d coord);

    int  getlab() ;
    void setlab(int label);

    int getIndice();
    void setIndice(int ind);
    double getHeight();
    void setHeight(double h);

protected:
    Eigen::Vector3d  _3dcoord;
    int _label;
    int _indice;
    double _height;

};

#endif // POINT_H
