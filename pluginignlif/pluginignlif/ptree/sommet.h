#ifndef SOMMET_H
#define SOMMET_H

#include "point.h"
class Sommet : public Point
{
public:
    Sommet();

    double getscoresommet() const;
    void setscoresommet(double &score);

protected:

double _score;

};

#endif // SOMMET_H
