#include "lif_stepptreesegmentation.h"

#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_polygon2d.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_resultgroupiterator.h"

#include "ct_itemdrawable/ct_grid3d_points.h"

#include "ptreev2/segmentation.h"
#include "ptreev2/param.h"

#include <QMessageBox>
#include <limits>

// Alias for indexing models
#define DEFin_resScene "resScene"
#define DEFin_grp "grp"
#define DEFin_scene "scene"

// Constructor : initialization of parameters
LIF_StepPTreeSegmentation::LIF_StepPTreeSegmentation(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

    _scales = "150,100,80,60,30,15,5";
    _distDominated = 5.0;
    _resSolidityGrid = 0.50;
    _resGrid = 0.5;
}

LIF_StepPTreeSegmentation::~LIF_StepPTreeSegmentation()
{
}

// Step description (tooltip of contextual menu)
QString LIF_StepPTreeSegmentation::getStepDescription() const
{
    return tr("PTree Segmentation");
}

// Step detailled description
QString LIF_StepPTreeSegmentation::getStepDetailledDescription() const
{
    return tr("TO DO");
}

// Step copy method
CT_VirtualAbstractStep* LIF_StepPTreeSegmentation::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LIF_StepPTreeSegmentation(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LIF_StepPTreeSegmentation::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_resScene = createNewInResultModelForCopy(DEFin_resScene, tr("Scène à découper"));
    resIn_resScene->setZeroOrMoreRootGroup();
    resIn_resScene->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_resScene->addItemModel(DEFin_grp, DEFin_scene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène à segmenter"));
}

// Creation and affiliation of OUT models
void LIF_StepPTreeSegmentation::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_resScene);

    if(res != NULL)
    {
        res->addGroupModel(DEFin_grp, _outGroupModelName);
        res->addItemModel(_outGroupModelName, _outSceneModelName, new CT_Scene(), tr("Scène segmentée"));
        res->addItemModel(_outGroupModelName, _outCHModelName, new CT_Polygon2D(), tr("Convex Hull"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_score_size_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score Size"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_score_regularity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score Regularity"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_score_solidity_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score Solidity"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_score_summitOffset_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score SummitOffset"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_score_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Score"));
        res->addItemAttributeModel(_outSceneModelName, _outScene_k_ModelName, new CT_StdItemAttributeT<int>(CT_AbstractCategory::DATA_VALUE), tr("k"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void LIF_StepPTreeSegmentation::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *dialog = newStandardPostConfigurationDialog();

    dialog->addString(tr("Scales"), "", _scales, tr("List of neighbours numbers, separated by commas"));
    dialog->addDouble(tr("Distance to dominated trees"), "m", 0, 999, 4, _distDominated, 1, tr("Distance to dominated trees"));
    dialog->addDouble(tr("Solidity grid resolution"), "", 0.01, 999, 2, _resSolidityGrid, 1, tr("Resolution of the grid for solodity computation"));
    dialog->addDouble(tr("Optimization Resolution"), "", 0.1, 99999, 2, _resGrid, 1, tr("Resolution of the grid"));
}

void LIF_StepPTreeSegmentation::compute()
{
    QStringList scales = _scales.split(",", QString::SkipEmptyParts);
    QList<int> orderedScales;
    for (int i = 0 ; i < scales.size() ; i++)
    {
        QString scale = scales.at(i);
        bool ok;
        int sc = scale.toInt(&ok);
        if (ok && sc > 0 && !orderedScales.contains(sc))
        {
            orderedScales.append(sc);
        }
    }
    std::sort(orderedScales.begin(), orderedScales.end(), LIF_StepPTreeSegmentation::sortInReverseOrder);


    Param param(orderedScales, _distDominated, _resSolidityGrid, _resGrid);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);


    CT_ResultGroupIterator itGrp(res, this, DEFin_grp);
    while (itGrp.hasNext())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itGrp.next();
        CT_AbstractItemDrawableWithPointCloud* inScene = (CT_AbstractItemDrawableWithPointCloud*) grp->firstItemByINModelName(this, DEFin_scene);

        Segmentation segmentation(inScene, param);

        QList<CT_Scene*> segmentedScenes;
        QList<CT_Polygon2D*> segmentedHulls;
        segmentation.getSegmentedScenes(segmentedScenes,
                                        segmentedHulls,
                                        res,
                                        _outSceneModelName.completeName(),
                                        _outCHModelName.completeName(),
                                        _outScene_score_size_ModelName.completeName(),
                                        _outScene_score_regularity_ModelName.completeName(),
                                        _outScene_score_solidity_ModelName.completeName(),
                                        _outScene_score_summitOffset_ModelName.completeName(),
                                        _outScene_score_ModelName.completeName(),
                                        _outScene_k_ModelName.completeName());

        for (int i = 0 ; i < segmentedScenes.size() ; i++)
        {
            CT_Scene* scene = segmentedScenes.at(i);
            CT_Polygon2D* hull = segmentedHulls.at(i);

            if (scene != NULL)
            {
                CT_StandardItemGroup* scGrp = new CT_StandardItemGroup(_outGroupModelName.completeName(), res);
                grp->addGroup(scGrp);
                scGrp->addItemDrawable(scene);

                if (hull != NULL)
                {
                    scGrp->addItemDrawable(hull);
                }
            }
        }
    }
}

