#ifndef LIF_STEPPTREESEGMENTATION_H
#define LIF_STEPPTREESEGMENTATION_H

#include "ct_step/abstract/ct_abstractstep.h"

class LIF_StepPTreeSegmentation: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    LIF_StepPTreeSegmentation();

    ~LIF_StepPTreeSegmentation();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    QString _scales;
    double _distDominated;
    double _resSolidityGrid;
    double _resGrid;




    static bool sortInReverseOrder(int a, int b)
    {
        return a > b;
    }

};

#endif // LIF_STEPPTREESEGMENTATION_H
