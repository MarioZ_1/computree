CT_PREFIX = ../../computreev6

COMPUTREE += ctlibio

MUST_USE_OPENCV = 1

include($${CT_PREFIX}/plugin_shared.pri)


QT += concurrent

TARGET = plug_ignlif

HEADERS += \
    $$CT_LIB_PREFIX/ctlibplugin/pluginentryinterface.h \
    lif_pluginentry.h \
#    step/lif_stepdtm.h \
#    step/lif_steppitfilling.h \
    step/lif_steppitfilling02.h \
#    step/lif_stepoutliersremoving.h \
#    step/lif_stepptreesegmentation.h \
#    ptreev2/point.h \
#    ptreev2/forest.h \
#    ptreev2/tree.h \
#    ptreev2/convexhull.h \
#    ptreev2/param.h \
#    ptreev2/segmentation.h \
#    metric/lif_metricvolume.h \
    lif_pluginmanager.h
#    ptree/arbre.h \
#    ptree/convexHull.h \
#    ptree/critere.h \
#    ptree/filter_segDyn.h \
#    ptree/foret.h \
#    ptree/param.h \
#    ptree/point.h \
#    ptree/resfusion.h \
#    ptree/segmentation.h \
#    ptree/sommet.h \
#    ptree/tools.h \

SOURCES += lif_pluginentry.cpp \
#    step/lif_stepdtm.cpp \
#    step/lif_steppitfilling.cpp \
    step/lif_steppitfilling02.cpp \
#    step/lif_stepoutliersremoving.cpp \
#    step/lif_stepptreesegmentation.cpp \
#    ptreev2/point.cpp \
#    ptreev2/forest.cpp \
#    ptreev2/tree.cpp \
#    ptreev2/convexhull.cpp \
#    ptreev2/param.cpp \
#    ptreev2/segmentation.cpp \
#    metric/lif_metricvolume.cpp \
    lif_pluginmanager.cpp
#    ptree/arbre.cpp \
#    ptree/critere.cpp \
#    ptree/filter_segDyn.cpp \
#    ptree/foret.cpp \
#    ptree/param.cpp \
#    ptree/point.cpp \
#    ptree/resfusion.cpp \
#    ptree/segmentation.cpp \
#    ptree/sommet.cpp \
#    ptree/tools.cpp \
#    ptree/convexHull.cpp \


TRANSLATIONS += languages/pluginignlif_en.ts \
                languages/pluginignlif_fr.ts
