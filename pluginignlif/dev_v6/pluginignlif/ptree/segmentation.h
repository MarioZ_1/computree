#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include <QtGui>

#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits>

#include <algorithm>
#include <vector>
#include <cfloat>
#include <fstream>

#include <eigen/Eigen/Core>

#include "tools.h"
#include "param.h"
#include "convexHull.h"
#include "filter_segDyn.h"
#include "critere.h"
#include "resfusion.h"
#include "point.h"
#include "sommet.h"
#include "arbre.h"
#include "foret.h"



class Segmentation {
public :

    Segmentation();
    Foret& segmPPL(MeshModel* m, MeshModel *mSort, CMeshO::PerVertexAttributeHandle< int > &label, Foret& forest, int k, Param &p);
    std::vector<int> getLabelPPL(MeshModel* mSort, double radius, Foret& forest, Eigen::Vector3d &point3d, int k, CMeshO::PerVertexAttributeHandle< int > &label);
    int getCH(MeshModel* mSort,Foret &forest,const Eigen::Vector3d &point3d);
    int getChFIN(std::vector<std::vector <Eigen::Vector3d> > &forest3d,const Eigen::Vector3d &point3d);

    Foret getApex(std::vector<Foret> &Resultats, Param &p );
    std::vector<std::vector<Sommet> > getCHPeres(Foret &ResPere, Foret &ResFils);
    void Fusion(int labelpere,Arbre arbrePere, std::vector <Sommet> &SommetsFils, Foret &ResFus,int k,Param &p);
    void compareScores(Foret &ResPere,Foret &ResFils,int k, Param &p);


    void segmFIN(MeshModel* m, MeshModel *mSort, std::vector<Sommet> listeSommets, CMeshO::PerVertexAttributeHandle< int > &label, CMeshO::PerVertexAttributeHandle< int > &isSommet, CMeshO::PerVertexAttributeHandle< int > &isSommetv2, Param& p);
    int getLabelFIN(double radius, std::vector<std::vector <Eigen::Vector3d> > &forest3d, Eigen::Vector3d point3d , int k, CMeshO::PerVertexAttributeHandle< int > &label);


    int nb_lab ; // nombre de labels = indice du premier label non utilisé
    vcg::GridStaticPtr<CVertexO,double> PtGrid; // Grille statique pour la gestion des points
    std::vector<Point> _Nuage;

};

#endif // SEGMENTATION_H
