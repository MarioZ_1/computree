#ifndef TOOLS_H
#define TOOLS_H

#include "sommet.h"
#include <eigen/Eigen/Core>

bool compare4fby3(Eigen::Vector4d _c1, Eigen::Vector4d _c2);
bool compare2fby2(Eigen::Vector2d _c1, Eigen::Vector2d _c2);
bool compare3fby1(Eigen::Vector3d _c1, Eigen::Vector3d _c2);
bool compare3fby3(Eigen::Vector3d _c1, Eigen::Vector3d _c2);
bool compareApexbyScore(Sommet _S1, Sommet _S2);

//double calculDist3D (Eigen::Vector3d &current ,std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr> closestObj);
bool selectNeighbor(Eigen::Vector3d &current, Eigen::Vector3d &neighbor, double Th2D, double Th3D);


#endif // TOOLS_H
