<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>LIF_MetricVolume</name>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="48"/>
        <source>Métriques volume</source>
        <translation>Volume metrics</translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="54"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- volume_in (m3)&lt;br&gt;- volume_in_gap (m3)&lt;br&gt;- volume_out (m3)&lt;br&gt;- volume_out_gap (m3)&lt;br&gt;- volume_tot (m3)&lt;br&gt;- volume_tot_gap (m3)&lt;br&gt;- surface_gap (m²)&lt;br&gt;- surface_NA (m²)&lt;br&gt;- h_max (m)&lt;br&gt;- h_mean (m)&lt;br&gt;- h_sd (m)&lt;br&gt;- rumple&lt;br&gt;- rumple_hmean&lt;br&gt;</source>
        <translation>Following values are computed:&lt;br&gt;- volume_in (m3)&lt;br&gt;- volume_in_gap (m3)&lt;br&gt;- volume_out (m3)&lt;br&gt;- volume_out_gap (m3)&lt;br&gt;- volume_tot (m3)&lt;br&gt;- volume_tot_gap (m3)&lt;br&gt;- surface_gap (m²)&lt;br&gt;- surface_NA (m²)&lt;br&gt;- h_max (m)&lt;br&gt;- h_mean (m)&lt;br&gt;- h_sd (m)&lt;br&gt;- rumple&lt;br&gt;- rumple_hmean&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="75"/>
        <source>Seuil de hauteur max pour les trouées</source>
        <translation>Max height threshold for gaps</translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="238"/>
        <source>volume_in</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="239"/>
        <source>volume_in_gap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="240"/>
        <source>volume_out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="241"/>
        <source>volume_out_gap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="242"/>
        <source>volume_tot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="243"/>
        <source>volume_tot_gap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="244"/>
        <source>surface_gap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="245"/>
        <source>surface_NA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="246"/>
        <source>h_max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="247"/>
        <source>h_mean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="248"/>
        <source>h_sd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="249"/>
        <source>rumple</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/lif_metricvolume.cpp" line="250"/>
        <source>rumple_hmean</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LIF_StepDTM</name>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="92"/>
        <source>LIDAR Ground filtering and DTM computation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="97"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="110"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="114"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="121"/>
        <source>Resolution of the DTM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="122"/>
        <source>Threshold for excessive low zmin levels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="123"/>
        <source>Threshold</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="124"/>
        <source>Threshold Fine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="131"/>
        <location filename="../step/lif_stepdtm.cpp" line="135"/>
        <source>DTM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="133"/>
        <source>Vegetation points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="134"/>
        <source>Ground points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="137"/>
        <source>DTM tmp1 : ZMIN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="138"/>
        <source>DTM tmp2 : low</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="139"/>
        <source>DTM tmp3 : end of step 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="140"/>
        <source>DTM tmp4 : end of step 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="141"/>
        <source>DTM tmp5 : interpol (begin step2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="143"/>
        <source>Mask 1: step2 base</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="144"/>
        <source>Mask 2: step2 after hole filling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="145"/>
        <source>Mask 3: step2 after polygon filling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="146"/>
        <source>Mask 4: step2 after erosion</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="147"/>
        <source>Mask 5: step2 base</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="181"/>
        <source>Begin step 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="195"/>
        <source>Zmin done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="233"/>
        <source>Low values filtering done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="234"/>
        <source>Begin step 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="259"/>
        <source>Begin new iteration (Step 2): %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="290"/>
        <location filename="../step/lif_stepdtm.cpp" line="488"/>
        <source>Interpolation done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="321"/>
        <source>Mask creation done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="359"/>
        <source>Hole filling done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="368"/>
        <source>Polygons filling done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="408"/>
        <source>Mask erosion done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="426"/>
        <location filename="../step/lif_stepdtm.cpp" line="549"/>
        <source>Mask applied</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="427"/>
        <source>naNumber = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="444"/>
        <source>Begin step 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="455"/>
        <source>Begin new iteration (Step 3): %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="530"/>
        <source>Local derivatives done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="558"/>
        <source>Begin step 4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepdtm.cpp" line="585"/>
        <source>Classification done</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LIF_StepOutliersRemoving</name>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="58"/>
        <source>Upper outliers removing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="64"/>
        <source>This step is an inversion of LIF_StepPitFilling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="70"/>
        <source>http://www.sciencedirect.com/science/article/pii/S0303243411000535</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="85"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="86"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="95"/>
        <source>Outliers removed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="103"/>
        <source>Height difference for outliers to be removed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepoutliersremoving.cpp" line="245"/>
        <source>Turn %2, Number of modified pixels: %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LIF_StepPTreeSegmentation</name>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="50"/>
        <source>PTree Segmentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="56"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="70"/>
        <source>Scène à découper</source>
        <translation>Scene to cut</translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="72"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="73"/>
        <source>Scène à segmenter</source>
        <translation>Scene to segment</translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="84"/>
        <source>Scène segmentée</source>
        <translation>Segmented scene</translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="85"/>
        <source>Convex Hull</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="86"/>
        <source>Score Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="87"/>
        <source>Score Regularity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="88"/>
        <source>Score Solidity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="89"/>
        <source>Score SummitOffset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="90"/>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="91"/>
        <source>k</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="100"/>
        <source>Scales</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="100"/>
        <source>List of neighbours numbers, separated by commas</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="101"/>
        <source>Distance to dominated trees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="102"/>
        <source>Solidity grid resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="102"/>
        <source>Resolution of the grid for solodity computation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="103"/>
        <source>Optimization Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_stepptreesegmentation.cpp" line="103"/>
        <source>Resolution of the grid</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LIF_StepPitFilling</name>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="62"/>
        <source>Pit filling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="68"/>
        <source>Pit filling algorithm from:&lt;br&gt;&lt;b&gt;Vega C., Durrieu S., 2011.&lt;/b&gt;&lt;br&gt;&lt;em&gt;Multi-level filtering segmentation to measure individual tree parameters basedon Lidar data: Application to a mountainous forest with heterogeneous stands.&lt;/em&gt;&lt;br&gt;International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="77"/>
        <source>http://www.sciencedirect.com/science/article/pii/S0303243411000535</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="92"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="93"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="102"/>
        <source>Pits filled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="110"/>
        <source>Depth of pits to be filled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling.cpp" line="244"/>
        <source>Turn %2, Number of modified pixels: %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LIF_StepPitFilling02</name>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="62"/>
        <source>Pit filling (v2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="68"/>
        <source>Pit filling algorithm from:&lt;br&gt;&lt;b&gt;Vega C., Durrieu S., 2011.&lt;/b&gt;&lt;br&gt;&lt;em&gt;Multi-level filtering segmentation to measure individual tree parameters basedon Lidar data: Application to a mountainous forest with heterogeneous stands.&lt;/em&gt;&lt;br&gt;International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="77"/>
        <source>http://www.sciencedirect.com/science/article/pii/S0303243411000535</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="92"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="93"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="102"/>
        <source>Pits filled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="110"/>
        <source>Depth of pits to be filled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/lif_steppitfilling02.cpp" line="262"/>
        <source>Turn %2, Number of modified pixels: %1</source>
        <translation></translation>
    </message>
</context>
</TS>
