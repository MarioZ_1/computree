#include "param.h"

using namespace std;

Param::Param()
{
}


void Param::setbound(Eigen::Vector3i bound)
{
    _bound = bound;
}
Eigen::Vector3i Param :: getbound()
{
    return _bound;
}

void Param::setdensity(double d)
{
    _density = d;
}
double Param::getdensity(){
    return _density;
}


void Param ::setneighborV(Eigen::Vector2i neighborV)
{
    _neighborV = neighborV;
}
Eigen::Vector2i Param::getneighborV()
{
    return _neighborV;
}


void Param::setscale(int scale)
{
    _scale = scale;
}
int Param ::getscale(){
    return _scale;
}


void Param ::setminDist(double minDist)
{
    _minDist = minDist;
}
double Param::getminDist(){
    return _minDist;
}


void Param::setminH(double minH)
{
    _minH = minH;
}
double Param::getminH(){
    return _minH;
}


void Param::setGrd(int grd)
{
    _grd = grd;
}
int Param::getGrd(){
    return _grd;
}

void Param::setcoefcompar(double coefcompar)
{
    _coefcompar=coefcompar;
}

double Param::getcoefcompar()
{
    return _coefcompar;
}
