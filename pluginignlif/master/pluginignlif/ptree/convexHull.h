#ifndef CONVEXHULL_H
#define CONVEXHULL_H

#include <QtGui>

#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits>


#include <algorithm>
#include <vector>
#include <cfloat>
#include <fstream>

#include <eigen/Eigen/Core>
#include "tools.h"
#include "param.h"
#include "convexHull.h"

class ConvexHull {

public:


    ConvexHull();
    static bool pointInConvexHull(Eigen::Vector3d p,  std::vector<Eigen::Vector3d> branche);
    static void printConvexHull(std::vector<Eigen::Vector3d > branche);
    static double computeArea( std::vector<Eigen::Vector3d > bound);
    static double addNewPointAndComputeArea(Eigen::Vector3d p, std::vector<Eigen::Vector3d > branche);
    static std::vector<Eigen::Vector3d > buildLowerConvexHull(std::vector<Eigen::Vector3d> branche);
    static std::vector<Eigen::Vector3d > buildUpperConvexHull(std::vector<Eigen::Vector3d> branche);
    static std::vector<Eigen::Vector3d> buildConvexHull(std::vector<Eigen::Vector3d> branche);
//    static int getCommonAreas(int l, int c, std::vector< std::vector <Eigen::Vector3d > > arbre, std::vector<int> labels_voisins, Eigen::Vector3d  p);
    static double computeDistMax(std::vector<Eigen::Vector3d> branche, Eigen::Vector3d apex);

    static double det(Eigen::Vector3d  p, Eigen::Vector3d  q, Eigen::Vector3d  r);
    static bool isRightTurn(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r);

};




#endif // CONVEXHULL_H
