#include "arbre.h"

#include <QDebug>

using namespace std;

Arbre::Arbre()
{
}
Arbre::~Arbre()
{

}

const std::vector<Point> &Arbre::getlistePoints() const
{
    return _listePoints;
}

void Arbre::setlistePoints( std::vector<Point> liste)
{
    _listePoints=liste;
}

void Arbre::push_newPoint(Point point)
{
    int ind = point.getIndice();
    int lab = point.getlab();
//    int numCh = point.getCh();
    double height =point.getHeight();
    Eigen::Vector3d coord =point.getcoord();

    int nbPoints =getNbPoints();
    if (nbPoints==0)
    {
        Sommet newSommet;
//        newSommet.setCh(numCh);
        newSommet.setcoord(coord);
        newSommet.setHeight(height);
        newSommet.setlab(lab);
        newSommet.setIndice(ind);

        setSommet(newSommet);
    }

    std::vector<Point> tmp=getlistePoints();
    tmp.push_back(point);
    setlistePoints(tmp);
}

const Sommet& Arbre::getSommet() const
{
    return _sommet;
}

void Arbre::setSommet(Sommet sommet)
{
    _sommet =sommet;
}

void Arbre::setbound(std::vector<Eigen::Vector3d> bound)
{
    _convexHull=bound;
}

std::vector<Eigen::Vector3d> Arbre::getbound()
{
    return _convexHull;
}

std::vector<Eigen::Vector3d> Arbre::getarbre3d() const
{
    std::vector<Eigen::Vector3d> arbre3d;
    std::vector<Point> list=getlistePoints();
    qDebug() << "list size : " << list.size();
    Point ptmp;
    Eigen::Vector3d ptmp3d;
    for (std::vector<Point>::iterator it =list.begin(); it!=list.end();it++)
    {
        ptmp = *it;
        ptmp3d = ptmp.getcoord();
        arbre3d.push_back(ptmp3d);
    }
    return arbre3d;
}


double Arbre::getScoreArbre()
{
    return _score;
}

void Arbre::setScoreArbre(double score)
{
    Sommet Stmp = getSommet();
    Stmp.setscoresommet(score);
    setSommet(Stmp);
    _score = score;

}

void Arbre::setDistMaxArbre(double DistMax)
{
    _Distmax = DistMax;
}
double Arbre::getDistMaxArbre()
{
    return _Distmax;
}

Point Arbre::extractPoint(int indice)
{
    std::vector<Point> list=getlistePoints();
    for (std::vector<Point>::iterator it =list.begin(); it!=list.end();++it)
    {
        Point ptmp = *it;
        int ind = ptmp.getIndice();
        if (ind==indice)
        {
            return ptmp;
        }
    }
    Point pointerror(-1);
    return pointerror;
}

int Arbre::getIndicePoint(int num)
 {
     int count =0;
     std::vector<Point> list=getlistePoints();
     for (std::vector<Point>::iterator it =list.begin(); it!=list.end();++it)
     {
         if (count==num)
         {
             Point ptmp=*it;
             return ptmp.getIndice();
         }
         count++;
     }

     return -1;
 }


unsigned int Arbre::getNbPoints()
{
    std::vector<Point> listePoints=getlistePoints();
    return listePoints.size();
}

int Arbre::getlabelTree()
{
    std::vector<Point> listePoints =getlistePoints();
    Point sommet = listePoints[0];
    int numarbre =sommet.getlab();
    return numarbre;

}

void Arbre::setlabelTree(int newlab)
{
    std::vector<Point> list =getlistePoints();
    Sommet s =getSommet();
    s.setlab(newlab);
    setSommet(s);
    for (std::vector<Point>::iterator it =list.begin(); it!=list.end();++it)
    {
        Point ptmp = *it;
        ptmp.setlab(newlab);
        *it=ptmp;
    }
    setlistePoints(list);
}
int Arbre::getIndiceSommet()
{
    Sommet som = getSommet();
    int ind = som.getIndice();

    return ind;
}

std::vector<Sommet> Arbre::getSommetsFils(std::vector<Sommet> &listesommets)
{
    int labelsPere =getlabelTree();
    std::vector < Sommet > SommetsFils;

    for (std::vector < Sommet >::iterator it=listesommets.begin();it!=listesommets.end();++it)
    {
        Sommet Stmp =*it;
        int indicesommetFils = Stmp.getIndice();
        Point ptmp = extractPoint(indicesommetFils);
        int labFils = ptmp.getlab();
        if (labFils==labelsPere)
        {
            SommetsFils.push_back(Stmp);
        }
    }

    return SommetsFils;
}

void Arbre::clearTree()
{
    std::vector<Point> tmp =getlistePoints();
    tmp.clear();
    setlistePoints(tmp);
}
