#include "lif_pluginentry.h"
#include "lif_pluginmanager.h"

LIF_PluginEntry::LIF_PluginEntry()
{
    _pluginManager = new LIF_PluginManager();
}

LIF_PluginEntry::~LIF_PluginEntry()
{
    delete _pluginManager;
}

QString LIF_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* LIF_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_ignlif, LIF_PluginEntry)
#endif
