/************************************************************************************
* Filename :  lsis_beam4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_BEAM4D_H
#define LSIS_BEAM4D_H

// La direction et l'origine du beam sont stockes sous forme de LSIS_Point4DDouble
#include "../point4d/lsis_point4d.h"

// Utilisation des assertions
#include "assert.h"
#include <QString>

/*! \def    EPSILON_INTERSECTION_RAY
            Redefinition of the zero for the ray-box intersection algorithm */
#define EPSILON_INTERSECTION_RAY 0.000001    // 10^-6

/** \class  LSIS_Beam4D
    \brief  This class represents a 4D beam in a parametric manner.
            Each ray has a normalized direction (norm = 1).
*/
class LSIS_Beam4D
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
    *  \brief Default constructor
    *
    *  Default constructor of the class
    *  Each attribute will be set to 0 or NULL
    *  Each vector will be empty
    *
    */
    LSIS_Beam4D();

    /*!
    *  \brief Constructor
    *
    *  Constructor of the class
    *
    *  \param origin : origin of the ray
    *  \param direction : direction of the ray
    */
    LSIS_Beam4D(const LSIS_Point4DDouble& origin, const LSIS_Point4DDouble& direction );

    /*!
    *  \brief Destructor
    *
    *  Destructor of the class
    *
    */
    ~LSIS_Beam4D();

//********************************************//
//                Operators                   //
//********************************************//
    /*!
    *  \brief Access operator
    *
    *  \param t : time spent by the ray
    *
    *  \return Returns the point of the ray at a given time
    */
    inline LSIS_Point4DDouble operator() (double t) const
    {
        LSIS_Point4DDouble result;
        result = _origin + t * _direction;
        return result;
    }

//********************************************//
//                  Getters                   //
//********************************************//
    /*!
    *  \brief Getter of the class
    *
    *  \return Returns the origin of the ray
    */
    inline LSIS_Point4DDouble getOrigin () const { return _origin; }

    /*!
    *  \brief Getter of the class
    *
    *  \return Returns the direction of the ray
    */
    inline LSIS_Point4DDouble getDirection () const { return _direction; }

//********************************************//
//                  Setters                   //
//********************************************//
    /*!
    *  \brief Setter of the class
    */
    inline void setOrigin ( const LSIS_Point4DDouble& origin )
    {
        _origin = origin;
    }

    /*!
    *  \brief Setter of the class
    */
    inline void setDirection ( const LSIS_Point4DDouble& direction )
    {
        assert( direction.norm() != 0 );
        _direction = direction;
        _direction.normalize();
    }

//********************************************//
//                   Tools                    //
//********************************************//
    /*!
    *  \brief Calculates the intersection between a grid and a ray
    *
    *  This method uses the algorithm from Williams et al.
    *  ****************************************************************
    *  Williams, A., Barrus, S., & Morley, R. (2005).
    *  An efficient and robust ray-box intersection algorithm.
    *  ACM SIGGRAPH 2005
    *  1-4.
    *  *****************************************************************
    *
    *  \param bot : bottom left corner of the box to intersect
    *  \param top : top right corner of the box to intersect
    *  \param near : output, nearest 4D point of the intersection
    *  \param far : output, farest 4D point intersection
    *
    *  \return Returns false if no intersection was found, true else
    */
    bool intersect ( const LSIS_Point4DDouble& bot, const LSIS_Point4DDouble& top, LSIS_Point4DDouble& nearP, LSIS_Point4DDouble& farP ) const;

    /*!
    *  \brief Test the intersection between a grid and a ray
    *
    *  This method uses the algorithm from Williams et al.
    *  ****************************************************************
    *  Williams, A., Barrus, S., & Morley, R. (2005).
    *  An efficient and robust ray-box intersection algorithm.
    *  ACM SIGGRAPH 2005
    *  1-4.
    *  *****************************************************************
    *
    *  \param bot : bottom left corner of the box to intersect
    *  \param top : top right corner of the box to intersect
    *
    *  \return Returns false if no intersection was found, true else
    */
    bool intersect ( const LSIS_Point4DDouble& bot, const LSIS_Point4DDouble& top ) const;

private :
    /*!
     * \brief Utilitary function for intersect
     */
    bool updateIntervals( const double &bottomCorner,const double &upperCorner,
                          const double &origin, const double &direction,
                          double &t0,double &t1 ) const;

/* **************************************************************** */
/* Attributs de la classe                                           */
/* **************************************************************** */
private :
    LSIS_Point4DDouble _origin;          /*!< Origin of the ray*/
    LSIS_Point4DDouble _direction;       /*!< Direction of the ray*/
};

#endif // LSIS_BEAM4D_H
