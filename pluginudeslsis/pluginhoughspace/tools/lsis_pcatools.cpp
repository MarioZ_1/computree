/************************************************************************************
* Filename :  lsis_pcatools.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_pcatools.h"

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

LSIS_Point4DFloat LSIS_PcaTools::getCentroid(QVector<LSIS_Pixel4D> &pixels)
{
    int nbPoints = pixels.size();

    LSIS_Point4DFloat outCentroid;
    outCentroid.setZero();

    for( int i = 0 ; i < nbPoints ; i++ )
    {
        outCentroid += pixels.at(i);
    }

    for( int i = 0 ; i < 4 ; i++ )
    {
        outCentroid(i) /= nbPoints;
    }

    return outCentroid;
}

LSIS_Point4DFloat LSIS_PcaTools::getCentroid(QVector<LSIS_Point4DFloat> &pixels)
{
    int nbPoints = pixels.size();

    LSIS_Point4DFloat outCentroid;
    outCentroid.setZero();

    for( int i = 0 ; i < nbPoints ; i++ )
    {
        for( int j = 0 ; j < 4 ; j++ )
        {
            outCentroid(j) += pixels.at(i)(j);
        }
    }

    for( int i = 0 ; i < 4 ; i++ )
    {
        outCentroid(i) /= nbPoints;
    }

    return outCentroid;
}

void LSIS_PcaTools::acp4D(QVector< LSIS_Pixel4D > &pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                          float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 )
{
    int nbPoints = pixels.size();
    LSIS_Pixel4D currPixel;

    // Il faut centrer le nuage de pixels pour cela on doit calculer son barycentre
    LSIS_Point4DFloat centroid = getCentroid( pixels );

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 4, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 4 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i) - centroid(i);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}

void LSIS_PcaTools::acp4DNonCentree(QVector< LSIS_Pixel4D > &pixels,
                                    LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                                    float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 )
{
    int nbPoints = pixels.size();
    LSIS_Pixel4D currPixel;

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 4, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 4 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}

void LSIS_PcaTools::acp4D(QVector< LSIS_Point4DFloat > &pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                          float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 )
{
    int nbPoints = pixels.size();
    LSIS_Point4DFloat currPixel;

    // Il faut centrer le nuage de pixels pour cela on doit calculer son barycentre
    LSIS_Point4DFloat centroid = getCentroid( pixels );

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 4, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 4 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i) - centroid(i);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}

void LSIS_PcaTools::acp4D(QVector< LSIS_Point4DFloat > &pixels,
                          LSIS_Point4DFloat& centroid,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                          float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 )
{
    int nbPoints = pixels.size();
    LSIS_Point4DFloat currPixel;

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 4, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 4 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i) - centroid(i);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}

void LSIS_PcaTools::acp4DNonCentree(QVector< LSIS_Point4DFloat > &pixels,
                                    LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                                    float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 )
{
    int nbPoints = pixels.size();
    LSIS_Point4DFloat currPixel;

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 4, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);

        if( std::isnan( currPixel.w() ) || std::isnan( currPixel.x() ) || std::isnan( currPixel.y() ) || std::isnan( currPixel.z() ) )
        {
            qDebug() << "Point nan " << currPixel.w() << currPixel.x() << currPixel.y() << currPixel.z();
        }

        else
        {
            for( int i = 0 ; i < 4 ; i++ )
            {
                pointsMatrix( i, j ) = currPixel(i);
            }
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}

void LSIS_PcaTools::acp3D(QVector< LSIS_Pixel4D > &pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3,
                          float& outLambda1, float& outLambda2, float& outLambda3 )
{
    int nbPoints = pixels.size();
    LSIS_Pixel4D currPixel;

    // Il faut centrer le nuage de pixels pour cela on doit calculer son barycentre
    LSIS_Point4DFloat centroid = getCentroid( pixels );

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 3, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 3 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i+1) - centroid(i);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix3f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix3f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector3cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix3cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector3cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 3 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 3 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();

    outV1.set( 0, eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real() );
    outV2.set( 0, eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real() );
    outV3.set( 0, eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real() );
}

void LSIS_PcaTools::acp3D(QVector< LSIS_Point4DFloat > &pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3,
                          float& outLambda1, float& outLambda2, float& outLambda3 )
{
    int nbPoints = pixels.size();
    LSIS_Point4DFloat currPixel;

    // Il faut centrer le nuage de pixels pour cela on doit calculer son barycentre
    LSIS_Point4DFloat centroid = getCentroid( pixels );

    // On cree la matrice 4*nbPoints et on la remplit (conversion du tableau de points en matrice en fait)
    Eigen::MatrixXf pointsMatrix ( 3, nbPoints );
    for( int j = 0 ; j < nbPoints ; j++ )
    {
        currPixel = pixels.at(j);
        for( int i = 0 ; i < 3 ; i++ )
        {
            pointsMatrix( i, j ) = currPixel(i+1) - centroid(i+1);
        }
    }

    // On calcule la matrice de covariance des points
    Eigen::Matrix3f cov = pointsMatrix * pointsMatrix.transpose();

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix3f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector3cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix3cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector3cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 3 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 3 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();

    outV1.set( 0, eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real() );
    outV2.set( 0, eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real() );
    outV3.set( 0, eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real() );
}

void LSIS_PcaTools::acp4DPonderee(QVector<LSIS_Point4DFloat> &points,
                                  QVector<float> &poids,
                                  float sommeDesPoids,
                                  LSIS_Point4DFloat &centroidPondere,
                                  LSIS_Point4DFloat &outV1, LSIS_Point4DFloat &outV2, LSIS_Point4DFloat &outV3, LSIS_Point4DFloat &outV4,
                                  float &outLambda1, float &outLambda2, float &outLambda3, float &outLambda4)
{
    // On calcule la matrice de covariance des points
    Eigen::Matrix4f cov = computeWeightedCovarianceMatrix( points, poids, sommeDesPoids );

    // On calcule les valeurs propres et les vecteurs propres de la matrice de covariance
    Eigen::EigenSolver< Eigen::Matrix4f > eigenSolver;
    eigenSolver.compute( cov );
    Eigen::Vector4cf eigenValues = eigenSolver.eigenvalues();
    Eigen::Matrix4cf eigenVectors = eigenSolver.eigenvectors();  // Les colonnes de cette matrice sont les vecteurs propres associes aux valeurs propres

    // Il faut trier les valeurs propres et permuter les vecteurs propres en accordance
    // On fait un tri decroissant, i.e. les plus grandes valeurs seront a gauche du tableau
    // On fait un tri brut, pour un vecteur de 4 valeurs ce n'est pas grave
    float maxValue;
    float currValue;
    int maxID;
    std::complex<float> swapVal;
    Eigen::Vector4cf swapVec;
    // Pour chaque valeur
    for ( int i = 0 ; i < 4 ; i++ )
    {
        maxID =i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 4 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }

    // On peut transformer le tout en pixels de sortie
    outLambda1 = eigenValues(0).real();
    outLambda2 = eigenValues(1).real();
    outLambda3 = eigenValues(2).real();
    outLambda4 = eigenValues(3).real();

    outV1.set( eigenVectors(0,0).real(), eigenVectors(1,0).real(), eigenVectors(2,0).real(), eigenVectors(3,0).real() );
    outV2.set( eigenVectors(0,1).real(), eigenVectors(1,1).real(), eigenVectors(2,1).real(), eigenVectors(3,1).real() );
    outV3.set( eigenVectors(0,2).real(), eigenVectors(1,2).real(), eigenVectors(2,2).real(), eigenVectors(3,2).real() );
    outV4.set( eigenVectors(0,3).real(), eigenVectors(1,3).real(), eigenVectors(2,3).real(), eigenVectors(3,3).real() );
}


Eigen::Matrix4f LSIS_PcaTools::computeWeightedCovarianceMatrix(QVector<LSIS_Point4DFloat> &points, QVector<float> &poids, float poidsTotal)
{
    int nPoints = points.size();
    float sommePoidsMultVariableWW = 0;
    float sommePoidsMultVariableWX = 0;
    float sommePoidsMultVariableWY = 0;
    float sommePoidsMultVariableWZ = 0;
    float sommePoidsMultVariableXX = 0;
    float sommePoidsMultVariableXY = 0;
    float sommePoidsMultVariableXZ = 0;
    float sommePoidsMultVariableYY = 0;
    float sommePoidsMultVariableYZ = 0;
    float sommePoidsMultVariableZZ = 0;
    float sommePoidsMultVariableW = 0;
    float sommePoidsMultVariableX = 0;
    float sommePoidsMultVariableY = 0;
    float sommePoidsMultVariableZ = 0;
    LSIS_Point4DFloat curPoint;

    for( int i = 0 ; i < nPoints ; i++ )
    {
        curPoint = points.at(i);
        sommePoidsMultVariableW += poids.at(i) * curPoint.w();
        sommePoidsMultVariableX += poids.at(i) * curPoint.x();
        sommePoidsMultVariableY += poids.at(i) * curPoint.y();
        sommePoidsMultVariableZ += poids.at(i) * curPoint.z();
        sommePoidsMultVariableWW += poids.at(i) * curPoint.w() * curPoint.w();
        sommePoidsMultVariableWX += poids.at(i) * curPoint.w() * curPoint.x();
        sommePoidsMultVariableWY += poids.at(i) * curPoint.w() * curPoint.y();
        sommePoidsMultVariableWZ += poids.at(i) * curPoint.w() * curPoint.z();
        sommePoidsMultVariableXX += poids.at(i) * curPoint.x() * curPoint.x();
        sommePoidsMultVariableXY += poids.at(i) * curPoint.x() * curPoint.y();
        sommePoidsMultVariableXZ += poids.at(i) * curPoint.x() * curPoint.z();
        sommePoidsMultVariableYY += poids.at(i) * curPoint.y() * curPoint.y();
        sommePoidsMultVariableYZ += poids.at(i) * curPoint.y() * curPoint.z();
        sommePoidsMultVariableZZ += poids.at(i) * curPoint.z() * curPoint.z();
    }

    float inversePoidsTotal = 1.0/poidsTotal;
    float covWW = (inversePoidsTotal * sommePoidsMultVariableWW ) - ( (inversePoidsTotal * sommePoidsMultVariableW ) * ( inversePoidsTotal * sommePoidsMultVariableW ) );
    float covWX = (inversePoidsTotal * sommePoidsMultVariableWX ) - ( (inversePoidsTotal * sommePoidsMultVariableW ) * ( inversePoidsTotal * sommePoidsMultVariableX ) );
    float covWY = (inversePoidsTotal * sommePoidsMultVariableWY ) - ( (inversePoidsTotal * sommePoidsMultVariableW ) * ( inversePoidsTotal * sommePoidsMultVariableY ) );
    float covWZ = (inversePoidsTotal * sommePoidsMultVariableWZ ) - ( (inversePoidsTotal * sommePoidsMultVariableW ) * ( inversePoidsTotal * sommePoidsMultVariableZ ) );
    float covXX = (inversePoidsTotal * sommePoidsMultVariableXX ) - ( (inversePoidsTotal * sommePoidsMultVariableX ) * ( inversePoidsTotal * sommePoidsMultVariableX ) );
    float covXY = (inversePoidsTotal * sommePoidsMultVariableXY ) - ( (inversePoidsTotal * sommePoidsMultVariableX ) * ( inversePoidsTotal * sommePoidsMultVariableY ) );
    float covXZ = (inversePoidsTotal * sommePoidsMultVariableXZ ) - ( (inversePoidsTotal * sommePoidsMultVariableX ) * ( inversePoidsTotal * sommePoidsMultVariableZ ) );
    float covYY = (inversePoidsTotal * sommePoidsMultVariableYY ) - ( (inversePoidsTotal * sommePoidsMultVariableY ) * ( inversePoidsTotal * sommePoidsMultVariableY ) );
    float covYZ = (inversePoidsTotal * sommePoidsMultVariableYZ ) - ( (inversePoidsTotal * sommePoidsMultVariableY ) * ( inversePoidsTotal * sommePoidsMultVariableZ ) );
    float covZZ = (inversePoidsTotal * sommePoidsMultVariableZZ ) - ( (inversePoidsTotal * sommePoidsMultVariableZ ) * ( inversePoidsTotal * sommePoidsMultVariableZ ) );

    Eigen::Matrix4f rslt;
    rslt(0,0) = covWW;
    rslt(0,1) = covWX;
    rslt(1,0) = covWX;
    rslt(0,2) = covWY;
    rslt(2,0) = covWY;
    rslt(0,3) = covWZ;
    rslt(3,0) = covWZ;
    rslt(1,1) = covXX;
    rslt(1,2) = covXY;
    rslt(2,1) = covXY;
    rslt(1,3) = covXZ;
    rslt(3,1) = covXZ;
    rslt(2,2) = covYY;
    rslt(2,3) = covYZ;
    rslt(3,2) = covYZ;
    rslt(3,3) = covZZ;

    return rslt;
}
