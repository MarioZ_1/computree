/************************************************************************************
* Filename :  lsis_houghspace4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_HOUGHSPACE4D_H
#define LSIS_HOUGHSPACE4D_H

// Un espace de hough derive d'une grille 4D
#include "ct_itemdrawable/ct_grid4d_sparse.h"

// Le lancer de rayon se fait a partir d'un nuage de normales (les positions des points sont compris dans le nuage de normales)
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

// Utilise des pixels pour se deplacer ou renvoyer des pixels d'interet
#include "../pixel/lsis_pixel4d.h"

// Utilise un mnt selon les traitements a efectuer
#include "ct_itemdrawable/ct_grid2dxy.h"
#include "ct_itemdrawable/ct_image2d.h"

// Pour le seuil d'Otsu on utilise un histogramme
#include "../histogram/lsis_histogram1d.h"

#include "../tools/lsis_debugtool.h"

// Outils de raytracing
#include "../raytracing4d/lsis_traversalalgorithm4d.h"

class CT_AbstractStep;

class LSIS_HoughSpace4D : public CT_Grid4D_Sparse<int>
{
public:
/* **************************************************************** */
/* Constructeurs et Destructeurs                                    */
/* **************************************************************** */
    /*!
     * \brief LSIS_HoughSpace4D
     *
     * Constructeur par defaut
     */
    LSIS_HoughSpace4D();

    /*!
     * \brief LSIS_HoughSpace4D
     *
     * Constructeur, cree l'espace de hough et le remplit
     *
     * \param rmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param xmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param ymin : limite inferieure de l'espace de Hough (en cartesien)
     * \param zmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param rmax : limite superieure de l'espace de Hough (en cartesien)
     * \param xmax : limite superieure de l'espace de Hough (en cartesien)
     * \param ymax : limite superieure de l'espace de Hough (en cartesien)
     * \param zmax : limite superieure de l'espace de Hough (en cartesien)
     * \param resw : resolution de l'espace de HoughQString (en cartesien)
     * \param resx : resolution de l'espace de Hough (en cartesien)
     * \param resy : resolution de l'espace de Hough (en cartesien)
     * \param resz : resolution de l'espace de Hough (en cartesien)
     * \param constructByBoundingBox : parametre pour avoir une signature differente
     * \param normalCloud : nuage de normales a partir duquel on remplit l'espace de Hough
     * \param step : (optionnel) etape creant un espace de hough. Ce parametre est utilise pour faire avancer la progress bar de l'etape entre deux valeurs     *
     * \param progressMin : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     * \param progressMax : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     */
    LSIS_HoughSpace4D( const QString& modelName, const CT_AbstractResult *result,
                       double rmin, double xmin, double ymin, double zmin,
                       double rmax, double xmax, double ymax, double zmax,
                       double resw, double resx, double resy, double resz,
                       bool constructByBoundingBox,
                       const CT_PointsAttributesNormal *normalCloud = NULL,
                       LSIS_DebugTool* step = NULL,
                       int progressMin = 0,
                       int progressMax = 100 );

    LSIS_HoughSpace4D( const CT_OutAbstractSingularItemModel* model, const CT_AbstractResult *result,
                       double rmin, double xmin, double ymin, double zmin,
                       double rmax, double xmax, double ymax, double zmax,
                       double resw, double resx, double resy, double resz,
                       bool constructByBoundingBox,
                       const CT_PointsAttributesNormal *normalCloud = NULL,
                       LSIS_DebugTool* step = NULL,
                       int progressMin = 0,
                       int progressMax = 100 );

    /*!
     * \brief LSIS_HoughSpace4D
     *
     * Constructeur, cree l'espace de hough et le remplit
     *
     * \param rmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param xmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param ymin : limite inferieure de l'espace de Hough (en cartesien)
     * \param zmin : limite inferieure de l'espace de Hough (en cartesien)
     * \param dimw : dimension de l'espace
     * \param dimx : dimension de l'espace
     * \param dimy : dimension de l'espace
     * \param dimz : dimension de l'espace
     * \param resw : resolution de l'espace de Hough (en cartesien)
     * \param resx : resolution de l'espace de Hough (en cartesien)
     * \param resy : resolution de l'espace de Hough (en cartesien)
     * \param resz : resolution de l'espace de Hough (en cartesien)
     * \param constructByBoundingBox : parametre pour avoir une signature differente
     * \param normalCloud : nuage de normales a partir duquel on remplit l'espace de Hough
     * \param step : (optionnel) etape creant un espace de hough. Ce parametre est utilise pour faire avancer la progress bar de l'etape entre deux valeurs     *
     * \param progressMin : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     * \param progressMax : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     */
    LSIS_HoughSpace4D( const QString& modelName, const CT_AbstractResult *result,
                       double rmin, double xmin, double ymin, double zmin,
                       size_t dimw, size_t dimx, size_t dimy, size_t dimz,
                       double resw, double resx, double resy, double resz,
                       const CT_PointsAttributesNormal *normalCloud = NULL,
                       LSIS_DebugTool* step = NULL,
                       int progressMin = 0,
                       int progressMax = 100 );

    LSIS_HoughSpace4D( const CT_OutAbstractSingularItemModel* model, const CT_AbstractResult *result,
                       double rmin, double xmin, double ymin, double zmin,
                       size_t dimw, size_t dimx, size_t dimy, size_t dimz,
                       double resw, double resx, double resy, double resz,
                       const CT_PointsAttributesNormal *normalCloud = NULL,
                       LSIS_DebugTool* step = NULL,
                       int progressMin = 0,
                       int progressMax = 100 );

/* **************************************************************** */
/* Outils constructeurs et destructeurs                             */
/* **************************************************************** */
    /*!
     * \brief fillFromNormals
     *
     * Remplit l'espace de Hough a aprtir d'un nuage de normales (le nuage de points est contenu dans le nuage de normales)
     *
     * \param normalCloud : nuage de normales a utiliser pour remplir l'espace de hough
     * \param progressMin : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     * \param progressMax : range dans lequel faire avancer la progress bar de l'etape lors de la creation de l'espace de Houhg
     */
    void fillFromNormals(const CT_PointsAttributesNormal* normalAttribute, int progressMin, int progressMax );

//    void fillFromNormalsParallel(const CT_PointsAttributesNormal* normalAttribute, int progressMin, int progressMax );

    void fillFromPointsAndNormals(const CT_AbstractPointCloudIndex* pointCloud,
                                  const CT_AbstractNormalCloud* normalCloud,
                                  int progressMin, int progressMax );

/* **************************************************************** */
/* Outils de filtrage et lissage                                    */
/* **************************************************************** */
    /*!
     * \brief threshByValue
     *
     * Seuille l'espace de hough avec une valeur donnee :
     * les pixels dont la valeur est inferieure au seuil sont filtres
     *
     * \param thresh : valeur du seuil
     */
    void threshByValue( int thresh );

    /*!
     * \brief getEnergyImage
     *
     * Calcule l'image d'energie
     *
     * \param gamma : coefficient gamma dans l'optimisation (dans l'integrale)
     * \param globalWeight :
     * \return
     */
    CT_Grid4D_Sparse<float>* getEnergyImage( float gamma, float globalWeight ) const;

    /*!
     * \brief getGradients
     *
     * Calcule les gradients en chaque cellule de l'espace de Hough
     * Les composantes des gradients sont stockees dans 4 images differentes
     *
     * \param outGradW : composante en w du gradient
     * \param outGradX : composante en x du gradient
     * \param outGradY : composante en y du gradient
     * \param outGradZ : composante en z du gradient
     */
    void getEnergyGradients(CT_Grid4D_Sparse<float> *outGradW, CT_Grid4D_Sparse<float> *outGradX, CT_Grid4D_Sparse<float> *outGradY, CT_Grid4D_Sparse<float> *outGradZ,
                            CT_Grid4D_Sparse<float>* energyImage ) const;

    /*!
     * \brief gradientEnergyImageAtPixel
     *
     * Calcule le gradient de l'energie image en un pixel donne
     * Dans le cas general le gradient est calcule par difference finie ( (energieImage( i+1 ) - energieImage( i-1 )) / 2.0 )
     * Toutefois sur les bords, le gradient est calcule par difference finie a droite ou a gauche selon le bord atteint ( energieImage(i) - energieImage(i-1) ou energieImage(i+1) - energieImage(i) )
     *
     * \param p : pixel d'interet
     * \param globalWeight : poids de l'energie globale dans le calcul de l'energie image (inclu dans [0,1])
     *
     * \return le gradient de l'energie image au pixel p obtenu par difference finie (centree/droite/gauche selon la position du pixel p dans l'image)
     */
    LSIS_Point4DFloat gradientEnergyImageAtPixel(LSIS_Pixel4D const & p, float globalWeight) const;

    /*!
     * \brief energyImageAtPixel
     *
     * Calcule l'energie image pour un pixel donne
     * On calcule cette energie comme une ponderation d'un terme d'energie locale et d'un terme d'energie globale :
     * Energie locale = ( minInNeighbourhood - image(p) ) / ( maxInNeighbourhood - minInNeighbourhood ) (c.f. energyImageLocalAtPixel( LSIS_Pixel4D const & p ) )
     * Energie globale = ( minInHoughSpace - image(p) ) / ( maxInHoughSpace - minInHoughSpace ) (c.f. energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     * \param globalWeight : poids de la composante "globale" dans le calcul de l'energie (la composante locale a un poids de 1-globalWeight). Ce parametre doit etre compris dans [0,1]
     *
     * \return l'energie image au pixel d'interet
     */
    float energyImageAtPixel( LSIS_Pixel4D const & p, float globalWeight ) const;

    /*!
     * \brief energyImageGlobalAtPixel
     *
     * Calcule le terme d'energie image globale pour un pixel donne
     * Energie globale = ( minInHoughSpace - image(p) ) / ( maxInHoughSpace - minInHoughSpace ) (c.f. energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     *
     * \return l'energie image globale au pixel d'interet
     */
    float energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) const;

    /*!
     * \brief energyImageLocalAtPixel
     *
     * Calcule le terme d'energie image locale pour un pixel donne
     * Energie locale = ( minInNeighbourhood - image(p) ) / ( maxInNeighbourhood - minInNeighbourhood ) (c.f. energyImageLocalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     *
     * \return l'energie image locale au pixel d'interet
     */
    float energyImageLocalAtPixel( LSIS_Pixel4D const & p ) const;

    void fastFilter(const CT_PointsAttributesNormal* normalAttribute,
                    float ratioMin);

    void markFilteringDirections(const CT_PointsAttributesNormal* normalAttribute,
                                  QVector<bool>& outFilterPositiveDirection,
                                  QVector<bool>& outFilterNegativeDirection,
                                  float ratioMin );

    void markFilteringDirection(const CT_Point& point,
                                const CT_Normal normal,
                                bool& outFilterPositiveDirection,
                                bool& outFilterNegativeDirection,
                                float ratioMin );

    void filterMarkedDirections(const CT_PointsAttributesNormal* normalAttribute,
                                 QVector<bool>& filterPositiveDirection,
                                 QVector<bool>& filterNegativeDirection );

    void filterDirection(const CT_Point& point,
                         const CT_Normal normal );

/* **************************************************************** */
/* Outils d'analyse                                                 */
/* **************************************************************** */
    /*!
     * \brief getLocalMaximasInBBox
     *
     * Au sein d'une bounding box, recupere l'ensemble des pixels qui sont des maximas locaux
     *
     * \param bot : pixel inferieur gauche de la bbox
     * \param top : pixel superieur droit de la bbox
     * \param minValue : valeur minimum pur qu'un pixel soit considere
     *
     * \return un vecteur contenant les pixels maximas locaux
     */
    QVector< LSIS_Pixel4D >* getLocalMaximasInBBox(LSIS_Pixel4D const & bot,
                                                   LSIS_Pixel4D const & top,
                                                   int minValue,
                                                   int neighbourhoodSize) const;

    // Recherche des maximas sans regarder le voisinage
    QVector< LSIS_Pixel4D >* locateLocalMaximasInBBox(LSIS_Pixel4D const & bot,
                                                      LSIS_Pixel4D const & top,
                                                      int minValue,
                                                      int nbMaximas = 1) const;

    /*!
     * \brief getLocalMaximasInHeighRangeOverDTM
     *
     * Recupere l'ensemble des pixels qui sont des maximas locaux entre deux valeurs de hauteur au dessus d'un mnt
     * \todo Cette methode peut etre optimisee, a l'heure actuelle elle parcours tous les pixels de l'espace de Hough pour verifier qu'ils soient dans le range, cette recherche peut etre limitee
     * \warning Cette methode peut etre optimisee, a l'heure actuelle elle parcours tous les pixels de l'espace de Hough pour verifier qu'ils soient dans le range, cette recherche peut etre limitee
     *
     * \todo Il faudrait limiter la recherche des voisins dans le calcul du maxima (pour l'instant on regarde tout, y compri le pixel en z juste au dessus de la limite) cela peut entrainer un "ratage" de maxima local.
     * \warning Il faudrait limiter la recherche des voisins dans le calcul du maxima (pour l'instant on regarde tout, y compri le pixel en z juste au dessus de la limite) cela peut entrainer un "ratage" de maxima local.
     *
     * \param mnt : mnt au dessus duquel chercher les maximas
     * \param minz : hauteur minimum des maximas a chercher
     * \param maxz : hauteur maximum des maximas a chercher
     *
     * \return un vecteur contenant les pixels maximas locaux
     */
    QVector< LSIS_Pixel4D >* getLocalMaximasInHeighRangeOverDTM(const CT_Image2D<float> * mnt,
                                                                float minz,
                                                                float maxz,
                                                                int minValue,
                                                                int neighbourhoodSize) const;

    /*!
     * \brief getLocalMaximas
     *
     * Recupere l'ensemble des maximas locaux de l'ensemble de l'espace de Hough
     *
     * \param minValue : valeur minimum pur qu'un pixel soit considere
     *
     * \return un vecteur contenant les pixels maximas locaux
     */
    QVector< LSIS_Pixel4D >* getLocalMaximas(int minValue,
                                             int neighbourhoodSize) const;

    QVector< LSIS_Pixel4D >* locateLocalMaximas(int minValue, int nMaximas) const;

    /*!
     * \brief getLocalMaximasWithinHeightRange
     *
     * Renvoie les maximas locaux de l'espace de Hough compris entre deux hauteurs en z relatives au bot.z de l'espace de hough
     *
     * \param minz : hauteur minimum des maximas a chercher
     * \param maxz : hauteur maximum des maximas a chercher
     * \param minValue : valeur minimum pur qu'un pixel soit considere
     *
     * \return un vecteur contenant les pixels maximas locaux
     */
    QVector< LSIS_Pixel4D >* getLocalMaximasWithinHeightRange(float zmin, float zmax , int minValue, int neighbourhoodSize) const;

    // On cherche les maximas sans regarder le voisinage
    QVector< LSIS_Pixel4D >* locateLocalMaximasWithinHeightRange(float zmin, float zmax , int minValue, int nMaximas) const;

    /*!
     * \brief morphoClosure
     *
     * Fermeture morphologique de l'image.
     * \warning QUE POUR LES PIXELS DE VALEURS NULLE
     *
     * \param size : taille de l'élement structurant
     */
    void morphoClosure( int size );

    /*!
     * \brief morphoDilate
     *
     * Dilatation morphologique de l'image.
     * \warning QUE POUR LES PIXELS DE VALEURS NULLE
     *
     * \param size : taille de l'élement structurant
     */
    LSIS_HoughSpace4D* morphoDilate( int size ) const;

    /*!
     * \brief morphoErode
     *
     * Dilatation morphologique de l'image.
     * \warning QUE POUR LES PIXELS DE VALEURS NULLE
     *
     * \param size : taille de l'élement structurant
     */
    LSIS_HoughSpace4D* morphoErode( int size ) const;

/* **************************************************************** */
/* Outils de filtrage                                               */
/* **************************************************************** */
//    /*!
//     * \brief meanFilter
//     *
//     * Lisse l'espace de Hough grace a un filtre qui ressemble beaucoup a un filtre moyen.
//     * En realite on calcule la somme de la valeurs des voisins plus autant de fois sa propre valeur avant la normalisation
//     *
//     * \param sizeSmooth : taille de la demi fenetre de lissage
//     */
//    void meanFilter ( int sizeSmooth );

    /*!
     * \brief setValueInWindow
     *
     * Affecte la meme valeur a tous les pixels a l'interieur d'une partie de l'image definie par une bbox
     * Typiquement utilisee lors d'un filtrage ou on va mettre tous les pixels a 0 ou a NA
     *
     * \param bot : pixel inferieur de la bbox
     * \param top : pixel superieur de la bbox
     * \param val : valeur a affecter a l'interieur de la bbox
     */
    void setValueInWindow(LSIS_Pixel4D& bot,
                          LSIS_Pixel4D& top,
                          int val);

    /*!
     * \brief getMinMaxInWindow
     *
     * Recupere les valeurs de l'espace de Hough dans une sous-image donnee par sa bbox
     * On recupere aussi un tableau contenant toutes les valeurs de la sous-image
     * \warning On ne tient pas compte des valeurs NA ni des valeurs 0 dans cette methode !
     *
     * \param bot : point inferieur de la sous-image
     * \param top : point superieur de la sous-image
     * \param outMin : valeur minimum de la sous image
     * \param outMax : valeur maximum de la sous image
     * \param outValues : tableau des valeurs de la sous-image
     */
    void getMinMaxInWindow(LSIS_Pixel4D& bot,
                           LSIS_Pixel4D& top,
                           int& outMin,
                           int& outMax,
                           QVector<int> &outValues);

private :
    /*!
     * \brief threshInWindow
     *
     * Seuille une sous image (si la valeur est inferieure au seuil donne on affecte une valeur precise)
     *
     * \param bot : point inferieur de la sous-image
     * \param top : point superieur de la sous-image
     * \param thresh : valeur du seuil
     * \param threshedValue : valeur a affecter aux pixels seuilles
     */
    void threshInWindow(LSIS_Pixel4D& bot,
                        LSIS_Pixel4D& top,
                        int thresh,
                        int threshedValue );

    /*!
     * \brief threshWindowWithRatioAndOtsu
     *
     * Pour une sous-image, cette methode applique le filtre par ratio et le filtre d'otsu dessus (c.f. methodologie espace de Hough en points et normales)
     *
     * \param bot : point inferieur de la sous-image
     * \param top : point superieur de la sous-image
     * \param ratioThreshCoeff : coefficient corresteur a appliquer au seul par ratio
     * \param otsuThreshCoeff  : coefficient corresteur a appliquer au seul d'Otsu
     * \param nbBinsInHistogram : nombre de colonnes a creer pour un histigramme l'histogramme
     */
    void threshWindowWithRatioAndOtsu(LSIS_Pixel4D& bot,
                                      LSIS_Pixel4D& top,
                                      float ratioThreshCoeff,
                                      float otsuThreshCoeff,
                                      int nbBinsInHistogram);

    /*!
     * \brief threshWithRatioAndOtsuForGivenW
     *
     * Fais passer les filtres/seuil d'otsu et de ratio sur toutes les fenetres 3D d'une dimension W fixee
     *
     * \param ratioThreshCoeff : coefficient corresteur a appliquer au seul par ratio
     * \param otsuThreshCoeff  : coefficient corresteur a appliquer au seul d'Otsu
     * \param nbBinsInHistogram : nombre de colonnes a creer pour un histigramme l'histogramme
     */
public:
    void threshWithRatioAndOtsuForGivenW(int w,
                                         float ratioThreshCoeff,
                                         float otsuThreshCoeff,
                                         int nbBinsInHistogram);
private:

public :
    /*!
     * \brief threshWithRatioAndOtsu
     *
     * Fais passer les filtres et seuils d'otsu et de ratio sur tout l'espace de Hough
     *
     * \param ratioThreshCoeff : coefficient corresteur a appliquer au seul par ratio
     * \param otsuThreshCoeff  : coefficient corresteur a appliquer au seul d'Otsu
     * \param nbBinsInHistogram : nombre de colonnes a creer pour un histigramme l'histogramme
     */
    void threshWithRatioAndOtsu(float ratioThreshCoeff,
                                float otsuThreshCoeff,
                                int nbBinsInHistogram);

//    void threshWithRatioAndOtsuParallel(float ratioThreshCoeff,
//                                float otsuThreshCoeff,
//                                int nbBinsInHistogram);

    /*!
     * \brief threshByValue
     *
     * Seuille l'espace de hough avec une valeur donnee :
     * les pixels dont la valeur est inferieure au seuil sont filtres
     *
     * \param thresh : valeur du seuil
     */
    void threshByValue( float thresh );

/* **************************************************************** */
/* Outils d'etapes                                                  */
/* **************************************************************** */
    /*!
     * \brief step
     *
     * Getter
     *
     * \return
     */
    inline LSIS_DebugTool* step() { return _step; }

    /*!
     * \brief setStep
     *
     * Setter
     *
     * \param step
     */
    inline void setStep( LSIS_DebugTool* step ) { _step = step; }

private :
    LSIS_DebugTool*    _step; /*!< Pour faire en sorte qu'un espace de hough puisse mettre une etape en debug ou faire avancer sa barre de progression */
};

class ParallelClass
{
public :
    ParallelClass()
    {
    }

    ParallelClass( LSIS_Beam4D beam, LSIS_TraversalAlgorithm4D<int>* traversal ) :
        _beam( beam ),
        _traversal( traversal )
    {
    }

    LSIS_Beam4D                     _beam;
    LSIS_TraversalAlgorithm4D<int>* _traversal;
};

class ParallelObjectOtsu
{
public :
    ParallelObjectOtsu()
    {
    }

    ParallelObjectOtsu(LSIS_HoughSpace4D* hs, int w, float ratioT, float otsuT, int nBins ) :
        _hs( hs ),
        _w( w ),
        _ratioThreshCoeff( ratioT ),
        _otsuThreshCoeff( otsuT ),
        _nbBinsInHistogra( nBins )
    {
    }

    LSIS_HoughSpace4D*  _hs;
    int _w;
    float _ratioThreshCoeff;
    float _otsuThreshCoeff;
    int _nbBinsInHistogra;
};

void runAlgoOnParallelObject( ParallelClass parallelObject );
void runOtsuThreshOnParrallel( ParallelObjectOtsu parallelObject );

#endif // LSIS_HOUGHSPACE4D_H
