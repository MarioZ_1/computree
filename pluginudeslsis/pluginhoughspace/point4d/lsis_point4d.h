/************************************************************************************
* Filename :  lsis_point4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_POINT4D_H
#define LSIS_POINT4D_H

#include <Eigen/Core>

class LSIS_Pixel4D;

/*!
 * \brief The LSIS_Point4DInt class
 *
 * A 4D point with integer coordinate
 * This class inherits from Eigen::Vector4i
 * It is inspired from the CT_Point class
 */
class LSIS_Point4DInt : public Eigen::Vector4i
{
public:
    /*!
     * \brief LSIS_Point4DInt
     *
     * Default constructor
     */
    inline LSIS_Point4DInt(void) : Eigen::Vector4i()
    {
    }

    /*!
     * \brief LSIS_Point4DInt
     *
     * Initializing constructor
     */
    inline LSIS_Point4DInt( int w, int x, int y, int z ) : Eigen::Vector4i( w, x, y, z )
    {
    }

    /*!
     * \brief LSIS_Point4DInt
     *
     * This constructor allows you to construct LSIS_Point4DInt from Eigen expressions
     */
    template<typename OtherDerived>
    inline LSIS_Point4DInt (const Eigen::MatrixBase<OtherDerived>& other) : Eigen::Vector4i( other )
    {
    }

    inline int& w()
    {
        return (*this)(0);
    }

    inline int& x()
    {
        return (*this)(1);
    }

    inline int& y()
    {
        return (*this)(2);
    }

    inline int& z()
    {
        return (*this)(3);
    }

    inline int w() const
    {
        return (*this)(0);
    }

    inline int x() const
    {
        return (*this)(1);
    }

    inline int y() const
    {
        return (*this)(2);
    }

    inline int z() const
    {
        return (*this)(3);
    }

    /*!
     * \brief operator=
     *
     * This method allows you to assign Eigen expressions to LSIS_Point4DInt
     */
    template<typename OtherDerived>
    inline LSIS_Point4DInt & operator= (const Eigen::MatrixBase <OtherDerived>& other)
    {
        Eigen::Vector4i::operator=(other);
        return *this;
    }

    inline void setw(const int& w )
    {
        (*this)(0) = w;
    }

    inline void setx(const int& x )
    {
        (*this)(1) = x;
    }

    inline void sety(const int& y )
    {
        (*this)(2) = y;
    }

    inline void setz(const int& z )
    {
        (*this)(3) = z;
    }

    inline void set(const int& w, const int& x, const int& y, const int& z)
    {
        setw(w);
        setx(x);
        sety(y);
        setz(z);
    }

    inline double norm3D()
    {
        return std::sqrt( x()*x() + y()*y() + z()*z() );
    }

    inline static double distance3D( LSIS_Point4DInt const & p1, LSIS_Point4DInt const & p2 )
    {
        LSIS_Point4DInt diff = p2 - p1;
        return diff.norm3D();
    }

    inline bool isInBBox( LSIS_Point4DInt& bot, LSIS_Point4DInt& top )
    {
        if( w() < bot.w() ||
            x() < bot.x() ||
            y() < bot.y() ||
            z() < bot.z() ||
            w() > top.w() ||
            x() > top.x() ||
            y() > top.y() ||
            z() > top.z() )
        {
            return false;
        }

        return true;

    }
};

/*!
 * \brief The LSIS_Point4DFloat class
 *
 * A 4D point with integer coordinate
 * This class inherits from Eigen::Vector4f
 * It is inspired from the CT_Point class
 */
class LSIS_Point4DFloat : public Eigen::Vector4f
{
public:
    /*!
     * \brief LSIS_Point4DFloat
     *
     * Default constructor
     */
    inline LSIS_Point4DFloat(void) : Eigen::Vector4f()
    {
    }

    /*!
     * \brief LSIS_Point4DFloat
     *
     * Initializing constructor
     */
    inline LSIS_Point4DFloat( float w, float x, float y, float z ) : Eigen::Vector4f( w, x, y, z )
    {
    }

    /*!
     * \brief LSIS_Point4DFloat
     *
     * This constructor allows you to construct LSIS_Point4DFloat from Eigen expressions
     */
    template<typename OtherDerived>
    inline LSIS_Point4DFloat (const Eigen::MatrixBase<OtherDerived>& other) : Eigen::Vector4f( other )
    {
    }

    inline  float& w()
    {
        return (*this)(0);
    }

    inline  float& x()
    {
        return (*this)(1);
    }

    inline  float& y()
    {
        return (*this)(2);
    }

    inline  float& z()
    {
        return (*this)(3);
    }

    inline float w() const
    {
        return (*this)(0);
    }

    inline float x() const
    {
        return (*this)(1);
    }

    inline float y() const
    {
        return (*this)(2);
    }

    inline float z() const
    {
        return (*this)(3);
    }

    /*!
     * \brief operator=
     *
     * This method allows you to assign Eigen expressions to LSIS_Point4DFloat
     */
    template<typename OtherDerived>
    inline LSIS_Point4DFloat & operator= (const Eigen::MatrixBase <OtherDerived>& other)
    {
        Eigen::Vector4f::operator=(other);
        return *this;
    }

    inline void setw(const  float& w )
    {
        (*this).w() = w;
    }

    inline void setx(const  float& x )
    {
        (*this).x() = x;
    }

    inline void sety(const  float& y )
    {
        (*this).y() = y;
    }

    inline void setz(const  float& z )
    {
        (*this).z() = z;
    }

    inline void set(const  float& w, const  float& x, const  float& y, const  float& z)
    {
        setw(w);
        setx(x);
        sety(y);
        setz(z);
    }

    inline double norm3D() const
    {
        return std::sqrt( x()*x() + y()*y() + z()*z() );
    }

    inline static double distance3D( LSIS_Point4DFloat const & p1, LSIS_Point4DFloat const & p2 )
    {
        LSIS_Point4DFloat diff = p2 - p1;
        return diff.norm3D();
    }

    inline static double distance4D( LSIS_Point4DFloat const & p1, LSIS_Point4DFloat const & p2 )
    {
        LSIS_Point4DFloat diff = p2 - p1;
        return diff.norm();
    }

    inline float scalarProduct3D( LSIS_Point4DFloat const & p ) const
    {
        return ( x()*p.x() + y()*p.y() + z()*p.z() );
    }

    inline float scalarProduct4D( LSIS_Point4DFloat const & p ) const
    {
        return ( w()*p.w() + x()*p.x() + y()*p.y() + z()*p.z() );
    }

    inline float angle3DDegres( LSIS_Point4DFloat const & p ) const
    {
        float scalProd3D = scalarProduct3D( p );
        return ( (180.0/M_PI) * (acos( scalProd3D /= ( p.norm3D() * norm3D() ) )) );
    }

    LSIS_Point4DFloat operator- ( LSIS_Point4DFloat const & p ) const;

    LSIS_Point4DFloat operator+= ( LSIS_Pixel4D const & p );

    static LSIS_Point4DFloat composanteParallele( LSIS_Point4DFloat& directionSurLaquelleProjetter, LSIS_Point4DFloat& vecAProjetter )
    {
        LSIS_Point4DFloat directionNormalisee = directionSurLaquelleProjetter / directionSurLaquelleProjetter.norm();
        float prodScalaire = directionNormalisee.scalarProduct4D( vecAProjetter );
        return ( directionNormalisee * prodScalaire );
    }
};

/*!
 * \brief The LSIS_Point4DDouble class
 *
 * A 4D point with integer coordinate
 * This class inherits from Eigen::Vector4i
 * It is inspired from the CT_Point class
 */
class LSIS_Point4DDouble : public Eigen::Vector4d
{
public:
    /*!
     * \brief LSIS_Point4DDouble
     *
     * Default constructor
     */
    inline LSIS_Point4DDouble(void) : Eigen::Vector4d()
    {
    }

    /*!
     * \brief LSIS_Point4DDouble
     *
     * Initializing constructor
     */
    inline LSIS_Point4DDouble( double w, double x, double y, double z ) : Eigen::Vector4d( w, x, y, z )
    {
    }

    /*!
     * \brief LSIS_Point4DDouble
     *
     * This constructor allows you to construct LSIS_Point4DDouble from Eigen expressions
     */
    template<typename OtherDerived>
    inline LSIS_Point4DDouble (const Eigen::MatrixBase<OtherDerived>& other) : Eigen::Vector4d( other )
    {
    }

    inline double& w()
    {
        return (*this)(0);
    }

    inline double& x()
    {
        return (*this)(1);
    }

    inline double& y()
    {
        return (*this)(2);
    }

    inline double& z()
    {
        return (*this)(3);
    }

    inline double w() const
    {
        return (*this)(0);
    }

    inline double x() const
    {
        return (*this)(1);
    }

    inline double y() const
    {
        return (*this)(2);
    }

    inline double z() const
    {
        return (*this)(3);
    }

    /*!
     * \brief operator=
     *
     * This method allows you to assign Eigen expressions to LSIS_Point4DDouble
     */
    template<typename OtherDerived>
    inline LSIS_Point4DDouble & operator= (const Eigen::MatrixBase <OtherDerived>& other)
    {
        Eigen::Vector4d::operator=(other);
        return *this;
    }

    inline void setw(const double& w )
    {
        (*this)(0) = w;
    }

    inline void setx(const double& x )
    {
        (*this)(0) = x;
    }

    inline void sety(const double& y )
    {
        (*this)(0) = y;
    }

    inline void setz(const double& z )
    {
        (*this)(0) = z;
    }

    inline void set(const double& w, const double& x, const double& y, const double& z)
    {
        setw(w);
        setx(x);
        sety(y);
        setz(z);
    }

    inline double norm3D()
    {
        return std::sqrt( x()*x() + y()*y() + z()*z() );
    }

    inline static double distance3D( LSIS_Point4DDouble const & p1, LSIS_Point4DDouble const & p2 )
    {
        LSIS_Point4DDouble diff = p2 - p1;
        return diff.norm3D();
    }
};

#endif // LSIS_POINT4D_H
