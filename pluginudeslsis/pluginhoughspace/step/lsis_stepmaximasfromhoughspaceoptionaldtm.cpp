/************************************************************************************
* Filename :  lsis_stepmaximasfromhoughspaceoptionaldtm.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_stepmaximasfromhoughspaceoptionaldtm.h"

#include "ct_itemdrawable/ct_grid2dxy.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"

#include "../houghspace/lsis_houghspace4d.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "streamoverload/streamoverload.h"

// Alias for indexing models
#define DEFin_rsltHoughSpace "rsltHoughSpace"
#define DEFin_grpHoughSpace "grpHoughSpace"
#define DEFin_itmHoughSpace "itmHoughSpace"

#define DEFout_rsltMaximas "rsltMaximas"
#define DEFout_grpMaximas "grpMaximas"
#define DEFout_itmMaximas "itmMaximas"

// Alias for indexing models
#define DEFin_rsltOptionalDtm "rsltOptionalDtm"
#define DEFin_grpOptionalDtm "grpOptionalDtm"
#define DEFin_itmOptionalDtm "itmOptionalDtm"

// Constructor : initialization of parameters
LSIS_StepMaximasFromHoughSpaceOptionalDtm::LSIS_StepMaximasFromHoughSpaceOptionalDtm(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepMaximasFromHoughSpaceOptionalDtm::getStepDescription() const
{
    return tr("Extrait les maximas d'un espace de Hough avec MNT optionnel");
}

// Step detailled description
QString LSIS_StepMaximasFromHoughSpaceOptionalDtm::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepMaximasFromHoughSpaceOptionalDtm::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepMaximasFromHoughSpaceOptionalDtm::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepMaximasFromHoughSpaceOptionalDtm(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepMaximasFromHoughSpaceOptionalDtm::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOptionalDtm = createNewInResultModelForCopy(DEFin_rsltOptionalDtm, tr("Optional DTM resule"), tr(""), true);
    resIn_rsltOptionalDtm->setZeroOrMoreRootGroup();
    resIn_rsltOptionalDtm->addGroupModel("", DEFin_grpOptionalDtm, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_rsltOptionalDtm->addItemModel(DEFin_grpOptionalDtm, DEFin_itmOptionalDtm, CT_Grid2DXY<float>::staticGetType(), tr("DTM"), tr(""), CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);

    CT_InResultModelGroup *resIn_rsltHoughSpace = createNewInResultModel(DEFin_rsltHoughSpace, tr("HoughSpace"), tr(""), true);
    resIn_rsltHoughSpace->setRootGroup( DEFin_grpHoughSpace, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmHoughSpace, LSIS_HoughSpace4D::staticGetType(), tr("HoughSpace"));
}

// Creation and affiliation of OUT models
void LSIS_StepMaximasFromHoughSpaceOptionalDtm::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltMaximas = createNewOutResultModel(DEFout_rsltMaximas, tr("Maximas"));
    res_rsltMaximas->setRootGroup(DEFout_grpMaximas, new CT_StandardItemGroup(), tr("Group"));
    res_rsltMaximas->addItemModel(DEFout_grpMaximas, DEFout_itmMaximas, new CT_Grid4D<int>(), tr("Hough Space filtred"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepMaximasFromHoughSpaceOptionalDtm::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("min z", "(m)", 0.0001, 100, 4, _minz, 1);
    configDialog->addDouble("max z", "(m)", 0.0002, 100, 4, _maxz, 1);
}

void LSIS_StepMaximasFromHoughSpaceOptionalDtm::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltDtm = inResultList.at(0);
    CT_ResultGroup* resIn_rsltHoughSpace = inResultList.at(1);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _outResultHoughSpaceMaximas = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpDtm(resIn_rsltDtm, this, DEFin_grpOptionalDtm);
    CT_ResultGroupIterator itIn_grpHoughSpace(resIn_rsltHoughSpace, this, DEFin_grpHoughSpace);
    while (itIn_grpDtm.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpDtm = (CT_AbstractItemGroup*) itIn_grpDtm.next();
        const CT_AbstractItemGroup* grpIn_grpHoughSpace = (CT_AbstractItemGroup*) itIn_grpHoughSpace.next();

        const CT_Grid2DXY<float>* itemIn_itmDtm = (CT_Grid2DXY<float>*)grpIn_grpDtm->firstItemByINModelName(this, DEFin_itmOptionalDtm);
        const LSIS_HoughSpace4D* itemIn_itmHoughSpace = (LSIS_HoughSpace4D*)grpIn_grpHoughSpace->firstItemByINModelName(this, DEFin_itmHoughSpace);
        if (itemIn_itmHoughSpace != NULL)
        {
            compute( itemIn_itmHoughSpace, itemIn_itmDtm );
        }
    }
}

void LSIS_StepMaximasFromHoughSpaceOptionalDtm::compute(const LSIS_HoughSpace4D* houghSpace, const CT_Grid2DXY<float>* mnt )
{
    float minw = houghSpace->minW();
    float minx = houghSpace->minX();
    float miny = houghSpace->minY();
    float minz = houghSpace->minZ();
    float maxw = houghSpace->maxW();
    float maxx = houghSpace->maxX();
    float maxy = houghSpace->maxY();
    float maxz = houghSpace->maxZ();
    float resr = houghSpace->wres();
    float resx = houghSpace->xres();
    float resy = houghSpace->yres();
    float resz = houghSpace->zres();

    // On cree un espace de Hough a partir de la bounding box du nuage de points
    CT_Grid4D<int>* hsFiltered = new CT_Grid4D<int>( DEFout_itmMaximas, _outResultHoughSpaceMaximas,
                                                     minw,
                                                     minx,
                                                     miny,
                                                     minz,
                                                     maxw,
                                                     maxx,
                                                     maxy,
                                                     maxz,
                                                     resr,
                                                     resx,
                                                     resy,
                                                     resz,
                                                     -std::numeric_limits<int>::max(),
                                                     0,
                                                     true);

    // Declare le tableau qui contiendra les maximas
    QVector< LSIS_Pixel4D >* maximas;

    if( mnt != NULL )
    {
        maximas = houghSpace->getLocalMaximasInHeighRangeOverDTM( mnt, _minz, _maxz );
    }

    else
    {
        // Calcul du niveau z correspondant a _minz a partir du sol (bot de la bbox)
        size_t minLevZ;
        if( !houghSpace->levZ( _minz + minz , minLevZ) )
        {
            PS_LOG->addErrorMessage( PS_LOG->itemdrawable, "La grille ne contient pas la hauteur " + QString::number(_minz) );
            qDebug() << "La grille ne contient pas la hauteur " << _minz;
            qDebug() << "Espace de Hough : ";
            qDebug() << houghSpace;
            return;
        }

        // Calcul du niveau z correspondant a _maxz
        size_t maxLevZ;
        if( !houghSpace->levZ( _maxz + minz , maxLevZ  ) )
        {
            PS_LOG->addErrorMessage( PS_LOG->itemdrawable, "La grille ne contient pas la hauteur " + QString::number(_minz) );
            qDebug() << "La grille ne contient pas la hauteur " << _maxz;
            qDebug() << "Espace de Hough : ";
            qDebug() << houghSpace;
            return;
        }

        // On limite la bbox de recherche par ces deux valeurs
        LSIS_Pixel4D bot( 0, 0, 0, minLevZ );
        LSIS_Pixel4D top( houghSpace->wdim(), houghSpace->xdim(), houghSpace->ydim(), maxLevZ );

        // On recupere les maximas dans cette bounding box
        maximas = houghSpace->getLocalMaximasInBBox( bot, top );
    }

    PS_LOG->addInfoMessage( PS_LOG->step, QString::number( maximas->size() ) + " maximas recuperes" );
    qDebug() << maximas->size() << " maximas locaux recuperes";

    // On remplit l'espace des maximas avec ces pixels
    foreach( LSIS_Pixel4D p, *maximas )
    {
        p.setValueIn( hsFiltered, p.valueIn(houghSpace) );
    }

    // Mise a jour du min max pour la grille de sortie
    hsFiltered->computeMinMax();

    // On attache les resultats
    // OUT results creation (move it to the appropried place in the code)
    CT_StandardItemGroup* grp_grpMaximas = new CT_StandardItemGroup(DEFout_grpMaximas, _outResultHoughSpaceMaximas);
    grp_grpMaximas->addItemDrawable( hsFiltered );
    _outResultHoughSpaceMaximas->addGroup(grp_grpMaximas);
}
