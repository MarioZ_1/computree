/************************************************************************************
* Filename :  lsis_pixel4ddecrescentvaluesorter.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_PIXEL4DDECRESCENTVALUESORTER_H
#define LSIS_PIXEL4DDECRESCENTVALUESORTER_H

#include "pixel/lsis_pixel4d.h"

template< typename DataImage >
struct LSIS_Pixel4DDecrescentValueSorter
{
    /*!
     * \brief LSIS_Pixel4DDecrescentValueSorter
     * Constructeur
     * \param image : image sur laquelle les pixels sont tries
     */
    inline LSIS_Pixel4DDecrescentValueSorter( CT_Grid4D_Sparse<DataImage> const * image ) : _image(image)
    {
    }

    /*!
     * \brief operator ()
     *
     * Comparateur de pixels utilise dans l'algorithme de tri de la bibliotheque std
     *
     * \param p1 : pixel a comparer
     * \param p2 : pixel a comparer
     * \return vrai si p1 a une plus petite valeur que p2
     */
    inline bool operator()( LSIS_Pixel4D const * p1, LSIS_Pixel4D const * p2)
    {
        return this->operator ()( *p1, *p2 );
    }

    /*!
     * \brief operator ()
     *
     * Comparateur de pixels utilise dans l'algorithme de tri de la bibliotheque std
     *
     * \param p1 : pixel a comparer
     * \param p2 : pixel a comparer
     * \return vrai si p1 a une plus petite valeur que p2
     */
    inline bool operator()( LSIS_Pixel4D const & p1, LSIS_Pixel4D const & p2)
    {
        return ( p1.valueIn(_image) > p2.valueIn(_image) );
    }

/* **************************************************************** */
/* Champs de la structure                                           */
/* **************************************************************** */
    /*!
     * \brief image
     * Image sur laquelle on trie les pixels selon leur valeur
     */
    CT_Grid4D_Sparse<DataImage> const * _image;
};

template< typename DataT >
QVector< LSIS_Pixel4D >* sortPixelsByValueDecrescentOrder ( QVector< LSIS_Pixel4D >* pixels, CT_Grid4D<DataT> const * image );

// Include template functions implementation
#include "lsis_pixel4ddecrescentvaluesorter.hpp"

#endif // LSIS_PIXEL4DDECRESCENTVALUESORTER_H
