CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

include($${CT_PREFIX}/all_check_dependencies.pri) # Just verify if there is problems with dependencies, do nothing else
include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)


TARGET = plug_normales

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    norm_pluginentry.h \
    norm_pluginmanager.h \
    step/norm_stepcomputesigma.h \
    tools/norm_acp.h \
    tools/norm_leastsquarefitting.h \
    grid/norm_pointgrid.h \
    grid/norm_pairint.h \
    octree/norm_octreenode.h \
    octree/norm_octreettools.h \
    octree/norm_octree.h \
    step/norm_stepcreatestdoctree.h \
    tools/norm_interpolation.h \
    step/norm_stepoctreenodefromquerrypoint.h \
    step/norm_stepneighbournodesfromquerrypoint.h \
    step/norm_stepsphericalneighbourhood.h \
    step/norm_stepapproximatesphericalneighbourhood.h \
    octree/norm_octreedrawmanager.h \
    itemdrawable/norm_sphere.h \
    itemdrawable/norm_standardspheredrawmanager.h \
    itemdrawable/norm_box.h \
    itemdrawable/norm_standardboxdrawmanager.h \
    step/norm_stepgeneratesphere.h \
    step/norm_stepgeneratebox.h \
    step/norm_stepfilterpca.h \
    step/norm_stepcomputenormalspca.h \
    grid/norm_pointgriddrawmanager.h \
    octree/norm_octreenodepca.h \
    octree/norm_abstractoctree.h \
    octree/norm_abstractnode.h \
    octree/norm_octreestd.h \
    tools/norm_boundingbox.h \
    octree/norm_octreepca.h \
    step/norm_stepcreateoctreepca.h \
    exporter/icro_ascnormexporter.h \
    step/norm_stepfilteronsigmaoctree.h \
    step/norm_stepfilteronsigmaoctreeandestimatenormals.h \
    newoctree/norm_newoctree.h \
    newoctree/norm_newoctreenode.h \
    newoctree/norm_newoctreedrawmanager.h \
    step/norm_stepcreatenewoctree.h \
    step/norm_stepgetnewoctreenodecontainingpoint.h \
    step/norm_stepgetnewoctreeneighbournodesfromquerrypoint.h \
    step/norm_stepsegmentfromnewoctree.h \
    streamoverload/streamoverload.h \
    streamoverload/streamoverload.hpp \
    new/norm_newoctreenodev2.h \
    new/norm_newoctreev2.h \
    new/norm_newoctreedrawmanagerv2.h \
    new/streamoverloads.h \
    step/norm_newstepcreateoctree.h \
    step/norm_newstepfittsurfaces.h \
    step/norm_newstepcomputenormalswithoutinterpolation.h \
    step/norm_newstepgetprojectedpoints.h \
    step/norm_newstepcomparenormals.h \
    step/norm_newstepquerrynodefrompoint.h \
    step/norm_newstepcomputeleavesneighbours.h \
    step/norm_newstepsamplepointonnodeface.h \
    step/norm_newsteporientsurfaces.h \
    step/norm_newstepcomputenormalswithlinearinterpolation.h \
    step/norm_newstepcomputenormalswithwendlandinterpolation.h \
    step/norm_newstepgetprojectedpointswithlinearinterpolation.h \
    exporter/norm_octreeexporter.h \
    step/norm_newsteploadoctree.h \
    step/norm_newstepgetprojectedpointsfromfile.h \
    step/norm_newstepcomparesetofnormals.h

SOURCES += \
    norm_pluginentry.cpp \
    norm_pluginmanager.cpp \
    step/norm_stepcomputesigma.cpp \
    tools/norm_acp.cpp \
    tools/norm_leastsquarefitting.cpp \
    grid/norm_pointgrid.cpp \
    grid/norm_pairint.cpp \
    octree/norm_octreenode.cpp \
    octree/norm_octreettools.cpp \
    octree/norm_octree.cpp \
    step/norm_stepcreatestdoctree.cpp \
    tools/norm_interpolation.cpp \
    step/norm_stepoctreenodefromquerrypoint.cpp \
    step/norm_stepneighbournodesfromquerrypoint.cpp \
    step/norm_stepsphericalneighbourhood.cpp \
    step/norm_stepapproximatesphericalneighbourhood.cpp \
    octree/norm_octreedrawmanager.cpp \
    itemdrawable/norm_sphere.cpp \
    itemdrawable/norm_standardspheredrawmanager.cpp \
    itemdrawable/norm_box.cpp \
    itemdrawable/norm_standardboxdrawmanager.cpp \
    step/norm_stepgeneratesphere.cpp \
    step/norm_stepgeneratebox.cpp \
    step/norm_stepfilterpca.cpp \
    step/norm_stepcomputenormalspca.cpp \
    grid/norm_pointgriddrawmanager.cpp \
    octree/norm_octreenodepca.cpp \
    octree/norm_abstractoctree.cpp \
    octree/norm_abstractnode.cpp \
    octree/norm_octreestd.cpp \
    tools/norm_boundingbox.cpp \
    octree/norm_octreepca.cpp \
    step/norm_stepcreateoctreepca.cpp \
    exporter/icro_ascnormexporter.cpp \
    step/norm_stepfilteronsigmaoctree.cpp \
    step/norm_stepfilteronsigmaoctreeandestimatenormals.cpp \
    newoctree/norm_newoctree.cpp \
    newoctree/norm_newoctreenode.cpp \
    newoctree/norm_newoctreedrawmanager.cpp \
    step/norm_stepcreatenewoctree.cpp \
    step/norm_stepgetnewoctreenodecontainingpoint.cpp \
    step/norm_stepgetnewoctreeneighbournodesfromquerrypoint.cpp \
    step/norm_stepsegmentfromnewoctree.cpp \
    streamoverload/streamoverload.cpp \
    new/norm_newoctreenodev2.cpp \
    new/norm_newoctreev2.cpp \
    new/norm_newoctreedrawmanagerv2.cpp \
    step/norm_newstepcreateoctree.cpp \
    step/norm_newstepfittsurfaces.cpp \
    step/norm_newstepcomputenormalswithoutinterpolation.cpp \
    step/norm_newstepgetprojectedpoints.cpp \
    step/norm_newstepcomparenormals.cpp \
    step/norm_newstepquerrynodefrompoint.cpp \
    step/norm_newstepcomputeleavesneighbours.cpp \
    step/norm_newstepsamplepointonnodeface.cpp \
    step/norm_newsteporientsurfaces.cpp \
    step/norm_newstepcomputenormalswithlinearinterpolation.cpp \
    step/norm_newstepcomputenormalswithwendlandinterpolation.cpp \
    step/norm_newstepgetprojectedpointswithlinearinterpolation.cpp \
    exporter/norm_octreeexporter.cpp \
    step/norm_newsteploadoctree.cpp \
    step/norm_newstepgetprojectedpointsfromfile.cpp \
    step/norm_newstepcomparesetofnormals.cpp

TRANSLATIONS += languages/pluginnormales_en.ts \
                languages/pluginnormales_fr.ts

INCLUDEPATH += .
