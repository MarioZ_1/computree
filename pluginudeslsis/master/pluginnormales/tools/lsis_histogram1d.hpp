/************************************************************************************
* Filename :  lsis_histogram1d.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_HISTOGRAM1D_HPP
#define LSIS_HISTOGRAM1D_HPP

#include "lsis_histogram1d.h"

// Utilise de l'affichage en debug
#include <QDebug>

// Utilise les limites numeriques
#include <limits>

// Utilise ceil et floor
#include <math.h>

template< typename DataT >
LSIS_Histogram1D<DataT>::LSIS_Histogram1D(float minVal, float maxVal, float res, bool nBins ) : _min(minVal), _max(maxVal)
{
    if ( nBins )
    {
        _nBins = res;
        _res = (_max - _min) / res;
    }

    else
    {
        _res = res;
        _nBins = ceil( (maxVal - minVal) / res );
    }

    _nEntries = 0;
    _bins = QVector<DataT>(_nBins, 0);
}

template< typename DataT >
LSIS_Histogram1D<DataT>::LSIS_Histogram1D(float minVal, float maxVal, float res, bool nBins, QVector<DataT> &values) :
    _nEntries (values.size()),
    _min(minVal),
    _max(maxVal)
{
    if( maxVal == minVal )
    {
        _nBins = 1;
        _res = 0;
        _bins.append(0);
        _bins[0] = _nEntries;
    }

    else
    {
        if ( nBins )
        {
            _nBins = res;
            _res = (double)(_max - _min) / _nBins;
        }

        else
        {
            _res = res;
            _nBins = ceil( (_max - _min) / res );
        }

        for ( int i = 0 ; i < _nBins ; i++ )
        {
            _bins.append(0);
        }

        // The index of a bin for a value v is calculated as
        // (v - min) / res
        int insertIndex = 0;
        int nValues = values.size();
        for ( int i = 0 ; i < nValues ; i++ )
        {
            insertIndex = floor( ( values.at(i) - _min ) / _res);

            if( insertIndex == _nBins )
            {
                _bins[ _nBins-1 ]++;
            }

            else
            {
                _bins[insertIndex]++;
            }
        }
    }
}

template< typename DataT >
LSIS_Histogram1D<DataT>::~LSIS_Histogram1D()
{
    _bins.clear();
}

template< typename DataT >
LSIS_Histogram1D<DataT>* LSIS_Histogram1D<DataT>::getCumulative()
{
    LSIS_Histogram1D<DataT>* rslt = new LSIS_Histogram1D<DataT>( _min, _max, _res, false );

    rslt->_nEntries = _nEntries;
    rslt->_nBins = _nBins;
    rslt->_bins[0] = _bins[0];
    for ( int i = 1 ; i < _nBins ; i++ )
    {
        rslt->_bins[i] = rslt->_bins[i-1] + _bins[i];
    }

    return rslt;
}

template< typename DataT >
QVector<DataT> *LSIS_Histogram1D<DataT>::getXTiles(int x)
{
    QVector<int>* rslt = new QVector<int>( x );

    float xTileSize = (float)_nEntries / (float)x;
    int cumulative = 0;
    int previousXTile = 0;
    for ( int i = 0 ; i < _nBins ; i++ )
    {
        cumulative += _bins[i];
        int currentXTile = floor( cumulative / xTileSize );

        if( currentXTile == x )
        {
            (*rslt)[x-1] = i;
            return rslt;
        }

        if ( currentXTile != previousXTile )
        {
            for( int j = previousXTile ; j <= currentXTile ; j++ )
            {
                (*rslt)[j] = i;
            }
            previousXTile = currentXTile;
        }
    }

    return rslt;
}

template< typename DataT >
int LSIS_Histogram1D<DataT>::getIthXTiles(int x, int i)
{
    return (_min + getXTiles(x)->at(i) * _res);
}

template< typename DataT >
QVector<DataT> *LSIS_Histogram1D<DataT>::getCentilesFromCumulative(int x)
{
    QVector<int>* rslt = new QVector<int>( x );

    float xTileSize = (float)_nEntries / (float)x;
    int previousXTile = 0;
    for ( int i = 0 ; i < _nBins ; i++ )
    {
        int currentXTile = floor( _bins[i] / xTileSize );

        if ( currentXTile != previousXTile )
        {
            (*rslt)[currentXTile] = i;
            previousXTile = currentXTile;
        }
    }

    return rslt;
}

template< typename DataT >
void LSIS_Histogram1D<DataT>::print( QString bef, QString aft )
{
    float startBin;
    float endBin;
    qDebug() << bef;
    printInfos();
    for ( int i = 0 ; i < _nBins ; i++ )
    {
        startBin = _min + (i*_res);
        endBin = startBin + _res;
        qDebug() << "Bin " << i << "[" << startBin << "," << endBin << "] a une valeur de " << _bins[i];
    }
    qDebug() << aft;
}

template< typename DataT >
void LSIS_Histogram1D<DataT>::printInfos(QString bef, QString aft)
{
    qDebug() << bef;
    qDebug() << "Min = " << _min << " Max = " << _max << " Res = " << _res << " NBins = " << _nBins << " NEntries = " << _nEntries;
    qDebug() << "Nombre effectif de bins " << _bins.size();
    qDebug() << aft;
}

template< typename DataT >
float LSIS_Histogram1D<DataT>::otsuThreshold()
{
    if( _nBins == 1 )
    {
        // Une seule valeur dans l'histogramme on renvoie cette valeur moins un
        // ce qui est le seuil le plus bas le plus proche de la valeur contenue dans l'histogramme
        return _min-1;
    }

    // Otsu threshold :
    float wBackground = 0;
    float wForeground = 0;
    float backgroundSum = 0;
    float foregroundSum = 0;
    float meanBackground = 0;
    float meanForeground = 0;
    float interVariance = 0;
    float maxInterVariance = -std::numeric_limits<float>::max();
    float bestThresh = 0;

    // Initialise la meanForegroundSum
    // a la somme totale des p(i)*x(i)
    for ( int i = 0 ; i < _nBins ; i++ )
    {
        foregroundSum += _bins[i] * (_min + (_res/2.0) + ( i * _res ) );
    }

    // On parcourt tous les seuils possibles (tous les bins de l'histogramme)
    for ( int i = 0 ; i < _nBins ; i++ )
    {
        // Mise a jour du poids background
        wBackground += _bins[i];

        // Mise a jour du poids foreground
        wForeground = _nEntries - wBackground;

        // Mise a jour de la moyenne du background
        backgroundSum += _bins[i] * (_min + (_res/2.0) + ( i * _res ) ) ;
        meanBackground = backgroundSum / wBackground;

        // Mise a jour de la moyenne du foregrounds
        meanForeground = ( foregroundSum - backgroundSum ) / wForeground;

        // Calcul de l'inter-variance pour le seuil courant
        interVariance = wForeground * wBackground * (meanBackground - meanForeground) * (meanBackground - meanForeground);

        // On retient la variance inter-classe maximale
        if ( interVariance > maxInterVariance )
        {
            if( i == _nBins )
            {
                bestThresh = (_min + (_nBins-1) * _res );
            }

            else
            {
                bestThresh = (_min + ( (i+1) * _res ) );
            }

            maxInterVariance = interVariance;
        }
    }

    return bestThresh;
}

template< typename DataT >
float LSIS_Histogram1D<DataT>::meanOfClass(float classMinValue, float classMaxValue)
{
    float mean = 0;
    int startBin = indexOfValue(classMinValue);
    int endBin = indexOfValue(classMaxValue);
    int nPixelsInClass = 0;

//    qDebug() << "Min max " << classMinValue << classMaxValue << " start end " << startBin << endBin << "size hist " << _nBins;
    for ( int i = startBin ; i < endBin ; i++ )
    {
        mean += ( centerOfBin(i) * getValueAtBin(i) );
        nPixelsInClass += getValueAtBin(i);
    }

    if( nPixelsInClass != 0 )
    {
        mean /= nPixelsInClass;
    }

    return mean;
}

template< typename DataT >
void LSIS_Histogram1D<DataT>::meanOfClasses( float threshold, float& outMeanLowClass, float& outMeanHighClass )
{
//    qDebug() << "Hisotgramme = " << (*this);
    // Calcul pour la classe basse
    outMeanLowClass = meanOfClass( _min, threshold );

    // Calcul pour la classe haute
    outMeanHighClass = meanOfClass( threshold, _max );

    if( outMeanLowClass == 0 || outMeanHighClass == 0 )
    {
        outMeanLowClass = 0;
        outMeanHighClass = 0;
    }
}


#endif // LSIS_HISTOGRAM1D_HPP
