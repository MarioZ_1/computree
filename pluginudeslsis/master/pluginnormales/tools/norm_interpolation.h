#ifndef NORM_INTERPOLATION_H
#define NORM_INTERPOLATION_H

#include "ct_point.h"

namespace NORM_Tools
{
    namespace NORM_Interpolation
    {
        /*!
         * \brief linearInterpolation
         *
         * Linear interpolation of a value into a range
         *
         * \param xa : lower bound of the input range
         * \param xb : upper bound of the input range
         * \param ya : lower bound of the output range
         * \param yb : upper bound of the output range
         * \param valToInterpolate : value to interpolate from [xa,xb] to [ya,yb]
         *
         * \return the interpolated value
         */
        float linearInterpolation( float xa, float xb,
                                   float ya, float yb,
                                   float valToInterpolate );

        /*!
         * \brief linearInterpolation
         *
         * Interpolates a point in 3D
         * Executes three independent interpolations (one for each coordinate of the point)
         *
         * \param inputLower : lower point of the input range
         * \param inputUpper : lower point of the input range
         * \param outputLower : lower point of the input range
         * \param outputUpper : lower point of the input range
         * \param pointToInterpolate : point to interpolate from [inputLower, inputUpper] to [outputLower, outputUpper]
         * \param outInterpolatedPoint : the interpolated point
         */
        void linearInterpolation( const CT_Point& inputLower, const CT_Point& inputUpper,
                                  const CT_Point& outputLower, const CT_Point& outputUpper,
                                  const CT_Point& pointToInterpolate,
                                  CT_Point& outInterpolatedPoint );

        /*!
         * \brief wendlandCoeff
         *
         * Renvoie la valeur de la fonction de wendland pour un espace a trois dimensions et de continuite C2 pour un support donne
         * C.f. http://www.csee.umbc.edu/~rheingan/pubs/smi2001.pdf
         *
         * Interpolating Implicit Surfaces From Scattered Surface Data Using Compactly Supported Radial Basis Functions
         * Bryan S. Morse
         *
         * \param support : taille du support compact
         * \param radius : rayon courant pour lequel on cherche la valeur de la fonction de wendland
         *
         * \return
         */
        float wendlandCoeff( float support, float radius );
    }
}

#endif // NORM_INTERPOLATION_H
