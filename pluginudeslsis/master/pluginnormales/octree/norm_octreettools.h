#ifndef NORM_OCTREETTOOLS_H
#define NORM_OCTREETTOOLS_H

#include "ct_point.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_defines.h"

namespace NORM_OctreeTools
{
    /*!
     * \brief sortPointsFromPivot
     *
     * Sort points from a sub cloud according to a pivot value and a chosen axis:
     * All points with value lower than the pivot value will be left-sided in the output index cloud
     * all points with a greater value will be right-sided n the output index cloud
     *
     * \warning The input scene is not modified since we don't want to mess with the original point cloud
     * \warning The output index cloud has to be previously created and initialized
     *
     * Let's have an index cloud iCloud to be sorted between indices a and b
     * then the result value of this algorithm will be v such as :
     * - all indices in [a,b] lower than (v are left)left-sided) are lower than the pivot value
     * - all indices in [a,b] lower than (v are left)left-sided) are greater or equal to the pivot value
     *
     * \param sortFirstIndex : first index of the sub cloud to be sorted
     * \param sortLastIndex : last index of the subcloud to be sorted
     * \param pivot : pivot location in 3d space
     * \param sortAxis : axis of the pivot to take into account
     * \param outSortedIndexCloudPtr : sorted index cloud
     * \return the index of the hyper plane separation (see detailled explanations above)
     */
    size_t sortPointsFromPivot( size_t sortFirstIndex,
                                size_t sortLastIndex,
                                float pivot,
                                int sortAxis,
                                CT_PointCloudIndexVector* outSortedIndexCloudPtr );

    size_t sortPointsFromPivot(size_t sortFirstIndex,
                                size_t sortLastIndex,
                                float pivot,
                                int sortAxis,
                                CT_MPCIR outSortedIndexCloudPtr );

    size_t sortPointsFromPivot(size_t sortFirstIndex,
                                size_t sortLastIndex,
                                float pivot,
                                int sortAxis,
                                CT_AbstractModifiableCloudIndexT<CT_PointData>* outSortedIndexCloudPtr );

    /*!
     * \brief sortMortonOrder
     *
     * Sorts a set of points in morton order.
     * This method calls the sortPointFromPivot
     * \warning The input scene is not modified, only the output point cloud is
     *
     * \param sortFirstIndex : first index of the points to be sorted
     * \param sortLastIndex : last index of the points to be sorted
     * \param center : pivot location in 3d space
     * \param blocs : array of values being the benchmarks between each of the octants made by the pivot point
     * \param outSortedIndexCloudPtr : the index cloud sorted ni morton order in [sortFirstIndex ; sortLastIndex]
     */
    void sortMortonOrder ( size_t sortFirstIndex,
                           size_t sortLastIndex,
                           const CT_Point& center,
                           size_t blocs[],
                           CT_PointCloudIndexVector* outSortedIndexCloudPtr);

    void sortMortonOrder ( size_t sortFirstIndex,
                           size_t sortLastIndex,
                           const CT_Point& center,
                           size_t blocs[],
                           CT_MPCIR outSortedIndexCloudPtr);

    /*!
     * \brief getBoundingCubeFromBoundingBox
     *
     * Calculates the bounding cube of a given point cloud (different from the bounding box).
     * The bottom left point of the point cloud is kept as the bottom left point of the bounding cube
     *
     * \param bboxBot : bottom left point of the bounding box of the point cloud
     * \param bboxTop : top right point of the bounding box of the point cloud
     * \param outBcBot : resulting bottom left point of the bounding cube
     * \param outBcTop : resulting top rigth point of the bounding cube
     */
    void getBoundingCubeFromBoundingBox ( const CT_Point& bboxBot, const CT_Point& bboxTop, CT_Point& outBcBot, CT_Point& outBcTop );
}

#endif // NORM_OCTREETTOOLS_H
