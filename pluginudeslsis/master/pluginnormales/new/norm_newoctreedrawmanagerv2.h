#ifndef NORM_NEWOCTREEDRAWMANAGERV2_H
#define NORM_NEWOCTREEDRAWMANAGERV2_H

// Inherits from this class
#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class NORM_NewOctreeDrawManagerV2 : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
     * \brief NORM_NewOctreeDrawManager
     *
     * Constructor of the NORM_NewOctreeDrawManager class
     *
     * \param drawConfigurationName : name of the configuration
     */
    NORM_NewOctreeDrawManagerV2(QString drawConfigurationName = "");

//********************************************//
//                Drawing tools               //
//********************************************//
    /*!
     * \brief draw
     *
     * Draw an octree : draw each of the octree nodes box
     *
     * \param view : graphic view
     * \param painter : painter to paint objects in the view
     * \param itemDrawable : octree to be drawn
     */
    virtual void draw(GraphicsViewInterface& view,
                      PainterInterface& painter,
                      const CT_AbstractItemDrawable& itemDrawable) const;

//**********************************************//
//          Drawing configuration tools         //
//**********************************************//
    static QString staticInitConfigDisplayBoxes();
    static QString staticInitConfigDisplayCentroid();
    static QString staticInitConfigDisplayV3Pca();
    static QString staticInitConfigSizeV3Pca();

//**********************************************//
//                  Attributes                  //
//**********************************************//
protected :
    // Drawing configuration
    const static QString INDEX_CONFIG_DISPLAY_BOXES;
    const static QString INDEX_CONFIG_DISPLAY_CENTROID;
    const static QString INDEX_CONFIG_DISPLAY_ACP_V3;
    const static QString INDEX_CONFIG_SIZE_ACP_V3;

protected :
// ******************************************** //
// Configuration tools                          //
// Member attributes                            //
// ******************************************** //
    /*!
     * \brief createDrawConfiguration
     *
     * Creates the draw configuration menu (displayed in the draw configuration window in computree)
     *
     * \param drawConfigurationName : draw configuration name
     *
     * \return the draw configuration
     */
    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;
};

#endif // NORM_NEWOCTREEDRAWMANAGERV2_H
