#include "norm_stepneighbournodesfromquerrypoint.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

#include "octree/norm_octreestd.h"

// Alias for indexing models
#define DEFin_Result "Result"
#define DEFin_Group "Group"
#define DEFin_ItemIndexCloud "ItemIndexCloud"

#define DEFout_Result "Result"
#define DEFout_GroupGeneral "GroupGeneral"
#define DEFout_GroupNeiNodes "GroupNeiNodes"
#define DEFout_GroupSingleNeighbour "GroupSingleNeighbour"
#define DEFout_ItemNeighbourScene "ItemNeighbourScene"
#define DEFout_GroupQuerryNode "GroupQuerryNode"
#define DEFout_ItemQuerryScene "ItemQuerryScene"
#define DEFout_ItemAttributeCloudIdNode "ItemAttributeCloudIdNode"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;

// Constructor : initialization of parameters
NORM_StepNeighbourNodesFromQuerryPoint::NORM_StepNeighbourNodesFromQuerryPoint(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _querryx = 0;
    _querryy = 0;
    _querryz = 0;
    _minNodeWidth = 0.01;
    _minNbPoints = 10;
}

// Step description (tooltip of contextual menu)
QString NORM_StepNeighbourNodesFromQuerryPoint::getStepDescription() const
{
    return tr("Cherche la cellule a laquelle appartient un point ainsi que ses voisins");
}

// Step detailled description
QString NORM_StepNeighbourNodesFromQuerryPoint::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepNeighbourNodesFromQuerryPoint::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepNeighbourNodesFromQuerryPoint::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepNeighbourNodesFromQuerryPoint(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepNeighbourNodesFromQuerryPoint::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_Result = createNewInResultModel(DEFin_Result, tr("inResult"));
    resIn_Result->setRootGroup(DEFin_Group, CT_AbstractItemGroup::staticGetType(), tr("inGroup"));
    resIn_Result->addItemModel(DEFin_Group, DEFin_ItemIndexCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("inItemIndexCloud"));

}

// Creation and affiliation of OUT models
void NORM_StepNeighbourNodesFromQuerryPoint::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_Result = createNewOutResultModel(DEFout_Result, tr("Result"));
    res_Result->setRootGroup(DEFout_GroupGeneral, new CT_StandardItemGroup(), tr("Groupe general"));
    res_Result->addGroupModel(DEFout_GroupGeneral, DEFout_GroupNeiNodes, new CT_StandardItemGroup(), tr("Group Neighbour Nodes"));
    res_Result->addGroupModel(DEFout_GroupNeiNodes, DEFout_GroupSingleNeighbour, new CT_StandardItemGroup(), tr("Simple Voisin"));
    res_Result->addItemModel(DEFout_GroupSingleNeighbour, DEFout_ItemNeighbourScene, new CT_Scene(), tr("Noeud Voisin"));
    res_Result->addGroupModel(DEFout_GroupGeneral, DEFout_GroupQuerryNode, new CT_StandardItemGroup(), tr("Group Querry Node"));
    res_Result->addItemModel(DEFout_GroupQuerryNode, DEFout_ItemQuerryScene, new CT_Scene(), tr("Noeud demande"));
    res_Result->addItemModel(DEFout_GroupGeneral, DEFout_ItemAttributeCloudIdNode, new AttributEntier(), tr("Coloration de l'octree"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepNeighbourNodesFromQuerryPoint::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("x", "", -9999, 9999, 4, _querryx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _querryy, 1);
    configDialog->addDouble("", "", -9999, 9999, 4, _querryz, 1);
    configDialog->addDouble("MinNodeWidth", "", 0.0001, 100, 4, _minNodeWidth, 1);
    configDialog->addInt("NbPtsMin", "", 1, 9999, _minNbPoints);
}

void NORM_StepNeighbourNodesFromQuerryPoint::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_Result = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_Group);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();
        
        const CT_AbstractItemDrawableWithPointCloud* itemIn_ItemWithPointCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_Group->firstItemByINModelName(this, DEFin_ItemIndexCloud);
        CT_PCIR inputIndexCloud = itemIn_ItemWithPointCloud->getPointCloudIndexRegistered();

        if (itemIn_ItemWithPointCloud != NULL)
        {
            // OUT results creation (move it to the appropried place in the code)
            CT_StandardItemGroup* grp_GroupGeneral = new CT_StandardItemGroup(DEFout_GroupGeneral, res_Result);
            CT_StandardItemGroup* grp_GroupNeiNodes = new CT_StandardItemGroup(DEFout_GroupNeiNodes, res_Result);
            res_Result->addGroup(grp_GroupGeneral);
            grp_GroupGeneral->addGroup( grp_GroupNeiNodes );

            // On cree l'octree
            CT_Point bot = createCtPoint( itemIn_ItemWithPointCloud->minX(), itemIn_ItemWithPointCloud->minY(), itemIn_ItemWithPointCloud->minZ() );
            CT_Point top = createCtPoint( itemIn_ItemWithPointCloud->maxX(), itemIn_ItemWithPointCloud->maxY(), itemIn_ItemWithPointCloud->maxZ() );
            NORM_OctreeStd octree( _minNodeWidth, _minNbPoints, bot, top, inputIndexCloud );
            octree.createOctree();

            // On fait la recherche du noeud contenant le point
            CT_Point querryPoint = createCtPoint(_querryx, _querryy, _querryz );
            NORM_OctreeNode* node = octree.nodeContainingPoint( querryPoint );

            // On cree une scene a partir des points de ce noeud
            CT_PointCloudIndexVector* outIndexVector = node->getIndexCloud();

            // Calcul de la bounding box
            CT_PointAccessor pAccessor;
            CT_Point curPoint;
            float xmin = std::numeric_limits<float>::max();
            float xmax = -std::numeric_limits<float>::max();
            float ymin = std::numeric_limits<float>::max();
            float ymax = -std::numeric_limits<float>::max();
            float zmin = std::numeric_limits<float>::max();
            float zmax = -std::numeric_limits<float>::max();
            size_t nbPoints = outIndexVector->size();
            for( size_t i = 0 ; i < nbPoints ; i++ )
            {
                pAccessor.pointAt( outIndexVector->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }

            // Creation de la scene
            CT_PCIR outIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexVector );
            CT_Scene* item_outItemScene = new CT_Scene(DEFout_ItemQuerryScene, res_Result);
            item_outItemScene->setPointCloudIndexRegistered( outIndexVectorRegistered );

            // On ajoute cette scene aux resultats
            CT_StandardItemGroup* grp_GroupQuerryNode = new CT_StandardItemGroup(DEFout_GroupQuerryNode, res_Result);
            grp_GroupGeneral->addGroup(grp_GroupQuerryNode);
            grp_GroupQuerryNode->addItemDrawable(item_outItemScene);

            // On recupere les noeuds voisins de ce noeud d'interet
            QVector< NORM_AbstractNode* > neighbourNodes;
            octree.neighboursLeafNodes( node, neighbourNodes );

            // Et on cree une scene pour chacun d'eux
            foreach ( NORM_AbstractNode* currNei, neighbourNodes )
            {
                NORM_OctreeNode* currNeiStd = (NORM_OctreeNode*)currNei;
                // On cree une scene a partir des points de ce noeud
                CT_PointCloudIndexVector* curIndexVector = currNeiStd->getIndexCloud();
                CT_PCIR curIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( curIndexVector );
                CT_Scene* item_curItemScene = new CT_Scene(DEFout_ItemNeighbourScene, res_Result);
                item_curItemScene->setPointCloudIndexRegistered( curIndexVectorRegistered );

                // On ajoute cette scene aux resultats
                CT_StandardItemGroup* grp_GroupSingleNei = new CT_StandardItemGroup(DEFout_GroupSingleNeighbour, res_Result);
                grp_GroupNeiNodes->addGroup(grp_GroupSingleNei);
                grp_GroupSingleNei->addItemDrawable(item_curItemScene);
            }

            // On fait pareil pour la couleur des points dans l'octree
            AttributEntier* item_AttributeCloudIdNode = octree.getIdAttribute( DEFout_ItemAttributeCloudIdNode, res_Result );
            grp_GroupGeneral->addItemDrawable(item_AttributeCloudIdNode);
        }
    }
}
