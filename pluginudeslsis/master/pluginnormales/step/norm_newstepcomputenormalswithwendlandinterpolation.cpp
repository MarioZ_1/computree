#include "norm_newstepcomputenormalswithwendlandinterpolation.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "new/norm_newoctreev2.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/ct_scene.h"

#include <QElapsedTimer>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"
#define DEFin_itmScene "itmScene"

// Constructor : initialization of parameters
NORM_NewStepComputeNormalsWithWendlandInterpolation::NORM_NewStepComputeNormalsWithWendlandInterpolation(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepComputeNormalsWithWendlandInterpolation::getStepDescription() const
{
    return tr("3ter - Calcule les normales en utilisant une interpolation par RBF de Wendland");
}

// Step detailled description
QString NORM_NewStepComputeNormalsWithWendlandInterpolation::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepComputeNormalsWithWendlandInterpolation::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepComputeNormalsWithWendlandInterpolation::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepComputeNormalsWithWendlandInterpolation(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepComputeNormalsWithWendlandInterpolation::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmScene, CT_Scene::staticGetType(), tr("Input Scene"));
}

// Creation and affiliation of OUT models
void NORM_NewStepComputeNormalsWithWendlandInterpolation::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelNormals, new CT_PointsAttributesNormal(), tr("Normals [Wendland]"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepComputeNormalsWithWendlandInterpolation::createPostConfigurationDialog()
{
//    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void NORM_NewStepComputeNormalsWithWendlandInterpolation::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmScene );
        if ( itemIn_itmOctree != NULL )
        {
            QElapsedTimer timer;
            timer.start();

            CT_PointsAttributesNormal* normals = itemIn_itmOctree->getNormalsAttributeWithWendlandInterpolation( _autoRenameModelNormals.completeName(),
                                                                                                                 res_RsltOctree,
                                                                                                                 itemIn_itmScene->getPointCloudIndexRegistered() );
            qDebug() << "Temps de calcul des normales " << timer.elapsed();

            grpIn_grpOctree->addItemDrawable( normals );
        }
    }
}
