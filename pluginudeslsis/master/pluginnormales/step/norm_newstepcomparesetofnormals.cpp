#include "norm_newstepcomparesetofnormals.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/ct_scene.h"

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include <QFileInfo>
#include <QDir>
#include <QDebug>

// Alias for indexing out models
#define DEF_rsltComparison "rsltComparison"
#define DEF_grpComparison "grpComparison"
#define DEF_itmScene "itmScene"
#define DEF_itmAngle "itmAngle"

#define NORM_NewStepCompareSetOfNormals_epsilon 1e-6

// Constructor : initialization of parameters
NORM_NewStepCompareSetOfNormals::NORM_NewStepCompareSetOfNormals(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepCompareSetOfNormals::getStepDescription() const
{
    return tr("00 - Compare les normales calculees par plusieurs methodes sur un meme nuage de points");
}

// Step detailled description
QString NORM_NewStepCompareSetOfNormals::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepCompareSetOfNormals::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepCompareSetOfNormals::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepCompareSetOfNormals(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepCompareSetOfNormals::createInResultModelListProtected()
{
}

// Creation and affiliation of OUT models
void NORM_NewStepCompareSetOfNormals::createOutResultModelListProtected()
{
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepCompareSetOfNormals::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "File 1", CT_FileChoiceButton::OneOrMoreExistingFiles, "*.*", _fileList );
}

void NORM_NewStepCompareSetOfNormals::compute()
{
    if( _fileList.size() < 2 )
    {
        qDebug() << "Erreur : Il manque un fichier a selectionner (au moins deux)";
        return;
    }

    int nFiles = _fileList.size();
    for( int i = 0 ; i < nFiles - 1 ; i++ )
    {
        for( int j = i+1 ; j < nFiles ; j++ )
        {
            QString file1Name = _fileList.at(i);
            int nChampsFile1 = -1;
            int nHeaderLinesFile1 = -1;
            getFileFormat( file1Name, nChampsFile1, nHeaderLinesFile1 );
            qDebug() << "Format 1 : nChamps " << nChampsFile1 << "HeaderLines = " << nHeaderLinesFile1;

            QString file2Name = _fileList.at(j);
            int nChampsFile2 = -1;
            int nHeaderLinesFile2 = -1;
            getFileFormat( file2Name, nChampsFile2, nHeaderLinesFile2 );
            qDebug() << "Format 2 : nChamps " << nChampsFile2 << "HeaderLines = " << nHeaderLinesFile2;

            QFileInfo fileInfo1( file1Name );
            QFileInfo fileInfo2( file2Name );
            QString outputLogFile = fileInfo1.absolutePath() + "/" + fileInfo1.baseName() + "_" + fileInfo2.baseName() + ".log";

            compareTwoFiles( file1Name, nChampsFile1, nHeaderLinesFile1,
                             file2Name, nChampsFile2, nHeaderLinesFile2,
                             outputLogFile );
        }
    }
}

void NORM_NewStepCompareSetOfNormals::getFileFormat(QString fileName, int& outnChamps, int& outNHeaderLines )
{
    QFile file( fileName );
    if( !file.open( QIODevice::ReadOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier" << fileName;
        return;
    }

    QTextStream stream( &file );

    QString firstLine = stream.readLine();

    QStringList listWords = firstLine.split(" ", QString::SkipEmptyParts);

    // Regarde si le premier mot est bien un nombre pour compter le nombre de lignes d'entete
    outNHeaderLines = 0;
    bool isNumber = false;
    listWords.at( outNHeaderLines ).toFloat( &isNumber );

    qDebug() << "Read " << fileName;
    qDebug() << "First line header ? " << !isNumber;

    while( !isNumber )
    {
        outNHeaderLines++;

        listWords = stream.readLine().split(" ", QString::SkipEmptyParts);
        listWords.first().toFloat( &isNumber );
    }

    // Compte le nombre de champs
    outnChamps = listWords.size();

    qDebug() << "NChamps " << outnChamps << " base sur la ligne lue : " << listWords;

    file.close();
}

void NORM_NewStepCompareSetOfNormals::compareTwoFiles(QString file1Name,
                                                      int nChampsFile1,
                                                      int nHeaderLinesFile1,
                                                      QString file2Name,
                                                      int nChampsFile2,
                                                      int nHeaderLinesFile2,
                                                      QString outputFileName)
{
    qDebug() << "NChamps = " << nChampsFile1 << nChampsFile2;
    assert( nChampsFile1 == 6 || nChampsFile1 == 9 );
    assert( nChampsFile2 == 6 || nChampsFile2 == 9 );
    QFile file1( file1Name );
    if( !file1.open( QIODevice::ReadOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier" << file1Name;
        return;
    }
    QFile file2( file2Name );
    if( !file2.open( QIODevice::ReadOnly ) )
    {
        file1.close();
        qDebug() << "Erreur lors de l'ouverture du fichier" << file2Name;
        return;
    }
    QFile logFile( outputFileName );
    if( !logFile.open( QIODevice::WriteOnly ) )
    {
        file1.close();
        file2.close();
        qDebug() << "Erreur lors de l'ouverture du fichier" << outputFileName;
        return;
    }

    QTextStream streamf1( &file1 );
    QTextStream streamf2( &file2 );
    QTextStream streamflog( &logFile );
    int nLines = 0;

    float x, y, z;
    float nx1, ny1, nz1;
    float nx2, ny2, nz2;
    QString line1;
    QStringList listWords1;
    QString line2;
    QStringList listWords2;

    // Skip lines
    for( int i = 0 ; i < nHeaderLinesFile1 ; i++ )
    {
        streamf1.readLine();
        nLines++;
    }
    for( int i = 0 ; i < nHeaderLinesFile2 ; i++ )
    {
        streamf2.readLine();
    }

    int nValidAngles = 0;
    while( !streamf1.atEnd() )
    {
        if( streamf2.atEnd() )
        {
            file1.close();
            file2.close();

            qDebug() << "Erreur : Les fichiers n'ont pas le meme nombre de lignes" << nLines;

            return;
        }

        line1 = streamf1.readLine();
        listWords1 = line1.split(" ", QString::SkipEmptyParts);
        assert( nChampsFile1 == listWords1.size() );
        if( nChampsFile1 == 6 )
        {
            if( listWords1.size() != 6 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 1 ?";
                qDebug() << "Ligne lue" << line1;
                qDebug() << "Soit " << listWords1.size() << " champs (6 attendus)";
                return;
            }

            x = listWords1.at(0).toFloat();
            y = listWords1.at(1).toFloat();
            z = listWords1.at(2).toFloat();
            nx1 = listWords1.at(3).toFloat();
            ny1 = listWords1.at(4).toFloat();
            nz1 = listWords1.at(5).toFloat();
        }
        else if( nChampsFile1 == 9 )
        {
            if( listWords1.size() != 9 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 1 ?";
                qDebug() << "Ligne lue" << line1;
                qDebug() << "Soit " << listWords1.size() << " champs (9 attendus)";
                return;
            }

            x = listWords1.at(0).toFloat();
            y = listWords1.at(1).toFloat();
            z = listWords1.at(2).toFloat();
            nx1 = listWords1.at(6).toFloat();
            ny1 = listWords1.at(7).toFloat();
            nz1 = listWords1.at(8).toFloat();
        }
        else
        {
            qDebug() << "Erreur dans le type de fichier on quitte";
            return;
        }

        line2 = streamf2.readLine();
        listWords2 = line2.split(" ", QString::SkipEmptyParts);
        assert( nChampsFile2 == listWords2.size() );
        if( nChampsFile2 == 6 )
        {
            if( listWords2.size() != 6 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 2 ?";
                qDebug() << "Ligne lue" << line2;
                qDebug() << "Soit " << listWords2.size() << " champs (6 attendus)";
                return;
            }


            nx2 = listWords2.at(3).toFloat();
            ny2 = listWords2.at(4).toFloat();
            nz2 = listWords2.at(5).toFloat();
        }
        else if( nChampsFile2 == 9 )
        {
            if( listWords2.size() != 9 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 2 ?";
                qDebug() << "Ligne lue" << line2;
                qDebug() << "Soit " << listWords2.size() << " champs (9 attendus)";
                return;
            }

            nx2 = listWords2.at(6).toFloat();
            ny2 = listWords2.at(7).toFloat();
            nz2 = listWords2.at(8).toFloat();
        }
        else
        {
            qDebug() << "Erreur dans le type de fichier on quitte";
            return;
        }

        // Augmente le nombre de lignes lues
        nLines++;

        // Calcule l'angle entre les deux normales
        float angle = angle3D( nx1, ny1, nz1,
                               nx2, ny2, nz2 );

        if( angle >= 0 && angle <= 90 )
        {
            streamflog << angle << "\n";
            nValidAngles++;
        }
    }

    if( !streamf2.atEnd() )
    {
        file1.close();
        file2.close();

        qDebug() << "Erreur : Les fichiers n'ont pas le meme nombre de lignes (" << nLines << ")";
        qDebug() << streamf2.readLine();

        return;
    }

    file1.close();
    file2.close();
}

float NORM_NewStepCompareSetOfNormals::angle3D(float x1, float y1, float z1,
                                          float x2, float y2, float z2)
{
    float scalarProduct = x1*x2 + y1*y2 + z1*z2;
    float norm1 = sqrt(x1*x1 + y1*y1 + z1*z1);
    float norm2 = sqrt(x2*x2 + y2*y2 + z2*z2);

    // Si on a une normale nulle alors on ne peut pas comparer
    if( norm1 == 0 || norm2 == 0 )
    {
        return -1;
    }

    // Calcul du cos selon le signe du produit scalaire pour obtenir un angle entre 0 et 90
    float cos;
    if( scalarProduct > 0 )
    {
        cos = scalarProduct / ( norm1 * norm2 );
    }
    else
    {
        cos = (-scalarProduct) / ( norm1 * norm2 );
    }

    // On s'assure qu'on est bien dans le range (on accepte les petites marges d'erreurs dues a la precision numerique)
    if( cos > 1 )
    {
        if( cos > 1 + NORM_NewStepCompareSetOfNormals_epsilon )
        {
            // Ne devrait pas arriver
            return -2;
        }
        else
        {
            cos = 1;
        }
    }

    if( cos < -1 )
    {
        if( cos < -1 - NORM_NewStepCompareSetOfNormals_epsilon )
        {
            // Ne devrait pas arriver
            return -2;
        }
        else
        {
            cos = -1;
        }
    }

    // On revient a l'angle
    float angleRad = acos( cos );

    // On transforme en degres
    return ( angleRad * 180.0 / M_PI );
}
