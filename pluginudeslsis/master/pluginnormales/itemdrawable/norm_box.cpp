#include "norm_box.h"

const NORM_StandardBoxDrawManager NORM_Box::BOX_DRAW_MANAGER;

CT_DEFAULT_IA_INIT(NORM_Box)

NORM_Box::NORM_Box()
    : CT_AbstractShape()
{
    setBaseDrawManager(&BOX_DRAW_MANAGER);
}

NORM_Box::NORM_Box(const CT_OutAbstractSingularItemModel *model,
                   const CT_AbstractResult *result,
                   CT_BoxData* data)
    : CT_AbstractShape(model, result, data)
{
    setBaseDrawManager(&BOX_DRAW_MANAGER);
}

NORM_Box::NORM_Box(const QString &modelName,
                   const CT_AbstractResult *result,
                   CT_BoxData* data)
    : CT_AbstractShape(modelName, result, data)
{
    setBaseDrawManager(&BOX_DRAW_MANAGER);
}

QString NORM_Box::getType() const
{
    return staticGetType();
}

QString NORM_Box::staticGetType()
{
    return CT_AbstractShape::staticGetType() + "/NORM_Box";
}

CT_AbstractItemDrawable* NORM_Box::copy(const CT_OutAbstractItemModel *model,
                                        const CT_AbstractResult *result,
                                        CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED(copyModeList);
    NORM_Box* box = new NORM_Box( (const CT_OutAbstractSingularItemModel *)model,
                                  result,
                                  (getPointerData() != NULL) ? ( (const CT_BoxData&)getData()).clone() : NULL );
    box->setId(id());
    box->setAlternativeDrawManager(getAlternativeDrawManager());

    return box;
}

CT_AbstractItemDrawable* NORM_Box::copy(const QString &modelName,
                                           const CT_AbstractResult *result,
                                           CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED(copyModeList);
    NORM_Box *box = new NORM_Box(modelName,
                                 result,
                                 (getPointerData() != NULL) ? ((const CT_BoxData&)getData()).clone() : NULL);
    box->setId(id());
    box->setAlternativeDrawManager(getAlternativeDrawManager());

    return box;
}
