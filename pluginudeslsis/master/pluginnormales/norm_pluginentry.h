#ifndef NORM_PLUGINENTRY_H
#define NORM_PLUGINENTRY_H

#include "interfaces.h"

class NORM_PluginManager;

class NORM_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    NORM_PluginEntry();
    ~NORM_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    NORM_PluginManager *_pluginManager;
};

#endif // NORM_PLUGINENTRY_H