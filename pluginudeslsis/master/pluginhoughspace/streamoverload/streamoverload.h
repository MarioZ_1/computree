/************************************************************************************
* Filename :  streamoverload.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef STREAMOVERLOAD_H
#define STREAMOVERLOAD_H

#include <iostream>
#include <QDebug>
#include <Eigen/Core>
#include <QTextStream>

class LSIS_Point4DInt;
class LSIS_Point4DFloat;
class LSIS_Point4DDouble;
class LSIS_Beam4D;

template < typename DataImage >
class CT_Grid4D;

template < typename DataImage >
class LSIS_ActiveContour4DContinuous;

template < typename DataImage >
class LSIS_Histogram1D;

QDebug operator<< (QDebug debug, LSIS_Point4DInt const & p );
QDebug operator<< (QDebug debug, LSIS_Point4DFloat const & p );
QDebug operator<< (QDebug debug, LSIS_Point4DDouble const & p );
QDebug operator<< (QDebug debug, LSIS_Beam4D const & beam );

template <typename DataT>
QDebug operator<< (QDebug debug, Eigen::DenseBase<DataT> const & m );

template <typename DataT>
QTextStream operator<< (QTextStream stream, Eigen::DenseBase<DataT> const & m );

template <typename DataT>
QDebug operator<< (QDebug debug, CT_Grid4D<DataT> const & grid );

template <typename DataT>
std::ostream& operator<< (std::ostream& stream, CT_Grid4D<DataT> const & grid );

template <typename DataImage>
QDebug operator<< (QDebug debug, LSIS_ActiveContour4DContinuous<DataImage> const & c );

template <typename DataImage>
std::ostream& operator<< (std::ostream& stream, LSIS_ActiveContour4DContinuous<DataImage> const & c );

template <typename DataT>
QDebug operator<< (QDebug stream, LSIS_Histogram1D<DataT> const & h );

template <typename DataT>
std::ostream& operator<< (std::ostream& stream, LSIS_Histogram1D<DataT> const & h );

// Inclusion des implementations templatees
#include "streamoverload.hpp"

#endif // STREAMOVERLOAD_H
