#ifndef LSIS_EFFECTIVEFILTERVISITOR_HPP
#define LSIS_EFFECTIVEFILTERVISITOR_HPP

#include "lsis_effectivefiltervisitor.h"

template < typename DataT >
LSIS_EffectiveFilterVisitor<DataT>::LSIS_EffectiveFilterVisitor(CT_Grid4D<DataT> *grid) :
    LSIS_AbstractVisitorGrid4D<DataT>( grid )
{
}

template < typename DataT >
LSIS_EffectiveFilterVisitor<DataT>::~LSIS_EffectiveFilterVisitor()
{
}

template < typename DataT >
void LSIS_EffectiveFilterVisitor<DataT>::visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam)
{
    Q_UNUSED(beam);
    LSIS_AbstractVisitorGrid4D<DataT>::_grid->setValue(levw, levx, levy, levz, 0);
}

#endif // LSIS_EFFECTIVEFILTERVISITOR_HPP
