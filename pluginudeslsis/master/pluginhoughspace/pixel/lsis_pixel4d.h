/************************************************************************************
* Filename :  lsis_pixel4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_PIXEL4D_H
#define LSIS_PIXEL4D_H

// Un pixel 4D est un point4D a coordonnees entieres (heritage)
#include "../point4d/lsis_point4d.h"

// Les pixels se deplacent et accedent a des informations de grilles 4D
#include "ct_itemdrawable/ct_grid4d.h"
#include "ct_itemdrawable/ct_grid4d_sparse.h"

// Utilise les assertions
#include "../assert/assertwithmessage.h"

// Conversion radian->degres
#define RAD_TO_DEG 180.0/M_PI

/* ******************************************************************************************************** */
/* Definitions de macros outils                                                                             */
/* ******************************************************************************************************** */
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une image par un pixel //////////////////////////////////////////// //
/*!
 * \def beginForAllPixelsInImage( pixel, dimw, dimx, dimy, dimz )
 *
 * Macro qui permet de parcourir tous les pixels d'une image de taille "dimw, dimx, dimy, dimz".
 * Elle est associee a la macro endForAllPixelsInImage( pixel, grid, dimw, dimx, dimy, dimz ).
 * Exemple :
 * beginForAllPixelsInImage( pixel, dimw, dimx, dimy, dimz )
 * {
 *   pixel.print();
 * }
 * endForAllPixelsInImage( pixel, grid, dimw, dimx, dimy, dimz )
*/
#define beginForAllPixelsImage( pixel, dimw, dimx, dimy, dimz ) \
for ( pixel.w() = 0 ; pixel.w() < dimw ; pixel.w()++ )\
{\
    for ( pixel.x() = 0 ; pixel.x() < dimx ; pixel.x()++ )\
    {\
        for ( pixel.y() = 0 ; pixel.y() < dimy ; pixel.y()++ )\
        {\
            for ( pixel.z() = 0 ; pixel.z() < dimz ; pixel.z()++ )\
            {\

/*!
 * \def endForAllPixelsInImage( pixel, dimw, dimx, dimy, dimz )
 * Macro associee a beginForAllPixelsInImage( pixel, dimw, dimx, dimy, dimz )
*/
#define endForAllPixelsImage( pixel, dimw, dimx, dimy, dimz ) \
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'un voisinage de pixel au sein d'un cone de recherche ////////////// //
/*!
  * \def beginForAllPixelsInCone( pixCentral direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone )
  * Macro qui permet de parcourir tous les pixels a l'interieur d'un cone d'appex pixCentral, de direction direction et d'angle angleMax au sein d'une image de taille "dimw, dimx, dimy, dimz".
  * Elle est associee a la macro endForForAllPixelsInCone( pixCentral, direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone )
*/
#define beginForAllPixelsInCone( pixCentral, direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone ) \
{\
    LSIS_Point4DFloat curDirInMacro; \
    LSIS_Pixel4D      curDirInMacroPixel; \
    float curAngleDegresInMacro; \
    for ( pixInCone.w() = pixCentral.w()-size ; pixInCone.w() <= pixCentral.w()+size ; pixInCone.w()++ )\
    {\
        for ( pixInCone.x() = pixCentral.x()-size ; pixInCone.x() <= pixCentral.x()+size ; pixInCone.x()++ )\
        {\
            for ( pixInCone.y() = pixCentral.y()-size ; pixInCone.y() <= pixCentral.y()+size ; pixInCone.y()++ )\
            {\
                for ( pixInCone.z() = pixCentral.z()-size ; pixInCone.z() <= pixCentral.z()+size ; pixInCone.z()++ )\
                {\
                    if ( pixInCone.w() >= 0 && pixInCone.w() < dimw && \
                         pixInCone.x() >= 0 && pixInCone.x() < dimx && \
                         pixInCone.y() >= 0 && pixInCone.y() < dimy && \
                         pixInCone.z() >= 0 && pixInCone.z() < dimz ) \
                    {\
                        /* Recupere la direction courante*/ \
                        curDirInMacroPixel = pixInCone - pixCentral; \
                        curDirInMacro.set( curDirInMacroPixel.w(), curDirInMacroPixel.x(), curDirInMacroPixel.y(), curDirInMacroPixel.z() ) ; \
                        /* Calcule l'angle entre la direction courante et la direction du cone */ \
                        curAngleDegresInMacro = curDirInMacro.angle3DDegres( direction ); \
                        /* Compare a l'angle maximum authorise */ \
                        if( curAngleDegresInMacro <= angleMax ) \
                        {


/*!
  * \def endForForAllPixelsInCone( pixCentral, direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone )
  * Macro associee a beginForAllPixelsInCone( pixCentral, direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone ) \
*/
#define endForAllPixelsInCone( pixCentral, direction, angleMax, size, dimw, dimx, dimy, dimz, pixInCone ) \
                        }\
                    }\
                }\
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'un voisinage de pixel ///////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro qui permet de parcourir le voisinage (dont la taille est donnee par size) d'un pixel dans une image de taille "dimw, dimx, dimy, dimz".
  * Elle est associee a la macro endForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Exemple :
  * Pixel4DIntPtr cur = new Pixel4DInt(1,5,4,3);
  * Pixel4DIntPtr nei = new Pixel4DInt();
  * // ... get the image dimension here ...
  * beginForAllPixelsNeighbours( cur, dimw, dimx, dimy, dimz, nei, 1 )
  * {
  *     nei.print();
  * }
  * endForAllPixelsNeighbours( cur, dimw, dimx, dimy, dimz, nei, 1 )
*/
#define beginForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
for ( neighbourPtr.w() = pixel.w()-size ; neighbourPtr.w() <= pixel.w()+size ; neighbourPtr.w()++ )\
{\
    for ( neighbourPtr.x() = pixel.x()-size ; neighbourPtr.x() <= pixel.x()+size ; neighbourPtr.x()++ )\
    {\
        for ( neighbourPtr.y() = pixel.y()-size ; neighbourPtr.y() <= pixel.y()+size ; neighbourPtr.y()++ )\
        {\
            for ( neighbourPtr.z() = pixel.z()-size ; neighbourPtr.z() <= pixel.z()+size ; neighbourPtr.z()++ )\
            {\
                if ( neighbourPtr.w() >= 0 && neighbourPtr.w() < dimw && \
                     neighbourPtr.x() >= 0 && neighbourPtr.x() < dimx && \
                     neighbourPtr.y() >= 0 && neighbourPtr.y() < dimy && \
                     neighbourPtr.z() >= 0 && neighbourPtr.z() < dimz && \
                     ( pixel.w() != neighbourPtr.w() || pixel.x() != neighbourPtr.x() || pixel.y() != neighbourPtr.y() || pixel.z() != neighbourPtr.z()  ) ) \
                {

/*!
  * \def endForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
                }\
            }\
        }\
    }\
}

#define beginForAllPixelsNeighbours3D( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
neighbourPtr.w() = pixel.w(); \
for ( neighbourPtr.x() = pixel.x()-size ; neighbourPtr.x() <= pixel.x()+size ; neighbourPtr.x()++ )\
{\
    for ( neighbourPtr.y() = pixel.y()-size ; neighbourPtr.y() <= pixel.y()+size ; neighbourPtr.y()++ )\
    {\
        for ( neighbourPtr.z() = pixel.z()-size ; neighbourPtr.z() <= pixel.z()+size ; neighbourPtr.z()++ )\
        {\
            if ( neighbourPtr.x() >= 0 && neighbourPtr.x() < dimx && \
                 neighbourPtr.y() >= 0 && neighbourPtr.y() < dimy && \
                 neighbourPtr.z() >= 0 && neighbourPtr.z() < dimz && \
                 ( pixel.x() != neighbourPtr.x() || \
                   pixel.y() != neighbourPtr.y() || \
                   pixel.z() != neighbourPtr.z()  ) ) \
            {

/*!
  * \def endForAllPixelsNeighbours3D( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsNeighbours3D( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsNeighbours3D( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'un voisinage de pixel ///////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsNeighboursAndCenter( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro equivalente a beginForAllPixelsNeighbours( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) mais qui parcours aussi le pixel central
*/
#define beginForAllPixelsNeighboursAndCenter( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
for ( neighbourPtr.w() = pixel.w()-size ; neighbourPtr.w() <= pixel.w()+size ; neighbourPtr.w()++ )\
{\
    for ( neighbourPtr.x() = pixel.x()-size ; neighbourPtr.x() <= pixel.x()+size ; neighbourPtr.x()++ )\
    {\
        for ( neighbourPtr.y() = pixel.y()-size ; neighbourPtr.y() <= pixel.y()+size ; neighbourPtr.y()++ )\
        {\
            for ( neighbourPtr.z() = pixel.z()-size ; neighbourPtr.z() <= pixel.z()+size ; neighbourPtr.z()++ )\
            {\
                if ( neighbourPtr.w() >= 0 && neighbourPtr.w() < dimw && \
                     neighbourPtr.x() >= 0 && neighbourPtr.x() < dimx && \
                     neighbourPtr.y() >= 0 && neighbourPtr.y() < dimy && \
                     neighbourPtr.z() >= 0 && neighbourPtr.z() < dimz )\
                {

/*!
  * \def endForAllPixelsNeighboursAndCenter( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsNeighboursAndCenter( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsNeighboursAndCenter( pixel, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
                }\
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une bounding box ////////////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr )
  * Macro qui permet de parcourir les pixels d'une image contenus dans une bounding box de coordonnees image
  * Elle est associee a la macro beginForAllPixelsInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
  * Exemple :
  * Pixel4DIntPtr cur = new Pixel4DInt();
  * Pixel4DIntPtr bot = new Pixel4DInt(10,10,10,10);
  * Pixel4DIntPtr top = new Pixel4DInt(20,20,20,20);
  * // ... get the image dimension here ...
  * // ... on parcours seulements les pixels compris entre (10,10,10,10) et (20,20,20,20) en coordonnees image
  * beginForAllPixelsInBBox( bot, top, dimw, dimx, dimy, dimz, cur )
  * {
  *     cur.print();
  * }
  * endForAllPixelsInBBox( bot, top, dimw, dimx, dimy, dimz, cur )
*/
#define beginForAllPixelsInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr ) \
for ( pixPtr.w()= botPtr.w() ; pixPtr.w() <= topPtr.w() ; pixPtr.w()++ )\
{\
    for ( pixPtr.x() = botPtr.x(); pixPtr.x() <= topPtr.x() ; pixPtr.x()++ )\
    {\
        for ( pixPtr.y() = botPtr.y() ; pixPtr.y() <= topPtr.y() ; pixPtr.y()++ )\
        {\
            for ( pixPtr.z() = botPtr.z() ; pixPtr.z() <= topPtr.z(); pixPtr.z()++ )\
            {\
                if ( pixPtr.w() >= 0 && pixPtr.w() < dimw && \
                     pixPtr.x() >= 0 && pixPtr.x() < dimx && \
                     pixPtr.y() >= 0 && pixPtr.y() < dimy && \
                     pixPtr.z() >= 0 && pixPtr.z() < dimz )\
                {

/*!
  * \def endForAllPixelsInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
  * Macro associee a beginForAllPixelsInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
*/
#define endForAllPixelsInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr ) \
                }\
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

#define beginForAllPixelsInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr ) \
for ( pixPtr.x() = botPtr.x(); pixPtr.x() <= topPtr.x() ; pixPtr.x()++ )\
{\
    for ( pixPtr.y() = botPtr.y() ; pixPtr.y() <= topPtr.y() ; pixPtr.y()++ )\
    {\
        for ( pixPtr.z() = botPtr.z() ; pixPtr.z() <= topPtr.z(); pixPtr.z()++ )\
        {\
             if( pixPtr.x() >= 0 && pixPtr.x() < dimx && \
                 pixPtr.y() >= 0 && pixPtr.y() < dimy && \
                 pixPtr.z() >= 0 && pixPtr.z() < dimz )\
            {

/*!
  * \def endForAllPixelsInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr )
  * Macro associee a beginForAllPixelsInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr )
*/
#define endForAllPixelsInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr ) \
            }\
        }\
    }\
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une unique dimension ////////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsInDim( pixPtr, idim, ndim )
  * Macro qui parcours tous les pixels d'une dimension donnee
  * idim est l'identifiant de cette dimension (0 pour w, 1 pour x, ... )
  * ndim est la taille de cette dimension (le nombre de pixels selon cet axe)
  */
#define beginForAllPixelsInDim( pixPtr, idim, ndim ) \
for ( (*pixPtr)[idim] = 0 ; (*pixPtr)[idim] < ndim ; (*pixPtr)[idim]++ )\
{\

/*!
  * \def endForAllPixelsInDim( pixPtr, idim, ndim )
  * Macro associee a la macro beginForAllPixelsInDim( pixPtr, idim, ndim )
  */
#define endForAllPixelsInDim( pixPtr, idim, ndim ) \
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une image par un pixel //////////////////////////////////////////// //
/*!
 * \def beginForAllPixelsPtrInImage( pixelPtr, dimw, dimx, dimy, dimz )
 *
 * Macro qui permet de parcourir tous les pixels d'une image de taille "dimw, dimx, dimy, dimz".
 * Elle est associee a la macro endForAllPixelsPtrInImage( pixelPtr, grid, dimw, dimx, dimy, dimz ).
 * Exemple :
 * beginForAllPixelsPtrInImage( pixelPtr, dimw, dimx, dimy, dimz )
 * {
 *   pixelPtr->print();
 * }
 * endForAllPixelsPtrInImage( pixelPtr, grid, dimw, dimx, dimy, dimz )
*/
#define beginForAllPixelsPtrImage( pixelPtr, dimw, dimx, dimy, dimz ) \
for ( pixelPtr->w() = 0 ; pixelPtr->w() < dimw ; pixelPtr->w()++ )\
{\
    for ( pixelPtr->x() = 0 ; pixelPtr->x() < dimx ; pixelPtr->x()++ )\
    {\
        for ( pixelPtr->y() = 0 ; pixelPtr->y() < dimy ; pixelPtr->y()++ )\
        {\
            for ( pixelPtr->z() = 0 ; pixelPtr->z() < dimz ; pixelPtr->z()++ )\
            {\

/*!
 * \def endForAllPixelsPtrInImage( pixelPtr, dimw, dimx, dimy, dimz )
 * Macro associee a beginForAllPixelsPtrInImage( pixelPtr, dimw, dimx, dimy, dimz )
*/
#define endForAllPixelsPtrImage( pixelPtr, dimw, dimx, dimy, dimz ) \
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'un voisinage de pixel ///////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro qui permet de parcourir le voisinage (dont la taille est donnee par size) d'un pixel dans une image de taille "dimw, dimx, dimy, dimz".
  * Elle est associee a la macro endForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Exemple :
  * Pixel4DIntPtr cur = new Pixel4DInt(1,5,4,3);
  * Pixel4DIntPtr nei = new Pixel4DInt();
  * // ... get the image dimension here ...
  * beginForAllPixelsPtrNeighbours( cur, dimw, dimx, dimy, dimz, nei, 1 )
  * {
  *     nei->print();
  * }
  * endForAllPixelsPtrNeighbours( cur, dimw, dimx, dimy, dimz, nei, 1 )
*/
#define beginForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
for ( neighbourPtr->w() = pixelPtr->w()-size ; neighbourPtr->w() <= pixelPtr->w()+size ; neighbourPtr->w()++ )\
{\
    for ( neighbourPtr->x() = pixelPtr->x()-size ; neighbourPtr->x() <= pixelPtr->x()+size ; neighbourPtr->x()++ )\
    {\
        for ( neighbourPtr->y() = pixelPtr->y()-size ; neighbourPtr->y() <= pixelPtr->y()+size ; neighbourPtr->y()++ )\
        {\
            for ( neighbourPtr->z() = pixelPtr->z()-size ; neighbourPtr->z() <= pixelPtr->z()+size ; neighbourPtr->z()++ )\
            {\
                if ( neighbourPtr->w() >= 0 && neighbourPtr->w() < dimw && \
                     neighbourPtr->x() >= 0 && neighbourPtr->x() < dimx && \
                     neighbourPtr->y() >= 0 && neighbourPtr->y() < dimy && \
                     neighbourPtr->z() >= 0 && neighbourPtr->z() < dimz && \
                     ( pixelPtr->w() != neighbourPtr->w() || pixelPtr->x() != neighbourPtr->x() || pixelPtr->y() != neighbourPtr->y() || pixelPtr->z() != neighbourPtr->z()  ) ) \
                {

/*!
  * \def endForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
                }\
            }\
        }\
    }\
}

#define beginForAllPixelsPtrNeighbours3D( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
neighbourPtr->w() = pixelPtr->w(); \
for ( neighbourPtr->x() = pixelPtr->x()-size ; neighbourPtr->x() <= pixelPtr->x()+size ; neighbourPtr->x()++ )\
{\
    for ( neighbourPtr->y() = pixelPtr->y()-size ; neighbourPtr->y() <= pixelPtr->y()+size ; neighbourPtr->y()++ )\
    {\
        for ( neighbourPtr->z() = pixelPtr->z()-size ; neighbourPtr->z() <= pixelPtr->z()+size ; neighbourPtr->z()++ )\
        {\
            if ( neighbourPtr->x() >= 0 && neighbourPtr->x() < dimx && \
                 neighbourPtr->y() >= 0 && neighbourPtr->y() < dimy && \
                 neighbourPtr->z() >= 0 && neighbourPtr->z() < dimz && \
                 ( pixelPtr->x() != neighbourPtr->x() || \
                   pixelPtr->y() != neighbourPtr->y() || \
                   pixelPtr->z() != neighbourPtr->z()  ) ) \
            {

/*!
  * \def endForAllPixelsPtrNeighbours3D( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsPtrNeighbours3D( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsPtrNeighbours3D( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'un voisinage de pixel ///////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsPtrNeighboursAndCenter( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro equivalente a beginForAllPixelsPtrNeighbours( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) mais qui parcours aussi le pixel central
*/
#define beginForAllPixelsPtrNeighboursAndCenter( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
for ( neighbourPtr->w() = pixelPtr->w()-size ; neighbourPtr->w() <= pixelPtr->w()+size ; neighbourPtr->w()++ )\
{\
    for ( neighbourPtr->x() = pixelPtr->x()-size ; neighbourPtr->x() <= pixelPtr->x()+size ; neighbourPtr->x()++ )\
    {\
        for ( neighbourPtr->y() = pixelPtr->y()-size ; neighbourPtr->y() <= pixelPtr->y()+size ; neighbourPtr->y()++ )\
        {\
            for ( neighbourPtr->z() = pixelPtr->z()-size ; neighbourPtr->z() <= pixelPtr->z()+size ; neighbourPtr->z()++ )\
            {\
                if ( neighbourPtr->w() >= 0 && neighbourPtr->w() < dimw && \
                     neighbourPtr->x() >= 0 && neighbourPtr->x() < dimx && \
                     neighbourPtr->y() >= 0 && neighbourPtr->y() < dimy && \
                     neighbourPtr->z() >= 0 && neighbourPtr->z() < dimz )\
                {

/*!
  * \def endForAllPixelsPtrNeighboursAndCenter( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
  * Macro associee a beginForAllPixelsPtrNeighboursAndCenter( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size )
*/
#define endForAllPixelsPtrNeighboursAndCenter( pixelPtr, dimw, dimx, dimy, dimz, neighbourPtr, size ) \
                }\
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une bounding box ////////////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsPtrInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr )
  * Macro qui permet de parcourir les pixels d'une image contenus dans une bounding box de coordonnees image
  * Elle est associee a la macro beginForAllPixelsPtrInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
  * Exemple :
  * Pixel4DIntPtr cur = new Pixel4DInt();
  * Pixel4DIntPtr bot = new Pixel4DInt(10,10,10,10);
  * Pixel4DIntPtr top = new Pixel4DInt(20,20,20,20);
  * // ... get the image dimension here ...
  * // ... on parcours seulements les pixels compris entre (10,10,10,10) et (20,20,20,20) en coordonnees image
  * beginForAllPixelsPtrInBBox( bot, top, dimw, dimx, dimy, dimz, cur )
  * {
  *     cur->print();
  * }
  * endForAllPixelsPtrInBBox( bot, top, dimw, dimx, dimy, dimz, cur )
*/
#define beginForAllPixelsPtrInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr ) \
for ( pixPtr->w()= botPtr->w() ; pixPtr->w() <= topPtr->w() ; pixPtr->w()++ )\
{\
    for ( pixPtr->x() = botPtr->x(); pixPtr->x() <= topPtr->x() ; pixPtr->x()++ )\
    {\
        for ( pixPtr->y() = botPtr->y() ; pixPtr->y() <= topPtr->y() ; pixPtr->y()++ )\
        {\
            for ( pixPtr->z() = botPtr->z() ; pixPtr->z() <= topPtr->z(); pixPtr->z()++ )\
            {\
                if ( pixPtr->w() >= 0 && pixPtr->w() < dimw && \
                     pixPtr->x() >= 0 && pixPtr->x() < dimx && \
                     pixPtr->y() >= 0 && pixPtr->y() < dimy && \
                     pixPtr->z() >= 0 && pixPtr->z() < dimz )\
                {

/*!
  * \def endForAllPixelsPtrInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
  * Macro associee a beginForAllPixelsPtrInBBox( pixPtrelPtr, dimw, dimx, dimy, dimz, neighbour, size )
*/
#define endForAllPixelsPtrInBBox( botPtr, topPtr, dimw, dimx, dimy, dimz, pixPtr ) \
                }\
            }\
        }\
    }\
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

#define beginForAllPixelsPtrInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr ) \
for ( pixPtr->x() = botPtr->x(); pixPtr->x() <= topPtr->x() ; pixPtr->x()++ )\
{\
    for ( pixPtr->y() = botPtr->y() ; pixPtr->y() <= topPtr->y() ; pixPtr->y()++ )\
    {\
        for ( pixPtr->z() = botPtr->z() ; pixPtr->z() <= topPtr->z(); pixPtr->z()++ )\
        {\
             if( pixPtr->x() >= 0 && pixPtr->x() < dimx && \
                 pixPtr->y() >= 0 && pixPtr->y() < dimy && \
                 pixPtr->z() >= 0 && pixPtr->z() < dimz )\
            {

/*!
  * \def endForAllPixelsPtrInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr )
  * Macro associee a beginForAllPixelsPtrInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr )
*/
#define endForAllPixelsPtrInBBox3D( botPtr, topPtr, dimx, dimy, dimz, pixPtr ) \
            }\
        }\
    }\
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Definition d'une macro de parcours d'une unique dimension ////////////////////////////////////////////// //
/*!
  * \def beginForAllPixelsPtrInDim( pixPtr, idim, ndim )
  * Macro qui parcours tous les pixels d'une dimension donnee
  * idim est l'identifiant de cette dimension (0 pour w, 1 pour x, ... )
  * ndim est la taille de cette dimension (le nombre de pixels selon cet axe)
  */
#define beginForAllPixelsPtrInDim( pixPtr, idim, ndim ) \
for ( (*pixPtr)[idim] = 0 ; (*pixPtr)[idim] < ndim ; (*pixPtr)[idim]++ )\
{\

/*!
  * \def endForAllPixelsPtrInDim( pixPtr, idim, ndim )
  * Macro associee a la macro beginForAllPixelsPtrInDim( pixPtr, idim, ndim )
  */
#define endForAllPixelsPtrInDim( pixPtr, idim, ndim ) \
}
// Fin de la definition des macros //////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

/* ******************************************************************************************************** */
/* Fin des definitions de macros outils                                                                     */
/* ******************************************************************************************************** */

class LSIS_Pixel4D : public LSIS_Point4DInt
{
public :
    /*!
     * \brief LSIS_Pixel4D
     *
     * Constructor
     */
    LSIS_Pixel4D() : LSIS_Point4DInt() {}

    /*!
     * \brief LSIS_Pixel4D
     *
     * Constructor
     *
     * \param w
     * \param x
     * \param y
     * \param z
     */
    LSIS_Pixel4D( int w, int x, int y , int z ) : LSIS_Point4DInt( w, x, y, z ) {}

    /*!
     * \brief LSIS_Pixel4D
     *
     * Constructeur de copie
     *
     * \param p : pixel a copier
     */
    LSIS_Pixel4D( const LSIS_Pixel4D& p ) : LSIS_Point4DInt( p.w(), p.x(), p.y(), p.z() ) {}

/* **************************************************************** */
/* Outils de positionnement du pixel dans l'image                   */
/* **************************************************************** */
    /*!
     * \brief isIn
     *
     * Verifie si un pixel est dans les dimensions de l'image
     *
     * \param image : image dans laquelle doit se situer le pixel
     * \return true si le pixel est dans l'image, false sinon
     */
    template< typename DataImage >
    bool isIn( const CT_Grid4D_Sparse<DataImage>& image ) const
    {
        return isIn( *image );
    }

    /*!
     * \brief isIn
     *
     * Verifie si un pixel est dans les dimensions de l'image
     *
     * \param image : image dans laquelle doit se situer le pixel
     * \return true si le pixel est dans l'image, false sinon
     */
    template< typename DataImage >
    bool isIn( const CT_Grid4D_Sparse<DataImage>* image ) const
    {
        return ( w() >= 0 && w() < image->wdim() &&
                 x() >= 0 && x() < image->xdim() &&
                 y() >= 0 && y() < image->ydim() &&
                 z() >= 0 && z() < image->zdim() );
    }

    /*!
     * \brief isIn
     *
     * Verifie si la coordonne demandee (0/1/2/3 <=> w/x/y/z) est dans l'image
     *
     * \param image : image dans laquelle doit se situer le pixel
     * \param i : coordonnee d'interet (0/1/2/3 <=> w/x/y/z)
     *
     * \return true si la coordonnee demandee est dans l'image, false sinon
     */
    template< typename DataImage >
    bool isIn( const CT_Grid4D_Sparse<DataImage>* image, int i ) const
    {
        assertWithMessage( i >= 0 && i < 4, "Tentative d'acces a une coordonnee inexistante : elle doit etre entre 0 et 4 et on demande la " << i );
        return ( (*this)(i) >= 0 && (*this)(i) < image->dim()(i) );
    }

    /*!
     * \brief isBorder
     *
     * Verifie si un pixel est une bordure de l'image
     * i.e. Si une des coordonnees de ce pixel est sur un bord de l'image
     *
     * \param image : image sur laquelle est positionne le pixel
     *
     * \return true si le pixel est un bord, false sinon
     */
    template< typename DataImage >
    bool isBorder( const CT_Grid4D_Sparse<DataImage>* image ) const
    {
        return isBorder( *image );
    }

    /*!
     * \brief isBorder
     *
     * Verifie si un pixel est une bordure de l'image
     * i.e. Si une des coordonnees de ce pixel est sur un bord de l'image
     *
     * \param image : image sur laquelle est positionne le pixel
     *
     * \return true si le pixel est un bord, false sinon
     */
    template< typename DataImage >
    bool isBorder( const CT_Grid4D_Sparse<DataImage>& image ) const
    {
        return ( w() == 0 || w() == image.wdim()-1 ||
                 x() == 0 || x() == image.xdim()-1 ||
                 y() == 0 || y() == image.ydim()-1 ||
                 z() == 0 || z() == image.zdim()-1 );
    }

    /*!
     * \brief isIn
     *
     * Verifie si la coordonne demandee (0/1/2/3 <=> w/x/y/z) est un bord de l'image
     *
     * \param image : image dans laquelle doit se situer le pixel
     * \param i : coordonnee d'interet (0/1/2/3 <=> w/x/y/z)
     *
     * \return true si la coordonnee demandee est au bord de l'image, false sinon
     */
    template< typename DataImage >
    bool isBorder( const CT_Grid4D_Sparse<DataImage>* image, int i ) const
    {
        assertWithMessage( i >= 0 && i < 4, "Tentative d'acces a une coordonnee inexistante : elle doit etre entre 0 et 4 et on demande la " << i );
        return ( (*this)(i) == 0 || (*this)(i) == image->dim()(i)-1 );
    }

    /*!
     * \brief setFromCartesian
     *
     * Accede au pixel d'une image 4D contenant un point 4D
     *
     * \param p : point d'interet
     * \param image : image sur laquelle se positionne le pixel
     */
    template< typename DataImage >
    bool setFromCartesian( LSIS_Point4DFloat const & p, const CT_Grid4D_Sparse<DataImage>* image )
    {
        return setFromCartesian( p, *image );
    }

    /*!
     * \brief setFromCartesian
     *
     * Accede au pixel d'une image 4D contenant un point 4D
     *
     * \param p : point d'interet
     * \param image : image sur laquelle se positionne le pixel
     *
     * \return false si le pixel est en dehors de l'image, true sinon
     */
    template< typename DataImage >
    bool setFromCartesian( LSIS_Point4DFloat const & p, const CT_Grid4D_Sparse<DataImage>& image )
    {
        Eigen::Vector4d bot = image.boundingBoxBot();
        Eigen::Vector4d top = image.boundingBoxTop();
        Eigen::Vector4d res = image.res();
        bool rslt = true;

        for( int i = 0 ; i < 4 ; i++ )
        {
            if ( p(i) < bot(i) || p(i) > top(i) )
            {
                // On met la coordonnee courante a -infini et les suivantes a infini pour se reperer dans l'affichage (savoir quelle coordonnee etait hors de l'image)
                (*this)(i) = -std::numeric_limits<int>::max();
                rslt = false;
            }

            if ( p(i) == top(i) )
            {
                (*this)(i) =  image.dim()(i) - 1;
            }

            else
            {
                (*this)(i) = floor( ( p(i) - bot(i) ) / res(i) );
            }
        }

        return rslt;
    }

/* **************************************************************** */
/* Un pixel peut acceder aux valeurs de l'image et le smodifier     */
/* **************************************************************** */
public :
    /*!
     * \brief valueIn
     *
     * Renvoie la valeur du pixel dans une image 4D.
     *
     * \param image : image sur laquelle on cherche la valeur du pixel
     * \return la valeur du pixel dans l'image
     */
    template< typename DataImage >
    inline DataImage valueIn ( const CT_Grid4D_Sparse<DataImage>& image ) const
    {
        return valueIn( &image );
    }

    /*!
     * \brief valueIn
     *
     * Renvoie la valeur du pixel dans une image 4D a valeurs entieres.
     * \warning Cette methode ne verifie pas l'existence du pixel dans l'image.
     *
     * \param image : image sur laquelle on cherche la valeur du pixel
     * \return la valeur du pixel dans l'image
     */
    template< typename DataImage >
    inline DataImage valueIn ( const CT_Grid4D_Sparse<DataImage>* image ) const
    {
        assertWithMessage( isIn(image) , "Le pixel " << (*this) << " est hors de l'image " << image );
        return image->value( w(), x(), y(), z() );
    }

    bool valueIn ( const CT_Grid4D_Sparse<bool>* image ) const;

    /*!
     * \brief setValueIn
     *
     * Affecte une valeur au pixel dans une image
     *
     * \param image : image
     * \param v : valeur a affecter dans l'image
     */
    template< typename DataImage >
    inline void setValueIn ( CT_Grid4D_Sparse<DataImage>& image, DataImage v ) const
    {
        setValueIn( &image, v );
    }

    /*!
     * \brief setValueIn
     *
     * Affecte une valeur au pixel dans une image
     *
     * \param image : image
     * \param v : valeur a affecter dans l'image
     */
    template< typename DataImage >
    inline void setValueIn ( CT_Grid4D_Sparse<DataImage>* image, DataImage v ) const
    {
        image->setValue( w(), x(), y(), z(), v );
    }

    /*!
     * \brief isLocalMaxima
     *
     * Indique si le pixel est un maxima local (maxima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     *
     * \return true si le pixel est un maxima local, false sinon
     */
    template< typename DataImage >
    inline bool isLocalMaxima ( CT_Grid4D_Sparse<DataImage> const & image ) const
    {
        return isLocalMaxima( &image );
    }

    /*!
     * \brief isLocalMaxima
     *
     * Indique si le pixel est un maxima local (maxima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     *
     * \return true si le pixel est un maxima local, false sinon
     */
    template< typename DataImage >
    bool isLocalMaxima ( CT_Grid4D_Sparse<DataImage> const * image,
                         int neighbourhoodSize ) const
    {
        DataImage curValue = valueIn( image );
        LSIS_Pixel4D nei;

        LSIS_Pixel4D size ( neighbourhoodSize, neighbourhoodSize, neighbourhoodSize, neighbourhoodSize );
        LSIS_Pixel4D bot = (*this) - size;
        LSIS_Pixel4D top = (*this) + size;

        // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
        for( int axe = 0 ; axe < 4 ; axe++ )
        {
            if( bot(axe) < 0 )
            {
                bot(axe) = 0;
            }

            if( bot(axe) >= image->dim()(axe) )
            {
                bot(axe) = image->dim()(axe) - 1;
            }

            if( top(axe) < 0 )
            {
                top(axe) = 0;
            }

            if( top(axe) >= image->dim()(axe) )
            {
                top(axe) = image->dim()(axe) - 1;
            }
        }

        for ( nei.w() = bot.w() ; nei.w() <= top.w() ; nei.w()++ )
        {
            for ( nei.x() = bot.x(); nei.x() <= top.x() ; nei.x()++ )
            {
                for ( nei.y() = bot.y() ; nei.y() <= top.y() ; nei.y()++ )
                {
                    for ( nei.z() = bot.z() ; nei.z() <= top.z(); nei.z()++ )
                    {
                        if ( nei != (*this) )
                        {
                            if ( nei.valueIn(image) > curValue )
                            {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /*!
     * \brief isLocalMaximaInBBox
     *
     * Indique si le pixel est un maxima au sein d'une bounding box (maxima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     * \param bot : pixel inferieur gauche de la bounding box
     * \param top : pixel superieur droit de la bounding box
     * \return true si le pixel est un maxima local dans la bbox, false sinon
     */
    template< typename DataImage >
    bool isLocalMaximaInBBox ( CT_Grid4D_Sparse<DataImage> const * image, LSIS_Pixel4D const & bot, LSIS_Pixel4D const & top ) const
    {
        DataImage curValue = valueIn( image );
        LSIS_Pixel4D nei;
        beginForAllPixelsInBBox( bot, top, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei )
        {
            if ( nei.valueIn(image) > curValue )
            {
                delete nei;
                return false;
            }
        }
        endForAllPixelsInBBox( bot, top, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei )

        return true;
    }

    /*!
     * \brief isLocalMaximaInZRange
     *
     * Indique si le pixel est un maxima au sein d'un range de couches en z (maxima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     * \param minLevZ : niveau en z minimum a prendre en compte dans le voisinage pour voir si le pixel est un maxima
     * \param maxLevZ : niveau en z maximum a prendre en compte dans le voisinage pour voir si le pixel est un maxima
     *
     * \return true si le pixel est un maxima en ne considerant que ses voisins compris dans le range [minLevZ, maxLevZ]
     */
    template< typename DataImage >
    bool isLocalMaximaInZRange ( CT_Grid4D_Sparse<DataImage> const * image, size_t minLevZ, size_t maxLevZ ) const
    {
        DataImage curValue = valueIn( image );
        LSIS_Pixel4D nei;
        beginForAllPixelsNeighbours( (*this), image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )
        {
            if( nei.z() >= minLevZ && nei.z() <= maxLevZ )
            {
                if ( nei.valueIn(image) > curValue )
                {
                    return false;
                }
            }
        }
        endForAllPixelsNeighbours( (*this), image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )

        return true;
    }

    /*!
     * \brief isLocalMinima
     *
     * Indique si le pixel est un minima local (minima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     *
     * \return true si le pixel est un minima local, false sinon
     */
    template< typename DataImage >
    inline bool isLocalMinima ( CT_Grid4D_Sparse<DataImage> const & image ) const
    {
        return isLocalMinima( &image );
    }

    /*!
     * \brief isLocalMinima
     *
     * Indique si le pixel est un minima local (minima au sens large : ses voisins sont tous inferieurs ou egaux)
     *
     * \param image : l'image sur laquelle on travaille
     *
     * \return true si le pixel est un minima local, false sinon
     */
    template< typename DataImage >
    bool isLocalMinima ( CT_Grid4D_Sparse<DataImage> const * image ) const
    {
        DataImage curValue = valueIn( image );
        LSIS_Pixel4D* nei = new LSIS_Pixel4D();
        beginForAllPixelsPtrNeighbours( this, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )
        {
            if ( nei->valueIn(image) < curValue )
            {
                delete nei;
                return false;
            }
        }
        endForAllPixelsPtrNeighbours( this, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )

        delete nei;
        return true;
    }

    /*!
     * \brief status
     *
     * Donne le statut d'un pixel sur l'image
     *
     * \param image : image sur laquelle est le pixel
     * \param isLocalMinima : vrai si le pixel est un maxima local au sens large
     * \param isLocalMaxima : vrai si le pixel est un minima local au sens large
     * \param isOnPlateau : vrai si le pixel est sur un plateau
     */
    template< typename DataImage >
    inline void status ( CT_Grid4D_Sparse<DataImage> const & image, bool& isLocalMinima, bool& isLocalMaxima, bool& isOnPlateau ) const
    {
        return status( &image, isLocalMinima, isLocalMaxima, isOnPlateau );
    }

    /*!
     * \brief status
     *
     * Donne le statut d'un pixel sur l'image
     *
     * \param image : image sur laquelle est le pixel
     * \param isLocalMinima : vrai si le pixel est un maxima local au sens large
     * \param isLocalMaxima : vrai si le pixel est un minima local au sens large
     * \param isOnPlateau : vrai si le pixel est sur un plateau
     */
    template< typename DataImage >
    void status ( CT_Grid4D_Sparse<DataImage> const * image, bool& isLocalMinima, bool& isLocalMaxima, bool& isOnPlateau ) const
    {
        isLocalMaxima = true;
        isLocalMinima = true;
        isOnPlateau = false;

        DataImage curValue = valueIn( image );
        LSIS_Pixel4D* nei = new LSIS_Pixel4D();
        beginForAllPixelsPtrNeighbours( this, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )
        {
            if ( nei->valueIn(image) < curValue )
            {
                isLocalMinima = false;
            }

            if ( nei->valueIn(image) > curValue )
            {
                isLocalMaxima = false;
            }

            if ( nei->valueIn(image) == curValue )
            {
                isOnPlateau = true;
            }
        }
        endForAllPixelsPtrNeighbours( this, image->wdim(), image->xdim(), image->ydim(), image->zdim(), nei, 1 )

        delete nei;
    }

    /*!
     * \brief getMinMaxInNeighbourhood
     *
     * Recupere les valeurs minimum et maximum dans un voisinage autour d'un pixel
     *  \warning la valeur du pixel d'interet est aussi prise en compte
     *
     * \param outMin : valeur minimum
     * \param outMax : valeur maximum
     * \param size : taille du voisinage
     */
    template< typename DataImage >
    void getMinMaxInNeighbourhood( CT_Grid4D_Sparse<DataImage> const & image, DataImage& outMin, DataImage& outMax, int size ) const
    {
        outMin = std::numeric_limits<DataImage>::max();
        outMax = -std::numeric_limits<DataImage>::max();

        DataImage neiValue;
        LSIS_Pixel4D nei;
        beginForAllPixelsNeighboursAndCenter( (*this), image.wdim(), image.xdim(), image.ydim(), image.zdim(), nei, size )
        {
            neiValue = nei.valueIn(image);

            if ( neiValue < outMin )
            {
                outMin = neiValue;
            }

            if ( neiValue > outMax )
            {
                outMax = neiValue;
            }
        }
        endForAllPixelsNeighboursAndCenter( (*this), image.wdim(), image.xdim(), image.ydim(), image.zdim(), nei, size )
    }

    /*!
     * \brief getMinMaxInNeighbourhood
     *
     * Recupere les valeurs minimum et maximum dans un voisinage autour d'un pixel
     *  \warning la valeur du pixel d'interet est aussi prise en compte
     *
     * \param outMin : valeur minimum
     * \param outMax : valeur maximum
     * \param size : taille du voisinage
     */
    template< typename DataImage >
    void getMinMaxInNeighbourhood( CT_Grid4D_Sparse<DataImage> const * image, DataImage& outMin, DataImage& outMax, int size ) const
    {
        getMinMaxInNeighbourhood( *image, outMin, outMax, size );
    }

/* **************************************************************** */
/* Deplacements, position et conversion sur une image               */
/* **************************************************************** */
    /*!
     * \brief toCartesianW
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension w
     */
    template< typename DataImage >
    inline float toCartesianW ( CT_Grid4D_Sparse<DataImage> const & im ) const
    {
        return toCartesianW( &im );
    }

    /*!
     * \brief toCartesianW
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension w
     */
    template< typename DataImage >
    inline float toCartesianW ( CT_Grid4D_Sparse<DataImage> const * im ) const
    {
        return ( im->minW() + ( w() + 0.5 ) * im->wres() );
    }

    /*!
     * \brief toCartesianX
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension x
     */
    template< typename DataImage >
    inline float toCartesianX ( const CT_Grid4D_Sparse<DataImage>& im ) const
    {
        return toCartesianX( &im );
    }

    /*!
     * \brief toCartesianX
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension x
     */
    template< typename DataImage >
    inline float toCartesianX ( const CT_Grid4D_Sparse<DataImage>* im ) const
    {
        return ( im->minX() + ( x() + 0.5 ) * im->xres() );
    }

    /*!
     * \brief toCartesianY
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension y
     */
    template< typename DataImage >
    inline float toCartesianY ( const CT_Grid4D_Sparse<DataImage>& im ) const
    {
        return toCartesianY( &im );
    }

    /*!
     * \brief toCartesianY
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension y
     */
    template< typename DataImage >
    inline float toCartesianY ( const CT_Grid4D_Sparse<DataImage>* im ) const
    {
        return ( im->minY() + ( y() + 0.5 ) * im->yres() );
    }

    /*!
     * \brief toCartesianZ
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension z
     */
    template< typename DataImage >
    inline float toCartesianZ ( const CT_Grid4D_Sparse<DataImage>& im ) const
    {
        return toCartesianZ( &im );
    }

    /*!
     * \brief toCartesianZ
     *
     * Convertit une coordonnee de pixel dans une image en coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return la coordonnee cartesienne du pixel selon la dimension z
     */
    template< typename DataImage >
    inline float toCartesianZ ( const CT_Grid4D_Sparse<DataImage>* im ) const
    {
        return ( im->minZ() + ( z() + 0.5 ) * im->zres() );
    }

    /*!
     * \brief toCartesian
     *
     * Convertit la ieme composante de la coordonnee du pixel en cartesien
     *
     * \param im : image sur laquelle se trouve le pixel
     * \param i : composante a convertir
     *
     * \return la composante du pixel en cartesien
     */
    template< typename DataImage >
    inline float toCartesian ( const CT_Grid4D_Sparse<DataImage>* im, int i ) const
    {
        return toCartesian( *im, i );
    }

    /*!
     * \brief toCartesian
     *
     * Convertit la ieme composante de la coordonnee du pixel en cartesien
     *
     * \param im : image sur laquelle se trouve le pixel
     * \param i : composante a convertir
     *
     * \return la composante du pixel en cartesien
     */
    template< typename DataImage >
    inline float toCartesian ( const CT_Grid4D_Sparse<DataImage>& im, int i ) const
    {
        assertWithMessage( i >= 0 && i < 4,"On demande une coordonnee qui n'existe pas : " << i );
        return ( im.minZ() + ( z() + 0.5 ) * im.zres() );
    }

    /*!
     * \brief getCartesian
     *
     * Convertit un pixel en coordonnees image vers des coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return un pixel dont les coordonnees sont l'equivalent en cartesien du pixel qui appelle la methode
     */
    template< typename DataImage >
    inline LSIS_Point4DFloat getCartesian ( CT_Grid4D_Sparse<DataImage> const * im ) const
    {
        return getCartesian( *im );
    }

    /*!
     * \brief getCartesian
     *
     * Convertit un pixel en coordonnees image vers des coordonnees cartesiennes
     *
     * \param im : image sur laquelle se trouve le pixel
     *
     * \return un pixel dont les coordonnees sont l'equivalent en cartesien du pixel qui appelle la methode
     */
    template< typename DataImage >
    inline LSIS_Point4DFloat getCartesian ( CT_Grid4D_Sparse<DataImage> const & im ) const
    {
        return LSIS_Point4DFloat( toCartesianW(im),
                                  toCartesianX(im),
                                  toCartesianY(im),
                                  toCartesianZ(im) );
    }

    /*!
     * \brief equals3D
     *
     * Compare deux pixels selon leurs coordonnees 3D
     *
     * \param p : pixel a comparer
     * \return true si les deux pixels ont les memes coordonnees en (x,y,z), false sinon
     */
    bool equals3D( LSIS_Pixel4D const & p ) const
    {
        return ( x() == p.x() &&
                 y() == p.y() &&
                 z() == p.z() );
    }

    LSIS_Pixel4D operator+ ( LSIS_Pixel4D const & p ) const
    {
        return LSIS_Pixel4D( w() + p.w(),
                             x() + p.x(),
                             y() + p.y(),
                             z() + p.z());
    }

    LSIS_Pixel4D operator- ( LSIS_Pixel4D const & p ) const
    {
        return LSIS_Pixel4D( w() - p.w(),
                             x() - p.x(),
                             y() - p.y(),
                             z() - p.z());
    }
};

#endif // LSIS_PIXEL4D_H
