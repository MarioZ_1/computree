#include "lsis_newstepfastfilterhoughspace.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Tools dependencies
#include "../houghspace/lsis_houghspace4d.h"
#include "streamoverload/streamoverload.h"

//Qt dependencies
#include <QElapsedTimer>

// Alias for indexing models
#define DEFin_rsltHoughSpace "rsltHoughSpace"
#define DEFin_grpHoughSpace "grpHoughSpace"
#define DEFin_itmHoughSpace "itmHoughSpace"
#define DEFin_itmNormals "itmNormals"

// Constructor : initialization of parameters
LSIS_NewStepFastFilterHoughSpace::LSIS_NewStepFastFilterHoughSpace(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minRatio = 2;
}

// Step description (tooltip of contextual menu)
QString LSIS_NewStepFastFilterHoughSpace::getStepDescription() const
{
    return tr("2 - Filtre un espace de Hough");
}

// Step detailled description
QString LSIS_NewStepFastFilterHoughSpace::getStepDetailledDescription() const
{
    return tr("Si l'algorithme qui crée l'espace de Hough suppose que les directions des normales "
              "des points ne sont pas forcément dans la bonne direction, il génère des cercles dans les deux directions. "
              "C'est pourquoi, si nous prenons l'exemple d'un tronc, il existe des valeurs élevées au centre du "
              "tronc mais aussi tout autour à une certaine distance. Ce filtre à pour but de supprimer les valeurs "
              "élevées en dehors du tronc. Pour chaque point du nuage l'algorithme va cumuler les valeurs dans la direction "
              "de la normale et cumuler dans une autre variable les valeurs dans le sens opposé de la normale. Le ratio "
              "calculé est max(cumul1, cumul2)/min(cumul1, cumul2). Si ce ratio est inférieur au ratio minimum les deux "
              "valeurs sont supprimer de la grille, sinon la valeur la moins élevée est supprimée.");
}

// Step URL
QString LSIS_NewStepFastFilterHoughSpace::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_NewStepFastFilterHoughSpace::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_NewStepFastFilterHoughSpace(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_NewStepFastFilterHoughSpace::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltHoughSpace = createNewInResultModelForCopy(DEFin_rsltHoughSpace,"Hough Space");
    resIn_rsltHoughSpace->setZeroOrMoreRootGroup();
    resIn_rsltHoughSpace->addGroupModel("",DEFin_grpHoughSpace,CT_AbstractItemGroup::staticGetType(),tr("Group"));
    resIn_rsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmNormals, CT_PointsAttributesNormal::staticGetType(), tr("Normals"));
    resIn_rsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmHoughSpace, LSIS_HoughSpace4D::staticGetType(), tr("HoughSpace"));
}

// Creation and affiliation of OUT models
void LSIS_NewStepFastFilterHoughSpace::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_rsltHoughSpace = createNewOutResultModelToCopy( DEFin_rsltHoughSpace );

    if(res_rsltHoughSpace != NULL)
    {
    }
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_NewStepFastFilterHoughSpace::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Ratio minimum"), "", 0.0001, 100, 4, _minRatio, 1, tr("Si le ratio calculé est inférieur à celui-ci "
                                                                                      "toutes les valeurs du point sont supprimées, sinon "
                                                                                      "les valeurs ayant eu le moins de votes cumulés sont "
                                                                                      "supprimées. Voir la description de l'étape pour plus "
                                                                                      "d'explication."));
}

void LSIS_NewStepFastFilterHoughSpace::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* outResultHoughSpace = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpHoughSpace( outResultHoughSpace, this, DEFin_grpHoughSpace );
    while( itIn_grpHoughSpace.hasNext() && !isStopped() )
    {
        CT_StandardItemGroup* grpIn_grpHoughSpace = (CT_StandardItemGroup*) itIn_grpHoughSpace.next();

        LSIS_HoughSpace4D* itemIn_itmHoughSpace = (LSIS_HoughSpace4D*)grpIn_grpHoughSpace->firstItemByINModelName( this, DEFin_itmHoughSpace );
        const CT_PointsAttributesNormal* itemIn_itmNormals = (CT_PointsAttributesNormal*)grpIn_grpHoughSpace->firstItemByINModelName( this, DEFin_itmNormals );

        if (itemIn_itmNormals != NULL && itemIn_itmHoughSpace != NULL )
        {
            itemIn_itmHoughSpace->fastFilter( itemIn_itmNormals,
                                              _minRatio );
        }
    }
}
