#include "lsis_stepaddsetsnakesoverdtm.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_grid2dxy.h"

//Tools dependencies
#include "ct_point.h"

// Alias for indexing models
#define DEFin_RsltDtm "RsltDtm"
#define DEFin_GrpDtm "GrpDtm"
#define DEFin_ItmDtm "ItmDtm"

#define DEFout_RsltSetSnakes "RsltSetSnakes"
#define DEFout_GrpSetSnakes "GrpSetSnakes"
#define DEFout_GrpGrpCircles "GrpGrpCircles"
#define DEFout_GrpCircles "GrpCircles"
#define DEFout_ItmCircle "ItmCircle"


// Constructor : initialization of parameters
LSIS_StepAddSetSnakesOverDTM::LSIS_StepAddSetSnakesOverDTM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepAddSetSnakesOverDTM::getStepDescription() const
{
    return tr("Charge des snakes et sort leur DBH");
}

// Step detailled description
QString LSIS_StepAddSetSnakesOverDTM::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepAddSetSnakesOverDTM::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepAddSetSnakesOverDTM::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepAddSetSnakesOverDTM(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepAddSetSnakesOverDTM::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_RsltDtm = createNewInResultModel(DEFin_RsltDtm, tr("Result DTM"), tr(""), true);
    resIn_RsltDtm->setZeroOrMoreRootGroup();
    resIn_RsltDtm->addGroupModel("", DEFin_GrpDtm, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_RsltDtm->addItemModel(DEFin_GrpDtm, DEFin_ItmDtm, CT_Grid2DXY<float>::staticGetType(), tr("Digital Terrain Model"));

}

// Creation and affiliation of OUT models
void LSIS_StepAddSetSnakesOverDTM::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_RsltSetSnakes = createNewOutResultModel(DEFout_RsltSetSnakes, tr("Result"));
    res_RsltSetSnakes->setRootGroup(DEFout_GrpSetSnakes, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addGroupModel(DEFout_GrpSetSnakes, DEFout_GrpGrpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addGroupModel(DEFout_GrpGrpCircles, DEFout_GrpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addItemModel(DEFout_GrpCircles, DEFout_ItmCircle, new CT_Circle(), tr("Circle"));

}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepAddSetSnakesOverDTM::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice("Snake Files", CT_FileChoiceButton::OneOrMoreExistingFiles, "*.ct_snake", _fileNames);
}

void LSIS_StepAddSetSnakesOverDTM::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_RsltDtm = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltSetSnakes = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_GrpDtm(resIn_RsltDtm, this, DEFin_GrpDtm);
    while (itIn_GrpDtm.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_GrpDtm = (CT_AbstractItemGroup*) itIn_GrpDtm.next();
        
        const CT_Grid2DXY<float>* itemIn_ItmDtm = (CT_Grid2DXY<float>*)grpIn_GrpDtm->firstItemByINModelName(this, DEFin_ItmDtm);
        if (itemIn_ItmDtm != NULL)
        {
            CT_StandardItemGroup* grp_GrpSnakes= new CT_StandardItemGroup(DEFout_GrpSetSnakes, res_RsltSetSnakes);
            res_RsltSetSnakes->addGroup(grp_GrpSnakes);

            foreach ( QString curFileName, _fileNames )
            {
                QFile file( curFileName );

                if( !file.open( QIODevice::ReadOnly ) )
                {
                    PS_LOG->addErrorMessage( PS_LOG->error, "Can not access file : \""+file.fileName()+"\"" );
                    return;
                }

                // Outils de lecture
                QTextStream fileStream(&file);

                float x,y,z,r;
                float xdbh = -9999, ydbh = -9999, rdbh = -9999;
                float heightCurCircle;
                float minDist = std::numeric_limits<float>::max();
                CT_Point curCenter;
                CT_Point curOrientation;
                curOrientation.setValues(0,0,1);

                while( !fileStream.atEnd() )
                {
                    fileStream >> r >> x >> y >> z;

                    heightCurCircle = z - itemIn_ItmDtm->valueAtXY( x, y );
                    if( fabs( heightCurCircle - 1.3 ) < minDist )
                    {
                        minDist = fabs( heightCurCircle - 1.3 );
                        xdbh = x;
                        ydbh = y;
                        rdbh = r;
                    }

                    curCenter.setValues( x, y, z );

                    CT_Circle* curCircle = new CT_Circle( DEFout_ItmCircle,
                                                          res_RsltSetSnakes,
                                                          new CT_CircleData( curCenter, curOrientation, r ) );

                    CT_StandardItemGroup* grp_GrpGrpCircle= new CT_StandardItemGroup(DEFout_GrpGrpCircles, res_RsltSetSnakes);
                    CT_StandardItemGroup* grp_GrpCircle= new CT_StandardItemGroup(DEFout_GrpCircles, res_RsltSetSnakes);
                    grp_GrpCircle->addItemDrawable(curCircle);
                    grp_GrpGrpCircle->addGroup( grp_GrpCircle );
                    grp_GrpSnakes->addGroup( grp_GrpGrpCircle );
                }

                if( rdbh > 0 )
                {
                    qDebug() << xdbh << ydbh << rdbh;
                }

                file.close();
            }
        }
    }
}
