/************************************************************************************
* Filename :  lsis_openactivecontour4dcontinuous.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_OPENACTIVECONTOUR4DCONTINUOUS_H
#define LSIS_OPENACTIVECONTOUR4DCONTINUOUS_H

// Se deplace sur une image
#include "ct_itemdrawable/ct_grid4d.h"

// Un contours actif est forme d'un ensemble de points 4D ranges dans une matrice a n lignes et 4 colonnes, n etant le nombre de points dans le contours
#include <Eigen/Core>

template< typename DataImage >
class LSIS_OpenActiveContour4DContinuous
{
public:
    LSIS_OpenActiveContour4DContinuous( float alpha,
                                        float beta,
                                        float gama,
                                        CT_Grid4D<DataImage>* image );

private :
    float                   _alpha;     /*!< Poids de l'elasticite */
    float                   _beta;      /*!< Poids de la courbure */
    float                   _gama;      /*!< Poids de l'energie image */

    Eigen::MatrixX4f        _points;    /*!< Points du contours places dans une matrice n lignes, 4 colonnes, la precision flottante est suffisante */
    CT_Grid4D<DataImage>*   _image;     /*!< Un contours actif se deplace sur une image */
};

// Implementation templatee
#include "lsis_openactivecontour4dcontinuous.hpp"

#endif // LSIS_OPENACTIVECONTOUR4DCONTINUOUS_H
