#include "connectedcomponent.h"
#include <QDebug>

int ConnectedComponent::_labelID = 1;

ConnectedComponent::ConnectedComponent() :
    _nCircles(0),
    _type(NA),
    _label(NA),
    _wasFused(false),
    _hasBeenAgregated(false)
{
}

void ConnectedComponent::displayAllChildren()
{
    displayAllChildrenRecursive(0);
}

void ConnectedComponent::displayAllChildrenRecursive( int depth )
{
    QString decalage;
    for( int i = 0 ; i < depth ; i++ )
    {
        decalage += "  ";
    }

    qDebug() << decalage << "----->";
    qDebug() << decalage << "-- Niveau " << depth << " :: Composante" << _label << "de type " << _type << " a " << _children.size() << " pour fils :";

    foreach( ConnectedComponent* component, _children )
    {
        component->displayAllChildrenRecursive( depth+1 );
    }
    qDebug() << decalage << "<-----";
}
