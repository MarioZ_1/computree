#ifndef ICRO_STEP3DGRIDFLOATTOMULTIPLE2DRASTERS_H
#define ICRO_STEP3DGRIDFLOATTOMULTIPLE2DRASTERS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"

#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>

/*!
 * \class ICRO_Step3dGridFloatToMultiple2dRasters
 * \ingroup Steps_ICRO
 * \brief <b>Cree un raster 2D pour chaque tranche d'une image 3D.</b>
 *
 * No detailled description for this step
 *
 * \param _sliceAxis
 * \param _saveDir
 *
 */

class ICRO_Step3dGridFloatToMultiple2dRasters: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_Step3dGridFloatToMultiple2dRasters(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for <float> step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void saveRastersFrom3dGrid( const CT_Grid3D<float>* grid, QString dirPath, QString fileNamePrefix );

    void saveRaster2dZLevel( const CT_Grid3D<float>* grid, int zLevel, QString dirPath, QString fileName );

private:

    // Step parameters
    QStringList     _saveDirList;    /*!<  */
    QString         _saveDir;
};

#endif // ICRO_STEP3DGRIDFLOATTOMULTIPLE2DRASTERS_H
