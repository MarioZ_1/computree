#ifndef ICRO_STEPSPLITANDMERGECONNECTEDCOMPONENTS_H
#define ICRO_STEPSPLITANDMERGECONNECTEDCOMPONENTS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "../tools/connectedcomponent.h"

#include "ct_itemdrawable/ct_grid3d.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

/*!
 * \class ICRO_StepSplitAndMergeConnectedComponents
 * \ingroup Steps_ICRO
 * \brief <b>Segmente une grille 3D en composantes connexes simplexes.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

class ICRO_StepSplitAndMergeConnectedComponents: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepSplitAndMergeConnectedComponents(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void computeConnectedComponentsUntilSimplexes(CT_Grid3D<int>* inputGrid,
                                                  CT_Grid3D<int>* outConnectedComponentsGrid,
                                                  double minIntensity,
                                                  int superPixelResolution,
                                                  int minComponentSize,
                                                  QVector<CT_CircleData *>& circles,
                                                  QVector<ConnectedComponent *>& outConnectedComponents);

    void splitConnectedComponentsUntilSimplexesMultiThresh(CT_Grid3D<int>* inputGrid,
                                                           CT_Grid3D<int>* outConnectedComponentsGrid,
                                                           int minComponentSize,
                                                           QVector<CT_CircleData *>& circles,
                                                           QVector<ConnectedComponent *>& outConnectedComponents);

    void labelSuperConnectedComponents(CT_Grid3D<int>* inputGrid,
                                       CT_Grid3D<int>* outConnectedComponentsGrid,
                                       double minIntensity,
                                       int superPixelResolution,
                                       QVector<ConnectedComponent *>& outConnectedComponents);

    void labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *inputGrid,
                                              CT_Grid3D<int> *outLabeledGrid,
                                              double minValue,
                                              int startx,
                                              int starty,
                                              int startz,
                                              int label,
                                              int resolution,
                                              ConnectedComponent* outSuperComponent);

    void markSuperComponent( ConnectedComponent* superComponent,
                             CT_Grid3D<int>* grid,
                             int label );

    void updateComponentsType(QVector< ConnectedComponent* >& components,
                              int minComponentSize);

    void addPixelsToSuperConnectedComponent(SuperPixel &superPixel,
                                            ConnectedComponent* outSuperComponent);


    ConnectedComponent* getComponentFromLabel(QVector< ConnectedComponent* >& components,
                                              int label);

    void getComponentsType(CT_Grid3D<int> *labeledGrid,
                           QVector< CT_CircleData* >& circles,
                           QVector< ConnectedComponent* >& components,
                           int minLabelSize);

    void splitComplexComponent(ConnectedComponent* componentToBeSplitted,
                               CT_Grid3D<int>* valueGrid,
                               CT_Grid3D<int>* labelGrid,
                               int initialResolution,
                               int minValue,
                               QVector<ConnectedComponent *> &outComponents);

    void splitComplexComponentWithThresh( ConnectedComponent* componentToBeSplitted,
                                          CT_Grid3D<int>* valueGrid,
                                          CT_Grid3D<int>* labelGrid,
                                          int thresh,
                                          QVector<ConnectedComponent *> &outComponents);

    void splitComponentUntilSimplexes(ConnectedComponent* componentToBeSplitted,
                                      QVector<ConnectedComponent *>& outSplittedComponents,
                                      int initialResolution,
                                      int minValue,
                                      int minLabelSize,
                                      CT_Grid3D<int>* intesityGrid,
                                      CT_Grid3D<int>* labelGrid );

    void mergeConnectedComponents(QVector<ConnectedComponent *>& connectedComponents,
                                  CT_Grid3D<int>* labelGrid,
                                  int nIterations );

    void initDistanceGrid(QVector<ConnectedComponent *>& connectedComponents,
                          CT_Grid3D<int>* distanceGrid );

    void getExternalPixelsOfConnectedComponent (CT_Grid3D<int>* labelGrid,
                                                CT_Grid3D<bool>* markGrid,
                                                ConnectedComponent* component,
                                                QVector< Pixel >& outPixels );

    void dilateConnectedComponentForDistance(ConnectedComponent* component,
                                             CT_Grid3D<int>* labelGrid,
                                             CT_Grid3D<int>* distanceGrid,
                                             QVector<ConnectedComponent *> &allComponents);

    void updateConnectedComponentBordersDistancesAndLabelAfterDilatation(ConnectedComponent* component,
                                                                         CT_Grid3D<int>* labelGrid,
                                                                         CT_Grid3D<int>* distanceGrid,
                                                                         QVector<ConnectedComponent *> &allComponents);

    QVector<CT_CircleData*> getCirclesFromFile(QString dir, QString fileName);

    bool circleIntersect2D( QVector< CT_CircleData* >& circles, CT_CircleData* c );

    bool circleIntersect2D( CT_CircleData* c1, CT_CircleData* c2 );

    void cleanFinalLabelGridFromIntesityGrid(CT_Grid3D<int>* intensityGrid,
                                             CT_Grid3D<int>* labelGrid);

    void countComponentsByType(QVector<ConnectedComponent *>& components,
                                int& outNTotal,
                                int &outNNA,
                                int& outNTooSmall,
                                int& outNBigEnough,
                                int& outNEmpty,
                                int& outNSimplex,
                                int& outNComplex );

    void relabelConnectedComponents(QVector<ConnectedComponent *> &components,
                                    CT_Grid3D<int>* labelGrid);

    void splitSceneFromPositiveLabels(const CT_Scene* inputScene,
                                      CT_Grid3D<int> *labelGrid,
                                      QVector< CT_PointCloudIndexVector* > outScenes);

    bool existsComplexComponents(QVector<ConnectedComponent *>& connectedComponents);

    void classifyComplexComponents(QVector<ConnectedComponent *>& inputConnectedComponents,
                                   QVector<ConnectedComponent *> &outputComplexesConnectedComponents,
                                   QVector<ConnectedComponent *>& outputNonComplexesConnectedComponents);

    void computeDistanceFromConnectedComponent( ConnectedComponent* component,
                                                Eigen::MatrixXi* outDistances,
                                                CT_Grid3D<int>* labelGrid,
                                                CT_Grid3D<bool>* markGrid );

private:

    // Step parameters
    double      _intensityThreshold;    /*!< Intensite minimale pour prendre en compte un pixel dans les composantes connexes (un super pixel doit contenir au moins un pixel de cette intensite pour etre pris en compte) */
    int         _minPixelsInComponent;  /*!< Nombre de pixels minimum que doit contenir une composante connexe pour etre prise en compte */
    int         _resolution;            /*!< Resolution de départ pour l'approche multi échelle (taille initiale d'un super pixel) */
    int         _nIterDilatation;       /*!< Nombre de dilatations maximum pour le merge */

    QVector< CT_CircleData* >   _circles;
    CT_Grid3D<bool>*            _markImage;

    CT_Grid3D<int>*             _intensityGrid;

    int _splitType;                 // Type de split a choisir dans la configuration box

    Eigen::MatrixXi*            _distances; // Matrice des distances entre chaque graine et chaque composante connexe
};

#endif // ICRO_STEPSPLITANDMERGECONNECTEDCOMPONENTS_H
