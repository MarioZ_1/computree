#include "icro_stepgetconnectedcomponentonintgridmultiresolution.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_iterator/ct_pointiterator.h"

#include <QQueue>

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"
#define DEFin_itmScene "itmScene"

#define DEFout_rsltResultConnectedComponentNonMultis "rsltResultConnectedComponentNonMultis"


#define DEFout_grpGeneral "grpGeneral"

#define DEFout_grpGrpCircles "grpGrpCircles"
#define DEFout_grpCircles "grpCircles"
#define DEFout_itmCircle "itmCircle"

#define DEFout_grpGrid3d "grpGrid3d"
#define DEFout_itmGrid3dLabel "itmGrid3dLabel"
#define DEFout_itmGrid3dType "itmGrid3dType"

#define DEFout_grpScene "grpScene"
#define DEFout_itmSceneTooSmall "itmSceneTooSmall"
#define DEFout_itmSceneEmpty "itmSceneEmpty"
#define DEFout_itmSceneSimplex "itmSceneSimplex"
#define DEFout_itmSceneComplex "itmSceneComplex"

#define DEFout_grpGrpSceneComponents "grpGrpSceneComponents"
#define DEFout_grpSceneComponents "grpSceneComponents"
#define DEFout_itmSceneComponent "itmSceneComponent"

// Constructor : initialization of parameters
ICRO_StepGetConnectedComponentOnIntGridMultiResolution::ICRO_StepGetConnectedComponentOnIntGridMultiResolution(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minValue = 100;
    _minPixelsInComponent = 100;
    _resolution = 1;
    _nStepsAggreg = 10;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getStepDescription() const
{
    return tr("Identifie les composantes connexes d'une grille 3D separes par une valeur minimum (int)");
}

// Step detailled description
QString ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetConnectedComponentOnIntGridMultiResolution::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetConnectedComponentOnIntGridMultiResolution(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmScene, CT_Scene::staticGetType(), tr("Scene"));
}

// Creation and affiliation of OUT models
void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponentNonMultis = createNewOutResultModel(DEFout_rsltResultConnectedComponentNonMultis, tr("Connected components (grid3d)"));
    res_rsltConnectedComponentNonMultis->setRootGroup(DEFout_grpGeneral, new CT_StandardItemGroup(), tr("Group General"));

    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrid3d, new CT_StandardItemGroup(), tr("Group Grids"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpCircles, new CT_StandardItemGroup(), tr("Group Group Circles"));

    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3dLabel, new CT_Grid3D<int>(), tr("Grid3d with labeled components"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3dType, new CT_Grid3D<int>(), tr("Grid3d with type components"));

    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmSceneTooSmall, new CT_Scene(), tr("Points in TOO_SMALL"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmSceneEmpty, new CT_Scene(), tr("Points in EMPTY"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmSceneSimplex, new CT_Scene(), tr("Points in SIMPLEX"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmSceneComplex, new CT_Scene(), tr("Points in COMPLEX"));

    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_rsltResultConnectedComponentNonMultis, DEFout_grpGrpCircles, new CT_StandardItemGroup(), tr("Group Grp Circles"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpCircles, DEFout_grpCircles, new CT_StandardItemGroup(), tr("Group Circle"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpCircles, DEFout_itmCircle, new CT_Circle(), tr("Circle") );

    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpSceneComponents, new CT_StandardItemGroup(), tr("Group Grp Scenes"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpSceneComponents, DEFout_grpSceneComponents, new CT_StandardItemGroup(), tr("Group Scene"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpSceneComponents, DEFout_itmSceneComponent, new CT_Scene(), tr("Component From Simplex") );
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Valeur min", "", 0, 9999, 4, _minValue );
    configDialog->addInt("n Min pixels","",1,999999999,_minPixelsInComponent);
    configDialog->addInt("Resolution","",1,999999999, _resolution);
    configDialog->addInt("N Steps Aggreg","",0,999999999, _nStepsAggreg);
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmHitGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmScene);
        if (itemIn_itmHitGrid != NULL && itemIn_itmScene != NULL )
        {
            CT_StandardItemGroup* grpGeneral = new CT_StandardItemGroup(DEFout_grpGeneral, resOut_rsltRaster3D );
            resOut_rsltRaster3D->addGroup( grpGeneral );

            CT_Grid3D<int>* itemOut_itmLabelGrid = new CT_Grid3D<int>( DEFout_itmGrid3dLabel, resOut_rsltRaster3D,
                                                                       itemIn_itmHitGrid->minX(), itemIn_itmHitGrid->minY(), itemIn_itmHitGrid->minZ(),
                                                                       itemIn_itmHitGrid->xArraySize(), itemIn_itmHitGrid->yArraySize(), itemIn_itmHitGrid->zArraySize(),
                                                                       itemIn_itmHitGrid->resolution(),
                                                                       std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            CT_Grid3D<int>* itemOut_itmTypeGrid = new CT_Grid3D<int>( DEFout_itmGrid3dType, resOut_rsltRaster3D,
                                                                      itemIn_itmHitGrid->minX(), itemIn_itmHitGrid->minY(), itemIn_itmHitGrid->minZ(),
                                                                      itemIn_itmHitGrid->xArraySize(), itemIn_itmHitGrid->yArraySize(), itemIn_itmHitGrid->zArraySize(),
                                                                      itemIn_itmHitGrid->resolution(),
                                                                      std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            // Calcule les composantes connexes et les etiquette dans la grille d'etiquettes
            QVector< ConnectedComponent* > connectedComponents;
            labelSuperConnectedComponents( itemIn_itmHitGrid,
                                           itemOut_itmLabelGrid,
                                           _minValue,
                                           _minPixelsInComponent,
                                           _resolution,
                                           connectedComponents );

            // Charge les cercles a utiliser comme position de reference des troncs
            QVector< CT_CircleData* > circles = getCirclesFromFile2( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );

            // Ajoute les cercles au resultat
            CT_StandardItemGroup* grpGrpCircle = new CT_StandardItemGroup(DEFout_grpGrpCircles, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpCircle );
            foreach( CT_CircleData* circle, circles )
            {
                CT_StandardItemGroup* grpCircle = new CT_StandardItemGroup(DEFout_grpCircles, resOut_rsltRaster3D );
                grpGrpCircle->addGroup( grpCircle );
                grpCircle->addItemDrawable( new CT_Circle( DEFout_itmCircle, resOut_rsltRaster3D,
                                              circle ) );
            }

            // Calcule le nombre de cercles qui intersectent chaque composante
            getComponentsType( itemOut_itmLabelGrid,
                               circles,
                               connectedComponents,
                               _minPixelsInComponent );

            // Etiquette chaque composante par son type dans la grille de type en fonction du nombre de cercles qui l'intersectent
            updateComponentsTypeAndLabelTypeGrid( connectedComponents,
                                                  itemOut_itmTypeGrid,
                                                  _minPixelsInComponent );

            // Lance la division recursive pour obtenir des composantes SIMPLEX
            QVector< ConnectedComponent* > finalComponents;
            foreach( ConnectedComponent* component, connectedComponents )
            {
                splitComponentUntilSimplexes( component,
                                              _resolution,
                                              itemIn_itmHitGrid,
                                              _minValue,
                                              itemOut_itmLabelGrid,
                                              itemOut_itmTypeGrid,
                                              _minPixelsInComponent,
                                              finalComponents );
            }

            // On aggrege les composantes aux composantes SIMPLEX
            aggregEmptyComponents3( _nStepsAggreg,
                                    itemIn_itmHitGrid,
                                    itemOut_itmLabelGrid,
                                    itemOut_itmTypeGrid,
                                    finalComponents );

            // On recupere les composantes aggregatrices
            QVector< ConnectedComponent* > aggregComponents;
            getSimplexNonAgregated( finalComponents, aggregComponents );

            // Chacune de ces composantes se fait labeliser par un identifiant negatif
            QVector< CT_PointCloudIndexVector* > componentsScene;
            int negativeID = -1;
            foreach( ConnectedComponent* component, aggregComponents )
            {
                // On marque dans la grille
                markPixelsInGrid( component->_pixels, itemOut_itmLabelGrid, negativeID );

                // On avance le label
                negativeID--;

                // On cree autant de futures scenes que de composantes negatives
                componentsScene.push_back( new CT_PointCloudIndexVector() );
            }

            // On divise la scene en fonction des labels negatifs
            splitSceneIntoComponentsFomNegativeLabels( itemIn_itmScene,
                                                       itemOut_itmLabelGrid,
                                                       componentsScene );

            // On ajoute chaque scene ainsi cree au resultat
            CT_StandardItemGroup* grpGrpSceneComponents = new CT_StandardItemGroup(DEFout_grpGrpSceneComponents, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpSceneComponents );
            foreach( CT_PointCloudIndexVector* pciv, componentsScene )
            {
                CT_StandardItemGroup* grpSceneComponent = new CT_StandardItemGroup(DEFout_grpSceneComponents, resOut_rsltRaster3D );
                grpGrpSceneComponents->addGroup( grpSceneComponent );
                CT_Scene* componentScene = new CT_Scene( DEFout_itmSceneComponent, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(pciv) );
                grpSceneComponent->addItemDrawable( componentScene );
            }


//            int nTotal, nTooSmall, nBigEnough, nNA, nEmpty, nSimplex, nComplex;
//            countComponentsByType( finalComponents,
//                                   nTotal,
//                                   nNA,
//                                   nTooSmall,
//                                   nBigEnough,
//                                   nEmpty,
//                                   nSimplex,
//                                   nComplex );
//            qDebug() << "Total " << nTotal;
//            qDebug() << "NA " << nNA;
//            qDebug() << "TooSmall " << nTooSmall;
//            qDebug() << "BigEnough " << nBigEnough;
//            qDebug() << "Empty " << nEmpty;
//            qDebug() << "Simplex " << nSimplex;
//            qDebug() << "Complex " << nComplex;

            // Ajout de la grille au resultat
            itemOut_itmLabelGrid->computeMinMax();
            itemOut_itmTypeGrid->computeMinMax();
            CT_StandardItemGroup* grpGrid3D = new CT_StandardItemGroup(DEFout_grpGrid3d, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrid3D );
            grpGrid3D->addItemDrawable( itemOut_itmLabelGrid );
            grpGrid3D->addItemDrawable( itemOut_itmTypeGrid );

            // On divise le nuage de points
            CT_PointCloudIndexVector* tooSmallIndices= new CT_PointCloudIndexVector();
            tooSmallIndices->setSortType(CT_AbstractCloudIndex::NotSorted);
            CT_PointCloudIndexVector* emptyIndices= new CT_PointCloudIndexVector();
            emptyIndices->setSortType(CT_AbstractCloudIndex::NotSorted);
            CT_PointCloudIndexVector* simplexIndices= new CT_PointCloudIndexVector();
            simplexIndices->setSortType(CT_AbstractCloudIndex::NotSorted);
            CT_PointCloudIndexVector* complexIndices= new CT_PointCloudIndexVector();
            complexIndices->setSortType(CT_AbstractCloudIndex::NotSorted);
            splitSceneIntoComponents( itemIn_itmScene,
                                      itemOut_itmTypeGrid,
                                      tooSmallIndices,
                                      emptyIndices,
                                      simplexIndices,
                                      complexIndices );

            // Ajout des nuages au resultat
            CT_Scene* tooSmallScene = new CT_Scene( DEFout_itmSceneTooSmall, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(tooSmallIndices) );
            CT_Scene* emptyScene = new CT_Scene( DEFout_itmSceneEmpty, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(emptyIndices) );
            CT_Scene* simplexScene = new CT_Scene( DEFout_itmSceneSimplex, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(simplexIndices) );
            CT_Scene* complexScene = new CT_Scene( DEFout_itmSceneComplex, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(complexIndices) );
            grpGrid3D->addItemDrawable( tooSmallScene );
            grpGrid3D->addItemDrawable( emptyScene );
            grpGrid3D->addItemDrawable( simplexScene );
            grpGrid3D->addItemDrawable( complexScene );
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::labelSuperConnectedComponents(CT_Grid3D<int> *inputGrid,
                                                                                           CT_Grid3D<int> *outLabeledGrid,
                                                                                           double minValue,
                                                                                           int minLabelSize,
                                                                                           int resolution,
                                                                                           QVector< ConnectedComponent* >& outConnectedComponents )
{
    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();

    int xdimSuperPixel = xdim / resolution;
    int ydimSuperPixel = ydim / resolution;
    int zdimSuperPixel = zdim / resolution;

    if( (xdim % resolution) != 0 )
    {
        xdimSuperPixel++;
    }

    if( (ydim % resolution) != 0 )
    {
        ydimSuperPixel++;
    }

    if( (zdim % resolution) != 0 )
    {
        zdimSuperPixel++;
    }

    for( int i = 0 ; i < xdimSuperPixel ; i++ )
    {
        for( int j = 0 ; j < ydimSuperPixel ; j++ )
        {
            for( int k = 0 ; k < zdimSuperPixel ; k++ )
            {
                SuperPixel currentPixel(i,j,k,resolution,inputGrid);

                // Si le super pixel courant contient un depart de graine pour une composante connexe
                if( currentPixel.containsNonLabeledPixelAboveValue(inputGrid,
                                                                   outLabeledGrid,
                                                                   minValue ) )
                {
                    // On recupere la composante connexe issue de ce super pixel
                    ConnectedComponent* currentConnectedComponent = new ConnectedComponent();
                    labelSuperConnectedComponentFromSeed( inputGrid,
                                                          outLabeledGrid,
                                                          minValue,
                                                          i, j, k,
                                                          ConnectedComponent::_labelID,
                                                          resolution,
                                                          currentConnectedComponent );

                    // VERSION 2
                    // On l'ajoute a l'ensemble des composantes
                    outConnectedComponents.push_back( currentConnectedComponent );

                    // On met a jour son label
                    currentConnectedComponent->_label = ConnectedComponent::_labelID;

                    // Et on avance le label
                    ConnectedComponent::_labelID++;
                    // VERSION 2

//                    // Si la composante contient assez de supers pixels
//                    if( currentConnectedComponent->_pixels.size() >= minLabelSize )
//                    {
//                        // On l'ajoute a l'ensemble des composantes
//                        outConnectedComponents.push_back( currentConnectedComponent );
//
//                        // On met a jour son label
//                        currentConnectedComponent->_label = ConnectedComponent::_labelID;

//                        // Et on avance le label
//                        ConnectedComponent::_labelID++;
//                    }

//                    else
//                    {
//                        // La composante est trop petite il faut demarquer la super composante
//                        // On la marque a -1 pour ne pas repasser dessus (la remettre a NA en ferait une nouvelle graine potentielle)
//                        markSuperComponent( currentConnectedComponent,
//                                            outLabeledGrid,
//                                            TOO_SMALL );

//                        // On met a jour son label et son type
//                        currentConnectedComponent->_label = TOO_SMALL;
//                        currentConnectedComponent->_type = TOO_SMALL;
//                    }
                }
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *hitGrid,
                                                                                                  CT_Grid3D<int> *outLabeledGrid,
                                                                                                  double minValue,
                                                                                                  int startx,
                                                                                                  int starty,
                                                                                                  int startz,
                                                                                                  int label,
                                                                                                  int resolution,
                                                                                                  ConnectedComponent* outSuperComponent)
{
    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< SuperPixel > f;

    // WARNING : Les startx starty startz sont les coordonnees du super pixel
    SuperPixel firstSuperPixel( startx,
                                starty,
                                startz,
                                resolution,
                                hitGrid );

    // On marque le premier super pixel
    firstSuperPixel.setValueInGrid( outLabeledGrid,
                                    label );

    // Ajoute le super pixel a la composante
    outSuperComponent->_superPixels.append( firstSuperPixel );

    // Ajoute les pixels du super pixels a la composante
    addPixelsToSuperConnectedComponent( firstSuperPixel,
                                        outSuperComponent );

    // On l'ajoute a la file
    f.enqueue( firstSuperPixel );

    // Tant que la file n'est pas vide
    SuperPixel currentSuperPixel( resolution );
    while( !f.empty() )
    {
        // On prend le premier element de la file
        currentSuperPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            if( currentSuperPixel._p.x() + i >= 0 )
            {
                for( int j = -1 ; j <= 1 ; j++ )
                {
                    if( currentSuperPixel._p.y() + j >= 0 )
                    {
                        for( int k = -1 ; k <= 1 ; k++ )
                        {
                            if( currentSuperPixel._p.z() + k >= 0 )
                            {
                                SuperPixel neiSuperPixel( currentSuperPixel._p.x() + i,
                                                          currentSuperPixel._p.y() + j,
                                                          currentSuperPixel._p.z() + k,
                                                          resolution,
                                                          hitGrid );

                                // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                                // Si le voisin n'est pas deja marque et si il a une valeur superieure a la valeur minimum souhaitee
                                if( neiSuperPixel.containsNonLabeledPixelAboveValue( hitGrid,
                                                                                     outLabeledGrid,
                                                                                     minValue ) )
                                {
                                    // Marque le super pixel
                                    neiSuperPixel.setValueInGrid( outLabeledGrid,
                                                                  label );

                                    // Ajoute le super pixel a la composante
                                    outSuperComponent->_superPixels.append( neiSuperPixel );

                                    // Ajoute les pixels a la composante
                                    addPixelsToSuperConnectedComponent( neiSuperPixel,
                                                                        outSuperComponent );

                                    // Ajoute le super pixel a al file
                                    f.enqueue( neiSuperPixel );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::markSuperComponent(ConnectedComponent *superComponent,
                                                                                CT_Grid3D<int> *grid,
                                                                                int label)
{
    foreach( SuperPixel p, superComponent->_superPixels )
    {
        p.setValueInGrid( grid, label );
    }
}

bool ICRO_StepGetConnectedComponentOnIntGridMultiResolution::superPixelContainsValuablePixel(Eigen::Vector3i &superPixel,
                                                                                             CT_Grid3D<int> *hitGrid,
                                                                                             CT_Grid3D<int> *labelGrid,
                                                                                             int minValue,
                                                                                             int resolution )
{
    // Convertit le super pixel en pixels
    int botx = superPixel.x() * resolution;
    int boty = superPixel.y() * resolution;
    int botz = superPixel.z() * resolution;

    int topx = (superPixel.x()+1) * resolution;
    int topy = (superPixel.y()+1) * resolution;
    int topz = (superPixel.z()+1) * resolution;

    // Parcours tous les pixels du super pixel pour les marquer
    for( int x = botx ; x <= topx ; x++ )
    {
        for( int y = boty ; y <= topy ; y++ )
        {
            for( int z = botz ; z <= topz ; z++ )
            {
                if( labelGrid->value( x, y, z ) == labelGrid->NA() &&
                    hitGrid->value( x, y, z ) >= minValue )
                {
                    return true;
                }
            }
        }
    }

    return false;
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::addPixelsToSuperConnectedComponent(SuperPixel& superPixel,
                                                                                                ConnectedComponent *outSuperComponent )
{
    // Recupere les pixels du super pixel
    QVector< Eigen::Vector3i > pixelsFromSuperPixel;
    superPixel.getPixels( pixelsFromSuperPixel );

    // Ajoute le vecteur de pixels
    outSuperComponent->_pixels << pixelsFromSuperPixel;
}

QVector<CT_CircleData*> ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getCirclesFromFile(QString dir,
                                                                                                   QString fileName)
{
    QVector<CT_CircleData*> rslt;
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            rslt.push_back( new CT_CircleData( center, direction, r ) );
        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }

    qDebug() << "Loaded " << rslt.size() << " cercles";
    return rslt;
}

QVector<CT_CircleData *> ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getCirclesFromFile2(QString dir, QString fileName)
{
    QVector<CT_CircleData*> rslt;
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            CT_CircleData* currCircle = new CT_CircleData( center, direction, r );

            if( circleIntersect2D( rslt, currCircle ) )
            {
                qDebug() << "Intersect";
                delete currCircle;
            }

            else
            {
                rslt.push_back( currCircle );
            }

        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }

    qDebug() << "Loaded " << rslt.size() << " cercles";
    return rslt;
}

bool ICRO_StepGetConnectedComponentOnIntGridMultiResolution::circleIntersect2D(QVector<CT_CircleData *> &circles, CT_CircleData *c)
{
    foreach( CT_CircleData* curr, circles )
    {
        if( circleIntersect2D( curr, c ) )
        {
            return true;
        }
    }

    return false;
}

bool ICRO_StepGetConnectedComponentOnIntGridMultiResolution::circleIntersect2D(CT_CircleData *c1, CT_CircleData *c2)
{
    CT_Point ce1 = c1->getCenter();
    CT_Point ce2 = c2->getCenter();
    float dist2D = sqrt( (ce1.x()-ce2.x())*(ce1.x()-ce2.x()) + (ce1.y()-ce2.y())*(ce1.y()-ce2.y()) );

    if( dist2D < (c1->getRadius() + c2->getRadius()) )
    {
        return true;
    }

    return false;
}

ConnectedComponent *ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getComponentFromLabel(QVector<ConnectedComponent *> &components,
                                                                                                  int label )
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_label == label )
        {
            return component;
        }
    }

    return NULL;
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::updateComponentsTypeAndLabelTypeGrid(QVector<ConnectedComponent *> &components,
                                                                                                  CT_Grid3D<int>* outTypeGrid,
                                                                                                  int minComponentSize )
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minComponentSize )
        {
            component->_type = TOO_SMALL;
        }

        else
        {
            if( component->_nCircles == 0 )
            {
                component->_type = EMPTY;
            }

            else if( component->_nCircles == 1 )
            {
                component->_type = SIMPLEX;
            }

            else if( component->_nCircles >= 2 )
            {
                component->_type = COMPLEX;
            }
        }

        markSuperComponent( component, outTypeGrid, component->_type );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getComponentsType(CT_Grid3D<int> *labeledGrid,
                                                                               QVector< CT_CircleData* >& circles,
                                                                               QVector< ConnectedComponent* >& components,
                                                                               int minLabelSize )
{
    // Attribue le type TOO_SMALL
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minLabelSize )
        {
            component->_type = TOO_SMALL;
        }
    }

    // Pour chaque cercle
    foreach( CT_CircleData* circleData, circles )
    {
        // Labels traverses par le cercle
        QVector<int> labelsOfCircle;

        // On regarde si le cercle touche un pixel de la composante courante
        // BBox du cercle dans la grille
        size_t botx, boty, botz;
        labeledGrid->colX( circleData->getCenter().x() - circleData->getRadius(), botx );
        labeledGrid->linY( circleData->getCenter().y() - circleData->getRadius(), boty );
        labeledGrid->levelZ( circleData->getCenter().z(), botz );

        size_t topx, topy, topz;
        labeledGrid->colX( circleData->getCenter().x() + circleData->getRadius(), topx );
        labeledGrid->linY( circleData->getCenter().y() + circleData->getRadius(), topy );
        labeledGrid->levelZ( circleData->getCenter().z(), topz );

        // Pour chaque pixel qu'intersecte le cercle courant
        for( int i = botx ; i <= topx ; i++ )
        {
            for( int j = boty ; j <= topy ; j++ )
            {
                for( int k = botz ; k <= topz ; k++ )
                {
//                    qDebug() << "If 0 --";
                    // Si une etiquette coupe le cercle : la valeur a ete etiquette (different de NA) et la composante est assez grande
                    if( labeledGrid->value(i,j,k) != labeledGrid->NA() )
//                        &&
//                        labeledGrid->value(i,j,k) != TOO_SMALL )
                    {
//                        qDebug() << "If 1 --";
                        ConnectedComponent* fromLabel = getComponentFromLabel( components, labeledGrid->value(i,j,k) );
                        if( fromLabel != NULL && fromLabel->_type != TOO_SMALL )
                        {
//                            qDebug() << "If 2 --";
                            // Et qu'on ne l'avait pas encore traversee pour ce cercle
                            if( !labelsOfCircle.contains( labeledGrid->value(i,j,k) ) )
                            {
//                                qDebug() << "Get from label --";
                                // On recupere la composante associee
                                ConnectedComponent* component = getComponentFromLabel( components,
                                                                                       labeledGrid->value(i,j,k) );
//                                qDebug() << "Get from label ok";

//                                qDebug() << "If 3 --";
                                if( component != NULL )
                                {
                                    // On l'ajoute au tableau des composantes traversees
                                    labelsOfCircle.push_back( labeledGrid->value(i,j,k) );

                                    // La composante intersecte un cercle de plus
                                    component->_nCircles++;
                                }
//                                qDebug() << "If 3 ok";
                            }
//                            qDebug() << "If 2 ok";
                        }
//                        qDebug() << "If 1 ok";
                    }
//                    qDebug() << "If 0 ok";
                }
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::labelComponentInGridFromPixels(ConnectedComponent *component,
                                                                                            CT_Grid3D<int> *grid,
                                                                                            int label)
{
    foreach( Eigen::Vector3i v, component->_pixels )
    {
        grid->setValue( v.x(), v.y(), v.z(), label );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::splitComplexComponent(ConnectedComponent *component,
                                                                                   int initialResolution,
                                                                                   CT_Grid3D<int> *valueGrid,
                                                                                   int minValue,
                                                                                   CT_Grid3D<int> *labelGrid,
                                                                                   CT_Grid3D<int> *typeGrid,
                                                                                   int minLabelSize,
                                                                                   QVector< ConnectedComponent* >& outComponents )
{
    if( initialResolution > 1 )
    {
        // On clear les labels et type de la composante
        markSuperComponent( component,
                            labelGrid,
                            labelGrid->NA() );

        markSuperComponent( component,
                            typeGrid,
                            typeGrid->NA() );

        // On transforme chaque super pixel de la composante en super pixel de resolution plus fine
        int currentResolution = initialResolution / 2;
        QVector< SuperPixel > superPixelsAtCurrentResolution;
        foreach( SuperPixel p, component->_superPixels )
        {
            // Chaque super pixel se transforme en huit super pixels de resolution plus fine
            for( int i = 0 ; i < 2 ; i++ )
            {
                for( int j = 0 ; j < 2 ; j++ )
                {
                    for( int k = 0 ; k < 2 ; k++ )
                    {
                        SuperPixel subSuperPixel( 2*p._p.x() + i,
                                                  2*p._p.y() + j,
                                                  2*p._p.z() + k,
                                                  currentResolution,
                                                  valueGrid );
                        superPixelsAtCurrentResolution.append( subSuperPixel );
                    }
                }
            }
        }

        // On remplace le tableau de super pixels
        component->_superPixels.clear();
        component->_superPixels = superPixelsAtCurrentResolution;

        // ////////////////////////////////////////////////////////////////////////////////
        // On lance une recherche de composantes connexes a partir de cette resolution
        // ////////////////////////////////////////////////////////////////////////////////
        foreach( SuperPixel sp, component->_superPixels )
        {
            if( sp.containsNonLabeledPixelAboveValue( valueGrid, labelGrid, minValue ) )
            {
                ConnectedComponent* connectedComponentAtCurrentResolution = new ConnectedComponent();
                labelSuperConnectedComponentFromSeed( valueGrid,
                                                      labelGrid,
                                                      minValue,
                                                      sp._p.x(), sp._p.y(), sp._p.z(),
                                                      ConnectedComponent::_labelID,
                                                      currentResolution,
                                                      connectedComponentAtCurrentResolution );

//                // Si la composante contient assez de supers pixels
//                if( connectedComponentAtCurrentResolution->_pixels.size() >= minLabelSize )
//                {
                    // On l'ajoute a l'ensemble des composantes
                    outComponents.push_back( connectedComponentAtCurrentResolution );

                    // On met a jour son label
                    connectedComponentAtCurrentResolution->_label = ConnectedComponent::_labelID;

                    // Et on avance le label
                    ConnectedComponent::_labelID++;
//                }

//                else
//                {
//                    // La composante est trop petite il faut demarquer la super composante
//                    // On la marque a -1 pour ne pas repasser dessus (la remettre a NA en ferait une nouvelle graine potentielle)
//                    markSuperComponent( connectedComponentAtCurrentResolution,
//                                        labelGrid,
//                                        TOO_SMALL );

//                    // On met a jour son label et son type
//                    connectedComponentAtCurrentResolution->_label = TOO_SMALL;
//                    connectedComponentAtCurrentResolution->_type = TOO_SMALL;
//                }
            }
        }
    }

    else
    {
        // La resolution est de 1 ou 0 donc on renvoie la composante telle quelle
        outComponents.push_back( component );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::splitComponentUntilSimplexes(ConnectedComponent *component,
                                                                                          int initialResolution,
                                                                                          CT_Grid3D<int> *valueGrid,
                                                                                          int minValue,
                                                                                          CT_Grid3D<int> *labelGrid,
                                                                                          CT_Grid3D<int> *typeGrid,
                                                                                          int minLabelSize,
                                                                                          QVector<ConnectedComponent *>& outComponents)
{
    // Et que la composante est complexe
    if( component->_type == COMPLEX )
    {
//        qDebug() << "Composante" << component->_label << "at resolution " << initialResolution << " : COMPLEX";
        // Si la resolution est divisible
        if( initialResolution > 1 )
        {
//            qDebug() << "Split Complex --";
            // On divise la composante complexe
            QVector<ConnectedComponent *> splitedComponentsAtCurrentResolution;
            splitComplexComponent( component,
                                   initialResolution,
                                   valueGrid,
                                   minValue,
                                   labelGrid,
                                   typeGrid,
                                   minLabelSize,
                                   splitedComponentsAtCurrentResolution );
//            qDebug() << "Split Complex ok";

            // On rattache les composantes a leur au pere
            foreach( ConnectedComponent* splitedComponentAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                component->_children.push_back( splitedComponentAtCurrentResolution );
            }

            // Charge les cercles a utiliser comme position de reference des troncs
            QVector< CT_CircleData* > circles = getCirclesFromFile2( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );

            // Calcule le nombre de cercles qui intersectent chaque composante
//            qDebug() << "GET component type --";
            getComponentsType( labelGrid,
                               circles,
                               splitedComponentsAtCurrentResolution,
                               _minPixelsInComponent );
//            qDebug() << "GET component type ok";

            // Etiquette chaque composante par son type dans la grille de type en fonction du nombre de cercles qui l'intersectent
//            qDebug() << "UPDATE component type --";
            updateComponentsTypeAndLabelTypeGrid( splitedComponentsAtCurrentResolution,
                                                  typeGrid,
                                                  minLabelSize );
//            qDebug() << "UPDATE component type ok";

            // Pour chaque composante creee par la division on continue le processus
            foreach( ConnectedComponent* splitedComponentAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                splitComponentUntilSimplexes( splitedComponentAtCurrentResolution,
                                              initialResolution / 2,
                                              valueGrid,
                                              minValue,
                                              labelGrid,
                                              typeGrid,
                                              minLabelSize,
                                              outComponents );
            }
        }

        // Si la composante est complexe mais que l'on ne peut plus reduire la resolution
        else
        {
//            qDebug() << "Else 2 --";
            // On ajoute simplement la composante au resultat
            component->_resolutionFinale = initialResolution;
            outComponents.push_back( component );
//            qDebug() << "Else 2 --";
        }
    }

    // Si la composante est VIDE ou SIMPLEX (les TOO_SMALL ne sont pas traitees ici car elles n'apparaissent pas dans le tableau de composantes connexes)
    else
    {
//        qDebug() << "Else 1 --";
        // On l'ajoute a la sortie
        component->_resolutionFinale = initialResolution;
        outComponents.push_back( component );
//        qDebug() << "Else 1 ok";
//        qDebug() << "Composante" << component->_label << "at resolution " << initialResolution << " : EMPTY or SIMPLEX";
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::countComponentsByType(QVector<ConnectedComponent *> &components,
                                                                                   int &outNTotal,
                                                                                   int &outNNA,
                                                                                   int &outNTooSmall,
                                                                                   int &outNBigEnough,
                                                                                   int &outNEmpty,
                                                                                   int &outNSimplex,
                                                                                   int &outNComplex)
{
    outNTotal = components.size();
    outNTooSmall = 0;
    outNBigEnough = 0;
    outNEmpty = 0;
    outNSimplex = 0;
    outNComplex = 0;
    outNNA = 0;

    foreach( ConnectedComponent* component, components )
    {
        switch( component->_type )
        {
            case NA :
            {
                outNNA++;
                break;
            }
            case TOO_SMALL :
            {
                outNTooSmall++;
                break;
            }
            case EMPTY :
            {
                outNEmpty++;
                break;
            }
            case SIMPLEX :
            {
                outNSimplex++;
                break;
            }
            case COMPLEX :
            {
                outNComplex++;
                break;
            }
        }
    }

    outNBigEnough = outNTotal - outNTooSmall;
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::printComponentsResolution(QVector<ConnectedComponent *> &components)
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_type == SIMPLEX )
        {
            qDebug() << "Composante SIMPLEX de label " << component->_label << " s'est arrete a " << component->_resolutionFinale;
        }
        if( component->_type == COMPLEX )
        {
            qDebug() << "Composante COMPLEX de label " << component->_label << " s'est arrete a " << component->_resolutionFinale;
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::splitSceneIntoComponents(const CT_Scene *inputScene,
                                                                                      CT_Grid3D<int>* typeGrid,
                                                                                      CT_PointCloudIndexVector *outTooSmall,
                                                                                      CT_PointCloudIndexVector *outEmpty,
                                                                                      CT_PointCloudIndexVector *outSimplex,
                                                                                      CT_PointCloudIndexVector *outComplex)
{
    CT_PointIterator itP(inputScene->getPointCloudIndex());
    while(itP.hasNext())
    {
        const CT_Point &point = itP.next().currentPoint();
        size_t index = itP.currentGlobalIndex();

        switch( typeGrid->valueAtXYZ( point.x(), point.y(), point.z()) )
        {
            case TOO_SMALL :
            {
                outTooSmall->addIndex( index );
                break;
            }
            case EMPTY :
            {
                outEmpty->addIndex( index );
                break;
            }
            case SIMPLEX :
            {
                outSimplex->addIndex( index );
                break;
            }
            case COMPLEX :
            {
                outComplex->addIndex( index );
                break;
            }
            default :
            {
                break;
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponents(int maxDilatation,
                                                                                   CT_Grid3D<int>* valueImage,
                                                                                   CT_Grid3D<int>* labelImage,
                                                                                   CT_Grid3D<int>* typeImage,
                                                                                   QVector< ConnectedComponent* >& components)
{
    for( int i = 0 ; i < maxDilatation ; i++ )
    {
        aggregEmptyComponentsSingleDilatation( valueImage,
                                               labelImage,
                                               typeImage,
                                               components );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponents2(int maxDilatation, CT_Grid3D<int> *valueImage, CT_Grid3D<int> *labelImage, CT_Grid3D<int> *typeImage, QVector<ConnectedComponent *> &components)
{
    for( int i = 0 ; i < maxDilatation ; i++ )
    {
        qDebug() << "---------- Agregation " << i << " sur " << maxDilatation;
        aggregEmptyComponentsSingleDilatation2( valueImage,
                                                labelImage,
                                                typeImage,
                                                components );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponents3(int maxDilatation, CT_Grid3D<int> *valueImage, CT_Grid3D<int> *labelImage, CT_Grid3D<int> *typeImage, QVector<ConnectedComponent *> &components)
{
    CT_Grid3D<bool>* markImage = new CT_Grid3D<bool>( NULL, NULL,
                                                      valueImage->minX(), valueImage->minY(), valueImage->minZ(),
                                                      valueImage->xArraySize(), valueImage->yArraySize(), valueImage->zArraySize(),
                                                      valueImage->resolution(),
                                                      false, false );

    for( int i = 0 ; i < maxDilatation ; i++ )
    {
        qDebug() << "---------- Agregation " << i << " sur " << maxDilatation;
        aggregEmptyComponentsSingleDilatation3( valueImage,
                                                labelImage,
                                                typeImage,
                                                markImage,
                                                components );
    }

    delete markImage;
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponentsSingleDilatation(CT_Grid3D<int>* valueImage,
                                                                                                   CT_Grid3D<int>* labelImage,
                                                                                                   CT_Grid3D<int>* typeImage,
                                                                                                   QVector< ConnectedComponent* >& components )
{
    int xdim = valueImage->xArraySize();
    int ydim = valueImage->yArraySize();
    int zdim = valueImage->zArraySize();

    // On parcours toute l'image
    for( int i = 0 ; i < xdim ; i++ )
    {
        for( int j = 0 ; j < ydim ; j++ )
        {
            for( int k = 0 ; k < zdim ; k++ )
            {
                // Est ce que le pixel est un pixel de type SIMPLEX ?
                if( typeImage->value(i,j,k) == SIMPLEX )
                {
                    // Alors on met tout son voisinage au meme label
                    int botx = std::max(0,i-1);
                    int boty = std::max(0,j-1);
                    int botz = std::max(0,k-1);
                    int topx = std::min(xdim-1,i+1);
                    int topy = std::min(ydim-1,j+1);
                    int topz = std::min(zdim-1,k+1);
                    for( int ii = botx ; ii <= topx ; ii++ )
                    {
                        for( int jj = boty ; jj <= topy ; jj++ )
                        {
                            for( int kk = botz ; kk <= topz ; kk++ )
                            {
                                // Si le pixel voisin appartient a une autre composante
                                if( labelImage->value(ii,jj,kk) != labelImage->NA() )
                                {
                                    // Et que cette composante n'est pas une composante SIMPLEX ou COMPLEX
                                    if( typeImage->value(ii,jj,kk) != SIMPLEX && typeImage->value(ii,jj,kk) != COMPLEX )
                                    {
                                        ConnectedComponent* toAdd = getComponentFromLabel( components, labelImage->value(ii,jj,kk) );
                                        assert( toAdd != NULL );

                                        // On labelise cette composante avec le meme label que le pixel qui a servi a l'atteindre
                                        markSuperComponent( toAdd, labelImage, labelImage->value(i,j,k) );

                                        // Et on met son type a SIMPLEX
                                        markSuperComponent( toAdd, typeImage, SIMPLEX );

                                        qDebug() << "Les composantes " << labelImage->value(i,j,k) << " et " << toAdd->_label << " ont ete fusionnees";
                                    }
                                }

                                else
                                {
                                    // Affectation du meme label
                                    labelImage->setValue(ii,jj,kk,labelImage->value(i,j,k));

                                    // Et affectation du meme type
                                    typeImage->setValue(ii,jj,kk,typeImage->value(i,j,k));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponentsSingleDilatation2(CT_Grid3D<int> *valueImage,
                                                                                                    CT_Grid3D<int> *labelImage,
                                                                                                    CT_Grid3D<int> *typeImage,
                                                                                                    QVector<ConnectedComponent *> &components)
{
    int xdim = valueImage->xArraySize();
    int ydim = valueImage->yArraySize();
    int zdim = valueImage->zArraySize();

    // Pour chaque composante
    foreach( ConnectedComponent* component, components )
    {
        // Si la composante n'a pas ete agregee avant
        if( component->_wasFused == false )
        {
            QVector< Pixel > pixelsAddedToComponent;
            QVector< int > labelsAddedToComponent;

            // Si la composante est une SIMPLEX
            if( component->_type == SIMPLEX )
            {
                // Pour chaque pixel
                foreach ( Pixel p, component->_pixels )
                {
                    // Alors on met tout son voisinage au meme label
                    int botx = std::max(0,p.x()-1);
                    int boty = std::max(0,p.y()-1);
                    int botz = std::max(0,p.z()-1);
                    int topx = std::min(xdim-1,p.x()+1);
                    int topy = std::min(ydim-1,p.y()+1);
                    int topz = std::min(zdim-1,p.z()+1);
                    for( int ii = botx ; ii <= topx ; ii++ )
                    {
                        for( int jj = boty ; jj <= topy ; jj++ )
                        {
                            for( int kk = botz ; kk <= topz ; kk++ )
                            {
                                // Si le pixel voisin n'appartient a aucune composante
                                if( labelImage->value(ii,jj,kk) == labelImage->NA() )
                                {
                                    // On doit l'ajouter a la liste des pixels a ajouter a la composante courante
                                    Pixel pAdd; pAdd << ii, jj, kk;
                                    pixelsAddedToComponent.push_back( pAdd );
                                }

                                // Si le pixel voisin n'appartient pas a la meme composante
                                else if( labelImage->value(ii,jj,kk) != component->_label )
                                {
                                    // Et que cette composante n'a pas deja ete fusionnee
                                    if( !labelsAddedToComponent.contains( labelImage->value(ii,jj,kk) ) )
                                    {
                                        // Et que cette composante n'est pas une composante SIMPLEX ou COMPLEX
                                        if( typeImage->value(ii,jj,kk) != SIMPLEX && typeImage->value(ii,jj,kk) != COMPLEX )
                                        {
                                            ConnectedComponent* toAdd = getComponentFromLabel( components, labelImage->value(ii,jj,kk) );
                                            // Et qu'elle n'a pas deja ete aggregee
                                            if( toAdd->_wasFused == false )
                                            {
                                                if( toAdd == NULL )
                                                {
                                                    qDebug() << "Cherche composante " << labelImage->value(ii,jj,kk);
                                                }
                                                assert( toAdd != NULL );

                                                component->_fused.push_back( toAdd );
                                                labelsAddedToComponent.push_back( toAdd->_label );
                                                toAdd->_wasFused = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Ainsi que pour chaque pixel des composantes deja fusionnees
                foreach( ConnectedComponent* fusedComponent, component->_fused )
                {
                    foreach ( Pixel p, fusedComponent->_pixels )
                    {
                        // Alors on met tout son voisinage au meme label
                        int botx = std::max(0,p.x()-1);
                        int boty = std::max(0,p.y()-1);
                        int botz = std::max(0,p.z()-1);
                        int topx = std::min(xdim-1,p.x()+1);
                        int topy = std::min(ydim-1,p.y()+1);
                        int topz = std::min(zdim-1,p.z()+1);
                        for( int ii = botx ; ii <= topx ; ii++ )
                        {
                            for( int jj = boty ; jj <= topy ; jj++ )
                            {
                                for( int kk = botz ; kk <= topz ; kk++ )
                                {
                                    // Si le pixel voisin n'appartient a aucune composante
                                    if( labelImage->value(ii,jj,kk) == labelImage->NA() )
                                    {
                                        // On doit l'ajouter a la liste des pixels a ajouter a la composante courante
                                        Pixel pAdd; pAdd << ii, jj, kk;
                                        pixelsAddedToComponent.push_back( pAdd );
                                    }

                                    // Si le pixel voisin n'appartient pas a la meme composante
                                    else if( labelImage->value(ii,jj,kk) != component->_label )
                                    {
                                        // Et que cette composante n'a pas deja ete fusionnee
                                        if( !labelsAddedToComponent.contains( labelImage->value(ii,jj,kk) ) )
                                        {
                                            // Et que cette composante n'est pas une composante SIMPLEX ou COMPLEX
                                            if( typeImage->value(ii,jj,kk) != SIMPLEX && typeImage->value(ii,jj,kk) != COMPLEX )
                                            {
                                                ConnectedComponent* toAdd = getComponentFromLabel( components, labelImage->value(ii,jj,kk) );
                                                // Et qu'elle n'a pas deja ete aggregee
                                                if( toAdd->_wasFused == false )
                                                {
                                                    if( toAdd == NULL )
                                                    {
                                                        qDebug() << "Cherche composante " << labelImage->value(ii,jj,kk);
                                                    }
                                                    assert( toAdd != NULL );

                                                    component->_fused.push_back( toAdd );
                                                    labelsAddedToComponent.push_back( toAdd->_label );
                                                    toAdd->_wasFused = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // On ajoute tous les pixels a la composante courante
//            qDebug() << "toto 1";
            component->_pixels += pixelsAddedToComponent;
//            qDebug() << "titi 1";

            // Et on marque ces pixels avec la meme etiquette et le meme type
            markPixelsInGrid( pixelsAddedToComponent, labelImage, component->_label );
            markPixelsInGrid( pixelsAddedToComponent, typeImage, component->_type );

            // Cette composante devient elle aussi SIMPLEX
            // Et elle prend le meme label
            foreach( ConnectedComponent* co, component->_fused )
            {
                co->_type = SIMPLEX;
                co->_label = component->_label;
                co->_wasFused = true;
                markSuperComponent( co, labelImage, co->_label );
                markSuperComponent( co, typeImage, co->_type );
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::aggregEmptyComponentsSingleDilatation3(CT_Grid3D<int> *valueImage,
                                                                                                    CT_Grid3D<int> *labelImage,
                                                                                                    CT_Grid3D<int> *typeImage,
                                                                                                    CT_Grid3D<bool> *markImage,
                                                                                                    QVector<ConnectedComponent *> &components)
{
    // Pour chaque composante
    foreach( ConnectedComponent* component, components )
    {
        // Si la composante est une SIMPLEX et qu'elle n'a pas ete aggregee a une autre avant
        if( (component->_type == SIMPLEX || component->_type == COMPLEX ) && component->_wasFused == false )
        {
            // On recupere les pixels du bords de la composante
            QVector< Pixel > externalPixels;
            getExternalPixelsOfConnectedComponent( labelImage, markImage, component, externalPixels );

            // Pour chaque pixel de bord
            foreach( Pixel p, externalPixels )
            {
                // Si le pixel n'appartient a aucune composante
                if( labelImage->value( p.x(), p.y(), p.z() ) == labelImage->NA() )
                {
                    // On ajoute ce pixel a la composante
                    component->_pixels.push_back( p );

                    // Et on marque p avec le meme label que la composante et le meme type
                    labelImage->setValue( p.x(), p.y(), p.z(), component->_label );
                    typeImage->setValue( p.x(), p.y(), p.z(), component->_type );
                }

                // Sinon, si le pixel n'appartient pas a la meme composante
                if( labelImage->value( p.x(), p.y(), p.z() ) != component->_label )
                {
                    // On recupere cette composante
                    ConnectedComponent* toAddComponent = getComponentFromLabel( components, labelImage->value( p.x(), p.y(), p.z() ) );
                    assert( toAddComponent != NULL );

                    // Et que cette composante n'est pas une SIMPLEX ni une COMPLEX et n'a pas deja ete agregee
                    if( toAddComponent->_type != SIMPLEX && toAddComponent->_type != COMPLEX && toAddComponent->_wasFused == false )
                    {
                        // Il faut l'aggreger a la composante courante
                        component->_pixels += toAddComponent->_pixels;

                        // Et il faut etiquetter les pixels correspondant avec l'etiquette de la composante aggregatrice et le meme type
                        markSuperComponent( toAddComponent, labelImage, component->_label );
                        markSuperComponent( toAddComponent, typeImage, component->_type );

                        // On la marque comme aggregee
                        toAddComponent->_wasFused = true;
                    }
                }
            }
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::markPixelsInGrid(QVector<Pixel> &pixels, CT_Grid3D<int> *grid, int value)
{
    foreach( Pixel p, pixels )
    {
        grid->setValue( p.x(), p.y(), p.z(), value );
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getExternalPixelsOfConnectedComponent(CT_Grid3D<int> *labelGrid,
                                                                                                   CT_Grid3D<bool> *markGrid,
                                                                                                   ConnectedComponent *component,
                                                                                                   QVector<Pixel> &outPixels)
{
    int xdim = labelGrid->xArraySize();
    int ydim = labelGrid->yArraySize();
    int zdim = labelGrid->zArraySize();

    outPixels.clear();
    foreach( Pixel p, component->_pixels )
    {
        // Pour tous les voisins
        int botx = std::max(0,p.x()-1);
        int boty = std::max(0,p.y()-1);
        int botz = std::max(0,p.z()-1);
        int topx = std::min(xdim-1,p.x()+1);
        int topy = std::min(ydim-1,p.y()+1);
        int topz = std::min(zdim-1,p.z()+1);
        for( int ii = botx ; ii <= topx ; ii++ )
        {
            for( int jj = boty ; jj <= topy ; jj++ )
            {
                for( int kk = botz ; kk <= topz ; kk++ )
                {
                    // SI le pixel appartient a une autre composante et n'a pas deja ete marque
                    if( labelGrid->value( ii,jj,kk ) != component->_label && markGrid->value( ii,jj,kk ) == false )
                    {
                        // On marque le pixel et on l'ajoute au vecteur de sortie
                        markGrid->setValue( ii, jj, kk, true );
                        Pixel n; n << ii, jj, kk;
                        outPixels.push_back( n );
                    }
                }
            }
        }
    }

    // On demarque les pixels de la grille de marquage
    foreach( Pixel p, outPixels )
    {
        markGrid->setValue( p.x(), p.y(), p.z(), false );
    }
}

int ICRO_StepGetConnectedComponentOnIntGridMultiResolution::countNonAggregatedPixels(QVector<ConnectedComponent *> &components, bool countComplex )
{
    int rslt = 0;

    foreach( ConnectedComponent* component, components )
    {
        if( component->_wasFused == false )
        {
            if( countComplex )
            {
                rslt++;
            }
            else
            {
                if( component->_type != COMPLEX )
                {
                    rslt++;
                }
            }
        }
    }

    return rslt;
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::getSimplexNonAgregated(QVector<ConnectedComponent *> &allComponents,
                                                                                    QVector<ConnectedComponent *> &outSimplexNonAggregatedComponents)
{
    outSimplexNonAggregatedComponents.clear();
    foreach( ConnectedComponent* component, allComponents )
    {
        if( component->_type == SIMPLEX && component->_wasFused == false )
        {
            outSimplexNonAggregatedComponents.push_back( component );
        }
    }
}

void ICRO_StepGetConnectedComponentOnIntGridMultiResolution::splitSceneIntoComponentsFomNegativeLabels(const CT_Scene *inputScene,
                                                                                                       CT_Grid3D<int> *labelGrid,
                                                                                                       QVector<CT_PointCloudIndexVector *> outScenes)
{
    CT_PointIterator itP(inputScene->getPointCloudIndex());
    while(itP.hasNext())
    {
        const CT_Point &point = itP.next().currentPoint();
        size_t index = itP.currentGlobalIndex();

        if( labelGrid->valueAtXYZ( point.x(), point.y(), point.z() ) < 0 )
        {
            outScenes[ -labelGrid->valueAtXYZ( point.x(), point.y(), point.z() )-1 ]->addIndex(index);
        }
    }
}
