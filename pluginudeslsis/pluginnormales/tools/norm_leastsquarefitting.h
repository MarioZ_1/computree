/********************************************************************************
* Filename :  NORM_leastsquarefitting.h							*
*																				*
* Copyright (C) 2015 Joris RAVAGLIA												*
* Author: Joris Ravaglia														*
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                        *
*																				*
* This file is part of the oc_pluginoctree plugin                               *
* for the CompuTree v2.0 software.                                              *
* The oc_pluginoctree plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the               *
* GNU Lesser General Public License as published by the Free Software Foundation*
* either version 3 of the License, or (at your option) any later version.       *
*																				*
* The oc_pluginoctree plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY*
* or FITNESS FOR A PARTICULAR PURPOSE.                                          *
* See the GNU Lesser General Public License for more details.                   *
*																				*
* You should have received a copy of the GNU Lesser General Public License		*
* along with this program. If not, see <http://www.gnu.org/licenses/>.			*
*																				*
*********************************************************************************/

#ifndef NORM_LEASTSQUAREFITTING_H
#define NORM_LEASTSQUAREFITTING_H

#include "ct_global/ct_repository.h"
#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"
#include "ct_accessor/ct_pointaccessor.h"

namespace NORM_Tools
{
    namespace NORM_ChangeBasis
    {
        float scalarProduct(CT_Point& p1, CT_Point& p2 );

        Eigen::Matrix3d changeBasisMatrix(CT_Point& ob1, CT_Point& ob2,CT_Point& ob3,
                                          CT_Point &nb1, CT_Point &nb2, CT_Point &nb3);

        CT_Point pointInNewBasis(Eigen::Matrix3d &changeBasisMatrix,
                                 const CT_Point &pointInOldBasis );
    }

    namespace NORM_LeastSquareFitting
    {
        /*!
         * \brief quadraticSurfaceFitting
         *
         * Fit a quadratic surface to the point cloud by least mean square method
         *
         * \param indexCloud : input indNORM cloud
         * \param outA : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outB : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outC : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outD : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outE : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outF : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         */
        void quadraticSurfaceFitting( const CT_AbstractPointCloudIndex* indexCloud,
                                      float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        void quadraticSurfaceFitting( const QVector<int>* indexCloud,
                                      float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        /*!
         * \brief quadraticSurfaceFitting
         *
         * Fit a quadratic surface to the point cloud by least mean square method
         *
         * \param indexCloud : input indNORM cloud
         * \param outA : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outB : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outC : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outD : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outE : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         * \param outF : parameter of the surface of equation f(x,y) = a x^2 + b xy + c y^2 + dx + ey + f
         */
        void quadraticSurfaceFitting( const CT_MPCIR indexCloud,
                                      float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        void quadraticSurfaceFitting( const QVector<CT_Point>* points,
                                      float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        void quadraticSurfaceFitting(QVector<int>* indices,
                                     const CT_PointAccessor& pAccessor,
                                     const CT_Point centroid,
                                     Eigen::Matrix3d& changeBaseMatrix,
                                     float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        float quadraticSurfaceFittingWithRMSE(QVector<int>* indices,
                                              const CT_PointAccessor& pAccessor,
                                              const CT_Point centroid,
                                              Eigen::Matrix3d& changeBaseMatrix,
                                              float& outA, float& outB, float& outC, float& outD, float& outE, float& outF );

        Eigen::Vector4f newton (Eigen::Matrix3f &A, Eigen::Vector3f &B, float C, Eigen::Vector4f x0, float seuil, int nIterMax );

        Eigen::Vector4f newtonStep (Eigen::Matrix3f &A, Eigen::Vector3f &B, float C, Eigen::Vector4f& x , Eigen::Vector4f x0);

        Eigen::Vector4f quadFunc (Eigen::Matrix3f &A, Eigen::Vector3f &B, float C, Eigen::Vector4f& x , Eigen::Vector4f &x0);

        Eigen::Matrix4f quadGrad(Eigen::Matrix3f &A, Eigen::Vector3f &B, Eigen::Vector4f& x );
    }
}

#endif // NORM_LEASTSQUAREFITTING_H
