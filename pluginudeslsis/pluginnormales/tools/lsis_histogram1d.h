/************************************************************************************
* Filename :  lsis_histogram1d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_HISTOGRAM1D_H
#define LSIS_HISTOGRAM1D_H

// Un histogramme stocke les compteurs dans un qvector
#include <QVector>

// Utilise pour l'affichage
#include <QString>

// On utilise la fonction sfloor
#include <math.h>

/*!
 * \brief The LSIS_Histogram1D class
 *
 * Un histogramme 1D
 */
template< typename DataT >
class LSIS_Histogram1D
{
public:
/* **************************************************************** */
/* Constructeurs Destructeurs                                       */
/* **************************************************************** */
    /*!
     * \brief LSIS_Histogram1D
     *
     * Constructeur de la classe.
     * Cree un histogramme vide en fonction de ses valeurs min et max.
     *
     * \param minVal : valeur minimale de l'histogramme
     * \param maxVal : valeur maximale de l'histogramme
     * \param res : deux possibilites : (1) resolution de l'histogramme (taille de chaque bin) OU (2) nombre de bins dans l'histogramme
     * \param nBins : booleen qui indique si la valeur de res doit etre consideree comme la resolution de l'histogramme ou comme le nombre de bins de cet histogramme (de l'un découle l'autre)
     */
    LSIS_Histogram1D( float minVal, float maxVal, float res, bool nBins );

    /*!
     * \brief LSIS_Histogram1D
     *
     * Constructeur de la classe.
     * Cree et remplit un histogramme en fonction de ses valeurs min et max et d'un tableau de valeurs.
     *
     * \param minVal : valeur minimale de l'histogramme
     * \param maxVal : valeur maximale de l'histogramme
     * \param res : deux possibilites : (1) resolution de l'histogramme (taille de chaque bin) OU (2) nombre de bins dans l'histogramme
     * \param nBins : booleen qui indique si la valeur de res doit etre consideree comme la resolution de l'histogramme ou comme le nombre de bins de cet histogramme (de l'un découle l'autre)
     * \param values : valeurs a ajouter a l'histogramme
     */
    LSIS_Histogram1D( float minVal, float maxVal, float res, bool nBins, QVector<DataT> &values );

    /*!
      * Destructeur
      */
    ~LSIS_Histogram1D();

/* **************************************************************** */
/* Outils d'analyse et statistiques                                 */
/* **************************************************************** */
    /*!
     * \brief getCumulative
     *
     * Calcule l'histogramme cumule.
     *
     */
    LSIS_Histogram1D* getCumulative ();

    /*!
     * \brief getXCentiles
     *
     * Calcule l'ensemble des x-tiles d'un histogramme
     *
     * \param x : valeur de x-tile (x = 4 <=> quartiles, x = 100 <=> centiles)
     *
     * \return un tableau de x elements qui contient l'indice du bin pour lequel on atteint le x-tile
     */
    QVector<DataT>* getXTiles( int x );

    /*!
     * \brief getIthXTiles
     *
     * Calcule le ieme x-tile
     *
     * \param x : valeur de x-tile (x = 4 <=> quartiles, x = 100 <=> centiles)
     *
     * \return le ieme x-tile
     */
    int getIthXTiles( int x, int i );

    /*!
     * \brief getCentiles
     *
     * Calcule l'ensemble des x-tiles sur un histogramme cumule
     *
     * \param x : valeur de x-tile (x = 4 <=> quartiles, x = 100 <=> centiles)
     *
     * \return un tableau de x elements qui contient l'indice du bin pour lequel on atteint le x-tile
     */
    QVector<DataT>* getCentilesFromCumulative( int x );

/* **************************************************************** */
/* Outils de seuillage                                              */
/* **************************************************************** */
    /*!
     * \brief otsuThreshold
     *
     * Calcule un seuil selon la methode d'Otsu (qui maximise la variance inter classe).
     *
     * \return la valeur du seuil.
     */
    float otsuThreshold();

    /*!
     * \brief meanOfClass
     *
     * Calcule la valeur moyenne de la classe formee par les pixels dontlavaleur est comprise dans l'intervalle [classMinValue, classMaxValue]
     *
     * \param classMinValue : valeur minimum de la classe
     * \param classMaxValue : valeur maximum de la classe
     *
     * \return la valeur moyenne de la classe
     */
    float meanOfClass( float classMinValue, float classMaxValue );

    /*!
     * \brief meanOfClasses
     *
     * Methode a utiliser pour calculer la moyenne de deux classes distinctes d'un histogramme.
     * Les deux classes sont separees par une valeur de seuil.
     * Cette methode est utilisee lors du seuillage de l'espace de Hough par ratio et otsu
     *
     * \param threshold : valeur du seuil separant les deux classes
     * \param outMeanLowClass : moyenne de la classe la plus basse
     * \param outMeanHighClass : moyenne de la classe la plus haute
     */
    void meanOfClasses( float threshold, float& outMeanLowClass, float& outMeanHighClass );

/* **************************************************************** */
/* Outils divers                                                    */
/* **************************************************************** */
    /*!
     * \brief print
     *
     * Affiche un histogramme dans la console (sous forme de texte, pas de graphique).
     *
     * \param bef : message avant d'afficher
     * \param aft : message apres l'affichage
     *
     */
    void print(QString bef = "", QString aft = "");

    /*!
     * \brief printInfos
     *
     * Affiche seulement les parametres
     *
     * \param bef : message avant d'afficher les parametres
     * \param aft : message apres l'affichage les parametres
     */
    void printInfos(QString bef = "", QString aft = "");

/* **************************************************************** */
/* Getters setters                                                  */
/* **************************************************************** */
    inline float getMin() const
    {
        return _min;
    }

    inline float getMax() const
    {
        return _max;
    }

    inline float getRes() const
    {
        return _res;
    }

    inline int getNBins() const
    {
        return _nBins;
    }

    inline int getNEntries() const
    {
        return _nEntries;
    }

    inline float getRange() const
    {
        return (_max - _min);
    }

    inline int getValueAtBin( int i ) const
    {
        return _bins.at(i);
    }

    inline float getRangeRatioMaxOverMin() const
    {
        return (_max / _min);
    }

    inline float getRangeRatioMinOverMax() const
    {
        return (_min / _max);
    }

    inline int indexOfValue( float value ) const
    {
        // The index of a bin for a value v is calculated as
        // (v - min) / res
        return floor( ( value - _min ) / _res );
    }

    inline float centerOfBin( int i ) const
    {
        return ( _min + ( 0.5 + i) * _res );
    }

/* **************************************************************** */
/* Attributs de la classe                                           */
/* **************************************************************** */
private :
    int             _nEntries;      /*!< Nombre de valeurs entrees dans l'histogramme */
    DataT           _min;           /*!< Valeur minimale de l'histogramme */
    DataT           _max;           /*!< Valeur maximale de l'histogramme */
    float           _res;           /*!< Resolution de l'histogramme (taille des bins) */
    int             _nBins;         /*!< Nombre de cases de l'histogramme */
    QVector<DataT>  _bins;          /*!< Tableau des valeurs de chaque bin de l'histogramme */
};

// Implementation templatee
#include "lsis_histogram1d.hpp"

#endif // LSIS_HISTOGRAM1D_H
