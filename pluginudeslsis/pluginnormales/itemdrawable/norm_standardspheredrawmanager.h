#ifndef NORM_STANDARDSPHEREDRAWMANAGER_H
#define NORM_STANDARDSPHEREDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractshapedrawmanager.h"

class NORM_StandardSphereDrawManager : public CT_StandardAbstractShapeDrawManager
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
     * \brief NORM_StandardSphereDrawManager
     *
     * Constructor of the NORM_StandardSphereDrawManager class
     *
     * \param drawConfigurationName : name of the configuration
     */
    NORM_StandardSphereDrawManager(QString drawConfigurationName = "");

//********************************************//
//                Drawing tools               //
//********************************************//
    /*!
     * \brief draw
     *
     * Draw asphere : draw a sphere
     *
     * \param view : graphic view
     * \param painter : painter to paint objects in the view
     * \param itemDrawable : octree to be drawn
     */
    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;
};

#endif // NORM_STANDARDSPHEREDRAWMANAGER_H
