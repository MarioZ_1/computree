#ifndef NORM_STEPGENERATEBOX_H
#define NORM_STEPGENERATEBOX_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

/*!
 * \class NORM_StepGenerateBox
 * \ingroup Steps_NORM
 * \brief <b>Generates a box.</b>
 *
 * No detailled description for this step
 *
 * \param _botx 
 * \param _boty 
 * \param _botz 
 * \param _topx 
 * \param _topy 
 * \param _topz 
 *
 */

class NORM_StepGenerateBox: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    NORM_StepGenerateBox(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _botx;    /*!<  */
    double    _boty;    /*!<  */
    double    _botz;    /*!<  */
    double    _topx;    /*!<  */
    double    _topy;    /*!<  */
    double    _topz;    /*!<  */

};

#endif // NORM_STEPGENERATEBOX_H
