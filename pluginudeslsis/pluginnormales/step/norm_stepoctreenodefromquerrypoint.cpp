#include "norm_stepoctreenodefromquerrypoint.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

#include "octree/norm_octree.h"
#include "octree/norm_octreenode.h"

// Alias for indexing models
#define DEFin_ResultOctree "ResultOctree"
#define DEFin_GroupOctree "GroupOctree"
#define DEFin_ItemOctree "ItemOctree"

#define DEFout_ResultPointsInNode "ResultPointsInNode"
#define DEFout_GroupPointsInNode "GroupPointsInNode"
#define DEFout_ItemPointsInNode "ItemPointsInNode"

typedef NORM_Octree NORM_Octree;

// Constructor : initialization of parameters
NORM_StepOctreeNodeFromQuerryPoint::NORM_StepOctreeNodeFromQuerryPoint(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _querryx = 0;
    _querryy = 0;
    _querryz = 0;
}

// Step description (tooltip of contextual menu)
QString NORM_StepOctreeNodeFromQuerryPoint::getStepDescription() const
{
    return tr("Enlights the octree leaf containing a querry point");
}

// Step detailled description
QString NORM_StepOctreeNodeFromQuerryPoint::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepOctreeNodeFromQuerryPoint::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepOctreeNodeFromQuerryPoint::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepOctreeNodeFromQuerryPoint(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepOctreeNodeFromQuerryPoint::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_Result = createNewInResultModel(DEFin_ResultOctree, tr("Resultat d'une creation d'octree"));
    resIn_Result->setRootGroup(DEFin_GroupOctree, CT_AbstractItemGroup::staticGetType(), tr("Groupe contenant un octree"));
    resIn_Result->addItemModel(DEFin_GroupOctree, DEFin_ItemOctree, NORM_Octree::staticGetType(), tr("Un octree cree"));
}

// Creation and affiliation of OUT models
void NORM_StepOctreeNodeFromQuerryPoint::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_Result = createNewOutResultModel(DEFout_ResultPointsInNode, tr("Extracted points"));
    res_Result->setRootGroup(DEFout_GroupPointsInNode, new CT_StandardItemGroup(), tr("Group"));
    res_Result->addItemModel(DEFout_GroupPointsInNode, DEFout_ItemPointsInNode, new CT_Scene(), tr("Scene extraite"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepOctreeNodeFromQuerryPoint::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText("Querry point", "", "");
    configDialog->addDouble("x", "", -9999, 9999, 4, _querryx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _querryy, 1);
    configDialog->addDouble("z", "", -9999, 9999, 4, _querryz, 1);
}

void NORM_StepOctreeNodeFromQuerryPoint::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_ResultOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_Result = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_ResultOctree, this, DEFin_GroupOctree);
    while (itIn_Group.hasNext() && !isStopped())
    {
        // Recupere l'octree
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();
        const NORM_Octree* itemIn_ItemOctree = (NORM_Octree*)grpIn_Group->firstItemByINModelName(this, DEFin_ItemOctree);

        if (itemIn_ItemOctree != NULL)
        {
            // On fait la recherche du noeud contenant le point
            CT_Point querryPoint = createCtPoint(_querryx, _querryy, _querryz );
            NORM_OctreeNode* node = (NORM_OctreeNode*)itemIn_ItemOctree->nodeContainingPoint( querryPoint );

            // On cree une scene a partir des points de ce noeud
            CT_PointCloudIndexVector* outIndexVector = node->getIndexCloud();

            // Calcul de la bounding box
            CT_PointAccessor pAccessor;
            CT_Point curPoint;
            float xmin = std::numeric_limits<float>::max();
            float xmax = -std::numeric_limits<float>::max();
            float ymin = std::numeric_limits<float>::max();
            float ymax = -std::numeric_limits<float>::max();
            float zmin = std::numeric_limits<float>::max();
            float zmax = -std::numeric_limits<float>::max();
            size_t nbPoints = outIndexVector->size();
            for( size_t i = 0 ; i < nbPoints ; i++ )
            {
                pAccessor.pointAt( outIndexVector->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }

            // Creation de la scene
            CT_PCIR outIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexVector );
            CT_Scene* item_outItemScene = new CT_Scene(DEFout_ItemPointsInNode, res_Result);
            item_outItemScene->setPointCloudIndexRegistered( outIndexVectorRegistered );
            item_outItemScene->setBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax );

            // On ajoute cette scene aux resultats
            CT_StandardItemGroup* grp_Group= new CT_StandardItemGroup(DEFout_GroupPointsInNode, res_Result);
            res_Result->addGroup(grp_Group);
            grp_Group->addItemDrawable(item_outItemScene);

            PS_LOG->addInfoMessage( PS_LOG->info, "Il y a " + QString::number( outIndexVector->size() ) + " points dans le noeud contenant le point cherche" );
        }
    }
}
