#include "norm_stepcreatestdoctree.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "octree/norm_octreestd.h"

// Alias for indexing models
#define DEFin_Result "Result"
#define DEFin_Group "Group"
#define DEFin_IndexCloud "IndexCloud"

#define DEFout_ResultOctree "ResultOctree"
#define DEFout_GroupOctree "GroupOctree"
#define DEFout_ItemAttributeCloudIdNode "ItemAttributeCloudIdNode"
#define DEFout_ItemAttributeCloudColorNode "ItemAttributeCloudColorNode"
#define DEFout_ItemOctree "Octree"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesColor AttributCouleur;

// Constructor : initialization of parameters
NORM_StepCreateStdOctree::NORM_StepCreateStdOctree(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minNodeSize = 0.0001;
    _nbPointsMinInNode = 1;
}

// Step description (tooltip of contextual menu)
QString NORM_StepCreateStdOctree::getStepDescription() const
{
    return tr("Cree un octree classique");
}

// Step detailled description
QString NORM_StepCreateStdOctree::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepCreateStdOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepCreateStdOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepCreateStdOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepCreateStdOctree::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_Result = createNewInResultModelForCopy(DEFin_Result, tr("inResult"), tr(""), true);
    resIn_Result->setRootGroup(DEFin_Group, CT_AbstractItemGroup::staticGetType(), tr("inGroup"));
    resIn_Result->addItemModel(DEFin_Group, DEFin_IndexCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("IndexCloud"));
}

// Creation and affiliation of OUT models
void NORM_StepCreateStdOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultOctree = createNewOutResultModel(DEFout_ResultOctree, tr("Resultat creation octree"));
    res_ResultOctree->setRootGroup(DEFout_GroupOctree, new CT_StandardItemGroup(), tr("GroupeOctree"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudIdNode, new AttributEntier(), tr("Identifiant des noeuds [0,7]"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudColorNode, new AttributCouleur(), tr("Coloration des points par noeuds"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemOctree, new NORM_OctreeStd(), tr("Octree"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepCreateStdOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Taille de cellule minimale", "", 0.0001, 100, 4, _minNodeSize, 1);
    configDialog->addInt("Nombre de points minimum par cellule", "", 1, 10000, _nbPointsMinInNode);
}

void NORM_StepCreateStdOctree::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_Group);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();
        
        const CT_AbstractItemDrawableWithPointCloud* itemIn_IndexCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_Group->firstItemByINModelName(this, DEFin_IndexCloud);
        CT_PCIR inputIndexCloud = itemIn_IndexCloud->getPointCloudIndexRegistered();

        if (itemIn_IndexCloud != NULL)
        {
            // OUT results creation (move it to the appropried place in the code)
            CT_StandardItemGroup* grp_GroupOctree= new CT_StandardItemGroup(DEFout_GroupOctree, res_ResultOctree);
            res_ResultOctree->addGroup(grp_GroupOctree);

            // On cree l'octree
            CT_Point bot = createCtPoint( itemIn_IndexCloud->minX(), itemIn_IndexCloud->minY(), itemIn_IndexCloud->minZ() );
            CT_Point top = createCtPoint( itemIn_IndexCloud->maxX(), itemIn_IndexCloud->maxY(), itemIn_IndexCloud->maxZ() );
            NORM_OctreeStd* octree = new NORM_OctreeStd ( _minNodeSize, _nbPointsMinInNode, bot, top, inputIndexCloud, DEFout_ItemOctree ,res_ResultOctree );
            octree->createOctree();

            AttributEntier* item_AttributeCloudIdNode = octree->getIdAttribute( DEFout_ItemAttributeCloudIdNode, res_ResultOctree );
            AttributColor* item_AttributeCloudColorNode = octree->getColorAttribute( DEFout_ItemAttributeCloudColorNode, res_ResultOctree );

            grp_GroupOctree->addItemDrawable(item_AttributeCloudIdNode);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudColorNode);
            grp_GroupOctree->addItemDrawable( octree );
        }
    }
}
