#include "norm_newstepcreateoctree.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

#include "ct_itemdrawable/ct_scene.h"

#include "new/norm_newoctreev2.h"

#include <QElapsedTimer>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltScene "RsltScene"
#define DEFin_grpScene "grpScene"
#define DEFin_itmScene "itmScene"

// Constructor : initialization of parameters
NORM_NewStepCreateOctree::NORM_NewStepCreateOctree(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
    _nPointsMinForPca = 10;
    _sigma3Max = 5;
    _sigma2Min = 5;
    _dimensionMin = 0.05;
    _dimensionMaxForPca = 1;

    _criteriaList << "Lambda3" << "RMSE";
    _criteriaChoice = "Lambda3";
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepCreateOctree::getStepDescription() const
{
    return tr("1 - Cree un octree sur une scene");
}

// Step detailled description
QString NORM_NewStepCreateOctree::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepCreateOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepCreateOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepCreateOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepCreateOctree::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltScene = createNewInResultModelForCopy(DEFin_rsltScene, "Scene Result");
    resIn_rsltScene->setZeroOrMoreRootGroup();

    resIn_rsltScene->addGroupModel("", DEFin_grpScene, CT_AbstractItemGroup::staticGetType(), tr("Input scene Group"));
    resIn_rsltScene->addItemModel( DEFin_grpScene, DEFin_itmScene, CT_Scene::staticGetType(), tr("Input Scene"));
}

// Creation and affiliation of OUT models
void NORM_NewStepCreateOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltSegments = createNewOutResultModelToCopy( DEFin_rsltScene );

    if(res_RsltSegments != NULL)
    {
        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelOctree, new NORM_NewOctreeV2(), "Octree", "Octree PCA over input scene");
        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeId, new CT_PointsAttributesScalarTemplated<int>(), tr("Identifiant des noeuds [0,7]"));
//        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeL1, new CT_PointsAttributesScalarTemplated<float>(), tr("L1 -1 pour NA"));
//        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeL2, new CT_PointsAttributesScalarTemplated<float>(), tr("L2 -1 pour NA"));
//        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeL3, new CT_PointsAttributesScalarTemplated<float>(), tr("L3 -1 pour NA"));
//        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeSigma, new CT_PointsAttributesScalarTemplated<float>(), tr("Sigma [0,30], -1 pour NA"));
//        res_RsltSegments->addItemModel( DEFin_grpScene, _autoRenameModelPointsNodeL3L2, new CT_PointsAttributesScalarTemplated<float>(), tr("L3/L2 -1 pour NA"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepCreateOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble( "Dimension min de cellule", "", 0.0001, std::numeric_limits<double>::max(), 4, _dimensionMin );
    configDialog->addInt( "NbPtsMinForPca", "", 3, std::numeric_limits<int>::max(), _nPointsMinForPca );
    configDialog->addDouble( "Dimension en dessous de laquelle lancer une acp", "", 0.0001, std::numeric_limits<double>::max(), 4, _dimensionMaxForPca );
    configDialog->addDouble( "Sigma3Max [%]", "", 0.0001, 30, 4, _sigma3Max );
    configDialog->addDouble( "Sigma2Min [%]", "", 0.0001, 50, 4, _sigma2Min );

    configDialog->addStringChoice( "Critere", "", _criteriaList, _criteriaChoice );

    configDialog->addDouble( "RMSE Max", "", 0.0001, 50, 8, _rmseMax );
}

void NORM_NewStepCreateOctree::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltScene = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpScene( res_RsltScene, this, DEFin_grpScene );
    while ( itIn_grpScene.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpScene = (CT_StandardItemGroup*) itIn_grpScene.next();

        CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpScene->firstItemByINModelName( this, DEFin_itmScene );
        if ( itemIn_itmScene != NULL )
        {
            QElapsedTimer timer;
            timer.start();

            LeafCriteria critere;
            if( _criteriaChoice == "Lambda3" )
            {
                critere = Lambdas;
            }
            else if( _criteriaChoice == "RMSE" )
            {
                critere = RMSE;
            }
            else
            {
                qDebug() << "Erreur dans le choix du critere, l'etape s'arrete";
                return;
            }

            NORM_NewOctreeV2* octree = new NORM_NewOctreeV2( _autoRenameModelOctree.completeName(),
                                                             res_RsltScene,
                                                             itemIn_itmScene,
                                                             _nPointsMinForPca,
                                                             _dimensionMaxForPca,
                                                             critere,
                                                             _sigma2Min,
                                                             _sigma3Max,
                                                             _rmseMax,
                                                             _dimensionMin );
            qDebug() << "Temps de creation de l'octree" << timer.elapsed();
            qDebug() << "Octree cree";
            grpIn_grpScene->addItemDrawable( octree );

            timer.start();
            CT_PointsAttributesScalarTemplated<int>* pointNodeIndicesInOctree = octree->getPointsIdAttribute( _autoRenameModelPointsNodeId.completeName(),
                                                                                                              res_RsltScene,
                                                                                                              itemIn_itmScene->getPointCloudIndexRegistered() );
            qDebug() << "Temps de recuperation des id pour les points" << timer.elapsed();
            grpIn_grpScene->addItemDrawable( pointNodeIndicesInOctree );

//            timer.start();
//            CT_PointsAttributesScalarTemplated<float>* pointNodeSigmaInOctree = octree->getPointsSigmaAttribute( _autoRenameModelPointsNodeSigma.completeName(),
//                                                                                                                 res_RsltScene,
//                                                                                                                 itemIn_itmScene->getPointCloudIndexRegistered() );
//            qDebug() << "Temps de recuperation des sigmas pour les points" << timer.elapsed();
//            grpIn_grpScene->addItemDrawable( pointNodeSigmaInOctree );


//            CT_PointsAttributesScalarTemplated<float>* pointNodeL1InOctree = new CT_PointsAttributesScalarTemplated<float>( _autoRenameModelPointsNodeL1.completeName(),
//                                                                                                                            res_RsltScene,
//                                                                                                                            itemIn_itmScene->getPointCloudIndexRegistered() );
//            CT_PointsAttributesScalarTemplated<float>* pointNodeL2InOctree = new CT_PointsAttributesScalarTemplated<float>( _autoRenameModelPointsNodeL2.completeName(),
//                                                                                                                            res_RsltScene,
//                                                                                                                            itemIn_itmScene->getPointCloudIndexRegistered() );
//            CT_PointsAttributesScalarTemplated<float>* pointNodeL3InOctree = new CT_PointsAttributesScalarTemplated<float>( _autoRenameModelPointsNodeL3.completeName(),
//                                                                                                                            res_RsltScene,
//                                                                                                                            itemIn_itmScene->getPointCloudIndexRegistered() );
//            CT_PointsAttributesScalarTemplated<float>* pointNodeSigmaInOctree = new CT_PointsAttributesScalarTemplated<float>( _autoRenameModelPointsNodeSigma.completeName(),
//                                                                                                                               res_RsltScene,
//                                                                                                                               itemIn_itmScene->getPointCloudIndexRegistered() );
//            CT_PointsAttributesScalarTemplated<float>* pointNodeL3L2InOctree = new CT_PointsAttributesScalarTemplated<float>( _autoRenameModelPointsNodeL3L2.completeName(),
//                                                                                                                              res_RsltScene,
//                                                                                                                              itemIn_itmScene->getPointCloudIndexRegistered() );

//            octree->getPcaAttributes( pointNodeL1InOctree,
//                                      pointNodeL2InOctree,
//                                      pointNodeL3InOctree,
//                                      pointNodeSigmaInOctree,
//                                      pointNodeL3L2InOctree );

//            grpIn_grpScene->addItemDrawable( pointNodeL1InOctree );
//            grpIn_grpScene->addItemDrawable( pointNodeL2InOctree );
//            grpIn_grpScene->addItemDrawable( pointNodeL3InOctree );
//            grpIn_grpScene->addItemDrawable( pointNodeSigmaInOctree );
//            grpIn_grpScene->addItemDrawable( pointNodeL3L2InOctree );
        }
    }
}
