/************************************************************************************
* Filename :  icro_ascnormexporter.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "icro_ascnormexporter.h"

#include <QTextStream>
#include <QFile>
#include <QFileInfo>

#include "ct_global/ct_context.h"
#include "ct_tools/ct_numerictostringconversiont.h"
#include "ct_iterator/ct_pointiterator.h"

ICRO_ASCNormExporter::ICRO_ASCNormExporter() : CT_AbstractExporterPointAttributesSelection()
{
}

ICRO_ASCNormExporter::~ICRO_ASCNormExporter()
{

}

QString ICRO_ASCNormExporter::getExporterCustomName() const
{
    return "Points, ASCII(X,Y,Z,NX,NY,NZ)";
}

void ICRO_ASCNormExporter::init()
{
    addNewExportFormat(FileFormat("asc", tr("Fichier asc")));
}

bool ICRO_ASCNormExporter::setItemDrawableToExport(const QList<CT_AbstractItemDrawable*> &list)
{
    clearErrorMessage();

    QList<CT_AbstractItemDrawable*> myList;
    QListIterator<CT_AbstractItemDrawable*> it(list);

    while(it.hasNext())
    {
        CT_AbstractItemDrawable *item = it.next();

        if(dynamic_cast<CT_IAccessPointCloud*>(item) != NULL)
            myList.append(item);
    }

    if(myList.isEmpty())
    {
        setErrorMessage(tr("Aucun ItemDrawable du type CT_IAccessPointCloud"));
        return false;
    }

    return CT_AbstractExporter::setItemDrawableToExport(myList);
}

CT_AbstractExporter* ICRO_ASCNormExporter::copy() const
{
    return new ICRO_ASCNormExporter();
}

bool ICRO_ASCNormExporter::protectedExportToFile()
{
    QFileInfo exportPathInfo = QFileInfo(exportFilePath());
    QString path = exportPathInfo.path();
    QString baseName = exportPathInfo.baseName();
    QString suffix = "asc";
    QString filePath = QString("%1/%2.%4").arg(path).arg(baseName).arg(suffix);

    QFile file(filePath);

    if(file.open(QFile::WriteOnly | QFile::Text))
    {
        QTextStream txtStream(&file);

        txtStream << "X Y Z NX NY NZ\n";

        CT_AbstractNormalCloud *nc = createNormalCloudBeforeExportToFile();

        float nx = 0;
        float ny = 0;
        float nz = 0;

        int totalToExport = itemDrawableToExport().size();
        int nExported = 0;

        // write data
        QListIterator<CT_AbstractItemDrawable*> it(itemDrawableToExport());

        while(it.hasNext())
        {
            CT_AbstractItemDrawable *item = it.next();

            CT_PointIterator itP(dynamic_cast<CT_IAccessPointCloud*>(item)->getPointCloudIndex());

            size_t totalSize = itP.size();
            size_t i = 0;

            while(itP.hasNext())
            {
                const CT_Point &point = itP.next().currentPoint();

                txtStream << CT_NumericToStringConversionT<double>::toString(point(0)) << " ";
                txtStream << CT_NumericToStringConversionT<double>::toString(point(1)) << " ";
                txtStream << CT_NumericToStringConversionT<double>::toString(point(2)) << " ";

                if(nc == NULL)
                {
                    txtStream << "0 0 0\n";
                }
                else
                {
                    const CT_Normal &no = nc->constNormalAt(itP.cIndex());
                    nx = no(0);
                    ny = no(1);
                    nz = no(2);

                    txtStream << nx << " ";
                    txtStream << ny << " ";
                    txtStream << nz << "\n";
                }

                ++i;

                setExportProgress((((i*100)/totalSize)+nExported)/totalToExport);
            }

            nExported += 100;
        }

        clearWorker();

        file.close();
        return true;
    }

    clearWorker();

    return false;
}
