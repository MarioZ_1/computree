/************************************************************************************
* Filename :  norm_newoctree.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_NEWOCTREE_H
#define NORM_NEWOCTREE_H

#include "ct_cloudindex/registered/ct_standardmodifiablecloudindexregisteredt.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"
#include "ct_point.h"

#include "ct_itemdrawable/ct_line.h"

class NORM_NewOctreeNode;
class NORM_NewOctreeDrawManager;
class CT_Scene;

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;
typedef CT_PointsAttributesColor AttributCouleur;
typedef CT_PointsAttributesNormal AttributeNormals;

class NORM_NewOctree : public CT_AbstractItemDrawableWithoutPointCloud
{
public:
    NORM_NewOctree();

    ~NORM_NewOctree();

    /*!
     * \brief NORM_NewOctree
     *
     * Constructeur
     *
     * \param bot : point inferieur gauche de la boite englobante du nuage
     * \param top : point superieur droit de la boite englobante du nuage
     * \param dimensionMin : dimension minimale d'une feuille de l'octree
     * \param nbPointsMin : nombre de points minimum dans une feuille de l'octree
     * \param inputIndexCloud : nuage d'indices des points a inserer dans l'octree
     * \param modelName : name of the model of the octree in the result
     * \param result : result in which put the octree
     */
    NORM_NewOctree(const CT_Point& bot,
                   const CT_Point& top,
                   float dimensionMin,
                   float dimensionMinAcp,
                   int nbPointsMin,
                   float sigmaThreshPercent,
                   float rmseThresh,
                   CT_PCIR inputIndexCloud,
                   const QString modelName,
                   const CT_AbstractResult *result ,
                   QVector<CT_LineData *>& curPolyline);

    /*!
     * \brief draw
     *
     * Dessine l'octree (les cellules)
     *
     * \param view : vue dans laquelle s'affiche l'objet
     * \param painter : outil de dessin
     */
    void draw(GraphicsViewInterface& view,
              PainterInterface& painter,
              bool drawNodeBoxes,
              QVector<bool>& drawBoxes,
              bool drawAcpBasis ) const;

    float dimensionMin() const;
    void setDimensionMin(float dimensionMin);

/* **************************************************************** */
/* CT_AbstractItemDrawableWithoutPointCloud method redefinition     */
/* **************************************************************** */
    virtual QString getType() const
    {
        return staticGetType();
    }

    static QString staticGetType()
    {
        return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/NORM_NewOctree";
    }

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList );

/* **************************************************************** */
/* Tools                                                            */
/* **************************************************************** */
    /*!
     * \brief getIdAttribute
     *
     *  Creates an attribute cloud :
     *  Each point of the cloud is given an ID attribute corresponding to its location in the octree (from 0 to 7)
     *
     * \param modelName : name of the attribute cloud model
     * \param result : the attribute cloud has to be attached to a model
     *
     * \return an attribute cloud of ID of each point in the octree
     */
    AttributEntier* getIdAttribute(QString modelName,
                                   CT_AbstractResult* result);

    AttributEntier* getNbPointsAttribute(QString modelName,
                                         CT_AbstractResult* result);

    AttributFloat* getSigmaAttribute(QString modelName,
                                     CT_AbstractResult* result);

    AttributFloat* getSigmaPercentAttribute(QString modelName,
                                            CT_AbstractResult* result);

    AttributFloat* getLineaityAttribute(QString modelName,
                                        CT_AbstractResult* result);

    AttributFloat* getFittingErrorAttribute(QString modelName,
                                            CT_AbstractResult* result);

    AttributFloat* getFittingRmseAttribute(QString modelName,
                                           CT_AbstractResult* result);

    AttributeNormals* getNormalsRecursive(QString modelName, CT_AbstractResult *result,
                                          const CT_Point& scanPos,
                                          bool towards );

    AttributeNormals* getNormalsRecursiveLinearInterpolation(QString modelName, CT_AbstractResult *result,
                                                             const CT_Point& scanPos,
                                                             bool towards,
                                                             float radiusMax );

    AttributeNormals* getNormalsRecursiveCRBFInterpolation(QString modelName, CT_AbstractResult *result,
                                                           const CT_Point& scanPos,
                                                           bool towards,
                                                           float radiusMax );

    void getNormals( AttributeNormals* normals );

    /*!
     * \brief getIdAttribute
     *
     *  Creates an attribute cloud :
     *  Each point of the cloud is given an color attribute corresponding to its location in the octree (from blue to red)
     *
     * \param modelName : name of the attribute cloud model
     * \param result : the attribute cloud has to be attached to a model
     *
     * \return an attribute cloud of color of each point in the octree
     */
    AttributCouleur* getColorAttribute(QString modelName,
                                       CT_AbstractResult* result);

    NORM_NewOctreeNode* getNodeContainingPoint( const CT_Point& p ) const;

    void getQuadraticPointCloudAndSign (QVector<CT_Scene*>& outScenes,
                                         QString modelNameScenes,
                                         CT_AbstractResult* resultScenes,
                                         AttributEntier *outCoulors,
                                         QString modelNameCoulors,
                                         CT_AbstractResult* resultCoulors);

    void orientPcaBasis(QVector<CT_LineData *> &curPolyline);

    int nbPointsMin() const;
    void setNbPointsMin(int nbPointsMin);

    NORM_NewOctreeNode *root() const;
    void setRoot(NORM_NewOctreeNode *root);

    float sigmaThreshPerCent() const;
    void setSigmaThreshPerCent(float sigmaThreshPerCent);

    float rmseThresh() const;
    void setRmseThresh(float rmseThresh);

    float dimensionMinAcp() const;
    void setDimensionMinAcp(float dimensionMinAcp);

    CT_PCIR indexCloud() const;
    void setIndexCloud(const CT_PCIR &indexCloud);

    QVector< CT_Scene* >* segmentFromPcaDirections( double maxAngle,
                                                    QString modelName,
                                                    CT_AbstractResult* result);

    QVector< CT_Scene* >* segmentFromNormalDirections( double maxAngle,
                                                       QString modelName,
                                                       CT_AbstractResult* result);

private:
    CT_Point     _bot;                  /*!< Point inferieur gauche du cube englobant */
    CT_Point     _top;                  /*!< Point superieur droit du cube englobant */

    float        _dimensionMin;         /*!< Demi-dimension minimale d'une feuille de l'octre */
    float        _dimensionMinAcp;      /*!< Demi-dimension a partir de laquelle on fait les calculs d'acp et de fitting */
    int          _nbPointsMin;          /*!< Nombre de points minimum (inclu) dans une feuille de l'octree */
    float        _sigmaThreshPerCent;   /*!< Seuil sur le sigma en pourcent (entre 0 et 100 au lieu de 0 et 1/3) */
    float        _rmseThresh;           /*!< Seuil sur la rmse du fitting de quadrique */

    NORM_NewOctreeNode*     _root;      /*!< Noeud racine de l'octree */

    CT_PCIR                 _indexCloud;/*!< Nuage d'indices qui a servi a creer l'octree */

    const static NORM_NewOctreeDrawManager  NEW_OCTREE_DRAW_MANAGER;    /*!< Draw manager of the octree itemdrawable */
};

#endif // NORM_NEWOCTREE_H
