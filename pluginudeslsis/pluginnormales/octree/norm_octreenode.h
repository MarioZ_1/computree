#ifndef NORM_OCTREENODE_H
#define NORM_OCTREENODE_H

#include <QVector>

#include "ct_point.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "norm_abstractnode.h"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesColor AttributColor;

//  Ordre des fils :
//
//  Y             6 - 7
// /|\           /   / |
//  |           4 _ 5  |
//  |    Z      |   |  3
//  |   /       |   | /
//  |  /        0 _ 1
//  | /
//  |/______________\ X
//                  /
//
// Morton order : z first, then y then x

class NORM_AbstractOctree;

class NORM_OctreeNode : public NORM_AbstractNode
{
public:
    NORM_OctreeNode( NORM_AbstractOctree* octree,
                     NORM_OctreeNode* father,
                     size_t first,
                     size_t last,
                     const CT_Point& center,
                     size_t codex,
                     size_t codey,
                     size_t codez );

    virtual bool isSubdivisible();

    virtual void subdivide();

    void createOctreeRecursive();

    void createOctreeRecursive( int minPcaDepth );

    void setIdAttributeRecursive(AttributEntier* attributs, size_t currentID );

    void setColorAttributeRecursive(CT_AbstractColorCloud *attributs, size_t currentID );

    void getChildCenterFromID(size_t id, CT_Point& outChildCenter );

    void getChildCodeFromID(size_t id , size_t &outCodeX, size_t &outCodeY, size_t &outCodeZ);

    void getCenterFromID( size_t id );

    void printInfos();
    void printInfosRecursive();

    inline size_t nbPoints () const
    {
        if( _last > _first )
        {
            return ( _last - _first );
        }

        else
        {
            return 0;
        }
    }

    // TEMPORAIRE
    void printNodesInBBoxRecursive(const CT_Point& bot, const CT_Point top , size_t &totalNbPts);
    // TEMPORAIRE FIN

    void printBBox();
    void printCodes();
    void getBoundingBox( CT_Point& outBot, CT_Point& outTop );
    double getBot( size_t c );
    double getTop( size_t c );

    void printPoints();

    CT_Point center() const;
    void setCenter(const CT_Point &center);

    void printBinary(size_t n, size_t length , QString bef = "");

    inline bool isLeaf() const
    {
        return ( _children.at(0) == NULL );
    }

    NORM_OctreeNode* leafNodeAtCodeRecursive( size_t codex,
                                              size_t codey,
                                              size_t codez,
                                              size_t maxDepth = std::numeric_limits<size_t>::max() );

    NORM_OctreeNode* parentNodeAtCodeRecursive(size_t maxDepth );

    CT_PointCloudIndexVector* getIndexCloud();

    QVector<CT_Point>* getPointCloud();

    void getAllLeavesRecursive(QVector<NORM_AbstractNode *> &outLeafNodes);

    void getAllLeavesRecursiveInSphere(QVector<NORM_AbstractNode *> &outLeafNodes, const CT_Point& center, float radius );

    bool intersectsSphere( const CT_Point& center, float radius );

    void getCentersOfNeighboursAtSameLevel( QVector<CT_Point>& outCentersOfNeighbours );

    void leavesInTraversalDirectionRecursive(const CT_Point& traversalDirection,
                                              QVector<NORM_AbstractNode *> &outNeiLeafInTraversalDirection );

    void getTraversedChildInDirection(const CT_Point &direction, QVector<NORM_OctreeNode*>& outTraversedChildrenInDirection );

    void drawRecursive(GraphicsViewInterface& view,
                       PainterInterface& painter,
                       size_t idChild) const;

    inline void setOctree( NORM_AbstractOctree* octree )
    {
        _octree = octree;
    }

    NORM_OctreeNode* getChildren( int idChildren )
    {
        return _children.at( idChildren );
    }

    size_t first() const;
    void setFirst(const size_t &first);

    size_t last() const;
    void setLast(const size_t &last);

protected :
    NORM_AbstractOctree*        _octree;

    size_t                      _codeX;
    size_t                      _codeY;
    size_t                      _codeZ;

    size_t                      _first;
    size_t                      _last;

    size_t                      _depth;
    float                       _width;

    NORM_OctreeNode*            _father;
    QVector< NORM_OctreeNode* > _children;

    CT_Point                    _center;
    CT_Point                    _centroid;

    const static QVector<QColor>_qColorChild;
    const static QVector<CT_Color>_ctColorChild;
};

/*!
 * \brief SphereBBoxCollision
 *
 * Box/sphere collision test.
 *
 * \param bot : bottom left corner of the bounding box
 * \param top : top right corner of the bounding box
 * \param center : center of the sphere
 * \param radius : radius of the sphere
 *
 * \return true if the box and the sphere collide
 */
bool sphereBBoxCollision (const CT_Point &bot, const CT_Point &top, const CT_Point& center, float radius);

/*!
 * \brief pointIsInBBox
 *
 * Indicates if a point is inside a box
 *
 * \param bot : bottom left corner of the box
 * \param top : top right corner of the box
 * \param p : querry point
 * \return true if p is inside the box, false else
 */
bool pointIsInBBox( const CT_Point& bot, const CT_Point& top, const CT_Point& p );

#endif // NORM_OCTREENODE_H
