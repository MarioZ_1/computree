#ifndef NOMR_OCTREE_H
#define NOMR_OCTREE_H

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"
#include "norm_abstractoctree.h"

#include "norm_abstractnode.h"

#include "ct_point.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_cloudindex/registered/ct_standardmodifiablecloudindexregisteredt.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_result/abstract/ct_abstractresult.h"
#include "norm_octreedrawmanager.h"

class NORM_AbstractNode;

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesColor AttributCouleur;

//  Ordre des fils :
//
//  Y             6 - 7
// /|\           /   / |
//  |           4 _ 5  |
//  |    Z      |   |  3
//  |   /       |   | /
//  |  /        0 _ 1
//  | /
//  |/______________\ X
//                  /

// **************************************************************************
// Frisken, Sarah F., and Ronald N. Perry. "Simple and efficient traversal methods for quadtrees and octrees." Journal of Graphics Tools 7.3 (2002): 1-11.
// **************************************************************************

class NORM_Octree : public CT_AbstractItemDrawableWithoutPointCloud, public NORM_AbstractOctree
{
public:
/* **************************************************************** */
/* Constructuors and destructors                                    */
/* **************************************************************** */
    /*!
     * \brief NORM_Octree
     *
     * Default constructor
     */
    NORM_Octree();

    /*!
     * \brief NORM_Octree
     *
     *  Constructor of the NORM_Octree class
     *  Constructs the octree from a given point cloud
     *  And registers a new index cloud with the same indices sorted in morton order
     *
     * \param minNodeSize : minimum authorized width of an octree cell
     * \param minNbPoints : minimum number of points for a cell to be subdivisble
     * \param bot : bottom left corner of the input point cloud
     * \param top : upper right corner of the input point cloud
     * \param inputIndexCloud : input point cloud
     */
    NORM_Octree(float minNodeSize,
                 size_t minNbPoints,
                 CT_Point bot,
                 CT_Point top,
                 CT_PCIR inputIndexCloud );

    /*!
     * \brief NORM_Octree
     *
     *  Constructor of the NORM_Octree class
     *  Constructs the octree from a given point cloud
     *  And registers a new index cloud with the same indices sorted in morton order
     *
     * \param minNodeSize : minimum authorized width of an octree cell
     * \param minNbPoints : minimum number of points for a cell to be subdivisble
     * \param bot : bottom left corner of the input point cloud
     * \param top : upper right corner of the input point cloud
     * \param inputIndexCloud : input point cloud
     * \param model : model of the octree in the result
     * \param result : result in which put the octree
     */
    NORM_Octree(float minNodeSize,
                 size_t minNbPoints,
                 CT_Point bot,
                 CT_Point top,
                 CT_PCIR inputIndexCloud,
                 const CT_OutAbstractSingularItemModel *model,
                 const CT_AbstractResult *result );

    /*!
     * \brief NORM_Octree
     *
     *  Constructor of the NORM_Octree class
     *  Constructs the octree from a given point cloud
     *  And registers a new index cloud with the same indices sorted in morton order
     *
     * \param minNodeSize : minimum authorized width of an octree cell
     * \param minNbPoints : minimum number of points for a cell to be subdivisble
     * \param bot : bottom left corner of the input point cloud
     * \param top : upper right corner of the input point cloud
     * \param inputIndexCloud : input point cloud
     * \param modelName : modelname in the result
     * \param result : result in which put the octree
     */
    NORM_Octree(float minNodeSize,
                 size_t minNbPoints,
                 CT_Point bot,
                 CT_Point top,
                 CT_PCIR inputIndexCloud,
                 const QString &modelName,
                 const CT_AbstractResult *result);

    /*!
     * \brief initRoot
     *  Refer to abstract octree
     */
    virtual void initRoot() = 0;

    /*!
     * \brief createOctree
     *
     * Recursively constructs the octreee according to the octree node subdivision rules
     * \warning Don't forget to call this method after you create an octree
     *
     */
    virtual void createOctree();

/* **************************************************************** */
/* Querry tools                                                     */
/* **************************************************************** */
    /*!
     * \brief nodeContainingPoint
     *
     * Access the octree leaf containing a querry point
     *
     * \param p : querry point
     *
     * \return the node containing p
     */
    NORM_AbstractNode* nodeContainingPoint( CT_Point& p ) const;

    /*!
     * \brief nodeAtCode
     *
     * Access the octree leaf from a given code
     *
     * \param codex : querry code
     * \param codey : querry code
     * \param codez : querry code
     *
     * \return the leaf corresponding to the querry code
     */
    NORM_AbstractNode* nodeAtCode( size_t codex,
                       size_t codey,
                       size_t codez ) const;

    /*!
     * \brief nodeAtCodeAndMaxDepth
     *
     * Access the deepest octree node with the given code and which depth is no more than a given depth
     *
     * \param codex : querry code
     * \param codey : querry code
     * \param codez : querry code
     * \param maxDepth : maximum depth to reach
     *
     * \return the deepest cell within the depth range corresponding to the querry code
     */
    NORM_AbstractNode* nodeAtCodeAndMaxDepth( size_t codex,
                                  size_t codey,
                                  size_t codez,
                                  size_t maxDepth ) const;

    /*!
     * \brief neighboursNodeAtSameLevelOrAbove
     *
     * Search the nodes at same depth than the querry node that are neighbour of the querry node
     *
     * \param querryNode : querry node
     * \param outNeighbours : vector of its neighbour nodes
     * \param insertNullNodes : when set to true, the output will also contain nodes that are outside the bounding cube as NULL pointers (used in the neighboursLeafNodes method)
     */
    void neighboursNodeAtSameLevelOrAbove (NORM_AbstractNode* querryNode,
                                           QVector<NORM_AbstractNode*>& outNeighbours,
                                           bool insertNullNodes = false );

    /*!
     * \brief neighboursLeafNodes
     *
     * Search all the leaves that are neighbours of a querry node
     *
     * \param querryNode : querry node
     * \param outNeighbours : vector of the neighbour nodes
     */
    void neighboursLeafNodes ( NORM_AbstractNode* querryNode,
                               QVector<NORM_AbstractNode*>& outNeighbours );

    /*!
     * \brief deepestNodeContainingBBBox
     *
     * Access the deepest node containing a bounding box
     *
     * \param bot : bottom left corner of the bounding box
     * \param top : upper right corner of the bounding box
     *
     * \return the deepest node containing the bounding box
     */
    NORM_AbstractNode* deepestNodeContainingBBBox(const CT_Point &bot,
                                      const CT_Point &top) const;

    /*!
     * \brief sphericalNeighbourhood
     *
     * Calcule un voisinage spherique autour d'un point.
     * Ce voisinage comprend ce point lui-meme
     *
     * \param center : point autour duquel prendre un voisinage
     * \param radius : rayon du voisinage spherique
     *
     * \return un tableau d'indices des points dans le voisinage spherique
     */
    CT_PointCloudIndexVector* sphericalNeighbourhood( const CT_Point& center,
                                                      double radius ) const;

    /*!
     * \brief approximateSphericalNeighbourhood
     *
     * Calcule une approximation de voisinage spherique :
     * tous les points de toutes les cellules de l'octree étant totalement ou partiellement inclues dans la sphere de voisinage
     *
     * \param center : point autour duquel prendre un voisinage
     * \param radius : rayon du voisinage spherique
     *
     * \return un tableau d'indices des points dans le voisinage quasi-spherique
     */
    CT_PointCloudIndexVector* approximateSphericalNeighbourhood( const CT_Point& center,
                                                                 double radius ) const;

/* **************************************************************** */
/* Tools                                                            */
/* **************************************************************** */
    /*!
     * \brief getIdAttribute
     *
     *  Creates an attribute cloud :
     *  Each point of the cloud is given an ID attribute corresponding to its location in the octree (from 0 to 7)
     *
     * \param modelName : name of the attribute cloud model
     * \param result : the attribute cloud has to be attached to a model
     *
     * \return an attribute cloud of ID of each point in the octree
     */
    AttributEntier* getIdAttribute(QString modelName,
                                   CT_AbstractResult* result);

    /*!
     * \brief getIdAttribute
     *
     *  Creates an attribute cloud :
     *  Each point of the cloud is given an color attribute corresponding to its location in the octree (from blue to red)
     *
     * \param modelName : name of the attribute cloud model
     * \param result : the attribute cloud has to be attached to a model
     *
     * \return an attribute cloud of color of each point in the octree
     */
    AttributCouleur* getColorAttribute(QString modelName,
                                       CT_AbstractResult* result);

    /*!
     * \brief contains
     *
     * Indicates if the octree contains a given point
     * A point is inside the octree if it is inside the bounding cube
     *
     * \param p : querry point
     *
     * \return true is p is inside the bounding cube
     */
    bool contains ( const CT_Point& p );

    /*!
     * \brief draw
     *
     * Draw each cell of the octree as boxes
     *
     * \param view : view to draw
     * \param painter : painter used to draw objects
     */
    inline void draw(GraphicsViewInterface& view,
                     PainterInterface& painter );

    /*!
     * \brief printTree
     *
     * Prints the nodes of the octree
     */
    void printTree() const;

/* **************************************************************** */
/* Auto generated getters and setters                               */
/* **************************************************************** */
    inline CT_MPCIR modifiablepointCloudIndexRegistered() const
    {
        return _indexCloud;
    }

    NORM_AbstractNode* root() const;
    void setRoot(NORM_AbstractNode *root);

    size_t maxDepth() const;
    void setMaxDepth(const size_t &maxDepth);

    float minNodeSize() const;
    void setMinNodeSize(float minNodeSize);

    size_t minNbPoints() const;
    void setMinNbPoints(const size_t &minNbPoints);

    CT_Point bot() const;
    void setBot(const CT_Point &bot);

    CT_Point top() const;
    void setTop(const CT_Point &top);

    CT_PCIR inputIndexCloud() const;
    void setInputIndexCloud(const CT_PCIR &inputIndexCloud);

    CT_MPCIR indexCloud() const;
    void setIndexCloud(const CT_MPCIR &indexCloud);

/* **************************************************************** */
/* CT_AbstractItemDrawableWithoutPointCloud method redefinition     */
/* **************************************************************** */
    virtual QString getType() const
    {
        return staticGetType();
    }

    static QString staticGetType()
    {
        return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/NORM_Circle";
    }

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList ) = 0;

/* **************************************************************** */
/* Class data members                                               */
/* **************************************************************** */
protected :
    NORM_AbstractNode*          _root;              /*!< Octree root node */

    size_t                      _maxDepth;          /*!< Maximum depth authorized in the tree */
    size_t                      _powMaxDepth;       /*!< Storage of the value 2^(madDepth - 1) used for point querry purposes */
    float                       _minNodeSize;       /*!< Minimum width of a cell */
    size_t                      _minNbPoints;       /*!< Minimum number of point for a cell to be subdivisible */

    CT_Point                    _bot;               /*!< Bottom left corner of the input point cloud */
    CT_Point                    _top;               /*!< Upper right corner of the input point cloud */

    CT_Point                    _inputBot;          /*!< Bottom left corner of the bounding cube */
    CT_Point                    _inputTop;          /*!< Upper right corner of the bounding cube */

    CT_PCIR                     _inputIndexCloud;   /*!< Input point cloud */
    CT_MPCIR                    _indexCloud;        /*!< Index cloud as modified during octree generation */

    const static NORM_OctreeDrawManager    OCTREE_DRAW_MANAGER; /*!< Draw manager of the octree itemdrawable */
};

/*!
 * \brief indexOfMostSignificantBit
 *
 * Calculates the index of the left most significant bit from a variable containing a known number of digits
 *
 * \param n : variable we want to know the msb
 * \param nBits : number of digits in n
 *
 * \return the index of the most significant bit
 */
size_t indexOfMostSignificantBit(size_t n , size_t nBits);

#endif // NOMR_OCTREE_H
