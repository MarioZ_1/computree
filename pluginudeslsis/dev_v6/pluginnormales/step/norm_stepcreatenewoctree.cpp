/************************************************************************************
* Filename :  norm_stepcreatenewoctree.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepcreatenewoctree.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/ct_scene.h"

#include "newoctree/norm_newoctree.h"

#include <QDebug>

// Alias for indexing models
#define DEFin_Result "inRslt_IndexCloud"
#define DEFin_Group "inGrp_IndexCloud"
#define DEFin_IndexCloud "inItm_IndexCloud"

#define DEFout_ResultOctree "outRslt_Octree"
#define DEFout_GroupOctree "outGrp_GroupOctree"
#define DEFout_ItemOctree "outItm_Octree"
#define DEFout_ItemAttributeNormals "out_Itm_Normals"
#define DEFout_ItemAttributeCloudIdNode "outItm_IndexNode"
#define DEFout_ItemAttributeCloudSigmaNode "outItm_SigmaNode"
#define DEFout_ItemAttributeCloudLinearityNode "outItm_LinearityNode"
#define DEFout_ItemAttributeCloudFittingError "outItm_FittingError"
#define DEFout_ItemAttributeCloudFittingRmse "outItm_FittingRmse"
#define DEFout_ItemAttributeCloudColorNode "out_Itm_ColorNode"
#define DEFout_ItemAttributeQuadraSign "out_Itm_QuadraSign"

#define DEFout_GroupGroupLine "out_Grp_Grp_Line"
#define DEFout_GroupLine "out_Grp_Line"
#define DEFout_ItemLine "out_Itm_Line"

#define DEFout_GroupGroupScene "out_Grp_Grp_Scene"
#define DEFout_GroupScene "out_Grp_Scene"
#define DEFout_ItemScene "out_Itm_Scene"

#define DEFout_GroupGroupQuadraSign "out_Grp_Grp_QuadraSign"
#define DEFout_GroupQuadraSign "out_Grp_QuadraSign"
#define DEFout_ItemQuadraSign "out_Itm_QuadraSign"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;
typedef CT_PointsAttributesColor AttributCouleur;
typedef CT_PointsAttributesNormal AttributeNormals;

// Constructor : initialization of parameters
NORM_StepCreateNewOctree::NORM_StepCreateNewOctree(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _dimensionMin = 0.01;
    _dimensionMinAcp = 1;
    _nbPointsMin = 10;
    _threshSigmaPerCent = 10;
    _threshRmse = 0.001;

    _scanPosx = 0;
    _scanPosy = 0;
    _scanPosz = 0;
    _towards = true;
}

// Step description (tooltip of contextual menu)
QString NORM_StepCreateNewOctree::getStepDescription() const
{
    return tr("Cree un nouvel octree classique");
}

// Step detailled description
QString NORM_StepCreateNewOctree::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepCreateNewOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepCreateNewOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepCreateNewOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepCreateNewOctree::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_Result = createNewInResultModelForCopy(DEFin_Result, tr("inResult"), tr(""), true);
    resIn_Result->setRootGroup(DEFin_Group, CT_AbstractItemGroup::staticGetType(), tr("inGroup"));
    resIn_Result->addItemModel(DEFin_Group, DEFin_IndexCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("IndexCloud"));
}

// Creation and affiliation of OUT models
void NORM_StepCreateNewOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultOctree = createNewOutResultModel(DEFout_ResultOctree, tr("Resultat creation octree"));
    res_ResultOctree->setRootGroup(DEFout_GroupOctree, new CT_StandardItemGroup(), tr("GroupeOctree"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemOctree, new NORM_NewOctree(), tr("Octree"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudIdNode, new AttributEntier(), tr("Identifiant des noeuds [0,7]"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudSigmaNode, new AttributFloat(), tr("Sigma des noeuds"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudColorNode, new AttributCouleur(), tr("Coloration des points par noeuds"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeNormals, new AttributeNormals(), tr("Normales calculees par fitting de quadriques"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudFittingError, new AttributFloat(), tr("Erreur de fitting"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudFittingRmse, new AttributFloat(), tr("Fitting RMSE"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudLinearityNode, new AttributFloat(), tr("Linearity index"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeQuadraSign, new AttributEntier(), tr("Quadra Sign"));

    res_ResultOctree->addGroupModel(DEFout_GroupOctree, DEFout_GroupGroupLine, new CT_StandardItemGroup(), tr("Group Group Line"));
    res_ResultOctree->addGroupModel(DEFout_GroupGroupLine, DEFout_GroupLine, new CT_StandardItemGroup(), tr("Group Line"));
    res_ResultOctree->addItemModel(DEFout_GroupLine, DEFout_ItemLine, new CT_Line(), tr("Line"));

    res_ResultOctree->addGroupModel(DEFout_GroupOctree, DEFout_GroupGroupScene, new CT_StandardItemGroup(), tr("Group Group Scene"));
    res_ResultOctree->addGroupModel(DEFout_GroupGroupScene, DEFout_GroupScene, new CT_StandardItemGroup(), tr("Group Scene"));
    res_ResultOctree->addItemModel(DEFout_GroupScene, DEFout_ItemScene, new CT_Scene(), tr("Scene"));

    res_ResultOctree->addGroupModel(DEFout_GroupOctree, DEFout_GroupGroupQuadraSign, new CT_StandardItemGroup(), tr("Group Group QuadraSign"));
    res_ResultOctree->addGroupModel(DEFout_GroupGroupQuadraSign, DEFout_GroupQuadraSign, new CT_StandardItemGroup(), tr("Group QuadraSign"));
    res_ResultOctree->addItemModel(DEFout_GroupQuadraSign, DEFout_ItemQuadraSign, new AttributEntier(), tr("QuadraSign"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepCreateNewOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Demi-taille de cellule minimale", "[m]", 0.0001, 100, 4, _dimensionMin, 1);
    configDialog->addDouble("Demi-dimension a partir de laquelle faire l'acp", "[m]", 0.0001, 100, 4, _dimensionMinAcp, 1);
    configDialog->addInt("Nombre de points minimum par cellule", "", 1, 10000, _nbPointsMin );
    configDialog->addDouble("Seuil sigma", "[%]", 0.0001, 100, 4, _threshSigmaPerCent, 1);
    configDialog->addDouble("Seuil Rmse", "[m]", 0.0001, 100, 4, _threshRmse, 1);

    configDialog->addDouble("Scan pos x", "[m]", -9999, 9999, 4, _scanPosx, 1);
    configDialog->addDouble("Scan pos y", "[m]", -9999, 9999, 4, _scanPosy, 1);
    configDialog->addDouble("Scan pos z", "[m]", -9999, 9999, 4, _scanPosz, 1);
    configDialog->addBool("Orient towards scan pos (backward else)", "", "", _towards );
}

void NORM_StepCreateNewOctree::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_Group);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();

        const CT_AbstractItemDrawableWithPointCloud* itemIn_IndexCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_Group->firstItemByINModelName(this, DEFin_IndexCloud);
        CT_PCIR inputIndexCloud = itemIn_IndexCloud->getPointCloudIndexRegistered();

        if (itemIn_IndexCloud != NULL)
        {
            CT_StandardItemGroup* grp_GroupOctree= new CT_StandardItemGroup(DEFout_GroupOctree, res_ResultOctree);
            res_ResultOctree->addGroup(grp_GroupOctree);

            // On cree l'octree
            CT_Point bot = createCtPoint( itemIn_IndexCloud->minX(), itemIn_IndexCloud->minY(), itemIn_IndexCloud->minZ() );
            CT_Point top = createCtPoint( itemIn_IndexCloud->maxX(), itemIn_IndexCloud->maxY(), itemIn_IndexCloud->maxZ() );
            QVector<CT_LineData*> polyLine;
            NORM_NewOctree* octree = new NORM_NewOctree (bot,
                                                         top,
                                                         _dimensionMin,
                                                         _dimensionMinAcp,
                                                         _nbPointsMin,
                                                         _threshSigmaPerCent,
                                                         _threshRmse,
                                                         inputIndexCloud,
                                                         DEFout_ItemOctree,
                                                         res_ResultOctree,
                                                         polyLine );


            CT_StandardItemGroup* grp_GroupGroupLine = new CT_StandardItemGroup(DEFout_GroupGroupLine, res_ResultOctree);
            grp_GroupOctree->addGroup( grp_GroupGroupLine );
            int nLines = polyLine.size();
            for( int i = 0 ; i < nLines ; i++ )
            {
                CT_Line* curLine = new CT_Line( DEFout_ItemLine, res_ResultOctree, polyLine.at(i) );
                CT_StandardItemGroup* grp_GroupLine = new CT_StandardItemGroup(DEFout_GroupLine, res_ResultOctree);
                grp_GroupGroupLine->addGroup( grp_GroupLine );
                grp_GroupLine->addItemDrawable( curLine );
            }

            QVector<CT_Scene*> quadraScenes;
            AttributEntier* item_AttributeCloudQuadraSign = new AttributEntier( DEFout_ItemAttributeQuadraSign, res_ResultOctree, octree->indexCloud() );
            octree->getQuadraticPointCloudAndSign( quadraScenes, DEFout_ItemScene, res_ResultOctree,
                                                   item_AttributeCloudQuadraSign, DEFout_ItemQuadraSign, res_ResultOctree );

            CT_StandardItemGroup* grp_GroupGroupScene = new CT_StandardItemGroup(DEFout_GroupGroupScene, res_ResultOctree);
            grp_GroupOctree->addGroup( grp_GroupGroupScene );
            int nScenes = quadraScenes.size();
            for( int i = 0 ; i < nScenes ; i++ )
            {
                CT_Scene* curScene = quadraScenes.at(i);
                CT_StandardItemGroup* grp_GroupScene = new CT_StandardItemGroup(DEFout_GroupScene, res_ResultOctree);
                grp_GroupGroupScene->addGroup( grp_GroupScene );
                grp_GroupScene->addItemDrawable( curScene );
            }


//            CT_StandardItemGroup* grp_GroupGroupQuadraSign = new CT_StandardItemGroup(DEFout_GroupGroupQuadraSign, res_ResultOctree);
//            grp_GroupOctree->addGroup( grp_GroupGroupQuadraSign );
//            int nQuadraSign = quadraAttributes.size();
//            for( int i = 0 ; i < nQuadraSign ; i++ )
//            {
//                qDebug() << "New entier" << i;
//                AttributEntier* curQuadSign = quadraAttributes.at(i);
//                CT_StandardItemGroup* grp_GroupQuadraSign = new CT_StandardItemGroup(DEFout_GroupQuadraSign, res_ResultOctree);
//                grp_GroupGroupQuadraSign->addGroup( grp_GroupQuadraSign );
//                grp_GroupQuadraSign->addItemDrawable( curQuadSign );
//            }

            AttributEntier* item_AttributeCloudIdNode = octree->getNbPointsAttribute( DEFout_ItemAttributeCloudIdNode, res_ResultOctree );
            AttributFloat* item_AttributeCloudSigmaNode = octree->getSigmaPercentAttribute(DEFout_ItemAttributeCloudSigmaNode, res_ResultOctree );
            AttributCouleur* item_AttributeCloudColorNode = octree->getColorAttribute( DEFout_ItemAttributeCloudColorNode, res_ResultOctree );

            CT_Point scanPos = createCtPoint( _scanPosx,
                                              _scanPosy,
                                              _scanPosz );
            AttributeNormals* item_AttributeNormals = octree->getNormalsRecursive( DEFout_ItemAttributeNormals, res_ResultOctree, scanPos, _towards );
//            AttributeNormals* item_AttributeNormals = octree->getNormalsRecursiveLinearInterpolation( DEFout_ItemAttributeNormals, res_ResultOctree, scanPos, _towards, 10 );
//            AttributeNormals* item_AttributeNormals = octree->getNormalsRecursiveCRBFInterpolation( DEFout_ItemAttributeNormals, res_ResultOctree, scanPos, _towards, 2 );

            AttributFloat* item_AttributeCloudFittingError = octree->getFittingErrorAttribute( DEFout_ItemAttributeCloudFittingError, res_ResultOctree );
            AttributFloat* item_AttributeCloudFittingRmse = octree->getFittingRmseAttribute( DEFout_ItemAttributeCloudFittingRmse, res_ResultOctree );
            AttributFloat* item_AttributeCloudLinearity = octree->getLineaityAttribute( DEFout_ItemAttributeCloudLinearityNode, res_ResultOctree );

            grp_GroupOctree->addItemDrawable( octree );
            grp_GroupOctree->addItemDrawable(item_AttributeCloudIdNode);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudSigmaNode);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudColorNode);
            grp_GroupOctree->addItemDrawable(item_AttributeNormals);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudFittingError);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudFittingRmse);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudLinearity);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudQuadraSign);
        }
    }
}
