#include "norm_newsteploadoctree.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "new/norm_newoctreev2.h"

// Alias for indexing models
#define DEFout_rsltOctree "rsltOctree"
#define DEFout_grpOctree "grpOctree"
#define DEFout_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepLoadOctree::NORM_NewStepLoadOctree(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepLoadOctree::getStepDescription() const
{
    return tr("A - Charge un octree compresse depuis un fichier lsis_octree");
}

// Step detailled description
QString NORM_NewStepLoadOctree::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepLoadOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepLoadOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepLoadOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepLoadOctree::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void NORM_NewStepLoadOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltOctere = createNewOutResultModel(DEFout_rsltOctree, tr("Result Octree"));
    res_rsltOctere->setRootGroup(DEFout_grpOctree, new CT_StandardItemGroup(), tr("Group"));
    res_rsltOctere->addItemModel(DEFout_grpOctree, DEFout_itmOctree, new NORM_NewOctreeV2(), tr("Octree"));

}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepLoadOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice("Octree file", CT_FileChoiceButton::OneExistingFile, "*.lsis_octree", _fileNames);
}

void NORM_NewStepLoadOctree::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rsltOctree = outResultList.at(0);

    CT_StandardItemGroup* grp_grpOctree = new CT_StandardItemGroup(DEFout_grpOctree, res_rsltOctree);
    res_rsltOctree->addGroup(grp_grpOctree);

    foreach ( QString curFileName, _fileNames )
    {
        NORM_NewOctreeV2* octree = NORM_NewOctreeV2::loadFromFile( curFileName,
                                                                   DEFout_itmOctree,
                                                                   res_rsltOctree );
        grp_grpOctree->addItemDrawable( octree );
    }
}
