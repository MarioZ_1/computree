/************************************************************************************
* Filename :  norm_newoctreenode.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_newoctreenode.h"
#include "norm_newoctree.h"
#include "tools/norm_acp.h"
#include "tools/norm_leastsquarefitting.h"
#include "tools/norm_interpolation.h"
#include "ct_color.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_itemdrawable/ct_scene.h"
#include "tools/norm_boundingbox.h"
#include "ct_iterator/ct_mutablepointiterator.h"
#include <QQueue>
#include <QDebug>

#include "streamoverload/streamoverload.h"

#define QCOLOR0 QColor::fromHsv( 0, 255, 255)
#define QCOLOR1 QColor::fromHsv( 45, 255, 255)
#define QCOLOR2 QColor::fromHsv( 90, 255, 255)
#define QCOLOR3 QColor::fromHsv( 135, 255, 200)
#define QCOLOR4 QColor::fromHsv( 180, 255, 255)
#define QCOLOR5 QColor::fromHsv( 225, 255, 255)
#define QCOLOR6 QColor::fromHsv( 270, 255, 255)
#define QCOLOR7 QColor::fromHsv( 315, 255, 255)

#define CTCOLOR0 CT_Color( QColor::fromHsv( 0, 255, 255) )
#define CTCOLOR1 CT_Color( QColor::fromHsv( 45, 255, 255) )
#define CTCOLOR2 CT_Color( QColor::fromHsv( 90, 255, 255) )
#define CTCOLOR3 CT_Color( QColor::fromHsv( 135, 255, 200) )
#define CTCOLOR4 CT_Color( QColor::fromHsv( 180, 255, 255) )
#define CTCOLOR5 CT_Color( QColor::fromHsv( 225, 255, 255) )
#define CTCOLOR6 CT_Color( QColor::fromHsv( 270, 255, 255) )
#define CTCOLOR7 CT_Color( QColor::fromHsv( 315, 255, 255) )

#define NORM_NEW_OCTREE_EPSILONE 0.000001

const QVector<QColor> NORM_NewOctreeNode::_qColorChild = QVector<QColor>() << QCOLOR0 << QCOLOR1 << QCOLOR2 << QCOLOR3 << QCOLOR4 << QCOLOR5 << QCOLOR6 << QCOLOR7;
const QVector<CT_Color> NORM_NewOctreeNode::_ctColorChild = QVector<CT_Color>() << CTCOLOR0 << CTCOLOR1 << CTCOLOR2 << CTCOLOR3 << CTCOLOR4 << CTCOLOR5 << CTCOLOR6 << CTCOLOR7;

NORM_NewOctreeNode::NORM_NewOctreeNode(NORM_NewOctree *octree,
                                       NORM_NewOctreeNode *father,
                                       CT_Point &center,
                                       double dimension ) :
    _center( center),
    _centroid( createCtPoint() ),
    _dimension( dimension ),
    _nbPoints( 0 ),
    _octree( octree ),
    _father( father ),
    _ptIndices( new QVector<int>() ),
    _visited(false)
{
    _children.resize(8);
    for( int i = 0 ; i < 8 ; i++ )
    {
        _children[i] = NULL;
    }

    // On regarde si le noeud courant peut etre subdivise par rapport a sa taille
    if( !isLeafDimension() )
    {
        delete _ptIndices;
        _ptIndices = NULL;
    }
}

NORM_NewOctreeNode::~NORM_NewOctreeNode()
{
    if( !isLeaf() )
    {
        for( int i= 0 ; i < 8 ; i++ )
        {
            if( _children.at(i) != NULL )
            {
                delete _children.at(i);
            }
        }
    }

    else
    {
        delete _ptIndices;
    }
}

bool NORM_NewOctreeNode::isLeafDimension() const
{
    return ( _dimension < _octree->dimensionMin() );
}

bool NORM_NewOctreeNode::isLeafNbPoint() const
{
    for( int i = 0 ; i < 8 ; i++ )
    {
        if( _children.at(i) != NULL )
        {
            if( _children.at(i)->_nbPoints < _octree->nbPointsMin() )
            {
                return true;
            }
        }
    }

    return false;
}

bool NORM_NewOctreeNode::isLeafNbPointV2(int nFilsErreurMax) const
{
    int nFilsErreur = 0;

    for( int i = 0 ; i < 8 ; i++ )
    {
        if( _children.at(i) != NULL )
        {
            if( _children.at(i)->_nbPoints < _octree->nbPointsMin() )
            {
                nFilsErreur++;
            }
        }
    }

    return ( nFilsErreur >= nFilsErreurMax );
}

int NORM_NewOctreeNode::getOctantContainingPoint(const CT_Point &point) const
{
    int oct = 0;

    if(point.x() >= _center.x()) oct |= 4;
    if(point.y() >= _center.y()) oct |= 2;
    if(point.z() >= _center.z()) oct |= 1;

    return oct;
}

void NORM_NewOctreeNode::insert(const CT_Point& point, int index)
{
    _nbPoints++;
    _centroid += point;

    // Si c'est une feuille, on ajoute l'indice
    if( isLeafDimension() )
    {
        _ptIndices->push_back( index );
        return;
    }

    // Si ce n'est pas une feuille on ajoute le point dans l'octant correspondant
    else
    {
        // Calcul de l'octant
        int octant = getOctantContainingPoint( point );

        // Creation des noeuds fils
        double childDimension = _dimension / 2.0;

        // On calcule le centre du fils correspondant
        CT_Point childCenter = _center;
        childCenter.x() += _dimension * 0.5 * (octant&4 ? 1 : -1 );
        childCenter.y() += _dimension * 0.5 * (octant&2 ? 1 : -1 );
        childCenter.z() += _dimension * 0.5 * (octant&1 ? 1 : -1 );

        // Et on cree le fils s'il n'existait pas deja
        if( _children[octant] == NULL )
        {
            _children[octant] = new NORM_NewOctreeNode( _octree,
                                                        this,
                                                        childCenter,
                                                        childDimension );
        }

        // Recursion
        _children[octant]->insert(point, index );
    }
}

void NORM_NewOctreeNode::updateCentroid()
{
    _centroid *= (1.0 / _nbPoints );

    for( size_t i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            _children[i]->updateCentroid();
        }
    }
}

void NORM_NewOctreeNode::filterFromNbPoints()
{
    if( !isLeafNbPoint() )
    {
        // On continue la recursion
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->filterFromNbPoints();
            }
        }
    }

    else
    {
        // Il faut recuperer tous les indices des noeuds fils et les copier dans le noeud courant
        _ptIndices = new QVector<int> ();
        updateIndicesFromChildren( _ptIndices );
        assert( _ptIndices->size() == _nbPoints );

        // Et on supprime les fils
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                delete _children[i];
                _children[i] = NULL;
            }
        }
    }
}

void NORM_NewOctreeNode::filterFromNbPointsV2( int nbFilsErreurMax )
{
    if( !isLeafNbPointV2( nbFilsErreurMax ) )
    {
        // On continue la recursion
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->filterFromNbPointsV2( nbFilsErreurMax );
            }
        }
    }

    else
    {
        // Il faut recuperer tous les indices des noeuds fils et les copier dans le noeud courant
        _ptIndices = new QVector<int> ();
        updateIndicesFromChildren( _ptIndices );
        assert( _ptIndices->size() == _nbPoints );

        // Et on supprime les fils
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                delete _children[i];
                _children[i] = NULL;
            }
        }
    }
}

void NORM_NewOctreeNode::filterFromAcp( float minSize )
{
    if( _dimension <= minSize )
    {
        // On met a jour l'ACP du noeud courant
        if( !isLeaf() )
        {
            _ptIndices = new QVector<int> ();
            updateIndicesFromChildren( _ptIndices );
        }

        if( _ptIndices->size() < 3 )
        {
            _l1 = 1;
            _l2 = 1;
            _l3 = 1;

            return;
        }

        NORM_Tools::NORM_PCA::pca( _ptIndices,
                                   _centroid,
                                   _l1, _l2, _l3,
                                   _v1, _v2, _v3 );

        assert( !std::isnan( getSigmaPerCent() ) );

        if( getSigmaPerCent() > _octree->sigmaThreshPerCent() )
        {
            // On continue la recursion
            if( !isLeaf() )
            {
                delete _ptIndices;
                _ptIndices = NULL;
            }

            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    _children[i]->filterFromAcp( minSize );
                }
            }
        }

        else
        {
            // On supprime les fils
            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    delete _children[i];
                    _children[i] = NULL;
                }
            }
        }
    }

    else
    {
        // On continue la recursion
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->filterFromAcp( minSize );
            }
        }
    }
}

void NORM_NewOctreeNode::filterFromAcpAndUpdateQuadra(float minSize)
{
    if( _dimension <= minSize || isLeaf() )
    {
        // On met a jour l'ACP du noeud courant
        if( !isLeaf() )
        {
            _ptIndices = new QVector<int> ();
            updateIndicesFromChildren( _ptIndices );
        }

        if( _ptIndices->size() < 3 )
        {
            _l1 = 1;
            _l2 = 1;
            _l3 = 1;

            return;
        }

        updateAcpAndQuadraticSurface();

        if( getSigmaPerCent() > _octree->sigmaThreshPerCent() )
        {
            // On continue la recursion
            if( !isLeaf() )
            {
                delete _ptIndices;
                _ptIndices = NULL;
            }

            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    _children[i]->filterFromAcpAndUpdateQuadra( minSize );
                }
            }
        }

        else
        {
            // On supprime les fils
            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    delete _children[i];
                    _children[i] = NULL;
                }
            }
        }
    }

    else
    {
        // On continue la recursion
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->filterFromAcpAndUpdateQuadra( minSize );
            }
        }
    }
}

void NORM_NewOctreeNode::filterFromRmseAndUpdateQuadra(float minSize)
{
    if( _dimension <= minSize || isLeaf() )
    {
        // On met a jour l'ACP du noeud courant
        if( !isLeaf() )
        {
            _ptIndices = new QVector<int> ();
            updateIndicesFromChildren( _ptIndices );
        }

        if( _ptIndices->size() < 3 )
        {
            _l1 = 1;
            _l2 = 1;
            _l3 = 1;

            return;
        }

        // On met a jour la surface quadratique du noeud
        _rmse = updateAcpAndQuadraticSurfaceAndGetRmse();

        // Si le critere de rmse fitting n'est pas rempli on subdivise encore
        if( _rmse > _octree->rmseThresh() )
        {
            // On continue la recursion
            if( !isLeaf() )
            {
                delete _ptIndices;
                _ptIndices = NULL;
            }

            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    _children[i]->filterFromRmseAndUpdateQuadra( minSize );
                }
            }
        }

        else
        {
            // On supprime les fils
            for( size_t i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    delete _children[i];
                    _children[i] = NULL;
                }
            }
        }
    }

    else
    {
        // On continue la recursion
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->filterFromRmseAndUpdateQuadra( minSize );
            }
        }
    }
}

void NORM_NewOctreeNode::updateIndicesFromChildren(QVector<int>* indices )
{
    if( isLeaf() )
    {
        (*indices) += (*_ptIndices);
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->updateIndicesFromChildren( indices );
            }
        }
    }
}

void NORM_NewOctreeNode::drawRecursive(GraphicsViewInterface &view,
                                       PainterInterface &painter,
                                       size_t idChild,
                                       bool drawNodeBoxes,
                                       QVector<bool>& drawBoxes,
                                       bool drawAcpBasis ) const
{
    // Draw each leaf and stop recursion
    if ( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            if( drawNodeBoxes )
            {
                if( drawBoxes.at(idChild) )
                {
                    painter.setColor( _qColorChild[idChild] );
                    painter.drawCube( _center(0) - _dimension, _center(1) - _dimension, _center(2) - _dimension,
                                      _center(0) + _dimension, _center(1) + _dimension, _center(2) + _dimension);
                }
            }

            if( drawAcpBasis )
            {
                if( drawBoxes.at(idChild) )
                {
                    painter.setColor( 255, 0, 0 );
                    painter.drawLine( _centroid.x(), _centroid.y(), _centroid.z(),
                                      _centroid.x() + (_v1.x() * _dimension), _centroid.y() + (_v1.y() * _dimension), _centroid.z() + (_v1.z() * _dimension) );
                    painter.setColor( 0, 255, 0 );
                    painter.drawLine( _centroid.x(), _centroid.y(), _centroid.z(),
                                      _centroid.x() + (_v2.x() * _dimension), _centroid.y() + (_v2.y() * _dimension), _centroid.z() + (_v2.z() * _dimension) );
                    painter.setColor( 0, 0, 255 );
                    painter.drawLine( _centroid.x(), _centroid.y(), _centroid.z(),
                                      _centroid.x() + (_v3.x() * _dimension), _centroid.y() + (_v3.y() * _dimension), _centroid.z() + (_v3.z() * _dimension) );
                }
            }
        }
    }

    // else continue recursion over children
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->drawRecursive( view, painter, i, drawNodeBoxes, drawBoxes, drawAcpBasis );
            }
        }
    }
}

void NORM_NewOctreeNode::setIdAttributeRecursive(AttributEntier *attributs, size_t currentID) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), currentID );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setIdAttributeRecursive( attributs, i );
            }
        }
    }
}

void NORM_NewOctreeNode::setNbPointsAttributeRecursive(AttributEntier *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), _nbPoints );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setNbPointsAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setSigmaAttributeRecursive(AttributFloat *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), getSigma() );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setSigmaAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setSigmaPercentAttributeRecursive(AttributFloat *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), getSigmaPerCent() );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setSigmaPercentAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setLinearityAttributeRecursive(AttributFloat *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), getLinearity() );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setLinearityAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setFittingErrorAttributeRecursive(AttributFloat *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            CT_PointAccessor pointAccessor;

            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point currentPointBaseLocale;

            Eigen::Matrix3d retourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

            float currentError;
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;
                currentPointBaseLocale = versBaseLocale * currentPointBaseCartesienneCentre;
                currentError = currentPointBaseLocale.z() -
                               ( _a * currentPointBaseLocale.x()*currentPointBaseLocale.x() +
                                 _b * currentPointBaseLocale.x()*currentPointBaseLocale.y() +
                                 _c * currentPointBaseLocale.y()*currentPointBaseLocale.y() +
                                 _d * currentPointBaseLocale.x() +
                                 _e * currentPointBaseLocale.y() +
                                 _f );
                attributs->setValueAt( _ptIndices->at(i), fabs( currentError ) );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setFittingErrorAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setFittingRmseAttributeRecursive(AttributFloat *attributs) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            CT_PointAccessor pointAccessor;

            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point currentPointBaseLocale;

            Eigen::Matrix3d retourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

            float currentError;
            float rmse = 0;
            float sumError = 0;
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;
                currentPointBaseLocale = versBaseLocale * currentPointBaseCartesienneCentre;
                currentError = currentPointBaseLocale.z() -
                               ( _a * currentPointBaseLocale.x()*currentPointBaseLocale.x() +
                                 _b * currentPointBaseLocale.x()*currentPointBaseLocale.y() +
                                 _c * currentPointBaseLocale.y()*currentPointBaseLocale.y() +
                                 _d * currentPointBaseLocale.x() +
                                 _e * currentPointBaseLocale.y() +
                                 _f );
                sumError += fabs( currentError );
            }

            rmse = sumError / nPts;

            for( int i = 0 ; i < nPts ; i++ )
            {
                attributs->setValueAt( _ptIndices->at(i), rmse );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setFittingRmseAttributeRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNode::setColorAttributeRecursive(CT_AbstractColorCloud *attributs, size_t currentID) const
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            int nPts = _ptIndices->size();
            for( int i = 0 ; i < nPts ; i++ )
            {
                (*attributs)[_ptIndices->at(i)] = _ctColorChild[currentID];
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setColorAttributeRecursive( attributs, i );
            }
        }
    }
}

void NORM_NewOctreeNode::computeNeighboursRecursive( bool onlyLeaves )
{
    if( onlyLeaves && isLeaf() )
    {
        if( isLeaf() )
        {
            // Calcule ses propres voisins
            computeNodeNeighbours();
        }
        else
        {
            // Et lance le calcul sur ses fils
            for( int i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    _children[i]->computeNeighboursRecursive( onlyLeaves );
                }
            }
        }
    }

    else
    {
        // Calcule ses propres voisins
        computeNodeNeighbours();

        // Et lance le calcul sur ses fils
        for( int i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->computeNeighboursRecursive( onlyLeaves );
            }
        }
    }

}

void NORM_NewOctreeNode::computeNodeNeighbours()
{
    // On prend une bbox elargie de la taille minimale d'une cellule
    // Et on regarde toutes les feuilles qui sont dans cette bbox
    CT_Point bboxBot;
    CT_Point bboxTop;
    getBBox( bboxBot, bboxTop );

    double dimMin = _octree->dimensionMin();
    for( int i = 0 ; i < 3 ; i++ )
    {
        bboxBot(i) -= dimMin;
        bboxTop(i) += dimMin;
    }

    _octree->root()->leavesInBBox( bboxBot, bboxTop, _neighbours, this );
}

void NORM_NewOctreeNode::computeAcpLeavesRecursive()
{
    if( _ptIndices != NULL )
    {
        updateAcp();
    }

    for( size_t i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            _children[i]->computeAcpLeavesRecursive();
        }
    }
}

void NORM_NewOctreeNode::computeNormalsRecursive(AttributeNormals *outNormals,
                                                 const CT_Point &scanPos,
                                                 bool towards )
{
    CT_AbstractNormalCloud* outNormalCloud = outNormals->getNormalCloud();

    if( isLeaf() )
    {
        if( !(this->_rmse <= _octree->rmseThresh() && _nbPoints >= _octree->nbPointsMin()) )
        {
            int nbPoints = _ptIndices->size();
            CT_Normal currentNormal(0,0,0,0);
            for( int i = 0 ; i < nbPoints ; i++ )
            {
                outNormalCloud->replaceNormal( _ptIndices->at(i), currentNormal );
            }
        }

        else
        {
            int nbPoints = _ptIndices->size();
            CT_PointAccessor pointAccessor;
            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point currentPointBaseLocale;
            CT_Point currentNormalBaseLocale;
            CT_Point currentNormalBaseCartesienne;
            CT_Normal currentNormal;

            Eigen::Matrix3d retourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

    //        // Nouveaute : on cherche le projette orthogonal du point sur la surface
    //        Eigen::Matrix3f A;
    //        A(0,0) = _a;         A(0,1) = _b/2.0;     A(0,2) = 0;
    //        A(1,0) = _b/2.0;     A(1,1) = _c;         A(1,2) = 0;
    //        A(2,0) = 0;          A(2,1) = 0;          A(2,2) = 0;
    //        Eigen::Vector3f B; B(0) = _d; B(1) = _e; B(2) = -1;
    //        Eigen::Vector4f p0;

            for( int i = 0 ; i < nbPoints ; i++ )
            {
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;
                currentPointBaseLocale = versBaseLocale * currentPointBaseCartesienneCentre;

    //            for(int j = 0 ; j < 3 ; j++ ) p0(j) = currentPointBaseLocale(j); p0(3) = 0;
    //            Eigen::Vector4f projette = NORM_Tools::NORM_LeastSquareFitting::newton( A, B, _f, p0, 1e-6, 50 );

    //            currentNormalBaseLocale(0) = (2*_a*projette.x()) + (_b*projette.y()) + _d;
    //            currentNormalBaseLocale(1) = (2*_c*projette.y()) + (_b*projette.x()) + _e;
    //            currentNormalBaseLocale(2) = -1;

                // Ancienne version sans projection orthogonale
                currentNormalBaseLocale(0) = (2*_a*currentPointBaseLocale.x()) + (_b*currentPointBaseLocale.y()) + _d;
                currentNormalBaseLocale(1) = (2*_c*currentPointBaseLocale.y()) + (_b*currentPointBaseLocale.x()) + _e;
                currentNormalBaseLocale(2) = -1;
                currentNormalBaseLocale.normalize();

                // On repasse la normale dans la base canonique cartesienne
                currentNormalBaseCartesienne = retourBaseCartesienne * currentNormalBaseLocale;

                for( int k = 0 ; k < 3 ; k++ )
                {
                    currentNormal(k) = currentNormalBaseCartesienne(k);
                }

    //            // Oriente les normales vers un point de la scene
    //            currentDirection = currentPointBaseCartesienne - scanPos;
    //            float prodScal = 0;
    //            for( int k = 0 ; k < 3 ; k++ )
    //            {
    //                prodScal += currentDirection(k)*currentNormal(k);
    //            }

    //            if( towards )
    //            {
    //                if( prodScal > 0 )
    //                {
    //                    for( int k = 0 ; k < 3 ; k++ )
    //                    {
    //                        currentNormal(k) *= -1;
    //                    }
    //                }
    //            }

    //            else
    //            {
    //                if( prodScal < 0 )
    //                {
    //                    for( int k = 0 ; k < 3 ; k++ )
    //                    {
    //                        currentNormal(k) *= -1;
    //                    }
    //                }
    //            }

                // Et on l'ajoute au tableau des normales
                outNormalCloud->replaceNormal( _ptIndices->at(i), currentNormal );
            }
        }
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->computeNormalsRecursive( outNormals, scanPos, towards );
            }
        }
    }
}

void NORM_NewOctreeNode::computeNormalsRecursiveLinearInterpolation(AttributeNormals* outNormals,
                                                                    const CT_Point& scanPos,
                                                                    bool towards,
                                                                    float radiusMax )
{
    CT_AbstractNormalCloud* outNormalCloud = outNormals->getNormalCloud();

    if( isLeaf() )
    {
        if( getSigmaPerCent() > _octree->sigmaThreshPerCent() )
        {
            CT_Normal currentNormal;
            currentNormal(0) = 0;
            currentNormal(1) = 0;
            currentNormal(2) = 0;
            int nbPoints = _ptIndices->size();
            for( int i = 0 ; i < nbPoints ; i++ )
            {
                outNormalCloud->normalAt( _ptIndices->at(i) ) = currentNormal;
            }
        }

        else
        {
            int nbPoints = _ptIndices->size();
            CT_PointAccessor pointAccessor;
            CT_Point currentDirection;
            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point currentPointBaseLocale;
            CT_Point currentNormalBaseLocale;
            CT_Point currentNormalBaseCartesienne;
            CT_Normal currentNormal;

            Eigen::Matrix3d retourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

            float radiusMaxSquare = radiusMax * radiusMax;
            float a = -1.0 / radiusMax;
            float b = 1;

            // Pour chaque point de la cellule
            for( int i = 0 ; i < nbPoints ; i++ )
            {
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;
                currentPointBaseLocale = versBaseLocale * currentPointBaseCartesienneCentre;

                // On regarde chaque cellule voisine
                int nNei = _neighbours.size();
                NORM_NewOctreeNode* curNei;
                float curSquareDist;
                float curWeight = a * sqrt( ( currentPointBaseCartesienne.x() - _centroid.x() ) * ( currentPointBaseCartesienne.x() - _centroid.x() ) +
                                            ( currentPointBaseCartesienne.y() - _centroid.y() ) * ( currentPointBaseCartesienne.y() - _centroid.y() ) +
                                            ( currentPointBaseCartesienne.z() - _centroid.z() ) * ( currentPointBaseCartesienne.z() - _centroid.z() ) ) + b;
                float sumWeight = curWeight;
                float cura = _a * curWeight;        // On prend en compte le noeud courant des le debut
                float curb = _b * curWeight;
                float curc = _c * curWeight;
                float curd = _d * curWeight;
                float cure = _e * curWeight;
                float curf = _f * curWeight;

    //            qDebug() << "FIRST " << cura << "poids " << curWeight << sumWeight << "original nei" << _a;

                for( int j = 0 ; j < nNei ; j++ )
                {
                    curNei = _neighbours.at(j);

                    // On calcule la distance entre le point courant et le barycentre de la cellule voisine
                    curSquareDist = ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) * ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) +
                                    ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) * ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) +
                                    ( currentPointBaseCartesienne.z() - curNei->_centroid.z() ) * ( currentPointBaseCartesienne.z() - curNei->_centroid.z() );

                    // Si le point est dans la zone d'influence du voisin courant alors il faut en tenir compte dans la moyenne ponderee
                    if( curSquareDist < radiusMaxSquare && curNei->getSigmaPerCent() < _octree->sigmaThreshPerCent() )
                    {
                        // Calcule le poids correspondant en fonction de la distanceau barycentre
                        // Interpolation lineaire ici a remplacer par une wendland sous peu
                        curWeight = a *( sqrt(curSquareDist) ) + b;

                        sumWeight += curWeight;
                        cura += curNei->_a * curWeight;
                        curb += curNei->_b * curWeight;
                        curc += curNei->_c * curWeight;
                        curd += curNei->_d * curWeight;
                        cure += curNei->_e * curWeight;
                        curf += curNei->_f * curWeight;

    //                    qDebug() << curNei->_a <<"Somme " << cura << "poids " << curWeight << sumWeight << "original nei" << _a << curNei->_a;
                    }
                }

                // On normalise les coefficients
                cura /= sumWeight;
                curb /= sumWeight;
                curc /= sumWeight;
                curd /= sumWeight;
                cure /= sumWeight;
                curf /= sumWeight;

    //            qDebug() << "De " << _a << _b << _c << _d << _e << _f << " on passe a " << cura << curb << curc << curd << cure << curf;

                // /////////////////////////////////////////////////////////////////////////////////////
                // Et on fait la projection sur la surface que l'on vient d'obtenir par moyenne ponderee
                // Ancienne version sans projection orthogonale
                currentNormalBaseLocale(0) = (2*cura*currentPointBaseLocale.x()) + (curb*currentPointBaseLocale.y()) + curd;
                currentNormalBaseLocale(1) = (2*curc*currentPointBaseLocale.y()) + (curb*currentPointBaseLocale.x()) + cure;
                currentNormalBaseLocale(2) = -1;
                currentNormalBaseLocale.normalize();

                // On repasse la normale dans la base canonique cartesienne
                currentNormalBaseCartesienne = retourBaseCartesienne * currentNormalBaseLocale;

                for( int k = 0 ; k < 3 ; k++ )
                {
                    currentNormal(k) = currentNormalBaseCartesienne(k);
                }

                // Oriente les normales vers un point de la scene
                currentDirection = currentPointBaseCartesienne - scanPos;
                float prodScal = 0;
                for( int k = 0 ; k < 3 ; k++ )
                {
                    prodScal += currentDirection(k)*currentNormal(k);
                }

                if( towards )
                {
                    if( prodScal > 0 )
                    {
                        for( int k = 0 ; k < 3 ; k++ )
                        {
                            currentNormal(k) *= -1;
                        }
                    }
                }

                else
                {
                    if( prodScal < 0 )
                    {
                        for( int k = 0 ; k < 3 ; k++ )
                        {
                            currentNormal(k) *= -1;
                        }
                    }
                }

                // Et on l'ajoute au tableau des normales
                outNormalCloud->normalAt( _ptIndices->at(i) ) = currentNormal;
                // /////////////////////////////////////////////////////////////////////////////////////
            }
        }
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->computeNormalsRecursiveLinearInterpolation( outNormals, scanPos, towards, radiusMax );
            }
        }
    }
}

void NORM_NewOctreeNode::computeNormalsRecursiveCRBFInterpolation(AttributeNormals *outNormals,
                                                                  const CT_Point &scanPos,
                                                                  bool towards,
                                                                  float radiusMax)
{
    CT_AbstractNormalCloud* outNormalCloud = outNormals->getNormalCloud();
    float radiusMaxSquare = radiusMax * radiusMax;

    if( isLeaf() )
    {
//        if( getSigmaPerCent() > _octree->sigmaThreshPerCent() )
//        {
//            CT_Normal currentNormal;
//            currentNormal(0) = 0;
//            currentNormal(1) = 0;
//            currentNormal(2) = 0;
//            int nbPoints = _ptIndices->size();
//            for( int i = 0 ; i < nbPoints ; i++ )
//            {
//                outNormalCloud->normalAt( _ptIndices->at(i) ) = currentNormal;
//            }
//        }

//        else
//        {
            // On calcule la matrice pour passer a la base cartesienne (je calcule toujours l'inverse...)
            Eigen::Matrix3d retourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d retourBaseLocale;
            retourBaseLocale = retourBaseCartesienne.inverse();

            // On calcule la matrice de la surface quadratique dans la base cartesienne
            Eigen::Matrix3d aBaseLocale;
            Eigen::Matrix3d aBaseCartesienne;
            aBaseLocale << -_a, -0.5*_b, 0,
                           -0.5*_b, -_c, 0,
                           0, 0, 0;
            aBaseCartesienne = retourBaseLocale.transpose() * aBaseLocale * retourBaseLocale;

            Eigen::Vector3d bBaseLocale;
            Eigen::Vector3d bBaseCartesienne;
            bBaseLocale << -_d, -_e, 1;
            bBaseCartesienne = retourBaseLocale.transpose() * bBaseLocale;


            // Pour chaque point de la cellule on calcule sa normale
            int nbPoints = _ptIndices->size();
            CT_PointAccessor pointAccessor;
            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point neiPointBaseCartesienneCentre;
            CT_Normal currentNormal;
            for( int i = 0 ; i < nbPoints ; i++ )
            {
                // Declarations pour la normale finale du point comme une somme ponderee
                double sumWeight = 1;
                Eigen::Vector3d sumWeightedNormals;
                sumWeightedNormals.setZero();

                // On recupere le point courrant
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;

                // On calcule sa normale dans la base cartesienne
                Eigen::Vector3d curNormaleBaseCartesienne;
                curNormaleBaseCartesienne = 2 * aBaseCartesienne * currentPointBaseCartesienneCentre + bBaseCartesienne;

                //On la normalise
                curNormaleBaseCartesienne.normalize();

                // On l'ajoute a la somme ponderee
                float curWeight = 1;
                sumWeightedNormals += curWeight * curNormaleBaseCartesienne;

                // ///////////////////////////////////////////////////////////////////////////////////////////////////// //
                // Interpolation avec les cellules voisines et brdf                                     //////////////// //
                // ///////////////////////////////////////////////////////////////////////////////////////////////////// //
                // On regarde chaque cellule voisine
                NORM_NewOctreeNode* curNei;
                int nNei = _neighbours.size();
                float curSquareDist;
                for( int j = 0 ; j < nNei ; j++ )
                {
                    // On accede au voisin et au point centre sur le barycentre du voisin
                    curNei = _neighbours.at(j);
                    neiPointBaseCartesienneCentre = currentPointBaseCartesienne - curNei->_centroid;

                    // On calcule la distance entre le point courant et le barycentre de la cellule voisine
                    curSquareDist = ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) * ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) +
                                    ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) * ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) +
                                    ( currentPointBaseCartesienne.z() - curNei->_centroid.z() ) * ( currentPointBaseCartesienne.z() - curNei->_centroid.z() );

                    // Si le point est dans la zone d'influence du voisin courant alors il faut en tenir compte dans la moyenne ponderee
                    if( curSquareDist < radiusMaxSquare /*&& curNei->getSigmaPerCent() < _octree->sigmaThreshPerCent()*/ )
                    {
                        // Calcule le poids correspondant en fonction de la distanceau barycentre par wendland
                        curWeight = NORM_Tools::NORM_Interpolation::wendlandCoeff( radiusMax, sqrt( curSquareDist ) );
                        sumWeight += curWeight;

                        // On calcule la matrice du noeud voisin pour passer a la base cartesienne
                        Eigen::Matrix3d neiRetourBaseCartesienne;
                        neiRetourBaseCartesienne << curNei->_v1(0) , curNei->_v2(0) , curNei->_v3(0),
                                                    curNei->_v1(1) , curNei->_v2(1) , curNei->_v3(1),
                                                    curNei->_v1(2) , curNei->_v2(2) , curNei->_v3(2);
                        Eigen::Matrix3d neiRetourBaseLocale;
                        neiRetourBaseLocale = neiRetourBaseCartesienne.inverse();

                        // On calcule la matrice de la surface quadratique du noeud voisin dans la base cartesienne
                        Eigen::Matrix3d neiABaseLocale;
                        Eigen::Matrix3d neiABaseCartesienne;
                        neiABaseLocale << -curNei->_a, -0.5*curNei->_b, 0,
                                          -0.5*curNei->_b, -curNei->_c, 0,
                                          0, 0, 0;
                        neiABaseCartesienne = neiRetourBaseLocale.transpose() * neiABaseLocale * neiRetourBaseLocale;

                        Eigen::Vector3d neiBBaseLocale;
                        Eigen::Vector3d neiBBaseCartesienne;
                        neiBBaseLocale << -curNei->_d, -curNei->_e, 1;
                        neiBBaseCartesienne = neiRetourBaseLocale.transpose() * neiBBaseLocale;

                        // On calcule la normale du point courant dans le noeud voisin
                        Eigen::Vector3d neiNormaleBaseCartesienne;
                        neiNormaleBaseCartesienne = 2 * neiABaseCartesienne * neiPointBaseCartesienneCentre + neiBBaseCartesienne;

                        //On la normalise
                        neiNormaleBaseCartesienne.normalize();

                        // Et on l'ajoute a la somme ponderee
                        sumWeightedNormals += curWeight * neiNormaleBaseCartesienne;
                    }
                }

                // On normalise la somme et
                // On convertit la normale sous la forme d'eigen vers la forme computree
                for( int k = 0 ; k < 3 ; k++ )
                {
                    currentNormal(k) = curNormaleBaseCartesienne(k) / sumWeight;
                }

                // Et on l'ajoute au tableau des normales
                outNormalCloud->normalAt( _ptIndices->at(i) ) = currentNormal;
            }
//        }
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->computeNormalsRecursiveCRBFInterpolation( outNormals, scanPos, towards, radiusMax );
            }
        }
    }
}

void NORM_NewOctreeNode::computeNormalsRecursiveCRBFInterpolationGlobal(AttributeNormals *outNormals, const CT_Point &scanPos, bool towards, float radiusMax)
{
    CT_AbstractNormalCloud* outNormalCloud = outNormals->getNormalCloud();

    if( isLeaf() )
    {
//        if( getSigmaPerCent() > _octree->sigmaThreshPerCent() )
//        {
//            CT_Normal currentNormal;
//            currentNormal(0) = 0;
//            currentNormal(1) = 0;
//            currentNormal(2) = 0;
//            int nbPoints = _ptIndices->size();
//            for( int i = 0 ; i < nbPoints ; i++ )
//            {
//                outNormals->setNormalAt( _ptIndices->at(i), currentNormal );
//            }
//        }

//        else
//        {

            int nbPoints = _ptIndices->size();
            CT_PointAccessor pointAccessor;
            CT_Point currentDirection;
            CT_Point currentPointBaseCartesienne;
            CT_Point currentPointBaseCartesienneCentre;
            CT_Point currentPointBaseLocale;
            CT_Point currentNormalBaseLocale;
            CT_Point currentNormalBaseCartesienne;
            CT_Normal currentNormal;


            Eigen::Matrix3d retourBaseCartesienne;
            Eigen::Matrix3d neiVersBaseLocale;
            Eigen::Matrix3d neiRetourBaseCartesienne;
            retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                                     _v1(1) , _v2(1) , _v3(1),
                                     _v1(2) , _v2(2) , _v3(2);
            Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

            Eigen::Matrix3d curSurfaceALocal;
            Eigen::Matrix3d neiSurfaceALocal;
            curSurfaceALocal << _a , 0.5*_b , 0, 0.5*_b , _c , 0, 0, 0, 0;

            Eigen::Vector3d curSurfaceBLocal;
            Eigen::Vector3d neiSurfaceBLocal;
            curSurfaceBLocal << _c , _d, 0;

            Eigen::Matrix3d curSurfaceAGlobal;
            Eigen::Matrix3d neiSurfaceAGlobal;
            curSurfaceAGlobal = versBaseLocale.transpose() * curSurfaceALocal * versBaseLocale;

            Eigen::Vector3d curSurfaceBGlobal;
            Eigen::Vector3d neiSurfaceBGlobal;
            curSurfaceBGlobal = versBaseLocale.transpose() * curSurfaceBLocal;

            float radiusMaxSquare = radiusMax * radiusMax;

            // Pour chaque point de la cellule
            for( int i = 0 ; i < nbPoints ; i++ )
            {
                pointAccessor.pointAt( _ptIndices->at(i), currentPointBaseCartesienne );
                currentPointBaseCartesienneCentre = currentPointBaseCartesienne - _centroid;
                currentPointBaseLocale = versBaseLocale * currentPointBaseCartesienneCentre;

                // On regarde chaque cellule voisine
                int nNei = _neighbours.size();
                NORM_NewOctreeNode* curNei;
                float curSquareDist;
                float curWeight = 1;

                float sumWeight = curWeight;
                float cura = curSurfaceAGlobal(0,0) * curWeight;        // On prend en compte le noeud courant des le debut
                float curb = 2*curSurfaceAGlobal(0,1) * curWeight;
                float curc = curSurfaceAGlobal(1,1) * curWeight;
                float curd = curSurfaceBGlobal(0) * curWeight;
                float cure = curSurfaceBGlobal(1) * curWeight;
                float curf = _f * curWeight;

                for( int j = 0 ; j < nNei ; j++ )
                {
                    curNei = _neighbours.at(j);
                    neiSurfaceALocal << curNei->_a , 0.5*curNei->_b , 0, 0.5*curNei->_b , curNei->_c , 0, 0, 0, 0;
                    neiSurfaceBLocal << curNei->_c , curNei->_d, 0;
                    neiRetourBaseCartesienne << curNei->_v1(0) , curNei->_v2(0) , curNei->_v3(0),
                                                curNei->_v1(1) , curNei->_v2(1) , curNei->_v3(1),
                                                curNei->_v1(2) , curNei->_v2(2) , curNei->_v3(2);
                    neiVersBaseLocale = neiRetourBaseCartesienne.inverse();
                    neiSurfaceAGlobal = neiVersBaseLocale.transpose() * neiSurfaceALocal * neiVersBaseLocale;
                    neiSurfaceBGlobal = neiVersBaseLocale.transpose() * neiSurfaceBLocal;

                    // On calcule la distance entre le point courant et le barycentre de la cellule voisine
                    curSquareDist = ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) * ( currentPointBaseCartesienne.x() - curNei->_centroid.x() ) +
                                    ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) * ( currentPointBaseCartesienne.y() - curNei->_centroid.y() ) +
                                    ( currentPointBaseCartesienne.z() - curNei->_centroid.z() ) * ( currentPointBaseCartesienne.z() - curNei->_centroid.z() );

                    // Si le point est dans la zone d'influence du voisin courant alors il faut en tenir compte dans la moyenne ponderee
                    if( curSquareDist < radiusMaxSquare /*&& curNei->getSigmaPerCent() < _octree->sigmaThreshPerCent()*/ )
                    {
                        // Calcule le poids correspondant en fonction de la distanceau barycentre
                        // Interpolation lineaire ici a remplacer par une wendland sous peu
                        curWeight = NORM_Tools::NORM_Interpolation::wendlandCoeff( radiusMax, sqrt( curSquareDist ) );

                        sumWeight += curWeight;
                        cura += neiSurfaceAGlobal(0,0) * curWeight;
                        curb += 2*neiSurfaceAGlobal(0,1) * curWeight;
                        curc += neiSurfaceAGlobal(1,1) * curWeight;
                        curd += neiSurfaceBGlobal(0) * curWeight;
                        cure += neiSurfaceBGlobal(1) * curWeight;
                        curf += curNei->_f * curWeight;

    //                    qDebug() << curNei->_a <<"Somme " << cura << "poids " << curWeight << sumWeight << "original nei" << _a << curNei->_a;
                    }
                }

                // On normalise les coefficients
                cura /= sumWeight;
                curb /= sumWeight;
                curc /= sumWeight;
                curd /= sumWeight;
                cure /= sumWeight;
                curf /= sumWeight;

    //            qDebug() << "De " << _a << _b << _c << _d << _e << _f << " on passe a " << cura << curb << curc << curd << cure << curf;

                // /////////////////////////////////////////////////////////////////////////////////////
                // Et on fait la projection sur la surface que l'on vient d'obtenir par moyenne ponderee
                // Ancienne version sans projection orthogonale
                currentNormalBaseLocale(0) = (2*cura*currentPointBaseLocale.x()) + (curb*currentPointBaseLocale.y()) + curd;
                currentNormalBaseLocale(1) = (2*curc*currentPointBaseLocale.y()) + (curb*currentPointBaseLocale.x()) + cure;
                currentNormalBaseLocale(2) = -1;
                currentNormalBaseLocale.normalize();

                // On repasse la normale dans la base canonique cartesienne
                currentNormalBaseCartesienne = retourBaseCartesienne * currentNormalBaseLocale;

                for( int k = 0 ; k < 3 ; k++ )
                {
                    currentNormal(k) = currentNormalBaseCartesienne(k);
                }

                // Oriente les normales vers un point de la scene
                currentDirection = currentPointBaseCartesienne - scanPos;
                float prodScal = 0;
                for( int k = 0 ; k < 3 ; k++ )
                {
                    prodScal += currentDirection(k)*currentNormal(k);
                }

                if( towards )
                {
                    if( prodScal > 0 )
                    {
                        for( int k = 0 ; k < 3 ; k++ )
                        {
                            currentNormal(k) *= -1;
                        }
                    }
                }

                else
                {
                    if( prodScal < 0 )
                    {
                        for( int k = 0 ; k < 3 ; k++ )
                        {
                            currentNormal(k) *= -1;
                        }
                    }
                }

                // Et on l'ajoute au tableau des normales
                outNormalCloud->normalAt( _ptIndices->at(i) ) = currentNormal;
                // /////////////////////////////////////////////////////////////////////////////////////
            }
//        }
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->computeNormalsRecursiveCRBFInterpolation( outNormals, scanPos, towards, radiusMax );
            }
        }
    }
}

NORM_NewOctreeNode *NORM_NewOctreeNode::getLeafContainingPointRecursive(const CT_Point &p)
{
    if( isLeaf() )
    {
        return this;
    }

    else
    {
        // Calcul de l'octant contenant le point
        int octant = getOctantContainingPoint( p );

        if( _children[octant] != NULL )
        {
            return _children[octant]->getLeafContainingPointRecursive( p );
        }

        else
        {
            return NULL;
        }
    }
}

void NORM_NewOctreeNode::leavesInBBox(const CT_Point &bot, const CT_Point &top, QVector<NORM_NewOctreeNode *> &outputNodes, NORM_NewOctreeNode* callingNode )
{
    if( this == callingNode )
    {
        return;
    }

    // Si le noeud intersecte la bbox
    if( intersect( bot, top ) )
    {
        if( isLeaf() )
        {
            // Si c'est unefeuille on l'ajoute au tableau
            outputNodes.push_back( this );
            return;
        }

        else
        {
            // Sinon on continue la recursion jusqu'aux feuilles
            for( int i = 0 ; i < 8 ; i++ )
            {
                if( _children[i] != NULL )
                {
                    _children[i]->leavesInBBox( bot, top, outputNodes, callingNode );
                }
            }
        }
    }

    else
    {
        // Sinon ne rien faire
        return;
    }
}

bool NORM_NewOctreeNode::intersect(const CT_Point &bot, const CT_Point &top)
{
    CT_Point nodeBot;
    CT_Point nodeTop;

    getBBox( nodeBot, nodeTop );

    for( int i = 0 ; i < 3 ; i++ )
    {
        if( nodeBot(i) > top(i) )
        {
            return false;
        }

        if( bot(i) > nodeTop(i) )
        {
            return false;
        }
    }

    return true;
}

bool NORM_NewOctreeNode::intersectNoContactPoint(const CT_Point &bot, const CT_Point &top)
{
    CT_Point nodeBot;
    CT_Point nodeTop;

    getBBox( nodeBot, nodeTop );

    for( int i = 0 ; i < 3 ; i++ )
    {
        if( nodeBot(i) >= top(i) )
        {
            return false;
        }

        if( bot(i) >= nodeTop(i) )
        {
            return false;
        }
    }

    return true;
}

NodeBoxStatus NORM_NewOctreeNode::isInBox(const CT_Point &bot, const CT_Point &top)
{
    CT_Point nodeBot;
    CT_Point nodeTop;

    getBBox( nodeBot, nodeTop );

    for( int i = 0 ; i < 3 ; i++ )
    {
        if( nodeBot(i) > top(i) )
        {
            return OUTSIDE_BOX;
        }

        if( bot(i) > nodeTop(i) )
        {
            return OUTSIDE_BOX;
        }
    }

    // On intersecte, le noeud est il completement dedans ?
    for( int i = 0 ; i < 3 ; i++ )
    {
        if( nodeTop(i) > top(i) )
        {
            return INTERSECT_BOX;
        }

        if( bot(i) > nodeBot(i) )
        {
            return INTERSECT_BOX;
        }
    }

    return INSIDE_BOX;
}

const QVector<NORM_NewOctreeNode *> &NORM_NewOctreeNode::neighbours() const
{
    return _neighbours;
}

void NORM_NewOctreeNode::setNeighbours(const QVector<NORM_NewOctreeNode *> &neighbours)
{
    _neighbours = neighbours;
}

void NORM_NewOctreeNode::getBBox(CT_Point &outBot, CT_Point &outTop) const
{
    for( int i = 0 ; i < 3 ; i++ )
    {
        outBot(i) = _center(i) - _dimension;
        outTop(i) = _center(i) + _dimension;
    }
}

void NORM_NewOctreeNode::getCorner(int cornerID, CT_Point &outCorner) const
{
    CT_Point bot, top;
    getBBox( bot, top );

    switch( cornerID )
    {
        case 1 :
        {
            outCorner.x() = bot.x();
            outCorner.y() = bot.y();
            outCorner.z() = bot.z();
            break;
        }

        case 2 :
        {
            outCorner.x() = bot.x();
            outCorner.y() = top.y();
            outCorner.z() = bot.z();
            break;
        }

        case 3 :
        {
            outCorner.x() = top.x();
            outCorner.y() = top.y();
            outCorner.z() = bot.z();
            break;
        }

        case 4 :
        {
            outCorner.x() = top.x();
            outCorner.y() = bot.y();
            outCorner.z() = bot.z();
            break;
        }

        case 5 :
        {
            outCorner.x() = bot.x();
            outCorner.y() = bot.y();
            outCorner.z() = top.z();
            break;
        }

        case 6 :
        {
            outCorner.x() = bot.x();
            outCorner.y() = top.y();
            outCorner.z() = top.z();
            break;
        }

        case 7 :
        {
            outCorner.x() = top.x();
            outCorner.y() = top.y();
            outCorner.z() = top.z();
            break;
        }

        case 8 :
        {
            outCorner.x() = top.x();
            outCorner.y() = bot.y();
            outCorner.z() = top.z();
            break;
        }

        default :
            break;
    }
}

void NORM_NewOctreeNode::getCorners(CT_Point &out1,
                                    CT_Point &out2,
                                    CT_Point &out3,
                                    CT_Point &out4,
                                    CT_Point &out5,
                                    CT_Point &out6,
                                    CT_Point &out7,
                                    CT_Point &out8) const
{
    CT_Point bot, top;
    getBBox( bot, top );

    out1.x() = bot.x();
    out1.y() = bot.y();
    out1.z() = bot.z();

    out2.x() = bot.x();
    out2.y() = top.y();
    out2.z() = bot.z();

    out3.x() = top.x();
    out3.y() = top.y();
    out3.z() = bot.z();

    out4.x() = top.x();
    out4.y() = bot.y();
    out4.z() = bot.z();

    out5.x() = bot.x();
    out5.y() = bot.y();
    out5.z() = top.z();

    out6.x() = bot.x();
    out6.y() = top.y();
    out6.z() = top.z();

    out7.x() = top.x();
    out7.y() = top.y();
    out7.z() = top.z();

    out8.x() = top.x();
    out8.y() = bot.y();
    out8.z() = top.z();
}

CT_PointCloudIndexVector *NORM_NewOctreeNode::getIndexCloud() const
{
    CT_PointCloudIndexVector* rslt = new CT_PointCloudIndexVector();
    rslt->setSortType( CT_AbstractCloudIndex::NotSorted );

    size_t nPoints = _ptIndices->size();
    for( size_t i = 0 ; i < nPoints ; i++ )
    {
        rslt->addIndex( _ptIndices->at(i) );
    }

    return rslt;
}

void NORM_NewOctreeNode::getAllPointsFromLeaves()
{
    _ptIndices = new QVector<int> ();
    updateIndicesFromChildren( _ptIndices );
}

void NORM_NewOctreeNode::updateAcp()
{
    NORM_Tools::NORM_PCA::pca( _ptIndices,
                               _centroid,
                               _l1, _l2, _l3,
                               _v1, _v2, _v3 );
}

void NORM_NewOctreeNode::updateAcpAndQuadraticSurface()
{
    updateAcp();

    QVector<CT_Point>* pointsInLocalBasis = getPointCloudInAcpBasis();
    NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( pointsInLocalBasis,
                                                                  _a, _b, _c, _d, _e, _f );
    delete pointsInLocalBasis;
}

float NORM_NewOctreeNode::updateAcpAndQuadraticSurfaceAndGetRmse()
{
    updateAcp();

    QVector<CT_Point>* pointsInLocalBasis = getPointCloudInAcpBasis();
    NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( pointsInLocalBasis,
                                                                  _a, _b, _c, _d, _e, _f );

    float sumError = 0;
    CT_Point curPointLocalBasis;
    for( int i = 0 ; i < _nbPoints ; i++ )
    {
        curPointLocalBasis = pointsInLocalBasis->at(i);

        sumError += fabs( curPointLocalBasis.z() - (_a*curPointLocalBasis.x()*curPointLocalBasis.x() +
                                                    _b*curPointLocalBasis.x()*curPointLocalBasis.y() +
                                                    _c*curPointLocalBasis.y()*curPointLocalBasis.y() +
                                                    _d*curPointLocalBasis.x() +
                                                    _e*curPointLocalBasis.y() +
                                                    _f) );
    }
    float rmse = sumError / (float)_nbPoints;

    delete pointsInLocalBasis;

    return rmse;
}

CT_Scene *NORM_NewOctreeNode::getSegment(double angleMax,
                                         QString modelName,
                                         CT_AbstractResult *result)
{
    CT_PointCloudIndexVector* outIndexCloud = new CT_PointCloudIndexVector();
    QQueue<NORM_NewOctreeNode*> queue;
    queue.append( this );
    setVisited( true );

    NORM_NewOctreeNode* currentNode;
    NORM_NewOctreeNode* currentNei;
    CT_Point currentV1;
    CT_Point currentNeiV1;
    double currentAngle;
    while( !queue.empty() )
    {
        currentNode = queue.dequeue();
        currentV1 = currentNode->_v3;

        /* Faire l'ajout des points a la scene */
        QVector< int >* currentNodeIndices = currentNode->_ptIndices;
        int nbPoints = currentNodeIndices->size();
        for( int i = 0 ; i < nbPoints ; i++ )
        {
            outIndexCloud->addIndex( currentNodeIndices->at(i) );
        }

        // Continue le parcours en largeur
        const QVector<NORM_NewOctreeNode*>& currentNeighbours = currentNode->neighbours();
        int nNei = currentNeighbours.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            currentNei = currentNeighbours.at(i);
            if( !currentNei->_visited )
            {
                currentNeiV1 = currentNei->_v3;
                double scalarProd = currentV1.x()*currentNeiV1.x() +
                                    currentV1.y()*currentNeiV1.y() +
                                    currentV1.z()*currentNeiV1.z();
                if( scalarProd < 0 )
                {
                    for( int k = 0 ; k < 3 ; k++ )
                    {
                        currentNeiV1(k) *= -1;
                    }
                }

                // MAJ produit scalaire
                scalarProd = currentV1.x()*currentNeiV1.x() +
                             currentV1.y()*currentNeiV1.y() +
                             currentV1.z()*currentNeiV1.z();

                if( scalarProd >= 1 )
                {
                    currentAngle = 0;
                }

                else
                {
                    currentAngle = acos(scalarProd);// Pas de division par les normes car les vecteurs propres sont deja normes lors de l'acp
                    currentAngle *= 180.0 / M_PI;
                }

                if( currentAngle < angleMax )
                {
                    queue.append( currentNei );
                    currentNei->setVisited( true );
                }
            }
        }
    }

    // Creation de la scene
    CT_PCIR outIndexCloudRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexCloud );
    CT_Scene* item_outItemScene = new CT_Scene( modelName, result );
    item_outItemScene->setPointCloudIndexRegistered( outIndexCloudRegistered );

    return item_outItemScene;
}

CT_Scene *NORM_NewOctreeNode::getSegmentFromNormals(double angleMax, QString modelName, CT_AbstractResult *result)
{
    CT_PointCloudIndexVector* outIndexCloud = new CT_PointCloudIndexVector();
    QQueue<NORM_NewOctreeNode*> queue;
    queue.append( this );
    setVisited( true );

    NORM_NewOctreeNode* currentNode;
    NORM_NewOctreeNode* currentNei;
    CT_Point currentPointOnTouchingFace;
    CT_Point curNodeNormal;
    CT_Point neiNodeNormal;
    double currentAngle;
    while( !queue.empty() )
    {
        currentNode = queue.dequeue();

        /* Faire l'ajout des points a la scene */
        QVector< int >* currentNodeIndices = currentNode->_ptIndices;
        int nbPoints = currentNodeIndices->size();
        for( int i = 0 ; i < nbPoints ; i++ )
        {
            outIndexCloud->addIndex( currentNodeIndices->at(i) );
        }

        // Continue le parcours en largeur
        const QVector<NORM_NewOctreeNode*>& currentNeighbours = currentNode->neighbours();
        int nNei = currentNeighbours.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            currentNei = currentNeighbours.at(i);

            // Si le voisin n'a pas deja ete traite
            if( !currentNei->_visited )
            {
                // ON NE TIENT COMPTE DES VOISINS UNIQUEMENT SI LES NOEUDS SE TOUCHENT PAR FACE
                CubeNeibourhoogType neiType = currentNode->getCubeNeibourhoogType( currentNei );
                if( neiType != VERTEX_NEIGHBOURHOOD && neiType != EDGE_NEIGHBOURHOOD )
                {
                    // On calcule le point central de la face de contact entre les deux voisins
                    if( currentNode->_dimension < currentNei->_dimension )
                    {
                        currentPointOnTouchingFace = currentNode->getFaceCenter( neiType );
                    }

                    else
                    {
                        currentPointOnTouchingFace = currentNei->getFaceCenter( getOppositeFaceNeibourhoogType( neiType ) );
                    }

                    // On calcule la normale de ce point central pour les deux noeuds
                    curNodeNormal = currentNode->getPointNormalFromCartesianSpace( currentPointOnTouchingFace );
                    neiNodeNormal = currentNei->getPointNormalFromCartesianSpace( currentPointOnTouchingFace );

                    // Calcul de l'angle entre les deux normales
                    double scalarProd = curNodeNormal.x()*neiNodeNormal.x() +
                                        curNodeNormal.y()*neiNodeNormal.y() +
                                        curNodeNormal.z()*neiNodeNormal.z();
                    if( scalarProd < 0 )
                    {
                        for( int k = 0 ; k < 3 ; k++ )
                        {
                            neiNodeNormal(k) *= -1;
                        }
                    }

                    // MAJ produit scalaire
                    scalarProd = curNodeNormal.x()*neiNodeNormal.x() +
                                 curNodeNormal.y()*neiNodeNormal.y() +
                                 curNodeNormal.z()*neiNodeNormal.z();

                    if( scalarProd >= 1 )
                    {
                        currentAngle = 0;
                    }

                    else
                    {
                        currentAngle = acos(scalarProd);// Pas de division par les normes car les vecteurs sont deja normes
                        currentAngle *= 180.0 / M_PI;
                    }

                    if( currentAngle < angleMax )
                    {
                        queue.append( currentNei );
                        currentNei->setVisited( true );
                    }
                }
            }
        }
    }

    // Creation de la scene
    CT_PCIR outIndexCloudRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexCloud );
    CT_Scene* item_outItemScene = new CT_Scene( modelName, result );
    item_outItemScene->setPointCloudIndexRegistered( outIndexCloudRegistered );

    return item_outItemScene;
}

void NORM_NewOctreeNode::segmentFromPcaDirectionsRecursive(double angleMax,
                                                           QString modelName,
                                                           CT_AbstractResult *result,
                                                           QVector<CT_Scene*>* segments )
{
    if( isLeaf() && _nbPoints >= _octree->nbPointsMin() )
    {
        if( !_visited )
        {
            segments->push_back( getSegment(angleMax,
                                            modelName,
                                            result ) );
        }
    }

    else
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->segmentFromPcaDirectionsRecursive( angleMax,
                                                                 modelName,
                                                                 result,
                                                                 segments );
            }
        }
    }
}

void NORM_NewOctreeNode::segmentFromNormalDirectionsRecursive(double angleMax, QString modelName, CT_AbstractResult *result, QVector<CT_Scene *> *segments)
{
    if( isLeaf() && _nbPoints >= _octree->nbPointsMin() )
    {
        if( !_visited )
        {
            segments->push_back( getSegmentFromNormals(angleMax,
                                                       modelName,
                                                       result ) );
        }
    }

    else
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->segmentFromNormalDirectionsRecursive( angleMax,
                                                                    modelName,
                                                                    result,
                                                                    segments );
            }
        }
    }
}

QVector<CT_Point>* NORM_NewOctreeNode::getPointCloudInAcpBasis()
{
    QVector<CT_Point>* rslt= new QVector<CT_Point>();

    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

    CT_PointAccessor pointAccessor;
    CT_Point currentPointInCartesianBasis;
    CT_Point currentPointInCartesianBasisCentered;
    CT_Point currentPointInLocalBasis;
    int nbPoints = _ptIndices->size();
    for( int i = 0 ; i < nbPoints ; i++ )
    {
        pointAccessor.pointAt( _ptIndices->at(i), currentPointInCartesianBasis );
        currentPointInCartesianBasisCentered = currentPointInCartesianBasis - _centroid;
        currentPointInLocalBasis = versBaseLocale * currentPointInCartesianBasisCentered;

        rslt->push_back( currentPointInLocalBasis );
    }

    return rslt;
}

CT_Point NORM_NewOctreeNode::getPointInAcpBasis(CT_Point &p)
{
    CT_Point pCentered = p - _centroid;

    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

    return ( versBaseLocale * pCentered );
}

CT_Point NORM_NewOctreeNode::getPointNormalFromCartesianSpace(CT_Point &p)
{
    CT_Point pCentered = p - _centroid;

    // On calcule la matrice pour passer a la base cartesienne (je calcule toujours l'inverse...)
    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d retourBaseLocale;
    retourBaseLocale = retourBaseCartesienne.inverse();

    // On calcule la matrice de la surface quadratique dans la base cartesienne
    Eigen::Matrix3d aBaseLocale;
    Eigen::Matrix3d aBaseCartesienne;
    aBaseLocale << -_a, -0.5*_b, 0,
                   -0.5*_b, -_c, 0,
                   0, 0, 0;
    aBaseCartesienne = retourBaseLocale.transpose() * aBaseLocale * retourBaseLocale;

    Eigen::Vector3d bBaseLocale;
    Eigen::Vector3d bBaseCartesienne;
    bBaseLocale << -_d, -_e, 1;
    bBaseCartesienne = retourBaseLocale.transpose() * bBaseLocale;

    // On calcule sa normale dans la base cartesienne
    Eigen::Vector3d curNormaleBaseCartesienne;
    curNormaleBaseCartesienne = 2 * aBaseCartesienne * pCentered + bBaseCartesienne;

    //On la normalise
    curNormaleBaseCartesienne.normalize();

    return curNormaleBaseCartesienne;
}

CT_NMPCIR NORM_NewOctreeNode::getPointCloudInAcpBasisNMPCIR()
{
    int nPoints = _ptIndices->size();
    CT_NMPCIR rslt = PS_REPOSITORY->createNewPointCloud(nPoints);
    CT_MutablePointIterator pointIterator(rslt);

    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

    CT_PointAccessor pointAccessor;
    CT_Point currentPointInCartesianBasis;
    CT_Point currentPointInCartesianBasisCentered;
    CT_Point currentPointInLocalBasis;
    for( int i = 0 ; i < nPoints ; i++ )
    {
        pointAccessor.pointAt( _ptIndices->at(i), currentPointInCartesianBasis );
        currentPointInCartesianBasisCentered = currentPointInCartesianBasis - _centroid;
        currentPointInLocalBasis = versBaseLocale * currentPointInCartesianBasisCentered;

        pointIterator.next().replaceCurrentPoint( currentPointInLocalBasis );
    }

    return rslt;
}

CT_NMPCIR NORM_NewOctreeNode::getQuadraticPointCloud( float resx, float resy )
{
    float xMin = -_dimension;

    int nPointsx = _dimension*2 / resx;
    int nPointsy = _dimension*2 / resy;

    int nPoints = nPointsx * nPointsy;
    CT_Point curPointLocal;
    CT_Point curPointGlobalCentered;
    CT_Point curPointGlobal;
    CT_NMPCIR rslt = PS_REPOSITORY->createNewPointCloud(nPoints);
    CT_MutablePointIterator pointIterator(rslt);

    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);

    float curX = xMin;
    float curY = xMin;

    for( int i = 0 ; i < nPointsx ; i++ )
    {
        for( int j = 0 ; j < nPointsy ; j++ )
        {
            // On calcule le point sur la quadrique
            curPointLocal.setValues( curX, curY, quadraInLocalZ( curX, curY) );
            curPointGlobalCentered = retourBaseCartesienne * curPointLocal;
            curPointGlobal = curPointGlobalCentered + _centroid;
            pointIterator.next().replaceCurrentPoint( curPointGlobal );
            curY += resy;
        }
        curY = xMin;
        curX += resx;
    }

    return rslt;
}

double NORM_NewOctreeNode::quadraInLocalZ(CT_Point &p)
{
    return ( _a*p.x()*p.x() +
             _b*p.x()*p.y() +
             _c*p.y()*p.y() +
             _d*p.x() +
             _e*p.y() +
             _f);
}

double NORM_NewOctreeNode::quadraInLocalZ(double x, double y)
{
    return ( _a*x*x +
             _b*x*y +
             _c*y*y +
             _d*x +
             _e*y +
             _f );
}

void NORM_NewOctreeNode::getQuadraticPointCloudAndSignRecursive(QVector<CT_Scene *> &outScenes,
                                                                QString modelNameScenes,
                                                                CT_AbstractResult *resultScenes,
                                                                AttributEntier *outCoulors,
                                                                QString modelNameCoulors,
                                                                CT_AbstractResult *resultCoulors)
{
    // Si on est sur une feuille alors on cree un nuage de points et le nuage d'entier qui va avec
    if( isLeaf() )
    {
        if( !_ptIndices->empty() )
        {
            CT_NMPCIR outPointCloud = getQuadraticPointCloud( 0.01, 0.01 );
            CT_Scene* outScene = new CT_Scene( modelNameScenes, resultScenes, outPointCloud );
            outScenes.push_back( outScene );

            CT_Point curPoint;
            CT_PointAccessor ptAccessor;
            int nPoints= _ptIndices->size();
            for( int i = 0 ; i < nPoints ; i++ )
            {
                curPoint = ptAccessor.pointAt( _ptIndices->at(i) );
                outCoulors->setValueAt( _ptIndices->at(i), signeQuadra( curPoint ) );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->getQuadraticPointCloudAndSignRecursive( outScenes, modelNameScenes, resultScenes,
                                                                      outCoulors, modelNameCoulors, resultCoulors );
            }
        }
    }
}

void NORM_NewOctreeNode::orientConnexComponentAcpBasis()
{
    QQueue<NORM_NewOctreeNode*> queue;
    queue.append( this );
    setVisited( true );

    NORM_NewOctreeNode* currentNode;
    NORM_NewOctreeNode* currentNei;
    while( !queue.empty() )
    {
        // Prendre le premier de la file
        currentNode = queue.dequeue();

        // Continue le parcours en largeur en regardant les voisins du noeud courant
        const QVector<NORM_NewOctreeNode*>& currentNeighbours = currentNode->neighbours();
        int nNei = currentNeighbours.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            currentNei = currentNeighbours.at(i);

            if( !currentNei->_visited )
            {
                // Le voisin n'a pas ete parcouru
                // Si il appartient a la meme composante connexe d'orientation
                // alors on l'ajoute a la file et on le marque
            }
        }
    }
}

void NORM_NewOctreeNode::orientConnexComponentAcpBasisRecursive( int& nNodesConnex,
                                                                 QVector<CT_LineData*>& outPolyline)
{
    if( nNodesConnex == 0 )
    {
        qDebug() << "Centre du voxel depart du chemin" << _center;
    }
    nNodesConnex++;

    setVisited( true );

    const QVector<NORM_NewOctreeNode*>& currentNeighbours = neighbours();
    int nNei = currentNeighbours.size();
    NORM_NewOctreeNode* currentNei;
    for( int i = 0 ; i < nNei ; i++ )
    {
        currentNei = currentNeighbours.at(i);

        if( !currentNei->_visited )
        {
            // ///////////////////////////////////////////////////
            // On oriente le voisin
            // ///////////////////////////////////////////////////
            // On regarde si ils se touchent par une face
            CubeNeibourhoogType neiType = getCubeNeibourhoogType( currentNei );

//            qDebug() << "Centre courant " << _center.x() << _center.y() << _center.z()
//                     << "Centre voisin  " << currentNei->_center.x() << currentNei->_center.y() << currentNei->_center.z();

            if( neiType != VERTEX_NEIGHBOURHOOD && neiType != EDGE_NEIGHBOURHOOD )
            {
//                qDebug() << "Un voisin touche par face";

                // Si ils se touchent par une face on oriente selon la face
                // On marque comme oriente
                switch( neiType )
                {
                    case TOP_FACE :
                        orientNeighbourAcpBasisFromTopFace( currentNei );
                        break;

                    case BOT_FACE :
                        orientNeighbourAcpBasisFromBotFace( currentNei );
                        break;

                    case LEFT_FACE :
                        orientNeighbourAcpBasisFromLeftFace( currentNei );
                        break;

                    case RIGHT_FACE :
                        orientNeighbourAcpBasisFromRightFace( currentNei );
                        break;

                    case FRONT_FACE :
                        orientNeighbourAcpBasisFromFrontFace( currentNei );
                        break;

                    case BACK_FACE :
                        orientNeighbourAcpBasisFromBackFace( currentNei );
                        break;

                    default :
                        qDebug() << "Je sais pas quelle face";
                        break;
                }

                CT_LineData* curLineData = new CT_LineData( _centroid, currentNei->_centroid );
                outPolyline.push_back( curLineData );

                // Et on lance la recursion dessus
                currentNei->orientConnexComponentAcpBasisRecursive( nNodesConnex, outPolyline );
            }
        }
    }
}

void NORM_NewOctreeNode::orientConnexComponentAcpBasisRecursiveBreathFirst( int& nNodesConnex,
                                                                            QVector<CT_LineData*>& outPolyline)
{
    QQueue< NORM_NewOctreeNode* > queue;
    queue.enqueue( this );
    setVisited( true );
    nNodesConnex++;

    qDebug() << "Centre du noeud de depat " << center();

    while( !queue.empty() )
    {
        NORM_NewOctreeNode* curNode = queue.dequeue();

        const QVector<NORM_NewOctreeNode*>& currentNeighbours = curNode->neighbours();
        int nNei = currentNeighbours.size();
        NORM_NewOctreeNode* currentNei;
        for( int i = 0 ; i < nNei ; i++ )
        {
            currentNei = currentNeighbours.at(i);

            if( !currentNei->_visited )
            {
                // ///////////////////////////////////////////////////
                // On oriente le voisin
                // ///////////////////////////////////////////////////
                // On regarde si ils se touchent par une face
                CubeNeibourhoogType neiType = curNode->getCubeNeibourhoogType( currentNei );

                if( neiType != VERTEX_NEIGHBOURHOOD && neiType != EDGE_NEIGHBOURHOOD )
                {
                    queue.enqueue( currentNei );
                    currentNei->setVisited( true );
                    nNodesConnex++;

                    // Si ils se touchent par une face on oriente selon la face
                    // On marque comme oriente
                    switch( neiType )
                    {
                        case TOP_FACE :
                            curNode->orientNeighbourAcpBasisFromTopFace( currentNei );
                            break;

                        case BOT_FACE :
                            curNode->orientNeighbourAcpBasisFromBotFace( currentNei );
                            break;

                        case LEFT_FACE :
                            curNode->orientNeighbourAcpBasisFromLeftFace( currentNei );
                            break;

                        case RIGHT_FACE :
                            curNode->orientNeighbourAcpBasisFromRightFace( currentNei );
                            break;

                        case FRONT_FACE :
                            curNode->orientNeighbourAcpBasisFromFrontFace( currentNei );
                            break;

                        case BACK_FACE :
                            curNode->orientNeighbourAcpBasisFromBackFace( currentNei );
                            break;

                        default :
                            qDebug() << "Je sais pas quelle face";
                            assert(false);
                            break;
                    }

                    CT_LineData* curLineData = new CT_LineData( curNode->_centroid, currentNei->_centroid );
                    outPolyline.push_back( curLineData );
                }
            }
        }
    }
}

void NORM_NewOctreeNode::orientPcaRecursive(bool &tmpFirstFound,
                                            QVector<CT_LineData*>& curPolyline )
{
    if( tmpFirstFound )
    {
        return;
    }

    // Si on est une feuille alors on peut orienter les voisins
    if( isLeaf() )
    {
        // SI on a deja ete oriente, on ne fait rien
        if( _visited )
        {
            return;
        }

        else
        {
            int nnodes = 0;
//            orientConnexComponentAcpBasisRecursive( nnodes, curPolyline );
            orientConnexComponentAcpBasisRecursiveBreathFirst( nnodes, curPolyline );
            qDebug() << "NNodes = " << nnodes;
            tmpFirstFound = true;
        }
    }

    // Sinon on descend jusqu'a trouver une feuille qui n'aie pas encore ete orientee
    else
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->orientPcaRecursive( tmpFirstFound, curPolyline );
            }
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromTopFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 5, corner );
        curDim =_dimension;
    }

    else
    {
        nei->getCorner( 1, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p5 p6 p7 p8] ou [n1 n2 n3 n4]
    int npx = 20;    float stepx = _dimension * 2.0 / (float)npx;
    int npy = 20;    float stepy = _dimension * 2.0 / (float)npy;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npx ;i++ )
    {
        for( int j = 0 ; j < npy ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x() + (i*stepx), corner.y() + (j*stepy), corner.z() );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npx * npy) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npx * npy) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromBotFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 1, corner );
        curDim =_dimension;
    }

    else
    {
        nei->getCorner( 5, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p1 p2 p3 p4] ou [n5 n6 n7 n8]
    int npx = 20;    float stepx = _dimension * 2.0 / (float)npx;
    int npy = 20;    float stepy = _dimension * 2.0 / (float)npy;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npx ;i++ )
    {
        for( int j = 0 ; j < npy ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x() + (i*stepx), corner.y() + (j*stepy), corner.z() );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npx * npy) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npx * npy) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromLeftFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 1, corner );
        curDim =_dimension;
    }

    else
    {
        nei->getCorner( 4, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p1 p2 p5 p6] ou [n3 n4 n7 n8]
    int npy = 20;    float stepy = _dimension * 2.0 / (float)npy;
    int npz = 20;    float stepz = _dimension * 2.0 / (float)npz;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npy ;i++ )
    {
        for( int j = 0 ; j < npz ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x(), corner.y() + (i*stepy), corner.z() + (j*stepz) );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npy * npz) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npy * npz) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromRightFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 4, corner );
        curDim =_dimension;
    }

    else
    {
        nei->getCorner( 1, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p3 p4 p7 p8] ou [n1 n2 n5 n6]
    int npy = 20;    float stepy = _dimension * 2.0 / (float)npy;
    int npz = 20;    float stepz = _dimension * 2.0 / (float)npz;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npy ;i++ )
    {
        for( int j = 0 ; j < npz ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x(), corner.y() + (i*stepy), corner.z() + (j*stepz) );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npy * npz) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npy * npz) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromFrontFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 2, corner );
        curDim =_dimension;
    }

    else
    {
        nei->getCorner( 1, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p2 p3 p6 p7] ou [n1 n4 n5 n8]
    int npx = 20;    float stepx = _dimension * 2.0 / (float)npx;
    int npz = 20;    float stepz = _dimension * 2.0 / (float)npz;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npx ;i++ )
    {
        for( int j = 0 ; j < npz ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x() + (i*stepx), corner.y(), corner.z() + (j*stepz) );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npx * npz) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npx * npz) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::orientNeighbourAcpBasisFromBackFace( NORM_NewOctreeNode* nei )
{
    // On regarde le noeud le plus petit entre le noeud courant et le voisin
    // Et on prend les points de la face la plus petite
    CT_Point corner;
    double curDim;
    if( _dimension < nei->_dimension )
    {
        getCorner( 1, corner );
        curDim = _dimension;
    }

    else
    {
        nei->getCorner( 2, corner );
        curDim = nei->_dimension;
    }

    // Si le voisin est lineaire alors on le met dans le meme sens que le noeud courant
    // On teste l'index linearite et on utilise le produit scalaire
    if( getLinearity() < 0.1 || nei->getLinearity() < 0.1 )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }

        return;
    }

    // On teste np1 * np2 points dans la face [p1 p4 p5 p8] ou [n2 n3 n6 n7]
    int npx = 20;    float stepx = _dimension * 2.0 / (float)npx;
    int npz = 20;    float stepz = _dimension * 2.0 / (float)npz;
    int nDifferences = 0;
    CT_Point curPoint;
    for( int i = 0 ; i < npx ;i++ )
    {
        for( int j = 0 ; j < npz ; j++ )
        {
            // Calcule le point correspondant sur la face
            curPoint.setValues( corner.x() + (i*stepx), corner.y(), corner.z() + (j*stepz) );

            // Regarde si il y a une difference de signe
            if( signeQuadra( curPoint ) * nei->signeQuadra( curPoint ) < 0 )
            {
                nDifferences++;
            }
        }
    }

    // Si il y a plus de la moitie des points qui sont de signe different
    if( nDifferences > ( (npx * npz) / 2.0 ) )
    {
        // Alors on inverse la base du voisin
        nei->reverseOrientation();
    }

    if( nDifferences == ( (npx * npz) / 2.0 ) )
    {
        // Pas pls d'indices que ca sur l'orientation, on oriente grace au produit scalaire
        float scalarProd = _v3.x()*nei->_v3.x() + _v3.y()*nei->_v3.y() + _v3.z()*nei->_v3.z();

        // Pour un produit scalaire negatif il faut inverser la base du voisin
        if( scalarProd < 0 )
        {
            nei->reverseOrientation();
        }
    }
}

void NORM_NewOctreeNode::reverseOrientation()
{
    Eigen::Vector3d swap = _v2;
    _v2 = _v1;
    _v1 = swap;
    _v3 = -1 * _v3;

    // a pour x et xdevient y
    float swapFloat = _c;
    _c = -_a;
    _a = -swapFloat;
    _b *= -1;

    swapFloat = _d;
    _d = -_e;
    _e = -swapFloat;

    _f *= -1;
}

int NORM_NewOctreeNode::signeQuadra(const CT_Point &p, QString text )
{
    // Transforme p dans la base locale
    Eigen::Matrix3d retourBaseCartesienne;
    retourBaseCartesienne << _v1(0) , _v2(0) , _v3(0),
                             _v1(1) , _v2(1) , _v3(1),
                             _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d versBaseLocale = retourBaseCartesienne.inverse();

    CT_Point pGlobalCentered = p - _centroid;
    CT_Point pLocal = versBaseLocale * pGlobalCentered;

    float surfaceHeight = _a*pLocal.x()*pLocal.x() +
                          _b*pLocal.x()*pLocal.y() +
                          _c*pLocal.y()*pLocal.y() +
                          _d*pLocal.x() +
                          _e*pLocal.y() +
                          _f;

    if( text != "" )
    {
        qDebug() << "Global " << p << " Local " << pLocal;
        qDebug() << text << "Surface Height " << surfaceHeight << "pLocalZ " << pLocal.z();
    }

    if( pLocal.z() > surfaceHeight )
    {
        return 1;
    }

    return -1;
}

CubeNeibourhoogType NORM_NewOctreeNode::getCubeNeibourhoogType(NORM_NewOctreeNode *nei)
{
    int nInside = 0;

    CT_Point curNodeBot;
    CT_Point curNodeTop;
    getBBox( curNodeBot, curNodeTop );

    CT_Point neiNodeBot;
    CT_Point neiNodeTop;
    nei->getBBox( neiNodeBot, neiNodeTop );

    CT_Point p1, p2, p3, p4, p5, p6, p7, p8;
    CT_Point np1, np2, np3, np4, np5, np6, np7, np8;

    getCorners( p1, p2, p3, p4, p5, p6, p7, p8 );
    nei->getCorners( np1, np2, np3, np4, np5, np6, np7, np8 );

//    qDebug() << "Courant " << "(" << p5.x() << p5.y() << p5.z() << ")"
//                << "(" << p6.x() << p6.y() << p6.z() << ")"
//                << "(" << p7.x() << p7.y() << p7.z() << ")"
//                << "(" << p8.x() << p8.y() << p8.z() << ")";

//    qDebug() << "Voisin " << "(" << np1.x() << np1.y() << np1.z() << ")"
//                << "(" << np2.x() << np2.y() << np2.z() << ")"
//                << "(" << np3.x() << np3.y() << np3.z() << ")"
//                << "(" << np4.x() << np4.y() << np4.z() << ")";

    // Si le centre d'une face est incluse (sens large : incluse ou "touchant") dans l'autre face :
    // - Pour 3 coordonnees : impossible (le cube voisin serait dans le cube courant)
    // - Pour 2 coordonnees : voisinage de face
    // - Pour 1 coordonnees : voisinage d'arete
    // - Pour 0 coordonnees : voisinage par sommet

    CT_Point& curCenter = _center;
    CT_Point& neiCenter = nei->centerRef();

    // //////////////////////////////////////////////////////////////////////
    // Test de l'inclusion du centre du voisin dans le cube courant
    int nCoordNeiDansCur = 0;
    if( neiCenter.x() >= curNodeBot.x() && neiCenter.x() <= curNodeTop.x() )
    {
        // Le centre du voisin est dans le range x du cube courant
//        qDebug() << "Le centre du voisin est dans le range x du cube courant";
        nCoordNeiDansCur++;
    }

    if( neiCenter.y() >= curNodeBot.y() && neiCenter.y() <= curNodeTop.y() )
    {
        // Le centre du voisin est dans le range y du cube courant
//        qDebug() << "Le centre du voisin est dans le range y du cube courant";
        nCoordNeiDansCur++;
    }

    if( neiCenter.z() >= curNodeBot.z() && neiCenter.z() <= curNodeTop.z() )
    {
        // Le centre du voisin est dans le range z du cube courant
//        qDebug() << "Le centre du voisin est dans le range z du cube courant";
        nCoordNeiDansCur++;
    }
    // //////////////////////////////////////////////////////////////////////

    // //////////////////////////////////////////////////////////////////////
    // Test de l'inclusion du centre du courant dans le cube voisin
    int nCoordCurDansNei = 0;
    if( curCenter.x() >= neiNodeBot.x() && curCenter.x() <= neiNodeTop.x() )
    {
        // Le centre du voisin est dans le range x du cube courant
//        qDebug() << "Le centre du voisin est dans le range x du cube courant 2";
        nCoordCurDansNei++;
    }

    if( curCenter.y() >= neiNodeBot.y() && curCenter.y() <= neiNodeTop.y() )
    {
        // Le centre du voisin est dans le range y du cube courant
//        qDebug() << "Le centre du voisin est dans le range y du cube courant2";
        nCoordCurDansNei++;
    }

    if( curCenter.z() >= neiNodeBot.z() && curCenter.z() <= neiNodeTop.z() )
    {
        // Le centre du voisin est dans le range z du cube courant
//        qDebug() << "Le centre du voisin est dans le range z du cube courant 2";
        nCoordCurDansNei++;
    }
    // //////////////////////////////////////////////////////////////////////

    nInside = std::max( nCoordCurDansNei, nCoordNeiDansCur );
//    qDebug() << "nInside = " << nInside;

    if( nInside == 2 )
    {
        // On regarde quelle est la face commune
        // Face de gauche
        if( np4.x() < p1.x() + NORM_NEW_OCTREE_EPSILONE && np4.x() > p1.x() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Left";
            return LEFT_FACE;
        }

        // Face de droite
        else if( np1.x() < p4.x() + NORM_NEW_OCTREE_EPSILONE && np1.x() > p4.x() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Right";
            return RIGHT_FACE;
        }

        // Face de devant
        else if( np2.y() < p1.y() + NORM_NEW_OCTREE_EPSILONE && np2.y() > p1.y() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Front";
            return BACK_FACE;
        }

        // Face de derriere
        else if( np1.y() < p2.y() + NORM_NEW_OCTREE_EPSILONE && np1.y() > p2.y() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Back";
            return FRONT_FACE;
        }

        // Face du dessus
        else if( np1.z() < p5.z() + NORM_NEW_OCTREE_EPSILONE && np1.z() > p5.z() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Top";
            return TOP_FACE;
        }

        // Face du dessous
        else if( np5.z() < p1.z() + NORM_NEW_OCTREE_EPSILONE && np5.z() > p1.z() - NORM_NEW_OCTREE_EPSILONE )
        {
//            qDebug() << "Bot";
            return BOT_FACE;
        }
    }

    if( nInside == 1 )
    {
        return EDGE_NEIGHBOURHOOD;
    }

    return VERTEX_NEIGHBOURHOOD;
}

CubeNeibourhoogType NORM_NewOctreeNode::getOppositeFaceNeibourhoogType(CubeNeibourhoogType type)
{
    if( type == EDGE_NEIGHBOURHOOD || type == VERTEX_NEIGHBOURHOOD )
    {
        assert(false);
    }

    switch ( type )
    {
        case TOP_FACE :
        {
            return BOT_FACE;
            break;
        }

        case BOT_FACE :
        {
            return TOP_FACE;
            break;
        }

        case LEFT_FACE :
        {
            return RIGHT_FACE;
            break;
        }

        case RIGHT_FACE :
        {
            return LEFT_FACE;
            break;
        }

        case FRONT_FACE :
        {
            return BACK_FACE;
            break;
        }

        case BACK_FACE :
        {
            return FRONT_FACE;
            break;
        }
    }
}

bool NORM_NewOctreeNode::visited() const
{
    return _visited;
}

void NORM_NewOctreeNode::setVisited(bool visited)
{
    _visited = visited;
}

void NORM_NewOctreeNode::setVisitedRecursive(bool visited)
{
    _visited = visited;

    for( int i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            _children[i]->setVisitedRecursive( visited );
        }
    }
}

CT_Point NORM_NewOctreeNode::center() const
{
    return _center;
}

CT_Point &NORM_NewOctreeNode::centerRef()
{
    return _center;
}

void NORM_NewOctreeNode::setCenter(const CT_Point &center)
{
    _center = center;
}

CT_Point NORM_NewOctreeNode::getFaceCenter(CubeNeibourhoogType neiType) const
{
    switch( neiType )
    {
        case TOP_FACE :
            return createCtPoint( _center.x(), _center.y(), _center.z() + _dimension );
            break;

        case BOT_FACE :
            return createCtPoint( _center.x(), _center.y(), _center.z() - _dimension );
            break;

        case LEFT_FACE :
            return createCtPoint( _center.x() - _dimension, _center.y(), _center.z() );
            break;

        case RIGHT_FACE :
            return createCtPoint( _center.x() + _dimension, _center.y(), _center.z() );
            break;

        case FRONT_FACE :
            return createCtPoint( _center.x(), _center.y() + _dimension, _center.z() );
            break;

        case BACK_FACE :
            return createCtPoint( _center.x(), _center.y() - _dimension, _center.z() );
            break;

        default :
            qDebug() << "Je sais pas quelle face";
            qDebug() << "\n CT_Point NORM_NewOctreeNode::getFaceCenter(CubeNeibourhoogType neiType) const\n";
            exit(1);
            break;
    }
}



