/************************************************************************************
* Filename :  norm_newoctreedrawmanager.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_newoctreedrawmanager.h"
#include "norm_newoctree.h"
#include <QDebug>

// Initialise static attributes
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_ACP_BASIS = NORM_NewOctreeDrawManager::staticInitConfigDisplayAcpBasis();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_1 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes1();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_2 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes2();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_3 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes3();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_4 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes4();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_5 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes5();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_6 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes6();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_7 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes7();
const QString NORM_NewOctreeDrawManager::INDEX_CONFIG_DISPLAY_NODE_BOXES_8 = NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes8();

NORM_NewOctreeDrawManager::NORM_NewOctreeDrawManager( QString drawConfigurationName )
    : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? QString("NORM_NEW_OCTREE") : drawConfigurationName)
{
}


void NORM_NewOctreeDrawManager::draw(GraphicsViewInterface& view,
                                         PainterInterface& painter,
                                         const CT_AbstractItemDrawable& itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    // Get the item to display
    const NORM_NewOctree& octree = dynamic_cast<const NORM_NewOctree& >(itemDrawable);

    bool drawAcpBasis = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_ACP_BASIS).toBool();
    bool drawNodeBoxes = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES).toBool();
    QVector<bool> drawBoxes;
    drawBoxes.resize(8);
    drawBoxes[0] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_1).toBool();
    drawBoxes[1] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_2).toBool();
    drawBoxes[2] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_3).toBool();
    drawBoxes[3] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_4).toBool();
    drawBoxes[4] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_5).toBool();
    drawBoxes[5] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_6).toBool();
    drawBoxes[6] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_7).toBool();
    drawBoxes[7] = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_NODE_BOXES_8).toBool();

    // Draw the octree
    octree.draw( view, painter,
                 drawNodeBoxes,
                 drawBoxes,
                 drawAcpBasis );
}

CT_ItemDrawableConfiguration NORM_NewOctreeDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));

    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes(), "Display Node boxes", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayAcpBasis(), "Display Acp Basis", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes1(), "Display Node boxes 1", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes2(), "Display Node boxes 2", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes3(), "Display Node boxes 3", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes4(), "Display Node boxes 4", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes5(), "Display Node boxes 5", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes6(), "Display Node boxes 6", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes7(), "Display Node boxes 7", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(staticInitConfigDisplayNodeBoxes8(), "Display Node boxes 8", CT_ItemDrawableConfiguration::Bool, true);


    return item;
}

// PROTECTED //
QString NORM_NewOctreeDrawManager::staticInitConfigDisplayAcpBasis()
{
    return "AOCTREE_DAB";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes()
{
    return "AOCTREE_DNB";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes1()
{
    return "AOCTREE_DNB1";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes2()
{
    return "AOCTREE_DNB2";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes3()
{
    return "AOCTREE_DNB3";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes4()
{
    return "AOCTREE_DNB4";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes5()
{
    return "AOCTREE_DNB5";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes6()
{
    return "AOCTREE_DNB6";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes7()
{
    return "AOCTREE_DNB7";
}

QString NORM_NewOctreeDrawManager::staticInitConfigDisplayNodeBoxes8()
{
    return "AOCTREE_DNB8";
}
