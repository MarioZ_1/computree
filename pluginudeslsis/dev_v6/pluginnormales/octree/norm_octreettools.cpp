#include "norm_octreettools.h"

#include "ct_accessor/ct_pointaccessor.h"
#include "ct_cloudindex/abstract/ct_abstractmodifiablecloudindext.h"
#include "ct_cloudindex/registered/ct_standardmodifiablecloudindexregisteredt.hpp"

size_t NORM_OctreeTools::sortPointsFromPivot(size_t sortFirstIndex,
                                             size_t sortLastIndex,
                                             float pivot,
                                             int sortAxis,
                                             CT_PointCloudIndexVector* outSortedIndexCloudPtr)
{
    CT_PointAccessor pAccess;
    CT_Point currPoint;

    if ( sortLastIndex == sortFirstIndex )
    {
        // Then there is no points to be sorted
        // Return the index of the last sorted point
        return sortLastIndex;
    }

    else if ( sortLastIndex - sortFirstIndex == 1 )
    {
        // Then there is only one point p to sort
        // p can be either lower or greater than the sorting center
        pAccess.pointAt( outSortedIndexCloudPtr->indexAt(sortFirstIndex), currPoint );
        if ( currPoint(sortAxis) < pivot )
        {
            // p is below the center, then return the last index
            return sortLastIndex;
        }

        else
        {
            // p is above the center, return the first index
            return sortFirstIndex;
        }
    }

    else
    {
        // There are more than one point in the sub cloud
        // We use a quicksort algorithm to sort them
        size_t i = sortFirstIndex, j = sortLastIndex-1, swap;

        // Quick sort algorithm
        while ( i < j )
        {
            while ( i < j && pAccess.pointAt( outSortedIndexCloudPtr->indexAt(i) )(sortAxis) < pivot )
            {
                i++;
            }

            while ( i < j && pAccess.pointAt( outSortedIndexCloudPtr->indexAt(j) )(sortAxis) >= pivot )
            {
                j--;
            }

            // Swap the ith and jth elements (indices)
            swap = (*outSortedIndexCloudPtr)[i];
            outSortedIndexCloudPtr->replaceIndex( i, (*outSortedIndexCloudPtr)[j], false);
            outSortedIndexCloudPtr->replaceIndex( j, swap, false );
        }

        // Testing the ending value of the sorting algorithm
        if ( i == sortLastIndex - 1 )
        {
            // Points where already sorted
            // The last one has not been tested since i == j == endIndex - 1
            // So we test it
            if ( pAccess.pointAt( outSortedIndexCloudPtr->indexAt(sortLastIndex - 1) )(sortAxis) < pivot )
            {
                return sortLastIndex;
            }

            else
            {
                return ( sortLastIndex - 1);
            }
        }
        return i;
    }
}

size_t NORM_OctreeTools::sortPointsFromPivot(size_t sortFirstIndex,
                                             size_t sortLastIndex,
                                             float pivot,
                                             int sortAxis,
                                             CT_MPCIR outSortedIndexCloudPtr)
{
    return sortPointsFromPivot( sortFirstIndex,
                                sortLastIndex,
                                pivot,
                                sortAxis,
                                outSortedIndexCloudPtr->abstractModifiableCloudIndexT() );
}

size_t NORM_OctreeTools::sortPointsFromPivot(size_t sortFirstIndex,
                                             size_t sortLastIndex,
                                             float pivot,
                                             int sortAxis,
                                             CT_AbstractModifiableCloudIndexT<CT_PointData>* outSortedIndexCloudPtr)
{
    CT_PointAccessor pAccess;
    CT_Point currPoint;

    if ( sortLastIndex == sortFirstIndex )
    {
        // Then there is no points to be sorted
        // Return the index of the last sorted point
        return sortLastIndex;
    }

    else if ( sortLastIndex - sortFirstIndex == 1 )
    {
        // Then there is only one point p to sort
        // p can be either lower or greater than the sorting center
        pAccess.pointAt( outSortedIndexCloudPtr->indexAt(sortFirstIndex), currPoint );
        if ( currPoint(sortAxis) < pivot )
        {
            // p is below the center, then return the last index
            return sortLastIndex;
        }

        else
        {
            // p is above the center, return the first index
            return sortFirstIndex;
        }
    }

    else
    {
        // There are more than one point in the sub cloud
        // We use a quicksort algorithm to sort them
        size_t i = sortFirstIndex, j = sortLastIndex-1, swap;

        // Quick sort algorithm
        while ( i < j )
        {
            while ( i < j && pAccess.pointAt( outSortedIndexCloudPtr->indexAt(i) )(sortAxis) < pivot )
            {
                i++;
            }

            while ( i < j && pAccess.pointAt( outSortedIndexCloudPtr->indexAt(j) )(sortAxis) >= pivot )
            {
                j--;
            }

            // Swap the ith and jth elements (indices)
            swap = outSortedIndexCloudPtr->indexAt(i);
            outSortedIndexCloudPtr->replaceIndex( i, outSortedIndexCloudPtr->indexAt(j), false );
            outSortedIndexCloudPtr->replaceIndex( j, swap, false );
        }

        // Testing the ending value of the sorting algorithm
        if ( i == sortLastIndex - 1 )
        {
            // Points where already sorted
            // The last one has not been tested since i == j == endIndex - 1
            // So we test it
            if ( pAccess.pointAt( outSortedIndexCloudPtr->indexAt(sortLastIndex - 1) )(sortAxis) < pivot )
            {
                return sortLastIndex;
            }

            else
            {
                return ( sortLastIndex - 1);
            }
        }
        return i;
    }
}

void NORM_OctreeTools::sortMortonOrder( size_t sortFirstIndex,
                                        size_t sortLastIndex,
                                        const CT_Point &center,
                                        size_t blocs[],
                                        CT_PointCloudIndexVector* outSortedIndexCloudPtr)
{
    // Sorting points
    blocs[0] = sortFirstIndex;
    blocs[8] = sortLastIndex;

    // Along the z axis
    blocs[4] = sortPointsFromPivot(  blocs[0], blocs[8], center(2), 2, outSortedIndexCloudPtr );

    // Along the Y axis
    blocs[2] = sortPointsFromPivot(  blocs[0], blocs[4], center(1), 1, outSortedIndexCloudPtr );
    blocs[6] = sortPointsFromPivot(  blocs[4], blocs[8], center(1), 1, outSortedIndexCloudPtr );

    // Along the X axis
    blocs[1] = sortPointsFromPivot(  blocs[0], blocs[2], center(0), 0, outSortedIndexCloudPtr );
    blocs[3] = sortPointsFromPivot(  blocs[2], blocs[4], center(0), 0, outSortedIndexCloudPtr );
    blocs[5] = sortPointsFromPivot(  blocs[4], blocs[6], center(0), 0, outSortedIndexCloudPtr );
    blocs[7] = sortPointsFromPivot(  blocs[6], blocs[8], center(0), 0, outSortedIndexCloudPtr );
}

void NORM_OctreeTools::sortMortonOrder( size_t sortFirstIndex,
                                        size_t sortLastIndex,
                                        const CT_Point &center,
                                        size_t blocs[],
                                        CT_MPCIR outSortedIndexCloudPtr)
{
    // Sorting points
    blocs[0] = sortFirstIndex;
    blocs[8] = sortLastIndex;

    // Along the z axis
    blocs[4] = sortPointsFromPivot(  blocs[0], blocs[8], center(2), 2, outSortedIndexCloudPtr );

    // Along the Y axis
    blocs[2] = sortPointsFromPivot(  blocs[0], blocs[4], center(1), 1, outSortedIndexCloudPtr );
    blocs[6] = sortPointsFromPivot(  blocs[4], blocs[8], center(1), 1, outSortedIndexCloudPtr );

    // Along the X axis
    blocs[1] = sortPointsFromPivot(  blocs[0], blocs[2], center(0), 0, outSortedIndexCloudPtr );
    blocs[3] = sortPointsFromPivot(  blocs[2], blocs[4], center(0), 0, outSortedIndexCloudPtr );
    blocs[5] = sortPointsFromPivot(  blocs[4], blocs[6], center(0), 0, outSortedIndexCloudPtr );
    blocs[7] = sortPointsFromPivot(  blocs[6], blocs[8], center(0), 0, outSortedIndexCloudPtr );
}

void NORM_OctreeTools::getBoundingCubeFromBoundingBox(const CT_Point &bboxBot,
                                                      const CT_Point &bboxTop,
                                                      CT_Point &outBcBot,
                                                      CT_Point &outBcTop)
{
    // Same lower left corner
    outBcBot = bboxBot;

    // Compute the axis with the greatest lenght
    float   greatestLenght = -1;
    float currentLength;
    for ( int i = 0 ; i < 3 ; i++ )
    {
        currentLength = bboxTop(i) - bboxBot(i);

        if ( currentLength > greatestLenght )
        {
            greatestLenght = currentLength;
        }
    }

    for ( int i = 0 ; i < 3 ; i++ )
    {
        outBcTop(i) = outBcBot(i) + greatestLenght;
    }
}


