#include "norm_octreeexporter.h"

#include "new/norm_newoctreev2.h"

#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>

NORM_OctreeExporter::NORM_OctreeExporter() :
    CT_AbstractExporter()
{
}

NORM_OctreeExporter::~NORM_OctreeExporter()
{
}

QString NORM_OctreeExporter::getExporterCustomName() const
{
    return tr("LSIS_Octree_NewOctreeV2");
}

CT_StepsMenu::LevelPredefined NORM_OctreeExporter::getExporterSubMenuName() const
{
    return CT_StepsMenu::LP_Voxels;
}


void NORM_OctreeExporter::init()
{
    addNewExportFormat(FileFormat("lsis_octree", tr("Fichiers Octree LSIS")));

    setToolTip(tr("Export d'un octree pour compression avec perte"));
}

bool NORM_OctreeExporter::setItemDrawableToExport(const QList<CT_AbstractItemDrawable*> &list)
{
    clearErrorMessage();

    QList<CT_AbstractItemDrawable*> myList;
    QListIterator<CT_AbstractItemDrawable*> it(list);
    int nOctrees= 0;

    while(it.hasNext())
    {
        CT_AbstractItemDrawable *item = it.next();
        if(dynamic_cast<NORM_NewOctreeV2*>(item) != NULL)
        {
            myList.append(item);
            ++nOctrees;
        }
    }

    if(nOctrees == 0)
    {
        setErrorMessage(tr("Aucun ItemDrawable du type NORM_NewOctreeV2"));
        return false;
    }

    return CT_AbstractExporter::setItemDrawableToExport(myList);
}

bool NORM_OctreeExporter::configureExport()
{
    if(!errorMessage().isEmpty())
    {
        QMessageBox::critical(NULL, tr("Erreur"), errorMessage(), QMessageBox::Ok);
        return false;
    }

    return true;
}

CT_AbstractExporter* NORM_OctreeExporter::copy() const
{
    return new NORM_OctreeExporter();
}

bool NORM_OctreeExporter::protectedExportToFile()
{
    bool ok = true;

    QFileInfo exportPathInfo = QFileInfo(exportFilePath());
    QString path = exportPathInfo.path();
    QString baseName = exportPathInfo.baseName();
    QString suffix = "lsis_octree";

    QString indice = "";
    if( itemDrawableToExport().size() > 1 )
    {
        indice = "_0";
    }
    int cpt = 0;

    QListIterator<CT_AbstractItemDrawable*> it(itemDrawableToExport());
    while (it.hasNext())
    {
        NORM_NewOctreeV2* item = dynamic_cast<NORM_NewOctreeV2*>(it.next());
        if (item != NULL)
        {
            QString filePath = QString("%1/%2%3.%4").arg(path).arg(baseName).arg(indice).arg(suffix);

            item->saveToFile( filePath );

            indice = QString("_%1").arg(++cpt);
        }
    }

    return ok;
}
