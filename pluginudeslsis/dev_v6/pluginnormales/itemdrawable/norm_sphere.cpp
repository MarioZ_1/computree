#include "norm_sphere.h"

const NORM_StandardSphereDrawManager NORM_Sphere::SPHERE_DRAW_MANAGER;

CT_DEFAULT_IA_INIT(NORM_Sphere)

NORM_Sphere::NORM_Sphere()
    : CT_Circle()
{
    setBaseDrawManager(&SPHERE_DRAW_MANAGER);
}

NORM_Sphere::NORM_Sphere(const CT_OutAbstractSingularItemModel *model,
                         const CT_AbstractResult *result,
                         CT_CircleData* data)
    : CT_Circle(model, result, data)
{
    setBaseDrawManager(&SPHERE_DRAW_MANAGER);
}

NORM_Sphere::NORM_Sphere(const QString &modelName,
                         const CT_AbstractResult *result,
                         CT_CircleData* data)
    : CT_Circle(modelName, result, data)
{
    setBaseDrawManager(&SPHERE_DRAW_MANAGER);
}

QString NORM_Sphere::getType() const
{
    return staticGetType();
}

QString NORM_Sphere::staticGetType()
{
    return CT_AbstractShape::staticGetType() + "/NORM_Sphere";
}

double NORM_Sphere::getRadius() const
{
    return ((const CT_CircleData&)getData()).getRadius();
}

double NORM_Sphere::getError() const
{
    return ((const CT_CircleData&)getData()).getError();
}

CT_AbstractItemDrawable* NORM_Sphere::copy(const CT_OutAbstractItemModel *model,
                                           const CT_AbstractResult *result,
                                           CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED(copyModeList);
    NORM_Sphere* sphere = new NORM_Sphere( (const CT_OutAbstractSingularItemModel *)model,
                                           result,
                                           (getPointerData() != NULL) ? ( (const CT_CircleData&)getData()).clone() : NULL );
    sphere->setId(id());
    sphere->setAlternativeDrawManager(getAlternativeDrawManager());

    return sphere;
}

CT_AbstractItemDrawable* NORM_Sphere::copy(const QString &modelName,
                                           const CT_AbstractResult *result,
                                           CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED(copyModeList);
    NORM_Sphere *sphere = new NORM_Sphere(modelName,
                                          result,
                                          (getPointerData() != NULL) ? ((const CT_CircleData&)getData()).clone() : NULL);
    sphere->setId(id());
    sphere->setAlternativeDrawManager(getAlternativeDrawManager());

    return sphere;
}
