#ifndef NORM_SPHERE_H
#define NORM_SPHERE_H

// Derive d'un CT_Circlepuisqu'ils ont les memes parametres, seul le drawManager change
#include "ct_itemdrawable/ct_circle.h"
#include "norm_standardspheredrawmanager.h"

/*!
 * \class NORM_Sphere
 * \ingroup PluginShared_Items
 * \brief <b>ItemDrawable managing a NORM_SphereData</b>
 *
 * It represents a sphere in 3D, defined by a center, a direction and a radius.
 *
 */
class NORM_Sphere : public CT_Circle
{
    // IMPORTANT pour avoir le nom de l'ItemDrawable
    Q_OBJECT

public:
    /*!
     * \brief NORM_Sphere
     * Default constructor
     */
    NORM_Sphere();

    /**
      * \brief Contructeur avec une instance des donnes (NORM_SphereData*), ne peut etre NULL !
      * (Supprime dans le destructeur de la classe).
      */
    NORM_Sphere(const CT_OutAbstractSingularItemModel *model,
                const CT_AbstractResult *result,
                CT_CircleData* data);

    /**
      * \brief Contructeur avec une instance des donnes (NORM_SphereData*), ne peut etre NULL !
      * (Supprime dans le destructeur de la classe).
      */
    NORM_Sphere(const QString &modelName,
                const CT_AbstractResult *result,
                CT_CircleData* data);


    /**
      * ATTENTION : ne pas oublier de redfinir ces deux méthodes si vous héritez de cette classe.
      */
    virtual QString getType() const;
    static QString staticGetType();

    double getRadius() const;
    double getError() const;

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList);
    virtual CT_AbstractItemDrawable *copy(const QString &modelName, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList);

private:

    CT_DEFAULT_IA_BEGIN(NORM_Sphere)
    CT_DEFAULT_IA_V2(NORM_Sphere, CT_AbstractCategory::staticInitDataX(), &NORM_Sphere::getCenterX, QObject::tr("Centre X"))
    CT_DEFAULT_IA_V2(NORM_Sphere, CT_AbstractCategory::staticInitDataY(), &NORM_Sphere::getCenterY, QObject::tr("Centre Y"))
    CT_DEFAULT_IA_V2(NORM_Sphere, CT_AbstractCategory::staticInitDataZ(), &NORM_Sphere::getCenterZ, QObject::tr("Centre Z"))
    CT_DEFAULT_IA_V2(NORM_Sphere, CT_AbstractCategory::staticInitDataRadius(), &NORM_Sphere::getRadius, QObject::tr("Rayon du cercle"))
    CT_DEFAULT_IA_END(NORM_Sphere)

    const static NORM_StandardSphereDrawManager SPHERE_DRAW_MANAGER;
};

#endif // NORM_SPHERE_H
