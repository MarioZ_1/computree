#include "icro_stepsplitandmergeconnectedcomponents.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_iterator/ct_pointiterator.h"

#include <QQueue>

#define SPLIT_SPACE_icro_stepsplitandmergeconnectedcomponents 0
#define SPLIT_INTENSITY_icro_stepsplitandmergeconnectedcomponents 1

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"
#define DEFin_itmScene "itmScene"

#define DEFout_rsltResultConnectedComponentNonMultis "rsltResultConnectedComponentsGrid"

// Resultat contient un groupe general
#define DEFout_grpGeneral "grpGeneral"

// Groupe general contient
// - un groupe de groupe de cercles pour les arbres
// - une grille 3D qui contient les composantes connexes
// - un groupe de groupe de scene pour la segmentation finale
#define DEFout_grpGrpTreeStems "grpGrpTreeStems"
#define DEFout_grpGrpTreeClouds "grpGrpTreeClouds"
#define DEFout_itmConnectedComponentsGrid "itmConnectedComponentsGrid"

// Le groupe de groupe de cercles contient un groupe de cercles
#define DEFout_grpTreeStems "grpTreeStems"

// le groupe de cercle contient un cercle
#define DEFout_itmTreeStems "itmTreeStems"

// Le groupe de groupe de scenes contient un groupe de scenes
#define DEFout_grpTreeClouds "grpTreeClouds"

// le groupe de scenes contient une scene
#define DEFout_itmTreeClouds "itmTreeClouds"

// Constructor : initialization of parameters
ICRO_StepSplitAndMergeConnectedComponents::ICRO_StepSplitAndMergeConnectedComponents(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _intensityThreshold = 20;
    _minPixelsInComponent = 10;
    _resolution = 8;
    _nIterDilatation = 5;
    _splitType = 0;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepSplitAndMergeConnectedComponents::getStepDescription() const
{
    return tr("Split and merge de composantes connexes");
}

// Step detailled description
QString ICRO_StepSplitAndMergeConnectedComponents::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepSplitAndMergeConnectedComponents::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepSplitAndMergeConnectedComponents::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepSplitAndMergeConnectedComponents(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepSplitAndMergeConnectedComponents::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmScene, CT_Scene::staticGetType(), tr("Scene"));
}

// Creation and affiliation of OUT models
void ICRO_StepSplitAndMergeConnectedComponents::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponentNonMultis = createNewOutResultModel(DEFout_rsltResultConnectedComponentNonMultis, tr("Connected components (grid3d)"));

    // Resultat contient un groupe general
    res_rsltConnectedComponentNonMultis->setRootGroup(DEFout_grpGeneral, new CT_StandardItemGroup(), tr("TreeStems and Connected Components"));

    // Groupe general contient
    // - un groupe de groupe de cercles pour les arbres
    // - une grille 3D qui contient les composantes connexes
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGeneral, DEFout_itmConnectedComponentsGrid, new CT_Grid3D<int>(), tr("Connected Components"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpTreeStems, new CT_StandardItemGroup(), tr("Group Group Tree Stems"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpTreeClouds, new CT_StandardItemGroup(), tr("Group Group Isolated Trees"));

    // Le groupe de groupe de cercles contient un groupe de cercles
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpTreeStems, DEFout_grpTreeStems, new CT_StandardItemGroup(), tr("Stem Group"));

    // le groupe de cercle contient un cercle
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpTreeStems, DEFout_itmTreeStems, new CT_Circle(), tr("Stem locataions") );

    // Le groupe de groupe de cercles contient un groupe de cercles
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpTreeClouds, DEFout_grpTreeClouds, new CT_StandardItemGroup(), tr("Tree Group"));

    // le groupe de cercle contient un cercle
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpTreeClouds, DEFout_itmTreeClouds, new CT_Scene(), tr("Isolated Tree") );
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepSplitAndMergeConnectedComponents::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Valeur min (inutile en split intensite)", "", 0, 999999999, 4, _intensityThreshold );
    configDialog->addInt("n Min pixels","",1,999999999,_minPixelsInComponent);
    configDialog->addInt("Resolution (inutile en split intensite)","",1,999999999, _resolution);
    configDialog->addInt("nIter Dilatations","",0,999999999, _nIterDilatation);

    CT_ButtonGroup &bg_type = configDialog->addButtonGroup( _splitType );
    configDialog->addExcludeValue("","","Split par résolution",bg_type,SPLIT_SPACE_icro_stepsplitandmergeconnectedcomponents);
    configDialog->addExcludeValue("","","Plit par intensite",bg_type,SPLIT_INTENSITY_icro_stepsplitandmergeconnectedcomponents);
}

void ICRO_StepSplitAndMergeConnectedComponents::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmIntensityGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmScene);
        _intensityGrid = itemIn_itmIntensityGrid;

        if (itemIn_itmIntensityGrid != NULL && itemIn_itmScene != NULL )
        {
            // Resultat contient un groupe general
            CT_StandardItemGroup* grpGeneral = new CT_StandardItemGroup(DEFout_grpGeneral, resOut_rsltRaster3D );
            resOut_rsltRaster3D->addGroup( grpGeneral );

            // Creation de la grille contenant les composantes connexes
            CT_Grid3D<int>* itemOut_itmConnectedComponentsGrid = new CT_Grid3D<int>( DEFout_itmConnectedComponentsGrid, resOut_rsltRaster3D,
                                                                                     itemIn_itmIntensityGrid->minX(), itemIn_itmIntensityGrid->minY(), itemIn_itmIntensityGrid->minZ(),
                                                                                     itemIn_itmIntensityGrid->xArraySize(), itemIn_itmIntensityGrid->yArraySize(), itemIn_itmIntensityGrid->zArraySize(),
                                                                                     itemIn_itmIntensityGrid->resolution(),
                                                                                     std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            // Cree une image de marque utile plus tard (lors du merge)
            _markImage = new CT_Grid3D<bool>( DEFout_itmConnectedComponentsGrid, resOut_rsltRaster3D,
                                              itemIn_itmIntensityGrid->minX(), itemIn_itmIntensityGrid->minY(), itemIn_itmIntensityGrid->minZ(),
                                              itemIn_itmIntensityGrid->xArraySize(), itemIn_itmIntensityGrid->yArraySize(), itemIn_itmIntensityGrid->zArraySize(),
                                              itemIn_itmIntensityGrid->resolution(),
                                              false, false );

            // Charge les cercles a utiliser comme position de reference des troncs
            _circles = getCirclesFromFile( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );

            // Calcule la division en composantes connexes simplexes en multi resolution
            QVector<ConnectedComponent *> components;

//            labelSuperConnectedComponents( itemIn_itmIntensityGrid,
//                                           itemOut_itmConnectedComponentsGrid,
//                                           _intensityThreshold,
//                                           _resolution,
//                                           components );

            if( _splitType == SPLIT_SPACE_icro_stepsplitandmergeconnectedcomponents )
            {
                computeConnectedComponentsUntilSimplexes( itemIn_itmIntensityGrid,
                                                          itemOut_itmConnectedComponentsGrid,
                                                          _intensityThreshold,
                                                          _resolution,
                                                          _minPixelsInComponent,
                                                          _circles,
                                                          components );
            }

            else if ( _splitType == SPLIT_INTENSITY_icro_stepsplitandmergeconnectedcomponents )
            {
                splitConnectedComponentsUntilSimplexesMultiThresh( itemIn_itmIntensityGrid,
                                                                   itemOut_itmConnectedComponentsGrid,
                                                                   _minPixelsInComponent,
                                                                   _circles,
                                                                   components );
            }

            int nTotal, nNA, nAConsiderer, nComplexes, nSimplexes, nVides, nPetites;
            countComponentsByType( components,
                                   nTotal,
                                   nNA,
                                   nPetites,
                                   nAConsiderer,
                                   nVides,
                                   nSimplexes,
                                   nComplexes );
            qDebug() << "nTotal " << nTotal;
            qDebug() << "nNA " << nNA;
            qDebug() << "nAConsiderer " << nAConsiderer;
            qDebug() << "nComplexes " << nComplexes;
            qDebug() << "nSimplexes " << nSimplexes;
            qDebug() << "nVides " << nVides;
            qDebug() << "nPetites " << nPetites;

            // Labelise seulement les composantes simplexes en positif, le reste en negatif
            relabelConnectedComponents( components,
                                        itemOut_itmConnectedComponentsGrid );

            // Fusionne les composantes au besoin
            mergeConnectedComponents( components,
                                      itemOut_itmConnectedComponentsGrid,
                                      _nIterDilatation );

            // Nettoie l'image de labels pour diminuer le nombre de cubes a afficher (apres dilatations, il y a beaucoup de cubes qui appartiennent a une composante connexe et qui osnt vides)
            cleanFinalLabelGridFromIntesityGrid( itemIn_itmIntensityGrid,
                                                 itemOut_itmConnectedComponentsGrid );

            // Mise a jour des min max de la grille des composantes connexes pour affichage en couleurs
            itemOut_itmConnectedComponentsGrid->computeMinMax();
            qDebug() << "Min max des labels " << itemOut_itmConnectedComponentsGrid->dataMin() << itemOut_itmConnectedComponentsGrid->dataMax();

            // Decoupe la scene d'entree selon les labels
            // On cree autant de futures scenes que de composantes positives
            int nPositiveComponents = itemOut_itmConnectedComponentsGrid->dataMax();
            QVector< CT_PointCloudIndexVector* > componentsScenes;
            for( int i = 0 ; i < nPositiveComponents ; i++ )
            {
                componentsScenes.push_back( new CT_PointCloudIndexVector() );
            }

            // On divise la scene en fonction des labels negatifs
            splitSceneFromPositiveLabels( itemIn_itmScene,
                                          itemOut_itmConnectedComponentsGrid,
                                          componentsScenes );

            // ////////////////////////////////////////////////////////////////////////////////////////////////////
            // Ajout de la grille au resultat :
            // Le groupe general contient une grille 3D
            grpGeneral->addItemDrawable( itemOut_itmConnectedComponentsGrid );

            // Ajoute les cercles au resultat :
            // Le groupe general contient un groupe de groupe de cercles
            CT_StandardItemGroup* grpGrpCircle = new CT_StandardItemGroup(DEFout_grpGrpTreeStems, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpCircle );

            foreach( CT_CircleData* circle, _circles )
            {
                // Le groupe de groupe de cercles contient un groupe de cercles
                CT_StandardItemGroup* grpCircle = new CT_StandardItemGroup(DEFout_grpTreeStems, resOut_rsltRaster3D );
                grpGrpCircle->addGroup( grpCircle );

                // Le groupe de cercles contient un cercle
                grpCircle->addItemDrawable( new CT_Circle( DEFout_itmTreeStems, resOut_rsltRaster3D, circle ) );
            }

            // Ajoute les scenes au resultat :
            // Le groupe general contient un groupe de groupe de scenes
            CT_StandardItemGroup* grpGrpScenes = new CT_StandardItemGroup(DEFout_grpGrpTreeClouds, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpScenes );

            foreach( CT_PointCloudIndexVector* indexCloud, componentsScenes )
            {
                // Le groupe de groupe de scenes contient un groupe de scenes
                CT_StandardItemGroup* grpScenes = new CT_StandardItemGroup(DEFout_grpTreeClouds, resOut_rsltRaster3D );
                grpGrpScenes->addGroup( grpScenes );

                // Le groupe de cercles contient un cercle
                grpScenes->addItemDrawable( new CT_Scene( DEFout_itmTreeClouds, resOut_rsltRaster3D, PS_REPOSITORY->registerPointCloudIndex(indexCloud) ) );
            }
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::computeConnectedComponentsUntilSimplexes(CT_Grid3D<int> *inputGrid,
                                                                                         CT_Grid3D<int> *outConnectedComponentsGrid,
                                                                                         double minIntensity,
                                                                                         int superPixelResolution,
                                                                                         int minComponentSize,
                                                                                         QVector< CT_CircleData* >& circles,
                                                                                         QVector<ConnectedComponent *>& outConnectedComponents)
{
    // Calcule les composantes connexes a la resolution initiale et les etiquette dans la grille des composantes connexes
    QVector< ConnectedComponent* > highResolutionConnectedComponents;
    labelSuperConnectedComponents( inputGrid,
                                   outConnectedComponentsGrid,
                                   minIntensity,
                                   superPixelResolution,
                                   highResolutionConnectedComponents );


    // Calcule le nombre de cercles qui intersectent chaque composante
    getComponentsType( outConnectedComponentsGrid,
                       circles,
                       highResolutionConnectedComponents,
                       minComponentSize );

    // Lance la division recursive pour obtenir des composantes SIMPLEX
    foreach( ConnectedComponent* component, highResolutionConnectedComponents )
    {
        splitComponentUntilSimplexes( component,
                                      outConnectedComponents,
                                      _resolution,
                                      _intensityThreshold,
                                      _minPixelsInComponent,
                                      inputGrid,
                                      outConnectedComponentsGrid );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::splitConnectedComponentsUntilSimplexesMultiThresh(CT_Grid3D<int> *inputGrid,
                                                                                                  CT_Grid3D<int> *outConnectedComponentsGrid,
                                                                                                  int minComponentSize,
                                                                                                  QVector<CT_CircleData *> &circles,
                                                                                                  QVector<ConnectedComponent *> &outConnectedComponents)
{
    qDebug() << "SPLIT";
    int valueThresh = 1;
    // Calcule simplement les composantes initiales avec un seuil de 0 (tous les pixels sont pris en compte)
    QVector< ConnectedComponent* > initialConnectedComponents;
    labelSuperConnectedComponents( inputGrid,
                                   outConnectedComponentsGrid,
                                   valueThresh,
                                   1,
                                   initialConnectedComponents );

//    int negativeID = -1;
//    foreach( ConnectedComponent* component, initialConnectedComponents )
//    {
//        component->_label = negativeID;
//        markSuperComponent( component, outConnectedComponentsGrid, component->_label );
//        negativeID--;
//    }

    // Identifie le type de chaque composante
    getComponentsType( outConnectedComponentsGrid,
                       circles,
                       initialConnectedComponents,
                       minComponentSize );

    // On classe les composantes trouvees en deux : les complexes et les autres
    QVector< ConnectedComponent* > complexesConnectedComponents;
    classifyComplexComponents( initialConnectedComponents,
                               complexesConnectedComponents,
                               outConnectedComponents );

//    qDebug() << "Il y a " << complexesConnectedComponents.size() << "complexes et " << outConnectedComponents.size() << "non complexes";

    // Tant qu'il existe des composantes complexes
    while( !complexesConnectedComponents.isEmpty() )
    {
        // On monte le seuil
        valueThresh += 2;
        qDebug() << "Value Thresh " << valueThresh;

        // On essaie le split avec ce nouveau seuil
        QVector< ConnectedComponent* > splittedComponents;
        foreach( ConnectedComponent* component, complexesConnectedComponents )
        {
            assert( component->_type == COMPLEX );
            // Chaque composante est splittee
            splitComplexComponentWithThresh( component,
                                             inputGrid,
                                             outConnectedComponentsGrid,
                                             valueThresh,
                                             splittedComponents );

            qDebug() << "La composante " << component->_label << "de taille " << component->_pixels.size() << "se divise en " << splittedComponents.size() << "composantes";

            // On libere la memoire
            delete component;
        }

        // On vide la queue de complexes
        complexesConnectedComponents.clear();

        // On met a jour le type des composantes creees
        getComponentsType( outConnectedComponentsGrid,
                           circles,
                           splittedComponents,
                           minComponentSize );

        // On classe les composantes ainsi creees
        classifyComplexComponents( splittedComponents,
                                   complexesConnectedComponents,
                                   outConnectedComponents );
        qDebug() << "Il y a " << complexesConnectedComponents.size() << "complexes et " << outConnectedComponents.size() << "non complexes";
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::labelSuperConnectedComponents(CT_Grid3D<int> *inputGrid,
                                                                              CT_Grid3D<int> *outConnectedComponentsGrid,
                                                                              double minIntensity,
                                                                              int superPixelResolution,
                                                                              QVector< ConnectedComponent* >& outConnectedComponents )
{
    qDebug() << "minintensity " << minIntensity << " resolution " << superPixelResolution;
    // Calcule le nombre de super pixels dans la grille pour la resolution donnee
    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();

    int xdimSuperPixel = xdim / superPixelResolution;
    int ydimSuperPixel = ydim / superPixelResolution;
    int zdimSuperPixel = zdim / superPixelResolution;

    if( (xdim % superPixelResolution) != 0 )
    {
        xdimSuperPixel++;
    }

    if( (ydim % superPixelResolution) != 0 )
    {
        ydimSuperPixel++;
    }

    if( (zdim % superPixelResolution) != 0 )
    {
        zdimSuperPixel++;
    }

    // Recherche de composantes connexes de superpixels
    // On cherche dans tous les superpixels un superpixel qui peut servir de graine a unecomposante connexe
    for( int i = 0 ; i < xdimSuperPixel ; i++ )
    {
        for( int j = 0 ; j < ydimSuperPixel ; j++ )
        {
            for( int k = 0 ; k < zdimSuperPixel ; k++ )
            {
                SuperPixel currentPixel(i,j,k,superPixelResolution,inputGrid);

                // Si le super pixel courant contient un depart de graine pour une composante connexe
                if( currentPixel.containsNonLabeledPixelAboveValue(inputGrid,
                                                                   outConnectedComponentsGrid,
                                                                   minIntensity ) )
                {
                    // On recupere la composante connexe issue de ce super pixel
                    ConnectedComponent* currentConnectedComponent = new ConnectedComponent();
                    labelSuperConnectedComponentFromSeed( inputGrid,
                                                          outConnectedComponentsGrid,
                                                          minIntensity,
                                                          i, j, k,
                                                          ConnectedComponent::_labelID,
                                                          superPixelResolution,
                                                          currentConnectedComponent );

                    // On met a jour son label
                    currentConnectedComponent->_label = ConnectedComponent::_labelID;

                    // Et on l'ajoute a l'ensemble des composantes
                    outConnectedComponents.push_back( currentConnectedComponent );

                    // Et on avance le label de composante connexe
                    ConnectedComponent::_labelID++;
                }
            }
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *hitGrid,
                                                                                     CT_Grid3D<int> *outLabeledGrid,
                                                                                     double minValue,
                                                                                     int startx,
                                                                                     int starty,
                                                                                     int startz,
                                                                                     int label,
                                                                                     int resolution,
                                                                                     ConnectedComponent* outSuperComponent)
{
    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< SuperPixel > f;

    // WARNING : Les startx starty startz sont les coordonnees du super pixel
    SuperPixel firstSuperPixel( startx,
                                starty,
                                startz,
                                resolution,
                                hitGrid );

    // On marque le premier super pixel
    firstSuperPixel.setValueInGrid( outLabeledGrid,
                                    label );

    // Ajoute le super pixel a la composante
    outSuperComponent->_superPixels.append( firstSuperPixel );

    // Ajoute les pixels du super pixels a la composante
    addPixelsToSuperConnectedComponent( firstSuperPixel,
                                        outSuperComponent );

    // On l'ajoute a la file
    f.enqueue( firstSuperPixel );

    // Tant que la file n'est pas vide
    SuperPixel currentSuperPixel( resolution );
    while( !f.empty() )
    {
        // On prend le premier element de la file
        currentSuperPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            if( currentSuperPixel._p.x() + i >= 0 )
            {
                for( int j = -1 ; j <= 1 ; j++ )
                {
                    if( currentSuperPixel._p.y() + j >= 0 )
                    {
                        for( int k = -1 ; k <= 1 ; k++ )
                        {
                            if( currentSuperPixel._p.z() + k >= 0 )
                            {
                                SuperPixel neiSuperPixel( currentSuperPixel._p.x() + i,
                                                          currentSuperPixel._p.y() + j,
                                                          currentSuperPixel._p.z() + k,
                                                          resolution,
                                                          hitGrid );

                                // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                                // Si le voisin n'est pas deja marque et si il a une valeur superieure a la valeur minimum souhaitee
                                if( neiSuperPixel.containsNonLabeledPixelAboveValue( hitGrid,
                                                                                     outLabeledGrid,
                                                                                     minValue ) )
                                {
                                    // Marque le super pixel
                                    neiSuperPixel.setValueInGrid( outLabeledGrid,
                                                                  label );

                                    // Ajoute le super pixel a la composante
                                    outSuperComponent->_superPixels.append( neiSuperPixel );

                                    // Ajoute les pixels a la composante
                                    addPixelsToSuperConnectedComponent( neiSuperPixel,
                                                                        outSuperComponent );

                                    // Ajoute le super pixel a al file
                                    f.enqueue( neiSuperPixel );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::markSuperComponent(ConnectedComponent *superComponent,
                                                                                CT_Grid3D<int> *grid,
                                                                                int label)
{
    foreach( SuperPixel p, superComponent->_superPixels )
    {
        p.setValueInGrid( grid, label );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::updateComponentsType(QVector<ConnectedComponent *> &components,
                                                                     int minComponentSize)
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minComponentSize )
        {
            component->_type = TOO_SMALL;
        }

        else
        {
            if( component->_nCircles == 0 )
            {
                component->_type = EMPTY;
            }

            else if( component->_nCircles == 1 )
            {
                component->_type = SIMPLEX;
            }

            else if( component->_nCircles >= 2 )
            {
                component->_type = COMPLEX;
            }
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::addPixelsToSuperConnectedComponent(SuperPixel& superPixel,
                                                                                                ConnectedComponent *outSuperComponent )
{
    // Recupere les pixels du super pixel
    QVector< Eigen::Vector3i > pixelsFromSuperPixel;
    superPixel.getPixels( pixelsFromSuperPixel );

    // Ajoute le vecteur de pixels
    outSuperComponent->_pixels << pixelsFromSuperPixel;
}

ConnectedComponent *ICRO_StepSplitAndMergeConnectedComponents::getComponentFromLabel(QVector<ConnectedComponent *> &components,
                                                                                                  int label )
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_label == label )
        {
            return component;
        }
    }

    return NULL;
}

void ICRO_StepSplitAndMergeConnectedComponents::getComponentsType(CT_Grid3D<int> *labeledGrid,
                                                                  QVector< CT_CircleData* >& circles,
                                                                  QVector< ConnectedComponent* >& components,
                                                                  int minLabelSize )
{
    // Attribue le type TOO_SMALL
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minLabelSize )
        {
            component->_type = TOO_SMALL;
        }
    }

    // Pour chaque cercle
    foreach( CT_CircleData* circleData, circles )
    {
        // Labels traverses par le cercle
        QVector<int> labelsOfCircle;

        // On regarde si le cercle touche un pixel de la composante courante
        // BBox du cercle dans la grille
        size_t botx, boty, botz;
        labeledGrid->colX( circleData->getCenter().x() - circleData->getRadius(), botx );
        labeledGrid->linY( circleData->getCenter().y() - circleData->getRadius(), boty );
        labeledGrid->levelZ( circleData->getCenter().z(), botz );

        size_t topx, topy, topz;
        labeledGrid->colX( circleData->getCenter().x() + circleData->getRadius(), topx );
        labeledGrid->linY( circleData->getCenter().y() + circleData->getRadius(), topy );
        labeledGrid->levelZ( circleData->getCenter().z(), topz );

        // Pour chaque pixel qu'intersecte le cercle courant
        for( int i = botx ; i <= topx ; i++ )
        {
            for( int j = boty ; j <= topy ; j++ )
            {
                for( int k = botz ; k <= topz ; k++ )
                {
                    // Si une etiquette coupe le cercle : la valeur a ete etiquettee (different de NA)
                    if( labeledGrid->value(i,j,k) != labeledGrid->NA() )
                    {
                        ConnectedComponent* fromLabel = getComponentFromLabel( components, labeledGrid->value(i,j,k) );

                        // Si la composante n'est pas trop petite
                        if( fromLabel != NULL && fromLabel->_type != TOO_SMALL )
                        {
                            // Et qu'on ne l'avait pas encore traversee pour ce cercle
                            if( !labelsOfCircle.contains( labeledGrid->value(i,j,k) ) )
                            {
                                // On recupere la composante associee
                                ConnectedComponent* component = getComponentFromLabel( components,
                                                                                       labeledGrid->value(i,j,k) );

                                if( component != NULL )
                                {
                                    // On l'ajoute au tableau des composantes traversees
                                    labelsOfCircle.push_back( labeledGrid->value(i,j,k) );

                                    // La composante intersecte un cercle de plus
                                    component->_nCircles++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // Mise a jour des types des composantes en fonction du nombre de cercles qui intersectent
    updateComponentsType( components, _minPixelsInComponent );
}

void ICRO_StepSplitAndMergeConnectedComponents::splitComplexComponent(ConnectedComponent* componentToBeSplitted,
                                                                      CT_Grid3D<int> *valueGrid,
                                                                      CT_Grid3D<int> *labelGrid,
                                                                      int initialResolution,
                                                                      int minValue,
                                                                      QVector< ConnectedComponent* >& outComponents )
{
    // Si les super pixels sont divisibles
    if( initialResolution > 1 )
    {
        // On clear les labels de la composante
        markSuperComponent( componentToBeSplitted,
                            labelGrid,
                            labelGrid->NA() );

        // On transforme chaque super pixel de la composante en super pixel de resolution plus fine
        int currentResolution = initialResolution / 2;
        QVector< SuperPixel > superPixelsAtCurrentResolution;
        foreach( SuperPixel p, componentToBeSplitted->_superPixels )
        {
            // Chaque super pixel se transforme en huit super pixels de resolution plus fine
            for( int i = 0 ; i < 2 ; i++ )
            {
                for( int j = 0 ; j < 2 ; j++ )
                {
                    for( int k = 0 ; k < 2 ; k++ )
                    {
                        SuperPixel subSuperPixel( 2*p._p.x() + i,
                                                  2*p._p.y() + j,
                                                  2*p._p.z() + k,
                                                  currentResolution,
                                                  valueGrid );
                        superPixelsAtCurrentResolution.append( subSuperPixel );
                    }
                }
            }
        }

        // On remplace le tableau de super pixels
        componentToBeSplitted->_superPixels.clear();
        componentToBeSplitted->_superPixels = superPixelsAtCurrentResolution;

        // ////////////////////////////////////////////////////////////////////////////////
        // On lance une recherche de composantes connexes a partir de cette resolution
        // ////////////////////////////////////////////////////////////////////////////////
        foreach( SuperPixel sp, componentToBeSplitted->_superPixels )
        {
            if( sp.containsNonLabeledPixelAboveValue( valueGrid, labelGrid, minValue ) )
            {
                ConnectedComponent* connectedComponentAtCurrentResolution = new ConnectedComponent();
                labelSuperConnectedComponentFromSeed( valueGrid,
                                                      labelGrid,
                                                      minValue,
                                                      sp._p.x(), sp._p.y(), sp._p.z(),
                                                      ConnectedComponent::_labelID,
                                                      currentResolution,
                                                      connectedComponentAtCurrentResolution );

                    // On l'ajoute a l'ensemble des composantes
                    outComponents.push_back( connectedComponentAtCurrentResolution );

                    // On met a jour son label
                    connectedComponentAtCurrentResolution->_label = ConnectedComponent::_labelID;

                    // Et on avance le label
                    ConnectedComponent::_labelID++;
            }
        }
    }

    else
    {
        // La resolution est de 1 donc on renvoie la composante telle quelle (on est deja a l'echelle du pixel)
        outComponents.push_back( componentToBeSplitted );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::splitComplexComponentWithThresh(ConnectedComponent *componentToBeSplitted,
                                                                                CT_Grid3D<int> *valueGrid,
                                                                                CT_Grid3D<int> *labelGrid,
                                                                                int thresh,
                                                                                QVector<ConnectedComponent *> &outComponents)
{
    // On clear les labels de la composante
    markSuperComponent( componentToBeSplitted,
                        labelGrid,
                        labelGrid->NA() );

    // ////////////////////////////////////////////////////////////////////////////////
    // On lance une recherche de composantes connexes a partir de cette resolution
    // ////////////////////////////////////////////////////////////////////////////////
    foreach( SuperPixel sp, componentToBeSplitted->_superPixels )
    {
        if( sp.containsNonLabeledPixelAboveValue( valueGrid,
                                                  labelGrid,
                                                  thresh ) )
        {
            ConnectedComponent* connectedComponentAtCurrentResolution = new ConnectedComponent();
            labelSuperConnectedComponentFromSeed( valueGrid,
                                                  labelGrid,
                                                  thresh,
                                                  sp._p.x(), sp._p.y(), sp._p.z(),
                                                  ConnectedComponent::_labelID,
                                                  1,
                                                  connectedComponentAtCurrentResolution );

            // On l'ajoute a l'ensemble des composantes
            outComponents.push_back( connectedComponentAtCurrentResolution );

            // On met a jour son label
            connectedComponentAtCurrentResolution->_label = ConnectedComponent::_labelID;

            // Et on avance le label
            ConnectedComponent::_labelID++;
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::splitComponentUntilSimplexes(ConnectedComponent* componentToBeSplitted,
                                                                             QVector<ConnectedComponent *>& outSplittedComponents,
                                                                             int initialResolution,
                                                                             int minValue,
                                                                             int minLabelSize,
                                                                             CT_Grid3D<int>* intesityGrid,
                                                                             CT_Grid3D<int>* labelGrid )
{
    // Si que la composante est complexe
    if( componentToBeSplitted->_type == COMPLEX )
    {
        // Et que les superpixels contiennent plus d'un super pixel (sinon on est deja au niveau du pixel, on ne peut pas descendre plus bas)
        if( initialResolution > 1 )
        {
            // On divise la composante complexe mere et on recupere les composantes filles resultantes
            QVector<ConnectedComponent *> splitedComponentsAtCurrentResolution;
            splitComplexComponent( componentToBeSplitted,
                                   intesityGrid,
                                   labelGrid,
                                   initialResolution,
                                   minValue,
                                   splitedComponentsAtCurrentResolution );

            // On rattache les composantes filles a leur mere
            foreach( ConnectedComponent* splitedComponentAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                componentToBeSplitted->_children.push_back( splitedComponentAtCurrentResolution );
            }

            // Calcule le nombre de cercles qui intersectent chaque composante
            getComponentsType( labelGrid,
                               _circles,
                               splitedComponentsAtCurrentResolution,
                               _minPixelsInComponent );

            // Pour chaque composante creee par la division on continue le processus
            foreach( ConnectedComponent* componentToBeSplittedAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                splitComponentUntilSimplexes( componentToBeSplittedAtCurrentResolution,
                                              outSplittedComponents,
                                              initialResolution / 2,
                                              minValue,
                                              minLabelSize,
                                              intesityGrid,
                                              labelGrid );
            }
        }

        // Si la composante est complexe mais que l'on ne peut plus reduire la resolution
        else
        {
            // On ajoute simplement la composante au resultat
            componentToBeSplitted->_resolutionFinale = initialResolution;
            outSplittedComponents.push_back( componentToBeSplitted );
        }
    }

    // Si la composante est VIDE ou SIMPLEX (les TOO_SMALL ne sont pas traitees ici car elles n'apparaissent pas dans le tableau de composantes connexes)
    else
    {
        // On l'ajoute a la sortie
        componentToBeSplitted->_resolutionFinale = initialResolution;
        outSplittedComponents.push_back( componentToBeSplitted );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::mergeConnectedComponents(QVector<ConnectedComponent *> &connectedComponents,
                                                                         CT_Grid3D<int> *labelGrid,
                                                                         int nIterations)
{
    // Cree et initialisa la grille de la transformee en distance
    CT_Grid3D<int>* distanceGrid = new CT_Grid3D<int>( NULL, NULL,
                                                       labelGrid->minX(), labelGrid->minY(), labelGrid->minZ(),
                                                       labelGrid->xArraySize(), labelGrid->yArraySize(), labelGrid->zArraySize(),
                                                       labelGrid->resolution(),
                                                       std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );
//    qDebug() << "Init distance";
    initDistanceGrid( connectedComponents,
                      distanceGrid );
//    qDebug() << "Init distance done";

    // Commence les croissances iteratives
    for( int i = 0 ; i < nIterations ; i++ )
    {
//        qDebug() << "Iteration " << i;
        // Chaque composante connexe SIMPLEXE doit croitre
        // Donc on recherche parmis toutes les composantes existantes
        foreach( ConnectedComponent* component, connectedComponents )
        {
            assert( component!= NULL );
            // Celles qui sont simplex (qui ont pu en aggreger d'autres deja)
            if( component->_type == SIMPLEX )
            {
                assert( component!= NULL );
                // Et on les fait grossir en calculant la distance des bords
                dilateConnectedComponentForDistance( component,
                                                     labelGrid,
                                                     distanceGrid,
                                                     connectedComponents );
            }
        }

        // Apres l'etape ci dessus, on n'a modifie que la grille de distance :
        // Les valeurs negatives sont les distances calculees a cette iteration ci
        // La valeur -NA indique qu'il y a un conflit entre plusieurs composantes connexes et qu'il faut donclaisser tomber ce pixelpar la suite
        // Pour les pixels non conflictuels, il faut repasser les distances en positif et etiquetter avec une composante connexe
        foreach( ConnectedComponent* component, connectedComponents )
        {
            // Celles qui sont simplex (qui ont pu en aggreger d'autres deja)
            if( component->_type == SIMPLEX )
            {
                updateConnectedComponentBordersDistancesAndLabelAfterDilatation( component,
                                                                                 labelGrid,
                                                                                 distanceGrid,
                                                                                 connectedComponents );
            }
        }
//        qDebug() << "Termine iteration" << i;
    }

    // TEMPORAIRE
    // Tous les pixels en conflit (donc qui ont une distance de -NA) se voient attribuer un label unique
    int xdim = labelGrid->xArraySize();
    int ydim = labelGrid->yArraySize();
    int zdim = labelGrid->zArraySize();
    int cpt = 0;
    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                if( distanceGrid->value(x,y,z) == -distanceGrid->NA() )
                {
                    labelGrid->setValue( x,y,z, 0 );
                    cpt++;
                }
            }
        }
    }
//    qDebug() << "Il y a " << cpt << "conflits";
    // TEMPORAIRE FIN

    // Libere la memoire
    delete distanceGrid;
}

void ICRO_StepSplitAndMergeConnectedComponents::initDistanceGrid(QVector<ConnectedComponent *> &connectedComponents,
                                                                 CT_Grid3D<int> *distanceGrid)
{
    foreach( ConnectedComponent* component, connectedComponents )
    {
        if( component->_type == SIMPLEX )
        {
            markSuperComponent( component,
                                distanceGrid,
                                0 );
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::getExternalPixelsOfConnectedComponent(CT_Grid3D<int> *labelGrid,
                                                                                      CT_Grid3D<bool> *markGrid,
                                                                                      ConnectedComponent *component,
                                                                                      QVector<Pixel> &outPixels)
{
    assert( component != NULL );
    assert( labelGrid != NULL );
    assert( markGrid != NULL );
//    qDebug() << "Debut external";
    int xdim = labelGrid->xArraySize();
//    qDebug() << "dim1 ok";
    int ydim = labelGrid->yArraySize();
//    qDebug() << "dim1 ok";
    int zdim = labelGrid->zArraySize();
//    qDebug() << "dim1 ok";

//    int xdim2 = markGrid->xArraySize();
//    qDebug() << "dim2 ok";
//    int ydim2 = markGrid->yArraySize();
//    qDebug() << "dim2 ok";
//    int zdim2 = markGrid->zArraySize();
//    qDebug() << "dim2 ok";
//    assert( xdim == xdim2 );
//    assert( ydim == ydim2 );
//    assert( zdim == zdim2 );
//    qDebug() << "Tailles verifiees";

    outPixels.clear();
//    qDebug() << "Tableau vide";
//    qDebug() << "External go";
    foreach( Pixel p, component->_pixels )
    {
//        qDebug() << " --";
        // Pour tous les voisins
        int botx = std::max(0,p.x()-1);
        int boty = std::max(0,p.y()-1);
        int botz = std::max(0,p.z()-1);
        int topx = std::min(xdim-1,p.x()+1);
        int topy = std::min(ydim-1,p.y()+1);
        int topz = std::min(zdim-1,p.z()+1);
        for( int ii = botx ; ii <= topx ; ii++ )
        {
            for( int jj = boty ; jj <= topy ; jj++ )
            {
                for( int kk = botz ; kk <= topz ; kk++ )
                {
//                    qDebug() << "0";
                    // SI le pixel appartient a une autre composante et n'a pas deja ete marque
                    if( labelGrid->value( ii,jj,kk ) != component->_label /*&& markGrid->value( ii,jj,kk ) == false*/ )
                    {
//                        qDebug() << "10";
                        if( markGrid->value( ii,jj,kk ) == false )
                        {
//                            qDebug() << "1";
                            // On marque le pixel et on l'ajoute au vecteur de sortie
                            markGrid->setValue( ii, jj, kk, true );
//                            qDebug() << "2";
                            Pixel n; n << ii, jj, kk;
//                            qDebug() << "3";
                            outPixels.push_back( n );
//                            qDebug() << "4";
                        }
                    }
                }
            }
        }
    }
//    qDebug() << "External ok";

    // On demarque les pixels de la grille de marquage
    foreach( Pixel p, outPixels )
    {
        markGrid->setValue( p.x(), p.y(), p.z(), false );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::dilateConnectedComponentForDistance(ConnectedComponent *component,
                                                                                    CT_Grid3D<int> *labelGrid,
                                                                                    CT_Grid3D<int> *distanceGrid,
                                                                                    QVector< ConnectedComponent* >& allComponents )
{
//    qDebug() << "Pixel bord";
    assert( component != NULL );
    // On recupere les pixels de bords de la composante
    QVector< Pixel > borderPixels;
    getExternalPixelsOfConnectedComponent( labelGrid,
                                           _markImage,
                                           component,
                                           borderPixels);
//    qDebug() << "Pixel bord ok";

    int xdim = labelGrid->xArraySize();
    int ydim = labelGrid->yArraySize();
    int zdim = labelGrid->zArraySize();

    // Pour chaque pixel de bord on va regarder la distance minimale a la composante connexe
    // Et si il y a conflit avec une autre composante connexe
//    qDebug() << "Start border";
    foreach( Pixel p, borderPixels )
    {
        // Si le pixel de bord appartient a une composante deja dectee (non simple)
        if( labelGrid->value( p.x(), p.y(), p.z() ) < 0 )
        {
//            qDebug() << "Pixel deja non simple" << labelGrid->value( p.x(), p.y(), p.z() );
            ConnectedComponent* touchedComponent = getComponentFromLabel( allComponents, labelGrid->value( p.x(), p.y(), p.z() ));
//            qDebug() << "Composante recuperee";
            assert( touchedComponent != NULL );
            if( touchedComponent->_type != COMPLEX )
            {
                // On regarde la distance de la composante mere a la composante fille

                // BBox du voisinage
                int botx = std::max(0,p.x()-1);
                int boty = std::max(0,p.y()-1);
                int botz = std::max(0,p.z()-1);
                int topx = std::min(xdim-1,p.x()+1);
                int topy = std::min(ydim-1,p.y()+1);
                int topz = std::min(zdim-1,p.z()+1);

                // On doit regarder le min dans le voisinage de p
                int minDistance = std::numeric_limits<int>::max();

                // Pour tous les voisins
                for( int ii = botx ; ii <= topx ; ii++ )
                {
                    for( int jj = boty ; jj <= topy ; jj++ )
                    {
                        for( int kk = botz ; kk <= topz ; kk++ )
                        {
                            // Si un pixel a le meme label de composante connexe
                            if( labelGrid->value(ii,jj,kk) == component->_label )
                            {
                                if( distanceGrid->value(ii,jj,kk) < minDistance )
                                {
                                    minDistance = distanceGrid->value(ii,jj,kk);
                                }
                            }
                        }
                    }
                }

                // On inscrit la distance du pixel en negatif (pour dire que cette distance a ete calculee ce tour de croissance ci
                // La distance du pixel est egale au minimum trouve plus 1
                distanceGrid->setValue( p.x(), p.y(), p.z(), -( minDistance+1 ) );

                // On recupere la composante atteinte
                ConnectedComponent* reachedComponent = getComponentFromLabel( allComponents, labelGrid->value( p.x(), p.y(), p.z() ) );

                // Et on met a jour ses potentiels agregats avec la composantemere courante et la distance trouvee
                reachedComponent->updatePotentialAgregate( component,
                                                           minDistance );

            }

//            // Sinon, c'etait un pixel isole (composante a un unique pixel)
//            else
//            {
//                // Si une distance a deja ete calculee pour ce pixel a ce tour (negatif donc puisque NA == INT_MAX)
//                // Ou a un tour precedent (car on a mis la valeur -NA donc une valeur negative egalement)
//                if( distanceGrid->value( p.x(), p.y(), p.z() ) < 0 )
//                {
//                    // Alors on met la distance a -NA pour dire qu'on ne s'en occupe plus
//                    distanceGrid->setValue( p.x(), p.y(), p.z(), -distanceGrid->NA() );
//                }

//                // Sinon, la croissance est possible
//                else
//                {
//                    qDebug() << "Croissance possible";
//                    // Que si le pixel n'appartenait pas deja a une composante simple (composantes simple ont un label > 0)
//                    if( labelGrid->value( p.x(), p.y(), p.z() ) < 0 ||
//                        labelGrid->value( p.x(), p.y(), p.z() ) == labelGrid->NA()
//                      )
//                    {
//                        qDebug() << "Ajout";
//                        // BBox du voisinage
//                        int botx = std::max(0,p.x()-1);
//                        int boty = std::max(0,p.y()-1);
//                        int botz = std::max(0,p.z()-1);
//                        int topx = std::min(xdim-1,p.x()+1);
//                        int topy = std::min(ydim-1,p.y()+1);
//                        int topz = std::min(zdim-1,p.z()+1);

//                        // On doit regarder le min dans le voisinage de p
//                        int minDistance = std::numeric_limits<int>::max();

//                        // Pour tous les voisins
//                        for( int ii = botx ; ii <= topx ; ii++ )
//                        {
//                            for( int jj = boty ; jj <= topy ; jj++ )
//                            {
//                                for( int kk = botz ; kk <= topz ; kk++ )
//                                {
//                                    // Si un pixel a le meme label de composante connexe
//                                    if( labelGrid->value(ii,jj,kk) == component->_label )
//                                    {
//                                        if( distanceGrid->value(ii,jj,kk) < minDistance )
//                                        {
//                                            minDistance = distanceGrid->value(ii,jj,kk);
//                                        }
//                                    }
//                                }
//                            }
//                        }

//                        // On inscrit la distance du pixel en negatif (pour dire que cette distance a ete calculee ce tour de croissance ci
//                        // La distance du pixel est egale au minimum trouve plus 1
//                        distanceGrid->setValue( p.x(), p.y(), p.z(), -( minDistance+1 ) );
//                    }

//                    else
//                    {
//                        qDebug() << "Refus";
//                    }
//                }
//            }
        }
        // Sinon, c'etait un pixel isole (composante a un unique pixel)
        else
        {
            // Si une distance a deja ete calculee pour ce pixel a ce tour (negatif donc puisque NA == INT_MAX)
            // Ou a un tour precedent (car on a mis la valeur -NA donc une valeur negative egalement)
            if( distanceGrid->value( p.x(), p.y(), p.z() ) < 0 )
            {
                // Alors on met la distance a -NA pour dire qu'on ne s'en occupe plus
                distanceGrid->setValue( p.x(), p.y(), p.z(), -distanceGrid->NA() );
            }

            // Sinon, la croissance est possible
            else
            {
//                qDebug() << "Croissance possible";
                // Que si le pixel n'appartenait pas deja a une composante simple (composantes simple ont un label > 0)
                if( labelGrid->value( p.x(), p.y(), p.z() ) < 0 ||
                    labelGrid->value( p.x(), p.y(), p.z() ) == labelGrid->NA()
                  )
                {
//                    qDebug() << "Ajout";
                    // BBox du voisinage
                    int botx = std::max(0,p.x()-1);
                    int boty = std::max(0,p.y()-1);
                    int botz = std::max(0,p.z()-1);
                    int topx = std::min(xdim-1,p.x()+1);
                    int topy = std::min(ydim-1,p.y()+1);
                    int topz = std::min(zdim-1,p.z()+1);

                    // On doit regarder le min dans le voisinage de p
                    int minDistance = std::numeric_limits<int>::max();

                    // Pour tous les voisins
                    for( int ii = botx ; ii <= topx ; ii++ )
                    {
                        for( int jj = boty ; jj <= topy ; jj++ )
                        {
                            for( int kk = botz ; kk <= topz ; kk++ )
                            {
                                // Si un pixel a le meme label de composante connexe
                                if( labelGrid->value(ii,jj,kk) == component->_label )
                                {
                                    if( distanceGrid->value(ii,jj,kk) < minDistance )
                                    {
                                        minDistance = distanceGrid->value(ii,jj,kk);
                                    }
                                }
                            }
                        }
                    }

                    // On inscrit la distance du pixel en negatif (pour dire que cette distance a ete calculee ce tour de croissance ci
                    // La distance du pixel est egale au minimum trouve plus 1
                    distanceGrid->setValue( p.x(), p.y(), p.z(), -( minDistance+1 ) );
                }

//                else
//                {
//                    qDebug() << "Refus";
//                }
            }
        }
    }
//    qDebug() << "Start border ok";
}

void ICRO_StepSplitAndMergeConnectedComponents::updateConnectedComponentBordersDistancesAndLabelAfterDilatation(ConnectedComponent *component,
                                                                                                                CT_Grid3D<int> *labelGrid,
                                                                                                                CT_Grid3D<int> *distanceGrid,
                                                                                                                QVector< ConnectedComponent* >& allComponents )
{
    QVector< Pixel > borderPixels;
    getExternalPixelsOfConnectedComponent( labelGrid,
                                           _markImage,
                                           component,
                                           borderPixels);

    foreach( Pixel p, borderPixels )
    {
        // Si la distance du pixel au tour courant (<0) est differente de -NA alors il n'y a pas de conflit
        if( distanceGrid->value( p.x(), p.y(), p.z() ) != -distanceGrid->NA() &&
            distanceGrid->value( p.x(), p.y(), p.z() ) < 0 &&
            distanceGrid->value( p.x(), p.y(), p.z() ) != distanceGrid->NA() )
        {
            // Deux cas peuvent se presenter :
            // 1 - Le pixel atteint n'appartien pas a une composante connexe deja detectee
            if( labelGrid->value(p.x(), p.y(), p.z()) == labelGrid->NA() )
            {
                // On ajoute simplement ce pixel a la composante simplexe mere
                // Il faut alors mettre a jour la distance qui a ete marquee en negatif
                // Et l'etiquette de composante connexe
                // Et il faut ajouter le pixel a la composante connexe
                distanceGrid->setValue( p.x(), p.y(), p.z(), abs( distanceGrid->value( p.x(), p.y(), p.z() ) ) );
                labelGrid->setValue( p.x(), p.y(), p.z(), component->_label );
                component->_pixels.push_back( p );
            }

            // 2 - Le pixel atteint fait partie d'une composante connexe deja detectee
            else
            {
                assert( labelGrid->value(p.x(), p.y(), p.z()) < 0 );

                // Il faut alors regarder si d'autres composantes mere ont atteint cette meme composante fille
                ConnectedComponent* componentToAgregate = getComponentFromLabel( allComponents, labelGrid->value(p.x(), p.y(), p.z()) );
                ConnectedComponent* aggregativeComponent = componentToAgregate->getComponentToAgregateWith();

                // Si il existe un conflit (deux composantes atteignent la composante fille en meme temps)
                if( aggregativeComponent == NULL )
                {
                    // Alors on n'agrege rien et on laisse la composante fille a -NA pour ne pas s'en occuper
                    // Les dernieres agregations se feront plus tard
                    markSuperComponent( componentToAgregate,
                                        distanceGrid,
                                        -distanceGrid->NA() );
                    markSuperComponent( componentToAgregate,
                                        labelGrid,
                                        0 );
                }

                // Si il n'y a pas de conflits
                else
                {
                    // Si la composante courante est celle qui doit agreger la composante fille
                    if( component->_label == aggregativeComponent->_label )
                    {
                        // Pour chaque pixel de la composante fille a agreger
                        foreach( Pixel np, componentToAgregate->_pixels )
                        {
                            // On ajoute ce pixel a la composante simplexe mere
                            // Il faut alors mettre a jour la distance qui a ete marquee en negatif
                            // Et l'etiquette de composante connexe
                            // Et il faut ajouter le pixel a la composante connexe
                            component->_pixels.push_back( np );
                            distanceGrid->setValue( np.x(), np.y(), np.z(), 0 );
                            labelGrid->setValue( np.x(), np.y(), np.z(), component->_label );
                        }
                    }
                }
            }
        }
    }
}

QVector<CT_CircleData *> ICRO_StepSplitAndMergeConnectedComponents::getCirclesFromFile(QString dir, QString fileName)
{
    QVector<CT_CircleData*> rslt;
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            CT_CircleData* currCircle = new CT_CircleData( center, direction, r );

            if( circleIntersect2D( rslt, currCircle ) )
            {
                delete currCircle;
            }

            else
            {
                rslt.push_back( currCircle );
            }

        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }

    qDebug() << "Loaded " << rslt.size() << " cercles";
    return rslt;
}

bool ICRO_StepSplitAndMergeConnectedComponents::circleIntersect2D(QVector<CT_CircleData *> &circles, CT_CircleData *c)
{
    foreach( CT_CircleData* curr, circles )
    {
        if( circleIntersect2D( curr, c ) )
        {
            return true;
        }
    }

    return false;
}

bool ICRO_StepSplitAndMergeConnectedComponents::circleIntersect2D(CT_CircleData *c1, CT_CircleData *c2)
{
    CT_Point ce1 = c1->getCenter();
    CT_Point ce2 = c2->getCenter();
    float dist2D = sqrt( (ce1.x()-ce2.x())*(ce1.x()-ce2.x()) + (ce1.y()-ce2.y())*(ce1.y()-ce2.y()) );

    if( dist2D < (c1->getRadius() + c2->getRadius()) )
    {
        return true;
    }

    return false;
}

void ICRO_StepSplitAndMergeConnectedComponents::cleanFinalLabelGridFromIntesityGrid(CT_Grid3D<int> *intensityGrid,
                                                                                    CT_Grid3D<int> *labelGrid)
{
    int xdim = intensityGrid->xArraySize();
    int ydim = intensityGrid->yArraySize();
    int zdim = intensityGrid->zArraySize();

    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                if( intensityGrid->value(x,y,z) == 0 && labelGrid->value(x,y,z) != 0 )
                {
                    labelGrid->setValue( x,y,z, labelGrid->NA() );
                }
            }
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::countComponentsByType(QVector<ConnectedComponent *> &components,
                                                                      int &outNTotal,
                                                                      int &outNNA,
                                                                      int &outNTooSmall,
                                                                      int &outNBigEnough,
                                                                      int &outNEmpty,
                                                                      int &outNSimplex,
                                                                      int &outNComplex)
{
    {
        outNTotal = components.size();
        outNTooSmall = 0;
        outNBigEnough = 0;
        outNEmpty = 0;
        outNSimplex = 0;
        outNComplex = 0;
        outNNA = 0;

        foreach( ConnectedComponent* component, components )
        {
            switch( component->_type )
            {
                case NA :
                {
                    outNNA++;
                    break;
                }
                case TOO_SMALL :
                {
                    outNTooSmall++;
                    break;
                }
                case EMPTY :
                {
                    outNEmpty++;
                    break;
                }
                case SIMPLEX :
                {
                    outNSimplex++;
                    break;
                }
                case COMPLEX :
                {
                    outNComplex++;
                    break;
                }
            }
        }

        outNBigEnough = outNTotal - outNTooSmall;
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::relabelConnectedComponents(QVector< ConnectedComponent* >& components,
                                                                           CT_Grid3D<int> *labelGrid)
{
    // Toutes les composantes non simplexes se voient attribuer un label negatif
    int newIdPositive = 1;
    int newIdNegative = -1;
    foreach( ConnectedComponent* component, components )
    {
        if( component->_type != SIMPLEX )
        {
            component->_label = newIdNegative;
            newIdNegative--;
        }
        else
        {
            component->_label = newIdPositive;
            newIdPositive++;
        }
//        else if( component->_type == SIMPLEX )
//        {
//            component->_label = newIdPositive;
//            newIdPositive++;
//        }
//        else if( component->_type == EMPTY )
//        {
//            component->_label = 0;
//        }
//        else if( component->_type == TOO_SMALL )
//        {
//            component->_label = 0;
//        }
        markSuperComponent( component,
                            labelGrid,
                            component->_label );
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::splitSceneFromPositiveLabels(const CT_Scene *inputScene,
                                                                             CT_Grid3D<int> *labelGrid,
                                                                             QVector<CT_PointCloudIndexVector *> outScenes)
{
    CT_PointIterator itP(inputScene->getPointCloudIndex());
    while(itP.hasNext())
    {
        const CT_Point &point = itP.next().currentPoint();
        size_t index = itP.currentGlobalIndex();

        if( labelGrid->valueAtXYZ( point.x(), point.y(), point.z() ) > 0 && labelGrid->valueAtXYZ( point.x(), point.y(), point.z() ) != labelGrid->NA() )
        {
            outScenes[ labelGrid->valueAtXYZ( point.x(), point.y(), point.z() )-1 ]->addIndex(index);
        }
    }
}

bool ICRO_StepSplitAndMergeConnectedComponents::existsComplexComponents(QVector<ConnectedComponent *> &connectedComponents)
{
    foreach( ConnectedComponent* component, connectedComponents )
    {
        if( component->_type == COMPLEX )
        {
            return true;
        }
    }

    return false;
}

void ICRO_StepSplitAndMergeConnectedComponents::classifyComplexComponents(QVector<ConnectedComponent *> &inputConnectedComponents,
                                                                          QVector<ConnectedComponent *> &outputComplexesConnectedComponents,
                                                                          QVector<ConnectedComponent *> &outputNonComplexesConnectedComponents)
{
    foreach( ConnectedComponent* component, inputConnectedComponents )
    {
        if( component->_type == COMPLEX )
        {
            outputComplexesConnectedComponents.push_back( component );
        }
        else
        {
            outputNonComplexesConnectedComponents.push_back( component );
        }
    }
}

void ICRO_StepSplitAndMergeConnectedComponents::computeDistanceFromConnectedComponent(ConnectedComponent *component,
                                                                                      Eigen::MatrixXi *outDistances,
                                                                                      CT_Grid3D<int> *labelGrid,
                                                                                      CT_Grid3D<bool> *markGrid)
{
    // On recupere les pixels de bords de la composante
    QVector< Pixel > borderPixels;
    getExternalPixelsOfConnectedComponent( labelGrid,
                                           markGrid,
                                           component,
                                           borderPixels);
}
