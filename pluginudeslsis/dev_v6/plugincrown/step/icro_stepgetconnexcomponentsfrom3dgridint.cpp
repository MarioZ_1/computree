#include "icro_stepgetconnexcomponentsfrom3dgridint.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include <QQueue>

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltResultConnectedComponents "rsltResultConnectedComponents"
#define DEFout_grpGrid3d "grpGrid3d"
#define DEFout_itmGrid3d "itmGrid3d"


// Constructor : initialization of parameters
ICRO_StepGetConnexComponentsFrom3dGridInt::ICRO_StepGetConnexComponentsFrom3dGridInt(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minValue = 100;
    _minPixelsInComponent = 100;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetConnexComponentsFrom3dGridInt::getStepDescription() const
{
    return tr("Identifie les composantes connexes d'une grille 3D separes par une valeur minimum (int)");
}

// Step detailled description
QString ICRO_StepGetConnexComponentsFrom3dGridInt::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepGetConnexComponentsFrom3dGridInt::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetConnexComponentsFrom3dGridInt::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetConnexComponentsFrom3dGridInt(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetConnexComponentsFrom3dGridInt::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3Rasterd = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3Rasterd->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3Rasterd->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));

}

// Creation and affiliation of OUT models
void ICRO_StepGetConnexComponentsFrom3dGridInt::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponents = createNewOutResultModel(DEFout_rsltResultConnectedComponents, tr("Connected components (grid3d)"));
    res_rsltConnectedComponents->setRootGroup(DEFout_grpGrid3d, new CT_StandardItemGroup(), tr("Group of grid3d"));
    res_rsltConnectedComponents->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3d, new CT_Grid3D<int>(), tr("Grid3d with labeled components"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetConnexComponentsFrom3dGridInt::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Valeur min", "", 0, 9999, 4, _minValue );
    configDialog->addInt("n Min pixels","",1,999999999,_minPixelsInComponent);
}

void ICRO_StepGetConnexComponentsFrom3dGridInt::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        const CT_Grid3D<int>* itemIn_itmRaster3D = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmRaster3D != NULL)
        {
            CT_Grid3D<int>* itemOut_itmRaster3D = new CT_Grid3D<int>( DEFout_itmGrid3d, resOut_rsltRaster3D,
                                                                      itemIn_itmRaster3D->minX(), itemIn_itmRaster3D->minY(), itemIn_itmRaster3D->minZ(),
                                                                      itemIn_itmRaster3D->xArraySize(), itemIn_itmRaster3D->yArraySize(), itemIn_itmRaster3D->zArraySize(),
                                                                      itemIn_itmRaster3D->resolution(),
                                                                      std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            labelConnectedComponents( itemIn_itmRaster3D,
                                      itemOut_itmRaster3D,
                                      _minValue,
                                      _minPixelsInComponent );

            // Ajout de la grille au resultat
            CT_StandardItemGroup* grpGrid3D = new CT_StandardItemGroup(DEFout_grpGrid3d, resOut_rsltRaster3D );
            grpGrid3D->addItemDrawable( itemOut_itmRaster3D );
            resOut_rsltRaster3D->addGroup( grpGrid3D );
        }
    }
}

void ICRO_StepGetConnexComponentsFrom3dGridInt::labelConnectedComponents(const CT_Grid3D<int> *inputGrid,
                                                                         CT_Grid3D<int> *outLabeledGrid,
                                                                         double minValue,
                                                                         int minLabelSize )
{
    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();
    int currentLabelID = 1;
    QList<Eigen::Vector3i> currentLabelPixels;

    for( int i = 0 ; i < xdim ; i++ )
    {
        for( int j = 0 ; j < ydim ; j++ )
        {
            for( int k = 0 ; k < zdim ; k++ )
            {
                // Parcours tous les pixels pour rechercher un pixel non encore etiquete et au dessus du seuil voulus
                if( outLabeledGrid->value( i,j,k ) == outLabeledGrid->NA() &&
                    inputGrid->value( i, j, k ) >= minValue )
                {

                    // On a trouve une nouvelle graine pour la recherche de composante connexe
                    labelConnectedComponent( inputGrid,
                                             outLabeledGrid,
                                             minValue,
                                             i,j,k,
                                             currentLabelID,
                                             currentLabelPixels );

                    if( currentLabelPixels.size() < minLabelSize )
                    {
                        // Filtre la composante en mettant la valeur -1
                        foreach( Eigen::Vector3i v, currentLabelPixels )
                        {
                            outLabeledGrid->setValue( v.x(), v.y(), v.z(), -1 );
                        }
                    }

                    else
                    {
                        // On incremente le label
                        currentLabelID++;
                    }
                }
            }
        }
    }

    outLabeledGrid->computeMinMax();
    qDebug() << "Il y a en tout " << currentLabelID << " composantes";
}

void ICRO_StepGetConnexComponentsFrom3dGridInt::labelConnectedComponent(const CT_Grid3D<int> *inputGrid,
                                                                        CT_Grid3D<int> *outLabeledGrid,
                                                                        double minValue,
                                                                        int startx,
                                                                        int starty,
                                                                        int startz,
                                                                        int label,
                                                                        QList<Eigen::Vector3i>& outPixelsOfComponent )
{
    outPixelsOfComponent.clear();
    int naValue = outLabeledGrid->NA();

    // On considere que le pixel en haut a droite au fond (le top de la bbox) est un pixel d''exterieur
    Eigen::Vector3i firstPixel;
    firstPixel << startx, starty, startz;

    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< Eigen::Vector3i > f;

    // On marque le premier pixel et on l'ajoute a la file
    outLabeledGrid->setValue( firstPixel.x(), firstPixel.y(), firstPixel.z(), label );
    outPixelsOfComponent.append( firstPixel );
    f.enqueue( firstPixel );

    // Tant que la file n'est pas vide
    Eigen::Vector3i curPixel;
    Eigen::Vector3i neiPixel;
    while( !f.empty() )
    {
        // On prend le premier element de la file
        curPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            for( int j = -1 ; j <= 1 ; j++ )
            {
                for( int k = -1 ; k <= 1 ; k++ )
                {
                    // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                    // Si le voisin n'est pas deja marque et si il a une valeur superieure a la valeur minimum souhaitee
                    if( outLabeledGrid->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) == naValue &&
                        inputGrid->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) >= minValue )
                    {
                        neiPixel(0) = curPixel.x()+i;
                        neiPixel(1) = curPixel.y()+j;
                        neiPixel(2) = curPixel.z()+k;

                        // On le marque et on l'ajoute a la file
                        outLabeledGrid->setValue( neiPixel.x(), neiPixel.y(), neiPixel.z(), label );
                        f.enqueue( neiPixel );
                        outPixelsOfComponent.append( neiPixel );
                    }
                }
            }
        }
    }
}
