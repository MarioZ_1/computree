#include "icro_stepmultiressplitcomponentsfromstempositions.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include <QQueue>

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltResultConnectedComponentNonMultis "rsltResultConnectedComponentNonMultis"

#define DEFout_grpGeneral "grpGeneral"

#define DEFout_grpGrpCircles "grpGrpCircles"
#define DEFout_grpCircles "grpCircles"
#define DEFout_itmCircle "itmCircle"

#define DEFout_grpGrid3d "grpGrid3d"
#define DEFout_itmGrid3dLabel "itmGrid3dLabel"
#define DEFout_itmGrid3dType "itmGrid3dType"

// Constructor : initialization of parameters
ICRO_StepMultiResSplitComponentsFromStemPositions::ICRO_StepMultiResSplitComponentsFromStemPositions(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minValue = 100;
    _minPixelsInComponent = 100;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepMultiResSplitComponentsFromStemPositions::getStepDescription() const
{
    return tr("Identifie les composantes connexes d'une grille 3D des troncs en multi resolution (int)");
}

// Step detailled description
QString ICRO_StepMultiResSplitComponentsFromStemPositions::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepMultiResSplitComponentsFromStemPositions::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepMultiResSplitComponentsFromStemPositions::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepMultiResSplitComponentsFromStemPositions(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepMultiResSplitComponentsFromStemPositions::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3Rasterd = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3Rasterd->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3Rasterd->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
}

// Creation and affiliation of OUT models
void ICRO_StepMultiResSplitComponentsFromStemPositions::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponentNonMultis = createNewOutResultModel(DEFout_rsltResultConnectedComponentNonMultis, tr("Connected components (grid3d)"));
    res_rsltConnectedComponentNonMultis->setRootGroup(DEFout_grpGeneral, new CT_StandardItemGroup(), tr("Group General"));

    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrid3d, new CT_StandardItemGroup(), tr("Group Grids"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpCircles, new CT_StandardItemGroup(), tr("Group Group Circles"));

    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3dLabel, new CT_Grid3D<int>(), tr("Grid3d with labeled components"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3dType, new CT_Grid3D<int>(), tr("Grid3d with type components"));

    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_rsltResultConnectedComponentNonMultis, DEFout_grpGrpCircles, new CT_StandardItemGroup(), tr("Group Grp Circles"));
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpCircles, DEFout_grpCircles, new CT_StandardItemGroup(), tr("Group Circle"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpCircles, DEFout_itmCircle, new CT_Circle(), tr("Circle") );
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepMultiResSplitComponentsFromStemPositions::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Valeur min", "", 0, 9999, 4, _minValue );
    configDialog->addInt("n Min pixels","",1,999999999,_minPixelsInComponent);
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        const CT_Grid3D<int>* itemIn_itmRaster3D = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmRaster3D != NULL)
        {
            CT_StandardItemGroup* grpGeneral = new CT_StandardItemGroup(DEFout_grpGeneral, resOut_rsltRaster3D );
            resOut_rsltRaster3D->addGroup( grpGeneral );

            CT_Grid3D<int>* itemOut_itmLabelGrid = new CT_Grid3D<int>( DEFout_itmGrid3dLabel, resOut_rsltRaster3D,
                                                                      itemIn_itmRaster3D->minX(), itemIn_itmRaster3D->minY(), itemIn_itmRaster3D->minZ(),
                                                                      itemIn_itmRaster3D->xArraySize(), itemIn_itmRaster3D->yArraySize(), itemIn_itmRaster3D->zArraySize(),
                                                                      itemIn_itmRaster3D->resolution(),
                                                                      std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            CT_Grid3D<int>* itemOut_itmTypeGrid = new CT_Grid3D<int>( DEFout_itmGrid3dType, resOut_rsltRaster3D,
                                                                      itemIn_itmRaster3D->minX(), itemIn_itmRaster3D->minY(), itemIn_itmRaster3D->minZ(),
                                                                      itemIn_itmRaster3D->xArraySize(), itemIn_itmRaster3D->yArraySize(), itemIn_itmRaster3D->zArraySize(),
                                                                      itemIn_itmRaster3D->resolution(),
                                                                      std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            getCirclesFromFile( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );
            CT_StandardItemGroup* grpGrpCircle = new CT_StandardItemGroup(DEFout_grpGrpCircles, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpCircle );
            foreach( CT_CircleData* circle, _circles )
            {
                CT_StandardItemGroup* grpCircle = new CT_StandardItemGroup(DEFout_grpCircles, resOut_rsltRaster3D );
                grpGrpCircle->addGroup( grpCircle );
                grpCircle->addItemDrawable( new CT_Circle( DEFout_itmCircle, resOut_rsltRaster3D,
                                              circle ) );
            }

            QVector< ConnectedComponentNonMulti* > components;
            labelConnectedComponentNonMultis( itemIn_itmRaster3D,
                                      itemOut_itmLabelGrid,
                                      _minValue,
                                      _minPixelsInComponent,
                                      components );

            getComponentsType( itemOut_itmLabelGrid,
                               itemOut_itmTypeGrid,
                               components );

            getComponentsType2( itemOut_itmTypeGrid );

            qDebug() << "Il y a en tout " << components.size() << " composantes";
            qDebug() << "Composante trop petite " << TROP_PETITE;
            qDebug() << "Composante vide " << VIDE;
            qDebug() << "Composante simple " << SIMPLE;
            qDebug() << "Composante complexe " << COMPLEXE;

            // Ajout de la grille au resultat
            CT_StandardItemGroup* grpGrid3D = new CT_StandardItemGroup(DEFout_grpGrid3d, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrid3D );
            grpGrid3D->addItemDrawable( itemOut_itmLabelGrid );
            grpGrid3D->addItemDrawable( itemOut_itmTypeGrid );
        }
    }
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::labelConnectedComponentNonMultis(const CT_Grid3D<int> *inputGrid,
                                                                                 CT_Grid3D<int> *outLabeledGrid,
                                                                                 double minValue,
                                                                                 int minLabelSize,
                                                                                 QVector<ConnectedComponentNonMulti *> &outComponents)
{
    outComponents.clear();

    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();
    int currentLabelID = 1;

    for( int i = 0 ; i < xdim ; i++ )
    {
        for( int j = 0 ; j < ydim ; j++ )
        {
            for( int k = 0 ; k < zdim ; k++ )
            {
                // Parcours tous les pixels pour rechercher un pixel non encore etiquete et au dessus du seuil voulus
                if( outLabeledGrid->value( i,j,k ) == outLabeledGrid->NA() &&
                    inputGrid->value( i, j, k ) >= minValue )
                {
                    ConnectedComponentNonMulti* currentConnectedComponentNonMulti = new ConnectedComponentNonMulti();

                    // On a trouve une nouvelle graine pour la recherche de composante connexe
                    labelConnectedComponentNonMulti( inputGrid,
                                             outLabeledGrid,
                                             minValue,
                                             i,j,k,
                                             currentLabelID,
                                             currentConnectedComponentNonMulti );

                    outComponents.push_back( currentConnectedComponentNonMulti );

                    if( currentConnectedComponentNonMulti->pixels->size() < minLabelSize )
                    {
                        // Filtre la composante en mettant la valeur TROP_PETITE
                        currentConnectedComponentNonMulti->_type = TROP_PETITE;
                        currentConnectedComponentNonMulti->_label = TROP_PETITE;
                        labelComponentInGrid( currentConnectedComponentNonMulti, outLabeledGrid, TROP_PETITE );
                    }

                    else
                    {
                        // On incremente le label
                        currentLabelID++;
                    }
                }
            }
        }
    }

    outLabeledGrid->computeMinMax();
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::labelConnectedComponentNonMulti(const CT_Grid3D<int> *inputGrid,
                                                                        CT_Grid3D<int> *outLabeledGrid,
                                                                        double minValue,
                                                                        int startx,
                                                                        int starty,
                                                                        int startz,
                                                                        int label,
                                                                        ConnectedComponentNonMulti* outPixelsOfComponent )
{
    outPixelsOfComponent->_label = label;
    outPixelsOfComponent->pixels->clear();
    int naValue = outLabeledGrid->NA();

    // On considere que le pixel en haut a droite au fond (le top de la bbox) est un pixel d''exterieur
    Eigen::Vector3i firstPixel;
    firstPixel << startx, starty, startz;

    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< Eigen::Vector3i > f;

    // On marque le premier pixel et on l'ajoute a la file
    outLabeledGrid->setValue( firstPixel.x(), firstPixel.y(), firstPixel.z(), label );
    outPixelsOfComponent->pixels->append( firstPixel );
    f.enqueue( firstPixel );

    // Tant que la file n'est pas vide
    Eigen::Vector3i curPixel;
    Eigen::Vector3i neiPixel;
    while( !f.empty() )
    {
        // On prend le premier element de la file
        curPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            for( int j = -1 ; j <= 1 ; j++ )
            {
                for( int k = -1 ; k <= 1 ; k++ )
                {
                    // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                    // Si le voisin n'est pas deja marque et si il a une valeur superieure a la valeur minimum souhaitee
                    if( outLabeledGrid->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) == naValue &&
                        inputGrid->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) >= minValue )
                    {
                        neiPixel(0) = curPixel.x()+i;
                        neiPixel(1) = curPixel.y()+j;
                        neiPixel(2) = curPixel.z()+k;

                        // On le marque et on l'ajoute a la file
                        outLabeledGrid->setValue( neiPixel.x(), neiPixel.y(), neiPixel.z(), label );
                        f.enqueue( neiPixel );
                        outPixelsOfComponent->pixels->append( neiPixel );
                    }
                }
            }
        }
    }
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::getComponentsType(CT_Grid3D<int> *labeledGrid,
                                                                          CT_Grid3D<int> *outTypeGrid,
                                                                          QVector< ConnectedComponentNonMulti* >& components )
{

    // Pour chaque cercle
    for( int circleId = 0 ; circleId < _circles.size() ; circleId++ )
    {
        QVector<int> labelsOfCircle;
        CT_CircleData* circleData = _circles.at(circleId);

        // On regarde si le cercle touche un pixel de la composante courante
        // BBox du cercle dans l'espace
        Eigen::Vector3d bot;
        Eigen::Vector3d top;
        bot.x() = circleData->getCenter().x() - circleData->getRadius();
        bot.y() = circleData->getCenter().y() - circleData->getRadius();
        bot.z() = circleData->getCenter().z();
        top.x() = circleData->getCenter().x() + circleData->getRadius();
        top.y() = circleData->getCenter().y() + circleData->getRadius();
        top.z() = circleData->getCenter().z();

        // BBox du cercle dans la grille
        size_t botx, boty, botz;
        labeledGrid->colX( bot.x(), botx );
        labeledGrid->linY( bot.y(), boty );
        labeledGrid->levelZ( bot.z(), botz );

        size_t topx, topy, topz;
        labeledGrid->colX( top.x(), topx );
        labeledGrid->linY( top.y(), topy );
        labeledGrid->levelZ( top.z(), topz );

        for( int i = botx ; i <= topx ; i++ )
        {
            for( int j = boty ; j <= topy ; j++ )
            {
                for( int k = botz ; k <= topz ; k++ )
                {
                    // Si une etiquette coupe le cercle
                    if( labeledGrid->value(i,j,k) != labeledGrid->NA() && labeledGrid->value(i,j,k) != TROP_PETITE )
                    {
                        // Et qu'on ne l'avait pas encore vue pour ce cercle
                        if( !labelsOfCircle.contains( labeledGrid->value(i,j,k) ) )
                        {
                            // On recupere la composante associee
                            ConnectedComponentNonMulti* component = getComponentFromLabel( components, labeledGrid->value(i,j,k) );
                            labelsOfCircle.push_back( labeledGrid->value(i,j,k) );
                            component->_nIntersectedCircles++;

                            switch ( component->_type )
                            {
                                case VIDE :
                                {
                                    component->_type = SIMPLE;
                                    break;
                                }
                                case SIMPLE :
                                {
                                    component->_type = COMPLEXE;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    int nTropPetites = 0;
    int nVides = 0;
    int nSimples = 0;
    int nComplexes = 0;

    foreach( ConnectedComponentNonMulti* component, components )
    {
        labelComponentInGrid( component, outTypeGrid, component->_type );

        switch ( component->_type )
        {
            case TROP_PETITE :
            {
                nTropPetites++;
                break;
            }
            case VIDE :
            {
                nVides++;
                break;
            }
            case SIMPLE :
            {
                nSimples++;
                break;
            }
            case COMPLEXE :
            {
                nComplexes++;
                break;
            }
        }
    }

    outTypeGrid->computeMinMax();

    qDebug() << "nTropPetites " << nTropPetites;
    qDebug() << "nVides " << nVides;
    qDebug() << "nSimples " << nSimples;
    qDebug() << "nComplexes " << nComplexes;
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::getComponentsType2(CT_Grid3D<int> *typeGrid)
{
    // Pour chaque cercle
    for( int circleId = 0 ; circleId < _circles.size() ; circleId++ )
    {
        qDebug() << "Circle " << circleId << " sur " << _circles.size();
        QVector<int> labelsOfCircle;
        CT_CircleData* circleData = _circles.at(circleId);

        // On regarde si le cercle touche un pixel de la composante courante
        // BBox du cercle dans l'espace
        Eigen::Vector3d bot;
        Eigen::Vector3d top;
        bot.x() = circleData->getCenter().x() - circleData->getRadius();
        bot.y() = circleData->getCenter().y() - circleData->getRadius();
        bot.z() = circleData->getCenter().z();
        top.x() = circleData->getCenter().x() + circleData->getRadius();
        top.y() = circleData->getCenter().y() + circleData->getRadius();
        top.z() = circleData->getCenter().z();

        // BBox du cercle dans la grille
        size_t botx, boty, botz;
        typeGrid->colX( bot.x(), botx );
        typeGrid->linY( bot.y(), boty );
        typeGrid->levelZ( bot.z(), botz );

        size_t topx, topy, topz;
        typeGrid->colX( top.x(), topx );
        typeGrid->linY( top.y(), topy );
        typeGrid->levelZ( top.z(), topz );

        for( int i = botx ; i <= topx ; i++ )
        {
            for( int j = boty ; j <= topy ; j++ )
            {
                for( int k = botz ; k <= topz ; k++ )
                {
                    if( typeGrid->value(i,j,k) == VIDE )
                    {
                        qDebug() << "Cercle " << i << " coupe vide ";
                    }
                }
            }
        }
    }
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::labelComponentInGrid(ConnectedComponentNonMulti *component, CT_Grid3D<int> *grid, int label)
{
    foreach( Eigen::Vector3i v, *(component->pixels) )
    {
        grid->setValue( v.x(), v.y(), v.z(), label );
    }
}

ConnectedComponentNonMulti *ICRO_StepMultiResSplitComponentsFromStemPositions::getComponentFromLabel(QVector<ConnectedComponentNonMulti *> &components,
                                                                                             int label)
{
    foreach( ConnectedComponentNonMulti* component, components )
    {
        if( component->_label == label )
        {
            return component;
        }
    }
}

void ICRO_StepMultiResSplitComponentsFromStemPositions::getCirclesFromFile(QString dir, QString fileName)
{
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            _circles.push_back( new CT_CircleData( center, direction, r ) );
        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }
}
