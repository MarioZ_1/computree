#ifndef ICRO_STEPGETOUTTERSURFACEFROM3DGRIDDOUBLE_H
#define ICRO_STEPGETOUTTERSURFACEFROM3DGRIDDOUBLE_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"

#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>

/*!
 * \class ICRO_StepGetOutterSurfaceFrom3dGridDouble
 * \ingroup Steps_ICRO
 * \brief <b>Cree un raster 2D pour chaque tranche d'une image 3D.</b>
 *
 * No detailled description for this step
 *
 * \param _sliceAxis
 * \param _saveDir
 *
 */

class ICRO_StepGetOutterSurfaceFrom3dGridDouble: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepGetOutterSurfaceFrom3dGridDouble(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void getOutterSurfaceElements(const CT_Grid3D<double> *grid, CT_Grid3D<bool>* outOutterSurfaceElements );

    void inverseBooleanGrid( const CT_Grid3D<bool>* inGrid, CT_Grid3D<bool>* outGrid );

private:
};

#endif // ICRO_STEPGETOUTTERSURFACEFROM3DGRIDDOUBLE_H
