#ifndef ICRO_STEPGETCONNEXCOMPONENTSFROM3DGRIDINT_H
#define ICRO_STEPGETCONNEXCOMPONENTSFROM3DGRIDINT_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"

/*!
 * \class ICRO_StepGetConnexComponentsFrom3dGridInt
 * \ingroup Steps_ICRO
 * \brief <b>Segmente une grille 3D en composantes connexes.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

class ICRO_StepGetConnexComponentsFrom3dGridInt: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepGetConnexComponentsFrom3dGridInt(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void labelConnectedComponents(const CT_Grid3D<int>* inputGrid,
                                  CT_Grid3D<int>* outLabeledGrid,
                                  double minValue,
                                  int minLabelSize);

    void labelConnectedComponent(const CT_Grid3D<int>* inputGrid,
                                 CT_Grid3D<int>* outLabeledGrid,
                                 double minValue,
                                 int startx,
                                 int starty,
                                 int startz,
                                 int label,
                                 QList<Eigen::Vector3i> &outPixelsOfComponent);

private:

    // Step parameters
    double      _minValue;    /*!<  */
    int         _minPixelsInComponent;
};

#endif // ICRO_STEPGETCONNEXCOMPONENTSFROM3DGRIDINT_H
