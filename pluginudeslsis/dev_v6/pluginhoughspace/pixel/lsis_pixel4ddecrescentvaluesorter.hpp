/************************************************************************************
* Filename :  lsis_pixel4ddecrescentvaluesorter.hpp                                 *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                                 *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_PIXEL4D_HPP
#define LSIS_PIXEL4D_HPP

#include "lsis_pixel4ddecrescentvaluesorter.h"

template< typename DataT >
QVector< LSIS_Pixel4D >* sortPixelsByValueDecrescentOrder ( QVector< LSIS_Pixel4D >* pixels, CT_Grid4D_Sparse<DataT> const * image )
{
    LSIS_Pixel4DDecrescentValueSorter<DataT> pixSorter( image );
    std::sort( pixels->begin(), pixels->end(), pixSorter );
    return pixels;
}

#endif // LSIS_PIXEL4D_HPP
