#ifndef LSIS_STEPTRANCHESURMNTOPTIONNEL_H
#define LSIS_STEPTRANCHESURMNTOPTIONNEL_H

#include "ct_step/abstract/ct_abstractstep.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_grid2dxy.h"

//Qt dependencies
#include <QDir>
#include <QFile>

/*!
 * \class LSIS_StepTrancheSurMntOptionnel
 * \ingroup Steps_LSIS
 * \brief <b>Fait une tranche au dessus du mnt ou a hauteur fixe si pas de mnt.</b>
 *
 * No detailled description for this step
 *
 * \param _minHeight 
 * \param _maxHeight 
 *
 */

class LSIS_StepTrancheSurMntOptionnel: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LSIS_StepTrancheSurMntOptionnel(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute( CT_Grid2DXY<float>* mnt, CT_AbstractItemDrawableWithPointCloud* pointCloud );

    void compute( CT_AbstractItemDrawableWithPointCloud* pointCloud );

    void computeAndSave( CT_Grid2DXY<float>* mnt, CT_AbstractItemDrawableWithPointCloud* pointCloud, QString dirPath, QString fileName );

    void computeAndSave( CT_AbstractItemDrawableWithPointCloud* pointCloud, QString dirPath, QString fileName );

    void updateBBox( CT_Point& bot, CT_Point& top, const CT_Point& point );

private:

    // Step parameters
    double              _minHeight;    /*!<  */
    double              _maxHeight;    /*!<  */
    QString             _dirPath;
    QString             _fileName;
    bool                _saveMode;
    CT_ResultGroup*     _resultatTranche;
};

#endif // LSIS_STEPTRANCHESURMNTOPTIONNEL_H
