#include "lsis_stepfiltersnake.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_shapedata/ct_circledata.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_image2d.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEFin_rsltEstimatedCircles "DEFin_rsltEstimatedCircles"
#define DEFin_grpGrpGrp_PB_SGLF "DEFin_grpGrpGrp_PB_SGLF"
#define DEFin_grpGrpNiveauZ "DEFin_grpGrpNiveauZ"
#define DEFin_grpCluster "DEFin_grpCluster"
#define DEFin_itmCircles "DEFin_itmCircles"


// Constructor : initialization of parameters
LSIS_StepFilterSnake::LSIS_StepFilterSnake(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _radiusVariationLimit = 0.05;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepFilterSnake::getStepDescription() const
{
    return tr("Filtre les snakes basé sur la variation du diamètre des cercles");
}

// Step detailled description
QString LSIS_StepFilterSnake::getStepDetailledDescription() const
{
    return tr("No detailed description for this step");
}

// Step URL
QString LSIS_StepFilterSnake::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepFilterSnake::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepFilterSnake(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepFilterSnake::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltHitGridSparse = createNewInResultModelForCopy(DEFin_rsltEstimatedCircles,"Set of Circles");
    resIn_rsltHitGridSparse->setZeroOrMoreRootGroup();
    resIn_rsltHitGridSparse->addGroupModel("", DEFin_grpGrpGrp_PB_SGLF, CT_AbstractItemGroup::staticGetType(), tr("Input PB_SGLF Group"));
    resIn_rsltHitGridSparse->addGroupModel(DEFin_grpGrpGrp_PB_SGLF, DEFin_grpGrpNiveauZ, CT_AbstractItemGroup::staticGetType(), tr("Input NiveauZ Group"));
    resIn_rsltHitGridSparse->addGroupModel(DEFin_grpGrpNiveauZ, DEFin_grpCluster, CT_AbstractItemGroup::staticGetType(), tr("Input Cluster Group"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpCluster, DEFin_itmCircles, CT_Circle::staticGetType(), tr("Cercle"));
}

// Creation and affiliation of OUT models
void LSIS_StepFilterSnake::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEFin_rsltEstimatedCircles);
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepFilterSnake::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Limite de variation du rayon"), tr("Mètres"), 0.01, 1.00, 2, _radiusVariationLimit);
}

void LSIS_StepFilterSnake::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rsltSetOfCircles= outResultList.at(0);

    size_t totalNumberOfSnakes = 0;

    QList<CT_StandardItemGroup*> groupsToBeRemoved;

    // IN results browsing
    CT_ResultGroupIterator itIn_grpGrpGrpPB_SGLF( res_rsltSetOfCircles, this, DEFin_grpGrpGrp_PB_SGLF );
    while ( itIn_grpGrpGrpPB_SGLF.hasNext() && !isStopped() )
    {
        CT_StandardItemGroup* grpIn_grpGrpGrpPB_SGLFS = (CT_StandardItemGroup*) itIn_grpGrpGrpPB_SGLF.next();
        CT_GroupIterator itIn_grpGrpNiveauZ( grpIn_grpGrpGrpPB_SGLFS, this, DEFin_grpGrpNiveauZ );

        while ( itIn_grpGrpNiveauZ.hasNext() && !isStopped() )
        {
            ++totalNumberOfSnakes;
            int i = 0;
            double lowestRadius = 0;
            double highestRadius = 0;
            CT_StandardItemGroup* grpIn_grpGrpNiveauZ = (CT_StandardItemGroup*) itIn_grpGrpNiveauZ.next();

            CT_GroupIterator itIn_grpCluster( grpIn_grpGrpNiveauZ, this, DEFin_grpCluster );
            while ( itIn_grpCluster.hasNext() && !isStopped() )
            {
                ++i;
                CT_StandardItemGroup* grpIn_grpCluster = (CT_StandardItemGroup*) itIn_grpCluster.next();
                const CT_Circle* itemIn_itmCircle = (CT_Circle*)grpIn_grpCluster->firstItemByINModelName(this, DEFin_itmCircles);

                if( itemIn_itmCircle != NULL )
                {
                    //Radius Variation Checkup
                    if(i != 1){
                        if(itemIn_itmCircle->getRadius() < lowestRadius){
                            lowestRadius = itemIn_itmCircle->getRadius();
                        }else if(itemIn_itmCircle->getRadius() > highestRadius){
                            highestRadius = itemIn_itmCircle->getRadius();
                        }
                    }else{
                        lowestRadius = itemIn_itmCircle->getRadius();
                        highestRadius = itemIn_itmCircle->getRadius();
                    }
                }
            }
            //Assignment for removal of snakes with higher radius differential
            if((highestRadius-lowestRadius)> _radiusVariationLimit){
                groupsToBeRemoved.append(grpIn_grpGrpNiveauZ);
            }
        }
    }

    size_t numberOfRemovedSnakes = groupsToBeRemoved.size();
    // Removal of snakes with higher radius differential
    while (!groupsToBeRemoved.isEmpty())
    {
        CT_StandardItemGroup *group = groupsToBeRemoved.takeLast();
        recursiveRemoveGroupIfEmpty((CT_StandardItemGroup*)group->parentGroup(), group);
    }

    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Nombre de snakes avant filtrage : %1")).arg(totalNumberOfSnakes));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Nombre de snakes éliminés : %1")).arg(numberOfRemovedSnakes));
}

void LSIS_StepFilterSnake::recursiveRemoveGroupIfEmpty(CT_StandardItemGroup *parent, CT_StandardItemGroup *group) const
{
    if(parent != NULL)
    {
        parent->removeGroup(group);

        if(parent->isEmpty())
            recursiveRemoveGroupIfEmpty((CT_StandardItemGroup*)parent->parentGroup(), parent);
    }
    else
    {
        ((CT_ResultGroup*)group->result())->removeGroupSomethingInStructure(group);
    }
}
