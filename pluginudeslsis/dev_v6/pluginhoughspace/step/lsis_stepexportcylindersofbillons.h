#ifndef LSIS_STEPEXPORTCYLINDERSOFBILLONS_H
#define LSIS_STEPEXPORTCYLINDERSOFBILLONS_H

//Tools dependencies
#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_scene.h"

/*!
 * \class LSIS_StepExportCylindersOfBillons
 * \ingroup Steps_MONP
 * \brief <b>Exporte des cylindres fittes par billons</b>
 *
 * No detailled description for this step
 *
 *
 */
class LSIS_StepExportCylindersOfBillons: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    LSIS_StepExportCylindersOfBillons(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:
    QStringList             _saveDirList;
    QString                 _saveDir;
    QString                 _filePrefix;
    CT_Image2D<float>*     _optionalMnt;
};

#endif // LSIS_STEPEXPORTCYLINDERSOFBILLONS_H
