#ifndef LSIS_FASTFILTERVISITOR_H
#define LSIS_FASTFILTERVISITOR_H

// Inherits from abstract visitor
#include "./lsis_abstractvisitorgrid4d.h"

template < typename DataImage >
class CT_Grid4D;

template < typename DataT >
class LSIS_FastFilterVisitor : public LSIS_AbstractVisitorGrid4D<DataT>
{
public:
    /*!
     * \brief LSIS_FastFilterVisitor
     *
     * Constructeur
     *
     * \param grid : grille que le visiteur viste
     */
    LSIS_FastFilterVisitor(CT_Grid4D<DataT>* grid);

    /*!
      * Destructeur (rien a faire, il ne doit pas liberer l'image qu'il visite!!)
      */
    virtual ~LSIS_FastFilterVisitor();

    /*!
     * \brief visit
     *
     * \param levw : coordonnee du pixel a visiter
     * \param levx : coordonnee du pixel a visiter
     * \param levy : coordonnee du pixel a visiter
     * \param levz : coordonnee du pixel a visiter
     * \param beam : rayon qui traverse la grille
     */
    virtual void visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam);

    int sumOfVisitedVotes() const;
    void setSumOfVisitedVotes(int sumOfVisitedVotes);

private :
    int     _sumOfVisitedVotes;
};

// Inclusion des implementations template
#include "lsis_fastfiltervisitor.hpp"

#endif // LSIS_FASTFILTERVISITOR_H
