/************************************************************************************
* Filename :  assertwithmessage.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef ASSERTWITHMESSAGE_H
#define ASSERTWITHMESSAGE_H

// Utilisation des assertions
#include <assert.h>
#include <iostream>

/* ******************************************************************************************************** */
/* Definition de macros outils                                                                              */
/* ******************************************************************************************************** */
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //
// Redefinition de la macro assert pour pouvoir y ajouter un message ////////////////////////////////////// //
// Source de documentation : http://stackoverflow.com/questions/3767869/adding-message-to-assert            //
#define assertWithMessage(condition, message) \
do { \
    if (!(condition)) \
    { \
        std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                  << " line " << __LINE__ << ": " << message << std::endl; \
    assert(false); \
    } \
} \
while (false)
// Fin de la redefinition de la macro ///////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////////////////////////////////////////////////////////////// //

#endif // ASSERTWITHMESSAGE_H
