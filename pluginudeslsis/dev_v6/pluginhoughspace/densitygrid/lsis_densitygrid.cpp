/************************************************************************************
* Filename :  lsis_densitygrid.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_densitygrid.h"

// Parcours des points
#include "ct_iterator/ct_pointiterator.h"

LSIS_DensityGrid::LSIS_DensityGrid(const CT_OutAbstractSingularItemModel *model,
                                   CT_AbstractResult *result,
                                   double xmin,
                                   double ymin,
                                   double zmin,
                                   size_t dimx,
                                   size_t dimy,
                                   size_t dimz,
                                   double resolution,
                                   CT_PCIR indexCloud )
    : CT_Grid3D<int> ( model,
                       result,
                       xmin, ymin, zmin,
                       dimx,dimy, dimz,
                       resolution,
                       -1,
                       0 )
{
    computeDensityFromIndexCloud( indexCloud );
}

LSIS_DensityGrid::LSIS_DensityGrid(const QString& modelName,
                                   CT_AbstractResult *result,
                                   double xmin,
                                   double ymin,
                                   double zmin,
                                   size_t dimx,
                                   size_t dimy,
                                   size_t dimz,
                                   double resolution,
                                   CT_PCIR indexCloud )
    : CT_Grid3D<int> ( modelName,
                       result,
                       xmin, ymin, zmin,
                       dimx,dimy, dimz,
                       resolution,
                       -1,
                       0 )
{
    computeDensityFromIndexCloud( indexCloud );
}

LSIS_DensityGrid::LSIS_DensityGrid(const CT_OutAbstractSingularItemModel *model,
                                   CT_AbstractResult *result,
                                   double xmin, double ymin, double zmin,
                                   double xmax, double ymax, double zmax,
                                   double resolution,
                                   bool coordConstructor,
                                   CT_PCIR indexCloud)
    : CT_Grid3D<int> ( model,
                       result,
                       xmin, ymin, zmin,
                       xmax, ymax, zmax,
                       resolution,
                       -1,
                       0)
{
    computeDensityFromIndexCloud( indexCloud );
}

LSIS_DensityGrid::LSIS_DensityGrid(const QString& modelName,
                                   CT_AbstractResult *result,
                                   double xmin, double ymin, double zmin,
                                   double xmax, double ymax, double zmax,
                                   double resolution,
                                   bool coordConstructor,
                                   CT_PCIR indexCloud)
    : CT_Grid3D<int> ( modelName,
                       result,
                       xmin, ymin, zmin,
                       xmax, ymax, zmax,
                       resolution,
                       -1,
                       0 )
{
    computeDensityFromIndexCloud( indexCloud );
}

void LSIS_DensityGrid::computeDensityFromIndexCloud(CT_PCIR indexCloud)
{
    size_t indice;

    CT_PointIterator it(indexCloud);

    // Parcours du nuage
    while(it.hasNext())
    {
        // On recupere le point courant
        const CT_Point& curPoint = it.next().currentPoint();

        // On calcule la cellule de la grille dans laquelle il se trouve
        if( indexAtXYZ( curPoint.x(), curPoint.y(), curPoint.z(), indice) )
        {
            // On incremente la densite de cette cellule
            addValueAtIndex( indice, 1 );
        }
    }

    // Mise a jour des valeurs min et max de la grille
    computeMinMax();
}
