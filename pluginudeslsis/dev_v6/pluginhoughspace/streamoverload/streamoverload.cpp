/************************************************************************************
* Filename :  streamoverload.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "streamoverload.h"

#include "../raytracing4d/lsis_beam4d.h"

QDebug operator<< (QDebug debug, LSIS_Point4DInt const & p )
{
    debug << "(" << p.w() << "," << p.x() << "," << p.y() << "," << p.z() << ")";

    return debug;
}

QDebug operator<< (QDebug debug, LSIS_Point4DFloat const & p )
{
    debug << "(" << p.w() << "," << p.x() << "," << p.y() << "," << p.z() << ")";

    return debug;
}

QDebug operator<< (QDebug debug, LSIS_Point4DDouble const & p )
{
    debug << "(" << p.w() << "," << p.x() << "," << p.y() << "," << p.z() << ")";

    return debug;
}

QDebug operator<< (QDebug debug, LSIS_Beam4D const & beam )
{
    debug << "\n{\n"
          << "  Origin    = " << beam.getOrigin() << "\n"
          << "  Direction = " << beam.getDirection() << "\n}";

    return debug;
}
