/************************************************************************************
* Filename :  lsis_readerhoughspace4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_READERHOUGHSPACE4D_H
#define LSIS_READERHOUGHSPACE4D_H

#include "ct_step/abstract/ct_abstractsteploadfileinscene.h"

class LSIS_ReaderHoughSpace4D : public CT_AbstractStepLoadFileInScene
{
    Q_OBJECT

public:
    LSIS_ReaderHoughSpace4D(CT_StepInitializeData &data);

    virtual void init();

    QString getStepDescription() const;

    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
    QList<FileFormat> getFileExtensionAccepted() const;

protected:
    void createOutResultModelListProtected();
    int readHeaderFile(QFile &f);
    void readDataFile(QFile &f, int offset, bool little_endian = false);

private :
    int     _nlevw;
    int     _nlevx;
    int     _nlevy;
    int     _nlevz;
    float   _minw;
    float   _minx;
    float   _miny;
    float   _minz;
    float   _resw;
    float   _resx;
    float   _resy;
    float   _resz;
    float   _noDataVal;
};

#endif // LSIS_READERHOUGHSPACE4D_H
