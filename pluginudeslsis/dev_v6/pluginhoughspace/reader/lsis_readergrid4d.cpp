/************************************************************************************
* Filename :  lsis_readergrid4d.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_readergrid4d.h"

// Pour les resultats
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_itemdrawable/model/outModel/ct_outstdgroupmodel.h"
#include "ct_itemdrawable/model/outModel/ct_outstdsingularitemmodel.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/ct_grid4d_dense.h"

// Acces au systeme de log dans computree
#include "ct_global/ct_context.h"

// Lecture de fichiers textes
#include <QTextStream>

// Sortie console en cas de probleme
#include <QDebug>

//Model index list
#define DEF_Search_Grid4d "grid4d"
#define DEF_Search_Grid4dGroup "grid4dgroup"
#define DEF_Search_Grid4dResult "grid4dresult"

LSIS_ReaderGrid4D::LSIS_ReaderGrid4D(CT_StepInitializeData &data) : CT_AbstractStepLoadFileInScene(data)
{
}

void LSIS_ReaderGrid4D::init()
{
    CT_AbstractStepLoadFileInScene::init();
}

QString LSIS_ReaderGrid4D::getStepDescription() const
{
    return tr("Chargement d'un fichier grid4d");
}

CT_VirtualAbstractStep* LSIS_ReaderGrid4D::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_ReaderGrid4D(dataInit);
}

QList<FileFormat> LSIS_ReaderGrid4D::getFileExtensionAccepted() const
{
    QList<FileFormat> list;
    list << FileFormat(QStringList() << ".grid4d" << ".grd4d" << ".GRD4D", tr("Grilles 4D"));

    return list;
}

void LSIS_ReaderGrid4D::createOutResultModelListProtected()
{
    CT_OutStdGroupModel* gridGroup = new CT_OutStdGroupModel(DEF_Search_Grid4dGroup);
    CT_OutStdSingularItemModel* grid = new CT_OutStdSingularItemModel(DEF_Search_Grid4d, new CT_Grid4D_Dense<float>());

    gridGroup->addItem(grid);

    addOutResultModel(new CT_OutResultModelGroup(DEF_Search_Grid4dResult, gridGroup, "CT_Grid4D"));
}

int LSIS_ReaderGrid4D::readHeaderFile(QFile &f)
{
    // Outils de lecture
    QString     currentLine;
    QStringList splitline;
    bool*       read = new bool();

    // Initialise les parametres de la grille
    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _nlevw = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 1")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 1") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 1"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _nlevx = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 2")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 2") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 2"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _nlevy = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 3")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 3") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 3"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _nlevz = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 3")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 3") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 3"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _minw = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 4")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 4") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 4"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _minx = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 5")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 5") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 5"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _miny = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 6")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 6") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 6"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _minz = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 4")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 4") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 4"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _resw = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 7")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 7") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 7"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _resx = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 7")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 7") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 7"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _resy = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 7")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 7") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 7"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _resz = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 7")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 7") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 7"));
    }

    currentLine = f.readLine();
    splitline = currentLine.split('\t');
    _noDataVal = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 8")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 8") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 8"));
    }

    return 0;
}

void LSIS_ReaderGrid4D::readDataFile(QFile &f, int offset, bool little_endian)
{
    Q_UNUSED( offset );
    Q_UNUSED( little_endian );

    // Get the output result
    CT_ResultGroup* out_res = getOutResultList().first();

    // Creating a grid with the parameters read from the header
    CT_Grid4D_Dense<float>* loadedGrid = new CT_Grid4D_Dense<float>( DEF_Search_Grid4d,
                                                         out_res,
                                                         _minw,
                                                         _minx,
                                                         _miny,
                                                         _minz,
                                                         _nlevw,
                                                         _nlevx,
                                                         _nlevy,
                                                         _nlevz,
                                                         _resw,
                                                         _resx,
                                                         _resy,
                                                         _resz,
                                                         _noDataVal,
                                                         0);

    QTextStream fileStream(&f);
    float currentValue;
    for ( int w = 0 ; w < _nlevw ; w++ )
    {
        for ( int x = 0 ; x < _nlevx ; x++ )
        {
            for ( int y = 0 ; y < _nlevy ; y++ )
            {
                for ( int z = 0 ; z < _nlevz ; z++ )
                {
                    fileStream >> currentValue;
                    loadedGrid->setValue( w, x, y, z, currentValue );
                }
            }
        }
    }

    loadedGrid->computeMinMax();

    CT_StandardItemGroup* gridGroup = new CT_StandardItemGroup(DEF_Search_Grid4dGroup, out_res);
    gridGroup->addItemDrawable( loadedGrid );
    out_res->addGroup( gridGroup );
}
