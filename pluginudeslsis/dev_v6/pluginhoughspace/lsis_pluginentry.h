#ifndef LSIS_PLUGINENTRY_H
#define LSIS_PLUGINENTRY_H

#include "interfaces.h"

class LSIS_PluginManager;

class LSIS_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    LSIS_PluginEntry();
    ~LSIS_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    LSIS_PluginManager *_pluginManager;
};

#endif // LSIS_PLUGINENTRY_H