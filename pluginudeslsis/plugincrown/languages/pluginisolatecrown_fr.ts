<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ICRO_Step3dGridBoolToMultiple2dRasters</name>
    <message>
        <location filename="../step/icro_step3dgridbooltomultiple2drasters.cpp" line="28"/>
        <source>Cree un raster 2D pour chaque tranche d&apos;une image 3D (bool)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridbooltomultiple2drasters.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridbooltomultiple2drasters.cpp" line="55"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridbooltomultiple2drasters.cpp" line="56"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridbooltomultiple2drasters.cpp" line="57"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_Step3dGridDoubleToMultiple2dRasters</name>
    <message>
        <location filename="../step/icro_step3dgriddoubletomultiple2drasters.cpp" line="28"/>
        <source>Cree un raster 2D pour chaque tranche d&apos;une image 3D (Double)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgriddoubletomultiple2drasters.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgriddoubletomultiple2drasters.cpp" line="55"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgriddoubletomultiple2drasters.cpp" line="56"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgriddoubletomultiple2drasters.cpp" line="57"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_Step3dGridFloatToMultiple2dRasters</name>
    <message>
        <location filename="../step/icro_step3dgridfloattomultiple2drasters.cpp" line="28"/>
        <source>Cree un raster 2D pour chaque tranche d&apos;une image 3D (Float)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridfloattomultiple2drasters.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridfloattomultiple2drasters.cpp" line="55"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridfloattomultiple2drasters.cpp" line="56"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridfloattomultiple2drasters.cpp" line="57"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_Step3dGridIntToMultiple2dRasters</name>
    <message>
        <location filename="../step/icro_step3dgridinttomultiple2drasters.cpp" line="28"/>
        <source>Cree un raster 2D pour chaque tranche d&apos;une image 3D (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridinttomultiple2drasters.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridinttomultiple2drasters.cpp" line="55"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridinttomultiple2drasters.cpp" line="56"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_step3dgridinttomultiple2drasters.cpp" line="57"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepComputeLabeledGridFromStemPositions</name>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="32"/>
        <source>Etiquette les voxels (int) faisant partie de chaque cercle en entree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="38"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="59"/>
        <source>Scène(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="63"/>
        <source>Scène</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="69"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="70"/>
        <source>Group of grid3d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepcomputelabeledgridfromstempositions.cpp" line="71"/>
        <source>Grid3d with labeled components</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetConnectedComponentOnIntGridMultiResolution</name>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="54"/>
        <source>Identifie les composantes connexes d&apos;une grille 3D separes par une valeur minimum (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="60"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="81"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="82"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="83"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="84"/>
        <source>Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="90"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="91"/>
        <source>Group General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="93"/>
        <source>Group Grids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="94"/>
        <source>Group Group Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="96"/>
        <source>Grid3d with labeled components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="97"/>
        <source>Grid3d with type components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="99"/>
        <source>Points in TOO_SMALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="100"/>
        <source>Points in EMPTY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="101"/>
        <source>Points in SIMPLEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="102"/>
        <source>Points in COMPLEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="104"/>
        <source>Group Grp Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="105"/>
        <source>Group Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="106"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="108"/>
        <source>Group Grp Scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="109"/>
        <source>Group Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp" line="110"/>
        <source>Component From Simplex</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetConnexComponentsFrom3dGridInt</name>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="31"/>
        <source>Identifie les composantes connexes d&apos;une grille 3D separes par une valeur minimum (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="37"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="58"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="59"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="60"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="67"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="68"/>
        <source>Group of grid3d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetconnexcomponentsfrom3dgridint.cpp" line="69"/>
        <source>Grid3d with labeled components</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetDensityGridFromHitGrid</name>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="28"/>
        <source>Calcule la densite de points par metre cube a partir d&apos;une grille de hits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="55"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="56"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="57"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="63"/>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="70"/>
        <source>Density image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetdensitygridfromhitgrid.cpp" line="66"/>
        <source>Image Group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetGradientNormFrom3dGridInt</name>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="26"/>
        <source>Calcule la norme du gradient avec l&apos;operateur de sobel adapte en 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="32"/>
        <source>Uses Sobel-Feldman kernels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="53"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="54"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="55"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="61"/>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="63"/>
        <source>Gradient norm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetgradientnormfrom3dgridint.cpp" line="62"/>
        <source>Gradient group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetOutterSurfaceFrom3dGridBool</name>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="30"/>
        <source>Extrait la surface externe a partir d&apos;une grille de densite (bool)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="57"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="58"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="59"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="66"/>
        <source>Elements of outter surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="67"/>
        <source>Group of elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridbool.cpp" line="68"/>
        <source>Binarized 3D volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetOutterSurfaceFrom3dGridDouble</name>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="30"/>
        <source>Extrait la surface externe a partir d&apos;une grille de densite (double)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="57"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="58"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="59"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="66"/>
        <source>Elements of outter surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="67"/>
        <source>Group of elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgriddouble.cpp" line="68"/>
        <source>Binarized 3D volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetOutterSurfaceFrom3dGridFloat</name>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="30"/>
        <source>Extrait la surface externe a partir d&apos;une grille de densite (float)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="57"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="58"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="59"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="66"/>
        <source>Elements of outter surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="67"/>
        <source>Group of elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridfloat.cpp" line="68"/>
        <source>Binarized 3D volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetOutterSurfaceFrom3dGridInt</name>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="30"/>
        <source>Extrait la surface externe a partir d&apos;une grille de densite (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="57"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="58"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="59"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="66"/>
        <source>Elements of outter surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="67"/>
        <source>Group of elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepgetouttersurfaceof3dgridint.cpp" line="68"/>
        <source>Binarized 3D volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepGetSimplexConnectedComponentGrid</name>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="46"/>
        <source>Identifie les composantes connexes d&apos;une grille 3D separes par une valeur minimum (int) selon une approche multi echelle (superpixels)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="52"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="73"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="74"/>
        <source>Group with 3D raster and scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="75"/>
        <source>Intensity raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="81"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="84"/>
        <source>TreeStems and Connected Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="89"/>
        <source>Group Group Tree Stems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="90"/>
        <source>Connected Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="93"/>
        <source>Group Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp" line="96"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepMultiResSplitComponentsFromStemPositions</name>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="38"/>
        <source>Identifie les composantes connexes d&apos;une grille 3D des troncs en multi resolution (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="44"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="65"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="66"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="67"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="73"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="74"/>
        <source>Group General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="76"/>
        <source>Group Grids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="77"/>
        <source>Group Group Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="79"/>
        <source>Grid3d with labeled components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="80"/>
        <source>Grid3d with type components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="82"/>
        <source>Group Grp Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="83"/>
        <source>Group Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepmultiressplitcomponentsfromstempositions.cpp" line="84"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepNormalize3dGridInt</name>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="31"/>
        <source>Normalise une image avec une combinaison de normalisation locale et globale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="37"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="58"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="59"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="60"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="66"/>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="73"/>
        <source>Normalised image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/icro_stepnormalize3dgridint.cpp" line="69"/>
        <source>Image Group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICRO_StepSplitAndMergeConnectedComponents</name>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="60"/>
        <source>Split and merge de composantes connexes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="66"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="87"/>
        <source>Result with 3d raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="88"/>
        <source>Group with 3D raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="89"/>
        <source>3d Raster (int)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="90"/>
        <source>Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="96"/>
        <source>Connected components (grid3d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="99"/>
        <source>TreeStems and Connected Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="104"/>
        <source>Connected Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="105"/>
        <source>Group Group Tree Stems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="106"/>
        <source>Group Group Isolated Trees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="109"/>
        <source>Stem Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="112"/>
        <source>Stem locataions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="115"/>
        <source>Tree Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_steps/icro_stepsplitandmergeconnectedcomponents.cpp" line="118"/>
        <source>Isolated Tree</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
