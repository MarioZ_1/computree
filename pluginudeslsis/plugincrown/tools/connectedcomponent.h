#ifndef CONNECTEDCOMPONENT_H
#define CONNECTEDCOMPONENT_H

#include <QVector>
#include "tools/multiresolutionpixel.h"
typedef Eigen::Vector3i Pixel;
class SuperPixel;

enum ConnectedComponentType
{
    NA = -2,
    TOO_SMALL = -1,
    EMPTY = 1,
    SIMPLEX,
    COMPLEX
};

class ConnectedComponent
{
public :
    static int _labelID;
    ConnectedComponent();

    void displayAllChildren();
    void displayAllChildrenRecursive(int depth);

    int                             _resolutionFinale;
    int                             _nCircles;
    int                             _type;
    int                             _label;
    int                             _idScene;
    QVector< SuperPixel >           _superPixels;
    QVector< Pixel >                _pixels;
    QVector< ConnectedComponent* >  _children;
    QVector< ConnectedComponent* >  _fused;
    bool                            _wasFused;


    QVector< ConnectedComponent* >  _potentialAgregateComponents;
    QVector< int >                  _potentialAgregateDistance;
    bool                            _hasBeenAgregated;

    ConnectedComponent* getComponentToAgregateWith()
    {
        assert( !_potentialAgregateComponents.isEmpty() );

        // Recherche la distance min
        int nPotentialAgregates = _potentialAgregateComponents.size();
        int minDistance = std::numeric_limits<int>::max();
        ConnectedComponent* minComponent = NULL;
        for( int i = 0 ; i < nPotentialAgregates ; i++ )
        {
            if( _potentialAgregateDistance.at(i) < minDistance )
            {
                minDistance = _potentialAgregateDistance.at(i);
                minComponent = _potentialAgregateComponents.at(i);
            }
        }

        // Recherche le nombre de composantes qui ont la meme distance min
        int nMinComponents = 0;
        for( int i = 0 ; i < nPotentialAgregates ; i++ )
        {
            if( _potentialAgregateDistance.at(i) == minDistance )
            {
                nMinComponents++;
            }
        }

        // Si il existe une seule composante a distance min alors on la renvoie
        assert( nMinComponents > 0 );
        if( nMinComponents == 1 )
        {
            return minComponent;
        }

        // Sinon c'est que deux composantes l'atteignent au meme moment et on renvoie NULL
        else
        {
            return NULL;
        }
    }

    void updatePotentialAgregate( ConnectedComponent* potentialAgregate,
                                  int potentialDistance )
    {
        int indexOfPotentialAggregate = _potentialAgregateComponents.indexOf( potentialAgregate );

        // Si le potentiel agregat n'etait pas deja dans le tableau
        if( indexOfPotentialAggregate == -1 )
        {
            // Alors on l'ajoute avec la distance donnee
            _potentialAgregateComponents.push_back( potentialAgregate );
            _potentialAgregateDistance.push_back( potentialDistance );
//            qDebug() << "Ajout ici";
        }

        // Si il y etait deja il faut alors seulement mettre a jour la distance
        else
        {
            _potentialAgregateDistance[ indexOfPotentialAggregate ] = std::min( _potentialAgregateDistance[ indexOfPotentialAggregate ], potentialDistance );
//            qDebug() << "Mise a jour de distance";
        }
    }
};

#endif // CONNECTEDCOMPONENT_H
