#include "onf_adjustplotpositioncylinderdrawmanager.h"


#include <QObject>


ONF_AdjustPlotPositionCylinderDrawManager::ONF_AdjustPlotPositionCylinderDrawManager(QString drawConfigurationName) : CT_StandardAbstractShapeDrawManager(drawConfigurationName.isEmpty() ? CT_Cylinder::staticName() : drawConfigurationName)
{
    _color = Qt::red;
    _transX = 0;
    _transY = 0;
    _circles = false;
    _fixedH = true;
    _h = 40;
    _selectedCylinder = NULL;
    _refZ = 0;
    _mode3D = false;
    _radiusFactor = 2.0;
}

ONF_AdjustPlotPositionCylinderDrawManager::~ONF_AdjustPlotPositionCylinderDrawManager()
{
}

void ONF_AdjustPlotPositionCylinderDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractShapeDrawManager::draw(view, painter, itemDrawable);

    const CT_Cylinder &item = dynamic_cast<const CT_Cylinder&>(itemDrawable);
    CT_Cylinder* cyl = (CT_Cylinder*) &item;

    Eigen::Vector3d center = item.getCenter();
    Eigen::Vector3d direction = item.getDirection();
    direction.normalize();

    center(0) += _transX;
    center(1) += _transY;

    QColor color = painter.getColor();
    if (&item == _selectedCylinder)
    {
        painter.setColor(_selectionColor);

    } else if (item.getCircleError() != 0)
    {
        painter.setColor(_movedColor);
    } else if (_highlightedCylinders.contains(cyl))
    {
        painter.setColor(_highlightColor);
    } else {
        painter.setColor(_color);
    }


    if (_mode3D)
    {
        double height = _h;

        if (!_fixedH)
        {
            height = item.getHeight();
        }

        Eigen::Vector3d bottom = center - (direction * item.getHeight()/2.0);
        center = bottom + (direction * height/2.0);

        if(_circles)
        {
            Eigen::Vector3d level = bottom;

            for (double h = 0 ; h < height ; h += 0.50)
            {
                level(2) = h;
                painter.drawCircle3D(level, direction, item.getRadius());
            }
        } else {
            painter.drawCylinder3D(center, direction, item.getRadius(), height);
            painter.drawCircle3D(bottom, direction, item.getRadius());
        }
    } else {
        Eigen::Vector3d center2d(center(0), center(1), _refZ);
        Eigen::Vector3d vertDir(0, 0, 1);
        painter.drawCylinder3D(center2d, vertDir, item.getRadius()*_radiusFactor, 0.1);
    }

    painter.setColor(color);
}

void ONF_AdjustPlotPositionCylinderDrawManager::setTranslation(double x, double y)
{
    _transX = x;
    _transY = y;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setParameters(bool circles, bool fixedH, double h, double radiusFactor)
{
    _circles = circles;
    _fixedH = fixedH;
    _h = h;
    _radiusFactor = radiusFactor;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setColor(QColor color)
{
    _color = color;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setSelectionColor(QColor color)
{
    _selectionColor = color;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setHighlightColor(QColor color)
{
    _highlightColor = color;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setMovedColor(QColor color)
{
    _movedColor = color;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setselectedCylinder(CT_Cylinder *selectedCylinder)
{
    _selectedCylinder = selectedCylinder;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setHighlightedCylinder(QList<CT_Cylinder *> selectedCylinder)
{
    _highlightedCylinders.clear();
    _highlightedCylinders.append(selectedCylinder);
}

void ONF_AdjustPlotPositionCylinderDrawManager::setRefZ(double refZ)
{
    _refZ = refZ;
}

void ONF_AdjustPlotPositionCylinderDrawManager::setMode3D(bool mode3D)
{
    _mode3D = mode3D;
}


CT_ItemDrawableConfiguration ONF_AdjustPlotPositionCylinderDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractShapeDrawManager::createDrawConfiguration(drawConfigurationName));

    return item;
}


