/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepcomputestorktrajectory.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_iterator/ct_resultitemiterator.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d.h"
#include "ct_itemdrawable/ct_line.h"
#include "tools/onf_computehitsthread.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>

#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInGrid   "grid"

ONF_StepComputeStorkTrajectory::ONF_StepComputeStorkTrajectory(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _range = 1;
}

QString ONF_StepComputeStorkTrajectory::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Calcul des trajectoires de Cigognes");
}

// Step description (tooltip of contextual menu)
QString ONF_StepComputeStorkTrajectory::getStepDetailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepComputeStorkTrajectory::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepComputeStorkTrajectory(dataInit);
}

void ONF_StepComputeStorkTrajectory::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grille"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGrid, CT_Grid3D<float>::staticGetType(), tr("Distances"));
}

void ONF_StepComputeStorkTrajectory::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
    {
        res->addGroupModel(DEF_SearchInGroup, _outTrajGRP_ModelName);
        res->addItemModel(_outTrajGRP_ModelName, _outTraj_ModelName, new CT_Line(), tr("Trajectoire"));
    }
}

void ONF_StepComputeStorkTrajectory::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addInt(tr("Portée de dilatation"),tr("cellules"), 1, 1e+10, _range);
}

void ONF_StepComputeStorkTrajectory::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itOut(outResult, this, DEF_SearchInGroup);
    // iterate over all groups
    while(itOut.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*)itOut.next();
        const CT_Grid3D<float>* gridIn = (CT_Grid3D<float>*)group->firstItemByINModelName(this, DEF_SearchInGrid);

        if (gridIn!=NULL)
        {
            for (size_t colx = 0 ; colx < gridIn->xdim() ; colx++)
            {
                for (size_t liny = 0 ; liny < gridIn->ydim() ; liny++)
                {
                    size_t levz = gridIn->zdim() - 1;

                    double minDist = gridIn->value(colx, liny, levz);
                    size_t minxxCoord = colx;
                    size_t minyyCoord = liny;
                    size_t minzzCoord = levz;

                    while (minDist > gridIn->resolution())
                    {
                        size_t minXX = 0;
                        if (minxxCoord > 0) {minXX = minxxCoord - 1;}
                        size_t minYY = 0;
                        if (minyyCoord > 0) {minYY = minyyCoord - 1;}
                        size_t minZZ = 0;
                        if (minzzCoord > 0) {minZZ = minzzCoord - 1;}

                        size_t maxXX = minxxCoord + 1;
                        if (maxXX > gridIn->xdim()) {maxXX = gridIn->xdim();}
                        size_t maxYY = minyyCoord + 1;
                        if (maxYY > gridIn->ydim()) {maxYY = gridIn->ydim();}
                        size_t maxZZ = minzzCoord + 1;
                        if (maxZZ > gridIn->zdim()) {maxZZ = gridIn->zdim();}


                        minDist = std::numeric_limits<double>::max();
                        size_t tmpXX = 0;
                        size_t tmpYY = 0;
                        size_t tmpZZ = 0;

                        for (size_t xx = minXX ; xx <= maxXX ; xx++)
                        {
                            for (size_t yy = minYY ; yy <= maxYY ; yy++)
                            {
                                for (size_t zz = minZZ ; zz <= maxZZ ; zz++)
                                {
                                    if (xx != minxxCoord || yy != minyyCoord || zz != minzzCoord)
                                    {
                                        double dist = gridIn->value(xx, yy, zz);
                                        if (dist != gridIn->NA() && dist < minDist)
                                        {
                                            minDist = dist;
                                            tmpXX = xx;
                                            tmpYY = yy;
                                            tmpZZ = zz;
                                        }
                                    }
                                }
                            }
                        }
                        Eigen::Vector3d pt1(gridIn->getCellCenterX(minxxCoord), gridIn->getCellCenterY(minyyCoord), gridIn->getCellCenterZ(minzzCoord));
                        Eigen::Vector3d pt2(gridIn->getCellCenterX(tmpXX), gridIn->getCellCenterY(tmpYY), gridIn->getCellCenterZ(tmpZZ));

                        CT_LineData* linedata = new CT_LineData(pt1, pt2);

                        CT_Line* line = new CT_Line(_outTraj_ModelName.completeName(), outResult, linedata);

                        CT_StandardItemGroup* grp = new CT_StandardItemGroup(_outTrajGRP_ModelName.completeName(), outResult);
                        grp->addItemDrawable(line);
                        group->addGroup(grp);

                        minxxCoord = tmpXX;
                        minyyCoord = tmpYY;
                        minzzCoord = tmpZZ;
                    }
                }
            }

        }
    }

    setProgress(99);
}


