/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepdilateboolgrid.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_iterator/ct_resultitemiterator.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "tools/onf_computehitsthread.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>

#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInGrid   "grid"

ONF_StepDilateBoolGrid::ONF_StepDilateBoolGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _range = 1;
}

QString ONF_StepDilateBoolGrid::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Dilatation d'une grille booléenne");
}

// Step description (tooltip of contextual menu)
QString ONF_StepDilateBoolGrid::getStepDetailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepDilateBoolGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepDilateBoolGrid(dataInit);
}

void ONF_StepDilateBoolGrid::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grille"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGrid, CT_Grid3D_Sparse<bool>::staticGetType(), tr("Grille"), "", CT_InAbstractModel::C_ChooseMultipleIfMultiple);
}

void ONF_StepDilateBoolGrid::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
    {
        res->addItemModel(DEF_SearchInGroup, _outGrid_ModelName, new CT_Grid3D_Sparse<bool>(), tr("Grille filtrée"));
    }
}

void ONF_StepDilateBoolGrid::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addInt(tr("Portée de dilatation"),tr("cellules"), 1, 1e+10, _range);
}

void ONF_StepDilateBoolGrid::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itOut(outResult, this, DEF_SearchInGroup);
    // iterate over all groups
    while(itOut.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)itOut.next();
        const CT_Grid3D_Sparse<bool>* gridIn = (CT_Grid3D_Sparse<bool>*)group->firstItemByINModelName(this, DEF_SearchInGrid);

        if (gridIn!=NULL)
        {
            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* outGrid = new CT_Grid3D_Sparse<bool>(_outGrid_ModelName.completeName(), outResult, gridIn->minX(), gridIn->minY(), gridIn->minZ(), gridIn->xdim(), gridIn->ydim(), gridIn->zdim(), gridIn->resolution(), false, false);

            QList<size_t> list;
            gridIn->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);

                if (gridIn->valueAtIndex(index))
                {
                    size_t colX, linY, levZ;
                    if (outGrid->indexToGrid(index, colX, linY, levZ))
                    {
                        if (gridIn->value(colX, linY, levZ))
                        {
                            size_t firstColx = colX - _range;
                            size_t lastColx  = colX + _range;
                            size_t firstLiny = linY - _range;
                            size_t lastLiny  = linY + _range;
                            size_t firstLinz = levZ - _range;
                            size_t lastLinz  = levZ + _range;

                            for (size_t xx = firstColx ; xx <= lastColx ; xx++)
                            {
                                for (size_t yy = firstLiny ; yy <= lastLiny ; yy++)
                                {
                                    for (size_t zz = firstLinz ; zz <= lastLinz ; zz++)
                                    {
                                        if (!outGrid->value(xx, yy, zz))
                                        {
                                            outGrid->setValue(xx, yy, zz, true);
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
            }

            outGrid->computeMinMax();
            group->addItemDrawable(outGrid);
        }
    }

    setProgress(99);
}


