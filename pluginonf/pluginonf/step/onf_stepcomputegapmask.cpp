/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepcomputegapmask.h"

#ifdef USE_OPENCV
#include "ct_global/ct_context.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_triangulation/ct_delaunayt.h"
#include "ct_triangulation/ct_nodet.h"
#include "ct_triangulation/ct_trianglet.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

#include <QtConcurrent>
#include <math.h>
#include <stdlib.h>
#include <limits>


#define DEF_SearchInResult   "ires"
#define DEF_SearchInGroup   "igrp"
#define DEF_SearchInMNH   "imns"

ONF_StepComputeGapMask::ONF_StepComputeGapMask(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _threshold = 5.0;
    _nbErodingSteps = -1;
}

QString ONF_StepComputeGapMask::getStepDescription() const
{
    return tr("Créer un masque des trouées");
}

QString ONF_StepComputeGapMask::getStepDetailledDescription() const
{
    return tr("Cette étape créée un raster entier. Toute valeur >= 0 est dans les trouées au seuil fixée. Le nombre indique à combien d'erosions le pixel trouée persiste.");
}

CT_VirtualAbstractStep* ONF_StepComputeGapMask::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new ONF_StepComputeGapMask(dataInit);
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeGapMask::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("MNH"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInMNH, CT_Image2D<float>::staticGetType(), tr("MNH"));
}

void ONF_StepComputeGapMask::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Seuil de hauteur de trouée :"), "m", -1e+10, 1e+10, 4, _threshold);
    configDialog->addInt(tr("Nombre d'érosions (-1 si toutes) :"), "", -1, 1e+10, _nbErodingSteps);

}

void ONF_StepComputeGapMask::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(resultModel != NULL) {
        resultModel->addItemModel(DEF_SearchInGroup, _outGapMaskodelName, new CT_Image2D<int>(), tr("Gap Mask"));
    }
}

void ONF_StepComputeGapMask::compute()
{
    // recupere les resultats de sortie
    const QList<CT_ResultGroup*> &outResList = getOutResultList();
    // récupération des modéles out
    CT_ResultGroup *outResult = outResList.at(0);

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    while (!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Image2D<float> *mnh = (CT_Image2D<float>*)group->firstItemByINModelName(this, DEF_SearchInMNH);

            if (mnh != NULL)
            {
                CT_Image2D<int>* gapMask = new CT_Image2D<int>(_outGapMaskodelName.completeName(), outResult, mnh->minX(), mnh->minY(), mnh->colDim(), mnh->linDim(), mnh->resolution(), mnh->minZ(), -9999, -1);
                CT_Image2D<quint8>* tmpMask = new CT_Image2D<quint8>(_outGapMaskodelName.completeName(), outResult, mnh->minX(), mnh->minY(), mnh->colDim(), mnh->linDim(), mnh->resolution(), mnh->minZ(), -1, 0);

                cv::Mat_<quint8> tmpMaskMat = tmpMask->getMat();

                size_t ncells = mnh->nCells();

                for (size_t index = 0 ; index < ncells ; index++)
                {
                    float val = mnh->valueAtIndex(index);

                    if (val == mnh->NA())
                    {
                        gapMask->setValueAtIndex(index, gapMask->NA());
                        tmpMask->setValueAtIndex(index, 0);
                    } else if (val > _threshold)
                    {
                        gapMask->setValueAtIndex(index, -1);
                        tmpMask->setValueAtIndex(index, 0);
                    } else {
                        gapMask->setValueAtIndex(index, 0);
                        tmpMask->setValueAtIndex(index, 1);
                    }
                }

                bool pixelsLosts = true;
                int nbErode = 0;
                cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

                while (pixelsLosts && (_nbErodingSteps == -1 || nbErode < _nbErodingSteps))
                {
                    pixelsLosts = false;

                    cv::erode(tmpMaskMat, tmpMaskMat, element, cv::Point(-1,-1), 1);
                    nbErode++;

                    for (size_t index = 0 ; index < ncells ; index++)
                    {
                        quint8 val = tmpMask->valueAtIndex(index);

                        if (gapMask->valueAtIndex(index) >= 0)
                        {
                            if (val == 1)
                            {
                                gapMask->setValueAtIndex(index, nbErode);
                            } else if (!pixelsLosts && gapMask->valueAtIndex(index) == (nbErode - 1)){
                                pixelsLosts = true;
                            }
                        }
                    }
                }

                delete tmpMask;

                // ajout du raster MNS
                group->addItemDrawable(gapMask);
                gapMask->computeMinMax();
            }

        }
        setProgress(100);
    }
}
#endif
