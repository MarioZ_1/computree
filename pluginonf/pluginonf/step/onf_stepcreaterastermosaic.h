#ifndef ONF_STEPCREATERASTERMOSAIC_H
#define ONF_STEPCREATERASTERMOSAIC_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include <Eigen/Core>


class ONF_StepCreateRasterMosaic : public CT_AbstractStep
{
    Q_OBJECT
public:
    ONF_StepCreateRasterMosaic(CT_StepInitializeData &dataInit);

    ~ONF_StepCreateRasterMosaic();

    QString getStepDescription() const;

    QString getStepDetailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);


protected:

    virtual void createInResultModelListProtected();

    virtual void createOutResultModelListProtected();

    virtual void createPostConfigurationDialog();

    virtual void compute();

private:

    Eigen::Vector2d _min;
    double _resolution;

    CT_AutoRenameModels     _outRasterModelName;

};

#endif // ONF_STEPCREATERASTERMOSAIC_H
