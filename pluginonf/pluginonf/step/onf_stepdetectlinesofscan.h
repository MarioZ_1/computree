/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPDETECTLINESOFSCAN_H
#define ONF_STEPDETECTLINESOFSCAN_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_circle2d.h"


// Inclusion of auto-indexation system
#include "ct_tools/model/ct_autorenamemodels.h"
#include "ct_point.h"
#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_itemdrawable/abstract/ct_abstractpointattributesscalar.h"

#include "Eigen/Core"
#include <QMutex>

class CT_StandardItemGroup;


class ONF_StepDetectLinesOfScan: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ONF_StepDetectLinesOfScan(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);



protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

protected:

    class ScanLineData : public QList<size_t> {
    public:
        ScanLineData() : QList<size_t>()
        {
            _length = 0;
            _centerX = 0;
            _centerY = 0;
            _centerZ = 0;
        }

        ScanLineData(const ScanLineData& line) : QList<size_t>(line)
        {
            _length = line._length;
            _centerX = line._centerX;
            _centerY = line._centerY;
            _centerZ = line._centerZ;
        }

        ScanLineData(double length, double centerX, double centerY, double centerZ) : QList<size_t>()
        {
            _length = length;
            _centerX = centerX;
            _centerY = centerY;
            _centerZ = centerZ;
        }

        ScanLineData(const QList<size_t> &list, double length, double centerX, double centerY, double centerZ) : QList<size_t>(list)
        {
            _length = length;
            _centerX = centerX;
            _centerY = centerY;
            _centerZ = centerZ;
        }

        ScanLineData& operator=(const ScanLineData& arg)
        {
            this->append(arg);
            this->_length = arg._length;
            this->_centerX = arg._centerX;
            this->_centerY = arg._centerY;
            this->_centerZ = arg._centerZ;
            return *this;
        }

        double _length;
        double _centerX;
        double _centerY;
        double _centerZ;

    };

    static bool orderLinesByAscendingNumberAndLength(ScanLineData *l1, ScanLineData *l2)
    {
        if (l1->size() < l2->size()) {return true;}
        if (l1->size() > l2->size()) {return false;}
        return l1->_length < l2->_length;
    }

    struct LineData {
        LineData(const CT_Point &pLow, const CT_Point &pHigh, size_t index1, size_t index2, float phi, double bottomLevel, double topLevel)
        {
            _processed = false;
            _distSum = 0;

            _index1 = index1;
            _index2 = index2;
            _pLow = pLow;
            _pHigh = pHigh;
            _phi = phi;

            Eigen::Vector3d dir = pHigh - pLow;
            dir.normalize();

            double t = (bottomLevel - pLow(2)) / dir(2);

            _lowCoord(0) = pLow(0) + t*dir(0);
            _lowCoord(1) = pLow(1) + t*dir(1);
            _lowCoord(2) = bottomLevel;

            t = (topLevel - pLow(2)) / dir(2);

            _highCoord(0) = pLow(0) + t*dir(0);
            _highCoord(1) = pLow(1) + t*dir(1);
            _highCoord(2) = topLevel;
        }

        float _phi;
        size_t _index1;
        size_t _index2;
        Eigen::Vector3d _pLow;
        Eigen::Vector3d _pHigh;
        Eigen::Vector3d _lowCoord;
        Eigen::Vector3d _highCoord;


        QList<ONF_StepDetectLinesOfScan::LineData*> _neighbors;
        bool                                                 _processed;
        double                                               _distSum;

        inline size_t neighborsCount() const {return _neighbors.size();}

    };


    class AlignmentsDetectorForScene
    {
    public:

        AlignmentsDetectorForScene(ONF_StepDetectLinesOfScan* step, CT_ResultGroup* res)
        {
            _step = step;
            _res = res;
        }

        typedef void result_type;
        void operator()(CT_StandardItemGroup* grp)
        {
            detectAlignmentsForScene(grp);
        }

        void detectAlignmentsForScene(CT_StandardItemGroup* grp);
                        

        void sortIndicesByGPSTime(const CT_AbstractPointCloudIndex* pointCloudIndexLAS,
                                  const CT_AbstractPointAttributesScalar* attributeGPS,
                                  const CT_AbstractPointCloudIndex* pointCloudIndex,
                                  QMultiMap<double, size_t> &sortedIndices);

        void createLinesOfScan(const QMultiMap<double, size_t> &sortedIndices,
                               QList<QList<size_t> > &linesOfScan,
                               QList<size_t> &isolatedPointIndices);

        void filterLinesOfScan(QList<QList<size_t> > &simplifiedLinesOfScan,
                               double thresholdZenithalAngleRadians,
                               QList<ScanLineData *> &keptLinesOfScan,
                               QList<size_t> &isolatedPointIndices);

        void denoiseLinesOfScan(const QList<QList<size_t> > &linesOfScan,
                                const CT_AbstractPointCloudIndex* pointCloudIndexLAS,
                                const CT_AbstractPointAttributesScalar* attributeIntensity,
                                QList<QList<size_t> > &simplifiedLinesOfScan,
                                QList<size_t> &isolatedPointIndices);

    private:
        ONF_StepDetectLinesOfScan*  _step;
        CT_ResultGroup*             _res;
    };


    QMutex                          _mutex;

    // Declaration of autoRenames Variables (groups or items added to In models copies)
    CT_AutoRenameModels    _grpClusterAll_ModelName;
    CT_AutoRenameModels    _grpClusterDenoised_ModelName;
    CT_AutoRenameModels    _grpClusterKept_ModelName;
    CT_AutoRenameModels    _clusterAll_ModelName;
    CT_AutoRenameModels    _clusterDenoised_ModelName;
    CT_AutoRenameModels    _clusterKept_ModelName;


    // Step parameters
    double      _thresholdGPSTime;
    double      _maxCurvature;
    double      _maxXYDist;
    double      _thresholdZenithalAngle;
    int         _minPts;

};

#endif // ONF_STEPDETECTLINESOFSCAN_H
