<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ONF_ActionAdjustPlotPosition</name>
    <message>
        <location filename="../actions/onf_actionadjustplotposition.cpp" line="56"/>
        <source>Tree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionadjustplotposition.cpp" line="917"/>
        <source>No selected Tree&lt;br&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionAdjustPlotPosition02</name>
    <message>
        <location filename="../actions/onf_actionadjustplotposition02.cpp" line="56"/>
        <source>Tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionadjustplotposition02.cpp" line="57"/>
        <source>DSM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionadjustplotposition02.cpp" line="1064"/>
        <source>No selected Tree&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_ActionAdjustPlotPositionOptions</name>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="94"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="127"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="153"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="413"/>
        <source> m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="124"/>
        <source>dX= </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="150"/>
        <source>dY= </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="172"/>
        <source>RAZ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="186"/>
        <source> biggest trees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="189"/>
        <source>Highlight the </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="209"/>
        <source>Move trees individually</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="338"/>
        <source>Hide pts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="355"/>
        <source>Intensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="365"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="385"/>
        <source>Last pts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="399"/>
        <source>Circles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="406"/>
        <source>Hfixed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions.ui" line="430"/>
        <source>Hmax</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionAdjustPlotPositionOptions02</name>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="94"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="127"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="153"/>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="491"/>
        <source> m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="124"/>
        <source>dX= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="150"/>
        <source>dY= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="172"/>
        <source>RAZ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="186"/>
        <source> biggest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="189"/>
        <source>Highlight </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="209"/>
        <source>Move trees individually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="216"/>
        <source>Apex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="242"/>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="413"/>
        <source>Hide pts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="430"/>
        <source>Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="440"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="460"/>
        <source>Last pts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="474"/>
        <source>Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="481"/>
        <source>Hfixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="507"/>
        <source>Radius factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="543"/>
        <source>Hmax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="600"/>
        <source>Plot ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="620"/>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionadjustplotpositionoptions02.ui" line="637"/>
        <source>Comments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_ActionAggregateItems</name>
    <message>
        <location filename="../actions/onf_actionaggregateitems.cpp" line="32"/>
        <location filename="../actions/onf_actionaggregateitems.cpp" line="37"/>
        <source>Regroupement d&apos;items</source>
        <translation>Item grouping</translation>
    </message>
</context>
<context>
    <name>ONF_ActionAggregateItemsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptions.ui" line="74"/>
        <source>Items</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptions.ui" line="93"/>
        <source>Next</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionAggregateItemsOptionsSelectionDialog</name>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptionsselectiondialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptionsselectiondialog.ui" line="35"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionaggregateitemsoptionsselectiondialog.ui" line="54"/>
        <source>Modality</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionDefineHeightLayerOptions</name>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="71"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="84"/>
        <source>Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="127"/>
        <source>1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="140"/>
        <source>10 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="150"/>
        <source>1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="160"/>
        <source>1 mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="219"/>
        <source>Zmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="232"/>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="252"/>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="297"/>
        <source> m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="242"/>
        <source>Zmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="273"/>
        <source>Seuil</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.ui" line="290"/>
        <source>Résolution</source>
        <translation>Resolution</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.cpp" line="56"/>
        <source>Zmin= %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actiondefineheightlayeroptions.cpp" line="57"/>
        <source>Zmax= %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionManualInventory</name>
    <message>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="82"/>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="87"/>
        <source>Inventaire Manuel</source>
        <translation>Manual inventory</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="290"/>
        <source>Attributs du cercle actif :</source>
        <translation>Active circle&apos;s attributes:</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="292"/>
        <source>H = %1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="294"/>
        <source>D = %1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="303"/>
        <location filename="../actions/onf_actionmanualinventory.cpp" line="332"/>
        <source>%1 = %2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionManualInventoryAttributesDialog</name>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryattributesdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryattributesdialog.ui" line="41"/>
        <source>&lt;b&gt;Saisie des attributs supplémentaires :&lt;/b&gt;</source>
        <translation>Additional attributes input</translation>
    </message>
</context>
<context>
    <name>ONF_ActionManualInventoryOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="86"/>
        <source>Séléction</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="99"/>
        <source>Choix cercle</source>
        <translation>Choose circle</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="109"/>
        <source>Saisie attributs</source>
        <translation>Input attributes</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="205"/>
        <source>Synchro. Caméra</source>
        <translation>Camera synchro</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="262"/>
        <source>Cercles :</source>
        <translation>Circles:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="269"/>
        <source>Actifs</source>
        <translation>Actives</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="279"/>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="326"/>
        <source>Autres</source>
        <translation>Others</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="289"/>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="333"/>
        <source>Poubelle</source>
        <translation>Trash</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="309"/>
        <source>Scènes :</source>
        <translation>Scenes:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="316"/>
        <source>Active</source>
        <translation>Actives</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="347"/>
        <source>Attributs</source>
        <translation>Attributes</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.ui" line="354"/>
        <source>Validation Auto</source>
        <translation>Auto validation</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="38"/>
        <source>Séléction de la scène active, sans modification [S]</source>
        <translation>Selection of active scene, without modifications [S]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="39"/>
        <source>Choix d&apos;un cercle pour une scène [D]</source>
        <translation>Choose circle for one scene [D]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="40"/>
        <source>Ouverture de la saisie des attributs pour une scène [F] ou [C] pour la scène active</source>
        <translation>Open attributes input for the active scene [F] or [C]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="41"/>
        <source>Séléctionne le cercle supérieur (CTRL MOLETTE +)</source>
        <translation>Select upper circle (CTRL WHEEL +)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="42"/>
        <source>Séléctionne le cercle inférieur (CTRL MOLETTE -)</source>
        <translation>Select lower circle (CTRL WHEEL -)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmanualinventoryoptions.cpp" line="43"/>
        <source>Valide la scène active [V] ou [Fin]</source>
        <translation>Validate active scene [V] or [End]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyAffiliationsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="142"/>
        <source>Afficher items :</source>
        <translation>Show items:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="152"/>
        <source>De référence</source>
        <translation>Reference</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="165"/>
        <source>A affilier</source>
        <translation>To affiliate</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="185"/>
        <source>Lignes</source>
        <translation>Lines</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="195"/>
        <source>Items</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="205"/>
        <source>Centres</source>
        <translation>Centers</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="215"/>
        <source>Uniquement sélectionnés</source>
        <translation>Selected ones only</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.ui" line="222"/>
        <source>Attributs</source>
        <translation>Attributes</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="48"/>
        <source>Afficher les lignes reliant les items affiliés</source>
        <translation>Show lines linking affiliated items</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="49"/>
        <source>N&apos;afficher que les items sélectionnés (de référence et à affilier</source>
        <translation>Only show selected items (reference and to affiliate ones)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="50"/>
        <source>Afficher les centres des items</source>
        <translation>Show centers of items</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="51"/>
        <source>Afficher les items</source>
        <translation>Show items</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="52"/>
        <source>Afficher les items de référence</source>
        <translation>Show reference items</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="53"/>
        <source>Afficher les items à affilier</source>
        <translation>show items to affiliate</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="54"/>
        <source>Affilier l&apos;item de référence et l&apos;item à affilier sélectionnés (A)</source>
        <translation>Show selected reference and to affiliate items [A]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="55"/>
        <source>Supprimer l&apos;affiliation de l&apos;item à affilier sélectionné (Z)</source>
        <translation>Clear affiliation of selected to affiliate item [Z]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyaffiliationsoptions.cpp" line="57"/>
        <source>Activer la selection (S) :
- Clic gauche = sélectionner un item de référence
- Clic droit  = sélectionner un item à affilier</source>
        <translation>Activate selection [S]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyClustersGroups02</name>
    <message>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="84"/>
        <source>Sélection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="89"/>
        <source>Sélection d&apos;éléments</source>
        <translation>Elements selection</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="609"/>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="618"/>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="646"/>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="655"/>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="723"/>
        <location filename="../actions/onf_actionmodifyclustersgroups02.cpp" line="758"/>
        <source>Pas de liste pour la position</source>
        <translation>No list for the position</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyClustersGroupsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="20"/>
        <source>Sélection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="111"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="175"/>
        <source>Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="118"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="182"/>
        <source>visible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="205"/>
        <source>Autres</source>
        <translation>Others</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="222"/>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="239"/>
        <source>Poubelle</source>
        <translation>Trash</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="288"/>
        <source>Mode Frontière</source>
        <translation>Limit mode</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="338"/>
        <source>|-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="345"/>
        <source>/ 1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="358"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="381"/>
        <source>by :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="388"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="398"/>
        <source>10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="408"/>
        <source>100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="431"/>
        <source>Mode Affectation</source>
        <translation>Affiliation mode</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="462"/>
        <source>Sélectionner un élément</source>
        <translation>Select one element</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="465"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="495"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="529"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="559"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="586"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="623"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="646"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="669"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="692"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="492"/>
        <source>Sélectionner de multiple éléments</source>
        <translation>Select multiple elements</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="526"/>
        <source>Remplacer la séléction</source>
        <translation>Replace selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="556"/>
        <source>Ajouter à la séléction</source>
        <translation>Add to selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="583"/>
        <source>Supprimer de la séléction</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions.ui" line="719"/>
        <source>Mode ClusterSplit</source>
        <translation>ClusterSplit mode</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyClustersGroupsOptions02</name>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="20"/>
        <source>Sélection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="87"/>
        <source>Groupe A</source>
        <translation>Group A</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="132"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="197"/>
        <source>visible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="155"/>
        <source>Groupe Z</source>
        <translation>Group Z</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="229"/>
        <source>Autres groupes</source>
        <translation>Others groups</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="239"/>
        <source>? (en attente)</source>
        <translation>? (stand by)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="249"/>
        <source>Poubelle</source>
        <translation>Trash</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="305"/>
        <source>Sélectionner un élément</source>
        <translation>Select one element</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="308"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="338"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="372"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="402"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="429"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="514"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="537"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="560"/>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="583"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="335"/>
        <source>Sélectionner de multiple éléments</source>
        <translation>Select multiple elements</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="369"/>
        <source>Remplacer la séléction</source>
        <translation>Replace selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="399"/>
        <source>Ajouter à la séléction</source>
        <translation>Add to selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="426"/>
        <source>Supprimer de la séléction</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.ui" line="624"/>
        <source>Afficher validés</source>
        <translation>Show validated</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="37"/>
        <source>Choisit le groupe A [double-clic GAUCHE]</source>
        <translation>Choose group A [LEFT double-clic]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="38"/>
        <source>Choisit le groupe B [double-clic DROIT]</source>
        <translation>Choose group B [RIGHT double-clic]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="39"/>
        <source>Etendre la séléction [SHIFT]</source>
        <translation>Extend selection [SHIFT]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="40"/>
        <source>Maintenir [CTRL] enfoncé pour activer temporairement</source>
        <translation>Maintain [CTRL] down to temporarily activate</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="42"/>
        <source>Affecte les points au groupe A [A]</source>
        <translation>Send points to group A [A]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="43"/>
        <source>Affecte les points au groupe Z [Z]</source>
        <translation>Send points to group Z [Z]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="44"/>
        <source>Affecte les points au groupe ? [E]</source>
        <translation>Send points to group ? [E]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="45"/>
        <source>Affecte les points à la poubelle [R]</source>
        <translation>Send points to trash [R]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="47"/>
        <source>Touche [espace] pour changer</source>
        <translation>[SPACE] key to switch</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyclustersgroupsoptions02.cpp" line="49"/>
        <source>Valider le groupe Z [V]</source>
        <translation>Validate group Z [V]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyDEM</name>
    <message>
        <location filename="../actions/onf_actionmodifydem.cpp" line="74"/>
        <location filename="../actions/onf_actionmodifydem.cpp" line="79"/>
        <source>DEM modification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmodifydem.cpp" line="280"/>
        <source>Merci de patienter pendant l&apos;interpolation
N.B. : en cas d&apos;interruption, les modifications déjà effectuées ne seront PAS annulées</source>
        <translation>Please wait during interpolation
N.B.: If you stop the process, modifications will NOT be cancelled</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmodifydem.cpp" line="280"/>
        <source>Interrompre l&apos;interpolation</source>
        <translation>Stop interpolation</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionmodifydem.cpp" line="281"/>
        <source>Interpolation</source>
        <translation>Interpolation</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyDEMOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="20"/>
        <source>Sélection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="65"/>
        <source>Sélectionner un élément</source>
        <translation>Select one element</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="68"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="98"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="132"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="162"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="189"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="322"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="339"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="356"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="373"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="390"/>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="442"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="95"/>
        <source>Sélectionner de multiple éléments</source>
        <translation>Select multiple elements</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="129"/>
        <source>Remplacer la séléction</source>
        <translation>Replace selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="159"/>
        <source>Ajouter à la séléction</source>
        <translation>Add to selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="186"/>
        <source>Supprimer de la séléction</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="244"/>
        <source>Taille pinceau : </source>
        <translation>Pencil size:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="257"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="280"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="416"/>
        <source>by </source>
        <translation>par </translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.ui" line="419"/>
        <source> m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="37"/>
        <source>Augmenter la hauteur des points sélectionnés [FLECHE DROITE]</source>
        <translation>Increase height of selected points [RIGHT ARROW]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="38"/>
        <source>Augmenter la hauteur des points sélectionnés, sans dépasser la hauteur du plus haut [FLECHE HAUT]</source>
        <translation>Increase height of selected points, without exceeding the highest one [UP ARROW]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="39"/>
        <source>Diminuer la hauteur des points sélectionnés [FLECHE GAUCHE]</source>
        <translation>Decrease the height of selected points [LEFT ARROW]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="40"/>
        <source>Diminuer la hauteur des points sélectionnés, sans descendre sous la hauteur du plus bas [FLECHE BAS]</source>
        <translation>Decrease the height of selected points, without exceeding lowest one [DOWN ARROW]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="41"/>
        <source>Réinitialiser la hauteur des points sélectionnés [RETOUR ARRIERE]</source>
        <translation>Reset selected points height [BACKSPACE]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="42"/>
        <source>Incrément: [+] pour augmenter, [-] pour diminuer</source>
        <translation>Step: [+] increase, [-] decrease</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifydemoptions.cpp" line="43"/>
        <source>Interpoler les points sélectionnés, à partir des points les entourant</source>
        <translation>Interpolate selected points, from surrounding ones</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyPositions2D</name>
    <message>
        <location filename="../actions/onf_actionmodifypositions2d.cpp" line="60"/>
        <location filename="../actions/onf_actionmodifypositions2d.cpp" line="65"/>
        <source>Modifier positions 2D</source>
        <translation>Modify 2D positions</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyPositions2DOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="180"/>
        <source> m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="203"/>
        <source>1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="213"/>
        <source>10 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="226"/>
        <source>1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="243"/>
        <source>Dessiner</source>
        <translation>Draw</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="250"/>
        <source>Plan</source>
        <translation>Plane</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.ui" line="260"/>
        <source>Lignes</source>
        <translation>Lines</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.cpp" line="38"/>
        <source>Déplacement normal dans la vue [F]</source>
        <translation>Normal view moving [F]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.cpp" line="39"/>
        <source>Modification d&apos;une position [D]</source>
        <translation>Move 2D position [D]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.cpp" line="40"/>
        <source>Ajout d&apos;une position [A]</source>
        <translation>Add 2D position [A]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifypositions2doptions.cpp" line="41"/>
        <source>Suppression d&apos;une position [S]</source>
        <translation>Remove 2D position [S]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyVoxelSegmentation</name>
    <message>
        <location filename="../actions/onf_actionmodifyvoxelsegmentation.cpp" line="94"/>
        <location filename="../actions/onf_actionmodifyvoxelsegmentation.cpp" line="99"/>
        <source>Segmentation</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionModifyVoxelSegmentationOptions</name>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="20"/>
        <source>Sélection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="87"/>
        <source>Groupe A</source>
        <translation>Group A</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="132"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="197"/>
        <source>visible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="155"/>
        <source>Groupe Z</source>
        <translation>Group Z</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="229"/>
        <source>Autres groupes</source>
        <translation>Others groups</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="239"/>
        <source>Poubelle</source>
        <translation>Trash</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="262"/>
        <source>Label A :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="312"/>
        <source>Sélectionner un élément</source>
        <translation>Select one element</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="315"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="345"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="379"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="409"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="436"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="538"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="561"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="584"/>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="607"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="342"/>
        <source>Sélectionner de multiple éléments</source>
        <translation>Select multiple elements</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="376"/>
        <source>Remplacer la séléction</source>
        <translation>Replace selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="406"/>
        <source>Ajouter à la séléction</source>
        <translation>Add to selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="433"/>
        <source>Supprimer de la séléction</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="648"/>
        <source>Afficher validés</source>
        <translation>Show validated</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.ui" line="658"/>
        <source>Séléctionner Validés</source>
        <translation>Select validated</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="37"/>
        <source>Choisit le groupe A [ALT clic GAUCHE]</source>
        <translation>Choose group A [ALT LEFT button]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="38"/>
        <source>Choisit le groupe Z [ALT clic DROIT]</source>
        <translation>Choose group Z [ALT RIGHT button]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="39"/>
        <source>Etendre la séléction [X]</source>
        <translation>Extend selection [X]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="40"/>
        <source>Maintenir [CTRL] enfoncé pour activer temporairement</source>
        <translation>Maintain [CTRL] down to temporarily activate</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="42"/>
        <source>Affecte les points au groupe A [A]</source>
        <translation>Send points to group A [A]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="43"/>
        <source>Affecte les points au groupe Z [Z]</source>
        <translation>Send points to group Z [Z]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="44"/>
        <source>Affecte les points à un nouveau groupe [E]</source>
        <translation>Send points in a new created group [E]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="45"/>
        <source>Affecte les points à la poubelle [R]</source>
        <translation>Send points to trash [R]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="47"/>
        <source>Touche [espace] pour changer</source>
        <translation>[SPACE] key to switch</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionmodifyvoxelsegmentationoptions.cpp" line="49"/>
        <source>Valider le groupe A [V]</source>
        <translation>Validate groupe A [V]</translation>
    </message>
    <message>
        <source>Valider le groupe Z [V]</source>
        <translation type="vanished">Validate group Z [V]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSegmentCrowns</name>
    <message>
        <location filename="../actions/onf_actionsegmentcrowns.cpp" line="628"/>
        <source>Caméra centrée en</source>
        <translation>Camera centered in</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionsegmentcrowns.cpp" line="917"/>
        <source>Cluster = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionsegmentcrowns.cpp" line="918"/>
        <source>MNS : Z = %1 m</source>
        <translation>DSM: Z = %1 m</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionsegmentcrowns.cpp" line="919"/>
        <source>Densité = %1 pts</source>
        <translation>Density = %1 pts</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSegmentCrownsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="161"/>
        <source>Switch in &quot;free move&quot; mode (shortcut : f)</source>
        <translation>Switch in &quot;free move&quot; mode [F]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="220"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="230"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="240"/>
        <source>Density</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="257"/>
        <source>Taille :</source>
        <translation>Size:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="352"/>
        <source>Active Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="432"/>
        <source>Only shows selected cells</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="456"/>
        <source>CRTL Mousewheel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="472"/>
        <source>1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="485"/>
        <source>10 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="495"/>
        <source>1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.ui" line="529"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="39"/>
        <source>Choisir le cluster actif (S = Select)</source>
        <translation>Choose active cluster (S = Select)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="40"/>
        <source>Dessiner une limite (D = Draw)</source>
        <translation>Draw a limit (D = Draw)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="41"/>
        <source>Inclure une zone dans le cluster actif (F = Fill)</source>
        <translation>Include an enclosed area in active cluster (F = Fill)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="42"/>
        <source>Naviguer en 3D (G)</source>
        <translation>3D Navigation (G)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="43"/>
        <source>Recentrer la vue sur la case sélectionnée</source>
        <translation>Center view on a selected cell</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="44"/>
        <source>Ajouter un nouveau cluster et le rendre actif</source>
        <translation>Add a new cluster and activate it</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="45"/>
        <source>Couleur du cluster actif (attention les couleurs sont recyclées)
Si enfoncé : le cluster actif est tracé en rouge, même en mode cluster</source>
        <translation>Active cluster color (warning: color are recylcled)
If down: active cluster in red, even in cluster mode</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="47"/>
        <source>Afficher / masquer les cellules vides</source>
        <translation>Show/Hide empty cells</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="48"/>
        <source>Choisir le mode de remplissage (pot de peinture) :
- Remplir uniquement la zone sélectionnée
- Remplir tous les pixels du cluster</source>
        <translation>Choose fill mode (paint can)
- Fill only selected area
- Fill all pixels of the cluster
</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="51"/>
        <source>Agrèges chaque petit cluster (&lt; n pixels) avec le grand cluster le plus proche</source>
        <translation>Merge each small cluster (&lt; n pixels) with nearest big cluster (&gt;= n)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="52"/>
        <source>Taille maximale d&apos;un petit cluster (en pixels)</source>
        <translation>Maximum size for a small cluster (in pixels)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="54"/>
        <source>Annuler (CTRL+Z)</source>
        <translation>Undo (CTRL+Z)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="55"/>
        <source>Rétablir (CTRL+Y)</source>
        <translation>Redo (CTRL+Y)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="57"/>
        <source>Afficher les clusters</source>
        <translation>Show clusters</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="58"/>
        <source>Afficher la carte de densité</source>
        <translation>Show density map</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="59"/>
        <source>Afficher le Modèle Numérique de Surface</source>
        <translation>Show Digital Surface Model</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="61"/>
        <source>Ajuster le plan de visualisation par étapes de 1 mètre</source>
        <translation>Ajust visualization plane by 1 meter steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="62"/>
        <source>Ajuster le plan de visualisation par étapes de 10 cm</source>
        <translation>Ajust visualization plane by 10 cm steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="63"/>
        <source>Ajuster le plan de visualisation par étapes de 1 cm</source>
        <translation>Ajust visualization plane by 1 cm steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="66"/>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="67"/>
        <source>Changer de cluster actif</source>
        <translation>Change active cluster</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="68"/>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="69"/>
        <source>Choisir la hauteur du plan de visualisation (CTRL + molette)</source>
        <translation>Move visualization plane upper or lower  (CTRL + wheel)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="70"/>
        <location filename="../views/actions/onf_actionsegmentcrownsoptions.cpp" line="71"/>
        <source>Changer la taille de pinceau (SHIFT + molette)</source>
        <translation>Change brush size (SHIFT + wheel)</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSegmentGaps</name>
    <message>
        <location filename="../actions/onf_actionsegmentgaps.cpp" line="673"/>
        <source>Caméra centrée en</source>
        <translation>Camera centered in</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSegmentGapsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="161"/>
        <source>Switch in &quot;free move&quot; mode (shortcut : f)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="220"/>
        <source>Noir</source>
        <translation>Black</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="230"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="240"/>
        <source>Density</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="257"/>
        <source>Taille :</source>
        <translation>Size:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="352"/>
        <source>Active Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="432"/>
        <source>Only shows selected cells</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="456"/>
        <source>CRTL Mousewheel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="472"/>
        <source>1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="485"/>
        <source>10 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="495"/>
        <source>1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.ui" line="529"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="39"/>
        <source>Choisir le cluster actif (S = Select)</source>
        <translation>Choose active cluster (S = Select)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="40"/>
        <source>Dessiner une limite (D = Draw)</source>
        <translation>Draw a limit (D = Draw)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="41"/>
        <source>Inclure une zone dans le cluster actif (F = Fill)</source>
        <translation>Include an enclosed area in active cluster (F = Fill)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="42"/>
        <source>Naviguer en 3D (G)</source>
        <translation>3D Navigation (G)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="43"/>
        <source>Recentrer la vue sur la case sélectionnée</source>
        <translation>Center view on a selected cell</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="44"/>
        <source>Ajouter un nouveau cluster et le rendre actif</source>
        <translation>Add a new cluster and activate it</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="45"/>
        <source>Couleur du cluster actif (attention les couleurs sont recyclées)
Si enfoncé : le cluster actif est tracé en rouge, même en mode cluster</source>
        <translation>Active cluster color (warning: color are recylcled)
If down: active cluster in red, even in cluster mode</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="47"/>
        <source>Afficher / masquer les cellules vides</source>
        <translation>Show/Hide empty cells</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="48"/>
        <source>Choisir le mode de remplissage (pot de peinture) :
- Remplir uniquement la zone sélectionnée
- Remplir tous les pixels du cluster</source>
        <translation>Choose fill mode (paint can)
- Fill only selected area
- Fill all pixels of the cluster
</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="51"/>
        <source>Agrèges chaque petit cluster (&lt; n pixels) avec le grand cluster le plus proche</source>
        <translation>Merge each small cluster (&lt; n pixels) with nearest big cluster (&gt;= n)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="52"/>
        <source>Taille maximale d&apos;un petit cluster (en pixels)</source>
        <translation>Maximum size for a small cluster (in pixels)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="54"/>
        <source>Annuler (CTRL+Z)</source>
        <translation>Undo (CTRL+Z)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="55"/>
        <source>Rétablir (CTRL+Y)</source>
        <translation>Redo (CTRL+Y)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="57"/>
        <source>Afficher les clusters</source>
        <translation>Show clusters</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="58"/>
        <source>Afficher la carte de densité</source>
        <translation>Show density map</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="59"/>
        <source>Afficher le Modèle Numérique de Surface</source>
        <translation>Show Digital Surface Model</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="61"/>
        <source>Ajuster le plan de visualisation par étapes de 1 mètre</source>
        <translation>Ajust visualization plane by 1 meter steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="62"/>
        <source>Ajuster le plan de visualisation par étapes de 10 cm</source>
        <translation>Ajust visualization plane by 10 cm steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="63"/>
        <source>Ajuster le plan de visualisation par étapes de 1 cm</source>
        <translation>Ajust visualization plane by 1 cm steps</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="66"/>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="67"/>
        <source>Changer de cluster actif</source>
        <translation>Change active cluster</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="68"/>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="69"/>
        <source>Choisir la hauteur du plan de visualisation (CTRL + molette)</source>
        <translation>Move visualization plane upper or lower  (CTRL + wheel)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="70"/>
        <location filename="../views/actions/onf_actionsegmentgapsoptions.cpp" line="71"/>
        <source>Changer la taille de pinceau (SHIFT + molette)</source>
        <translation>Change brush size (SHIFT + wheel)</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectCellsInGrid3D</name>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="68"/>
        <source>Sélectionne des cases dans une grille 3D</source>
        <translation>Select cells in 3D grid</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="494"/>
        <source>Extension validée</source>
        <translation>Expansion validated</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="506"/>
        <source>Colonization validée</source>
        <translation>Colonization validated</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="523"/>
        <source>Caméra centrée en</source>
        <translation>Camera centered in</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="562"/>
        <source>Opération annulée</source>
        <translation>Action cancelled</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="828"/>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="839"/>
        <source>Press &quot;y&quot; to apply, or &quot;n&quot; to cancel !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="836"/>
        <source>Select a cell as new view center !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="874"/>
        <source>Changement de mode de cumul pris en compte</source>
        <translation>Modify accumulation mode in cells</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="901"/>
        <source>Niveau en cours propagé sur l&apos;épaisseur (thickness)</source>
        <translation>Extends active level to the active thickness</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectcellsingrid3d.cpp" line="908"/>
        <source>Niveau en cours propagé sur TOUTE la grille</source>
        <translation>Extends active level to the complete grid</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectCellsInGrid3DColonizeDialog</name>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="23"/>
        <source>Directions de colonization autorisées :</source>
        <translation>Allowed directions for colonization:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="30"/>
        <source>X+ : vers la droite</source>
        <translation>X+: to the right</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="40"/>
        <source>X- : vers la gauche</source>
        <translation>X-: to the left</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="50"/>
        <source>Y+ : vers l&apos;avant</source>
        <translation>Y+: forwards</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="60"/>
        <source>Y- : vers l&apos;arrière</source>
        <translation>Y-: backwards</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="70"/>
        <source>Z+ : vers le haut</source>
        <translation>Z+:upper</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3dcolonizedialog.ui" line="80"/>
        <source>Z- : vers le bas</source>
        <translation>Z-: lower</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectCellsInGrid3DOptions</name>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="108"/>
        <source>Switch in &quot;add to selection&quot; mode (shortcut : a)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="134"/>
        <source>Switch in &quot;colonize adjacent and not empty values&quot; mode (shortcut : z)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="160"/>
        <source>Switch in &quot;Extends from selected cell&quot; mode (shortcut : e)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="186"/>
        <source>Switch in &quot;remove from selection&quot; mode (shortcut : r)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="212"/>
        <source>Switch in &quot;free move&quot; mode (shortcut : f)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="271"/>
        <source>Only shows selected cells</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="352"/>
        <source>CRTL Mousewheel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="368"/>
        <source>SHIFT Mousewheel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="402"/>
        <source>Set maximum level and thickness</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="405"/>
        <source>max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="415"/>
        <source>Copy active level selection to all visible levels (see thickness)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="429"/>
        <source>Copy active level selection to ALL levels of the grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="449"/>
        <source>Select &gt; 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="456"/>
        <source>Convex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="490"/>
        <source>%2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.ui" line="528"/>
        <source>Coef.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="42"/>
        <source>Change de mode : 2D / 3D (D = 2D / 3D)</source>
        <translation>Switch mode: 2D / 3D [D]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="44"/>
        <source>Ajouter des cellules (A = Add)</source>
        <translation>Add cells [A = Add]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="45"/>
        <source>Supprimer des cellules (R = Remove)</source>
        <translation>Remove cells [R = Remove]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="46"/>
        <source>Déplacement de la vue (F = Free Move)</source>
        <translation>Move the view [F = Free move]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="47"/>
        <source>Extension jusqu&apos;aux limites (E = Extends)
En mode 3D, cet outil ne fonctionne que sur des cellules séléctionnées</source>
        <translation>Expansion towards limits [E = Extends]
In 3D mode, this tool only works on selected cells</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="48"/>
        <source>Colonization des valeurs non vides (Z = coloniZe)</source>
        <translation>Colonization to not empty values [Z = coloniZe]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="49"/>
        <source>Sélection d&apos;une case pour centrer la vue</source>
        <translation>Center view on a selected cell</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="51"/>
        <source>Monter toute les cellules / les cellules sélectionnées</source>
        <translation>Show all cells / only selected cells</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="52"/>
        <source>Grille visible / masquée</source>
        <translation>Show/Hide grid</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="53"/>
        <source>Cumuler tous les niveaux</source>
        <translation>Accumulate all levels</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="54"/>
        <source>Copier le niveau actuel à toute la grille (ATTENTION écrasement des valeurs)</source>
        <translation>Copy active level to all the grid (WARNING : old values will be lost)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="55"/>
        <source>Copier le niveau actuel aux niveaux actuellement cumulés (ATTENTION écrasement des valeurs)</source>
        <translation>Copy active level to currently accumulated levels (WARNING : old values will be lost)</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="57"/>
        <source>Couleur des voxels non vides et non séléctionnés</source>
        <translation>Color for not empty and not selected voxels</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="59"/>
        <source>Mode de cumul des niveaux</source>
        <translation>Mode for levels accumulation</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="60"/>
        <source>Echelle de couleur calée sur le niveau en cours</source>
        <translation>Color scale computed on active level</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="62"/>
        <source>Niveau Z affiché, ou le plus bas si épaisseur &gt; 1 (CTRL MOLETTE)</source>
        <translation>Z level, or bottom level if thickness &gt; 1 [CTRL wheel]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="63"/>
        <source>Nombre de niveaux cumulés = épaisseur (SHIFT MOLETTE)</source>
        <translation>Number of accumulated levels = thickness [SHIFT wheel]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectcellsingrid3doptions.cpp" line="64"/>
        <source>Facteur de réduction de la taille des cellules 3D</source>
        <translation>Reduction factor for voxels drawing</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectClustersInLogs</name>
    <message>
        <location filename="../actions/onf_actionselectclustersinlogs.cpp" line="39"/>
        <source>Sélection</source>
        <translation type="unfinished">Selection</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionselectclustersinlogs.cpp" line="44"/>
        <source>Sélection d&apos;éléments</source>
        <translation type="unfinished">Elements selection</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectClustersInLogsOptions</name>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="20"/>
        <source>Sélection</source>
        <translation type="unfinished">Selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="38"/>
        <source>Sélectionner un élément par un simple clique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="41"/>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="71"/>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="131"/>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="161"/>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="188"/>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="215"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="68"/>
        <source>Sélectionner de multiple éléments à l&apos;aide d&apos;un rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="95"/>
        <source>Sélectionner de multiple éléments à l&apos;aide d&apos;un polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="128"/>
        <source>Remplacer la séléction</source>
        <translation type="unfinished">Replace selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="158"/>
        <source>Ajouter à la séléction</source>
        <translation type="unfinished">Add to selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="185"/>
        <source>Supprimer de la séléction</source>
        <translation type="unfinished">Clear selection</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="212"/>
        <source>Inverser la sélection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="239"/>
        <source>Cluster</source>
        <translation type="unfinished">Cluster</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.ui" line="252"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.cpp" line="11"/>
        <source>Sélectionner des clusters [E]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.cpp" line="12"/>
        <source>Sélectionner des billlons [E]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.cpp" line="13"/>
        <source>Ajouter la sélection [A]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectclustersinlogsoptions.cpp" line="14"/>
        <source>Retirer la sélection [Z]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectSceneForEachPosition</name>
    <message>
        <location filename="../actions/onf_actionselectsceneforeachposition.cpp" line="702"/>
        <source>No selected Tree&lt;br&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_ActionSelectSceneForEachPositionOptions</name>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="72"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="89"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="106"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="123"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="147"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="253"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="164"/>
        <source> biggest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="167"/>
        <source>Highlight the </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="180"/>
        <source>Default Ht</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="187"/>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="225"/>
        <source> m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="197"/>
        <source>Circles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="204"/>
        <source>Colorize All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.ui" line="218"/>
        <source>Hide not selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.cpp" line="40"/>
        <source>Ajouter le cluster sélectionné au groupe actif [A]</source>
        <translation>Add selected  cluster to active group [A]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.cpp" line="41"/>
        <source>Retirer le cluster sélectionné du groupe actif [Z]</source>
        <translation>Remove selected cluster from active group [Z]</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionselectsceneforeachpositionoptions.cpp" line="42"/>
        <source>Remplacer le groupe actif par le cluster sélectionné [E]</source>
        <translation>Replace active group by selected cluster [E]</translation>
    </message>
</context>
<context>
    <name>ONF_ActionSlicePointCloudOptions</name>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="53"/>
        <source>Epaisseur des couches :</source>
        <translation>Slices thickness:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="76"/>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="110"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="90"/>
        <source>Espace entre les couches :</source>
        <translation>Space between slices:</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="124"/>
        <source>Incrément</source>
        <translation>Increment step</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="131"/>
        <source>1 mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="141"/>
        <source>1 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="151"/>
        <source>10 cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.ui" line="164"/>
        <source>1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.cpp" line="41"/>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.cpp" line="42"/>
        <source>CTRL Molette</source>
        <translation>CTRL Wheel</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.cpp" line="43"/>
        <location filename="../views/actions/onf_actionslicepointcloudoptions.cpp" line="44"/>
        <source>SHIFT Molette</source>
        <translation>SHIFT Wheel</translation>
    </message>
</context>
<context>
    <name>ONF_ActionValidateInventory</name>
    <message>
        <location filename="../actions/onf_actionvalidateinventory.cpp" line="63"/>
        <location filename="../actions/onf_actionvalidateinventory.cpp" line="68"/>
        <source>Validation Inventaire</source>
        <translation>Inventory validation</translation>
    </message>
    <message>
        <location filename="../actions/onf_actionvalidateinventory.cpp" line="272"/>
        <source>ID = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../actions/onf_actionvalidateinventory.cpp" line="274"/>
        <source>Espèce = %1</source>
        <translation>Species = %1</translation>
    </message>
</context>
<context>
    <name>ONF_ActionValidateInventoryAttributesDialog</name>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryattributesdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryattributesdialog.ui" line="26"/>
        <source>ID terrain</source>
        <translation>Field ID</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryattributesdialog.ui" line="33"/>
        <source>Espèce</source>
        <translation>Species</translation>
    </message>
</context>
<context>
    <name>ONF_ActionValidateInventoryOptions</name>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="96"/>
        <source>Choix Item</source>
        <translation>Item choice</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="109"/>
        <source>Saisie attributs</source>
        <translation>Attributes input</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="125"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="139"/>
        <source>Synchro. Caméra</source>
        <translation>Camera Synchro.</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="149"/>
        <source>Attributs</source>
        <translation>Attributes</translation>
    </message>
    <message>
        <location filename="../views/actions/onf_actionvalidateinventoryoptions.ui" line="159"/>
        <source>Items</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_FilterByReturnType</name>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="95"/>
        <source>Type de retours à conserver</source>
        <translation>Returns type to keep</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="97"/>
        <source>Filter selon la classification</source>
        <translation>Filter on classification</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="97"/>
        <source>Conserver les classifications suivantes :</source>
        <translation>Keep following classifications:</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="98"/>
        <source>Végétation (3,4,5)</source>
        <translation>Vegetation (3,4,5)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="99"/>
        <source>Sol (2)</source>
        <translation>Ground (2)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="100"/>
        <source>Non classés (0,1)</source>
        <translation>Not classified (0,1)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="101"/>
        <source>Constructions (6)</source>
        <translation>Constructs (6)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="102"/>
        <source>Eau (9)</source>
        <translation>Water (9)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="103"/>
        <source>Autres valeurs à conserver (séparées par des ;)</source>
        <translation>Others values to keep (separated by ;)</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="262"/>
        <source>Filter by return type and Classification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filter/onf_filterbyreturntype.cpp" line="267"/>
        <source>Filter by return type. You can specify following types:
- First: first returns
- Last: last returns
- LastAndOnly: last and only returns
- Intermediare: Returns which are not first neither last
- Only: first returns if no other return for the ray
- All : don&apos;t filter on return type

You can also choose which classifications to keep.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_FilterKeepLastReturnInSlice</name>
    <message>
        <location filename="../filter/onf_filterkeeplastreturninslice.cpp" line="53"/>
        <source>Z minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filter/onf_filterkeeplastreturninslice.cpp" line="54"/>
        <source>Z maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filter/onf_filterkeeplastreturninslice.cpp" line="146"/>
        <source>Garde de dernier retour de chaque rayon dans la tranche</source>
        <translation>Keep last return of each ray in the slice</translation>
    </message>
</context>
<context>
    <name>ONF_FilterRemoveUpperOutliers</name>
    <message>
        <location filename="../filter/onf_filterremoveupperoutliers.cpp" line="53"/>
        <source>Résolution de la grille de filtrage</source>
        <translation>Filtering grid resolution</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterremoveupperoutliers.cpp" line="54"/>
        <source>Nombre de points maximum d&apos;une cellule éliminée</source>
        <translation>Maximum number of points for a removed cell</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterremoveupperoutliers.cpp" line="55"/>
        <source>Nombre de cases verticales autorisées entre la case filtrée et le voisinnage inférieur</source>
        <translation>Number of allowed vertical cells between filtered cell and lower neighbourhood</translation>
    </message>
    <message>
        <location filename="../filter/onf_filterremoveupperoutliers.cpp" line="172"/>
        <source>Elimine les points au dessus de la canopée</source>
        <translation>Remove points above the canopy</translation>
    </message>
</context>
<context>
    <name>ONF_MetricComputeStats</name>
    <message>
        <source>Calcul des indicateurs statistiques standard</source>
        <translation type="vanished">Compute standard statistical indicators</translation>
    </message>
    <message>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- Hmean : Moyenne&lt;br&gt;- Hsd   : Ecart-type (non biaisé)&lt;br&gt;_ Hskew : Skewness  (non biaisé)&lt;br&gt;- Hkurt : Kurtosis  (non biaisé)&lt;br&gt;- Hcv   : Coefficient de variation&lt;br&gt;&lt;em&gt;N.B. : Les formules du Skewness et du Kurtosis sont issues du package e1071 de R (versions SAS).&lt;/em&gt;</source>
        <translation type="vanished">Following values are computed:&lt;br&gt;- Hmean: Mean&lt;br&gt;- Hsd: Standard Error (unbiased)&lt;br&gt;_ Hskew: Skewness  (unbiased)&lt;br&gt;- Hkurt: Kurtosis  (unbiased)&lt;br&gt;- Hcv: Coefficient of variation&lt;br&gt;&lt;em&gt;N.B.: Formulas for Skewness and Kurtosis comes from R package e1071 (SAS versions).&lt;/em&gt;</translation>
    </message>
    <message>
        <source>Calcul d&apos;indicateurs de diagnostique</source>
        <translation type="vanished">Compute diagnostic indicators</translation>
    </message>
    <message>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- N : Nombre total de points- N_first : Nombre de points First- N_last : Nombre de points Last- N_int : Nombre de points Intermediate- N_only : Nombre de points Only- N_error : Nombre de points en Erreur (nombre de retours ou numéro de retour aberrant)- N_ground : Nombre de points Sol- N_veg : Nombre de Végétation - N_other : Nombre de points Autres (ni Végétation, ni Sol)- Range : Zmax - Zmin- NumberOfLines : Nombre de lignes vols couvrant la placette- N_bestLine : Nombre de points de la ligne de vols ayant le plus de points- N_secondLine : Nombre de points de la seconde ligne de vols ayant le plus de points- N_worstLine : Nombre de points de la ligne de vols ayant le moins de points</source>
        <translation type="vanished">Following values are computed: &lt;br&gt;- N: Total number of points&lt;br&gt;- N_first: Number of First points&lt;br&gt;- N_last: Number of Last points&lt;br&gt;- N_int: Number of Intermediate points&lt;br&gt;- N_only: Number of Only points&lt;br&gt;- N_error: Number of points in error (number of returns or return number not valid)&lt;br&gt;- N_ground: Number of ground pointsl&lt;br&gt;- N_veg: Number of vegetation points&lt;br&gt;- N_other: Number of points from other classifications (not vegetation neither ground)&lt;br&gt;- Range: Zmax - Zmin&lt;br&gt;- NumberOfLines: Number of lines of flight on the plot&lt;br&gt;- N_bestLine: Numlber of points of the line of flight with the most points&lt;br&gt;- N_secondLine: Numlber of points of the line of flight with the second greatest number of points&lt;br&gt;- N_worstLine: Numlber of points of the line of flight with the less points</translation>
    </message>
</context>
<context>
    <name>ONF_MetricIntensity</name>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="46"/>
        <source>Métriques d&apos;intensité (LAS H ou LAS Z)</source>
        <translation>Intensity metrics (LAS H or LAS Z)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="451"/>
        <source>n</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="452"/>
        <source>n_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="453"/>
        <source>i_max_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="454"/>
        <source>i_mean_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="455"/>
        <source>i_sd_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="456"/>
        <source>i_cv_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="457"/>
        <source>n_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="458"/>
        <source>i_max_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="459"/>
        <source>i_mean_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="460"/>
        <source>i_sd_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="461"/>
        <source>i_cv_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="463"/>
        <source>n_5_95_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="464"/>
        <source>i_5_95_mean_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="465"/>
        <source>i_5_95_sd_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="466"/>
        <source>i_5_95_cv_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="467"/>
        <source>n_5_95_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="468"/>
        <source>i_5_95_mean_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="469"/>
        <source>i_5_95_sd_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="470"/>
        <source>i_5_95_cv_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="472"/>
        <source>n_10_90_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="473"/>
        <source>i_10_90_mean_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="474"/>
        <source>i_10_90_sd_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="475"/>
        <source>i_10_90_cv_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="476"/>
        <source>n_10_90_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="477"/>
        <source>i_10_90_mean_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="478"/>
        <source>i_10_90_sd_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="479"/>
        <source>i_10_90_cv_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="481"/>
        <source>n_top25_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="482"/>
        <source>i_top25_mean_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="483"/>
        <source>i_top25_sd_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="484"/>
        <source>i_top25_cv_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="485"/>
        <source>n_top25_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="486"/>
        <source>i_top25_mean_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="487"/>
        <source>i_top25_sd_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="488"/>
        <source>i_top25_cv_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="490"/>
        <source>i_p05_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="491"/>
        <source>i_p10_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="492"/>
        <source>i_p25_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="493"/>
        <source>i_p50_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="494"/>
        <source>i_p75_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="495"/>
        <source>i_p90_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="496"/>
        <source>i_p95_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="497"/>
        <source>i_p05_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="498"/>
        <source>i_p10_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="499"/>
        <source>i_p25_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="500"/>
        <source>i_p50_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="501"/>
        <source>i_p75_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="502"/>
        <source>i_p90_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricintensity.cpp" line="503"/>
        <source>i_p95_only</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricLASPointCrown</name>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="44"/>
        <source>Métriques de houppier (LAS H ou LAS Z)</source>
        <translation>Crown metrics (LAS H or LAS Z)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="49"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- X_Apex&lt;br&gt;- Y_Apex&lt;br&gt;- Z_Apex&lt;br&gt;- Pen_1m (Penetration rate in upper 1st meter)&lt;br&gt;- Pen_2m (Penetration rate in upper 2nd meter)&lt;br&gt;- Pen_3m (Penetration rate in upper 3rd meter)&lt;br&gt;- Pen_4m (Penetration rate in upper 4th meter)&lt;br&gt;- Pen_5m (Penetration rate in upper 5th meter)&lt;br&gt;</source>
        <translation>Following values are computed:&lt;br&gt;- X_Apex&lt;br&gt;- Y_Apex&lt;br&gt;- Z_Apex&lt;br&gt;- Pen_1m (Penetration rate in upper 1st meter)&lt;br&gt;- Pen_2m (Penetration rate in upper 2nd meter)&lt;br&gt;- Pen_3m (Penetration rate in upper 3rd meter)&lt;br&gt;- Pen_4m (Penetration rate in upper 4th meter)&lt;br&gt;- Pen_5m (Penetration rate in upper 5th meter)&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="179"/>
        <source>X_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="180"/>
        <source>Y_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="181"/>
        <source>Z_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="182"/>
        <source>Pen_1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="183"/>
        <source>Pen_2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="184"/>
        <source>Pen_3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="185"/>
        <source>Pen_4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metriclaspointcrown.cpp" line="186"/>
        <source>Pen_5m</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricMinMaxLASFields</name>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="44"/>
        <source>Min et Max pour chaque champ LAS</source>
        <translation>Min and Max for each LAS field</translation>
    </message>
    <message>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- Intensity_Min&lt;br&gt;- Intensity_Max&lt;br&gt;- Return_Number_Min&lt;br&gt;- Return_Number_Max&lt;br&gt;- Number_of_Returns_Min&lt;br&gt;- Number_of_Returns_Max&lt;br&gt;- Classification_Flags_Min&lt;br&gt;- Classification_Flags_Max&lt;br&gt;- Scanner_Channel_Min&lt;br&gt;- Scanner_Channel_Max&lt;br&gt;- Scan_Direction_Flag_Min&lt;br&gt;- Scan_Direction_Flag_Max&lt;br&gt;- Edge_of_Flight_Line_Min&lt;br&gt;- Edge_of_Flight_Line_Max&lt;br&gt;- Classification_Min&lt;br&gt;- Classification_Max&lt;br&gt;- Scan_Angle_Rank_Min&lt;br&gt;- Scan_Angle_Rank_Max&lt;br&gt;- User_Data_Min&lt;br&gt;- User_Data_Max&lt;br&gt;- Point_Source_ID_Min&lt;br&gt;- Point_Source_ID_Max&lt;br&gt;- GPS_Time_Min&lt;br&gt;- GPS_Time_Max&lt;br&gt;- Red_Min&lt;br&gt;- Red_Max&lt;br&gt;- Green_Min&lt;br&gt;- Green_Max&lt;br&gt;- Blue_Min&lt;br&gt;- Blue_Max&lt;br&gt;</source>
        <translation type="vanished">Following values are computed:&lt;br&gt;- Intensity_Min&lt;br&gt;- Intensity_Max&lt;br&gt;- Return_Number_Min&lt;br&gt;- Return_Number_Max&lt;br&gt;- Number_of_Returns_Min&lt;br&gt;- Number_of_Returns_Max&lt;br&gt;- Classification_Flags_Min&lt;br&gt;- Classification_Flags_Max&lt;br&gt;- Scanner_Channel_Min&lt;br&gt;- Scanner_Channel_Max&lt;br&gt;- Scan_Direction_Flag_Min&lt;br&gt;- Scan_Direction_Flag_Max&lt;br&gt;- Edge_of_Flight_Line_Min&lt;br&gt;- Edge_of_Flight_Line_Max&lt;br&gt;- Classification_Min&lt;br&gt;- Classification_Max&lt;br&gt;- Scan_Angle_Rank_Min&lt;br&gt;- Scan_Angle_Rank_Max&lt;br&gt;- User_Data_Min&lt;br&gt;- User_Data_Max&lt;br&gt;- Point_Source_ID_Min&lt;br&gt;- Point_Source_ID_Max&lt;br&gt;- GPS_Time_Min&lt;br&gt;- GPS_Time_Max&lt;br&gt;- Red_Min&lt;br&gt;- Red_Max&lt;br&gt;- Green_Min&lt;br&gt;- Green_Max&lt;br&gt;- Blue_Min&lt;br&gt;- Blue_Max&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="50"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- X_Min&lt;br&gt;- X_Max&lt;br&gt;- Y_Min&lt;br&gt;- Y_Max&lt;br&gt;- Z_Min&lt;br&gt;- Z_Max&lt;br&gt;- Intensity_Min&lt;br&gt;- Intensity_Max&lt;br&gt;- Return_Number_Min&lt;br&gt;- Return_Number_Max&lt;br&gt;- Number_of_Returns_Min&lt;br&gt;- Number_of_Returns_Max&lt;br&gt;- Classification_Flags_Min&lt;br&gt;- Classification_Flags_Max&lt;br&gt;- Scanner_Channel_Min&lt;br&gt;- Scanner_Channel_Max&lt;br&gt;- Scan_Direction_Flag_Min&lt;br&gt;- Scan_Direction_Flag_Max&lt;br&gt;- Edge_of_Flight_Line_Min&lt;br&gt;- Edge_of_Flight_Line_Max&lt;br&gt;- Classification_Min&lt;br&gt;- Classification_Max&lt;br&gt;- Scan_Angle_Rank_Min&lt;br&gt;- Scan_Angle_Rank_Max&lt;br&gt;- User_Data_Min&lt;br&gt;- User_Data_Max&lt;br&gt;- Point_Source_ID_Min&lt;br&gt;- Point_Source_ID_Max&lt;br&gt;- GPS_Time_Min&lt;br&gt;- GPS_Time_Max&lt;br&gt;- Red_Min&lt;br&gt;- Red_Max&lt;br&gt;- Green_Min&lt;br&gt;- Green_Max&lt;br&gt;- Blue_Min&lt;br&gt;- Blue_Max&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="255"/>
        <source>X_Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="256"/>
        <source>X_Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="257"/>
        <source>Y_Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="258"/>
        <source>Y_Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="259"/>
        <source>Z_Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="260"/>
        <source>Z_Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="261"/>
        <source>Intensity_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="262"/>
        <source>Intensity_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="263"/>
        <source>Return_Number_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="264"/>
        <source>Return_Number_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="265"/>
        <source>Number_of_Returns_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="266"/>
        <source>Number_of_Returns_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="267"/>
        <source>Classification_Flags_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="268"/>
        <source>Classification_Flags_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="269"/>
        <source>Scanner_Channel_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="270"/>
        <source>Scanner_Channel_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="271"/>
        <source>Scan_Direction_Flag_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="272"/>
        <source>Scan_Direction_Flag_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="273"/>
        <source>Edge_of_Flight_Line_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="274"/>
        <source>Edge_of_Flight_Line_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="275"/>
        <source>Classification_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="276"/>
        <source>Classification_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="277"/>
        <source>Scan_Angle_Rank_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="278"/>
        <source>Scan_Angle_Rank_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="279"/>
        <source>User_Data_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="280"/>
        <source>User_Data_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="281"/>
        <source>Point_Source_ID_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="282"/>
        <source>Point_Source_ID_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="283"/>
        <source>GPS_Time_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="284"/>
        <source>GPS_Time_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="285"/>
        <source>Red_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="286"/>
        <source>Red_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="287"/>
        <source>Green_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="288"/>
        <source>Green_Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="289"/>
        <source>Blue_Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricminmaxlasfields.cpp" line="290"/>
        <source>Blue_Max</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricNApexMean</name>
    <message>
        <location filename="../metric/onf_metricnapexmean.cpp" line="51"/>
        <source>Moyenne des N plus hauts apex</source>
        <translation>Mean of N highest apex</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnapexmean.cpp" line="58"/>
        <source>Nombre apex</source>
        <translation>Apex number</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnapexmean.cpp" line="59"/>
        <source>%Hmax en dessous duquel les apex ne sont plus pris en compte</source>
        <translation>%Hmax for apex to be considered</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnapexmean.cpp" line="148"/>
        <source>HmApex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnapexmean.cpp" line="149"/>
        <source>nApex</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricNbyLASClass</name>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="44"/>
        <source>Nombre de points par modalité LAS</source>
        <translation>Number of points by LAS modality</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="49"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- N : Nombre total de points&lt;br&gt;- N_first : Nombre de points First&lt;br&gt;- N_last : Nombre de points Last&lt;br&gt;- N_int : Nombre de points Intermediate&lt;br&gt;- N_only : Nombre de points Only&lt;br&gt;- N_error : Nombre de points en Erreur (nombre de retours ou numéro de retour aberrant)&lt;br&gt;- N_Cla00_NeverClassified : Nombre de points classés 0&lt;br&gt;- N_Cla01_Unclassified : Nombre de points classés 1&lt;br&gt;- N_Cla02_Ground : Nombre de points classés 2&lt;br&gt;- N_Cla03_LowVegetation : Nombre de points classés 3&lt;br&gt;- N_Cla04_MediumVegetation : Nombre de points classés 4&lt;br&gt;- N_Cla05_HighVegetation : Nombre de points classés 5&lt;br&gt;- N_Cla06_Building : Nombre de points classés 6&lt;br&gt;- N_Cla07_LowPointNoise : Nombre de points classés 7&lt;br&gt;- N_Cla08_ModelKeyPoint : Nombre de points classés 8&lt;br&gt;- N_Cla09_Water : Nombre de points classés 9&lt;br&gt;- N_Cla10_Rail : Nombre de points classés 10&lt;br&gt;- N_Cla11_RoadSurface : Nombre de points classés 11&lt;br&gt;- N_Cla12_OverlapPoint : Nombre de points classés 12&lt;br&gt;- N_Cla13_WireGuard : Nombre de points classés 13&lt;br&gt;- N_Cla14_WireConductor : Nombre de points classés 14&lt;br&gt;- N_Cla15_TransmissionTower : Nombre de points classés 15&lt;br&gt;- N_Cla16_WireStructureConnector : Nombre de points classés 16&lt;br&gt;- N_Cla17_BridgeDeck : Nombre de points classés 17&lt;br&gt;- N_Cla18_HighNoise : Nombre de points classés 18&lt;br&gt;- N_Cla19_63_Reserved : Nombre de points classés 19 à 63&lt;br&gt;- N_Cla64_255_UserDefinable : Nombre de points classés 63 à 255&lt;br&gt;- ZRange : Zmax - Zmin&lt;br&gt;- NumberOfLines : Nombre de lignes vols couvrant la placette&lt;br&gt;- N_bestLine : Nombre de points de la ligne de vols ayant le plus de points&lt;br&gt;- N_secondLine : Nombre de points de la seconde ligne de vols ayant le plus de points&lt;br&gt;- N_worstLine : Nombre de points de la ligne de vols ayant le moins de points</source>
        <translation>Following values are computed:&lt;br&gt;- N: Total number of points&lt;br&gt;- N_first: Number of First points&lt;br&gt;- N_last: Number of Last points&lt;br&gt;- N_int: Number of Intermediate points&lt;br&gt;- N_only: Number of Only points&lt;br&gt;- N_error: Number of  Error points (number of return or return number not valid)&lt;br&gt;- N_Cla00_NeverClassified: Number of points classified 0&lt;br&gt;- N_Cla01_Unclassified: Number of points classified 1&lt;br&gt;- N_Cla02_Ground: Number of points classified 2&lt;br&gt;- N_Cla03_LowVegetation: Number of points classified 3&lt;br&gt;- N_Cla04_MediumVegetation: Number of points classified 4&lt;br&gt;- N_Cla05_HighVegetation: Number of points classified 5&lt;br&gt;- N_Cla06_Building: Number of points classified 6&lt;br&gt;- N_Cla07_LowPointNoise: Number of points classified 7&lt;br&gt;- N_Cla08_ModelKeyPoint: Number of points classified 8&lt;br&gt;- N_Cla09_Water: Number of points classified 9&lt;br&gt;- N_Cla10_Rail: Number of points classified 10&lt;br&gt;- N_Cla11_RoadSurface: Number of points classified 11&lt;br&gt;- N_Cla12_OverlapPoint: Number of points classified 12&lt;br&gt;- N_Cla13_WireGuard: Number of points classified 13&lt;br&gt;- N_Cla14_WireConductor: Number of points classified 14&lt;br&gt;- N_Cla15_TransmissionTower: Number of points classified 15&lt;br&gt;- N_Cla16_WireStructureConnector: Number of points classified 16&lt;br&gt;- N_Cla17_BridgeDeck: Number of points classified 17&lt;br&gt;- N_Cla18_HighNoise: Number of points classified 18&lt;br&gt;- N_Cla19_63_Reserved: Number of points classified 19 à 63&lt;br&gt;- N_Cla64_255_UserDefinable: Number of points classified 63 à 255&lt;br&gt;- ZRange: Zmax - Zmin&lt;br&gt;- NumberOfLines: Number of lines of flight covering the plot&lt;br&gt;- N_bestLine: Number of points for the line of flight with the most points&lt;br&gt;- N_secondLine: Number of points for the line of flight with the second most points&lt;br&gt;- N_worstLine: Number of points for the line of flight with the less points</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="258"/>
        <source>N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="259"/>
        <source>N_first</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="260"/>
        <source>N_last</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="261"/>
        <source>N_int</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="262"/>
        <source>N_only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="263"/>
        <source>N_error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="265"/>
        <source>N_Cla00_NeverClassified</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="266"/>
        <source>N_Cla01_Unclassified</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="267"/>
        <source>N_Cla02_Ground</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="268"/>
        <source>N_Cla03_LowVegetation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="269"/>
        <source>N_Cla04_MediumVegetation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="270"/>
        <source>N_Cla05_HighVegetation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="271"/>
        <source>N_Cla06_Building</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="272"/>
        <source>N_Cla07_LowPointNoise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="273"/>
        <source>N_Cla08_ModelKeyPoint</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="274"/>
        <source>N_Cla09_Water</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="275"/>
        <source>N_Cla10_Rail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="276"/>
        <source>N_Cla11_RoadSurface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="277"/>
        <source>N_Cla12_OverlapPoint</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="278"/>
        <source>N_Cla13_WireGuard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="279"/>
        <source>N_Cla14_WireConductor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="280"/>
        <source>N_Cla15_TransmissionTower</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="281"/>
        <source>N_Cla16_WireStructureConnector</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="282"/>
        <source>N_Cla17_BridgeDeck</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="283"/>
        <source>N_Cla18_HighNoise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="284"/>
        <source>N_Cla19_63_Reserved</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="285"/>
        <source>N_Cla64_255_UserDefinable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="287"/>
        <source>ZRange</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="289"/>
        <source>NumberOfLines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="290"/>
        <source>N_bestLine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="291"/>
        <source>N_secondLine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricnbylasclass.cpp" line="292"/>
        <source>N_worstLine</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricPointCrownShape</name>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="42"/>
        <source>Métriques de forme de houppier (LAS H ou LAS Z)</source>
        <translation>Crown shape metrics (LAS H or LAS Z)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="47"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- X_Apex&lt;br&gt;- Y_Apex&lt;br&gt;- Z_Apex&lt;br&gt;- NbPts_inf0_5m&lt;br&gt;- NbPts_inf1m&lt;br&gt;- NbPts_inf2m&lt;br&gt;- NbPts_inf3m&lt;br&gt;- NbPts_inf4m&lt;br&gt;- NbPts_inf5m&lt;br&gt;- MeanDistZ_inf0_5m&lt;br&gt;- MeanDistZ_inf1m&lt;br&gt;- MeanDistZ_inf2m&lt;br&gt;- MeanDistZ_inf3m&lt;br&gt;- MeanDistZ_inf4m&lt;br&gt;- MeanDistZ_inf5m&lt;br&gt;- MeanAngle_inf0_5m&lt;br&gt;- MeanAngle_inf1m&lt;br&gt;- MeanAngle_inf2m&lt;br&gt;- MeanAngle_inf3m&lt;br&gt;- MeanAngle_inf4m&lt;br&gt;- MeanAngle_inf5m&lt;br&gt;- MeanDistXY_inf0_5m&lt;br&gt;- MeanDistXY_inf1m&lt;br&gt;- MeanDistXY_inf2m&lt;br&gt;- MeanDistXY_inf3m&lt;br&gt;- MeanDistXY_inf4m&lt;br&gt;- MeanDistXY_inf5m&lt;br&gt;- DistXY_0_5m&lt;br&gt;- DistXY_1m&lt;br&gt;- DistXY_2m&lt;br&gt;- DistXY_3m&lt;br&gt;- DistXY_4m&lt;br&gt;- DistXY_5m&lt;br&gt;- Slope_0_5m&lt;br&gt;- Slope_1m&lt;br&gt;- Slope_2m&lt;br&gt;- Slope_3m&lt;br&gt;- Slope_4m&lt;br&gt;- Slope_5m&lt;br&gt;- Convexity_0_5m&lt;br&gt;- Convexity_1m&lt;br&gt;- Convexity_2m&lt;br&gt;- Convexity_3m&lt;br&gt;- Convexity_4m&lt;br&gt;- _AreaUnderCurve_1m&lt;br&gt;- _AreaUnderCurve_2m&lt;br&gt;- _AreaUnderCurve_3m&lt;br&gt;- _AreaUnderCurve_4m&lt;br&gt;- _AreaUnderCurve_5m&lt;br&gt;</source>
        <translation>Follwing values are computed:&lt;br&gt;- X_Apex&lt;br&gt;- Y_Apex&lt;br&gt;- Z_Apex&lt;br&gt;- NbPts_inf0_5m&lt;br&gt;- NbPts_inf1m&lt;br&gt;- NbPts_inf2m&lt;br&gt;- NbPts_inf3m&lt;br&gt;- NbPts_inf4m&lt;br&gt;- NbPts_inf5m&lt;br&gt;- MeanDistZ_inf0_5m&lt;br&gt;- MeanDistZ_inf1m&lt;br&gt;- MeanDistZ_inf2m&lt;br&gt;- MeanDistZ_inf3m&lt;br&gt;- MeanDistZ_inf4m&lt;br&gt;- MeanDistZ_inf5m&lt;br&gt;- MeanAngle_inf0_5m&lt;br&gt;- MeanAngle_inf1m&lt;br&gt;- MeanAngle_inf2m&lt;br&gt;- MeanAngle_inf3m&lt;br&gt;- MeanAngle_inf4m&lt;br&gt;- MeanAngle_inf5m&lt;br&gt;- MeanDistXY_inf0_5m&lt;br&gt;- MeanDistXY_inf1m&lt;br&gt;- MeanDistXY_inf2m&lt;br&gt;- MeanDistXY_inf3m&lt;br&gt;- MeanDistXY_inf4m&lt;br&gt;- MeanDistXY_inf5m&lt;br&gt;- DistXY_0_5m&lt;br&gt;- DistXY_1m&lt;br&gt;- DistXY_2m&lt;br&gt;- DistXY_3m&lt;br&gt;- DistXY_4m&lt;br&gt;- DistXY_5m&lt;br&gt;- Slope_0_5m&lt;br&gt;- Slope_1m&lt;br&gt;- Slope_2m&lt;br&gt;- Slope_3m&lt;br&gt;- Slope_4m&lt;br&gt;- Slope_5m&lt;br&gt;- Convexity_0_5m&lt;br&gt;- Convexity_1m&lt;br&gt;- Convexity_2m&lt;br&gt;- Convexity_3m&lt;br&gt;- Convexity_4m&lt;br&gt;- _AreaUnderCurve_1m&lt;br&gt;- _AreaUnderCurve_2m&lt;br&gt;- _AreaUnderCurve_3m&lt;br&gt;- _AreaUnderCurve_4m&lt;br&gt;- _AreaUnderCurve_5m&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="544"/>
        <source>X_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="545"/>
        <source>Y_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="546"/>
        <source>Z_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="547"/>
        <source>NbPts_inf0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="548"/>
        <source>NbPts_inf1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="549"/>
        <source>NbPts_inf2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="550"/>
        <source>NbPts_inf3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="551"/>
        <source>NbPts_inf4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="552"/>
        <source>NbPts_inf5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="553"/>
        <source>MeanDistZ_inf0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="554"/>
        <source>MeanDistZ_inf1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="555"/>
        <source>MeanDistZ_inf2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="556"/>
        <source>MeanDistZ_inf3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="557"/>
        <source>MeanDistZ_inf4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="558"/>
        <source>MeanDistZ_inf5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="559"/>
        <source>MeanAngle_inf0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="560"/>
        <source>MeanAngle_inf1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="561"/>
        <source>MeanAngle_inf2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="562"/>
        <source>MeanAngle_inf3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="563"/>
        <source>MeanAngle_inf4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="564"/>
        <source>MeanAngle_inf5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="565"/>
        <source>MeanDistXY_inf0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="566"/>
        <source>MeanDistXY_inf1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="567"/>
        <source>MeanDistXY_inf2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="568"/>
        <source>MeanDistXY_inf3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="569"/>
        <source>MeanDistXY_inf4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="570"/>
        <source>MeanDistXY_inf5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="571"/>
        <source>DistXY_0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="572"/>
        <source>DistXY_1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="573"/>
        <source>DistXY_2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="574"/>
        <source>DistXY_3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="575"/>
        <source>DistXY_4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="576"/>
        <source>DistXY_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="577"/>
        <source>Slope_0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="578"/>
        <source>Slope_1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="579"/>
        <source>Slope_2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="580"/>
        <source>Slope_3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="581"/>
        <source>Slope_4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="582"/>
        <source>Slope_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="583"/>
        <source>Convexity_0_5m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="584"/>
        <source>Convexity_1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="585"/>
        <source>Convexity_2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="586"/>
        <source>Convexity_3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="587"/>
        <source>Convexity_4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="588"/>
        <source>AreaUnderCurve_1m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="589"/>
        <source>AreaUnderCurve_2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="590"/>
        <source>AreaUnderCurve_3m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="591"/>
        <source>AreaUnderCurve_4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricpointcrownshape.cpp" line="592"/>
        <source>AreaUnderCurve_5m</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricQuantiles</name>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="66"/>
        <source>Paramétrage des quantiles à calculer :</source>
        <translation>Parametrization of quantiles to compute:</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="67"/>
        <source>- A partir de</source>
        <translation>- From</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="68"/>
        <source>- Jusqu&apos;à</source>
        <translation>- To</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="69"/>
        <source>- Avec un pas de</source>
        <translation>- By</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="70"/>
        <source>Préfixe à utiliser (ex : P99, Q99, H99, Z99,...)</source>
        <translation>Prefixe to use (ex.: P99, Q99, H99, Z99,...)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="72"/>
        <source>Métriques complémentaires :</source>
        <translation>Additionnal metrics:</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="73"/>
        <source>Minimum (Hmin ~ H00)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="74"/>
        <source>Médiane (Hmed ~ H50)</source>
        <translation>Median (Hmed ~ H50)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="75"/>
        <source>Percentile 99  (H99)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="76"/>
        <source>Maximum (Hmax ~ H100)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="174"/>
        <source>Hmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="175"/>
        <source>Hmed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="176"/>
        <source>H99</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="177"/>
        <source>Hmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricquantiles.cpp" line="59"/>
        <source>Calcul de quantiles sur les coordonnées Z des points</source>
        <translation>Compute quantiles on Z coordinates of points</translation>
    </message>
</context>
<context>
    <name>ONF_MetricRasterCrown</name>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="43"/>
        <source>Rumple, volume et pente (houppier)</source>
        <translation>Rumple, volume and slope (crown)</translation>
    </message>
    <message>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- rumple&lt;br&gt;- volume (m3)&lt;br&gt;- slope_max (degrees)&lt;br&gt;- slope_min (degrees)&lt;br&gt;- slope_moy (degrees)&lt;br&gt;- slope_sd (degrees)&lt;br&gt;- slope_Q25 (degrees)&lt;br&gt;- slope_Q50 - median (degrees)&lt;br&gt;- slope_Q75 (degrees)&lt;br&gt;</source>
        <translation type="vanished">Following values are computed:&lt;br&gt;- rumple&lt;br&gt;- volume (m3)&lt;br&gt;- slope_max (degrees)&lt;br&gt;- slope_min (degrees)&lt;br&gt;- slope_moy (degrees)&lt;br&gt;- slope_sd (degrees)&lt;br&gt;- slope_Q25 (degrees)&lt;br&gt;- slope_Q50 - median (degrees)&lt;br&gt;- slope_Q75 (degrees)&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="48"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- rumple&lt;br&gt;- area3d&lt;br&gt;- area2d&lt;br&gt;- crownThickness&lt;br&gt;- crownThicknessQ25&lt;br&gt;- crownThicknessQ50&lt;br&gt;- crownThicknessQ75&lt;br&gt;- crownThicknessQ95&lt;br&gt;- crownEquivRadius&lt;br&gt;- volume (m3)&lt;br&gt;- volume_Q25 (m3)&lt;br&gt;- volume_Q50 (m3)&lt;br&gt;- volume_Q75 (m3)&lt;br&gt;- slope_max (degrees)&lt;br&gt;- slope_min (degrees)&lt;br&gt;- slope_moy (degrees)&lt;br&gt;- slope_sd (degrees)&lt;br&gt;- slope_Q25 (degrees)&lt;br&gt;- slope_Q50 - median (degrees)&lt;br&gt;- slope_Q75 (degrees)&lt;br&gt;</source>
        <translation>The following values ​​are computed:&lt;br&gt;- rumple&lt;br&gt;- area3d&lt;br&gt;- area2d&lt;br&gt;- crownThickness&lt;br&gt;- crownThicknessQ25&lt;br&gt;- crownThicknessQ50&lt;br&gt;- crownThicknessQ75&lt;br&gt;- crownThicknessQ95&lt;br&gt;- crownEquivRadius&lt;br&gt;- volume (m3)&lt;br&gt;- volume_Q25 (m3)&lt;br&gt;- volume_Q50 (m3)&lt;br&gt;- volume_Q75 (m3)&lt;br&gt;- slope_max (degrees)&lt;br&gt;- slope_min (degrees)&lt;br&gt;- slope_moy (degrees)&lt;br&gt;- slope_sd (degrees)&lt;br&gt;- slope_Q25 (degrees)&lt;br&gt;- slope_Q50 - median (degrees)&lt;br&gt;- slope_Q75 (degrees)&lt;br</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="262"/>
        <source>rumple</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="263"/>
        <source>area3d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="264"/>
        <source>area2d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="265"/>
        <source>crownThickness</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="266"/>
        <source>crownThicknessQ25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="267"/>
        <source>crownThicknessQ50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="268"/>
        <source>crownThicknessQ75</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="269"/>
        <source>crownThicknessQ95</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="270"/>
        <source>crownEquivRadius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="271"/>
        <source>volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="272"/>
        <source>volume_topQ25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="273"/>
        <source>volume_topQ50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="274"/>
        <source>volume_topQ75</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="275"/>
        <source>slope_max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="276"/>
        <source>slope_min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="277"/>
        <source>slope_moy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="278"/>
        <source>slope_sd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="279"/>
        <source>slope_Q25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="280"/>
        <source>slope_Q50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown.cpp" line="281"/>
        <source>slope_Q75</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_MetricRasterCrown2</name>
    <message>
        <source>Métriques houppiers (test)</source>
        <translation type="vanished">Crown Metrics (test)</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="48"/>
        <source>Métriques houppiers (test, H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="215"/>
        <source>convexArea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="216"/>
        <source>maxDistApex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="217"/>
        <source>maxDist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="218"/>
        <source>minDistApex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="219"/>
        <source>minDist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="220"/>
        <source>maxExtendHeight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="221"/>
        <source>hMoyAboveMaxExtendHeight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown2.cpp" line="222"/>
        <source>volumeAboveMaxExtendHeight</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_MetricRasterCrown3</name>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="48"/>
        <source>Métriques houppiers (test, H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="445"/>
        <source>3convexArea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="446"/>
        <source>3maxDistApex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="447"/>
        <source>3maxDist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="448"/>
        <source>3minDistApex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="449"/>
        <source>3minDist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="451"/>
        <source>maxExtH_50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="452"/>
        <source>hMoyMaxExtH_50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="453"/>
        <source>volAboveMaxExtH_50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="455"/>
        <source>maxExtH_75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="456"/>
        <source>hMoyMaxExtH_75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="457"/>
        <source>volAboveMaxExtH_75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="459"/>
        <source>maxExtH_90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="460"/>
        <source>hMoyMaxExtH_90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="461"/>
        <source>volAboveMaxExtH_90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="463"/>
        <source>convexArea_50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="464"/>
        <source>maxDist_50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="466"/>
        <source>convexArea_75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="467"/>
        <source>maxDist_75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="469"/>
        <source>convexArea_90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown3.cpp" line="470"/>
        <source>maxDist_90</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_MetricRasterCrown4</name>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="48"/>
        <source>Métriques houppiers (test, H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="277"/>
        <source>convexArea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="278"/>
        <source>maxDistApex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="279"/>
        <source>maxDist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="280"/>
        <source>maxExtH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="281"/>
        <source>hMoyAboveMaxExtH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="282"/>
        <source>volAboveMaxExtH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrastercrown4.cpp" line="283"/>
        <source>area3dAboveMaxExtH</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_MetricRasterExtend</name>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="42"/>
        <location filename="../metric/onf_metricrasterextend.cpp" line="47"/>
        <source>Extention du raster</source>
        <translation>Raster extend</translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="90"/>
        <source>xmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="91"/>
        <source>xmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="92"/>
        <source>xsize</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="93"/>
        <source>ymin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="94"/>
        <source>ymax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="95"/>
        <source>ysize</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="96"/>
        <source>min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="97"/>
        <source>max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/onf_metricrasterextend.cpp" line="98"/>
        <source>na</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepAddAffiliationID</name>
    <message>
        <source>Ajouter un ID d&apos;affiliation par groupe</source>
        <translation type="vanished">Add an affiliation ID in each group</translation>
    </message>
    <message>
        <source>Résultat à affilier</source>
        <translation type="vanished">Result to affiliate</translation>
    </message>
    <message>
        <source>Veuillez choisir le résultat auquel vous voulez ajouter un ID</source>
        <translation type="vanished">Please choose the result for which you want to add an affiliation ID</translation>
    </message>
    <message>
        <source>Groupe à affilier</source>
        <translation type="vanished">Group to affiliate</translation>
    </message>
    <message>
        <source>Item de référence</source>
        <translation type="vanished">Reference Item</translation>
    </message>
    <message>
        <source>Si cet item est absent, aucun ID ne sera créé</source>
        <translation type="vanished">If this item is absent, no ID will be created</translation>
    </message>
</context>
<context>
    <name>ONF_StepAddAttributeValue</name>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="51"/>
        <source>Ajouter un attribut aux items</source>
        <translation>Add an attribute to all items</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="57"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="78"/>
        <source>Profile</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="80"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="81"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="99"/>
        <source>Nom de l&apos;attribut</source>
        <translation>Attribute name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddattributevalue.cpp" line="100"/>
        <source>Valeur de l&apos;attribut</source>
        <translation>Attribute value</translation>
    </message>
</context>
<context>
    <name>ONF_StepAddFakeCounter</name>
    <message>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="53"/>
        <source>Ajoute un compteur unitaire</source>
        <translation>Add an unit counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="80"/>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="84"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="82"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="83"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddfakecounter.cpp" line="93"/>
        <source>Counter</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepAddLASDataToPlots</name>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="60"/>
        <source>4- Ajoute les données LAS aux placettes</source>
        <translation>4- Add LAS data to plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="66"/>
        <source>Cette étape peut être ajoutée après une étape générant des placettes à partir d&apos;une scène.&lt;br&gt;Elle permet de récupérer pour chaque placette les données LAS adéquates, à partir des données LAS de la scène mère.&lt;br&gt;Ces données sont passée sous forme de référence, la mémoire occupée n&apos;augmente donc pas.</source>
        <translation>This step can be added after a step generating plot from a whole scene/tile.&lt;br&gt;It allows to retreive LAS data for each plot, from LAS data of the whole scene.&lt;br&gt;These data are passed as reference and not deeply copied in memory. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="89"/>
        <source>Placettes</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="91"/>
        <source>Groupe Scene complète</source>
        <translation>Complete scene group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="95"/>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="105"/>
        <source>Données LAS complètes</source>
        <translation>Complete LAS data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="98"/>
        <source>Groupe Placette</source>
        <translation>Plot group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="99"/>
        <source>Points de la placette</source>
        <translation>Plot points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="103"/>
        <source>Données LAS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="116"/>
        <source>Données LAS placette</source>
        <translation>Plot LAS data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddlasdatatoplots.cpp" line="125"/>
        <source>Données LAS dans un résultat séparé</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepAddTileXYAreas</name>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="58"/>
        <source>Ajout des emprises de dalles</source>
        <translation>Add footprints to tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="63"/>
        <source>Pour chaque fichier d&apos;entrée, ajoute l&apos;emprise de la dalle</source>
        <translation>For each input file, add the footprint  of the tile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="76"/>
        <source>Dalles</source>
        <translation>Tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="78"/>
        <source>Grp</source>
        <translation></translation>
    </message>
    <message>
        <source>Entête de fichier</source>
        <translation type="vanished">File header</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="79"/>
        <source>Item d&apos;emprise</source>
        <translation>Footprint item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="80"/>
        <source>FileName</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="87"/>
        <source>Coordonnée X de référence</source>
        <translation>Reference X coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="88"/>
        <source>Coordonnée Y de référence</source>
        <translation>Y reference coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="89"/>
        <source>Taille de la dalle unitaire</source>
        <translation>Size of tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="90"/>
        <source>Taille de la zone tampon</source>
        <translation>Size of buffer</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="91"/>
        <source>Les fichiers d&apos;entrée contiennent les buffers</source>
        <translation>Do input files include buffers</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="99"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="100"/>
        <source>Emprise (Buffer)</source>
        <translation>Footprint (Buffer)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="207"/>
        <source>Header %1 non géographique (impossible de déterminer l&apos;emprise)</source>
        <translation>Header %1 is not geographic (impossible to compute footprint)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="212"/>
        <source>Taille choisie pour les dalles :%1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="213"/>
        <source>Taille constatée des dalles (%1 dalles analysées) :</source>
        <translation>Observed tile size (%1 tiles analyzed):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="214"/>
        <source> - Taille minimale selon X :%1 m</source>
        <translation>- Minimum size along X: %1 m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="215"/>
        <source> - Taille minimale selon Y :%1 m</source>
        <translation>- Minimum size along Y: %1 m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="216"/>
        <source> - Taille maximale selon X :%1 m</source>
        <translation>- Maximum size along X: %1 m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="217"/>
        <source> - Taille maximale selon Y :%1 m</source>
        <translation>- Maximum size along Y: %1 m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepaddtilexyareas.cpp" line="221"/>
        <source>Attention : écart supérieur à 10 % de la taille choisie</source>
        <translation>Warning: Difference greater than 10% of the chosen size</translation>
    </message>
</context>
<context>
    <name>ONF_StepAdjustPlotPosition</name>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="91"/>
        <source>Recaler une placette terrain</source>
        <translation>Adjust field plot position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="97"/>
        <source>To Do</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="111"/>
        <source>Placette</source>
        <translation>Plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="113"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="124"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="114"/>
        <source>Arbre</source>
        <translation>Tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="115"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="148"/>
        <source>DBH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="116"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="149"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="117"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="151"/>
        <source>IDtree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="118"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="150"/>
        <source>IDplot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="119"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="152"/>
        <source>Species</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="122"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="125"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="126"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="127"/>
        <source>Attribut Hauteur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="131"/>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="134"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="133"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="147"/>
        <source>Arbre déplacé</source>
        <translation>Moved tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="153"/>
        <source>TransX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="154"/>
        <source>TransY</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="155"/>
        <source>Moved</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition.cpp" line="156"/>
        <source>zPointClicked</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepAdjustPlotPosition02</name>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="99"/>
        <source>Recaler une placette terrain (v02)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="105"/>
        <source>To Do</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="119"/>
        <source>Placette</source>
        <translation type="unfinished">Plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="121"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="133"/>
        <source>Groupe</source>
        <translation type="unfinished">Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="122"/>
        <source>Arbre</source>
        <translation type="unfinished">Tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="123"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="160"/>
        <source>DBH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="124"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="161"/>
        <source>Height</source>
        <translation type="unfinished">Hauteur</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="125"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="163"/>
        <source>IDtree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="126"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="162"/>
        <source>IDplot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="127"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="164"/>
        <source>Species</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="128"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="131"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="134"/>
        <source>Scène</source>
        <translation type="unfinished">Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="135"/>
        <source>Attributs LAS</source>
        <translation type="unfinished">LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="136"/>
        <source>Maxima</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="137"/>
        <source>MNS</source>
        <translation type="unfinished">DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="138"/>
        <source>Emprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="139"/>
        <source>Attribut Hauteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="143"/>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="146"/>
        <source>MNT</source>
        <translation type="unfinished">DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="145"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="159"/>
        <source>Arbre déplacé</source>
        <translation type="unfinished">Moved tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="165"/>
        <source>TransX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="166"/>
        <source>TransY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="167"/>
        <source>Moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="168"/>
        <source>zPointClicked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="177"/>
        <source>Fichier d&apos;export des placettes recalées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="177"/>
        <source>Fichier Texte (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepadjustplotposition02.cpp" line="310"/>
        <source>Time	Plot	Xbefore	Ybefore	TransX	TransY	Xafter	Yafter	Quality	Comment
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_StepChangeClusterThickness02</name>
    <message>
        <source>Création de clusters horizontaux / billon</source>
        <translation type="vanished">Creation of horizontal clusters by log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="69"/>
        <source>Modifier épaisseur de clusters horizontaux</source>
        <translation>Modify thickness of horizontal clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="74"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="87"/>
        <source>Billons / Clusters</source>
        <translation>Logs / Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="89"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="90"/>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="107"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="91"/>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="108"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepchangeclusterthickness02.cpp" line="98"/>
        <source>Epaisseur en Z  :</source>
        <translation>Z-thickness:</translation>
    </message>
</context>
<context>
    <name>ONF_StepClassifyGround</name>
    <message>
        <source>Classifier les points sol</source>
        <translation type="vanished">Classify ground points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="83"/>
        <source>Cette étape permet de séparer les points Sol et Végétation&lt;ul&gt;&lt;li&gt;Une grille Zmin est créée à la &lt;b&gt;résolution&lt;/b&gt; spécifiée&lt;/li&gt;&lt;li&gt;La densité de points situés entre Zmin et (Zmin + &lt;b&gt;épaisseur du sol&lt;/b&gt;) est calculée pour chaque case&lt;/li&gt;&lt;li&gt;La valeur NA est affectée à toute case dont la densité est inférieure à la &lt;b&gt;densité minimum&lt;/b&gt;&lt;/li&gt;&lt;li&gt;Un test de cohérence des Zmin restants est réalisé pour chaque case sur le &lt;b&gt;voisinage&lt;/b&gt; spécifié (nombre de cases). La valeur NA est affectée aux cases incohérentes&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>This step classify ground and vegetation points&lt;ul&gt;&lt;li&gt;A Zmin grid is created at specified &lt;b&gt;resolution&lt;/b&gt; &lt;/li&gt;&lt;li&gt;The point density between Zmin and (Zmin + &lt;b&gt;terrain thickness&lt;/b&gt;) is computed for each cell&lt;/li&gt;&lt;li&gt;NA value is affected to each cell with a density inferior to &lt;b&gt;minimum density&lt;/b&gt;&lt;/li&gt;&lt;li&gt;A consistency check is done on Zmin values for each cell with the specified &lt;b&gt;neighbourhood&lt;/b&gt; (number of cells). Each cell with an unconsistent value is set to NA. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="102"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="106"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="113"/>
        <source>Résolution de la grille :</source>
        <translation>Grid resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="114"/>
        <source>Epaisseur du sol :</source>
        <translation>Ground thickness:</translation>
    </message>
    <message>
        <source>Filtrage selon la densité et le voisinnage</source>
        <translation type="vanished">Filtering on density and neighbourhood consistency</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="116"/>
        <source>Densité minimum :</source>
        <translation>Minimum density:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="118"/>
        <source>Voisinage (points isolés) :</source>
        <translation>Neighbourhood (isolated points):</translation>
    </message>
    <message>
        <source>Points classifiés</source>
        <translation type="vanished">Classified points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="78"/>
        <source>Classifier les points sol (TLS)</source>
        <translation>Classify ground points (TLS)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="115"/>
        <source>Filtrage selon la densité</source>
        <translation>Filter by density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="117"/>
        <source>Filtrage selon le voisinnage</source>
        <translation>Filter by neighbourhood consistency</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="127"/>
        <source>Points végétation</source>
        <translation>Vegetation points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="128"/>
        <source>Points sol</source>
        <translation>Ground points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="130"/>
        <source>Rasters de classification</source>
        <translation>Classification raster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="132"/>
        <source>MNT (Zmin)</source>
        <translation>DTM (Zmin)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="133"/>
        <source>Densité pts sol</source>
        <translation>Ground points density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="176"/>
        <source>La scène d&apos;entrée %2 comporte %1 points.</source>
        <translation>The %2 input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="184"/>
        <source>Grille MNT à créer : %1 lignes sur %2 colonnes</source>
        <translation>DTM grid to create: %1 rows and %2 columns</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="207"/>
        <source>Grille Zmin créée</source>
        <translation>Zmin grid created</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="231"/>
        <source>Filtrage sur la densité terminé</source>
        <translation>Filtering on density achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="279"/>
        <source>Test de cohérence de voisinnage terminé</source>
        <translation>Neighbourhood consistency test achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepclassifyground.cpp" line="357"/>
        <source>Scène %3 : Création des scènes sol (%1 points) et végétation (%2 points) terminée</source>
        <translation>Scene %3: soil (%1 points) and vegetation (%2 points) scenes creation achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepCompare3DGridsContents</name>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="57"/>
        <source>Comparer deux grilles 3D</source>
        <translation>Compare two 3D grids</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="63"/>
        <source>Il est préférable que les grilles aient la même résolution et le même calage spatial.&lt;br&gt;Considérant A = Grille initiale (avant), B = Grille finale (après)En sortie l&apos;étape renvoie une grille contenant :&lt;br&gt;* 00 : A =  NA,    B =  NA&lt;br&gt;* 01 : A =  NA,    B &lt;  Seuil&lt;br&gt;* 02 : A =  NA,    B &gt;= Seuil&lt;br&gt;* 10 : A &lt;  Seuil, B =  NA&lt;br&gt;* 11 : A &lt;  Seuil, B &lt;  Seuil&lt;br&gt;* 12 : A &lt;  Seuil, B &gt;= Seuil&lt;br&gt;* 20 : A &gt;= Seuil, B =  NA&lt;br&gt;* 21 : A &gt;= Seuil, B &lt;  Seuil&lt;br&gt;* 22 : A &gt;= Seuil, B &gt;= Seuil&lt;br&gt;</source>
        <translation>It is better if the grid have same resolution and same spatial corner coordinate.&lt;br&gt;Lets define: A = initial Grid (before), B = final grid (after). As output the step give a grid with following values:&lt;br&gt;* 00 : A =  NA,    B =  NA&lt;br&gt;* 01 : A =  NA,    B &lt;  threshold&lt;br&gt;* 02 : A =  NA,    B &gt;= threshold&lt;br&gt;* 10 : A &lt;  threshold, B =  NA&lt;br&gt;* 11 : A &lt;  threshold, B &lt;  threshold&lt;br&gt;* 12 : A &lt;  threshold, B &gt;= threshold&lt;br&gt;* 20 : A &gt;= threshold, B =  NA&lt;br&gt;* 21 : A &gt;= threshold, B &lt;  threshold&lt;br&gt;* 22 : A &gt;= threshold, B &gt;= threshold&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="95"/>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="97"/>
        <source>Grille A (avant)</source>
        <translation>Grid A (before)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="96"/>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="100"/>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="109"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="99"/>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="101"/>
        <source>Grille B (après)</source>
        <translation>Grid B (after)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="108"/>
        <location filename="../step/onf_stepcompare3dgridscontents.cpp" line="110"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeAttributeMapFromClusters</name>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="57"/>
        <source>Mapper attribut par clusters (raster)</source>
        <translation>Map one attribute by cluster (raster)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="63"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="84"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="86"/>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="90"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="87"/>
        <source>Image (Clusters)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="88"/>
        <source>Image (attribut)</source>
        <translation>Image (attribute)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="91"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="92"/>
        <source>IDCluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="93"/>
        <source>Attribut</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="102"/>
        <source>Carte d&apos;attribut</source>
        <translation>Attribute map</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="110"/>
        <source>Valeur manquante dans le raster de sortie</source>
        <translation>Missing Value in output raster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeattributemapfromclusters.cpp" line="111"/>
        <source>Valeur par défaut dans le raster de sortie</source>
        <translation>Default value in output raster</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeBoundary</name>
    <message>
        <source>Calculer enveloppe concave</source>
        <translation type="vanished">Compute concave hull</translation>
    </message>
    <message>
        <source>No detailled description for this step</source>
        <translation type="vanished">Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <source>Scène(s)</source>
        <translation type="vanished">Scene(s)</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Scène</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <source>Résultat compteur</source>
        <translation type="vanished">Counter result</translation>
    </message>
    <message>
        <source>Compteur</source>
        <translation type="vanished">Counter</translation>
    </message>
    <message>
        <source>Dilaté Hull</source>
        <translation type="vanished">Dilated hull</translation>
    </message>
    <message>
        <source>Résolution</source>
        <translation type="vanished">Resolution</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeBoundaryV2</name>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="81"/>
        <source>Calcul d&apos;un raster d&apos;emprise</source>
        <translation>Compute footprint raster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="87"/>
        <source>Calcul d&apos;un raster logique, avec une valeur 1 pour toute cellule contenant au moins un point</source>
        <translation>Calculation of a logical raster with a value of 1 for any cell containing at least one point</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="108"/>
        <source>Emprise totale</source>
        <translation>Total footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="110"/>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="115"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="111"/>
        <source>Emprise</source>
        <translation>XY area</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="113"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="116"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="118"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="120"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="129"/>
        <source>Footprint</source>
        <translation>Emprise</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="131"/>
        <source>Footprint Raster</source>
        <translation>Raster footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeboundaryv2.cpp" line="138"/>
        <source>Résolution</source>
        <translation>Resolution</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeCHM</name>
    <message>
        <location filename="../step/onf_stepcomputechm.cpp" line="70"/>
        <source>Créer MNH</source>
        <translation>Create DHM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputechm.cpp" line="75"/>
        <source>Cette étape permet de générer un MNH à partir d&apos;un MNS et d&apos;un MNT. L&apos;emprise et la résolution du CHM seront calés sur le MNS. </source>
        <translation>This step generates an DHM from an DSM and a DEM. Thefootprint and resolution of the DHM will be calibrated from the DSM.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputechm.cpp" line="88"/>
        <location filename="../step/onf_stepcomputechm.cpp" line="92"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputechm.cpp" line="95"/>
        <location filename="../step/onf_stepcomputechm.cpp" line="99"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputechm.cpp" line="111"/>
        <source>MNH</source>
        <translation>DHM</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeCrownProjection</name>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="63"/>
        <source>Projections de houppier</source>
        <translation>Horizontal projection of crowns</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="69"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="90"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="92"/>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="105"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="93"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="103"/>
        <source>Enveloppe Convexe au sol</source>
        <translation>Convex hull (on ground)</translation>
    </message>
    <message>
        <source>Enveloppe Directionnelle au sol</source>
        <translation type="vanished">Directionnal hull (on ground)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="107"/>
        <source>Enveloppe Convexe d&apos;une tranche</source>
        <translation>Convex hull of slices</translation>
    </message>
    <message>
        <source>Enveloppe Directionnelle d&apos;une tranche</source>
        <translation type="vanished">Directionnal hull of slices</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="117"/>
        <source>Calculer les enveloppes convexes par tranches</source>
        <translation>Compute convex hulls for slices</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="118"/>
        <source>Espacement des tranches</source>
        <translation>Spacing between slices</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecrownprojection.cpp" line="119"/>
        <source>Epaisseur des tranches</source>
        <translation>Thickness of slices</translation>
    </message>
    <message>
        <source>Calculer les enveloppes directionnelles par tranches</source>
        <translation type="vanished">Compute directionnal hulls for slices</translation>
    </message>
    <message>
        <source>Nombre de directions</source>
        <translation type="vanished">Number of directions</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeCumulativeConvexHull</name>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="61"/>
        <source>Calculer enveloppe convexe cumulée</source>
        <translation>Compute cumulative convex hull</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="67"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="88"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="90"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="93"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="95"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="102"/>
        <source>Convex Hull (cumulative)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativeconvexhull.cpp" line="104"/>
        <source>Convex Hull</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeCumulativeNRTable</name>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="73"/>
        <source>Export N-R Table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="79"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="100"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="102"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="103"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="104"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="106"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="108"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="115"/>
        <source>N-R Table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativenrtable.cpp" line="124"/>
        <source>Choose Export file</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeCumulativeSummary</name>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="62"/>
        <source>Export summary of metrics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="68"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="89"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="91"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="92"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="93"/>
        <source>Attribut</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="95"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="97"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="104"/>
        <source>Summary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="113"/>
        <source>Choose Export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputecumulativesummary.cpp" line="243"/>
        <source>Item	Champ	Moyenne	Min	Max	Somme	nb_Val	nb_NA
</source>
        <translation>Item	Field	Mean	Min	Max	Sum	nb_Val	nb_NA
</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeDBHFromHeightAllometry</name>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="54"/>
        <source>Calculer un DBH par allométrie sur la hauteur</source>
        <translation>Compute one DBH par height allometry</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="59"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="72"/>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="75"/>
        <source>Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="74"/>
        <source>Apex (Grp)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="82"/>
        <source>Relation allometrique (H - Hmax)*(D - a*(H - 1.3)) = m :</source>
        <translation>Allometric relation (H - Hmax)*(D - a*(H - 1.3)) = m :</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="83"/>
        <source>Paramètre Hmax</source>
        <translation>Parameter Hmax</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="84"/>
        <source>Paramètre a</source>
        <translation>Parameter a</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="85"/>
        <source>Paramètre m</source>
        <translation>Parameter m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeedbhfromheightallometry.cpp" line="94"/>
        <source>DBH</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeDSM</name>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="71"/>
        <source>Créer MNS et MNH</source>
        <translation>Create DSM and DHM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="76"/>
        <source>Cette étape permet de générer un MNS et un MNH à partir d&apos;un nuage de points et d&apos;un MNT</source>
        <translation>This step generate a DSM and a DHM from a ground point cloud and a DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="89"/>
        <source>Points sol</source>
        <translation>Ground points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="93"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="94"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="101"/>
        <source>Interpolation</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="102"/>
        <source>Taille de la fenêtre d&apos;interpolation</source>
        <translation>Size of the interpolation window</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="102"/>
        <source>Cases</source>
        <translation>Cells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="103"/>
        <source>Convertir valeurs NA en min(MNS) ?</source>
        <translation>Convert NA values to min(DSM)?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="111"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="112"/>
        <source>MNH</source>
        <translation>DHM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsm.cpp" line="185"/>
        <source>Interpolation du MNS terminée</source>
        <translation>Interpolation of the DSM achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeDSMOnly</name>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="83"/>
        <source>Créer DSM (Zmax)</source>
        <translation>Create DSM (Zmax)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="88"/>
        <source>Cette étape permet de générer un Modèle Numérique de Surface (MNS).&lt;br&gt;Le MNS est calculé comme une grille Zmax à la &lt;b&gt;résolution&lt;/b&gt; spécifiée. &lt;br&gt;Ce MNS peut être optionellement interpolé et/ou lissée selon les options cochées.</source>
        <translation>This step creates de Digital Surface Model (DSM).&lt;br&gt;The DSM is computed as a Zmax grid at specified &lt;b&gt;resolution&lt;/b&gt;.&lt;br&gt;This DSM can be optionnally be interpolated and / or smoothed. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="103"/>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="107"/>
        <source>Points végétation</source>
        <translation>Vegetation points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="109"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="116"/>
        <source>Résolution de la grille :</source>
        <translation>Grid resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="117"/>
        <source>Interpolation</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="118"/>
        <source>Taille de la fenêtre d&apos;interpolation</source>
        <translation>Size of the interpolation window</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="118"/>
        <source>Cases</source>
        <translation>Cells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="119"/>
        <source>Lissage (filtre moyen)</source>
        <translation>Smoothing (mean filter)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="120"/>
        <source>Voisinage de lissage :</source>
        <translation>Smoothing neighbourhood:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="123"/>
        <source>Remplacer les valeurs NA par :</source>
        <translation>Replace the NA values ​​with:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="125"/>
        <source>Min(MNS)</source>
        <translation>Min(DSM)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="126"/>
        <source>La valeur ci-dessous</source>
        <translation>The value below</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="127"/>
        <source>Valeur de remplacement des NA :</source>
        <translation>NA replacement value:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="130"/>
        <source>Callage du coin (minX, minY) :</source>
        <translation>Corner position (minX, minY):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="132"/>
        <source>Sur la boite englobante de la scène</source>
        <translation>Using bounding box of the scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="133"/>
        <source>Par rapport aux coordonnées suivantes :</source>
        <translation>Multiples of the following  coordinates:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="134"/>
        <source>Coordonnée X :</source>
        <translation>X Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="135"/>
        <source>Coordonnée Y :</source>
        <translation>Y Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="136"/>
        <source>N.B. : Si une emprise a été fournie, c&apos;est elle qui sera utilisée dans tous les cas.</source>
        <translation>N.B .: If a footprint has been provided, it will be used in all cases.</translation>
    </message>
    <message>
        <source>Convertir valeurs NA en min(MNS) ?</source>
        <translation type="vanished">Convert NA values to min(DSM)?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="146"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="252"/>
        <source>Interpolation du MNS terminée</source>
        <translation>Interpolation of the DSM achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedsmonly.cpp" line="287"/>
        <source>Lissage du MNS terminé</source>
        <translation>Smoothing of the DSM completed</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeDTM02</name>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="73"/>
        <source>Créer MNT</source>
        <translation>Create DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="78"/>
        <source>Cette étape permet de séparer les points Sol et Végétation, et de générer :un Modèle Numérique de Terrain (MNT).&lt;br&gt;Le MNT est calculé comme une grille Zmin à la &lt;b&gt;résolution&lt;/b&gt; spécifiée. &lt;br&gt;Ce MNT peut être optionellement interpolé et/ou lissée selon les options cochées.</source>
        <translation> This step separate ground and vegetation points, and creates a Digital Terrain Model (DTM).&lt;br&gt; The DTM is computed as a Zmin grid with specified &lt;b&gt;resolution&lt;/b&gt;.&lt;br&gt; This DTM can be optionnally interpolated and / or smoothed. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="93"/>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="97"/>
        <source>Points sol</source>
        <translation>Ground points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="99"/>
        <source>Emprise</source>
        <translation>XY area shape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="106"/>
        <source>Résolution de la grille :</source>
        <translation>Grid resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="107"/>
        <source>Interpolation</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="108"/>
        <source>Taille de la fenêtre d&apos;interpolation</source>
        <translation>Size of interpolation window</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="108"/>
        <source>Cases</source>
        <translation>Cells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="109"/>
        <source>Lissage (filtre moyen)</source>
        <translation>Smoothing (mean filter)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="110"/>
        <source>Voisinage de lissage :</source>
        <translation>Smoothing neighbourhood:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="111"/>
        <source>Convertir valeurs NA en min(MNT) ?</source>
        <translation>Convert NA values to min(DTM)?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="119"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="215"/>
        <source>Interpolation du MNT terminée</source>
        <translation>DTM interpolation achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedtm02.cpp" line="250"/>
        <source>Lissage du MNT terminé</source>
        <translation>DTM smoothing achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeDominanceIndicators</name>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="42"/>
        <source>Calcul de l&apos;indice de compétition de Schutz 1989</source>
        <translation>Compute Schutz competition index (1989)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="48"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="69"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="72"/>
        <source>Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="71"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="73"/>
        <source>X_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="74"/>
        <source>Y_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="75"/>
        <source>Z_Apex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="76"/>
        <source>Diametre_Houppier</source>
        <translation>Crown_diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="80"/>
        <source>Item de sortie</source>
        <translation>Output item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="83"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="86"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="85"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="96"/>
        <source>indiceSchutz</source>
        <translation>SchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="97"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="108"/>
        <source>indiceSchutzTotal</source>
        <translation>TotalSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="98"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="109"/>
        <source>indiceSchutzHorTotal</source>
        <translation>TotalHorSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="99"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="110"/>
        <source>indiceSchutzVerTotal</source>
        <translation>TotalVerSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="100"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="112"/>
        <source>indiceSchutzMax</source>
        <translation>MaxSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="101"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="113"/>
        <source>indiceSchutzHorMax</source>
        <translation>MaxHorSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="102"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="114"/>
        <source>indiceSchutzVerMax</source>
        <translation>MaxVerSchutzIndex</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="103"/>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="115"/>
        <source>angleNeighbMax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="107"/>
        <source>Scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="111"/>
        <source>indiceSchutzVerTotalINT</source>
        <translation>TotalVerSchutzIndexINT</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputedominanceindicators.cpp" line="124"/>
        <source>Ajouter les attributs à un autre item en copie</source>
        <translation>Add attribute to another copied item</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeEmptinessGrid</name>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="71"/>
        <source>Compute emptiness voxel grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="89"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="93"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="94"/>
        <source>Normals</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="96"/>
        <source>Zone de calcul (optionnel)</source>
        <translation>Calculation area (optional)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="100"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="113"/>
        <source>Emptiness grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeemptinessgrid.cpp" line="121"/>
        <source>Resolution :</source>
        <translation>Resolution:</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeGapMask</name>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="68"/>
        <source>Créer un masque des trouées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="73"/>
        <source>Cette étape créée un raster entier. Toute valeur &gt;= 0 est dans les trouées au seuil fixée. Le nombre indique à combien d&apos;erosions le pixel trouée persiste.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="86"/>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="90"/>
        <source>MNH</source>
        <translation type="unfinished">DHM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="97"/>
        <source>Seuil de hauteur de trouée :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="98"/>
        <source>Nombre d&apos;érosions (-1 si toutes) :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputegapmask.cpp" line="107"/>
        <source>Gap Mask</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeHfromZandTIN</name>
    <message>
        <source>Calculer les hauteurs à l&apos;aide d&apos;un TIN</source>
        <translation type="vanished">Compute heights using a TIN</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="66"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="85"/>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="88"/>
        <source>TIN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="83"/>
        <source>Scène en Z</source>
        <translation>Scene in Z</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="61"/>
        <source>Calculer la hauteur des points à l&apos;aide d&apos;un TIN</source>
        <translation>Compute point heights using a TIN</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="79"/>
        <source>Z Scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="87"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="102"/>
        <source>Scene en H</source>
        <translation>Scene in H</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="141"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehfromzandtin.cpp" line="172"/>
        <source>Convertion terminée</source>
        <translation>Conversion achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeHillShadeRaster</name>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="30"/>
        <source>Compute hillShade raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="57"/>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="60"/>
        <source>DEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="59"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="70"/>
        <source>HillShade</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="79"/>
        <source>Azimut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehillshaderaster.cpp" line="80"/>
        <source>Altitude</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeHitGrid</name>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="69"/>
        <source>Créer grille 3D de densité de points</source>
        <translation>Create 3D points density grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="75"/>
        <source>Cette étape génère une grille 3D à la &lt;b&gt;résolution&lt;/b&gt; spécifiée.&lt;br&gt;Chaque case reçoit le nombre de points de la scène d&apos;entrée qu&apos;elle contient.</source>
        <translation>This step creates a 3D grid at the specified &lt;b&gt;resolution&lt;/b&gt;.&lt;br&gt;Each cell contains the number of points in it from the input scene. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="87"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="99"/>
        <source>Hits</source>
        <translation>Nombre de points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="106"/>
        <source>Résolution de la grille</source>
        <translation>Grid resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="106"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="108"/>
        <source>Callage du coin (minX, minY, minZ) :</source>
        <translation>How to compute coordinates of the grid corner (minX, minY, minZ):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="111"/>
        <source>Sur la boite englobante de la scène</source>
        <translation>Using bounding box of the scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="112"/>
        <source>Par rapport aux coordonnées suivantes :</source>
        <translation>Multiples of the following  coordinates:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="114"/>
        <source>Coordonnée X :</source>
        <translation>X Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="115"/>
        <source>Coordonnée Y :</source>
        <translation>Y Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputehitgrid.cpp" line="116"/>
        <source>Coordonnée Z :</source>
        <translation>Z Coordinate:</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeLAI2000Data</name>
    <message>
        <source>Calcul de données LAI-2000</source>
        <translation type="vanished">Computing of LAI 2000 data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="64"/>
        <source>Calculer données LAI-2000</source>
        <translation>Compute LAI-2000 data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="69"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="83"/>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="86"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="92"/>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="95"/>
        <source>Données LAI2000</source>
        <translation>LAI2000 data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="103"/>
        <source>Résolution de scan</source>
        <translation>Scan resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="85"/>
        <location filename="../step/onf_stepcomputelai2000data.cpp" line="94"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeNestVolume</name>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="74"/>
        <source>Délimiter le volume accessible d&apos;un nid</source>
        <translation>Delineate the accessible volume from a nest</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="91"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="95"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="105"/>
        <source>Hits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="106"/>
        <source>Empty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="107"/>
        <source>Radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="108"/>
        <source>distNest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="109"/>
        <source>toto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="117"/>
        <source>Résolution de la grille</source>
        <translation>Grid resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="117"/>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="118"/>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="119"/>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="124"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="118"/>
        <source>Rayon de recherche</source>
        <translation>Search radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="119"/>
        <source>Seuil en nombre de points</source>
        <translation>Threshold in number of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="121"/>
        <source>Centre X</source>
        <translation>X Center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="122"/>
        <source>Centre Y</source>
        <translation>Y Center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="123"/>
        <source>Centre Z</source>
        <translation>Z Center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="124"/>
        <source>Offset en Z</source>
        <translation>Z Offset</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="127"/>
        <source>Calculer les donées sur </source>
        <translation>Compute data on</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="130"/>
        <source>Une Shpère</source>
        <translation>A Sphere</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputenestvolume.cpp" line="131"/>
        <source>Un Cylindre</source>
        <translation>A Cylinder</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeOcclusionsSpace</name>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="84"/>
        <source>Compute occlusions space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="95"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="99"/>
        <source>Scan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="100"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="101"/>
        <source>Scanner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="114"/>
        <source>Occlusion space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="126"/>
        <source>Resolution of the grids</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="126"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="127"/>
        <source>Distance Threshold</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="131"/>
        <source>Reference for (minX, minY, minZ) corner of the grid :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="134"/>
        <source>Default mode : Bounding box of the scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="135"/>
        <source>Custom mode : Relative to folowing coordinates:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="136"/>
        <source>Custom mode : Relative to folowing coordinates + custom dimensions:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="137"/>
        <source>Custom mode : centered on folowing coordinates + custom dimensions:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="140"/>
        <source>X coordinate:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="141"/>
        <source>Y coordinate:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="142"/>
        <source>Z coordinate:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="144"/>
        <source>X dimension:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="145"/>
        <source>Y dimension:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="146"/>
        <source>Z dimension:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="286"/>
        <location filename="../step/onf_stepcomputeocclusionspace.cpp" line="310"/>
        <source>Dimensions spécifiées ne contenant pas les positions de scans : la grille a du être élargie !</source>
        <translation>Specified dimensions do not contain scans positions: the grid to be expanded!</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputePointHeightAttribute</name>
    <message>
        <source>Calculer la hauteur des points</source>
        <translation type="vanished">Compute point height</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="48"/>
        <source>Calculer la hauteur des points à l&apos;aide d&apos;un MNT</source>
        <translation>Compute point heights using a DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="62"/>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="65"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="64"/>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="69"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="67"/>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="70"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="80"/>
        <source>Height attribute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="81"/>
        <source>Height cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="91"/>
        <source>Créer des attributs de points hauteur</source>
        <translation>Create height points attribute</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputepointheightattribute.cpp" line="92"/>
        <source>Créer un nuage de points hauteur</source>
        <translation>Create height point cloud</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeRelativeIntensityAttribute</name>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="41"/>
        <source>Calculer l&apos;intensité relative</source>
        <translation>Compute relative intensity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="55"/>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="58"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="57"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="59"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="59"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputerelativeintensityattribute.cpp" line="69"/>
        <source>Relative intensity</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeScanDirection</name>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="50"/>
        <source>Calcule les directions de scan (ALS)</source>
        <translation>Computes scan directions (ALS)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="64"/>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="67"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="66"/>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="72"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="68"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="68"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="70"/>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="73"/>
        <source>Trajectory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="83"/>
        <source>Scan direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="187"/>
        <source>Pas d&apos;information de trajectoire pour le point (%1 ; %2 ; %3)</source>
        <translation>No trajectory information for the point (%1 ;%2 ;%3)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputescandirection.cpp" line="200"/>
        <source>Pas d&apos;information de trajectoire pour au total %1 points sur %2</source>
        <translation>No trajectory information for a total of %1 points on %2</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeSlopeRaster</name>
    <message>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="27"/>
        <source>Compute slope raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="33"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="54"/>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="57"/>
        <source>DEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="56"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputesloperaster.cpp" line="67"/>
        <source>Slope</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeSphereVoxels</name>
    <message>
        <source>Calul d&apos;une densité de points corrigée / sphère</source>
        <translation type="vanished">Computing of corrected points density by sphere</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="77"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="91"/>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="94"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="103"/>
        <source>Sphère</source>
        <translation>Sphere</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="111"/>
        <source>Fichier des sphères</source>
        <translation>Spheres file</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="112"/>
        <source>Résolution du scanner</source>
        <translation>Scanner resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="113"/>
        <source>Centre du scanner X</source>
        <translation>Scanner X center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="114"/>
        <source>Centre du scanner Y</source>
        <translation>Scanner Y center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="115"/>
        <source>Centre du scanner Z</source>
        <translation>Scanner Z center</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="100"/>
        <source>Sphères</source>
        <translation>Spheres</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="72"/>
        <source>Caluler densité de points corrigée par sphère</source>
        <translation>Compute corrected points density by sphere</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="93"/>
        <location filename="../step/onf_stepcomputespherevoxels.cpp" line="102"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeStorkTrajectory</name>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="64"/>
        <source>Calcul des trajectoires de Cigognes</source>
        <translation>Compute Stork trajectories</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="81"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="85"/>
        <source>Distances</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="95"/>
        <source>Trajectoire</source>
        <translation>Trajectory</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="102"/>
        <source>Portée de dilatation</source>
        <translation>Dilatation range</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputestorktrajectory.cpp" line="102"/>
        <source>cellules</source>
        <translation>cells</translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeTIN</name>
    <message>
        <location filename="../step/onf_stepcomputetin.cpp" line="54"/>
        <source>Créer TIN à partir de points</source>
        <translation>Create TIN from points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputetin.cpp" line="59"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputetin.cpp" line="72"/>
        <location filename="../step/onf_stepcomputetin.cpp" line="76"/>
        <source>Points sol</source>
        <translation>Ground points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputetin.cpp" line="89"/>
        <source>TIN</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepComputeVerticalProfile</name>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="64"/>
        <source>Créer profil vertical de densité de points</source>
        <translation>Create a vertical points density profile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="70"/>
        <source>Cette étape génère une profil selon l&apos;axe Z.</source>
        <translation>This step creates a profile along Z axis.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="81"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="85"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="93"/>
        <source>ProfilZ</source>
        <translation>ZProfile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="100"/>
        <source>Résolution du profil</source>
        <translation>Profile resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="100"/>
        <source>mètres</source>
        <translation>meters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="102"/>
        <source>Callage de l&apos;origine (X, Y, Z) :</source>
        <translation>How to compute coordinates of the grid corner (minX, minY, minZ):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="105"/>
        <source>Sur la boite englobante de la scène</source>
        <translation>Using bounding box of the scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="106"/>
        <source>Par rapport aux coordonnées suivantes :</source>
        <translation>Multiples of the following  coordinates:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="108"/>
        <source>Coordonnée X :</source>
        <translation>X Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="109"/>
        <source>Coordonnée Y :</source>
        <translation>Y Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcomputeverticalprofile.cpp" line="110"/>
        <source>Coordonnée Z :</source>
        <translation>Z Coordinate:</translation>
    </message>
</context>
<context>
    <name>ONF_StepConvertDEMToPoints</name>
    <message>
        <location filename="../step/onf_stepconvertdemtopoints.cpp" line="63"/>
        <source>Convert DEM to point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertdemtopoints.cpp" line="90"/>
        <location filename="../step/onf_stepconvertdemtopoints.cpp" line="94"/>
        <source>DEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertdemtopoints.cpp" line="104"/>
        <source>Point cloud</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepConvertFloatImageToqint32</name>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="49"/>
        <source>Convertir raster Float en raster quint32</source>
        <translation>Convert float raster to quint32 raster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="55"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="76"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="78"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="79"/>
        <source>Image (float)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertfloatimagetoqint32.cpp" line="89"/>
        <source>Image (qint32)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepConvertSceneToCluster</name>
    <message>
        <source>Conversion d&apos;une scène en cluster ordonné</source>
        <translation type="vanished">Conversion of a scene in ordered cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertscenetocluster.cpp" line="55"/>
        <source>Convertir CT_Scene en CT_PointCluster</source>
        <translation>Convert CT_Scene to CT_PointCluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconvertscenetocluster.cpp" line="61"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
</context>
<context>
    <name>ONF_StepConvertTINtoDTM</name>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="64"/>
        <source>Convertir un TIN en MNT</source>
        <translation>Convert TIN to DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="69"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="82"/>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="86"/>
        <source>TIN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="88"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="95"/>
        <source>Résolution de la grille :</source>
        <translation>Grid resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="103"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepconverttintodtm.cpp" line="163"/>
        <source>Convertion terminée</source>
        <translation>Conversion achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepCorrectALSProfile</name>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="50"/>
        <source>Corriger le profil de densité de points ALS</source>
        <translation>Correct an ALS points density profile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="56"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="77"/>
        <source>Profile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="79"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="80"/>
        <source>Profil</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="90"/>
        <source>Profil corrigé</source>
        <translation>Corrected profile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="91"/>
        <source>Seuil OTSU</source>
        <translation>OTSU threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="92"/>
        <source>Profil corrigé OTSU bas</source>
        <translation>Corrected profile - below OTSU threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="93"/>
        <source>Profil corrigé OTSU haut</source>
        <translation>Corrected profile - above OTSU threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcorrectalsprofile.cpp" line="102"/>
        <source>Supprimer les données en dessous de</source>
        <translation>Remove data below</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateColorComposite</name>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="36"/>
        <source>Create color composite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="42"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="63"/>
        <source>2D Images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="65"/>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="72"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="66"/>
        <source>Red band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="67"/>
        <source>Green band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="68"/>
        <source>Blue band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="70"/>
        <source>DSM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="73"/>
        <source>Z raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatecolorcomposite.cpp" line="83"/>
        <source>Color Composite</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateMaximaCloud</name>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="38"/>
        <source>Créer un nuage de points de maxima</source>
        <translation>Create maxima point cloud</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="44"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="65"/>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="69"/>
        <source>Maxima</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="67"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="68"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="70"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="72"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="83"/>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="86"/>
        <source>Maxima (points)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="97"/>
        <source>Créer un nuage de points</source>
        <translation>Create point cloud</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="98"/>
        <source>Créer des points de référence</source>
        <translation>Create reference points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="118"/>
        <source>Un MNT a été founit, les valeurs Z des maxima seront corrigées</source>
        <translation>A DTM has been provided, Z values of maxima will be corrected</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="120"/>
        <source>Aucun MNT n&apos;a été founit, les valeurs Z des maxima NE seront PAS corrigées</source>
        <translation>No DTM provided, Z values of maxima will NOT be corrected</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatemaximacloud.cpp" line="200"/>
        <source>Valeur manquante dans le MNT pour un l&apos;apex : x=%1 ; y=%2</source>
        <translation>Missing value in DTM for apex: x=%1 ; y=%</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreatePlotManagerFromFile</name>
    <message>
        <source>Créée un gestionnaire de placettes à partir d&apos;un fichier ASCII</source>
        <translation type="vanished">Create a plot manager from ASCII file (plot list)</translation>
    </message>
    <message>
        <source>1- Créée un gestionnaire de placettes à partir d&apos;un fichier ASCII</source>
        <translation type="vanished">1- Add a plot list manager from ASCII file</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="74"/>
        <source>1- Créée une liste de placettes à partir d&apos;un fichier ASCII</source>
        <translation>Create a plot list from ASCII file</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="80"/>
        <source>Charge des placettes depuis un fichier ASCII. &lt;br&gt;L&apos;import est configurable, le fichier devant contenir les champs suivants :&lt;br&gt;- IDplot : Identifiant placette&lt;br&gt;- X      : Coordonnée X de l&apos;arbre&lt;br&gt;- Y      : Coordonnée Y de l&apos;arbre&lt;br&gt;</source>
        <translation>Load plots from an ASCII file. &lt;br&gt;The import is configurable, the file must contain the following fields: &lt;br&gt;- IDplot: Plot identifier &lt;br&gt;- X: X coordinate of the tree &lt;br&gt;- Y: Y coordinate of the tree&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="105"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="108"/>
        <source>Emprise (sans buffer)</source>
        <translation>Footprint (without buffer)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="117"/>
        <source>Placettes</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="118"/>
        <source>Placette</source>
        <translation>Plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="119"/>
        <source>PlotID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="130"/>
        <source>Rayon de placette</source>
        <translation>Plot radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="153"/>
        <source>Champ X non défini</source>
        <translation>X field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="154"/>
        <source>Champ Y non défini</source>
        <translation>Y field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="192"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="239"/>
        <source>%1 placettes sur l&apos;emprise</source>
        <translation>% 1 plots in the footprint</translation>
    </message>
    <message>
        <source>Gestionnaire de placette (Plot List)</source>
        <translation type="vanished">Plot manager (plot list)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagerfromfile.cpp" line="107"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Gestionnaire de placettes (Plot List)</source>
        <translation type="vanished">Plot manager (plot list)</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreatePlotManagerGrid</name>
    <message>
        <source>Créée un gestionnaire de placettes (grille) à partir d&apos;une DataSource</source>
        <translation type="vanished">Create a plot manager (grid) from a DataSource</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="61"/>
        <source>1- Ajoute des gestionnaires de placette (grille)</source>
        <translation>1-  Add a plot grid manager</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="67"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="88"/>
        <source>Tuiles</source>
        <translation>Tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="90"/>
        <source>Groupe Racine</source>
        <translation>Root group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="92"/>
        <source>Emprise (sans buffer)</source>
        <translation>Footprint (without buffer)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="103"/>
        <source>Grille de Placettes</source>
        <translation>Plots grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="112"/>
        <source>Taille de placette (rayon/circulaire ; DEMI-coté/carrée)</source>
        <translation>Plot size (radius/circular ; half_side/square)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="113"/>
        <source>Espacement des placettes</source>
        <translation>Plots spacing</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="116"/>
        <source>Coordonnées de références pour les centres de placettes :</source>
        <translation>Reference coordinates for plots centers:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="117"/>
        <source>Coordonnée X de référence </source>
        <translation>X reference coordinates</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="118"/>
        <source>Coordonnée y de référence </source>
        <translation>Y reference coordinates</translation>
    </message>
    <message>
        <source>Source de données géographique</source>
        <translation type="vanished">Geographical data source</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotmanagergrid.cpp" line="91"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Gestionnaire de placettes (grid)</source>
        <translation type="vanished">Plot manager (grid)</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreatePlotsFromList</name>
    <message>
        <source>2- Créer des placettes à partir d&apos;une liste</source>
        <translation type="vanished">- Create plots from list / grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="58"/>
        <source>2- Créer des placettes à partir d&apos;une grille</source>
        <translation>2- Create plots from grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="64"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="85"/>
        <source>Placettes</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="87"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="88"/>
        <source>Liste de placettes</source>
        <translation>Plots list</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="98"/>
        <source>Placette (Groupe)</source>
        <translation>Plot (group)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="102"/>
        <source>Placette circulaire</source>
        <translation>Circular plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="104"/>
        <source>Placette carrée</source>
        <translation>Square plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="111"/>
        <source>Placette circulaire (buffer)</source>
        <translation>Circular plot (buffer)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="113"/>
        <source>Placette carrée (buffer)</source>
        <translation>Square plot (buffer)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="127"/>
        <source>Type de placette</source>
        <translation>Plot type</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="128"/>
        <source>Créer les buffers</source>
        <translation>Create buffers</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateplotsfromlist.cpp" line="129"/>
        <source>Taille de buffer</source>
        <translation>Buffer size</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreatePointGrid</name>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="61"/>
        <source>Create point voxel grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="79"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="83"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="93"/>
        <source>Grille de points</source>
        <translation>Point grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="101"/>
        <source>Résolution :</source>
        <translation>Resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatepointgrid.cpp" line="126"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateRasterMosaic</name>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="36"/>
        <source>Redallage raster</source>
        <translation>Raster clipping</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="54"/>
        <source>Emprises cibles</source>
        <translation>Target footprints</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="56"/>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="61"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="57"/>
        <source>Emprise cible</source>
        <translation>Target footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="59"/>
        <source>Rasters d&apos;entrée</source>
        <translation>Input rasters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="62"/>
        <source>Raster d&apos;entrée</source>
        <translation>Input raster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreaterastermosaic.cpp" line="77"/>
        <source>Raster</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateSeedGrid</name>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="81"/>
        <source>Create seed voxel grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="100"/>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="158"/>
        <source>Items</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="101"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="104"/>
        <source>Positions 2D</source>
        <translation>2D Positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="105"/>
        <source>Position 2D</source>
        <translation>2D Position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="116"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="119"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="126"/>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="129"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="135"/>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="139"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="148"/>
        <source>Grille de graines</source>
        <translation>Seed grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="159"/>
        <source>Scènes de points</source>
        <translation>Point scenes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="162"/>
        <source>Positions2D + MNT</source>
        <translation>2DPositions + DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgrid.cpp" line="172"/>
        <source>Offset :</source>
        <translation>Offset:</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateSeedGridFromLinesOfScan</name>
    <message>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="70"/>
        <source>Create seed voxel grid from Lines Of Scan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="88"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="93"/>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="97"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreateseedgridfromlinesofscan.cpp" line="106"/>
        <source>Grille de graines</source>
        <translation>Seed grid</translation>
    </message>
</context>
<context>
    <name>ONF_StepCreateTiling</name>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="61"/>
        <source>Créer un dallage</source>
        <translation>Create tiling</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="66"/>
        <source>Créer un dallage couvrant l&apos;emprise des items d&apos;entrée.</source>
        <translation>Create tiling covering footprints of input items</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="79"/>
        <source>Dalles</source>
        <translation>Tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="81"/>
        <source>Grp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="82"/>
        <source>Item (avec BoundingBox)</source>
        <translation>Items (with BoundingBox)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="89"/>
        <source>Taille de la dalle unitaire</source>
        <translation>Size of tile</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="90"/>
        <source>Conserver les emprises vides</source>
        <translation>Keep empty footprints</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="93"/>
        <source>Recaler sur une coordonnée de référence</source>
        <translation>Start on reference coordinates</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="94"/>
        <source>Coordonnée X de référence</source>
        <translation>X reference coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="95"/>
        <source>Coordonnée Y de référence</source>
        <translation>Y reference coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="100"/>
        <source>Emprise Créée</source>
        <translation>Created footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="101"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="102"/>
        <source>Emprise créée</source>
        <translation>Created footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepcreatetiling.cpp" line="147"/>
        <source>Item (id= %1) sans BoundingBox (impossible de déterminer l&apos;emprise)</source>
        <translation>Item (id= %1) without BoundingBox (impossible to compute footprint)</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectLinesOfScan</name>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="72"/>
        <source>Détecter les lignes de scan</source>
        <translation>Detect lines of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="78"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="99"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="101"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="102"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="102"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="103"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="112"/>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="113"/>
        <source>Lignes de scan complètes (toutes)</source>
        <translation>Complete lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="114"/>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="115"/>
        <source>Lignes de scan débruitées (toutes)</source>
        <translation>Denoised lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="116"/>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="117"/>
        <source>Lignes de scan (conservées)</source>
        <translation>Lines of scan (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="126"/>
        <source>1- Détéction des lignes (Toutes)</source>
        <translation>1 - Lines detection (All)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="127"/>
        <source>Seuil de temps GPS pour changer de ligne de scan</source>
        <translation>Threshold on GPS time for changing of line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="130"/>
        <source>2- Débruitage des lignes (Débruitées)</source>
        <translation>2- Denoising lines (Denoised)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="131"/>
        <source>Courbure maximale d&apos;une ligne de scan</source>
        <translation>Maximum cuvature of a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="132"/>
        <source>Distance XY maximale entre points d&apos;une ligne de scan</source>
        <translation>Maximum XY distance between points in a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="136"/>
        <source>3- Filtrage des lignes (Conservées)</source>
        <translation>3- Filtering lines (Conserved)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="137"/>
        <source>Angle zénithal maximal pour conserver une ligne de scan</source>
        <translation>Maximum zenithal angle to keep a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectlinesofscan.cpp" line="138"/>
        <source>Nombre de points minimal pour conserver une ligne de scan</source>
        <translation>Minimum number of points to keep a line of scan</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectSection06</name>
    <message>
        <source>Aggrégation verticale de clusters en billon</source>
        <translation type="vanished">Vertical merging of clusters in logs</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Cette étape prend en entrée des couches horizontales (layers) contenant des clusters.&lt;br&gt;Ce type de structure peut par exemple être produite par l&apos;étape &lt;em&gt;ONF_StepHorizontalClustering&lt;/em&gt;.&lt;br&gt;Les clusters adjacents verticalement sont regroupés en billons (groupes). Pour ce faire :&lt;ul&gt;&lt;li&gt; Les clusters dont la &lt;b&gt;distance verticale&lt;/b&gt; les séparant est inférieure au seuil choisi sont comparés deux à deux.&lt;/li&gt;&lt;li&gt;Si leurs boites englobantes s&apos;intersectent dans le plan XY, les clusters sont regroupés dans la même billon.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="vanished">This step need  horizontal layers containing points clusters as input. &lt;br&gt; This sort of data could for example be produced by the &lt;em&gt;ONF_StepHorizontalClustering&lt;/em&gt;.&lt;br&gt;The clusters which are vertically paralels are gathered in logs (groups). To do it:&lt;ul&gt;&lt;li&gt;Clusters which have a &lt;b&gt;vertical distance&lt;/b&gt; between then lesser than the specified threshold are compared two by two.&lt;/li&gt;&lt;li&gt;If their bounding boxes are intersecting in XY plane, clusters are bring together in the same log.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <source>Aggrégation verticale de clusters en billon (Ancienne Version)</source>
        <translation type="vanished">Vertical merging of clusters in logs (Old Version)</translation>
    </message>
    <message>
        <source>Clusters</source>
        <translation type="vanished">Clusters</translation>
    </message>
    <message>
        <source>Points</source>
        <translation type="vanished">Points</translation>
    </message>
    <message>
        <source>Distance en z (en + et en -) maximum entre deux groupes de points à comparer</source>
        <translation type="vanished">Maximum Z distance (+ or -) between 2 points clusters to compare</translation>
    </message>
    <message>
        <source>Billon</source>
        <translation type="vanished">Log</translation>
    </message>
    <message>
        <source>Billons</source>
        <translation type="vanished">Logs</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectSection07</name>
    <message>
        <source>Aggrégation verticale de clusters en billon</source>
        <translation type="vanished">Vertical merging of clusters in logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="72"/>
        <source>1- Aggréger verticalement les Clusters en Billons</source>
        <translation>1- Merge clusters vertically into logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="77"/>
        <source>Cette étape prend en entrée des couches horizontales (layers) contenant des clusters.&lt;br&gt;Ce type de structure peut par exemple être produite par l&apos;étape &lt;em&gt;ONF_StepHorizontalClustering&lt;/em&gt;.&lt;br&gt;Les clusters adjacents verticalement sont regroupés en billons (groupes). Pour ce faire :&lt;ul&gt;&lt;li&gt; Les clusters dont la &lt;b&gt;distance verticale&lt;/b&gt; les séparant est inférieure au seuil choisi sont comparés deux à deux.&lt;/li&gt;&lt;li&gt;Si leurs boites englobantes s&apos;intersectent dans le plan XY, les clusters sont regroupés dans la même billon.&lt;/li&gt;&lt;/ul&gt;N.B. : Les clusters ayant la plus grande boite englobante XY sont prioritaires.</source>
        <translation>This step need  horizontal layers containing points clusters as input. &lt;br&gt; This sort of data could for example be produced by the &lt;em&gt;ONF_StepHorizontalClustering&lt;/em&gt;.&lt;br&gt;The clusters which are vertically paralels are gathered in logs (groups). To do it:&lt;ul&gt;&lt;li&gt;Clusters which have a &lt;b&gt;vertical distance&lt;/b&gt; between then lesser than the specified threshold are compared two by two.&lt;/li&gt;&lt;li&gt;If their bounding boxes are intersecting in XY plane, clusters are bring together in the same log.&lt;/li&gt;&lt;/ul&gt;N.B.: Clusters with the higher bounding box have the priority.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="103"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="105"/>
        <source>Niveau Z (Grp)</source>
        <translation>Z-Level (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="106"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="107"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="116"/>
        <source>Distance en z (en + et en -) maximum entre deux groupes de points à comparer</source>
        <translation>Maximum Z distance (+ or -) between 2 points clusters to compare</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="155"/>
        <source>Billon</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectsection07.cpp" line="159"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="72"/>
        <source>Détecter des alignements verticaux de points</source>
        <translation>Detect vertical points alignements</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="78"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="99"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="101"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="102"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="112"/>
        <source>Clusters conservés</source>
        <translation>Kept clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="113"/>
        <source>Cluster conservé</source>
        <translation>Kept cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="114"/>
        <source>Ligne conservée</source>
        <translation>Kept line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="115"/>
        <source>Enveloppe convexe projetée</source>
        <translation>Projected convex hull</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="116"/>
        <source>Diamètre Estimé</source>
        <translation>Estimated diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="117"/>
        <source>DistMin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="118"/>
        <source>DistQ25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="119"/>
        <source>DistMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="120"/>
        <source>DistQ75</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="121"/>
        <source>DistMax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="122"/>
        <source>DistMean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="123"/>
        <source>DiamEq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="125"/>
        <source>Clusters éliminés</source>
        <translation>Dropped clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="126"/>
        <source>Cluster éliminé</source>
        <translation>Dropped cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="127"/>
        <source>Ligne éliminée</source>
        <translation>Dropped line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="128"/>
        <source>Enveloppe convexe projetée éliminée</source>
        <translation>Dropped projected convex hull</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="138"/>
        <source>1- Paramètres de validation des droites candidates :</source>
        <translation>1- Validation parameters for candidate lines:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="139"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="140"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="143"/>
        <source>2- Paramètres de création des clusters (à partir des droites candidates) :</source>
        <translation>2 - Parameters for cluster creation (from candidate lines):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="144"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="145"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="148"/>
        <source>3- Paramètres validation des clusters obtenus :</source>
        <translation>3- Validation parameters for obtained clusters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="149"/>
        <source>Supprimer les clusters dont la longueur est inférieure à</source>
        <translation>Drop clusters with a length inferior to</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="150"/>
        <source>Supprimer les clusters qui commence au dessus de </source>
        <translation>Drop clusters beginning above</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="151"/>
        <source>Valeur max. pour (DistMed - DistMoy) / DistMoy</source>
        <translation>Maximum value for (DistMed - DistMean) / DistMean</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments.cpp" line="152"/>
        <source>Seuil de distance par rapport au cercles</source>
        <translation>Distance threshold for circles</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments02</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="81"/>
        <source>Détecter des alignements verticaux de points (V2)</source>
        <translation>Detect vertical points alignements (V2)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="87"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="108"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="110"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="111"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="121"/>
        <source>Clusters conservés</source>
        <translation>Kept clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="122"/>
        <source>Cluster conservé</source>
        <translation>Kept cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="123"/>
        <source>Ligne conservée</source>
        <translation>Kept line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="124"/>
        <source>Enveloppe convexe projetée</source>
        <translation>Projected convex hull</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="125"/>
        <source>Diamètre Estimé</source>
        <translation>Estimated diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="126"/>
        <source>DistMin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="127"/>
        <source>DistQ25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="128"/>
        <source>DistMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="129"/>
        <source>DistQ75</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="130"/>
        <source>DistMax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="131"/>
        <source>DistMean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="132"/>
        <source>MaxDistBetweenPoints</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="133"/>
        <source>DiamEq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="135"/>
        <source>Clusters éliminés</source>
        <translation>Dropped clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="136"/>
        <source>Cluster éliminé</source>
        <translation>Dropped cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="137"/>
        <source>Ligne éliminée</source>
        <translation>Dropped line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="147"/>
        <source>1- Paramètres de création des droites candidates :</source>
        <translation>1- Parameters for creation of candidate lines:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="148"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="149"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="152"/>
        <source>2- Paramètres de création des clusters (à partir des droites candidates) :</source>
        <translation>2 - Parameters for cluster creation (from candidate lines):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="153"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="154"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="155"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="159"/>
        <source>3- Paramètres de validation/fusion des clusters obtenus :</source>
        <translation>3- Validation/Merging parameters for obtained clusters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="160"/>
        <source>Distance de recherche des clusters voisins</source>
        <translation>Search distance for neighbour clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="161"/>
        <source>Diamètre maximal possible pour un arbre</source>
        <translation>Maximum diamter for one tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="162"/>
        <source>Divergence maximale entre clusters à fusionner</source>
        <translation>Maximum divergence between clusters for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="164"/>
        <source>Supprimer les clusters dont la longueur est inférieure à</source>
        <translation>Drop clusters with a length inferior to</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="165"/>
        <source>Supprimer les clusters qui commence au dessus de </source>
        <translation>Drop clusters beginning above</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="169"/>
        <source>4- Paramètres de validation des diamètres :</source>
        <translation>4- Validation parameters for diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="170"/>
        <source>Epaisseur des cercles pour le scoring</source>
        <translation>Circles thickness for scoring</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments02.cpp" line="173"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments03</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="87"/>
        <source>Détecter des alignements verticaux de points (V3)</source>
        <translation>Detect vertical points alignements (V3)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="93"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="114"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="116"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <source>Scène</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="117"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="118"/>
        <source>Ligne de Scan</source>
        <translation>Scan line ID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="119"/>
        <source>Attribut codant la ligne de scan</source>
        <translation>Attribute storing scan line ID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="121"/>
        <source>Scène (complète)</source>
        <translation>Scene (complete)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="131"/>
        <source>Clusters conservés</source>
        <translation>Kept clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="132"/>
        <source>Cluster conservé</source>
        <translation>Kept cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="133"/>
        <source>Ligne conservée</source>
        <translation>Kept line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="134"/>
        <source>Enveloppe convexe projetée</source>
        <translation>Projected convex hull</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="135"/>
        <source>Diamètre Estimé</source>
        <translation>Estimated diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="136"/>
        <source>DistMin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="137"/>
        <source>DistQ25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="138"/>
        <source>DistMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="139"/>
        <source>DistQ75</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="140"/>
        <source>DistMax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="141"/>
        <source>DistMean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="142"/>
        <source>MaxDistBetweenPoints</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="143"/>
        <source>DiamEq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="144"/>
        <source>Hmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="146"/>
        <source>Clusters éliminés</source>
        <translation>Dropped clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="147"/>
        <source>Cluster éliminé</source>
        <translation>Dropped cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="148"/>
        <source>Ligne éliminée</source>
        <translation>Dropped line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="158"/>
        <source>1- Paramètres de création des droites candidates :</source>
        <translation>1- Parameters for creation of candidate lines:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="159"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="160"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="163"/>
        <source>2- Paramètres de création des clusters (à partir des droites candidates) :</source>
        <translation>2 - Parameters for cluster creation (from candidate lines):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="164"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="165"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="166"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="170"/>
        <source>3- Paramètres de validation/fusion des clusters obtenus :</source>
        <translation>3- Validation/Merging parameters for obtained clusters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="171"/>
        <source>Distance de recherche des clusters voisins</source>
        <translation>Search distance for neighbour clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="172"/>
        <source>Diamètre maximal possible pour un arbre</source>
        <translation>Maximum diamter for one tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="173"/>
        <source>Divergence maximale entre clusters à fusionner</source>
        <translation>Maximum divergence between clusters for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="174"/>
        <source>Angle zénithal maximal de la droite après fusion</source>
        <translation>Maximal zenithal angle for the lines after merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="175"/>
        <source>Rayon de recherche pour Hmax</source>
        <translation>Search radius for Hmax</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="177"/>
        <source>Supprimer les clusters dont la longueur est inférieure à</source>
        <translation>Drop clusters with a length inferior to</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="178"/>
        <source>Supprimer les clusters qui commence au dessus de </source>
        <translation>Drop clusters beginning above</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="182"/>
        <source>4- Paramètres de validation des diamètres :</source>
        <translation>4- Validation parameters for diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="183"/>
        <source>Ecart max Dmax n et n-1</source>
        <translation>Maximum gap for Dmax between n and n-1</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="184"/>
        <source>Epaisseur des cercles pour le scoring</source>
        <translation>Thickness of horizontal slices</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments03.cpp" line="187"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments04</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="93"/>
        <source>Détecter des alignements verticaux de points (V4)</source>
        <translation>Detect vertical points alignements (V4)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="99"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="120"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="122"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="123"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="124"/>
        <source>Temps GPS</source>
        <translation>GPS time</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="124"/>
        <source>Attribut codant le temps GPS</source>
        <translation>Attribut coding GPS time</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="125"/>
        <source>Intensité</source>
        <translation>Intensity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="125"/>
        <source>Attribut codant l&apos;intensité</source>
        <translation>Attribute coding intensity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="127"/>
        <source>Scène (complète)</source>
        <translation>Scene (complete)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="137"/>
        <source>Tiges</source>
        <translation>Stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="138"/>
        <source>Cluster</source>
        <translation>Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="139"/>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="141"/>
        <source>Diamètre</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="140"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="140"/>
        <source>0 = petite tige ; 1 = grosse tige</source>
        <translation>0 = small stem ; 1 = big stem</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="142"/>
        <source>Droite ajustée</source>
        <translation>Ajusted line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="146"/>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="148"/>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="150"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="147"/>
        <source>Lignes de scan complètes (toutes)</source>
        <translation>Complete lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="149"/>
        <source>Lignes de scan débruitées (toutes)</source>
        <translation>Denoised lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="151"/>
        <source>Lignes de scan (conservées)</source>
        <translation>Lines of scan (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="161"/>
        <source>1- Détéction des lignes de scan :</source>
        <translation>1-  Detection of lines of scan:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="162"/>
        <source>Seuil de temps GPS pour changer de ligne de scan</source>
        <translation>Threshold on GPS time for changing of line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="163"/>
        <source>Courbure maximale d&apos;une ligne de scan</source>
        <translation>Maximum cuvature of a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="164"/>
        <source>Distance XY maximale entre points d&apos;une ligne de scan</source>
        <translation>Maximum XY distance between points in a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="165"/>
        <source>Angle zénithal maximal pour conserver une ligne de scan</source>
        <translation>Maximum zenithal angle to keep a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="166"/>
        <source>Ne conserver que les lignes de scan avec au moins</source>
        <translation>Only keep lines of scan with at less</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="169"/>
        <source>2- Détéction des grosses tiges (fusion des lignes de scan adjacentes) :</source>
        <translation>2- Detection of big stems (merging adjacent lines of scan):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="170"/>
        <source>Multiplicateur pour la courbure</source>
        <translation>Multiplicator for curvature</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="171"/>
        <source>Increment en distance par point</source>
        <translation>Distance increment per point</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="172"/>
        <source>Distance de recherche maximum</source>
        <translation>Maximum search distance</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="175"/>
        <source>3- Détéction de la direction des grosses tiges:</source>
        <translation>3- Detection of big stems direction:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="176"/>
        <source>Résolution en azimuth / angle zénithal</source>
        <translation>Resolution in Azimut / Zenithal angle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="177"/>
        <source>Angle zénithal maximal</source>
        <translation>Maximum zenithal angle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="180"/>
        <source>4- Détéction des petites tiges (alignements) :</source>
        <translation>4- Detection of small stems (alignements):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="181"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="182"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="183"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="184"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="185"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="186"/>
        <source>Rayon d&apos;exclusion autour des grosses tiges</source>
        <translation>Exclusion radius around big stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="189"/>
        <source>5- Estimation des diamètres :</source>
        <translation>5- Estimation of diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="190"/>
        <source>Ratio maximal diamètre / nb. points</source>
        <translation>Maximum ratio diameter / number of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="191"/>
        <source>Diamètre minimal</source>
        <translation>Maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="192"/>
        <source>Diamètre maximal des petites tiges</source>
        <translation>Maximum diameter of small stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="193"/>
        <source>Nombre de points équivalents au diamètre maximal</source>
        <translation>Number of points equivalent to maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments04.cpp" line="196"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments05</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="91"/>
        <source>Détecter des alignements verticaux de points (V5)</source>
        <translation>Detect vertical points alignements (V5)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="97"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="118"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="120"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="121"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="122"/>
        <source>Temps GPS</source>
        <translation>GPS time</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="122"/>
        <source>Attribut codant le temps GPS</source>
        <translation>Attribut coding GPS time</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="123"/>
        <source>Intensité</source>
        <translation>Intensity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="123"/>
        <source>Attribut codant l&apos;intensité</source>
        <translation>Attribute coding intensity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="125"/>
        <source>Scène (complète)</source>
        <translation>Scene (complete)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="135"/>
        <source>Tiges</source>
        <translation>Stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="136"/>
        <source>Cluster</source>
        <translation>Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="137"/>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="139"/>
        <source>Diamètre</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="138"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="138"/>
        <source>0 = petite tige ; 1 = grosse tige</source>
        <translation>0 = small stem ; 1 = big stem</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="140"/>
        <source>Droite ajustée</source>
        <translation>Ajusted line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="144"/>
        <source>Sphère</source>
        <translation>Sphere</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="146"/>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="148"/>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="150"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="147"/>
        <source>Lignes de scan complètes (toutes)</source>
        <translation>Complete lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="149"/>
        <source>Lignes de scan débruitées (toutes)</source>
        <translation>Denoised lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="151"/>
        <source>Lignes de scan (conservées)</source>
        <translation>Lines of scan (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="161"/>
        <source>1- Détéction des lignes de scan :</source>
        <translation>1-  Detection of lines of scan:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="162"/>
        <source>Seuil de temps GPS pour changer de ligne de scan</source>
        <translation>Threshold on GPS time for changing of line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="163"/>
        <source>Courbure maximale d&apos;une ligne de scan</source>
        <translation>Maximum cuvature of a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="164"/>
        <source>Distance XY maximale entre points d&apos;une ligne de scan</source>
        <translation>Maximum XY distance between points in a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="165"/>
        <source>Angle zénithal maximal pour conserver une ligne de scan</source>
        <translation>Maximum zenithal angle to keep a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="166"/>
        <source>Ne conserver que les lignes de scan avec au moins</source>
        <translation>Only keep lines of scan with at less</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="169"/>
        <source>2- Estimation du diamètre des grosses tiges :</source>
        <translation>3- Detection of big stems direction:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="170"/>
        <source>Distance de recherche des voisins</source>
        <translation>Distance for neighbourood search:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="171"/>
        <source>Diamètre maximal</source>
        <translation>Maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="172"/>
        <source>Résolution pour la recherche de diamètre</source>
        <translation>Resolution for diameter search</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="175"/>
        <source>3- Détéction des petites tiges (alignements) :</source>
        <translation>4- Detection of small stems (alignements):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="176"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="177"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="178"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="179"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="180"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="181"/>
        <source>Rayon d&apos;exclusion autour des grosses tiges</source>
        <translation>Exclusion radius around big stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="184"/>
        <source>4- Estimation des diamètres :</source>
        <translation>5- Estimation of diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="185"/>
        <source>Ratio maximal diamètre / nb. points</source>
        <translation>Maximum ratio diameter / number of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="186"/>
        <source>Diamètre minimal</source>
        <translation>Maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="187"/>
        <source>Diamètre maximal des petites tiges</source>
        <translation>Maximum diameter of small stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="188"/>
        <source>Nombre de points équivalents au diamètre maximal</source>
        <translation>Number of points equivalent to maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments05.cpp" line="191"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments06</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="105"/>
        <source>Détecter des alignements verticaux de points (V6)</source>
        <translation>Detect vertical points alignements (V6)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="111"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="132"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="134"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="135"/>
        <source>Scène (complète)</source>
        <translation>Scene (complete)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="136"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="136"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="137"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="147"/>
        <source>Tiges</source>
        <translation>Stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="149"/>
        <source>Cluster</source>
        <translation>Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="150"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="155"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="159"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="160"/>
        <source>Diamètre</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="151"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="156"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="161"/>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="152"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="157"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="162"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="152"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="157"/>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="162"/>
        <source>0 = petite tige ; 1 = grosse tige</source>
        <translation>0 = small stem ; 1 = big stem</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="154"/>
        <source>Droite ajustée</source>
        <translation>Ajusted line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="163"/>
        <source>MaxHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="164"/>
        <source>Diamètre corrigé</source>
        <translation>Corrected diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="165"/>
        <source>Flag Allométrie</source>
        <translation>Allometry flag</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="170"/>
        <source>Debug (toutes)</source>
        <translation>Debug (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="171"/>
        <source>Lignes de scan complètes (toutes)</source>
        <translation>Complete lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="172"/>
        <source>Debug (débruitées)</source>
        <translation>Debug (denoised)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="173"/>
        <source>Lignes de scan débruitées (toutes)</source>
        <translation>Denoised lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="174"/>
        <source>Debug (conservées)</source>
        <translation>Debug (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="175"/>
        <source>Lignes de scan (conservées)</source>
        <translation>Lines of scan (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="185"/>
        <source>1- Détéction des lignes de scan :</source>
        <translation>1-  Detection of lines of scan:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="186"/>
        <source>Seuil de temps GPS pour changer de ligne de scan</source>
        <translation>Threshold on GPS time for changing of line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="187"/>
        <source>Courbure maximale d&apos;une ligne de scan</source>
        <translation>Maximum cuvature of a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="188"/>
        <source>Distance XY maximale entre points d&apos;une ligne de scan</source>
        <translation>Maximum XY distance between points in a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="189"/>
        <source>Angle zénithal maximal pour conserver une ligne de scan</source>
        <translation>Maximum zenithal angle to keep a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="190"/>
        <source>Ne conserver que les lignes de scan avec au moins</source>
        <translation>Only keep lines of scan with at less</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="193"/>
        <source>2- Estimation du diamètre :</source>
        <translation>5- Estimation of diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="194"/>
        <source>Diamètre minimal</source>
        <translation>Minimum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="195"/>
        <source>Diamètre maximal</source>
        <translation>Maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="196"/>
        <source>Diamètre maximal des tiges de sous-étage</source>
        <translation>Maximum diameter of understorey stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="197"/>
        <source>Distance de recherche des voisins</source>
        <translation>Distance for neighbourood search:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="198"/>
        <source>Ecartement maximal des lignes de scan</source>
        <translation>Maximum spacing of lines of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="199"/>
        <source>Résolution pour la recherche de tronc</source>
        <translation>Resolution for stem search</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="200"/>
        <source>Appliquer une fonction sigmoide pour le scoring</source>
        <translation>Apply a sigmoid function for scoring</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="201"/>
        <source>Fonction Sigmoide : coefficient K</source>
        <translation>Sigmoid function: coefficient K</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="202"/>
        <source>Fonction Sigmoide : coordonnée x0 du point d&apos;inflexion </source>
        <translation>Sigmoid function: coordinate of x0 inflexion poitn</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="203"/>
        <source>Ratio maximal diamètre / nb. points</source>
        <translation>Maximum ratio diameter / number of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="204"/>
        <source>Multiplicateur de diamètre pour les lignes de scan uniques</source>
        <translation>Diamter multiplicator for unique lines of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="207"/>
        <source>3- Détéction des petites tiges (alignements) :</source>
        <translation>4- Detection of small stems (alignements):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="208"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="209"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="210"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="211"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="212"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="215"/>
        <source>4- Relation allometrique (H - Hmax)*(D - a*(H - 1.3)) = m :</source>
        <translation>4- Allometric relation (H - Hmax)*(D - a*(H - 1.3)) = m :</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="216"/>
        <source>Rayon de recherche pour Hmax</source>
        <translation>Search radius for Hmax</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="217"/>
        <source>Paramètre Hmax</source>
        <translation>Parameter Hmax</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="218"/>
        <source>Paramètre a</source>
        <translation>Parameter a</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="219"/>
        <source>Paramètre m</source>
        <translation>Parameter m</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="220"/>
        <source>Tolérance sur H (+/-)</source>
        <translation>Tolerance on H (+/-)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="221"/>
        <source>Fichier de paramètres (rayons d&apos;exclusion)</source>
        <translation>Parameter file (exclusion radii)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments06.cpp" line="224"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDetectVerticalAlignments07</name>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="110"/>
        <source>Détecter des alignements verticaux de points (V7)</source>
        <translation>Detect vertical points alignements (V7)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="116"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="137"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="139"/>
        <source>Scènes (grp)</source>
        <translation>Scenes (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="140"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="140"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="141"/>
        <source>Scène (complète)</source>
        <translation>Scene (complete)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="142"/>
        <source>Scène (tiges)</source>
        <translation>Scene (stems)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="144"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="154"/>
        <source>Tiges</source>
        <translation>Stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="156"/>
        <source>Cluster</source>
        <translation>Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="157"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="162"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="166"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="167"/>
        <source>Diamètre</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="158"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="163"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="168"/>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="159"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="164"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="169"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="159"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="164"/>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="169"/>
        <source>0 = petite tige ; 1 = grosse tige</source>
        <translation>0 = small stem ; 1 = big stem</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="161"/>
        <source>Droite ajustée</source>
        <translation>Ajusted line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="170"/>
        <source>Diamètre corrigé</source>
        <translation>Corrected diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="171"/>
        <source>Flag Allométrie</source>
        <translation>Allometry flag</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="172"/>
        <source>X point max intensité</source>
        <translation>Max intensity point X</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="173"/>
        <source>Y point max intensité</source>
        <translation>Max intensity point Y</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="174"/>
        <source>Z point max intensité</source>
        <translation>Max intensity point Z</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="178"/>
        <source>Debug (toutes)</source>
        <translation>Debug (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="179"/>
        <source>Lignes de scan complètes (toutes)</source>
        <translation>Complete lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="180"/>
        <source>Debug (débruitées)</source>
        <translation>Debug (denoised)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="181"/>
        <source>Lignes de scan débruitées (toutes)</source>
        <translation>Denoised lines of scan (all)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="182"/>
        <source>Debug (conservées)</source>
        <translation>Debug (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="183"/>
        <source>Lignes de scan (conservées)</source>
        <translation>Lines of scan (kept)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="193"/>
        <source>1- Détéction des lignes de scan :</source>
        <translation>1-  Detection of lines of scan:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="194"/>
        <source>Seuil de temps GPS pour changer de ligne de scan</source>
        <translation>Threshold on GPS time for changing of line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="195"/>
        <source>Courbure maximale d&apos;une ligne de scan</source>
        <translation>Maximum cuvature of a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="196"/>
        <source>Distance XY maximale entre points d&apos;une ligne de scan</source>
        <translation>Maximum XY distance between points in a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="197"/>
        <source>Angle zénithal maximal pour conserver une ligne de scan</source>
        <translation>Maximum zenithal angle to keep a line of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="198"/>
        <source>Ne conserver que les lignes de scan avec au moins</source>
        <translation>Only keep lines of scan with at less</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="199"/>
        <source>Récupérer les parties de lignes perdues</source>
        <translation>Retreive lost lines parts</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="202"/>
        <source>2- Estimation du diamètre :</source>
        <translation>5- Estimation of diameters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="203"/>
        <source>Diamètre minimal</source>
        <translation>Minimum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="204"/>
        <source>Diamètre maximal</source>
        <translation>Maximum diameter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="205"/>
        <source>Diamètre maximal des tiges de sous-étage</source>
        <translation>Maximum diameter of understorey stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="206"/>
        <source>Distance de recherche des voisins</source>
        <translation>Distance for neighbourood search:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="207"/>
        <source>Ecartement maximal</source>
        <translation>Maximum spacing</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="208"/>
        <source>Résolution pour la recherche de tronc</source>
        <translation>Resolution for stem search</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="209"/>
        <source>Angle zénithal maximal pour la recherche de tronc</source>
        <translation>Maximal zenithal angle for stem search</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="210"/>
        <source>Proportion du rayon où les alignements sont à exclure</source>
        <translation>Radius propostion where alignments are to be excluded</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="212"/>
        <source>Appliquer une fonction sigmoide pour le scoring</source>
        <translation>Apply a sigmoid function for scoring</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="213"/>
        <source>Fonction Sigmoide : coefficient K</source>
        <translation>Sigmoid function: coefficient K</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="214"/>
        <source>Fonction Sigmoide : coordonnée x0 du point d&apos;inflexion </source>
        <translation>Sigmoid function: coordinate of x0 inflexion poitn</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="216"/>
        <source>Ratio maximal diamètre / nb. points</source>
        <translation>Maximum ratio diameter / number of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="217"/>
        <source>Multiplicateur de diamètre pour les lignes de scan uniques</source>
        <translation>Diamter multiplicator for unique lines of scan</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="220"/>
        <source>3- Détéction des petites tiges (alignements) :</source>
        <translation>3- Detection of small stems (alignements):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="221"/>
        <source>Distance maximum entre deux points d&apos;une droite candidate</source>
        <translation>Maximum distance between two points for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="222"/>
        <source>Angle zénithal maximal pour une droite candidate</source>
        <translation>Maximum zenithal angle for a candidate line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="223"/>
        <source>Distance maximum XY entre deux droites candidates à agréger</source>
        <translation>Maximum XY distance between two candidates lines for merging</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="224"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="225"/>
        <source>Pourcentage maximum de la longueur de segment sans points</source>
        <translation>Maximum percentage of segment length without points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="228"/>
        <source>4- Continuité verticale :</source>
        <translation>4- Vertical continuity:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="229"/>
        <source>Limite basse</source>
        <translation>Lower limit</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="230"/>
        <source>Limite haute</source>
        <translation>Upper limit</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="231"/>
        <source>Rayon de recherche</source>
        <translation>Search radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="232"/>
        <source>Saut maximal en Z entre deux points successifs</source>
        <translation>Maximal jump in Z between two successive points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdetectverticalalignments07.cpp" line="236"/>
        <source>Mode Debug Clusters</source>
        <translation>Clusters debug mode</translation>
    </message>
</context>
<context>
    <name>ONF_StepDilateBoolGrid</name>
    <message>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="63"/>
        <source>Dilatation d&apos;une grille booléenne</source>
        <translation>Dilate bool grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="80"/>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="84"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="93"/>
        <source>Grille filtrée</source>
        <translation>Filtered gird</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="100"/>
        <source>Portée de dilatation</source>
        <translation>Dilatation range</translation>
    </message>
    <message>
        <location filename="../step/onf_stepdilateboolgrid.cpp" line="100"/>
        <source>cellules</source>
        <translation>cells</translation>
    </message>
</context>
<context>
    <name>ONF_StepExportRastersInTable</name>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="77"/>
        <source>Exporter multi-raster dans une table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="82"/>
        <source>Cette étape permet d&apos;exporter une série de rasters (cohérents) dans une table.La raster de référence détermine la résolution et l&apos;emprise a priori.Une emprise peut être fournie pour filtrer les données à exporter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="97"/>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="100"/>
        <source>Rasters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="101"/>
        <source>Raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="103"/>
        <source>Emprise (optionnel)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="107"/>
        <source>Emprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="111"/>
        <source>Compteur (optionnel - nom adaptatif)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="113"/>
        <source>Compteur</source>
        <translation type="unfinished">Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="120"/>
        <source>Choix du fichier d&apos;export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="120"/>
        <source>Fichier texte (*.txt)</source>
        <translation type="unfinished">Text file (*.txt)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="121"/>
        <source>Pas de préfixe (si export adaptatif)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="236"/>
        <source>Tous les rasters n&apos;ont pas la même emprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepexportrastersintable.cpp" line="237"/>
        <source>Tous les rasters n&apos;ont pas la même résolution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractDiametersFromCylinders</name>
    <message>
        <source>Calcul d&apos;un diamètre moyen des cylindres / billon</source>
        <translation type="vanished">Computing of cylinders mean diameter by log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="67"/>
        <source>7- Calcul d&apos;un DBH par Billon à partir des Cylindres</source>
        <translation>7- Compute one DBH per log from cylinders</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="72"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="85"/>
        <source>Billons contenant des cylindres</source>
        <translation>Logs containing cylinders</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="87"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="88"/>
        <source>Cordonnée MNT</source>
        <translation>DTM coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="89"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="90"/>
        <source>Cylindre</source>
        <translation>Cylinder</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="97"/>
        <source>Hauteur de référence : </source>
        <translation>Reference height:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="98"/>
        <source>Hauteur minimale d&apos;évaluation :</source>
        <translation>Bottom height for evaluation:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="99"/>
        <source>Hauteur maximale d&apos;évaluation : </source>
        <translation>Top height for evaluation:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="100"/>
        <source>Décroissance métrique maximale : </source>
        <translation>Maximal taper:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="101"/>
        <source>Nombre de cylindres minimum pour ajuster un cercle : </source>
        <translation>Minimum number of cylinder to adjust a circle:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractdiametersfromcylinders.cpp" line="109"/>
        <source>Diamètre à 1.30m</source>
        <translation>Diameter at 1.30 m</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractLogBuffer</name>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="64"/>
        <source>Extraire les points autour d&apos;un Billon</source>
        <translation>Extract points around a log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="70"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="91"/>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="94"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="93"/>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="99"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="96"/>
        <source>Cerles</source>
        <translation>Circles</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="98"/>
        <source>Billon</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="100"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractlogbuffer.cpp" line="110"/>
        <source>Points extraits</source>
        <translation>Extracted points</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPlot</name>
    <message>
        <source>Extraction d&apos;une placette</source>
        <translation type="vanished">Extraction of plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="67"/>
        <source>Extraire une Placette circulaire</source>
        <translation>Extract a circular plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="72"/>
        <source>Cette étape permet d&apos;extraire les points de la scène d&apos;entrée contenus dans une placette circulaire.&lt;br&gt;On définit dans les paramètres son &lt;b&gt;centre (X,Y)&lt;/b&gt;, son &lt;b&gt;rayon&lt;/b&gt; (maximal), le &lt;b&gt;niveau Z minimum&lt;/b&gt; et le &lt;b&gt;niveau Z maximum&lt;/b&gt;.&lt;br&gt;Si on définit un &lt;b&gt;rayon de début de placette&lt;/b&gt;, cela permet d&apos;obtenir une placette annulaire.&lt;br&gt;On peut également définir un &lt;b&gt;azimut de début&lt;/b&gt; et un &lt;b&gt;azimut de fin&lt;/b&gt;, pour obtenir un secteur.</source>
        <translation>This step extracts a circular plot in the input points scene.&lt;br&gt;It defines the following parameters: &lt;b&gt;plot center (X,Y)&lt;/b&gt;, &lt;b&gt;plot radius&lt;/b&gt; (maximum), the &lt;b&gt;minimum Z level&lt;/b&gt; and the &lt;b&gt;maximum Z level&lt;/b&gt;.&lt;br&gt;The following optional parameters are also available: &lt;b&gt;beginning radius of the plot&lt;/b&gt; allowing to obtain an annulus, a &lt;b&gt;beginning azimut&lt;/b&gt; and an &lt;b&gt;ending azimut&lt;/b&gt; giving a sector. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="88"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="92"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <source>Scène(s) extraites</source>
        <translation type="vanished">Exctracted scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="102"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="110"/>
        <source>Coordonnée X du centre de la placette :</source>
        <translation>X coordinate of the plot center:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="111"/>
        <source>Coordonnée Y du centre de la placette :</source>
        <translation>Y coordinate of the plot center:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="112"/>
        <source>Rayon de début de la placette :</source>
        <translation>Beginning radius of plot:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="113"/>
        <source>Rayon de la placette (maximum) :</source>
        <translation>Ending radius of plot:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="114"/>
        <source>Azimut début (Nord = axe Y) :</source>
        <translation>Beginning azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="114"/>
        <location filename="../step/onf_stepextractplot.cpp" line="115"/>
        <source>Grades</source>
        <translation>Grades</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="115"/>
        <source>Azimut fin (Nord = axe Y) :</source>
        <translation>Ending azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="116"/>
        <source>Niveau Z minimum :</source>
        <translation>Minimum Z for plot:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="117"/>
        <source>Niveau Z maximum :</source>
        <translation>Maximum Z for plot:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="136"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="241"/>
        <source>La scène extraite comporte %1 points.</source>
        <translation>The extracted scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplot.cpp" line="244"/>
        <source>Aucun point n&apos;est dans l&apos;emprise choisie</source>
        <translation>No point in selected plot</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPlotBasedOnDTM</name>
    <message>
        <source>Cette étape permet d&apos;extraire les points de la scène d&apos;entrée contenus dans une placette circulaire.&lt;br&gt;On définit dans les paramètres son &lt;b&gt;centre (X,Y)&lt;/b&gt;, son &lt;b&gt;rayon&lt;/b&gt; (maximal), le &lt;b&gt;niveau Z minimum&lt;/b&gt; et le &lt;b&gt;niveau Z maximum&lt;/b&gt;.&lt;br&gt;Si on définit un &lt;b&gt;rayon de début de placette&lt;/b&gt;, cela permet d&apos;obtenir une placette annulaire.&lt;br&gt;On peut également définir un &lt;b&gt;azimut de début&lt;/b&gt; et un &lt;b&gt;azimut de fin&lt;/b&gt;, pour obtenir un secteur.&lt;br&gt;Cette étape fonctionne comme &lt;em&gt;ONF_StepExtractPlot&lt;/em&gt;, mais les niveaux Z sont spécifiés sous forme de hauteurs par rapport au MNT de référence choisi.</source>
        <translation type="obsolete">This step extracts a circular plot in the input points scene.&lt;br&gt;It defines the following parameters: &lt;b&gt;plot center (X,Y)&lt;/b&gt;, &lt;b&gt;plot radius&lt;/b&gt; (maximum), the &lt;b&gt;minimum Z level&lt;/b&gt; and the &lt;b&gt;maximum Z level&lt;/b&gt;.&lt;br&gt;The following optional parameters are also available: &lt;b&gt;beginning radius of the plot&lt;/b&gt; allowing to obtain an annulus, a &lt;b&gt;beginning azimut&lt;/b&gt; and an &lt;b&gt;ending azimut&lt;/b&gt; giving a sector.&lt;br&gt;This step work like &lt;em&gt;ONF_StepExtractPlot&lt;/em&gt;, but Z levels are specified as an height from choosen reference DTM. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="70"/>
        <source>Extraire les points dans une tranche parallèle au MNT</source>
        <translation>Extract point in a slice parallel to DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="75"/>
        <source>Cette étape permet d&apos;extraire les points de la scène d&apos;entrée contenus dans une tranche de hauteur depuis le MNT.</source>
        <translation>This step extracts points from input scene, in a height slice from DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="88"/>
        <source>MNT (Raster)</source>
        <translation>DTM (raster)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="91"/>
        <source>Modèle Numérique de Terrain</source>
        <translation>Digital Terrain Model</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="93"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="96"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="103"/>
        <source>H minimum :</source>
        <translation>Minimum Height:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="104"/>
        <source>H maximum :</source>
        <translation>Maximum Height:</translation>
    </message>
    <message>
        <source>Coordonnee X du centre de la placette :</source>
        <translation type="obsolete">X coordinate of the plot center:</translation>
    </message>
    <message>
        <source>Coordonnee Y du centre de la placette :</source>
        <translation type="obsolete">Y coordinate of the plot center:</translation>
    </message>
    <message>
        <source>Rayon de debut de la placette</source>
        <translation type="obsolete">Beginning radius of plot:</translation>
    </message>
    <message>
        <source>Rayon de la placette :</source>
        <translation type="obsolete">Ending radius of plot:</translation>
    </message>
    <message>
        <source>Azimut debut (Nord = axe Y) :</source>
        <translation type="obsolete">Beginning azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <source>Azimut fin (Nord = axe Y) :</source>
        <translation type="obsolete">Ending azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <source>Z minimum :</source>
        <translation type="obsolete">Minimum Z for plot:</translation>
    </message>
    <message>
        <source>Z maximum :</source>
        <translation type="obsolete">Maximum Z for plot:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="112"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="141"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="184"/>
        <source>La scène extraite comporte %1 points.</source>
        <translation>The extracted scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractplotbasedondtm.cpp" line="188"/>
        <source>Aucun point n&apos;est dans l&apos;emprise choisie</source>
        <translation>Not point in selected plot</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPlotBasedOnMNT02</name>
    <message>
        <source>Extraction d&apos;une&apos; placette // MNT</source>
        <translation type="vanished">Extraction of plot // DTM</translation>
    </message>
    <message>
        <source>Cette étape permet d&apos;extraire les points de la scène d&apos;entrée contenus dans une placette circulaire.&lt;br&gt;On définit dans les paramètres son &lt;b&gt;centre (X,Y)&lt;/b&gt;, son &lt;b&gt;rayon&lt;/b&gt; (maximal), le &lt;b&gt;niveau Z minimum&lt;/b&gt; et le &lt;b&gt;niveau Z maximum&lt;/b&gt;.&lt;br&gt;Si on définit un &lt;b&gt;rayon de début de placette&lt;/b&gt;, cela permet d&apos;obtenir une placette annulaire.&lt;br&gt;On peut également définir un &lt;b&gt;azimut de début&lt;/b&gt; et un &lt;b&gt;azimut de fin&lt;/b&gt;, pour obtenir un secteur.&lt;br&gt;Cette étape fonctionne comme &lt;em&gt;ONF_StepExtractPlot&lt;/em&gt;, mais les niveaux Z sont spécifiés sous forme de hauteurs par rapport au MNT de référence choisi.</source>
        <translation type="vanished">This step extracts a circular plot in the input points scene.&lt;br&gt;It defines the following parameters: &lt;b&gt;plot center (X,Y)&lt;/b&gt;, &lt;b&gt;plot radius&lt;/b&gt; (maximum), the &lt;b&gt;minimum Z level&lt;/b&gt; and the &lt;b&gt;maximum Z level&lt;/b&gt;.&lt;br&gt;The following optional parameters are also available: &lt;b&gt;beginning radius of the plot&lt;/b&gt; allowing to obtain an annulus, a &lt;b&gt;beginning azimut&lt;/b&gt; and an &lt;b&gt;ending azimut&lt;/b&gt; giving a sector.&lt;br&gt;This step work like &lt;em&gt;ONF_StepExtractPlot&lt;/em&gt;, but Z levels are specified as an height from choosen reference DTM. </translation>
    </message>
    <message>
        <source>MNT (Raster)</source>
        <translation type="vanished">DTM (raster)</translation>
    </message>
    <message>
        <source>Modèle Numérique de Terrain</source>
        <translation type="vanished">Digital Terrain Model</translation>
    </message>
    <message>
        <source>Scène(s)</source>
        <translation type="vanished">Scene(s)</translation>
    </message>
    <message>
        <source>Scène</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <source>Coordonnee X du centre de la placette :</source>
        <translation type="vanished">X coordinate of the plot center:</translation>
    </message>
    <message>
        <source>Coordonnee Y du centre de la placette :</source>
        <translation type="vanished">Y coordinate of the plot center:</translation>
    </message>
    <message>
        <source>Rayon de debut de la placette</source>
        <translation type="vanished">Beginning radius of plot:</translation>
    </message>
    <message>
        <source>Rayon de la placette :</source>
        <translation type="vanished">Ending radius of plot:</translation>
    </message>
    <message>
        <source>Azimut debut (Nord = axe Y) :</source>
        <translation type="vanished">Beginning azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <source>Azimut fin (Nord = axe Y) :</source>
        <translation type="vanished">Ending azimut of plot (North = Y axis):</translation>
    </message>
    <message>
        <source>Z minimum :</source>
        <translation type="vanished">Minimum Z for plot:</translation>
    </message>
    <message>
        <source>Z maximum :</source>
        <translation type="vanished">Maximum Z for plot:</translation>
    </message>
    <message>
        <source>Scène(s) extraites</source>
        <translation type="vanished">Exctracted scene(s)</translation>
    </message>
    <message>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation type="vanished">The input scene contains %1 points.</translation>
    </message>
    <message>
        <source>La scène extraite comporte %1 points.</source>
        <translation type="vanished">The extracted scene contains %1 points.</translation>
    </message>
    <message>
        <source>Aucun point n&apos;est dans l&apos;emprise choisie</source>
        <translation type="vanished">Not point in selected plot</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPointsForPlots</name>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="60"/>
        <source>3- Extraire les points par placette</source>
        <translation>3- Extract point per plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="66"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="87"/>
        <source>Placettes</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="89"/>
        <source>Groupe Scene</source>
        <translation>Scene group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="90"/>
        <source>Scène complete</source>
        <translation>Complete scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="91"/>
        <source>Groupe Placette</source>
        <translation>Plot group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="92"/>
        <source>Emprise placette</source>
        <translation>Plot footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="101"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="109"/>
        <source>Taille de la cellule de QuadTree pour l&apos;optimisation</source>
        <translation>Cell size for optimization Quatree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsforplots.cpp" line="202"/>
        <source>Le nuage de points contient %1 points</source>
        <translation>The point could contains %1 points</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPointsFromGrid</name>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="69"/>
        <source>Segmenter une scène à partir d&apos;une grille d&apos;indices</source>
        <translation>Segment a scene using an indice grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="96"/>
        <source>Grilles</source>
        <translation>Grids</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="100"/>
        <source>Scene à segmenter</source>
        <translation>Scene to be segmented</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="101"/>
        <source>Grille segmentée</source>
        <translation>Indice grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="111"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="112"/>
        <source>Scènes segmentées</source>
        <translation>Segmented scenes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsfromgrid.cpp" line="113"/>
        <source>ID cluster</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPointsInVerticalCylinders</name>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="61"/>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="189"/>
        <source>Rayon</source>
        <translation>Radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="76"/>
        <source>Extraire points dans des cylindres verticaux (par placette)</source>
        <translation>Extract points in vertical cylinders (by plot)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="82"/>
        <source>Cette étape permet d&apos;extraire des sous-parties cylindriques de scènes de points.&lt;br&gt;Un fichier ASCII passé en paramètre permet de définir les cylindres pour chaque placette (une ligne par cylindre).&lt;br&gt;Ce fichier doit contenir les champs suivants :&lt;br&gt;- ID_Plot : Identifiant placette&lt;br&gt;- ID_Tree : Identifiant du cylindre à extraire&lt;br&gt;- X : Coordonnée X du centre du cylindre&lt;br&gt;- Y : Coordonnée Y du centre du cylindre&lt;br&gt;- Z : Coordonnée Z du centre du cylindre&lt;br&gt;- Zmin : Z minium de la tranche à conserver dans le cylindre&lt;br&gt;- Zmax : Z maximum de la tranche à conserver dans le cylindre&lt;br&gt;- Rayon : Rayon (XY) du cylindre à conserver&lt;br&gt;Le champs ID_Plot est mis en correspondance avec le nom de la placette (cf. choix des résultats d&apos;entrée).&lt;br&gt;Si le champs ID_Plot est absent (NODATA), tous les cylindres sont conservés.&lt;br&gt;Il est également possible pour chaque cylindre de réaliser une translation de son centre (X,Y,Z) aux coordonnées (0,0,0).</source>
        <translation>This step extract cylinders in point scenes&lt;br&gt;A parameter ASCII file defines cylinders for each plot (one line per cylinder).&lt;br&gt;This file must contains following fields:&lt;br&gt;- ID_Plot: plot id&lt;br&gt;- ID_Tree: cylinder id&lt;br&gt;- X: X coordinate of plot center&lt;br&gt;- Y: Y  coordinate of plot center&lt;br&gt;- Z: Z coordinate of cylinder center&lt;br&gt;- Zmin: Z minimum of cylinder&lt;br&gt;- Zmax: Z maximum of cylinder&lt;br&gt;- Radius: Radius (XY) of the cylinder&lt;br&gt;The ID field is used as correspondance with plot name (cf. imput result selection).&lt;br&gt;If ID_Plot field is missing (NODATA), all cylinders are kept.&lt;br&gt;It is also possible for each cylinder to apply a translation of its center (X,Y,Z) towards (0,0,0) coordinates.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="116"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="120"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="121"/>
        <source>Item avec nom de fichier</source>
        <translation>Item containing file name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="122"/>
        <source>Nom de fichier</source>
        <translation>File name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="132"/>
        <source>Extracted Scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="133"/>
        <source>PlotID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="142"/>
        <source>Fichier des cylindres par placette</source>
        <translation>File with cylinders per plots</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="142"/>
        <source>Fichier ASCII (*.txt ; *.asc)</source>
        <translation>ASCII file (*.txt ; *.asc)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="143"/>
        <source>Appliquer translation</source>
        <translation>Apply translation</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="192"/>
        <source>Champ IDplot non défini (non bloquant)</source>
        <translation>Champ IDplot not défined (not mandatory)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="193"/>
        <source>Champ IDtree non défini</source>
        <translation>IDtree field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="194"/>
        <source>Champ X non défini</source>
        <translation>X field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="195"/>
        <source>Champ Y non défini</source>
        <translation>Y field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="196"/>
        <source>Champ Z non défini</source>
        <translation>Z field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="197"/>
        <source>Champ Zmin non défini</source>
        <translation>Zmin field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="198"/>
        <source>Champ Zmax non défini</source>
        <translation>Zmax field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="199"/>
        <source>Champ Rayon non défini</source>
        <translation>Radius field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpointsinverticalcylinders.cpp" line="250"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractPositionsFromDensity</name>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="60"/>
        <source>Créer des positions 2D à partir des densités des points</source>
        <translation>Create 2D position from points density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="66"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="87"/>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="90"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="89"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="99"/>
        <source>Positions 2D (grp)</source>
        <translation>2D Positions (grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="100"/>
        <source>Positions 2D</source>
        <translation>2D Positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="101"/>
        <source>Densité</source>
        <translation>Density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="102"/>
        <source>DensitéMax</source>
        <translation>MaxDensity </translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="103"/>
        <source>Grille de densité</source>
        <translation>Density grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="112"/>
        <source>Résolution du raster densité</source>
        <translation>Density raster resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="116"/>
        <source>Seuil en valeur relative</source>
        <translation>Threshold (relative value)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="117"/>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="119"/>
        <source>Seuil de densité (inclus)</source>
        <translation>Density threshold (included)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepextractpositionsfromdensity.cpp" line="118"/>
        <source>Seuil en valeur absolue</source>
        <translation>Threshold (absolute value)</translation>
    </message>
</context>
<context>
    <name>ONF_StepExtractSoil03</name>
    <message>
        <source>Séparation sol / végétation -&gt; MNT</source>
        <translation type="vanished">Soil / vegetation segmentation -&gt; DTM</translation>
    </message>
    <message>
        <source>Cette étape permet de séparer les points Sol et Végétation, et de générer :&lt;ul&gt;&lt;li&gt;Le Modèle Numérique de Terrain (MNT)&lt;/li&gt;&lt;li&gt;Le Modèle Numérique de Surface (MNS)&lt;/li&gt;&lt;li&gt;Le Modèle Numérique de Hauteur (MNH)&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;Etapes de l&apos;extraction du sol et de la création du MNT :&lt;ul&gt;&lt;li&gt;Une grille Zmin est créée à la &lt;b&gt;résolution&lt;/b&gt; spécifiée&lt;/li&gt;&lt;li&gt;La densité de points situés entre Zmin et (Zmin + &lt;b&gt;épaisseur du sol&lt;/b&gt;) est calculée pour chaque case&lt;/li&gt;&lt;li&gt;La valeur NA est affectée à toute case dont la densité est inférieure à la &lt;b&gt;densité minimum&lt;/b&gt;&lt;/li&gt;&lt;li&gt;Un test de cohérence des Zmin restants est réalisé pour chaque case sur le &lt;b&gt;voisinage&lt;/b&gt; spécifié (nombre de cases). La valeur NA est affectée aux cases incohérentes&lt;/li&gt;&lt;li&gt;Si l&apos; &lt;b&gt;interpolation&lt;/b&gt; est activée, les valeur NA sont remplacées par une moyenne des voisins natuels dans la grille (triangulation de Delaunay en 2D, fournie en sortie)&lt;/li&gt;&lt;li&gt;Si le &lt;b&gt;lissage&lt;/b&gt; est activé, chaque cellule est tranformée en la moyenne du k-voisinnage, avec k = &lt;b&gt;voisinnage de lissage&lt;/b&gt;&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;Le MNT est la grille résultante (interpolée et/ou lissée selon les options cochées).&lt;br&gt;Le MNS est simplement une grille Zmax de la même &lt;b&gt;résolution&lt;/b&gt;.&lt;br&gt;Le MNH est la soutraction MNS-MNT.&lt;br&gt;Les points Sol sont tous les points dont Z &amp;lt; (hauteur MNT + &lt;b&gt;épaisseur du sol&lt;/b&gt;).&lt;br&gt;Les points Végétation sont tous les points non classés sol.</source>
        <translation type="vanished">This step allows the separation of Soil and Vegetation points and generate:
&lt;ul&gt;
&lt;li&gt;Digital Terrain Model (DTM)&lt;/li&gt;
&lt;li&gt;Digital Surface Model (DSM)&lt;/li&gt;
&lt;li&gt;Digital Height Model (DHM)&lt;/li&gt;
&lt;/ul&gt;
&lt;br&gt;Steps for soil extraction and DTM creation:
&lt;ul&gt;
&lt;li&gt;Zmin grid is created with the specified &lt;b&gt;resolution&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;the density of points between Zmin and (Zmin + &lt;b&gt;soil depth&lt;/b&gt;) is calculated for each cell&lt;/li&gt;
&lt;li&gt;the value NA is assigned to each cell whose density is less than the &lt;b&gt;minimum density&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;a consistency check of remaining Zmin is performed for each cell on the specified &lt;b&gt;neighborhood&lt;/b&gt; (number of cells). The NA value is assigned to inconsistent cells&lt;/li&gt;
&lt;li&gt;if &lt;b&gt;interpolation&lt;/b&gt; is enabled, the NA value is replaced by an average of natural neighbors in the grid (2D Delaunay triangulation, given as output)&lt;/li&gt;
&lt;li&gt;if &lt;b&gt;smoothing&lt;/b&gt; is enabled, each cellvalue  is remplaced by the average k-Neighborhood, k = &lt;b&gt;smoothing neighborhood&lt;/b&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;br&gt;
the DTM is the resulting grid (interpolated and / or smoothed depending on the checked options).&lt;br&gt;
DSM is simply a grid containing Zmax for the same &lt;b&gt;resolution&lt;/b&gt;.&lt;br&gt;
DHM is the soutraction: DSM-DTM&lt;br&gt;
Soil points are all points for which Z &amp;lt; (DTM height + &lt;b&gt;soil depth&lt;/b&gt;).&lt;br&gt;
Vegetation points are all points not classified as soil.</translation>
    </message>
    <message>
        <source>Scène(s)</source>
        <translation type="vanished">Scene(s)</translation>
    </message>
    <message>
        <source>Scène</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <source>Résolution de la grille :</source>
        <translation type="vanished">Grid resolution:</translation>
    </message>
    <message>
        <source>Epaisseur du sol :</source>
        <translation type="vanished">Soil thickness:</translation>
    </message>
    <message>
        <source>Densité minimum :</source>
        <translation type="vanished">Minimum density:</translation>
    </message>
    <message>
        <source>Voisinage (points isolés) :</source>
        <translation type="vanished">Neighbourhood (isolated points):</translation>
    </message>
    <message>
        <source>Interpolation</source>
        <translation type="vanished">Interpolation</translation>
    </message>
    <message>
        <source>Lissage</source>
        <translation type="vanished">Smoothing</translation>
    </message>
    <message>
        <source>Voisinage de lissage :</source>
        <translation type="vanished">Smoothing neighbourhood:</translation>
    </message>
    <message>
        <source>Points végétation</source>
        <translation type="vanished">Vegetation points</translation>
    </message>
    <message>
        <source>Scène végétation</source>
        <translation type="vanished">Vegetation scene</translation>
    </message>
    <message>
        <source>Points sol</source>
        <translation type="vanished">Soil points</translation>
    </message>
    <message>
        <source>Scène sol</source>
        <translation type="vanished">Soil scene</translation>
    </message>
    <message>
        <source>Triangulation 2D</source>
        <translation type="vanished">2D triangulation</translation>
    </message>
    <message>
        <source>Modèle Numérique de terrain</source>
        <translation type="vanished">Digital Terrain Model</translation>
    </message>
    <message>
        <source>MNT (Raster)</source>
        <translation type="vanished">DTM (raster)</translation>
    </message>
    <message>
        <source>Modèle Numérique de Surface</source>
        <translation type="vanished">Digital Surface Model</translation>
    </message>
    <message>
        <source>MNS (Raster)</source>
        <translation type="vanished">DSM (raster)</translation>
    </message>
    <message>
        <source>Modèle Numérique de Hauteur</source>
        <translation type="vanished">Digital Height Model</translation>
    </message>
    <message>
        <source>MNH (Raster)</source>
        <translation type="vanished">DHM (raster)</translation>
    </message>
    <message>
        <source>Densité de points sol</source>
        <translation type="vanished">Soil points density</translation>
    </message>
    <message>
        <source>Densité pts sol (Raster)</source>
        <translation type="vanished">Soil point density (raster)</translation>
    </message>
    <message>
        <source>La scène d&apos;entrée %2 comporte %1 points.</source>
        <translation type="vanished">The %2 input scene contains %1 points.</translation>
    </message>
    <message>
        <source>Grille Zmin et MNS créés</source>
        <translation type="vanished">Zmin grid and DSM created</translation>
    </message>
    <message>
        <source>Filtrage sur la densité terminé</source>
        <translation type="vanished">Filtering on density achieved</translation>
    </message>
    <message>
        <source>Test de cohérence de voisinnage terminé</source>
        <translation type="vanished">Neighbourhood consistency test achieved</translation>
    </message>
    <message>
        <source>Triangulation des cases conservées terminée</source>
        <translation type="vanished">Kept cells triangulation achieved</translation>
    </message>
    <message>
        <source>Interpolation du MNT terminée</source>
        <translation type="vanished">DTM interpolation achieved</translation>
    </message>
    <message>
        <source>Lissage du MNT terminé</source>
        <translation type="vanished">DTM smoothing achieved</translation>
    </message>
    <message>
        <source>Création du MNH terminée</source>
        <translation type="vanished">DHM creation achieved</translation>
    </message>
    <message>
        <source>Scène %3 : Création des scènes sol (%1 points) et végétation (%2 points) terminée</source>
        <translation type="vanished">Scene %3: soil (%1 points) and vegetation (%2 points) scenes creation achieved</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterClustersBySize</name>
    <message>
        <source>Filtrage de clusters / nb. de points</source>
        <translation type="vanished">Filtering of clusters by points number</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="51"/>
        <source>Filtrer les Clusters par nombre de points</source>
        <translation>Filter clusters by points number</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="56"/>
        <source>Cette étape filtre des clusters (tout item contenant des points).&lt;br&gt;Tout cluster ayant un nombre de points strictement inférieur au &lt;b&gt;nombre de points minimum&lt;/b&gt; spécifié est éliminé.</source>
        <translation>This step filters clusters (any item containing points).&lt;br&gt;
All cluster with a number of points strictly below the specified &lt;b&gt;minimum number of points&lt;/b&gt; are removed.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="70"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="73"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="80"/>
        <source>Nombre de points minimum dans un cluster</source>
        <translation>Minimum points number in one cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="123"/>
        <source>Nombre de clusters avant filtrage : %1</source>
        <translation>Number of clusters before filtering: %1</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterclustersbysize.cpp" line="124"/>
        <source>Nombre de clusters éliminés : %1</source>
        <translation>Number of droped clusters: %1</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterElementsByXYArea</name>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="54"/>
        <source>Garder les Items contenus dans une emprise</source>
        <translation>Keep items containes in footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="60"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="81"/>
        <source>Items à filtrer</source>
        <translation>Item to filter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="83"/>
        <source>Groupe Emprise</source>
        <translation>Footprint group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="84"/>
        <source>Emprise</source>
        <translation>Footprint</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="85"/>
        <source>Groupe à filtrer</source>
        <translation>Group to filter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="86"/>
        <source>XY Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="87"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterelementsbyxyarea.cpp" line="88"/>
        <source>Y</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterGridByCloud</name>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="68"/>
        <source>Filter une grille 3D en fonction de scènes</source>
        <translation>Filter a 3D grid from scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="85"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="88"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="92"/>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="96"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="105"/>
        <source>Grille filtrée</source>
        <translation>Filtered gird</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbycloud.cpp" line="113"/>
        <source>Valeur pour les cases hors scène</source>
        <translation>Value for cells not included in scene</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterGridByValueAndNeighborhood</name>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="65"/>
        <source>Seuiller une grille 3D par valeur et voisinnage</source>
        <translation>Thresholding a 3D grid by value and neighborhood</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="82"/>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="86"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="95"/>
        <source>Grille filtrée</source>
        <translation>Filtered gird</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="102"/>
        <source>Seuil (minimum inclus)</source>
        <translation>Threshold (minimum included)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="103"/>
        <source>Nombre minimal de cellules voisines &gt;= seuil</source>
        <translation>Minimum number of neighboring cells &gt;= threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="104"/>
        <source>Voisinage</source>
        <translation>Neighbourhood</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergridbyvalueandneighborhood.cpp" line="104"/>
        <source>cellules</source>
        <translation>cells</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterGroupsByGroupsNumber</name>
    <message>
        <source>Filtrage de groupes niv.1 / nb. de groupes niv.2</source>
        <translation type="vanished">Filtering lvl 1 groups / number of lvl 2 groups</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="51"/>
        <source>2- Filter les Billons par nombre de Clusters</source>
        <translation>2- Filter logs by number of clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="56"/>
        <source>Cette étape très générique travaille sur deux niveau de groupes.&lt;br&gt;Tout groupe du niveau 1 contenant &lt;b&gt;nombre de groupes&lt;/b&gt; de niveau 2 insuffisant est éliminé.&lt;br&gt;Un usage de cette étape est d&apos;éliminer des groupes de niveau 1 ne contenant pas assez de groupes de niveau 2.&lt;br&gt;Comme par exemple après une étape ONF_StepDetectSection.</source>
        <translation>This very generic step is working on two group levels.&lt;br&gt;
Any group of level 1 containing an to lower &lt;b&gt;number of groups&lt;/b&gt; of level 2 is eliminated.&lt;br&gt;
So this step is eliminate groups of level 1 not containing enough groups of level 2, for example after a ONF_StepDetectSection step.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="73"/>
        <source>Groupes niv.1 (à filter)</source>
        <translation>Lvl 1 groups (to filter)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="75"/>
        <source>Groupe niv.1 (à filter)</source>
        <translation>Lvl 1 group (to filter)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="76"/>
        <source>Groupe niv.2 (à dénombrer)</source>
        <translation>Lvl 2 group (to count)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltergroupsbygroupsnumber.cpp" line="83"/>
        <source>Nombre de groupes minimum de niveau 2 dans un groupe de niveau 1</source>
        <translation>Minimum number of lvl 2 groups in a lvl 1 group</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterItemsByPosition</name>
    <message>
        <location filename="../step/onf_stepfilteritemsbyposition.cpp" line="53"/>
        <source>Garder les Items proches d&apos;une coordonnée</source>
        <translation>Kepp items near a specified coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilteritemsbyposition.cpp" line="59"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilteritemsbyposition.cpp" line="80"/>
        <source>Items à filtrer</source>
        <translation>Item to filter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilteritemsbyposition.cpp" line="82"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilteritemsbyposition.cpp" line="83"/>
        <source>Item</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterMaximaByClusterPositions</name>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="56"/>
        <source>Filtrer les maxima à partir de clusters de référence</source>
        <translation>Filter maximum from reference clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="62"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="83"/>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="87"/>
        <source>Maxima</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="85"/>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="89"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="86"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="90"/>
        <source>Cluster Position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="99"/>
        <source>Maxima filtrés</source>
        <translation>Filtered maximum</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="101"/>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="102"/>
        <source>MaximaID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="103"/>
        <source>PointClusterID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="111"/>
        <source>Rayons de recherche</source>
        <translation>Search radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyclusterpositions.cpp" line="112"/>
        <source>Rauons d&apos;exclusion</source>
        <translation>Exclusion radius</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterMaximaByNeighbourhood</name>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="40"/>
        <source>Filtrer les maxima par voisinage</source>
        <translation>Filter maxima by neighbourhood</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="46"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="67"/>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="71"/>
        <source>Maxima</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="69"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="70"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="73"/>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="76"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="75"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="86"/>
        <source>Maxima filtrés</source>
        <translation>Filtered maximum</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="94"/>
        <source>DeltaH maximum dans un houppier</source>
        <translation>DeltaH maximum in a crown</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="95"/>
        <source>Rayon de houppier minimal</source>
        <translation>Minimum crown radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood.cpp" line="96"/>
        <source>Rayon de houppier maximal</source>
        <translation>Maximum crown radius</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterMaximaByNeighbourhood02</name>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="41"/>
        <source>Filtrer les maxima par voisinage (v2)</source>
        <translation>Filter maxima by neighbourhood (v2)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="47"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="68"/>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="72"/>
        <source>Maxima</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="70"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="71"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="74"/>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="77"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="76"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="87"/>
        <source>Maxima filtrés</source>
        <translation>Filtered maximum</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="95"/>
        <source>DeltaH maximum dans un houppier</source>
        <translation>DeltaH maximum in a crown</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="96"/>
        <source>Rayon de houppier minimal</source>
        <translation>Minimum crown radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfiltermaximabyneighbourhood02.cpp" line="97"/>
        <source>Rayon de houppier maximal</source>
        <translation>Maximum crown radius</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterPointsByBoolGrid</name>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="63"/>
        <source>Filtrer les points par une Grille Booléenne</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="69"/>
        <source>Cette étape teste pour chaque point des scènes d&apos;entrée s&apos;il est contenu dans une case &quot;vraie&quot; de la grille booléenne choisie. Si oui, le point est conservé. Sinon, il n&apos;est pas conservé.&lt;br&gt;Plusieures scènes peuvent être traitées avec la même étape.&lt;br&gt;Chaque scène filtrée est ajoutée au groupe contenant la grille d&apos;entrée.Si le résultat d&apos;entrée contient plusieurs grilles, une scène est produite pour chacune (sur la base du cumul de toutes les scènes d&apos;entrée)</source>
        <translation>This step keeps only points contained in the cells of input boolean grid with its value equals to &quot;true&quot;. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="87"/>
        <source>Scènes à filtrer</source>
        <translation>Scene to filter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="91"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="94"/>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="98"/>
        <source>Grille(s) de filtrage</source>
        <translation>Filtering grid (bool)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="107"/>
        <source>Scène filtrée</source>
        <translation>Filtered scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="148"/>
        <source>Grille %1, Scène %2:</source>
        <translation>Grid %1, Scene %2:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="153"/>
        <source>La scène %1 points...</source>
        <translation>The scene contient %1 points...</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterpointsbyboolgrid.cpp" line="175"/>
        <source>...%1 points ont été conservés</source>
        <translation>...%1 points have been kept</translation>
    </message>
</context>
<context>
    <name>ONF_StepFilterWires</name>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="45"/>
        <source>Filtrage des fils / cables</source>
        <translation>Filter wires / cables</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="51"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="75"/>
        <source>Point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="86"/>
        <source>Slope</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="87"/>
        <source>RMSE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="88"/>
        <source>Scène conservée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="89"/>
        <source>fils / cables</source>
        <translation>Wires / Cables</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="100"/>
        <source>Resolution d&apos;optimisation</source>
        <translation>Optimization resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="103"/>
        <source>Seuil de pente</source>
        <translation>Slope threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="104"/>
        <source>Seuil Maxi pour la pente ?</source>
        <translation>Slope threshold is a maximum ?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="107"/>
        <source>Seuil de RMSE</source>
        <translation>RMSE threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfilterwires.cpp" line="108"/>
        <source>Seuil Maxi pour la RMSE ?</source>
        <translation>RMSE threshold is a maximum ?</translation>
    </message>
</context>
<context>
    <name>ONF_StepFitAndFilterCylindersInSections</name>
    <message>
        <source>Ajustement/Filtrage des cylindres / billon</source>
        <translation type="vanished">Fitting / Filtering of cylinders by log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="84"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="106"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="103"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="79"/>
        <source>6- Ajuster des Cylindres par Clusters/Billons</source>
        <translation>6- Ajust cylinders by clusters/Logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="105"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="107"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="108"/>
        <source>Point de référence</source>
        <translation>Reference point</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="115"/>
        <source>Rayon minimum  :</source>
        <translation>Minimum radius:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="116"/>
        <source>Rayon maximum  :</source>
        <translation>Maximum radius:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="117"/>
        <source>Filtrer les cylindres sur la RMSE</source>
        <translation>Filter the cylinders on the absolute RMSE</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="119"/>
        <source>Filtrer les cylindres sur la RMSE relative</source>
        <translation>Filter the cylinders on the relative RMSE</translation>
    </message>
    <message>
        <source>Filtrer les cylindres sur l&apos;erreur absolue</source>
        <translation type="vanished">Filter cylinders on absolute error</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="118"/>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="123"/>
        <source>Erreur maximum :</source>
        <translation>Maximum error:</translation>
    </message>
    <message>
        <source>Filtrer les cylindres sur l&apos;erreur relative</source>
        <translation type="vanished">Filter cylinders on relative error</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="120"/>
        <source>Erreur maximum relative au diamètre :</source>
        <translation>Maximum relative (to diameter) error:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="122"/>
        <source>Filtrer les directions sur la RMSE</source>
        <translation>Filter directions on the RMSE</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="125"/>
        <source>Filtrer les cylindres sur leur verticalité</source>
        <translation>Filter cylinders on verticallity</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="126"/>
        <source>Angle maximal à la verticale (depuis de zénith) :</source>
        <translation>Maximum angle from vertical (zenithal angle):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitandfiltercylindersinsections.cpp" line="134"/>
        <source>Cylindre</source>
        <translation>Cylinder</translation>
    </message>
</context>
<context>
    <name>ONF_StepFitCirclesAndFilter</name>
    <message>
        <source>Ajustement/Filtrage des cercles / cluster</source>
        <translation type="vanished">Fitting / Filtering of circles by log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="52"/>
        <source>Ajuster/Filtrer un Cercle horizontal par Cluster</source>
        <translation>Ajust/Filter one horizontal Circle for each Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="57"/>
        <source>Cette étape ajoute un cercle dans chaque cluster d&apos;entrée.&lt;br&gt;Les cercles sont ajustés par moindres carrés sur les groupes de points.&lt;br&gt;Les paramètres de l&apos;étape permettent d&apos;activer optionnellement un  &lt;b&gt;filtrage&lt;/b&gt; de cercles.&lt;br&gt;Les criètres de filtrages sont le &lt;b&gt;rayon minimum&lt;/b&gt;, le &lt;b&gt;rayon maximum&lt;/b&gt; et l&apos; &lt;b&gt;erreur d&apos;ajustement du cercle&lt;/b&gt; maximale autorisée.</source>
        <translation>This step adds a circle in each input cluster.&lt;br&gt;
Circles are adjusted by least squares on clusters of points.&lt;br&gt;
Parameters for this stepallow to optionally activate the &lt;b&gt;filtering&lt;/b &gt; of circles.&lt;br&gt;
Filtering criterions are &lt;b&gt;minimum radius&lt;/b&gt;, &lt;b&gt;maximum radius&lt;/b&gt; and maximum allowed &lt;b&gt;adjustment error of the circle&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="73"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="75"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="76"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="83"/>
        <source>Filtrer les cercles sur les critres suivants</source>
        <translation>Filter circles on  following criterion</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="84"/>
        <source>Rayon minimum  :</source>
        <translation>Minimum radius:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="85"/>
        <source>Rayon maximum  :</source>
        <translation>Maximum radius:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="86"/>
        <source>Erreur maximum :</source>
        <translation>Maximum error:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcirclesandfilter.cpp" line="95"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
</context>
<context>
    <name>ONF_StepFitCylinderOnCluster</name>
    <message>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="50"/>
        <source>Ajuster un Cylindre par cluster (en 3D)</source>
        <translation>Ajust one cylinder for each cluster (3D)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="56"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="77"/>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="80"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="79"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepfitcylinderoncluster.cpp" line="90"/>
        <source>Cylindre</source>
        <translation>Cylinder</translation>
    </message>
</context>
<context>
    <name>ONF_StepHorizontalClustering04</name>
    <message>
        <source>Clustering / tranches horizontales</source>
        <translation type="vanished">Clustering by horizontal slices</translation>
    </message>
    <message>
        <source>Cette étape vise à constituer de petits groupes de points aggrégés. L&apos;idée est d&apos;obtenir, dans le cas de troncs d&apos;arbres des arcs de cercle peu épais.&lt;br&gt;Pour ce faire, l&apos;étape fonctionne en deux étapes :&lt;ul&gt;&lt;li&gt; La scène est découpée en tranches horizontales (Layers) de l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie&lt;/li&gt;&lt;li&gt; Dans chacune des tranches, les points sont aggrégés en clusters en fonction de leur espacement en (x,y)&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;La &lt;b&gt;distance maximale séparant deux points d&apos;un même groupe&lt;/b&gt; est spécifiée en paramètre.</source>
        <translation type="vanished">This step create points clusters. In the case of tree trunks, the idea is to obtain thin circles arcs&lt;br&gt;
To do this, the step works in two phases:
&lt;Ul&gt;
&lt;li&gt;The scene is sliced in horizontal layers of defined &lt;b&gt;thickness&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;In each slice, the points are aggregated into clusters according to their spacing in (x, y)&lt;/li&gt;
&lt;/ul&gt;
The &lt;b&gt;maximum distance between two points within a group&lt;/b&gt; is specified as a parameter.</translation>
    </message>
    <message>
        <source>Scène(s)</source>
        <translation type="vanished">Scene(s)</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Scène à clusteriser</source>
        <translation type="vanished">Scene to clusterize</translation>
    </message>
    <message>
        <source>Distance maximum pour intégrer un point à un groupe :</source>
        <translation type="vanished">Maximum distance to integrate a point in a group:</translation>
    </message>
    <message>
        <source>Epaisseur des tranches horizontales :</source>
        <translation type="vanished">Thickness of horizontal slices:</translation>
    </message>
    <message>
        <source>Scène clusterisée</source>
        <translation type="vanished">Clusterized scene</translation>
    </message>
    <message>
        <source>Niveau Z (Grp)</source>
        <translation type="vanished">Z-Level (Grp)</translation>
    </message>
    <message>
        <source>Cluster (Grp))</source>
        <translation type="vanished">Cluster (Grp)</translation>
    </message>
    <message>
        <source>Points</source>
        <translation type="vanished">Points</translation>
    </message>
    <message>
        <source>La scène à clusteriser comporte %1 points.</source>
        <translation type="vanished">The scene to clusterize contains %1 points.</translation>
    </message>
    <message>
        <source>L&apos;étape a généré %1 couches horizontales.</source>
        <translation type="vanished">The step has generated %1 horizontal slices.</translation>
    </message>
</context>
<context>
    <name>ONF_StepHorizontalClustering05</name>
    <message>
        <source>Scène(s)</source>
        <translation type="obsolete">Scene(s)</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="obsolete">Group</translation>
    </message>
    <message>
        <source>Scène à clusteriser</source>
        <translation type="obsolete">Scene to clusterize</translation>
    </message>
    <message>
        <source>Distance maximum pour intégrer un point à un groupe :</source>
        <translation type="obsolete">Maximum distance to integrate a point in a group:</translation>
    </message>
    <message>
        <source>Epaisseur des tranches horizontales :</source>
        <translation type="obsolete">Thickness of horizontal slices:</translation>
    </message>
    <message>
        <source>Niveau Z (Grp)</source>
        <translation type="obsolete">Z-Level (Grp)</translation>
    </message>
    <message>
        <source>Cluster (Grp)</source>
        <translation type="obsolete">Cluster (Grp)</translation>
    </message>
    <message>
        <source>Points</source>
        <translation type="obsolete">Points</translation>
    </message>
    <message>
        <source>La scène à clusteriser comporte %1 points.</source>
        <translation type="obsolete">The scene to clusterize contains %1 points.</translation>
    </message>
    <message>
        <source>L&apos;étape a généré %1 couches horizontales.</source>
        <translation type="obsolete">The step has generated %1 horizontal slices.</translation>
    </message>
</context>
<context>
    <name>ONF_StepHorizontalClustering3D</name>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="69"/>
        <source>Clustering 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="74"/>
        <source>Cette étape vise à constituer de petits groupes de points aggrégés. L&apos;idée est d&apos;obtenir, dans le cas de troncs d&apos;arbres des arcs de cercle peu épais.&lt;br&gt;Pour ce faire, l&apos;étape fonctionne en deux étapes :&lt;ul&gt;&lt;li&gt; La scène est découpée en tranches horizontales (Layers) de l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie&lt;/li&gt;&lt;li&gt; Dans chacune des tranches, les points sont aggrégés en clusters en fonction de leur espacement en (x,y)&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;La &lt;b&gt;distance maximale séparant deux points d&apos;un même groupe&lt;/b&gt; est spécifiée en paramètre.&lt;br&gt;Cette version 05 de l&apos;étape permet de traiter séparément chaque scène d&apos;entrée.&lt;br&gt;De plus dans cette version, le résultat d&apos;entrée est en copie.</source>
        <translation>No detailled description for this step</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="96"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="98"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="99"/>
        <source>Scène à clusteriser</source>
        <translation>Scene to clusterize</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="106"/>
        <source>Distance maximum pour intégrer un point à un groupe :</source>
        <translation>Maximum distance to integrate a point in a group:</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="107"/>
        <source>Epaisseur des tranches horizontales :</source>
        <translation>Thickness of horizontal slices:</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="115"/>
        <source>Niveau Z (Grp)</source>
        <translation>Z-Level (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="116"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="117"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="158"/>
        <source>La scène à clusteriser comporte %1 points.</source>
        <translation>The scene to clusterize contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stephorizontalclustering3d.cpp" line="240"/>
        <source>L&apos;étape a généré %1 couches horizontales.</source>
        <translation>The step has generated %1 horizontal slices.</translation>
    </message>
</context>
<context>
    <name>ONF_StepImportSegmaFilesForMatching</name>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="56"/>
        <source>Fichiers SEGMA : un de ref, un à transformer</source>
        <translation>SEGMA files: one reference file, one file to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="62"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="90"/>
        <source>Positions de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="91"/>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="97"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="92"/>
        <source>Position de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="93"/>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="99"/>
        <source>Valeur</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="94"/>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="100"/>
        <source>IDsegma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="96"/>
        <source>Positions à transformer</source>
        <translation>Positions to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="98"/>
        <source>Position à transformer</source>
        <translation>Position to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="109"/>
        <source>Fichier SEGMA des positions de référence</source>
        <translation>SEGMA file for reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="110"/>
        <source>Fichier SEGMA des positions à transformer</source>
        <translation>SEGMA file for positions to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="161"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepimportsegmafilesformatching.cpp" line="209"/>
        <source>Ligne %1 du fichier TRANS non valide</source>
        <translation>Line %1 of TRANS file not valid</translation>
    </message>
</context>
<context>
    <name>ONF_StepKeepIntersectingItems</name>
    <message>
        <location filename="../step/onf_stepkeepintersectingitems.cpp" line="30"/>
        <source>Conserve les items intersectant les surfaces de référence</source>
        <translation>Keeps items intersecting reference surfaces</translation>
    </message>
    <message>
        <location filename="../step/onf_stepkeepintersectingitems.cpp" line="48"/>
        <source>Items to filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepkeepintersectingitems.cpp" line="51"/>
        <source>Item to filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepkeepintersectingitems.cpp" line="53"/>
        <location filename="../step/onf_stepkeepintersectingitems.cpp" line="55"/>
        <source>Filtering items</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepLoadDataFromItemPosition</name>
    <message>
        <source>Fichiers d&apos;un DataSource intersectant l&apos;emprise d&apos;Items</source>
        <translation type="vanished">DataSource files intersecting items XY projected area</translation>
    </message>
    <message>
        <source>Source de données géographique</source>
        <translation type="vanished">Geographical data source</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Données chargées</source>
        <translation type="vanished">Loaded data</translation>
    </message>
</context>
<context>
    <name>ONF_StepLoadPlotAreas</name>
    <message>
        <source>Fichier ASCII contenant l&apos;emprises de placettes circulaires</source>
        <translation type="vanished">ASCII file containing XY areas of circular plots</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="71"/>
        <source>Fichier ASCII contenant le centre de placettes circulaires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="77"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="98"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="100"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="101"/>
        <source>Entête de fichier</source>
        <translation>File header</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="110"/>
        <source>Emprise</source>
        <translation>XY area</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="118"/>
        <source>Rayon de placette (si pas de colonne rayon)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="159"/>
        <source>Champ ID_Plot non défini</source>
        <translation>ID_Plot field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="160"/>
        <source>Champ X non défini</source>
        <translation>X field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="161"/>
        <source>Champ Y non défini</source>
        <translation>Y field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="162"/>
        <source>Champ R non défini</source>
        <translation>R field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadplotareas.cpp" line="218"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
</context>
<context>
    <name>ONF_StepLoadPositionsForMatching</name>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="78"/>
        <source>Fichiers de positions (2) pour mise en correspondance</source>
        <translation>Positions files (2 files) to be linked</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="84"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="114"/>
        <source>Positions de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="115"/>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="122"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="116"/>
        <source>Position de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="117"/>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="124"/>
        <source>Valeur</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="118"/>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="125"/>
        <source>IDtree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="119"/>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="126"/>
        <source>IDplot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="121"/>
        <source>Positions à transformer</source>
        <translation>Positions to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="123"/>
        <source>Position à transformer</source>
        <translation>Position to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="195"/>
        <source>Placette en cours de traitement : %1</source>
        <translation>Processing plot: %1</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="254"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadpositionsformatching.cpp" line="323"/>
        <source>Ligne %1 du fichier TRANS non valide</source>
        <translation>Line %1 of TRANS file not valid</translation>
    </message>
</context>
<context>
    <name>ONF_StepLoadTreeMap</name>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="85"/>
        <source>Placette d&apos;inventaire forestier (Tree Map)</source>
        <translation>Forest inventory plot (Tree Map)</translation>
    </message>
    <message>
        <source>Charge des données d&apos;inventaire forestier depuis un fichier ASCII. &lt;br&gt;L&apos;import est configurable, le fichier devant contenir les champs suivants :&lt;br&gt;- IDplot : Identifiant placette&lt;br&gt;- IDtree : Identifiant arbre&lt;br&gt;- X      : Coordonnée X de l&apos;arbre&lt;br&gt;- Y      : Coordonnée Y de l&apos;arbre&lt;br&gt;- DBH    : Diamètre à 1.30 m de l&apos;arbre&lt;br&gt;&lt;br&gt;Une fois le format de fichier paramétré, l&apos;utilisateur indique quelle placette doit être chargée.&lt;br&gt;Seules les données de la placette choisie seront chargées.</source>
        <translation type="vanished">Load forest inventory data from ASCII file.&lt;br&gt;The import is configurable. The file must contain following fields:&lt;br&gt;- IDplot : plot id&lt;br&gt;- IDtree : tree id&lt;br&gt;- X      : X coordinate of tree&lt;br&gt;- Y      : Y coordinate of tree&lt;br&gt;- DBH    : Diameter at 1.30 m of tree&lt;br&gt;&lt;br&gt;Once the file format is parametrized, the user has to specify with plot should be loaded.&lt;br&gt;Only data from selected plot are loaded.</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="158"/>
        <source>Positions de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Position de référence</source>
        <translation type="vanished">Reference position</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="91"/>
        <source>Charge des données d&apos;inventaire forestier depuis un fichier ASCII. &lt;br&gt;L&apos;import est configurable, le fichier devant contenir les champs suivants :&lt;br&gt;- IDplot : Identifiant placette&lt;br&gt;- IDtree : Identifiant arbre&lt;br&gt;- X      : Coordonnée X de l&apos;arbre&lt;br&gt;- Y      : Coordonnée Y de l&apos;arbre&lt;br&gt;- DBH    : Diamètre à 1.30 m de l&apos;arbre&lt;br&gt;- H      : Hauteur de l&apos;arbre&lt;br&gt;- Species: Espèce de l&apos;arbre&lt;br&gt;&lt;br&gt;Une fois le format de fichier paramétré, l&apos;utilisateur indique quelle placette doit être chargée.&lt;br&gt;Seules les données de la placette choisie seront chargées.</source>
        <translation>Load forest inventory data from an ASCII file.&lt;br&gt;The import is configurable, the file must contain the following fields:&lt;br&gt;- IDplot: Plot identifier&lt;br&gt;- IDtree: Tree identifier&lt;br&gt;- X: X coordinate of the tree&lt;br&gt;- Y: Y coordinate of the tree&lt;br&gt;- DBH: Diameter at 1.30 m of the tree&lt;br&gt;- H: Height of the tree&lt;br&gt;- Species: Tree species&lt;br&gt;&lt;br&gt;Once the file format parameterized, the user indicates which plot is to be loaded.&lt;br&gt;Only data from the selected plot will be loaded.</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="136"/>
        <source>IDplacette à partir du nom de tour (boucles)</source>
        <translation>IDplot obtained from loop turn name (loops only)</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="137"/>
        <source>Sélection manuelle de l&apos;IDplacette</source>
        <translation>Manual selection of IDplot</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="146"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="148"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="159"/>
        <source>Plot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="160"/>
        <source>Tree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="161"/>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="162"/>
        <source>DBH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="163"/>
        <source>Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="164"/>
        <source>IDtree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="165"/>
        <source>IDplot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="166"/>
        <source>Species</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="167"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="179"/>
        <source>Choix de la placette</source>
        <translation>Plot choice</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="250"/>
        <source>Placette en cours de traitement : %1</source>
        <translation>Processing plot: %1</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="269"/>
        <source>Champ IDtree non défini</source>
        <translation>IDtree field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="270"/>
        <source>Champ X non défini</source>
        <translation>X field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="271"/>
        <source>Champ Y non défini</source>
        <translation>Y field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="272"/>
        <source>Champ DBH non défini</source>
        <translation>DBH field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="273"/>
        <source>Champ H non défini</source>
        <translation>H field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="274"/>
        <source>Champ Espèce non défini</source>
        <translation>Species field not defined</translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="275"/>
        <source>Champ Commentaire non défini</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_steploadtreemap.cpp" line="353"/>
        <source>Ligne %1 du fichier REF non valide</source>
        <translation>Line %1 of REF file not valid</translation>
    </message>
</context>
<context>
    <name>ONF_StepManualInventory</name>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="74"/>
        <source>Séléctionner un DBH par arbre</source>
        <translation>Select one DBH per tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="88"/>
        <location filename="../step/onf_stepmanualinventory.cpp" line="89"/>
        <location filename="../step/onf_stepmanualinventory.cpp" line="90"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="92"/>
        <source>Scènes</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="94"/>
        <source>Groupe de base</source>
        <translation>Base group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="95"/>
        <source>Niveau Z</source>
        <translation>Z level</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="96"/>
        <source>Cluster</source>
        <translation>Cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="97"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="98"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="99"/>
        <source>Position2D</source>
        <translation>2D Position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="107"/>
        <source>Ne pas accepter de cercle plus loin que :</source>
        <translation>Don&apos;t accept circles farther than:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="108"/>
        <source>Choisir préférenciellement le diamètre à + ou - :</source>
        <translation>Prefer diameters at + or -:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="143"/>
        <source>Cercle du DHP</source>
        <translation>DBH circle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="144"/>
        <source>Attributs</source>
        <translation>Attributes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="148"/>
        <source>DHP (cm)</source>
        <translation>DBH (cm)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="151"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="154"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="157"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="160"/>
        <source>H130</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="163"/>
        <source>Hauteur</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="381"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmanualinventory.cpp" line="381"/>
        <source>Bienvenue dans le mode manuel de cette étape !</source>
        <translation>Welcome to the manual mode of this step !</translation>
    </message>
</context>
<context>
    <name>ONF_StepMatchItemsPositions</name>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="118"/>
        <source>Co-registration de deux ensembles de positions 2D</source>
        <translation>Co-register two 2D positions sets</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="124"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="145"/>
        <source>Positions de référence</source>
        <translation>Reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="147"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="157"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="190"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="210"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="148"/>
        <source>Item de référence</source>
        <translation>Reference Item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="149"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="159"/>
        <source>Coordonnée X</source>
        <translation>X Coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="150"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="160"/>
        <source>Coordonnée Y</source>
        <translation>Y Coordinate</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="151"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="161"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="152"/>
        <source>IDplot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="153"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="162"/>
        <source>Valeur</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="155"/>
        <source>Positions à transformer</source>
        <translation>Positions to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="158"/>
        <source>Item à transformer</source>
        <translation>Item to transform</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="164"/>
        <source>Résultat compteur</source>
        <translation>Counter result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="166"/>
        <source>Compteur</source>
        <translation>Counter</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="173"/>
        <source>Positions transformées</source>
        <translation>Transformed positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="174"/>
        <source>Groupe racine</source>
        <translation>Root group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="175"/>
        <source>Matrice de transformation</source>
        <translation>Transformation matrix</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="176"/>
        <source>Qualité de Matching</source>
        <translation>Matching quality</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="179"/>
        <source>RMSE Dist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="182"/>
        <source>RMSE Val</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="185"/>
        <source>Max Dist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="188"/>
        <source>Max Val diff</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="191"/>
        <source>Position transformée</source>
        <translation>Transformed position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="194"/>
        <source>ID position transformée</source>
        <translation>ID of transformed position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="197"/>
        <source>ID position de référence</source>
        <translation>ID of reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="200"/>
        <source>Ecart ValTrans - ValRef</source>
        <translation>Difference (ValTrans - ValRef)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="203"/>
        <source>Distance 2D Trans - Ref</source>
        <translation>XY Distance (Trans - Ref)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="206"/>
        <source>Position de référence correspondante</source>
        <translation>Corresponding reference positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="207"/>
        <source>Ligne de correspondance</source>
        <translation>Linking line</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="209"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="211"/>
        <source>Positions intermédiaires</source>
        <translation>Intermediate positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="220"/>
        <source>Critères d&apos;affiliation des positions :</source>
        <translation>Positions matching criterium:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="222"/>
        <source>Distance maximale entre points appariés :</source>
        <translation>Maximum distance between matching points:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="223"/>
        <source>Seuil de taille relative minimum entre items appariés :</source>
        <translation>Minimum threshold  for relative size between matching items:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="224"/>
        <source>Taille relative minimale :</source>
        <translation>Minimum relative size:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="225"/>
        <source>Rotation maximale autorisée :</source>
        <translation>Maximum allowed rotation:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="226"/>
        <source>Inversion de direction possible (+- 180°)</source>
        <translation>Azimut inversion allowed (+- 180°)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="229"/>
        <source>Critères de qualité de matching :</source>
        <translation>Matching quality criterium:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="231"/>
        <source>Poid du critère Nb. pos. de référence ayant une pos. transformée proche :</source>
        <translation>Weight for criteria Nb. reference pos. near a transformed pos.:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="232"/>
        <source>Poid du critère Nb. pos. transformées ayant une pos. de référence proche :</source>
        <translation>Weight for criteria Nb. transformed pos. near a reference pos.:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="233"/>
        <source>Poid du critère Nb. pos. transformées ayant une pos. de référence proche avec une taille similaire :</source>
        <translation>Weight for criteria Nb. transformed pos. near a reference pos. with a similar size:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="236"/>
        <source>Mode de représentation :</source>
        <translation>Drawing choices:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="238"/>
        <source>Type de représentation :</source>
        <translation>Drawing type:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="241"/>
        <source>Valeur Z</source>
        <translation>Z value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="242"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="244"/>
        <source>Comment représenter en Z la variable de taille ?</source>
        <translation>How to represent variable size as Z coordinate?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="248"/>
        <source>Valeur absolue</source>
        <translation>Absolute value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="249"/>
        <source>Valeur relative</source>
        <translation>Relative value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="250"/>
        <source>En cas de valeur relative :</source>
        <translation>If Relative value choosen:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="251"/>
        <source>Valeur de Z/Rayon minimum</source>
        <translation>Z value / minimum radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="252"/>
        <source>Valeur de Z/Rayon maximum</source>
        <translation>Z value / maximum radius</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="255"/>
        <source>Export des données :</source>
        <translation>Data export:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="257"/>
        <source>Exporter un rapport de Recalage</source>
        <translation>Export matching report</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="258"/>
        <source>Fichier d&apos;export du rapport de Recalage</source>
        <translation>File name for matching report</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="258"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="261"/>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="262"/>
        <source>Fichier texte (*.txt)</source>
        <translation>Text file (*.txt)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="260"/>
        <source>Exporter les données dans un fichier multi-placettes</source>
        <translation>Export data in a multi-plots file</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="261"/>
        <source>Fichier d&apos;export (multi-placettes) de données transformées</source>
        <translation>Export file (multi-plots) for transformed data</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmatchitemspositions.cpp" line="262"/>
        <source>Fichier d&apos;export (multi-placettes) des paramètres de transformation</source>
        <translation>Export file (multi-plots) for transformation parameters</translation>
    </message>
</context>
<context>
    <name>ONF_StepMergeClustersFromPositions02</name>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="86"/>
        <source>Isoler les houppiers à partir de positions (et de clusters)</source>
        <translation>Segment crowns from 2D positions (using clusters)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="92"/>
        <source>Cette étape permet de générer une scène pour chaque positions 2D fournie.&lt;br&gt;A partir de chaque position, les clusters fournis en entrée sont agrégés pas à pas au plus proche voisin.&lt;br&gt;&lt;br&gt;Ensuite une action interactive permet de corriger cette attribution automatique.</source>
        <translation>This step generate one scene for each input 2D position.&lt;br&gt;From each given position, input clusters are merged step by step to nearest neighbour.&lt;br&gt;&lt;br&gt;After that, an interactive action allows to correct the result of automatic segmentation. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="118"/>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="123"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="119"/>
        <source>Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="121"/>
        <source>Positions 2D</source>
        <translation>2D Positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="124"/>
        <source>Position 2D</source>
        <translation>2D Position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="126"/>
        <source>MNT (Raster)</source>
        <translation>DTM (raster)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="129"/>
        <source>Modèle Numérique de Terrain</source>
        <translation>Digital Terrain Model</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="143"/>
        <source>Scène segmentée</source>
        <translation>Segmented scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="144"/>
        <source>Z MNT</source>
        <translation>DTM Z value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="153"/>
        <source>Hauteur de référence</source>
        <translation>Reference height</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="154"/>
        <source>Distance maximum entre clusters d&apos;un même groupe</source>
        <translation>Maximum distance between clusters of the same group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="155"/>
        <source>Correction interactive ?</source>
        <translation>Interactive correction?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="442"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeclustersfrompositions02.cpp" line="442"/>
        <source>Bienvenue dans le mode manuel de cette étape de filtrage.</source>
        <translation>Welcome to the manual mode of this step !</translation>
    </message>
</context>
<context>
    <name>ONF_StepMergeEndToEndSections04</name>
    <message>
        <source>Fusion de billons alignés</source>
        <translation type="vanished">Merging of aligned logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="71"/>
        <source>4- Fusionner les Billons successifs</source>
        <translation>4- Merge successive logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="76"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="95"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="97"/>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="116"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="98"/>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="117"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="99"/>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="118"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="106"/>
        <source>Epaisseur des groupes en Z  :</source>
        <translation>Z-thickness of groups:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="107"/>
        <source>Distance maximale entre extremités de billons à fusionner :</source>
        <translation>Maximum distance beetween extremities of logs to merge:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="108"/>
        <source>Nombre de barycentres a considerer aux extremites :</source>
        <translation>Number of barycenters to consider at extremities:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="109"/>
        <source>Facteur multiplicatif de maxDist :</source>
        <translation>Multiplicative factor for maxDist:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="110"/>
        <source>Chevauchement toléré en Z :</source>
        <translation>Tolerated Z overlapping:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="115"/>
        <source>Billons Fusionnées</source>
        <translation>Merged logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeendtoendsections04.cpp" line="119"/>
        <source>Barycentre</source>
        <translation>Barycenter</translation>
    </message>
</context>
<context>
    <name>ONF_StepMergeNeighbourClustersInGrid</name>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="70"/>
        <source>Fusionner les clusters jointifs d&apos;une grille</source>
        <translation>Merge neighbours clusters in a voxel grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="97"/>
        <source>Grilles</source>
        <translation>Grids</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="101"/>
        <source>Grille segmentée</source>
        <translation>Segmented grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="111"/>
        <source>Grille fusionnée</source>
        <translation>Merged grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="119"/>
        <source>Voisinnage de fusion en XY</source>
        <translation>Neighbourhood merging in XY</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="119"/>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="120"/>
        <source>cases</source>
        <translation>cells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighbourclustersingrid.cpp" line="120"/>
        <source>Voisinnage de fusion en Z</source>
        <translation>Neighbourhood merging in Z</translation>
    </message>
</context>
<context>
    <name>ONF_StepMergeNeighbourSections04</name>
    <message>
        <source>Fusion de billons parallèles</source>
        <translation type="vanished">Merging of parallel logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="72"/>
        <source>3- Fusionner les Billons parallèles</source>
        <translation>3- Merge parallel logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="77"/>
        <source>Cette étape prend en entrée une liste de billons. Chaque billon est composée d&apos;une séquence de clusters. &lt;br&gt;Un cluster est caractérisé par :&lt;ul&gt;&lt;li&gt;Une liste de points&lt;/li&gt;&lt;li&gt;Un barycentre (le barycentre des points)&lt;/li&gt;&lt;li&gt;Une valeur &lt;em&gt;buffer&lt;/em&gt;, égale à la distance entre le barycentre et le point le plus éloigné du barycentre&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;Ces billons sont issues d&apos;une étape précédente telle que &lt;em&gt;ONF_StepDetectSection&lt;/em&gt;. Cependant, en début d&apos;étape elles sont remaniées de façon à ce que les clusters aient l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie en paramètre de l&apos;étape.&lt;br&gt;Au sein de chaque billon ce remaniement consiste à prendre tous les points de tous les clusters, afin de recréer des clusters de l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie.&lt;br&gt;Ensuite, pour chaque cluster créé, on en détermine le barycentre et le buffer.&lt;br&gt;&lt;b&gt;Le but de cette étape est de fusionner des billons appartenant dans la réalité au même arbre&lt;/b&gt;.&lt;br&gt;Elle traite spécifiquement le cas des billons se chevauchant verticalement. Elle est complétée par &lt;em&gt;ONF_StepMergeEndToEndSections&lt;/em&gt;.&lt;br&gt;En plus de l&apos; &lt;b&gt;épaisseur de cluster&lt;/b&gt;, cette étape utilise les paramètres suivants :&lt;ul&gt;&lt;li&gt;Une &lt;b&gt;distance de recherche de voisinnage&lt;/b&gt; (paramètre d&apos;optimisation des calculs)&lt;/li&gt;&lt;li&gt;Une distance &lt;b&gt;deltaZ&lt;/b&gt; : écart vertical maximal entre deux barycentres comparés&lt;/li&gt;&lt;li&gt;Un critère &lt;b&gt;distMax&lt;/b&gt; : distance XY maximum entre deux barycentres de billons à fusionner&lt;/li&gt;&lt;li&gt;Un critère &lt;b&gt;ratioMax&lt;/b&gt; : accroissement maximal du buffer accepté en cas de fusion&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;Le fonctionnement de l&apos;étape est le suivant. Les billons sont comparées deux à deux par ordre décroissant de longueur selon Z.A chaque itération, on compare une billon A (la plus longue) constituée de n clusters ayant des barycentres Ai (i = 1 à n), avec une billon B constituée de m clusters ayant des barycentres Bj (j = 1 à m).&lt;br&gt;Pour ce faire on commence par calculer &lt;b&gt;medBuffer&lt;/b&gt; : la médiane des distances buffers des barycentres Ai.&lt;br&gt;Pour que A et B soient fusionnées, il faut que pour tout i et j tels que la distance verticale |Ai - Bj|z &lt; &lt;b&gt;deltaZ&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Qu&apos;aucune distance horizontale |Ai - Bj|xy ne soit supérieure à &lt;b&gt;distMax&lt;/b&gt;&lt;/li&gt;&lt;li&gt;Qu&apos;aucune distance horizontale |Ai - Bj|xy ne soit supérieure à &lt;b&gt;medDist&lt;/b&gt;&lt;/li&gt;&lt;li&gt;Qu&apos;au moins pour un couple Ai / Bj, le ratio |Ai - Bj| / MAX(buffer Ai, buffer Bj) soit inférieur à &lt;b&gt;ratioMax&lt;/b&gt;&lt;/ul&gt;En cas de fusion, les clusters et les barycentres sont recréés à partir de tous les points des deux billons sources pour former une nouvelle billon C.&lt;br&gt;La billon C devient la de facto la plus longue : elle est donc aussitôt utilisée dans l&apos;itération suivant dans la comparaison avec la prochaine billon (plus petite) de la liste.</source>
        <translation>This step takes as input a list of logs. Each log is composed of a sequence of clusters.&lt;br&gt;
A cluster is characterized by:
&lt;ul&gt;
&lt;li&gt;A list of points&lt;/li&gt;
&lt;li&gt;A centroid (the centroid points)&lt;/li&gt;
&lt;li&gt;A value of &lt;em&gt;buffer&lt;/em &gt; equal to the distance between the centroid and the farthest point from the centroid&lt;/li&gt;
&lt;/ul&gt; 
These logs are created in a previous step like &lt;em&gt;ONF_StepDetectSection&lt;/em&gt;. However, in the early stage they are processed so that the clusters have the selected &lt;b&gt;thickness&lt;/b&gt; at the beginning of the step.&lt;br&gt;
Within each log this process takes all points of all clusters, abd recreate clusters of the chosen &lt;b&gt;thickness&lt;/b&gt;.&lt;br&gt;
After, for each created cluster, we the centroid and the buffer are computed too.&lt;br&gt;
&lt;b&gt;The purpose of this step is to merge logs which in reality belongs to the same tree&lt;/b&gt;.&lt;br&gt;
It deals specifically with the case of vertically overlapping logs (parallels). It is supplemented by &lt;em&gt;ONF_StepMergeEndToEndSections &lt;/em&gt;. &lt;br&gt;
In addition to the &lt;b&gt;thickness of clusters&lt;/b&gt;, this step uses the following parameters:
&lt;ul&gt; 
&lt;li&gt; &lt;b&gt;Neighborhoud searching distance&lt;/b&gt; (optimization parameter)&lt;/li&gt;
&lt;li&gt;Distance &lt;b&gt;DeltaZ&lt;/b&gt;: Maximum vertical distance between two compared centroids&lt;/li&gt;
&lt;li&gt;A criterion &lt;b&gt;distMax&lt;/b&gt;: XY maximum distance between two centroids in logs to merge&lt;/li&gt;
&lt;li&gt;A criterion &lt;b&gt;ratioMax&lt;/b&gt;: Maximum increase of buffer accepted when merging&lt;/li&gt;
&lt;/ul&gt;
&lt;br&gt;The step works as follows. Logs are compared in pairs in order of decreasing Z length. At each iteration, we compare a log A (longest) consisting of n clusters with centroids Ai (i = 1 to n), with a log B consisting of m clusters with centroids Bj (j = 1 to m)&lt;br&gt;
To do this we first calculate &lt;b&gt;medBuffer&lt;/b&gt; the median buffers of Ai clusters&lt;br&gt;
A and B are merged, if for all i and j whith a vertical distance |Ai - Bj|z &amp;lt; &lt;b&gt;DeltaZ&lt;/b&gt;
&lt;ul&gt; 
&lt;li&gt;- no horizontal distance |Ai - Bj|xy is greater than &lt;b&gt;distMax&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;- no horizontal distance |Ai - Bj|xy is greater than &lt;b&gt;medDist&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;- at least for a couple Ai / Bj, the ratio |Ai - Bj| / MAX (buffer Ai, buffer Bj) is less than &lt;b&gt;ratioMax&lt;/b&gt;
&lt;/ul&gt;
If A and B are merged, clusters and centroids are recreated from all points of the logs A and B to form a new log C.&lt;br&gt;
Log C becomes the longest log: it is immediately used in the next iteration in comparison with the next log (smaller) of the list.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="126"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="128"/>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="147"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="129"/>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="148"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="130"/>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="149"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="137"/>
        <source>Epaisseur (en Z) des clusters  :</source>
        <translation>Z-thickness of clusters:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="138"/>
        <source>Distance de recherche de voisinage :</source>
        <translation>Distance for neighbourood search:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="139"/>
        <source>Distance XY maximum entre barycentres de clusters de billons à fusionner :</source>
        <translation>Maximum XY distance between barycenter of clusters for logs to merge:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="140"/>
        <source>Distance Z maximum entre barycentres de clusters de billons à fusionner :</source>
        <translation>Maximum Z distance between barycenters of clusters for logs to merge:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="141"/>
        <source>fois</source>
        <translation>times</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="141"/>
        <source>Facteur d&apos;accroissement maximal des distances XY entre barycentres de clusters de billons à fusionner&apos; :</source>
        <translation>Maximum increasing factor for XY distances between barycenters of cluster for logs to merge:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="146"/>
        <source>Billons Fusionnées</source>
        <translation>Merged logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergeneighboursections04.cpp" line="150"/>
        <source>Barycentre</source>
        <translation>Barycenter</translation>
    </message>
</context>
<context>
    <name>ONF_StepMergeScenesByModality</name>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="70"/>
        <source>Merge scenes interactively</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="76"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="97"/>
        <source>Scenes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="101"/>
        <source>Scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="126"/>
        <source>Modalities (comma separated)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="126"/>
        <source>List all wanted modalities, separated by commas)</source>
        <translation>List all wanted modalities, separated by commas</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="215"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmergescenesbymodality.cpp" line="215"/>
        <source>Bienvenue dans le mode manuel de cette étape de filtrage.</source>
        <translation>Welcome to the manual mode of this step.</translation>
    </message>
</context>
<context>
    <name>ONF_StepModifyDEM</name>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="79"/>
        <source>Modify DEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="106"/>
        <source>DEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="110"/>
        <source>DEM à modifier</source>
        <translation>DEM to modify</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="112"/>
        <source>2D Images (optionnel)</source>
        <translation>2D Images (optionnal)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="114"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="115"/>
        <source>Red band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="116"/>
        <source>Green band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="117"/>
        <source>Blue band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="128"/>
        <source>DEM modifié</source>
        <translation>Modified DEM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="245"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifydem.cpp" line="245"/>
        <source>Bienvenue dans le mode manuel de cette étape de modification de MNE.</source>
        <translation>Welcome to the manual mode of this step of DEM modification.</translation>
    </message>
</context>
<context>
    <name>ONF_StepModifyPositions2D</name>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="67"/>
        <source>Modifier des positions 2D</source>
        <translation>Modify 2D positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="81"/>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="90"/>
        <source>Positions 2D</source>
        <translation>2D Positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="83"/>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="91"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="84"/>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="92"/>
        <source>Position 2D</source>
        <translation>2D Position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="159"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifypositions2d.cpp" line="159"/>
        <source>Bienvenue dans le mode manuel de cette étape</source>
        <translation>Welcome to the manual mode of this step !</translation>
    </message>
</context>
<context>
    <name>ONF_StepModifyVoxelSegmentation</name>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="76"/>
        <source>Modify voxel grid segmentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="103"/>
        <source>Grilles</source>
        <translation>Grids</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="107"/>
        <source>Scene à segmenter</source>
        <translation>Scene to be segmented</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="108"/>
        <source>Grille de points</source>
        <translation>Point grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="109"/>
        <source>Grille segmentée</source>
        <translation>Segmented grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="110"/>
        <source>Grille topologique</source>
        <translation>Topological grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="120"/>
        <source>Grille segmentée corrigée</source>
        <translation>Corrected segmented grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="121"/>
        <source>IDs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="122"/>
        <source>Labels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="123"/>
        <source>VoxelCluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="124"/>
        <source>Label</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="132"/>
        <source>Ne conserver que les arbres validés ?</source>
        <translation>Keep only validated trees ?</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="237"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepmodifyvoxelsegmentation.cpp" line="237"/>
        <source>Bienvenue dans le mode manuel de cette étape de correction de segmentation.</source>
        <translation>Welcome to the manual mode of this step of segmentation correction.</translation>
    </message>
</context>
<context>
    <name>ONF_StepOptimizeGaussianOnMaximaNumber</name>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="35"/>
        <source>Filtre Gaussien optimisé par le nombre de maxima</source>
        <translation>Gaussian filter optimized by maxima number</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="41"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="62"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="64"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="65"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="75"/>
        <source>Image filtrée</source>
        <translation>Filtered image (Gaussian)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="76"/>
        <source>Sigma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="77"/>
        <source>Maxima</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="86"/>
        <source>Sigma max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="86"/>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="87"/>
        <source>en mètres</source>
        <translation>in meters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="87"/>
        <source>Incrément de Sigma par étape</source>
        <translation>Sigma increment for each step</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="88"/>
        <source>Ne pas détécter de maxima en dessous de</source>
        <translation>Don&apos;t detect maxima below</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="88"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="90"/>
        <source>Coeff Mult</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="92"/>
        <source>N.B. : Portée du filtre = 7.7 x Sigma (en mètres)</source>
        <translation>N.B.: Filter range = 7.7 x Sigma (in meters)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepoptimizegaussianonmaximanumber.cpp" line="174"/>
        <source>Sigma retenu : %1</source>
        <translation>Kept sigma: %1</translation>
    </message>
</context>
<context>
    <name>ONF_StepPolygonFromMask</name>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="60"/>
        <source>Création de polygones à partir de masques</source>
        <translation>Create polygons from masks</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="82"/>
        <source>Un unique polygone par masque</source>
        <translation>One single polygon for each mask</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="83"/>
        <source>Un ou plusieurs polygones par masque</source>
        <translation>One or more polygons for each mask</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="88"/>
        <source>Dalles</source>
        <translation>Tiles</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="90"/>
        <source>Grp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="91"/>
        <source>Masque</source>
        <translation>Mask</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="94"/>
        <source>XYRef (optionnel)</source>
        <translation>XYRef (optionnal)</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="95"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="96"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="105"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="109"/>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="111"/>
        <source>Polygone</source>
        <translation>Polygon</translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="114"/>
        <source>X_Ref</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_steppolygonfrommask.cpp" line="115"/>
        <source>Y_Ref</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ONF_StepReducePointsDensity</name>
    <message>
        <source>Réduire la Densité de points</source>
        <translation type="vanished">Reduce points density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="69"/>
        <source>2- Réduire la Densité de points</source>
        <translation>2- Reduce points density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="74"/>
        <source>Créée une grille régulière de la &lt;b&gt;résolution&lt;/b&gt; choisie. Ne garde que le point le plus proche du centre dans chaque case. </source>
        <translation>Create a regular grid with given &lt;b&gt;resolution&lt;/b&gt;. Keep only nearst point of the cell center in each cell of the grid. </translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="87"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="90"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="99"/>
        <source>Scène réduite</source>
        <translation>Filtered scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="106"/>
        <source>Résolution de la grille :</source>
        <translation>Grid resolution:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="141"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="209"/>
        <source>La scène de densité réduite comporte %1 points.</source>
        <translation>The filtered scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepreducepointsdensity.cpp" line="212"/>
        <source>Aucun point conservé pour cette scène</source>
        <translation>No point has been kept for this scene</translation>
    </message>
</context>
<context>
    <name>ONF_StepRefPointFromArcCenter</name>
    <message>
        <source>Création de points de réf. à partir d&apos;arcs</source>
        <translation type="vanished">Creation of ref. points from arcs</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfromarccenter.cpp" line="52"/>
        <source>Créer des points de référence à partir d&apos;Arcs</source>
        <translation>Create reference points from Arcs</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfromarccenter.cpp" line="57"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfromarccenter.cpp" line="70"/>
        <source>Polyline(s)</source>
        <translation>Polyline(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfromarccenter.cpp" line="74"/>
        <source>Polyligne</source>
        <translation>Polyline</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfromarccenter.cpp" line="82"/>
        <source>Barycentre</source>
        <translation>Barycenter</translation>
    </message>
</context>
<context>
    <name>ONF_StepRefPointFromBarycenter02</name>
    <message>
        <source>Création de points de réf. à partir de barycentres</source>
        <translation type="vanished">Creation of ref. points from barycenters</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfrombarycenter02.cpp" line="54"/>
        <source>Créer des points de référence à partir de Barycentres</source>
        <translation>Create reference points from barycenters</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfrombarycenter02.cpp" line="59"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfrombarycenter02.cpp" line="72"/>
        <source>Polylignes</source>
        <translation>Polylines</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfrombarycenter02.cpp" line="76"/>
        <source>Polyligne</source>
        <translation>Polyline</translation>
    </message>
    <message>
        <location filename="../step/onf_steprefpointfrombarycenter02.cpp" line="84"/>
        <source>Barycentre</source>
        <translation>Barycenter</translation>
    </message>
</context>
<context>
    <name>ONF_StepRemoveUpperNoise</name>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="73"/>
        <source>Remove upper noise points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="78"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="91"/>
        <source>Scène à débruiter</source>
        <translation>Scene to denoise</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="91"/>
        <source>Par exemple pour des scènes où les filtres matériels sont désactivés</source>
        <translation>For example of scenes where hardware filters are desactivated</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="94"/>
        <source>Groupe contenant la scène</source>
        <translation>Group containing the scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="95"/>
        <source>Scène bruitée</source>
        <translation>Noised scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="105"/>
        <source>Scène réduite</source>
        <translation>Denoised scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="113"/>
        <source>Grid resolution:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="114"/>
        <source>Minimum number of points for a filled cell:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="115"/>
        <source>Minimum number of points for a valid cell:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="116"/>
        <source>Maximum gap length:</source>
        <translation>Longueur de trouée maximale</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="204"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="248"/>
        <source>La scène de densité réduite comporte %1 points.</source>
        <translation>The filtered scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="249"/>
        <source>Nombre de points filtrés : %1</source>
        <translation>Number of filtered points: %1</translation>
    </message>
    <message>
        <location filename="../step/onf_stepremoveuppernoise.cpp" line="252"/>
        <source>Aucun point conservé pour cette scène</source>
        <translation>No point has been kept for this scene</translation>
    </message>
</context>
<context>
    <name>ONF_StepSegmentCrowns</name>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="74"/>
        <source>Segmenter des houppiers en 2D</source>
        <translation>Segment crowns in 2D</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="80"/>
        <source>Cette étape permet de segmenter des houppiers manuellement dans un plan horizontal.&lt;br&gt;Une première phase manuelle permet de déteminer la tranche verticale sur laquelle les points seront analysés. On y définit un niveau Z minimum, un niveau Z maximum, ainsi qu&apos;une résolution pour les rasters de travail :&lt;ul&gt;&lt;li&gt;Le Modèle Numérique de Surface (MNS) = hauteur du point le plus haut pour chaque case&lt;/li&gt;&lt;li&gt;La densité de point par case&lt;/li&gt;&lt;/ul&gt;Les rasters sont ensuite utilisés dans une seconde phase, afin de segmenter les houppiers. Un système par couleurs permet de façon semi-automatique de délimiter l&apos;emprise horizontale de chaque houppier. Sur la base d&apos;une pré-segmentation, l&apos;opérateur peut modifier les groupes (houppiers) en les fusionnant ou en les divisant.&lt;br&gt;En sortie, cette étapes produit :&lt;ul&gt;&lt;li&gt;Un raster avec une valeur entière différente pour chaque houppier&lt;/li&gt;&lt;li&gt;Une scène de points extraite pour chaque houppier&lt;/li&gt;&lt;li&gt;Un polygone horizontal correspondant à l&apos;enveloppe convexe des pixels de chaque houppier&lt;/li&gt;&lt;li&gt;Deux métriques donnant respectivement la surface des pixels, et la surface de l&apos;enveloppe convexe pour chaque houppier&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>This step allows to manually segment crowns in a horizontal plane.&lt;br&gt;A first manual phase allows defining a vertical slice on which the points will be analyzed. It defines a minimum level Z, a maximum Z level and a resolution for working rasters: &lt;ul&gt;&lt;li&gt;The Digital Surface Model (DSM) = height of the highest point for each cell&lt;/li&gt;&lt;li&gt;The dot density per cell&lt;/li&gt;&lt;/ul&gt;Rasters are then used in a second phase, to segment the crowns. A color system allows semi-automatically delineation the horizontal shape for each crown. On the basis of a pre-segmentation, the user can change the groups (crowns) by merging or dividing them&lt;br&gt;As output, this step products:&lt;ul&gt;&lt;li&gt;A raster with a different integer value for each crown&lt;/li&gt;&lt;li&gt;A points scene for each crown&lt;/li&gt;&lt;li&gt;A horizontal polygon corresponding to the convex hull of pixels of each crown&lt;/li&gt;&lt;li&gt;Two metrics giving respectively the surface of crown pixels, and the surface of the convex hull for each crown&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="110"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="114"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="120"/>
        <source>Densité, MNS et clusters</source>
        <translation>Density, DSM, Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="123"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="124"/>
        <source>Densité</source>
        <translation>Density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="125"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="128"/>
        <source>Scènes segmentées</source>
        <translation>Segmented scenes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="129"/>
        <source>ConvexHull</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="130"/>
        <source>Attributs du Houppier</source>
        <translation>Crown attributes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="133"/>
        <source>Aire du houppier</source>
        <translation>Crown area</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="136"/>
        <source>Aire du houppier convexe</source>
        <translation>Crown convex hull area</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="139"/>
        <source>Z max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="142"/>
        <source>ClusterID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="196"/>
        <source>Selection de l&apos;épaisseur des houppiers</source>
        <translation>Selection of crowns thickness</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="227"/>
        <source>Création des clusters</source>
        <translation>Creatiing clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="240"/>
        <source>Démarrage des post_traitements</source>
        <translation>Beginning post-processings</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="241"/>
        <source>Elimination des clusters vides</source>
        <translation>Removing empty clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="245"/>
        <source>Création nuages de points par cluster</source>
        <translation>Creating points clouds by cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="260"/>
        <source>Ajout des points aux scènes par cluster</source>
        <translation>Adding points to scenes per cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="265"/>
        <source>Enregistrement des clusters</source>
        <translation>Saving clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="272"/>
        <source>Création des Convex Hulls</source>
        <translation>Creating convex hulls</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="277"/>
        <source>Calcul des métriques</source>
        <translation>Computing metrics</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="294"/>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="530"/>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="551"/>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="571"/>
        <source>Segmentation des houppiers</source>
        <translation>Segmentation of crowns</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="294"/>
        <source>Phase 2 (Segmentation des houppiers) impossible à réaliser.</source>
        <translation>Phase 2 (Segmentation of crowns) impossible to do.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="299"/>
        <source>Post-Traitements terminés</source>
        <translation>Post-processings achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="530"/>
        <source>Mode manuel.
Phase 1 : Calcul de la carte de densité et du MNS.
L&apos;initialisation peut prendre un peu de temps...</source>
        <translation>Manual mode.
Phase 1: Computing density map and DSM.
The initialisation could take some time...</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="551"/>
        <source>Mode manuel.
Phase 2 : Segmentation des houppiers.
La pré-segmentation automatique peut prendre un peu de temps...</source>
        <translation>Manual mode.
Phase 2: Segmentation of crowns.
The automatic pre-segmentation could take some time...</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrowns.cpp" line="571"/>
        <source>Fin du mode manuel, démarrage des post-traitements
Cela peut prendre un peu de temps...</source>
        <translation>Manuel mode ended, beginning post-processings.
It could take some time...</translation>
    </message>
</context>
<context>
    <name>ONF_StepSegmentCrownsFromStemClusters</name>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="58"/>
        <source>Segmenter des houppiers à partir de Clusters tiges</source>
        <translation>Segment crowns from stem clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="64"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="85"/>
        <source>Tiges détéctées</source>
        <translation>Detected stems</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="87"/>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="89"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="88"/>
        <source>Scène complète</source>
        <translation>Complete scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="90"/>
        <source>Tige</source>
        <translation>Stem</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="99"/>
        <source>Cluster segmenté</source>
        <translation>Segmented cluster</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="100"/>
        <source>Zmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentcrownsfromstemclusters.cpp" line="108"/>
        <source>Distance Max</source>
        <translation>Max Distance</translation>
    </message>
</context>
<context>
    <name>ONF_StepSegmentFromSeedGrid</name>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="66"/>
        <source>Segment using seed voxel grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="83"/>
        <source>Grilles</source>
        <translation>Grids</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="87"/>
        <source>Grille de points</source>
        <translation>Point grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="88"/>
        <source>Grille de graines</source>
        <translation>Seed grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="97"/>
        <source>Segmentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="98"/>
        <source>Topologie</source>
        <translation>Topology</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="99"/>
        <source>Topologie inverse</source>
        <translation>Inverse topology</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="108"/>
        <source>Distance de recherche maximale en Z</source>
        <translation>Maximal searching distance in Z</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentfromseedgrid.cpp" line="109"/>
        <source>Distance de recherche maximale en XY</source>
        <translation>Maximal searching distance in XY</translation>
    </message>
</context>
<context>
    <name>ONF_StepSegmentGaps</name>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="77"/>
        <source>Segmenter des trouées en 2D</source>
        <translation>Segment gaps in 2D</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="83"/>
        <source>Cette étape permet de segmenter des trouées manuellement dans un plan horizontal.&lt;br&gt;Une première phase manuelle permet de déteminer la tranche verticale sur laquelle les points seront analysés. On y définit un niveau Z minimum, un niveau Z maximum, ainsi qu&apos;une résolution pour les rasters de travail :&lt;ul&gt;&lt;li&gt;Le Modèle Numérique de Surface (MNS) = hauteur du point le plus haut pour chaque case&lt;/li&gt;&lt;li&gt;La densité de point par case&lt;/li&gt;&lt;/ul&gt;Les rasters sont ensuite utilisés dans une seconde phase, afin de segmenter les trouées. Un système par couleurs permet de façon semi-automatique de délimiter l&apos;emprise horizontale de chaque trouée. Sur la base d&apos;une pré-segmentation, l&apos;opérateur peut modifier les groupes (trouées) en les fusionnant ou en les divisant.&lt;br&gt;En sortie, cette étapes produit :&lt;ul&gt;&lt;li&gt;Un raster avec une valeur entière différente pour chaque trouée&lt;/li&gt;&lt;li&gt;Une métrique donnant la surface des pixels pour chaque trouée&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>This step allows to manually segment gaps in a horizontal plane.&lt;br&gt;A first manual phase allows defining a vertical slice on which the points will be analyzed. It defines a minimum level Z, a maximum Z level and a resolution for working rasters: &lt;ul&gt;&lt;li&gt;The Digital Surface Model (DSM) = height of the highest point for each cell&lt;/li&gt;&lt;li&gt;The dot density per cell&lt;/li&gt;&lt;/ul&gt;Rasters are then used in a second phase, to segment the gaps. A color system allows semi-automatically delineation the horizontal shape for each gap. On the basis of a pre-segmentation, the user can change the groups (gaps) by merging or dividing them&lt;br&gt;As output, this step products:&lt;ul&gt;&lt;li&gt;A raster with a different integer value for each gap&lt;/li&gt;&lt;li&gt;One metric giving the surface of gap pixels&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="111"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="115"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="121"/>
        <source>Densité, MNS et clusters</source>
        <translation>Density, DSM, Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="124"/>
        <source>MNS</source>
        <translation>DSM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="125"/>
        <source>Densité</source>
        <translation>Density</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="126"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="129"/>
        <source>Attributs de la trouée</source>
        <translation>Gap attributes</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="132"/>
        <source>Aire de trouée</source>
        <translation>Gap area</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="135"/>
        <source>ClusterID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="143"/>
        <source>Ne détécter les trouées que dans l&apos;enveloppe convexe des points</source>
        <translation>Detect gaps only in horizontal convex hull of points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="187"/>
        <source>Selection de l&apos;épaisseur des houppiers</source>
        <translation>Selection of crowns thickness</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="218"/>
        <source>Création des clusters</source>
        <translation>Creatiing clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="231"/>
        <source>Démarrage des post_traitements</source>
        <translation>Beginning post-processings</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="232"/>
        <source>Elimination des clusters vides</source>
        <translation>Removing empty clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="235"/>
        <source>Enregistrement des clusters</source>
        <translation>Saving clusters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="241"/>
        <source>Calcul des métriques</source>
        <translation>Computing metrics</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="255"/>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="377"/>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="398"/>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="419"/>
        <source>Segmentation des trouées</source>
        <translation>Segment gaps in 2D</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="255"/>
        <source>Phase 2 (Segmentation des trouées) impossible à réaliser.</source>
        <translation>Phase 2 (Segmentation of gaps) impossible to do.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="260"/>
        <source>Post-Traitements terminés</source>
        <translation>Post-processings achieved</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="377"/>
        <source>Mode manuel.
Phase 1 : Calcul de la carte de densité et du MNS.
L&apos;initialisation peut prendre un peu de temps...</source>
        <translation>Manual mode.
Phase 1: Computing density map and DSM.
The initialisation could take some time...</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="398"/>
        <source>Mode manuel.
Phase 2 : Segmentation des trouées.
La pré-segmentation automatique peut prendre un peu de temps...</source>
        <translation>Manual mode.
Phase 2: Segmentation of gaps.
The automatic pre-segmentation could take some time...</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsegmentgaps.cpp" line="419"/>
        <source>Fin du mode manuel, démarrage des post-traitements
Cela peut prendre un peu de temps...</source>
        <translation>Manuel mode ended, beginning post-processings.
It could take some time...</translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectBBoxByFileName</name>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="62"/>
        <source>Charger l&apos;emprise correspondant à un nom de fichier</source>
        <translation>Load XY area shape corresponding to a file name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="68"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="89"/>
        <source>Emprises disponibles</source>
        <translation>XY area shapes available</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="90"/>
        <source>Résultat contenant toutes les emprises disponibles.
Chaque groupe contient :
- Une Emprise (item ayant une boite englobante : en général Forme 2D)
- Un item avec un attribut conteant le nom du fichier correspondant (Header)
</source>
        <translation>Result containning all XY area shapes available.
Each group contains:
- One XY area shape (item with a bounding box: generally a 2D shape)
- One item with an attribute givin the corresponding file name (header)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="95"/>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="105"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="96"/>
        <source>Item Nom de fichier</source>
        <translation>File name item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="97"/>
        <source>Nom de fichier</source>
        <translation>File name</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="98"/>
        <source>Emprise correspondante</source>
        <translation>Corresponding XY area shape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="101"/>
        <source>Fichier dont l&apos;emprise doit être chargée</source>
        <translation>File for which the XY area shape has to be loaded</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="102"/>
        <source>Résultat contenant le nom du fichier pour lequel il faut charger l&apos;emprise (Header)</source>
        <translation>Result containning the file name for which the XY area shape has to be loaded (header)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="106"/>
        <source>Entête de fichier</source>
        <translation>File Header</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectbboxbyfilename.cpp" line="115"/>
        <source>Emprise</source>
        <translation>XY area shape</translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectCellsInGrid3D</name>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="77"/>
        <source>Séléctionner une partie de Grille 3D</source>
        <translation>Select a part of a 3D grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="83"/>
        <source>Cette étape permet de générer une grille booléenne, représentant une séléction de cellules parmi celles de la grille de référence choisie en entrée.&lt;br&gt;Elle utilise un actionner, permettant de faire des séléction soit par plans horizontaux 2D, soit directement en 3D.&lt;br&gt;En sortie elle fournie également une copie de la grille d&apos;entrée pour laquelle toute les cases non sélectionnées sont réinitialisées à la valeur 0. </source>
        <translation>This step generates a Boolean grid, representing a selection of cells of the input reference grid.&lt;br&gt;It uses an interactive action to select cells by horizontal 2D planes or directly in 3D.&lt;br&gt;As output it also provides a copy of the input grid where any non-selected cell has been set to 0.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="90"/>
        <source>http://rdinnovation.onf.fr/projects/plugin-base/wiki/Fr_action_selectcells</source>
        <translation>http://rdinnovation.onf.fr/projects/plugin-base/wiki/En_action_selectcells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="105"/>
        <source>Result</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="113"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="141"/>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="142"/>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="145"/>
        <source>Grille filtrée</source>
        <translation>Filtered gird</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="144"/>
        <source>Cases séléctionnées</source>
        <translation>Selected cells</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="228"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3d.cpp" line="228"/>
        <source>Bienvenue dans le mode manuel de cette étape de filtrage.
Seuls les voxels séléctionés (Rouges) seront conservés.
Laisser la souris au-dessus d&apos;un bouton pour avoir des infos.</source>
        <translation>Welcome to the manual mode of this filtering step.
Only selected voxels (Red) will be kept.
Keep the cursor over a button to obtain informations. </translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectCellsInGrid3DByBinaryPattern</name>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="74"/>
        <source>Créer Grille Booléenne à partir d&apos;une Grille de Convolution</source>
        <translation>Create a boolean grid from a convolution 3D matrix</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="80"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="101"/>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="103"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="102"/>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="111"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="110"/>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="113"/>
        <source>Grille de séléction</source>
        <translation>Selection grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="112"/>
        <source>Grille de comptage</source>
        <translation>Counting grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="122"/>
        <source>Valeur minimale de la grille d&apos;entrée</source>
        <translation>Minimum value in input grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="123"/>
        <source>Motif binaire</source>
        <translation>Convolution motif</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="125"/>
        <source>Choix du mode de fitrage :</source>
        <translation>Choose filtering mode:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="128"/>
        <source>En valeur absolue :</source>
        <translation>As absolute value:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="129"/>
        <source>Seuil de séléction</source>
        <translation>Selection Threshold</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="131"/>
        <source>En % de la valeur maximale :</source>
        <translation>As % of maximum value:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectcellsingrid3dbybinarypattern.cpp" line="132"/>
        <source>Seuil de séléction (en % du max)</source>
        <translation>Detection threshold (in % of max)</translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectClustersInLogs</name>
    <message>
        <location filename="../step/onf_stepselectclustersinlogs.cpp" line="44"/>
        <source>Sélection de clusters dans des billons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectclustersinlogs.cpp" line="58"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectclustersinlogs.cpp" line="62"/>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectclustersinlogs.cpp" line="187"/>
        <source>Mode manuel</source>
        <translation type="unfinished">Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectclustersinlogs.cpp" line="187"/>
        <source>Bienvenue dans le mode manuel de cette étape de filtrage. Veuillez sélectionner les éléments dans la vue graphique puis valider en cliquant sur le pouce en haut de la fenêtre principale. Les éléments sélectionnés seront gardés dans le résultat de sortie.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectGroupsByReferenceHeight</name>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="68"/>
        <source>Filter des items dans une tranche de hauteur depuis un MNT</source>
        <translation>Filter items in a horizontal slice from DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="82"/>
        <source>Items</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="84"/>
        <source>Groupe de base</source>
        <translation>Base group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="85"/>
        <source>Z MNT</source>
        <translation>DTM Z value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="86"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="87"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectgroupsbyreferenceheight.cpp" line="100"/>
        <source>Hauteur de référence</source>
        <translation>Reference height</translation>
    </message>
</context>
<context>
    <name>ONF_StepSelectSceneForEachPosition</name>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="94"/>
        <source>Apparier scènes et positions terrain</source>
        <translation>Matching point scenes and field tree positions</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="100"/>
        <source>To Do</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="114"/>
        <source>Placette</source>
        <translation>Plot</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="116"/>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="127"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="117"/>
        <source>Arbre</source>
        <translation>Tree</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="118"/>
        <source>DBH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="119"/>
        <source>Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="120"/>
        <source>IDtree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="121"/>
        <source>IDplot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="122"/>
        <source>Species</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="125"/>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="128"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="132"/>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="135"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="134"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="148"/>
        <source>Scene arbre</source>
        <translation>Tree scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="149"/>
        <source>IDCluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="157"/>
        <source>Distance d&apos;appariement maximale :</source>
        <translation>Maximum matching distance</translation>
    </message>
    <message>
        <location filename="../step/onf_stepselectsceneforeachposition.cpp" line="158"/>
        <source>Correction interactive</source>
        <translation>Interactive correction</translation>
    </message>
</context>
<context>
    <name>ONF_StepSetAffiliationIDFromReference</name>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="60"/>
        <source>Jointure entre deux résultats ç l&apos;aide d&apos;IDs d&apos;affiliation</source>
        <translation>Join two results using affiliations IDs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="65"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="78"/>
        <source>Résultat de référence</source>
        <translation>Reference result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="80"/>
        <source>Groupe de référence</source>
        <translation>Reference group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="81"/>
        <source>Item de référence</source>
        <translation>Reference item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="83"/>
        <source>Résultat à affilier</source>
        <translation>To affiliate result</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="85"/>
        <source>Groupe à affilier</source>
        <translation>To affiliate group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="86"/>
        <source>Item à affilier</source>
        <translation>To affiliate item</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="93"/>
        <source>Affiliation par position 2D (3D sinon)</source>
        <translation>Join from 2D positions (3D else)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="94"/>
        <source>Correction des affiliations en mode manuel</source>
        <translation>Use manual mode to correst affiliations</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="104"/>
        <source>ID de référence</source>
        <translation>Reference ID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="109"/>
        <source>ID à affilier</source>
        <translation>To affiliate ID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="237"/>
        <source>Modification des affiliations</source>
        <translation>Modification of affiliations</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetaffiliationidfromreference.cpp" line="237"/>
        <source>Mode manuel.</source>
        <translation>Manual mode.</translation>
    </message>
</context>
<context>
    <name>ONF_StepSetFootCoordinatesVertically</name>
    <message>
        <source>Ajout d&apos;une coordonnée de base / billon // MNT</source>
        <translation type="vanished">Addition od a base coordinante by log // DTM</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="56"/>
        <source>5- Récupérer la coordonnée MNT pour chaque Billon</source>
        <translation>5- Retrieve DTM coordinate for each log</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="61"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="76"/>
        <source>MNT (Raster)</source>
        <translation>DTM (raster)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="79"/>
        <source>Modèle Numérique de Terrain</source>
        <translation>Digital Terrain Model</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="83"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="85"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="86"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="87"/>
        <source>Point de référence</source>
        <translation>Reference point</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsetfootcoordinatesvertically.cpp" line="99"/>
        <source>Coordonnée MNT</source>
        <translation>DTM coordinate</translation>
    </message>
</context>
<context>
    <name>ONF_StepSlicePointCloud</name>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="89"/>
        <source>Découper une scène en Tranches Horizontales</source>
        <translation>Segment scene in horizontal slices</translation>
    </message>
    <message>
        <source>Action manuelle permettant de découper une scène d&apos;entrée en tranches horizontales.&lt;br&gt;Il est possible d&apos;en régler intéractivement :&lt;br&gt;- L&apos;épaisseur (&lt;b&gt;_thickness&lt;/b&gt;)&lt;br&gt;- L&apos;espacement entre deux tranches (&lt;b&gt;_spacing&lt;/b&gt;)&lt;br&gt;&lt;br&gt;N.B. : Cette étape peut également fonctionner en mode non interactif, avec les paramètres choisis dans la boite de configuration. </source>
        <translation type="vanished">Manual action to segment an input scene in horizontal slices.&lt;br&gt;You can adjust interactively:&lt;br&gt;- &lt;b&gt;Thickness&lt;/b&gt; of slices&lt;br&gt;- &lt;b&gt;Spacing&lt;/b&gt; between two slices&lt;br&gt;N.B.: This step can also operate in non-interactive mode, using parameters defined in the configuration dialog.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="114"/>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="117"/>
        <source>Scène à découper</source>
        <translation>Scene to sclice</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="116"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Scène découpée</source>
        <translation type="vanished">Sliced scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="127"/>
        <source>Tranche</source>
        <translation>Slice</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="128"/>
        <source>Points de la tranche</source>
        <translation>Points of slice</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="137"/>
        <source>Epaisseur des tranches :</source>
        <translation>Slice thickness:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="95"/>
        <source>Action manuelle permettant de découper une scène d&apos;entrée en tranches horizontales.&lt;br&gt;Il est possible d&apos;en régler intéractivement :&lt;br&gt;- L&apos;épaisseur (&lt;b&gt;_thickness&lt;/b&gt;, en m)&lt;br&gt;- L&apos;espacement entre deux tranches (&lt;b&gt;_spacing&lt;/b&gt;, en m)&lt;br&gt;&lt;br&gt;N.B. : Cette étape peut également fonctionner en mode non interactif, avec les paramètres choisis dans la boite de configuration. </source>
        <translation>Manual action to cut an input scene into horizontal slices&lt;br&gt;&lt;br&gt;Following parameters can be adjusted interactively:&lt;br&gt;- The thickness(&lt;b&gt;_thickness&lt;/b&gt;, in m)&lt;br&gt;- Spacing between two slices (&lt;b&gt;_spacing&lt;/b&gt;, in m)&lt;br&gt;&lt;br&gt;NB : This step can also work in non-interactive mode, using the parameter values chosen in the configuration box.</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="137"/>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="138"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="138"/>
        <source>Espacement des tranches :</source>
        <translation>Slice spacing:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="139"/>
        <source>Choix interactif des paramètres</source>
        <translation>Interactively adjust parameters</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="267"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepslicepointcloud.cpp" line="268"/>
        <source>Bienvenue dans le mode manuel de cette étape.
Veuillez sélectionner les paramètres pour réaliser les tranches.</source>
        <translation>Welcome to the manual mode of this step.
Please adjust parameters to define slices. </translation>
    </message>
</context>
<context>
    <name>ONF_StepSmoothSkeleton</name>
    <message>
        <source>Lissage d&apos;une séquence de points de référence</source>
        <translation type="vanished">Smoothing of sequence of ref. points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="52"/>
        <source>Lisser une séquence de Points de référence</source>
        <translation>Smooth a sequence of refernce points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="57"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="70"/>
        <source>Billons / Clusters / Points de référence</source>
        <translation>Logs / Clusters / Reference points</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="72"/>
        <source>Billon (Grp)</source>
        <translation>Log (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="73"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="86"/>
        <source>Point de référence (lissé)</source>
        <translation>Reference point (smoothed)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepsmoothskeleton.cpp" line="74"/>
        <source>Point de référence</source>
        <translation>Reference point</translation>
    </message>
</context>
<context>
    <name>ONF_StepStandardizeIntensity</name>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="48"/>
        <source>Standardiser l&apos;intensité (ALS)</source>
        <translation>Standardize Intensity (ALS)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="62"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="64"/>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="69"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="65"/>
        <source>Attributs LAS</source>
        <translation>LAS attributs</translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="65"/>
        <source>Attribut LAS</source>
        <translation>LAS attribut</translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="67"/>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="70"/>
        <source>Trajectory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="80"/>
        <source>All Attributs (Icorr)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="81"/>
        <source>Corrected intensity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="90"/>
        <source>Altitude moyenne du vol</source>
        <translation>Average flight altitude</translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="229"/>
        <source>Pas d&apos;information de trajectoire pour le point (%1 ; %2 ; %3)</source>
        <translation>No trajectory information for the point (%1 ;%2 ;%3)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepstandardizeintensity.cpp" line="237"/>
        <source>Pas d&apos;information de trajectoire pour au total %1 points sur %2</source>
        <translation>No trajectory information for a total of %1 points on %2</translation>
    </message>
</context>
<context>
    <name>ONF_StepTransformPointCloud</name>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="63"/>
        <source>Appliquer une Matrice de Transformation à une Scène</source>
        <translation>Apply a transformation matrix to a scene</translation>
    </message>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="90"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="92"/>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="97"/>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="105"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="93"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="95"/>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="98"/>
        <source>Matrice de transformation</source>
        <translation>Transformation matrix</translation>
    </message>
    <message>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="104"/>
        <location filename="../step/onf_steptransformpointcloud.cpp" line="106"/>
        <source>Scène(s) transformée(s)</source>
        <translation>Transformed scene(s)</translation>
    </message>
</context>
<context>
    <name>ONF_StepValidateInventory</name>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="60"/>
        <source>Validation d&apos;inventaire</source>
        <translation>Inventory validation</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="74"/>
        <source>Items</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="76"/>
        <source>Groupe de base</source>
        <translation>Base group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="77"/>
        <source>Z MNT</source>
        <translation>DTM Z value</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="78"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="79"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="88"/>
        <source>Position de référence</source>
        <translation>Reference position</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="92"/>
        <source>Espèce</source>
        <translation>Species</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="95"/>
        <source>IDterrain</source>
        <translation>FieldID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="98"/>
        <source>IDitem</source>
        <translation>ItemID</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="107"/>
        <source>Fichier d&apos;espèces</source>
        <translation>Species file</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="108"/>
        <source>Hauteur de référence</source>
        <translation>Reference height</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="213"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvalidateinventory.cpp" line="213"/>
        <source>Bienvenue dans le mode manuel de cette étape !</source>
        <translation>Welcome to the manual mode of this step !</translation>
    </message>
</context>
<context>
    <name>ONF_StepVoxelClusterization</name>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="68"/>
        <source>Clusterization selon une grille voxel</source>
        <translation>Clusterization from voxel grid</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="74"/>
        <source>TO DO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="85"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="87"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="88"/>
        <source>Item à clusteriser</source>
        <translation>Item to clusterize</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="96"/>
        <source>Cluster (Grp)</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="97"/>
        <source>Cluster (Points)</source>
        <translation>Cluster (Points)</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="105"/>
        <source>Résolution de la grille</source>
        <translation>Grid resolution</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="105"/>
        <source>meters</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="107"/>
        <source>Callage du coin (minX, minY, minZ) :</source>
        <translation>How to compute coordinates of the grid corner (minX, minY, minZ):</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="110"/>
        <source>Sur la boite englobante de la scène</source>
        <translation>Using bounding box of the scene</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="111"/>
        <source>Par rapport aux coordonnées suivantes :</source>
        <translation>Multiples of the following  coordinates:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="113"/>
        <source>Coordonnée X :</source>
        <translation>X Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="114"/>
        <source>Coordonnée Y :</source>
        <translation>Y Coordinate:</translation>
    </message>
    <message>
        <location filename="../step/onf_stepvoxelclusterization.cpp" line="115"/>
        <source>Coordonnée Z :</source>
        <translation>Z Coordinate:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="105"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="106"/>
        <source>Xs</source>
        <translation>Xs</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="107"/>
        <source>Ys</source>
        <translation>Ys</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="108"/>
        <source>Zs</source>
        <translation>Zs</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="109"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="110"/>
        <source>ExclusionRadius</source>
        <translation>RayonExclusion</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="111"/>
        <source>Ni</source>
        <translation>Ni</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="112"/>
        <source>Nb</source>
        <translation>Nb</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="113"/>
        <source>Nt</source>
        <translation>Nt</translation>
    </message>
    <message>
        <location filename="../itemdrawable/onf_lvoxsphere.h" line="114"/>
        <source>N_excluded</source>
        <translation>N_exclus</translation>
    </message>
    <message>
        <source>Détéction tiges (ONF 2013)</source>
        <translation type="vanished">Stem detection (ONF 2013)</translation>
    </message>
    <message>
        <source>Détection tiges (ONF 2013)</source>
        <translation type="vanished">Stem detection (ONF 2013)</translation>
    </message>
    <message>
        <location filename="../onf_steppluginmanager.cpp" line="215"/>
        <location filename="../onf_steppluginmanager.cpp" line="216"/>
        <location filename="../onf_steppluginmanager.cpp" line="217"/>
        <location filename="../onf_steppluginmanager.cpp" line="218"/>
        <location filename="../onf_steppluginmanager.cpp" line="219"/>
        <location filename="../onf_steppluginmanager.cpp" line="220"/>
        <location filename="../onf_steppluginmanager.cpp" line="221"/>
        <location filename="../onf_steppluginmanager.cpp" line="298"/>
        <source>Détécter (tiges) - ONF 2013</source>
        <translation>Detect (stems) - ONF 2013</translation>
    </message>
    <message>
        <location filename="../onf_steppluginmanager.cpp" line="230"/>
        <location filename="../onf_steppluginmanager.cpp" line="231"/>
        <location filename="../onf_steppluginmanager.cpp" line="232"/>
        <source>Points de référence</source>
        <translation>Reference points</translation>
    </message>
    <message>
        <location filename="../onf_steppluginmanager.cpp" line="289"/>
        <location filename="../onf_steppluginmanager.cpp" line="325"/>
        <location filename="../onf_steppluginmanager.cpp" line="326"/>
        <location filename="../onf_steppluginmanager.cpp" line="327"/>
        <location filename="../onf_steppluginmanager.cpp" line="328"/>
        <source>Segmentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../onf_steppluginmanager.cpp" line="291"/>
        <location filename="../onf_steppluginmanager.cpp" line="292"/>
        <source>Cigognes</source>
        <translation>Storks</translation>
    </message>
</context>
</TS>
