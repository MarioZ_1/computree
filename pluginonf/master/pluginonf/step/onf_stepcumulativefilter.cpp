#include "onf_stepcumulativefilter.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "opencv2/imgproc/imgproc.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"



// Constructor : initialization of parameters
ONF_StepCumulativeFilter::ONF_StepCumulativeFilter(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _radius = 2.0;
}

// Step description (tooltip of contextual menu)
QString ONF_StepCumulativeFilter::getStepDescription() const
{
    return tr("Filtre cumulatif");
}

// Step detailled description
QString ONF_StepCumulativeFilter::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ONF_StepCumulativeFilter::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepCumulativeFilter::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepCumulativeFilter(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepCumulativeFilter::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_image, CT_AbstractImage2D::staticGetType(), tr("Image"));

}

// Creation and affiliation of OUT models
void ONF_StepCumulativeFilter::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _cumulativeImage_ModelName, new CT_Image2D<float>(), tr("Image cumulée"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepCumulativeFilter::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Rayon de sommation"), tr("en mètres"), 0, 999999, 2, _radius, 1);
}

void ONF_StepCumulativeFilter::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);


    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        CT_AbstractImage2D* imageIn = (CT_AbstractImage2D*)grp->firstItemByINModelName(this, DEFin_image);
        if (imageIn != NULL)

        {
            int ncells = std::ceil((_radius - (imageIn->resolution() / 2.0)) / imageIn->resolution());
            qDebug() << ncells;

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<float>* cumulativeImage = new CT_Image2D<float>(_cumulativeImage_ModelName.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), -1, 0);
            grp->addItemDrawable(cumulativeImage);

            for (int x = 0 ; x < cumulativeImage->colDim() ; x++)
            {
                for (int y = 0 ; y < cumulativeImage->linDim() ; y++)
                {
                    int xxMin = x - ncells; if (xxMin < 0) {xxMin = 0;}
                    int yyMin = y - ncells; if (yyMin < 0) {yyMin = 0;}

                    int xxMax = x + ncells; if (xxMax >= cumulativeImage->colDim()) {xxMax = cumulativeImage->colDim() - 1;}
                    int yyMax = y + ncells; if (yyMax >= cumulativeImage->linDim()) {yyMax = cumulativeImage->linDim() - 1;}

                    Eigen::Vector3d centerCoord;
                    cumulativeImage->getCellCenterCoordinates(x, y, centerCoord);

                    float sum = 0;
                    for (int xx = xxMin ; xx <= xxMax ; xx++)
                    {
                        for (int yy = yyMin ; yy <= yyMax ; yy++)
                        {
                            size_t index;
                            imageIn->index(xx, yy, index);

                            Eigen::Vector3d cellCoord;
                            cumulativeImage->getCellCenterCoordinates(xx, yy, cellCoord);

                            double dist = std::sqrt(pow(centerCoord(0) - cellCoord(0), 2) + pow(centerCoord(1) - cellCoord(1), 2));
                            if (dist <= _radius)
                            {
                                sum += (float)imageIn->valueAtIndexAsDouble(index);
                            }
                        }
                    }

                    cumulativeImage->setValue(x, y, sum);
                }

                setProgress(100.0* ((double)x / (double)(cumulativeImage->colDim())));
            }

            cumulativeImage->computeMinMax();
        }
    }
}
