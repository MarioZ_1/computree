/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepaddfakecounter.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

#include <QDebug>

// Alias for indexing models
#define DEFin_result "inres"
#define DEFin_grp "grp"
#define DEFin_item "item"
#define DEFin_nameAtt "nameAtt"

// Constructor : initialization of parameters
ONF_StepAddFakeCounter::ONF_StepAddFakeCounter(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ONF_StepAddFakeCounter::getStepDescription() const
{
    return tr("Ajoute un compteur unitaire");
}

// Step detailled description
QString ONF_StepAddFakeCounter::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString ONF_StepAddFakeCounter::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepAddFakeCounter::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepAddFakeCounter(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepAddFakeCounter::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn = createNewInResultModelForCopy(DEFin_result, tr("Nom"), tr(""), true);
    resIn->setZeroOrMoreRootGroup();
    resIn->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn->addItemModel(DEFin_grp, DEFin_item, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
    resIn->addItemAttributeModel(DEFin_item, DEFin_nameAtt, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Nom"));
}

// Creation and affiliation of OUT models
void ONF_StepAddFakeCounter::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_result);
    if (res != NULL)
    {
        res->addItemModel(DEFin_grp, _outCounter_ModelName, new CT_LoopCounter(), tr("Counter"));
    }
}


void ONF_StepAddFakeCounter::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut = outResultList.at(0);

    CT_ResultGroupIterator itOut(resOut, this, DEFin_grp);
    while (itOut.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itOut.next();
        CT_AbstractSingularItemDrawable* item = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, DEFin_item);
        if (item != NULL)
        {
            CT_AbstractItemAttribute *attribute = item->firstItemAttributeByINModelName(resOut, this, DEFin_nameAtt);
            if (attribute != NULL)
            {
                bool ok;
                QString filename = attribute->toString(item, &ok);

                if (ok && !filename.isEmpty())
                {
                    QSharedPointer<CT_Counter> counter = QSharedPointer<CT_Counter>(new CT_Counter(1));

                    CT_LoopCounter* loopCounter = new CT_LoopCounter(_outCounter_ModelName.completeName(), resOut, counter, NULL);
                    loopCounter->setTurnName(filename);
                    group->addItemDrawable(loopCounter);
                }
            }
        }
    }
}
