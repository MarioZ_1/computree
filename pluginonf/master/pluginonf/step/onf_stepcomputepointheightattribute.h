#ifndef ONF_STEPCOMPUTEPOINTHEIGHTATTRIBUTE_H
#define ONF_STEPCOMPUTEPOINTHEIGHTATTRIBUTE_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class ONF_StepComputePointHeightAttribute : public CT_AbstractStep
{
    Q_OBJECT

public:

    struct PointCore0_5
    {
        quint8  entire; // Edge of Flight Line (1 bit) - Scan Direction Flag (1 bit) - Number of Returns (3 bits) - Return Number (3 bits)

        typedef quint8 MASK;
    };

    struct PointCore6_10
    {
        quint16 entire; // Number of Returns (4 bits) - Return Number (4 bits)
                        // Edge of Flight Line (1 bit) - Scan Direction Flag (1 bit) - Scanner Channel (2 bits) - Classification Flags (4 bits)

        typedef quint16 MASK;
    };

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ONF_StepComputePointHeightAttribute(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPreConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    bool                    _ptsAttribute;
    bool                    _ptsCloud;
    bool                    _addLAS;
    // Step parameters

    CT_AutoRenameModels     _outHeightAttributeModelName;
    CT_AutoRenameModels     _outHeightCloudModelName;
    CT_AutoRenameModels     _lasAttributesOut_AutoRenameModel;
    CT_AutoRenameModels     _lasintensityOut_AutoRenameModel;
    CT_AutoRenameModels     _lasreturnNumberOut_AutoRenameModel;
    CT_AutoRenameModels     _lasnumberOfReturnsOut_AutoRenameModel;
    CT_AutoRenameModels     _lasclassificationFlagsOut_AutoRenameModel;
    CT_AutoRenameModels     _lasscannerChannelOut_AutoRenameModel;
    CT_AutoRenameModels     _lasscanDirectionFlagsOut_AutoRenameModel;
    CT_AutoRenameModels     _lasedgeOfFlightLineOut_AutoRenameModel;
    CT_AutoRenameModels     _lasclassificationOut_AutoRenameModel;
    CT_AutoRenameModels     _lasscanAngleRankOut_AutoRenameModel;
    CT_AutoRenameModels     _lasuserDataOut_AutoRenameModel;
    CT_AutoRenameModels     _laspointSourceIDOut_AutoRenameModel;
    CT_AutoRenameModels     _lasgpsTimeOut_AutoRenameModel;
    CT_AutoRenameModels     _lascolorOut_AutoRenameModel;
    CT_AutoRenameModels     _lascolorROut_AutoRenameModel;
    CT_AutoRenameModels     _lascolorGOut_AutoRenameModel;
    CT_AutoRenameModels     _lascolorBOut_AutoRenameModel;
    CT_AutoRenameModels     _laswavePacketOut_AutoRenameModel;
    CT_AutoRenameModels     _lasbyteOffsetWaveformOut_AutoRenameModel;
    CT_AutoRenameModels     _laswaveformPacketSizeOut_AutoRenameModel;
    CT_AutoRenameModels     _lasreturnPointWaveformOut_AutoRenameModel;
    CT_AutoRenameModels     _lasnirOut_AutoRenameModel;


};


#endif // ONF_STEPCOMPUTEPOINTHEIGHTATTRIBUTE_H
