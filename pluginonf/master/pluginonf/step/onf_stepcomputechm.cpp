/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepcomputechm.h"

#include "ct_global/ct_context.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_triangulation/ct_delaunayt.h"
#include "ct_triangulation/ct_nodet.h"
#include "ct_triangulation/ct_trianglet.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/image2dtools/ct_image2dnaturalneighboursinterpolator.h"


#include <QtConcurrent>
#include <math.h>
#include <stdlib.h>
#include <limits>


#define DEF_SearchInResult   "ires"
#define DEF_SearchInGroup   "igrp"
#define DEF_SearchInMNS   "imns"

#define DEF_SearchInResultMNT   "iresMNT"
#define DEF_SearchInGroupMNT   "igrpMNT"
#define DEF_SearchInMNT   "imnt"


ONF_StepComputeCHM::ONF_StepComputeCHM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

QString ONF_StepComputeCHM::getStepDescription() const
{
    return tr("Créer MNH");
}

QString ONF_StepComputeCHM::getStepDetailledDescription() const
{
    return tr("Cette étape permet de générer un MNH à partir d'un MNS et d'un MNT. L'emprise et la résolution du CHM seront calés sur le MNS. ");
}

CT_VirtualAbstractStep* ONF_StepComputeCHM::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new ONF_StepComputeCHM(dataInit);
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeCHM::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("MNS"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInMNS, CT_Image2D<float>::staticGetType(), tr("MNS"));


    CT_InResultModelGroupToCopy *resultModelMNT = createNewInResultModelForCopy(DEF_SearchInResultMNT, tr("MNT"));

    resultModelMNT->setZeroOrMoreRootGroup();
    resultModelMNT->addGroupModel("", DEF_SearchInGroupMNT);
    resultModelMNT->addItemModel(DEF_SearchInGroupMNT, DEF_SearchInMNT, CT_Image2D<float>::staticGetType(), tr("MNT"));
}

void ONF_StepComputeCHM::createPostConfigurationDialog()
{
}

void ONF_StepComputeCHM::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(resultModel != NULL) {
        resultModel->addItemModel(DEF_SearchInGroup, _outCHMModelName, new CT_Image2D<float>(), tr("MNH"));
    }
}

void ONF_StepComputeCHM::compute()
{
    CT_Image2D<float>* mnt = NULL;
    if (getInputResults().size() > 1)
    {
        CT_ResultGroup* resin_DTM = getInputResults().at(1);
        CT_ResultItemIterator it(resin_DTM, this, DEF_SearchInMNT);
        if (it.hasNext())
        {
            mnt = (CT_Image2D<float>*) it.next();
        } else {return;}
    } else {return;}


    // recupere les resultats de sortie
    const QList<CT_ResultGroup*> &outResList = getOutResultList();
    // récupération des modéles out
    CT_ResultGroup *outResult = outResList.at(0);

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    while (!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Image2D<float> *mns = (CT_Image2D<float>*)group->firstItemByINModelName(this, DEF_SearchInMNS);

            if (mnt != NULL && mns != NULL)
            {
                CT_Image2D<float>* chm = new CT_Image2D<float>(_outCHMModelName.completeName(), outResult, mns->minX(), mns->minY(), mns->colDim(), mns->linDim(), mns->resolution(), mns->minZ(), mns->NA(), 0);

                size_t ncells = mns->nCells();
                for (size_t index = 0 ; index < ncells ; index++)
                {
                    Eigen::Vector3d center;
                    mns->getCellCenterCoordinates(index, center);

                    float mnsVal = mns->valueAtIndex(index);
                    float mntVal = mnt->valueAtCoords(center(0), center(1));

                    if (mnsVal != mns->NA() && mntVal != mnt->NA())
                    {
                        chm->setValueAtIndex(index, mnsVal - mntVal);
                    } else {
                        chm->setValueAtIndex(index, chm->NA());
                    }

                    setProgress(90.0*(double)index/(double)ncells);
                }

                // ajout du raster MNS
                group->addItemDrawable(chm);
                chm->computeMinMax();
            }

        }
        setProgress(100);
    }
}
