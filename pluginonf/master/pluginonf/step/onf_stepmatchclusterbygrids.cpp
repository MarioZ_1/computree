/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepmatchclusterbygrids.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_iterator/ct_pointiterator.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>
#include <QFile>
#include <QTextStream>

#define DEF_SearchInResult "r"
#define DEF_SearchInScene   "sc"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInItemDrawableAtt    "att"
#define DEF_SearchInItemWithName    "iname"

#define DEF_SearchInResultGrid "rgrd"
#define DEF_SearchInGroupGrd   "grgrd"
#define DEF_SearchInGrid   "grd"
#define DEF_SearchInGridAtt   "grdAtt"

#define DEF_inResultCounter "rcounter"
#define DEF_inGroupCounter   "gcounter"
#define DEF_inCounter   "counter"

#define DEF_itemOut_grxy "grxy"
#define DEF_itemOut_grxz "grxz"
#define DEF_itemOut_gryz "gryz"

ONF_StepMatchClusterByGrids::ONF_StepMatchClusterByGrids(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _exportInLoop = false;
}

QString ONF_StepMatchClusterByGrids::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Attribuer les points de cluster à des grilles booléenes");
}

// Step description (tooltip of contextual menu)
QString ONF_StepMatchClusterByGrids::getStepDetailledDescription() const
{
    return tr("Pour chaque point de chaque cluster d'entrée, cette étape identifie la grille voxel contenant le point."
              "L'identifiant correspondant est attribué au point. Les données sont exportées dans un fichier ascii. ");
}

CT_VirtualAbstractStep* ONF_StepMatchClusterByGrids::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepMatchClusterByGrids(dataInit);
}

void ONF_StepMatchClusterByGrids::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Clusters"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Cluster à attribuer"));


    CT_InResultModelGroup *resultModelGrids = createNewInResultModel(DEF_SearchInResultGrid, tr("Grilles"));

    resultModelGrids->setZeroOrMoreRootGroup();
    resultModelGrids->addGroupModel("", DEF_SearchInGroupGrd);
    resultModelGrids->addItemModel(DEF_SearchInGroupGrd, DEF_SearchInGrid, CT_Grid3D_Sparse<bool>::staticGetType(), tr("Gille (bool)"));
    resultModelGrids->addItemAttributeModel(DEF_SearchInGrid, DEF_SearchInGridAtt, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Nom"));

    if (_exportInLoop)
    {
        CT_InResultModelGroup* res_counter = createNewInResultModel(DEF_inResultCounter, tr("Résultat compteur"), "", true);
        res_counter->setRootGroup(DEF_inGroupCounter);
        res_counter->addItemModel(DEF_inGroupCounter, DEF_inCounter, CT_LoopCounter::staticGetType(), tr("Compteur"));
        res_counter->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
    }

}

void ONF_StepMatchClusterByGrids::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);
}

void ONF_StepMatchClusterByGrids::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addBool(tr("Export dans une boucle"), "", tr("Activer"), _exportInLoop);
}

void ONF_StepMatchClusterByGrids::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice(tr("Fichier d'export des points affiliés"),CT_FileChoiceButton::OneNewFile, tr("Fichier ASCII (*.*)"), _outputFileName);
}

void ONF_StepMatchClusterByGrids::compute()
{
    if (_outputFileName.isEmpty()) {return;}
    QFile outputFile(_outputFileName.first());

    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().at(0);

    QList<CT_ResultGroup*> resultList = getInputResults();
    CT_ResultGroup* resultGrid = resultList.at(1);

    CT_ResultGroupIterator it0(outResult, this, DEF_SearchInGroup);

    int nclusters = 0;
    while(!isStopped() && it0.hasNext())
    {
        it0.next();
        nclusters++;
    }

    QMap<QString, CT_Grid3D_Sparse<bool>* > grids;
    CT_ResultGroupIterator itGrd0(resultGrid, this, DEF_SearchInGroupGrd);
    while(!isStopped() && itGrd0.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)itGrd0.next();
        CT_Grid3D_Sparse<bool>* grid = (CT_Grid3D_Sparse<bool>*)group->firstItemByINModelName(this, DEF_SearchInGrid);
        CT_AbstractItemAttribute* att = NULL;
        if (grid != NULL)
        {
            att = grid->firstItemAttributeByINModelName(resultGrid, this, DEF_SearchInGridAtt);

            QString name = att->toString(grid,NULL);

            if (!name.isEmpty())
            {
                grids.insert(name, grid);
            }
        }
    }

    QString exportBaseName = "";
    bool header = true;
    if (_exportInLoop && resultList.size() > 2)
    {
        header = false;
        CT_ResultGroup* resCounter = resultList.at(2);
        CT_ResultItemIterator itCounter(resCounter, this, DEF_inCounter);
        if (itCounter.hasNext())
        {
            const CT_LoopCounter* counter = (const CT_LoopCounter*) itCounter.next();

            if (counter != NULL)
            {
                QFileInfo fileinfo(counter->getTurnName());
                if (fileinfo.exists())
                {
                    exportBaseName = fileinfo.baseName();
                } else {
                    exportBaseName = counter->getTurnName();
                }
            }
            if (counter->getCurrentTurn() <= 1)
            {
                header = true;
            }
        }
    }

    if (header && outputFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&outputFile);
        stream << "PlotID\tPointID\tX\tY\tZ\tClusterID\tRefID\n";
        outputFile.close();
    }


    if (outputFile.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream stream(&outputFile);   CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
        int cpt = 0;
        // iterate over all groups
        while(!isStopped() && it.hasNext())
        {
            CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)it.next();
            const CT_AbstractItemDrawableWithPointCloud* cluster = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_SearchInScene);

            if (cluster != NULL)
            {
                CT_PointIterator itP(cluster->getPointCloudIndex()) ;
                while(itP.hasNext())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    size_t indice = itP.currentGlobalIndex();

                    QString correspName;

                    QMapIterator<QString, CT_Grid3D_Sparse<bool>* > itGrds(grids);
                    while (itGrds.hasNext() && correspName.isEmpty())
                    {
                        itGrds.next();
                        QString name = itGrds.key();
                        CT_Grid3D_Sparse<bool>* grid = itGrds.value();

                        bool val = grid->valueAtXYZ(point(0), point(1), point(2));
                        if (val)
                        {
                            correspName = name;
                        }
                    }

                    stream << exportBaseName << "\t" << indice << "\t" << CT_NumericToStringConversionT<double>::toString(point(0)) << "\t" << CT_NumericToStringConversionT<double>::toString(point(1)) << "\t" << CT_NumericToStringConversionT<double>::toString(point(2)) << "\t" << cluster->id()<< "\t" << correspName << "\n";
                }
            }

            setProgress(100 * cpt++ / nclusters);
        }

        outputFile.close();
    }

    setProgress(99);
}


