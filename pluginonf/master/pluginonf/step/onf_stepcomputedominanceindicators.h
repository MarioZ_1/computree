#ifndef ONF_STEPCOMPUTEDOMINANCEINDICATORS_H
#define ONF_STEPCOMPUTEDOMINANCEINDICATORS_H


#include "ct_step/abstract/ct_abstractstep.h"

// Inclusion of auto-indexation system
#include "ct_tools/model/ct_autorenamemodels.h"
#include "ct_math/delaunay2d/ct_delaunaytriangulation.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

#include "ct_math/delaunay2d/ct_delaunaytriangulation.h"


class ONF_StepComputeDominanceIndicators: public CT_AbstractStep
{
    Q_OBJECT

public:

    struct Apex {
        Apex(double x, double y, double z, double h, double diameter, CT_StandardItemGroup* group, CT_AbstractSingularItemDrawable* outItem)
        {
            _x = x;
            _y = y;
            _z = z;
            _h = h;
            _diameter = diameter;
            _group = group;
            _outItem = outItem;
        }

        double  _x;
        double  _y;
        double  _z;
        double  _h;
        double  _diameter;
        CT_StandardItemGroup* _group;
        CT_AbstractSingularItemDrawable* _outItem;
    };

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    ONF_StepComputeDominanceIndicators(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPreConfigurationDialog();
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    bool        _outItem;

    // Declaration of autoRenames Variables (groups or items added to In models copies)
    CT_AutoRenameModels    _itemAtt_ModelName;
    CT_AutoRenameModels    _indiceSchutzTotal_ModelName;
    CT_AutoRenameModels    _indiceSchutzHorTotal_ModelName;
    CT_AutoRenameModels    _indiceSchutzVerTotal_ModelName;
    CT_AutoRenameModels    _indiceSchutzMax_ModelName;
    CT_AutoRenameModels    _indiceSchutzHorMax_ModelName;
    CT_AutoRenameModels    _indiceSchutzVerMax_ModelName;
    CT_AutoRenameModels    _angleNeighbMax_ModelName;

    CT_AutoRenameModels    _outScene_ModelName;
    CT_AutoRenameModels    _indiceSchutzTotal_OutModelName;
    CT_AutoRenameModels    _indiceSchutzHorTotal_OutModelName;
    CT_AutoRenameModels    _indiceSchutzVerTotal_OutModelName;
    CT_AutoRenameModels    _indiceSchutzVerTotalINT_OutModelName;
    CT_AutoRenameModels    _indiceSchutzMax_OutModelName;
    CT_AutoRenameModels    _indiceSchutzHorMax_OutModelName;
    CT_AutoRenameModels    _indiceSchutzVerMax_OutModelName;
    CT_AutoRenameModels    _angleNeighbMax_OutModelName;

};


#endif // ONF_STEPCOMPUTEDOMINANCEINDICATORS_H
