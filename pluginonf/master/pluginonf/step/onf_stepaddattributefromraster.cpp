/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepaddattributefromraster.h"

#ifdef USE_OPENCV

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_itemdrawable/abstract/ct_abstractsingularitemdrawable.h"
#include "ct_itemdrawable/abstract/ct_abstractimage2d.h"


#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_item "item"

#define DEFin_resRaster "resRaster"
#define DEFin_RasterGrp "RasterGrp"
#define DEFin_Raster "Raster"



// Constructor : initialization of parameters
ONF_StepAddAttributeFromRaster::ONF_StepAddAttributeFromRaster(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _attName = "Value";
}

// Step description (tooltip of contextual menu)
QString ONF_StepAddAttributeFromRaster::getStepDescription() const
{
    return tr("Ajouter la valeurs d'un raster en tant qu'attribut");
}

// Step detailled description
QString ONF_StepAddAttributeFromRaster::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ONF_StepAddAttributeFromRaster::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepAddAttributeFromRaster::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepAddAttributeFromRaster(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepAddAttributeFromRaster::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Item"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_item, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));

    CT_InResultModelGroup *resultRaster = createNewInResultModel(DEFin_resRaster, tr("Raster"), "", true);
    resultRaster->setZeroOrMoreRootGroup();
    resultRaster->addGroupModel("", DEFin_RasterGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultRaster->addItemModel(DEFin_RasterGrp, DEFin_Raster, CT_AbstractImage2D::staticGetType(), tr("Raster"));
}

// Creation and affiliation of OUT models
void ONF_StepAddAttributeFromRaster::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);

    if(resCpy_res != NULL) {
        resCpy_res->addItemAttributeModel(DEFin_item, _att_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), _attName);
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepAddAttributeFromRaster::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addString(tr("Nom de l'attribut"), "", _attName);
}

void ONF_StepAddAttributeFromRaster::compute()
{

    CT_ResultGroup* resin_Raster = getInputResults().at(1);

    CT_AbstractImage2D* raster = NULL;
    CT_ResultItemIterator it(resin_Raster, this, DEFin_Raster);
    if (it.hasNext())
    {
        raster = (CT_AbstractImage2D*) it.next();
    }

    if (raster != NULL)
    {
        QList<CT_ResultGroup*> outResultList = getOutResultList();
        CT_ResultGroup* res = outResultList.at(0);

        // COPIED results browsing
        CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
        while (itCpy_grp.hasNext() && !isStopped())
        {
            CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();

            CT_AbstractSingularItemDrawable* item = (CT_AbstractSingularItemDrawable*)grp->firstItemByINModelName(this, DEFin_item);
            if (item != NULL)
            {
                size_t index;
                raster->indexAtCoords(item->getCenterX(), item->getCenterY(), index);

                double value = raster->valueAtIndexAsDouble(index);
                item->addItemAttribute(new CT_StdItemAttributeT<double>(_att_ModelName.completeName(),
                                                                        CT_AbstractCategory::DATA_VALUE,
                                                                        res,
                                                                        value));
            }
        }
    }
}

#endif
