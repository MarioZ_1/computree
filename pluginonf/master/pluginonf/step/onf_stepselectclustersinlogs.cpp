#include "onf_stepselectclustersinlogs.h"

// Inclusion of in models
#include "ct_itemdrawable/model/inModel/ct_inzeroormoregroupmodel.h"
#include "ct_itemdrawable/model/inModel/ct_instdgroupmodel.h"
#include "ct_itemdrawable/model/inModel/ct_instdsingularitemmodel.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"

// Inclusion of out models
#include "ct_itemdrawable/model/outModel/ct_outstdgroupmodel.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"
#include "ct_iterator/ct_groupiterator.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractsingularitemdrawable.h"

//Inclusion of actions
#include "actions/onf_actionselectclustersinlogs.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_model/tools/ct_modelsearchhelper.h"

#include <QMessageBox>

// Alias for indexing in models
#define DEF_resultIn_R "Res"
#define DEF_groupIn_Log "Glog"
#define DEF_groupIn_Cluster "Gcluster"
#define DEF_itemIn_I "Item"

// Constructor : initialization of parameters
ONF_StepSelectClustersInLogs::ONF_StepSelectClustersInLogs(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    m_doc = NULL;
    setManual(true);
}

// Step description (tooltip of contextual menu)
QString ONF_StepSelectClustersInLogs::getStepDescription() const
{
    return tr("Sélection de clusters dans des billons");
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepSelectClustersInLogs::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepSelectClustersInLogs(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepSelectClustersInLogs::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultInModel_R = createNewInResultModelForCopy(DEF_resultIn_R, tr("Result"), tr(""), false);
    resultInModel_R->setZeroOrMoreRootGroup();
    resultInModel_R->addGroupModel("", DEF_groupIn_Log);
    resultInModel_R->addGroupModel(DEF_groupIn_Log, DEF_groupIn_Cluster);
    resultInModel_R->addItemModel(DEF_groupIn_Cluster, DEF_itemIn_I, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
}

// Creation and affiliation of OUT models
void ONF_StepSelectClustersInLogs::createOutResultModelListProtected()
{
    createNewOutResultModelToCopy(DEF_resultIn_R);
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepSelectClustersInLogs::createPostConfigurationDialog()
{
    // No parameter dialog for this step
    //CT_StepConfigurableDialog *dialog = newStandardPostConfigurationDialog();

}

void ONF_StepSelectClustersInLogs::compute()
{
    m_doc = NULL;
    m_status = 0;

    // ----------------------------------------------------------------------------
    // Get the group model corresponding to DEF_groupIn_G
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup *resultOut = outResultList.first();

    // ---------------------------
    // Gets OUT results and models
    m_itemDrawableToAdd.clear();

    QList<CT_AbstractItemGroup*> groupsToRemove;

    CT_ResultGroupIterator itG_log(resultOut, this, DEF_groupIn_Log);

    // create a list of itemdrawable to add in the document
    while(itG_log.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup *groupLog = itG_log.next();

        quint64 logID = groupLog->id();

        CT_GroupIterator itG_cluster(groupLog, this, DEF_groupIn_Cluster);

        while(itG_cluster.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup *group_cluster = itG_cluster.next();

            CT_AbstractSingularItemDrawable *item = group_cluster->firstItemByINModelName(this, DEF_itemIn_I);

            if (item != NULL)
            {
                m_itemDrawableToAdd.insert(item, (CT_AbstractItemGroup*)group_cluster);

                m_clusterToLog.insert(item, logID);
                m_logToCluster.insert(logID, item);
            }
        }
    }      

    // request the manual mode
    requestManualMode();

    // remove item selected from the list (to not remove them from the out result)
    QListIterator<CT_AbstractItemDrawable*> it(m_validatedItems);

    while(it.hasNext() && !isStopped())
    {
        m_itemDrawableToAdd.remove(it.next());
    }

    groupsToRemove.append(m_itemDrawableToAdd.values());

    // we remove the parent group of all ItemDrawable that must be deleted from the out result
    // and all groups that don't contains a ItemDrawable researched
    QListIterator<CT_AbstractItemGroup*> itE(groupsToRemove);

    while(itE.hasNext())
    {
        CT_AbstractItemGroup *group = itE.next();
        recursiveRemoveGroup(group->parentGroup(), group);
    }

    m_status = 1;
    requestManualMode();
}

void ONF_StepSelectClustersInLogs::recursiveRemoveGroup(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const
{
    if(parent != NULL)
    {
        parent->removeGroup(group);

        if(parent->isEmptyOfGroups())
        {
            recursiveRemoveGroup(parent->parentGroup(), parent);
        }
    }
    else
    {
        ((CT_ResultGroup*)group->result())->removeGroupSomethingInStructure(group);
    }
}

void ONF_StepSelectClustersInLogs::initManualMode()
{
    // create a new 3D document
    if(m_doc == NULL)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_validatedItems.clear();
    m_doc->removeAllItemDrawable();

    // TODO add async with GuiManagerInterface
    QHashIterator<CT_AbstractItemDrawable*, CT_AbstractItemGroup*> it(m_itemDrawableToAdd);

    while(it.hasNext())
    {
        m_doc->addItemDrawable(*it.next().key());
    }

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionSelectClustersInLogs(&m_clusterToLog, &m_logToCluster, &m_validatedItems));


    QMessageBox::information(NULL, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de filtrage. Veuillez sélectionner les "
                                                         "éléments dans la vue graphique puis valider en cliquant "
                                                         "sur le pouce en haut de la fenêtre principale. Les éléments "
                                                         "sélectionnés seront gardés dans le résultat de sortie."), QMessageBox::Ok);
}

void ONF_StepSelectClustersInLogs::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {          
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = NULL;

            quitManualMode();
        }
    }
}
