#ifndef ONF_ALIGNEMENTPOINTCLUSTERDRAWMANAGER_H
#define ONF_ALIGNEMENTPOINTCLUSTERDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithpointclouddrawmanager.h"

class ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer;


class ONF_AlignementPointClusterDrawManager : public CT_StandardAbstractItemDrawableWithPointCloudDrawManager
{
public:
    ONF_AlignementPointClusterDrawManager(QString drawConfigurationName = "");
    virtual ~ONF_AlignementPointClusterDrawManager();

    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;

    void setLineVisible(bool visible);
    bool lineVisible();
    void setDataContainer(ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer *dataContainer, bool newCluster);

protected:
    const static QString INDEX_CONFIG_BARYCENTER_VISIBLE;

    static QString staticInitConfigBarycenterVisible();

    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;


private:
    bool            _lineVisible;
    bool            _newCluster;
    ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer   *_dataContainer;
    QColor          _unmatchedColor;


};

#endif // ONF_ALIGNEMENTPOINTCLUSTERDRAWMANAGER_H
