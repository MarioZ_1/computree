/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/onf_metricpointcrownshape.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

ONF_MetricPointCrownShape::ONF_MetricPointCrownShape() : CT_AbstractMetric_XYZ()
{
    declareAttributes();
}

ONF_MetricPointCrownShape::ONF_MetricPointCrownShape(const ONF_MetricPointCrownShape &other) : CT_AbstractMetric_XYZ(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
}

QString ONF_MetricPointCrownShape::getShortDescription() const
{
    return tr("Métriques de forme de houppier (LAS H ou LAS Z)");
}

QString ONF_MetricPointCrownShape::getDetailledDescription() const
{
    return tr("Les valeurs suivantes sont calculées :<br>"
              "- X_Apex<br>"
              "- Y_Apex<br>"
              "- Z_Apex<br>"
              "- NbPts_inf0_5m<br>"
              "- NbPts_inf1m<br>"
              "- NbPts_inf2m<br>"
              "- NbPts_inf3m<br>"
              "- NbPts_inf4m<br>"
              "- NbPts_inf5m<br>"
              "- MeanDistZ_inf0_5m<br>"
              "- MeanDistZ_inf1m<br>"
              "- MeanDistZ_inf2m<br>"
              "- MeanDistZ_inf3m<br>"
              "- MeanDistZ_inf4m<br>"
              "- MeanDistZ_inf5m<br>"
              "- MeanAngle_inf0_5m<br>"
              "- MeanAngle_inf1m<br>"
              "- MeanAngle_inf2m<br>"
              "- MeanAngle_inf3m<br>"
              "- MeanAngle_inf4m<br>"
              "- MeanAngle_inf5m<br>"
              "- MeanDistXY_inf0_5m<br>"
              "- MeanDistXY_inf1m<br>"
              "- MeanDistXY_inf2m<br>"
              "- MeanDistXY_inf3m<br>"
              "- MeanDistXY_inf4m<br>"
              "- MeanDistXY_inf5m<br>"
              "- DistXY_0_5m<br>"
              "- DistXY_1m<br>"
              "- DistXY_2m<br>"
              "- DistXY_3m<br>"
              "- DistXY_4m<br>"
              "- DistXY_5m<br>"
              "- Slope_0_5m<br>"
              "- Slope_1m<br>"
              "- Slope_2m<br>"
              "- Slope_3m<br>"
              "- Slope_4m<br>"
              "- Slope_5m<br>"
              "- Convexity_0_5m<br>"
              "- Convexity_1m<br>"
              "- Convexity_2m<br>"
              "- Convexity_3m<br>"
              "- Convexity_4m<br>"
              "- _AreaUnderCurve_1m<br>"
              "- _AreaUnderCurve_2m<br>"
              "- _AreaUnderCurve_3m<br>"
              "- _AreaUnderCurve_4m<br>"
              "- _AreaUnderCurve_5m<br>"
              );
}

ONF_MetricPointCrownShape::Config ONF_MetricPointCrownShape::metricConfiguration() const
{
    return m_configAndResults;
}

void ONF_MetricPointCrownShape::setMetricConfiguration(const ONF_MetricPointCrownShape::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *ONF_MetricPointCrownShape::copy() const
{
    return new ONF_MetricPointCrownShape(*this);
}

void ONF_MetricPointCrownShape::computeMetric()
{
    m_configAndResults._X_Apex.value = std::numeric_limits<double>::quiet_NaN();
    m_configAndResults._Y_Apex.value = std::numeric_limits<double>::quiet_NaN();
    m_configAndResults._Z_Apex.value = -std::numeric_limits<double>::max();
    m_configAndResults._NbPts_inf0_5m.value = 0;
    m_configAndResults._NbPts_inf1m.value = 0;
    m_configAndResults._NbPts_inf2m.value = 0;
    m_configAndResults._NbPts_inf3m.value = 0;
    m_configAndResults._NbPts_inf4m.value = 0;
    m_configAndResults._NbPts_inf5m.value = 0;
    m_configAndResults._MeanDistZ_inf0_5m.value = 0;
    m_configAndResults._MeanDistZ_inf1m.value = 0;
    m_configAndResults._MeanDistZ_inf2m.value = 0;
    m_configAndResults._MeanDistZ_inf3m.value = 0;
    m_configAndResults._MeanDistZ_inf4m.value = 0;
    m_configAndResults._MeanDistZ_inf5m.value = 0;
    m_configAndResults._MeanAngle_inf0_5m.value = 0;
    m_configAndResults._MeanAngle_inf1m.value = 0;
    m_configAndResults._MeanAngle_inf2m.value = 0;
    m_configAndResults._MeanAngle_inf3m.value = 0;
    m_configAndResults._MeanAngle_inf4m.value = 0;
    m_configAndResults._MeanAngle_inf5m.value = 0;
    m_configAndResults._MeanDistXY_inf0_5m.value = 0;
    m_configAndResults._MeanDistXY_inf1m.value = 0;
    m_configAndResults._MeanDistXY_inf2m.value = 0;
    m_configAndResults._MeanDistXY_inf3m.value = 0;
    m_configAndResults._MeanDistXY_inf4m.value = 0;
    m_configAndResults._MeanDistXY_inf5m.value = 0;
    m_configAndResults._DistXY_0_5m.value = 0;
    m_configAndResults._DistXY_1m.value = 0;
    m_configAndResults._DistXY_2m.value = 0;
    m_configAndResults._DistXY_3m.value = 0;
    m_configAndResults._DistXY_4m.value = 0;
    m_configAndResults._DistXY_5m.value = 0;
    m_configAndResults._Slope_0_5m.value = 0;
    m_configAndResults._Slope_1m.value = 0;
    m_configAndResults._Slope_2m.value = 0;
    m_configAndResults._Slope_3m.value = 0;
    m_configAndResults._Slope_4m.value = 0;
    m_configAndResults._Slope_5m.value = 0;
    m_configAndResults._Convexity_0_5m.value = 0;
    m_configAndResults._Convexity_1m.value = 0;
    m_configAndResults._Convexity_2m.value = 0;
    m_configAndResults._Convexity_3m.value = 0;
    m_configAndResults._Convexity_4m.value = 0;
    m_configAndResults._AreaUnderCurve_1m.value = 0;
    m_configAndResults._AreaUnderCurve_2m.value = 0;
    m_configAndResults._AreaUnderCurve_3m.value = 0;
    m_configAndResults._AreaUnderCurve_4m.value = 0;
    m_configAndResults._AreaUnderCurve_5m.value = 0;

    CT_PointIterator itP(pointCloud());
    while(itP.hasNext())
    {
        const CT_Point& point = itP.next().currentPoint();

        if ((plotArea() == NULL) || plotArea()->contains(point(0), point(1)))
        {
            if (point(2) > m_configAndResults._Z_Apex.value)
            {
                m_configAndResults._X_Apex.value = point(0);
                m_configAndResults._Y_Apex.value = point(1);
                m_configAndResults._Z_Apex.value = point(2);
            }
        }                
    }

    double MeanDXY_0_5m = 0;
    double MeanDXY_1m = 0;
    double MeanDXY_2m = 0;
    double MeanDXY_3m = 0;
    double MeanDXY_4m = 0;
    double MeanDXY_5m = 0;

    double MeanDXYcorr_0_5m = 0;
    double MeanDXYcorr_1m = 0;
    double MeanDXYcorr_2m = 0;
    double MeanDXYcorr_3m = 0;
    double MeanDXYcorr_4m = 0;
    double MeanDXYcorr_5m = 0;

    double SDpartDXY_0_5m = 0;
    double SDpartDXY_1m = 0;
    double SDpartDXY_2m = 0;
    double SDpartDXY_3m = 0;
    double SDpartDXY_4m = 0;
    double SDpartDXY_5m = 0;

    double SDDXY_0_5m = 0;
    double SDDXY_1m = 0;
    double SDDXY_2m = 0;
    double SDDXY_3m = 0;
    double SDDXY_4m = 0;
    double SDDXY_5m = 0;
    QList<double> distsXY_0_5m;
    QList<double> distsXY_1m;
    QList<double> distsXY_2m;
    QList<double> distsXY_3m;
    QList<double> distsXY_4m;
    QList<double> distsXY_5m;

    CT_PointIterator itP2(pointCloud());
    while(itP2.hasNext())
    {
        const CT_Point& point = itP2.next().currentPoint();

        if ((plotArea() == NULL) || plotArea()->contains(point(0), point(1)))
        {

            double distXY = sqrt(pow(m_configAndResults._X_Apex.value - point(0), 2) + pow(m_configAndResults._Y_Apex.value - point(1), 2));
            double distZ = m_configAndResults._Z_Apex.value - point(2);
            double angleRad = 0;
            if (distZ > 0) {angleRad = std::atan(distXY / distZ);}
            double angleDegrees = angleRad * 180.0 / M_PI;

            // Calcul des indicateurs simples par distance Z
            if (distZ < 0.5)
            {
                m_configAndResults._NbPts_inf0_5m.value += 1.0;
                m_configAndResults._MeanDistZ_inf0_5m.value += distZ;
                m_configAndResults._MeanAngle_inf0_5m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf0_5m.value += distXY;
            }
            if (distZ < 1)
            {
                m_configAndResults._NbPts_inf1m.value += 1.0;
                m_configAndResults._MeanDistZ_inf1m.value += distZ;
                m_configAndResults._MeanAngle_inf1m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf1m.value += distXY;
            }
            if (distZ < 2)
            {
                m_configAndResults._NbPts_inf2m.value += 1.0;
                m_configAndResults._MeanDistZ_inf2m.value += distZ;
                m_configAndResults._MeanAngle_inf2m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf2m.value += distXY;
            }
            if (distZ < 3)
            {
                m_configAndResults._NbPts_inf3m.value += 1.0;
                m_configAndResults._MeanDistZ_inf3m.value += distZ;
                m_configAndResults._MeanAngle_inf3m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf3m.value += distXY;
            }
            if (distZ < 4)
            {
                m_configAndResults._NbPts_inf4m.value += 1.0;
                m_configAndResults._MeanDistZ_inf4m.value += distZ;
                m_configAndResults._MeanAngle_inf4m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf4m.value += distXY;
            }
            if (distZ < 5)
            {
                m_configAndResults._NbPts_inf5m.value += 1.0;
                m_configAndResults._MeanDistZ_inf5m.value += distZ;
                m_configAndResults._MeanAngle_inf5m.value += angleDegrees;
                m_configAndResults._MeanDistXY_inf5m.value += distXY;
            }


            // Calculs des moyennes et écarts-types des distances XY par tranche

                if (distZ >= 0.25 && distZ < 0.75)
                {
                    MeanDXY_0_5m += distXY;
                    SDpartDXY_0_5m += distXY*distXY;
                    distsXY_0_5m.append(distXY);
                } else if (distZ >= 0.75 && distZ < 1.25)
                {
                    MeanDXY_1m += distXY;
                    SDpartDXY_1m += distXY*distXY;
                    distsXY_1m.append(distXY);
                } else if (distZ >= 1.50 && distZ < 2.50)
                {
                    MeanDXY_2m += distXY;
                    SDpartDXY_2m += distXY*distXY;
                    distsXY_2m.append(distXY);
                } else if (distZ >= 2.50 && distZ < 3.50)
                {
                    MeanDXY_3m += distXY;
                    SDpartDXY_3m += distXY*distXY;
                    distsXY_3m.append(distXY);
                } else if (distZ >= 3.50 && distZ < 4.50)
                {
                    MeanDXY_4m += distXY;
                    SDpartDXY_4m += distXY*distXY;
                    distsXY_4m.append(distXY);
                } else if (distZ >= 4.50 && distZ < 5.50)
                {
                    MeanDXY_5m += distXY;
                    SDpartDXY_5m += distXY*distXY;
                    distsXY_5m.append(distXY);
                }
        }
    }

    // Finalisation des calculs des indicateurs simples par distance Z
    if (m_configAndResults._NbPts_inf0_5m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf0_5m.value /= m_configAndResults._NbPts_inf0_5m.value;
        m_configAndResults._MeanAngle_inf0_5m.value /= m_configAndResults._NbPts_inf0_5m.value;
        m_configAndResults._MeanDistXY_inf0_5m.value /= m_configAndResults._NbPts_inf0_5m.value;
    }
    if (m_configAndResults._NbPts_inf1m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf1m.value /= m_configAndResults._NbPts_inf1m.value;
        m_configAndResults._MeanAngle_inf1m.value /= m_configAndResults._NbPts_inf1m.value;
        m_configAndResults._MeanDistXY_inf1m.value /= m_configAndResults._NbPts_inf1m.value;
    }
    if (m_configAndResults._NbPts_inf2m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf2m.value /= m_configAndResults._NbPts_inf2m.value;
        m_configAndResults._MeanAngle_inf2m.value /= m_configAndResults._NbPts_inf2m.value;
        m_configAndResults._MeanDistXY_inf2m.value /= m_configAndResults._NbPts_inf2m.value;
    }
    if (m_configAndResults._NbPts_inf3m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf3m.value /= m_configAndResults._NbPts_inf3m.value;
        m_configAndResults._MeanAngle_inf3m.value /= m_configAndResults._NbPts_inf3m.value;
        m_configAndResults._MeanDistXY_inf3m.value /= m_configAndResults._NbPts_inf3m.value;
    }
    if (m_configAndResults._NbPts_inf4m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf4m.value /= m_configAndResults._NbPts_inf4m.value;
        m_configAndResults._MeanAngle_inf4m.value /= m_configAndResults._NbPts_inf4m.value;
        m_configAndResults._MeanDistXY_inf4m.value /= m_configAndResults._NbPts_inf4m.value;
    }
    if (m_configAndResults._NbPts_inf5m.value > 0)
    {
        m_configAndResults._MeanDistZ_inf5m.value /= m_configAndResults._NbPts_inf5m.value;
        m_configAndResults._MeanAngle_inf5m.value /= m_configAndResults._NbPts_inf5m.value;
        m_configAndResults._MeanDistXY_inf5m.value /= m_configAndResults._NbPts_inf5m.value;
    }




    // Finalisation des calculs des moyennes et écarts-types des distances XY par tranche
    if (distsXY_0_5m.size() > 0)
    {
        MeanDXY_0_5m /= distsXY_0_5m.size();
        SDDXY_0_5m = std::sqrt(SDpartDXY_0_5m/distsXY_0_5m.size() - MeanDXY_0_5m*MeanDXY_0_5m);

        int nval = 0;
        for (int i = 0 ; i < distsXY_0_5m.size() ; i++)
        {
            double disti = distsXY_0_5m.at(i);
            if (disti > (MeanDXY_0_5m - SDDXY_0_5m) && disti < (MeanDXY_0_5m + SDDXY_0_5m))
            {
                MeanDXYcorr_0_5m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_0_5m /= nval;}
    }
    if (distsXY_1m.size() > 0)
    {
        MeanDXY_1m /= distsXY_1m.size();
        SDDXY_1m = std::sqrt(SDpartDXY_1m/distsXY_1m.size() - MeanDXY_1m*MeanDXY_1m);
        int nval = 0;
        for (int i = 0 ; i < distsXY_1m.size() ; i++)
        {
            double disti = distsXY_1m.at(i);
            if (disti > (MeanDXY_1m - SDDXY_1m) && disti < (MeanDXY_1m + SDDXY_1m))
            {
                MeanDXYcorr_1m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_1m /= nval;}
    }
    if (distsXY_2m.size() > 0)
    {
        MeanDXY_2m /= distsXY_2m.size();
        SDDXY_2m = std::sqrt(SDpartDXY_2m/distsXY_2m.size() - MeanDXY_2m*MeanDXY_2m);

        int nval = 0;
        for (int i = 0 ; i < distsXY_2m.size() ; i++)
        {
            double disti = distsXY_2m.at(i);
            if (disti > (MeanDXY_2m - SDDXY_2m) && disti < (MeanDXY_2m + SDDXY_2m))
            {
                MeanDXYcorr_2m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_2m /= nval;}
    }
    if (distsXY_3m.size() > 0)
    {
        MeanDXY_3m /= distsXY_3m.size();
        SDDXY_3m = std::sqrt(SDpartDXY_3m/distsXY_3m.size() - MeanDXY_3m*MeanDXY_3m);

        int nval = 0;
        for (int i = 0 ; i < distsXY_3m.size() ; i++)
        {
            double disti = distsXY_3m.at(i);
            if (disti > (MeanDXY_3m - SDDXY_3m) && disti < (MeanDXY_3m + SDDXY_3m))
            {
                MeanDXYcorr_3m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_3m /= nval;}
    }
    if (distsXY_4m.size() > 0)
    {
        MeanDXY_4m /= distsXY_4m.size();
        SDDXY_4m = std::sqrt(SDpartDXY_4m/distsXY_4m.size() - MeanDXY_4m*MeanDXY_4m);

        int nval = 0;
        for (int i = 0 ; i < distsXY_4m.size() ; i++)
        {
            double disti = distsXY_4m.at(i);
            if (disti > (MeanDXY_4m - SDDXY_4m) && disti < (MeanDXY_4m + SDDXY_4m))
            {
                MeanDXYcorr_4m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_4m /= nval;}
    }
    if (distsXY_5m.size() > 0)
    {
        MeanDXY_5m /= distsXY_5m.size();
        SDDXY_5m = std::sqrt(SDpartDXY_5m/distsXY_5m.size() - MeanDXY_5m*MeanDXY_5m);

        int nval = 0;
        for (int i = 0 ; i < distsXY_5m.size() ; i++)
        {
            double disti = distsXY_5m.at(i);
            if (disti > (MeanDXY_5m - SDDXY_5m) && disti < (MeanDXY_5m + SDDXY_5m))
            {
                MeanDXYcorr_5m += disti;
                nval++;
            }
        }
        if (nval > 0) {MeanDXYcorr_5m /= nval;}
    }

    m_configAndResults._DistXY_0_5m.value = MeanDXY_0_5m;
    m_configAndResults._DistXY_1m.value   = MeanDXY_1m;
    m_configAndResults._DistXY_2m.value   = MeanDXY_2m;
    m_configAndResults._DistXY_3m.value   = MeanDXY_3m;
    m_configAndResults._DistXY_4m.value   = MeanDXY_4m;
    m_configAndResults._DistXY_5m.value   = MeanDXY_5m;

    m_configAndResults._Slope_0_5m.value = std::atan((MeanDXYcorr_0_5m - 0.0)              / (0.5 - 0.0));
    m_configAndResults._Slope_1m.value =   std::atan((MeanDXYcorr_1m   - MeanDXYcorr_0_5m) / (1.0 - 0.5));
    m_configAndResults._Slope_2m.value =   std::atan((MeanDXYcorr_2m   - MeanDXYcorr_1m)   / (2.0 - 1.0));
    m_configAndResults._Slope_3m.value =   std::atan((MeanDXYcorr_3m   - MeanDXYcorr_2m)   / (3.0 - 2.0));
    m_configAndResults._Slope_4m.value =   std::atan((MeanDXYcorr_4m   - MeanDXYcorr_3m)   / (4.0 - 3.0));
    m_configAndResults._Slope_5m.value =   std::atan((MeanDXYcorr_5m   - MeanDXYcorr_4m)   / (5.0 - 4.0));

    m_configAndResults._Convexity_0_5m.value = std::atan((m_configAndResults._Slope_1m.value - m_configAndResults._Slope_0_5m.value) / (0.75 - 0.25));
    m_configAndResults._Convexity_1m.value =   std::atan((m_configAndResults._Slope_2m.value - m_configAndResults._Slope_1m.value)   / (1.25 - 0.75));
    m_configAndResults._Convexity_2m.value =   std::atan((m_configAndResults._Slope_3m.value - m_configAndResults._Slope_2m.value)   / (2.50 - 1.50));
    m_configAndResults._Convexity_3m.value =   std::atan((m_configAndResults._Slope_4m.value - m_configAndResults._Slope_3m.value)   / (3.50 - 2.50));
    m_configAndResults._Convexity_4m.value =   std::atan((m_configAndResults._Slope_5m.value - m_configAndResults._Slope_4m.value)   / (4.50 - 3.50));

    m_configAndResults._Slope_0_5m.value *= (180.0/M_PI);
    m_configAndResults._Slope_1m.value   *= (180.0/M_PI);
    m_configAndResults._Slope_2m.value   *= (180.0/M_PI);
    m_configAndResults._Slope_3m.value   *= (180.0/M_PI);
    m_configAndResults._Slope_4m.value   *= (180.0/M_PI);
    m_configAndResults._Slope_5m.value   *= (180.0/M_PI);

    if (MeanDXYcorr_1m > 0) {m_configAndResults._AreaUnderCurve_1m.value = (MeanDXYcorr_0_5m + 0.5*MeanDXYcorr_1m) / (1.0*MeanDXYcorr_1m);}
    if (MeanDXYcorr_2m > 0) {m_configAndResults._AreaUnderCurve_2m.value = (MeanDXYcorr_0_5m + 1.5*MeanDXYcorr_1m + 1.0*MeanDXYcorr_2m) / (2.0*MeanDXYcorr_2m);}
    if (MeanDXYcorr_3m > 0) {m_configAndResults._AreaUnderCurve_3m.value = (MeanDXYcorr_0_5m + 1.5*MeanDXYcorr_1m + 2.0*MeanDXYcorr_2m + 1.0*MeanDXYcorr_3m) / (3.0*MeanDXYcorr_3m);}
    if (MeanDXYcorr_4m > 0) {m_configAndResults._AreaUnderCurve_4m.value = (MeanDXYcorr_0_5m + 1.5*MeanDXYcorr_1m + 2.0*MeanDXYcorr_2m + 2.0*MeanDXYcorr_3m + 1.0*MeanDXYcorr_4m) / (4.0*MeanDXYcorr_4m);}
    if (MeanDXYcorr_5m > 0) {m_configAndResults._AreaUnderCurve_5m.value = (MeanDXYcorr_0_5m + 1.5*MeanDXYcorr_1m + 2.0*MeanDXYcorr_2m + 2.0*MeanDXYcorr_3m + 2.0*MeanDXYcorr_4m + 1.0*MeanDXYcorr_5m) / (5.0*MeanDXYcorr_5m);}

    setAttributeValueVaB(m_configAndResults._X_Apex);
    setAttributeValueVaB(m_configAndResults._Y_Apex);
    setAttributeValueVaB(m_configAndResults._Z_Apex);

    setAttributeValueVaB(m_configAndResults._NbPts_inf0_5m);
    setAttributeValueVaB(m_configAndResults._NbPts_inf1m);
    setAttributeValueVaB(m_configAndResults._NbPts_inf2m);
    setAttributeValueVaB(m_configAndResults._NbPts_inf3m);
    setAttributeValueVaB(m_configAndResults._NbPts_inf4m);
    setAttributeValueVaB(m_configAndResults._NbPts_inf5m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf0_5m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf1m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf2m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf3m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf4m);
    setAttributeValueVaB(m_configAndResults._MeanDistZ_inf5m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf0_5m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf1m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf2m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf3m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf4m);
    setAttributeValueVaB(m_configAndResults._MeanAngle_inf5m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf0_5m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf1m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf2m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf3m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf4m);
    setAttributeValueVaB(m_configAndResults._MeanDistXY_inf5m);

    setAttributeValueVaB(m_configAndResults._DistXY_0_5m);
    setAttributeValueVaB(m_configAndResults._DistXY_1m);
    setAttributeValueVaB(m_configAndResults._DistXY_2m);
    setAttributeValueVaB(m_configAndResults._DistXY_3m);
    setAttributeValueVaB(m_configAndResults._DistXY_4m);
    setAttributeValueVaB(m_configAndResults._DistXY_5m);
    setAttributeValueVaB(m_configAndResults._Slope_0_5m);
    setAttributeValueVaB(m_configAndResults._Slope_1m);
    setAttributeValueVaB(m_configAndResults._Slope_2m);
    setAttributeValueVaB(m_configAndResults._Slope_3m);
    setAttributeValueVaB(m_configAndResults._Slope_4m);
    setAttributeValueVaB(m_configAndResults._Slope_5m);
    setAttributeValueVaB(m_configAndResults._Convexity_0_5m);
    setAttributeValueVaB(m_configAndResults._Convexity_1m);
    setAttributeValueVaB(m_configAndResults._Convexity_2m);
    setAttributeValueVaB(m_configAndResults._Convexity_3m);
    setAttributeValueVaB(m_configAndResults._Convexity_4m);
    setAttributeValueVaB(m_configAndResults._AreaUnderCurve_1m);
    setAttributeValueVaB(m_configAndResults._AreaUnderCurve_2m);
    setAttributeValueVaB(m_configAndResults._AreaUnderCurve_3m);
    setAttributeValueVaB(m_configAndResults._AreaUnderCurve_4m);
    setAttributeValueVaB(m_configAndResults._AreaUnderCurve_5m);
}

void ONF_MetricPointCrownShape::declareAttributes()
{    
    registerAttributeVaB(m_configAndResults._X_Apex, CT_AbstractCategory::DATA_NUMBER, tr("X_Apex"));
    registerAttributeVaB(m_configAndResults._Y_Apex, CT_AbstractCategory::DATA_NUMBER, tr("Y_Apex"));
    registerAttributeVaB(m_configAndResults._Z_Apex, CT_AbstractCategory::DATA_NUMBER, tr("Z_Apex"));
    registerAttributeVaB(m_configAndResults._NbPts_inf0_5m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf0_5m"));
    registerAttributeVaB(m_configAndResults._NbPts_inf1m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf1m"));
    registerAttributeVaB(m_configAndResults._NbPts_inf2m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf2m"));
    registerAttributeVaB(m_configAndResults._NbPts_inf3m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf3m"));
    registerAttributeVaB(m_configAndResults._NbPts_inf4m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf4m"));
    registerAttributeVaB(m_configAndResults._NbPts_inf5m, CT_AbstractCategory::DATA_NUMBER, tr("NbPts_inf5m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf0_5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf0_5m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf1m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf1m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf2m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf2m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf3m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf3m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf4m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf4m"));
    registerAttributeVaB(m_configAndResults._MeanDistZ_inf5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistZ_inf5m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf0_5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf0_5m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf1m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf1m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf2m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf2m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf3m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf3m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf4m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf4m"));
    registerAttributeVaB(m_configAndResults._MeanAngle_inf5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanAngle_inf5m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf0_5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf0_5m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf1m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf1m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf2m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf2m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf3m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf3m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf4m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf4m"));
    registerAttributeVaB(m_configAndResults._MeanDistXY_inf5m, CT_AbstractCategory::DATA_NUMBER, tr("MeanDistXY_inf5m"));
    registerAttributeVaB(m_configAndResults._DistXY_0_5m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_0_5m"));
    registerAttributeVaB(m_configAndResults._DistXY_1m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_1m"));
    registerAttributeVaB(m_configAndResults._DistXY_2m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_2m"));
    registerAttributeVaB(m_configAndResults._DistXY_3m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_3m"));
    registerAttributeVaB(m_configAndResults._DistXY_4m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_4m"));
    registerAttributeVaB(m_configAndResults._DistXY_5m, CT_AbstractCategory::DATA_NUMBER, tr("DistXY_5m"));
    registerAttributeVaB(m_configAndResults._Slope_0_5m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_0_5m"));
    registerAttributeVaB(m_configAndResults._Slope_1m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_1m"));
    registerAttributeVaB(m_configAndResults._Slope_2m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_2m"));
    registerAttributeVaB(m_configAndResults._Slope_3m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_3m"));
    registerAttributeVaB(m_configAndResults._Slope_4m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_4m"));
    registerAttributeVaB(m_configAndResults._Slope_5m, CT_AbstractCategory::DATA_NUMBER, tr("Slope_5m"));
    registerAttributeVaB(m_configAndResults._Convexity_0_5m, CT_AbstractCategory::DATA_NUMBER, tr("Convexity_0_5m"));
    registerAttributeVaB(m_configAndResults._Convexity_1m, CT_AbstractCategory::DATA_NUMBER, tr("Convexity_1m"));
    registerAttributeVaB(m_configAndResults._Convexity_2m, CT_AbstractCategory::DATA_NUMBER, tr("Convexity_2m"));
    registerAttributeVaB(m_configAndResults._Convexity_3m, CT_AbstractCategory::DATA_NUMBER, tr("Convexity_3m"));
    registerAttributeVaB(m_configAndResults._Convexity_4m, CT_AbstractCategory::DATA_NUMBER, tr("Convexity_4m"));
    registerAttributeVaB(m_configAndResults._AreaUnderCurve_1m, CT_AbstractCategory::DATA_NUMBER, tr("AreaUnderCurve_1m"));
    registerAttributeVaB(m_configAndResults._AreaUnderCurve_2m, CT_AbstractCategory::DATA_NUMBER, tr("AreaUnderCurve_2m"));
    registerAttributeVaB(m_configAndResults._AreaUnderCurve_3m, CT_AbstractCategory::DATA_NUMBER, tr("AreaUnderCurve_3m"));
    registerAttributeVaB(m_configAndResults._AreaUnderCurve_4m, CT_AbstractCategory::DATA_NUMBER, tr("AreaUnderCurve_4m"));
    registerAttributeVaB(m_configAndResults._AreaUnderCurve_5m, CT_AbstractCategory::DATA_NUMBER, tr("AreaUnderCurve_5m"));

}

