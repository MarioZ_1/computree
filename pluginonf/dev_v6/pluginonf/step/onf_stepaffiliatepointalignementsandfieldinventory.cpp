/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepaffiliatepointalignementsandfieldinventory.h"

#include "tools/onf_alignementpointclusterdrawmanager.h"

#include <QMessageBox>

ONF_StepAffiliatePointAlignementsAndFieldInventory::ONF_StepAffiliatePointAlignementsAndFieldInventory() : SuperClass()
{
    _distThrehold = 3.0;
    _interactiveCorrection = true;

    _dataContainer = nullptr;

    setManual(true);

    _m_status = 0;
    _m_doc = nullptr;
}

ONF_StepAffiliatePointAlignementsAndFieldInventory::~ONF_StepAffiliatePointAlignementsAndFieldInventory()
{
}

QString ONF_StepAffiliatePointAlignementsAndFieldInventory::description() const
{
    return tr("Apparier alignements et positions terrain");
}

QString ONF_StepAffiliatePointAlignementsAndFieldInventory::detailledDescription() const
{
    return tr("To Do");
}

CT_VirtualAbstractStep* ONF_StepAffiliatePointAlignementsAndFieldInventory::createNewInstance() const
{
    return new ONF_StepAffiliatePointAlignementsAndFieldInventory();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepAffiliatePointAlignementsAndFieldInventory::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inPlot = createNewInResultModelForCopy(_inResPlot, tr("Placette"), "", true);
    res_inPlot->setZeroOrMoreRootGroup();
    res_inPlot->addGroupModel("", _inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inPlot->addItemModel(_inGrp, _inRef, CT_Circle2D::staticGetType(), tr("Arbre"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefDbh, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, tr("DBH"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefHeight, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, tr("Height"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefID, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::STRING, tr("IDtree"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefIDplot, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::STRING, tr("IDplot"));
    res_inPlot->addItemAttributeModel(_inRef, _inSpecies, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Species"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);

    CT_InResultModelGroup *res_inAlignements = createNewInResultModel(_inResAlignements, tr("Alignements"), "", true);
    res_inAlignements->setZeroOrMoreRootGroup();
    res_inAlignements->addGroupModel("", _inGrpAlignements, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inAlignements->addItemModel(_inGrpAlignements, _inAlignements, CT_PointCluster::staticGetType(), tr("Alignement"));

    CT_InResultModelGroup *resultScene = createNewInResultModel(_inResSc, tr("Scène"), "", true);
    resultScene->setZeroOrMoreRootGroup();
    resultScene->addGroupModel("", _inScGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultScene->addItemModel(_inScGrp, _inSc, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène"));

    CT_InResultModelGroup *resultScene2 = createNewInResultModel(_inResSc2, tr("Scène2"), "", true);
    resultScene2->setZeroOrMoreRootGroup();
    resultScene2->addGroupModel("", _inScGrp2, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultScene2->addItemModel(_inScGrp2, _inSc2, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène2"));

    CT_InResultModelGroup *resultDTM = createNewInResultModel(_inResDTM, tr("MNT"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", _inDTMGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(_inDTMGrp, _inDTM, CT_Image2D<float>::staticGetType(), tr("MNT"));
    resultDTM->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);

}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(_inResPlot);

    if(res != nullptr)
    {
        res->addItemModel(_inGrp, _outSceneModelName, new CT_PointCluster(), tr("Cluster arbre"));
        res->addItemAttributeModel(_outSceneModelName, _outSceneIDClusterAttModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID), tr("IDCluster"));
        res->addItemAttributeModel(_outSceneModelName, _outSceneCommentAttModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("Comment"));
    }
}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Distance d'appariement maximale :"), "m", 0, std::numeric_limits<double>::max(), 2, _distThrehold);
    postInputConfigDialog->addBool(tr("Correction interactive"), "", "", _interactiveCorrection);
}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::compute()
{
    setManual(_interactiveCorrection);

    _dataContainer = new ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer();

    CT_ResultGroup* res_inScene = getInputResults().at(2);
    CT_ResultItemIterator itSC(res_inScene, this, _inSc);
    if (itSC.hasNext())
    {
        _dataContainer->_scenes.append( (CT_AbstractItemDrawableWithPointCloud*)itSC.next());
    }

    CT_ResultGroup* res_inScene2 = getInputResults().at(3);
    CT_ResultItemIterator itSC2(res_inScene2, this, _inSc2);
    if (itSC2.hasNext())
    {
        _dataContainer->_scenes2.append( (CT_AbstractItemDrawableWithPointCloud*)itSC2.next());
    }

    CT_Image2D<float>* dtm = nullptr;
    if (getInputResults().size() > 4)
    {
        CT_ResultGroup* res_inDTM = getInputResults().at(4);
        CT_ResultItemIterator itDTM(res_inDTM, this, _inDTM);
        if (itDTM.hasNext())
        {
            dtm = (CT_Image2D<float>*) itDTM.next();
        }
    }

    _m_status = 0;
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* res_inAlignements = inResultList.at(1);

   _dataContainer->_drawManager = new ONF_AlignementPointClusterDrawManager();
   _dataContainer->_newClusterDrawManager = new ONF_AlignementPointClusterDrawManager("Added Points");

    CT_ResultGroupIterator itGrpSc(res_inAlignements, this, _inGrpAlignements);
    while (itGrpSc.hasNext())
    {
        const CT_AbstractItemGroup* grp = itGrpSc.next();

        CT_PointCluster* alignement = (CT_PointCluster*) grp->firstItemByINModelName(this, _inAlignements);

        if (alignement != nullptr)
        {
            _dataContainer->_alignements.append(alignement);

            alignement->setAlternativeDrawManager(_dataContainer->_drawManager);
        }
    }

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_positions = outResultList.at(0);

    _dataContainer->_result = resOut_positions;

    CT_ResultGroupIterator itGrp(resOut_positions, this, _inGrp);
    while (itGrp.hasNext())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itGrp.next();

        CT_Circle2D* circle = (CT_Circle2D*) grp->firstItemByINModelName(this, _inRef);
        if (circle != nullptr)
        {
            ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition* treePos = new ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition();
            treePos->_base(0) = circle->centerX();
            treePos->_base(1) = circle->centerY();
            treePos->_base(2) = 0;

            if (dtm != nullptr)
            {
                treePos->_base(2) = dtm->valueAtCoords(treePos->_base(0), treePos->_base(1));
                if (treePos->_base(2) == dtm->NA()) {treePos->_base(2) = 0;}
            }

            CT_AbstractItemAttribute* att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefDbh);
            if (att != nullptr) {treePos->_dbh = att->toFloat(circle, nullptr);}
            if (treePos->_dbh <= 0) {treePos->_dbh = 7.5;}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefHeight);
            if (att != nullptr) {treePos->_height = att->toFloat(circle, nullptr);}
            if (treePos->_height <= 0) {treePos->_height = -1;}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefID);
            if (att != nullptr) {treePos->_idTree = att->toString(circle, nullptr);}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefIDplot);
            if (att != nullptr) {treePos->_idPlot = att->toString(circle, nullptr);}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inSpecies);
            if (att != nullptr) {treePos->_species = att->toString(circle, nullptr);}

            _dataContainer->_positions.append(treePos);
            treePos->_grp = grp;
        }
    }

    // Tri par ordre décroissant de diamètre
    qSort(_dataContainer->_positions.begin(), _dataContainer->_positions.end(), ONF_StepAffiliatePointAlignementsAndFieldInventory::lessThan);

    positionMatching(_dataContainer);

    if (_interactiveCorrection)
    {
        requestManualMode();
        _m_status = 1;
    }

    for (int i = 0 ; i < _dataContainer->_positions.size() ; i++)
    {
        ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition* treePos = _dataContainer->_positions.at(i);

        if (treePos->_grp != nullptr)
        {
            // Création du cluster
            CT_PointCluster *outCluster = new CT_PointCluster(_outSceneModelName.completeName(), resOut_positions);

            // Fusion des scènes associées
            for (int i = 0 ; i < treePos->_alignementsIds.size() ; i++)
            {
                int id = treePos->_alignementsIds.at(i);
                CT_PointCluster* alignement = (CT_PointCluster*) _dataContainer->_alignements.at(id);
                if (alignement != nullptr)
                {
                    const CT_AbstractPointCloudIndex *pointCloudIndex = alignement->pointCloudIndex();
                    CT_PointIterator itP(pointCloudIndex);
                    while(itP.hasNext())
                    {
                        size_t index = itP.next().currentGlobalIndex();

                        if (_dataContainer->_newCluster[id] || !_dataContainer->_droppedPointsIds.contains(index))
                        {
                            outCluster->addPoint(index);
                        }
                    }
                }
            }

            if (outCluster->getPointCloudIndexSize() > 0)
            {
                // ajout du cluster

                QString sceneName = QString("%1_%2").arg(treePos->_idPlot).arg(treePos->_idTree);
                outCluster->addItemAttribute(new CT_StdItemAttributeT<QString>(_outSceneIDClusterAttModelName.completeName(), CT_AbstractCategory::DATA_ID, resOut_positions, sceneName));
                outCluster->addItemAttribute(new CT_StdItemAttributeT<QString>(_outSceneCommentAttModelName.completeName(), CT_AbstractCategory::DATA_VALUE, resOut_positions, treePos->_rmq));

                treePos->_grp->addSingularItem(outCluster);

            } else {
                delete outCluster;
            }
        }

        delete treePos;
    }

    for (int i = 0 ; i < _dataContainer->_alignements.size() ; i++)
    {
        _dataContainer->_alignements[i]->setAlternativeDrawManager(nullptr);
    }

    if (_interactiveCorrection)
    {
        requestManualMode();
    }

    qDeleteAll(_dataContainer->_addedAlignements.values());

    delete _dataContainer->_drawManager;
    delete _dataContainer->_newClusterDrawManager;

    delete _dataContainer;

}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::positionMatching(ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer* dataContainer)
{
    dataContainer->_matched.resize(dataContainer->_alignements.size());
    dataContainer->_matched.fill(false);

    dataContainer->_newCluster.resize(dataContainer->_alignements.size());
    dataContainer->_newCluster.fill(false);

    for (int ap = 0 ; ap < dataContainer->_alignements.size() ; ap++)
    {
        CT_PointCluster* alignement = (CT_PointCluster*) dataContainer->_alignements.at(ap);
        const CT_PointClusterBarycenter& bary = alignement->getBarycenter();

        ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition* bestPos = nullptr;
        double bestPosDist = std::numeric_limits<double>::max();

        for (int pos = 0 ; pos < dataContainer->_positions.size() ; pos++)
        {
            ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition* treePos = dataContainer->_positions.at(pos);

            double dist = sqrt(pow(treePos->_base(0) - bary.x(), 2) + pow(treePos->_base(1) - bary.y(), 2));

            if (dist < _distThrehold && dist < bestPosDist)
            {
                bestPosDist = dist;
                bestPos = treePos;
            }
        }

        if (bestPos != nullptr)
        {
            bestPos->addAlignement(ap);
            dataContainer->_matched[ap] = true;
        }

    }

}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::initManualMode()
{
    if(_m_doc == nullptr)
    {
        // create a new 3D document
        QColor col = Qt::black;
        _m_doc = getGuiContext()->documentManager()->new3DDocument(2.0, false, &col);

        ONF_ActionAffiliatePointAlignementsAndFieldInventory* action = new ONF_ActionAffiliatePointAlignementsAndFieldInventory(_dataContainer);

        // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
        _m_doc->setCurrentAction(action, false);
    }

    //    QMessageBox::information(nullptr,
    //                             tr("Mode manuel"),
    //                             tr("Bienvenue dans le mode manuel de cette étape.\n"),
    //                             QMessageBox::Ok);
}

void ONF_StepAffiliatePointAlignementsAndFieldInventory::useManualMode(bool quit)
{
    if(_m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(_m_status == 1)
    {
        if(!quit)
        {
            _m_doc = nullptr;
            quitManualMode();
        }
    }

}
