/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPFITANDFILTERCYLINDERSINSECTIONS_H
#define ONF_STEPFITANDFILTERCYLINDERSINSECTIONS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_circle.h"

class ONF_StepFitAndFilterCylindersInSections : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFitAndFilterCylindersInSections();

    QString description() const;

    QString detailledDescription() const;

    QStringList getStepRISCitations() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double                  _max_error;                 /*!< Erreur maximale sur l'ajustement de cylindres*/
    double                  _max_relative_error;        /*!< Erreur maximale sur l'ajustement de cylindres en % du diamètre*/
    double                  _min_radius;                /*!< Rayon minimal des cylindres ajustés*/
    double                  _max_radius;                /*!< Rayon maximal des cylindres ajustés*/
    bool                    _activeFiltering;           /*!< Faut-il filter les cylindres sur l'erreur ?*/
    bool                    _activeFilteringRelative;   /*!< Faut-il filter les cylindres sur l'erreur (erreur en %) ?*/
    bool                    _activeFilteringVerticality;/*!< Faut-il filter les cylindres sur la verticalité ?*/
    double                  _phi_max;                   /*!< Angle zénithal maximal*/
    bool                    _activeLinesFiltering;           /*!< Faut-il filter les cylindres sur l'erreur ?*/
    double                  _max_line_error;                 /*!< Erreur maximale sur l'ajustement de cylindres*/


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inSectionGroup;
    CT_HandleInStdGroup<>                                                               _inClusterGroup;
    CT_HandleInSingularItem<CT_PointCluster>                                            _inCluster;
    CT_HandleInSingularItem<CT_ReferencePoint>                                          _inRefPoint;

    CT_HandleOutSingularItem<CT_Cylinder>                                               _outCylinder;

};

#endif // ONF_STEPFITANDFILTERCYLINDERSINSECTIONS_H
