/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPEXPORTRASTERSINTABLE_H
#define ONF_STEPEXPORTRASTERSINTABLE_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_itemdrawable/abstract/ct_abstractareashape2d.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_itemdrawable/ct_image2d.h"

#include <QFile>

class ONF_StepExportRastersInTable : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepExportRastersInTable();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    QStringList     _fileName;
    bool            _noprefix;
    QFile           _outFile;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractImage2D, 1, -1>                                  _inRaster;

    CT_HandleInResultGroup<0,1>                                                         _inResultFootprint;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupFootprint;
    CT_HandleInStdGroup<>                                                               _inGroupFootprint;
    CT_HandleInSingularItem<CT_AbstractAreaShape2D>                                     _inFootprint;

    CT_HandleInResultGroup<0,1>                                                         _inResultCounter;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupCounter;
    CT_HandleInStdGroup<>                                                               _inGroupCounter;
    CT_HandleInSingularItem<CT_LoopCounter>                                             _inCounter;
};

#endif // ONF_STEPEXPORTRASTERSINTABLE_H
