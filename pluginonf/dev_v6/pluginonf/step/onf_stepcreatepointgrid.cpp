/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreatepointgrid.h"

#include "ct_log/ct_logmanager.h"

#include "ct_view/ct_buttongroup.h"

#include <math.h>
#include <iostream>
#include <QList>

ONF_StepCreatePointGrid::ONF_StepCreatePointGrid() : SuperClass()
{
    _resolution   = 0.25;
}

QString ONF_StepCreatePointGrid::description() const
{
    return tr("Create point voxel grid");
}

QString ONF_StepCreatePointGrid::detailledDescription() const
{
    return tr("Create a grid, where each cell store the list of point indices contained in this cell. ");
}

CT_VirtualAbstractStep* ONF_StepCreatePointGrid::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepCreatePointGrid();
}

//////////////////// PROTECTED //////////////////

void ONF_StepCreatePointGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
}

// Création et affiliation des modèles OUT
void ONF_StepCreatePointGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outPointGrid, tr("Grille de points"));
}

void ONF_StepCreatePointGrid::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution :"), "m", 0, std::numeric_limits<double>::max(), 2, _resolution);
}

void ONF_StepCreatePointGrid::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t n_points = pointCloudIndex->size();

            CT_Grid3D_Points* grid = CT_Grid3D_Points::createGrid3DFromXYZCoords(inScene->minX() - 1.0, inScene->minY() - 1.0, inScene->minZ() - 1.0,
                                                                inScene->maxX() + 1.0, inScene->maxY() + 1.0, inScene->maxZ() + 1.0,
                                                                _resolution, false);

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène d'entrée comporte %1 points.")).arg(n_points));

            int i = 0;
            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point &pt = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                grid->addPoint(index, pt(0), pt(1), pt(2));

                // progres de 0 à 100
                setProgress(100.0*i/n_points);
                ++i;
            }

            group->addSingularItem(_outPointGrid, grid);
        }
    }

}
