/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreateplotsfromlist.h"

#define DEF_typeCircular QString("circular")
#define DEF_typeSquare QString("square")


ONF_StepCreatePlotsFromList::ONF_StepCreatePlotsFromList() : SuperClass()
{
    _plotType = DEF_typeCircular;
    _createBuffers = true;
    _buffer = 5.0;
}

QString ONF_StepCreatePlotsFromList::description() const
{
    return tr("2- Créer des placettes à partir d'une grille");
}

QString ONF_StepCreatePlotsFromList::detailledDescription() const
{
    return tr("Génère les emprises d'une liste de placettes. ");
}

QString ONF_StepCreatePlotsFromList::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCreatePlotsFromList::createNewInstance() const
{
    return new ONF_StepCreatePlotsFromList();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCreatePlotsFromList::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Placettes"), tr(""), true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inPlotList, tr("Liste de placettes"));
}

void ONF_StepCreatePlotsFromList::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGrpPlot, tr("Placette (Groupe)"));

    if (_plotType == DEF_typeCircular)
    {
        manager.addItem(_outGrpPlot, _outPlotCircle, tr("Placette circulaire"));
    } else {
        manager.addItem(_outGrpPlot, _outPlotSquare, tr("Placette carrée"));
    }

    if (_createBuffers)
    {
        if (_plotType == DEF_typeCircular)
        {
            manager.addItem(_outGrpPlot, _outPlotCircleBuffer, tr("Placette circulaire (buffer)"));
        } else {
            manager.addItem(_outGrpPlot, _outPlotSquareBuffer, tr("Placette carrée (buffer)"));

        }
    }
}

void ONF_StepCreatePlotsFromList::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    QStringList types;
    types << DEF_typeCircular;
    types << DEF_typeSquare;

    postInputConfigDialog->addStringChoice(tr("Type de placette"), "", types, _plotType);
    postInputConfigDialog->addBool(tr("Créer les buffers"), "", "", _createBuffers);
    postInputConfigDialog->addDouble(tr("Taille de buffer"), "m", 0, std::numeric_limits<double>::max(), 2, _buffer);

}

void ONF_StepCreatePlotsFromList::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_PlotListInGrid* plotList : group->singularItems(_inPlotList))
        {
            if (isStopped()) {return;}

            CT_PlotListInGrid::Type type;
            if (_plotType == DEF_typeCircular)
            {
                type = CT_PlotListInGrid::T_Circle;
            } else {
                type = CT_PlotListInGrid::T_Square;
            }

            QHash<CT_AreaShape2DData*, size_t> plots = const_cast<CT_PlotListInGrid*>(plotList)->createPlots(type);

            QHashIterator<CT_AreaShape2DData*, size_t> itPl(plots);
            while (itPl.hasNext())
            {
                itPl.next();

                CT_StandardItemGroup* plGroup = new CT_StandardItemGroup();
                group->addGroup(_outGrpPlot, plGroup);

                if (_plotType == DEF_typeCircular)
                {
                    CT_Circle2DData* circleData = (CT_Circle2DData*) itPl.key();
                    CT_Circle2D* circle  = new CT_Circle2D(circleData);
                    plGroup->addSingularItem(_outPlotCircle, circle);

                    if (_createBuffers)
                    {
                        CT_Circle2DData* circleDataBuffer = new CT_Circle2DData(circleData->getCenter(), circleData->getRadius() + _buffer);
                        CT_Circle2D* circleBuffer  = new CT_Circle2D(circleDataBuffer);
                        plGroup->addSingularItem(_outPlotCircleBuffer, circleBuffer);
                    }
                } else {
                    CT_Box2DData* squareData = (CT_Box2DData*) itPl.key();
                    CT_Box2D* square     = new CT_Box2D(squareData);
                    plGroup->addSingularItem(_outPlotSquare, square);

                    if (_createBuffers)
                    {
                        CT_Box2DData* squareDataBuffer = new CT_Box2DData(squareData->getCenter(), squareData->getWidth() + 2.0*_buffer, squareData->getHeight() + 2.0*_buffer);
                        CT_Box2D* squareBuffer  = new CT_Box2D(squareDataBuffer);
                        plGroup->addSingularItem(_outPlotSquareBuffer, squareBuffer);
                    }
                }
            }
        }
    }
}
