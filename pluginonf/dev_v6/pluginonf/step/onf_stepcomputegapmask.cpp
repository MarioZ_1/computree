/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputegapmask.h"

#include "ct_triangulation/ct_delaunayt.h"
#include "ct_triangulation/ct_nodet.h"
#include "ct_triangulation/ct_trianglet.h"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

#include <QtConcurrent>
#include <math.h>
#include <stdlib.h>

ONF_StepComputeGapMask::ONF_StepComputeGapMask() : SuperClass()
{
    _threshold = 5.0;
    _nbErodingSteps = -1;
    _thresholdNA = -std::numeric_limits<double>::max();
}

QString ONF_StepComputeGapMask::description() const
{
    return tr("Créer un masque des trouées");
}

QString ONF_StepComputeGapMask::detailledDescription() const
{
    return tr("Cette étape créée un raster entier. Toute valeur >= 0 est dans les trouées au seuil fixée. Le nombre indique à combien d'erosions le pixel trouée persiste.");
}

CT_VirtualAbstractStep* ONF_StepComputeGapMask::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepComputeGapMask();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeGapMask::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inDEM, tr("DEM (height raster)"));
}

void ONF_StepComputeGapMask::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Seuil de hauteur de trouée :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _threshold);
    postInputConfigDialog->addInt(tr("Nombre d'érosions (-1 si toutes) :"), "", -1, std::numeric_limits<int>::max(), _nbErodingSteps);
    postInputConfigDialog->addDouble(tr("Considérer NA si < à :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _thresholdNA);
}

void ONF_StepComputeGapMask::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outGapMask, tr("Gap Mask"));
}

void ONF_StepComputeGapMask::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* dem : group->singularItems(_inDEM))
        {
            if (isStopped()) {return;}

            CT_Image2D<int>* gapMask = new CT_Image2D<int>(dem->minX(), dem->minY(), dem->xdim(), dem->ydim(), dem->resolution(), dem->minZ(), -9999, -1);
            CT_Image2D<quint8>* tmpMask = new CT_Image2D<quint8>(dem->minX(), dem->minY(), dem->xdim(), dem->ydim(), dem->resolution(), dem->minZ(), 0, 0);

            cv::Mat_<quint8> tmpMaskMat = tmpMask->getMat();

            size_t ncells = dem->nCells();

            for (size_t index = 0 ; index < ncells ; index++)
            {
                float val = dem->valueAtIndex(index);

                if (qFuzzyCompare(val, dem->NA()) || val < float(_thresholdNA))
                {
                    gapMask->setValueAtIndex(index, gapMask->NA());
                    tmpMask->setValueAtIndex(index, 0);
                } else if (val > float(_threshold))
                {
                    gapMask->setValueAtIndex(index, -1);
                    tmpMask->setValueAtIndex(index, 0);
                } else {
                    gapMask->setValueAtIndex(index, 0);
                    tmpMask->setValueAtIndex(index, 1);
                }
            }

            bool pixelsLosts = true;
            int nbErode = 0;
            cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

            while (pixelsLosts && (_nbErodingSteps == -1 || nbErode < _nbErodingSteps))
            {
                pixelsLosts = false;

                cv::erode(tmpMaskMat, tmpMaskMat, element, cv::Point(-1,-1), 1);
                nbErode++;

                for (size_t index = 0 ; index < ncells ; index++)
                {
                    quint8 val = tmpMask->valueAtIndex(index);

                    if (gapMask->valueAtIndex(index) >= 0)
                    {
                        if (val == 1)
                        {
                            gapMask->setValueAtIndex(index, nbErode);
                        } else if (!pixelsLosts && gapMask->valueAtIndex(index) == (nbErode - 1)){
                            pixelsLosts = true;
                        }
                    }
                }
            }

            delete tmpMask;

            // ajout du raster MNS
            group->addSingularItem(_outGapMask, gapMask);
            gapMask->computeMinMax();
        }

    }
    setProgress(100);

}

