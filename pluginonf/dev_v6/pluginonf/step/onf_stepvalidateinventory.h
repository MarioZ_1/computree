﻿/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPVALIDATEINVENTORY_H
#define ONF_STEPVALIDATEINVENTORY_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/ct_referencepoint.h"

class CT_AbstractItemGroup;

class ONF_StepValidateInventory: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepValidateInventory();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();

    void useManualMode(bool quit = false);

private:

    // Declaration of autoRenames Variables (groups or items added to In models copies)

    // Step parameters
    QStringList    _speciesFileName;
    double          _hRef;

    // Step parameters
    DocumentInterface                               *m_doc;
    int                                             m_status;

    QMap<const CT_StandardItemGroup*, float>                                         _dtmZvalues;
    QMap<const CT_StandardItemGroup*, const CT_AbstractSingularItemDrawable*>           *_selectedItem;
    QMultiMap<const CT_StandardItemGroup*, const CT_AbstractSingularItemDrawable*>      *_availableItem;
    QMap<const CT_StandardItemGroup*, QString>                                          *_species;
    QMap<const CT_StandardItemGroup*, QString>                                          *_ids;
    QStringList                                                                         _speciesList;

    void findBestItemForEachGroup();

};

#endif // ONF_STEPVALIDATEINVENTORY_H
