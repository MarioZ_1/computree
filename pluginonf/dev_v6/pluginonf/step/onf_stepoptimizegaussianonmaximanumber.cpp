#include "onf_stepoptimizegaussianonmaximanumber.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/types_c.h"

ONF_StepOptimizeGaussianOnMaximaNumber::ONF_StepOptimizeGaussianOnMaximaNumber() : SuperClass()
{
    _sigmaStep = 0.05;
    _sigmaMax = 1.0;
    _minHeight = 2.0;
    _CoefMult = 2.0;
}

QString ONF_StepOptimizeGaussianOnMaximaNumber::description() const
{
    return tr("Filtre Gaussien optimisé par le nombre de maxima");
}

QString ONF_StepOptimizeGaussianOnMaximaNumber::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepOptimizeGaussianOnMaximaNumber::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepOptimizeGaussianOnMaximaNumber::createNewInstance() const
{
    return new ONF_StepOptimizeGaussianOnMaximaNumber();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepOptimizeGaussianOnMaximaNumber::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inRes = createNewInResultModelForCopy(_inRes, tr("Image 2D"));
    res_inRes->setZeroOrMoreRootGroup();
    res_inRes->addGroupModel("", _inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRes->addItemModel(_inGrp, _inImage, CT_Image2D<float>::staticGetType(), tr("Image"));

}

void ONF_StepOptimizeGaussianOnMaximaNumber::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(_inRes);
    if (resCpy_res != nullptr)
    {
        resCpy_res->addItemModel(_inGrp, _filteredImage, new CT_Image2D<float>(), tr("Image filtrée"));
        resCpy_res->addItemAttributeModel(_filteredImage, _attSigma, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_VALUE), tr("Sigma"));
        resCpy_res->addItemModel(_inGrp, _maximaImage,  new CT_Image2D<qint32>(), tr("Maxima"));
    }
}

void ONF_StepOptimizeGaussianOnMaximaNumber::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Sigma max"), tr("en mètres"), 0, 1000, 2, _sigmaMax);
    postInputConfigDialog->addDouble(tr("Incrément de Sigma par étape"), tr("en mètres"), 0, 1000, 2, _sigmaStep);
    postInputConfigDialog->addDouble(tr("Ne pas détécter de maxima en dessous de"), tr("m"), 0, 99999, 2, _minHeight);

    postInputConfigDialog->addDouble(tr("Coeff Mult"), "", 1, 1000, 2, _CoefMult);
    postInputConfigDialog->addEmpty();
    postInputConfigDialog->addTitle(tr("N.B. : Portée du filtre = 7.7 x Sigma (en mètres)"));
}

void ONF_StepOptimizeGaussianOnMaximaNumber::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, _inGrp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();

        CT_Image2D<float>* imageIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, _inImage);
        if (imageIn != nullptr)
        {
            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);

            CT_Image2D<float>* filteredImage = new CT_Image2D<float>(_filteredImage.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), imageIn->NA(), imageIn->NA());
            grp->addSingularItem(filteredImage);

            CT_Image2D<qint32>* maximaImage = new CT_Image2D<qint32>(_maximaImage.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), imageIn->NA(), 0);
            grp->addSingularItem(maximaImage);

            cv::Mat_<float>& filteredMat = filteredImage->getMat();

            QMultiMap<double, int> nbMaxis;
            for (double sigmaInMeters = _sigmaStep ; sigmaInMeters <= (_sigmaMax + _sigmaMax/1000.0) ; sigmaInMeters += _sigmaStep)
            {
                double sigma = sigmaInMeters / imageIn->resolution();
                cv::GaussianBlur(imageIn->getMat(), filteredMat, cv::Size2d(0, 0), sigma);

                cv::Mat_<float> dilatedMat(filteredMat.rows, filteredMat.cols);
                cv::Mat maximaMat = cv::Mat::zeros(filteredMat.rows, filteredMat.cols, CV_32F); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!

                // Détéction des maxima
                cv::dilate(filteredMat, dilatedMat, cv::getStructuringElement(cv::MORPH_RECT, cv::Size2d(3,3)));
                cv::compare(filteredMat, dilatedMat, maximaMat, cv::CMP_EQ);

                // numérotation des maxima
                cv::Mat labels = cv::Mat::zeros(filteredMat.rows, filteredMat.cols, CV_32S); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!
                cv::connectedComponents(maximaMat, labels);

                QList<qint32> maximaIds;
                cv::Mat_<qint32> labs = labels;
                for (int col = 0 ; col < maximaMat.cols ; col++)
                {
                    for (int lin = 0 ; lin < maximaMat.rows ; lin++)
                    {
                        if (filteredMat(lin, col) < _minHeight)
                        {
                            labs(lin, col) = 0;
                        }

                        qint32 maxId = labs(lin, col);
                        if (maxId != 0 && !maximaIds.contains(maxId)) {maximaIds.append(maxId);}
                    }
                }

                maximaImage->getMat() = labels;

                int maxNumber = maximaIds.size();
                nbMaxis.insert(sigmaInMeters, maxNumber);
            }

            double sigmaInMeters = _sigmaStep;
            if (nbMaxis.size() > 0)
            {
                int limit = nbMaxis.last() * _CoefMult;

                QMapIterator<double, int> it(nbMaxis);
                it.toBack();
                while (it.hasPrevious())
                {
                    it.previous();
                    if (it.value() <= limit) {sigmaInMeters = it.key();}
                }
            }

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Sigma retenu : %1")).arg(sigmaInMeters));

            double sigma = sigmaInMeters / imageIn->resolution();
            cv::GaussianBlur(imageIn->getMat(), filteredMat, cv::Size2d(0, 0), sigma);

            cv::Mat_<float> dilatedMat(filteredMat.rows, filteredMat.cols);
            cv::Mat maximaMat = cv::Mat::zeros(filteredMat.rows, filteredMat.cols, CV_32F); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!

            // Détéction des maxima
            cv::dilate(filteredMat, dilatedMat, cv::getStructuringElement(cv::MORPH_RECT, cv::Size2d(3,3)));
            cv::compare(filteredMat, dilatedMat, maximaMat, cv::CMP_EQ);

            // numérotation des maxima
            cv::Mat labels = cv::Mat::zeros(filteredMat.rows, filteredMat.cols, CV_32S); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!
            cv::connectedComponents(maximaMat, labels);

            filteredImage->addItemAttribute(new CT_StdItemAttributeT<double>(_attSigma.completeName(), CT_AbstractCategory::DATA_VALUE, res, sigmaInMeters));

            filteredImage->computeMinMax();
            maximaImage->computeMinMax();
        }
    }
}
