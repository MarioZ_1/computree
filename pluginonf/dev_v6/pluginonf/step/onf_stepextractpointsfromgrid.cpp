/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepextractpointsfromgrid.h"

#include "ct_math/ct_mathpoint.h"

#include <QtConcurrent/QtConcurrentMap>
#include <QMessageBox>

ONF_StepExtractPointsFromGrid::ONF_StepExtractPointsFromGrid() : SuperClass()
{
}

QString ONF_StepExtractPointsFromGrid::description() const
{
    return tr("Segmenter une scène à partir d'une grille d'indices");
}

QString ONF_StepExtractPointsFromGrid::detailledDescription() const
{
    return tr("");
}

QString ONF_StepExtractPointsFromGrid::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepExtractPointsFromGrid::createNewInstance() const
{
    return new ONF_StepExtractPointsFromGrid();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepExtractPointsFromGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene à segmenter"));
    manager.addItem(_inGroup, _inGridSeeds, tr("Grille segmentée"));
}

void ONF_StepExtractPointsFromGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scènes segmentées"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("ID cluster"));
}

void ONF_StepExtractPointsFromGrid::compute()
{

    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* scene = grp->singularItem(_inScene);
        const CT_Grid3D_Sparse<int>* segmentationGrid = grp->singularItem(_inGridSeeds);

        if (scene != nullptr && segmentationGrid != nullptr)
        {
            // create output segmented scenes
            QVector<CT_PointCloudIndexVector*> outClouds(segmentationGrid->dataMax() + 1);
            for (int i = 0 ; i < outClouds.size() ; i++)
            {
                CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);
                outClouds[i] = resPointCloudIndex;
            }

            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();
            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext())
            {
                const CT_Point &pt = itP.next().currentPoint();
                int clusterNumber = segmentationGrid->valueAtXYZ(pt(0), pt(1), pt(2));
                if (clusterNumber != segmentationGrid->NA())
                {
                    outClouds[clusterNumber]->addIndex(itP.currentGlobalIndex());
                }
            }

            for (int i = 0 ; i < outClouds.size() ; i++)
            {
                CT_PointCloudIndexVector *resPointCloudIndex = outClouds[i];

                if (resPointCloudIndex->size() > 0)
                {
                    CT_StandardItemGroup* scGrp = new CT_StandardItemGroup();
                    grp->addGroup(_outGroup, scGrp);

                    resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);
                    CT_Scene* scOut = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));
                    scOut->updateBoundingBox();
                    scGrp->addSingularItem(_outScene, scOut);

                    scOut->addItemAttribute(_outAtt, new CT_StdItemAttributeT<int>(CT_AbstractCategory::DATA_ID, i));

                } else {
                    delete resPointCloudIndex;
                }
            }
        }
    }

}
