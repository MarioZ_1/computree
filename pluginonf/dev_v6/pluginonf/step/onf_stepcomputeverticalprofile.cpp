/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputeverticalprofile.h"

#include "tools/onf_computeverticalprofilethread.h"

#include <QFileInfo>

ONF_StepComputeVerticalProfile::ONF_StepComputeVerticalProfile() : SuperClass()
{
    _res = 0.5;
    _gridMode = 1;
    _xBase = 0;
    _yBase = 0;
    _zBase = 0;
}

QString ONF_StepComputeVerticalProfile::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Créer profil vertical de densité de points");
}

QString ONF_StepComputeVerticalProfile::detailledDescription() const
{
    return tr("Cette étape génère une profil selon l'axe Z.");
}

CT_VirtualAbstractStep* ONF_StepComputeVerticalProfile::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepComputeVerticalProfile();
}

void ONF_StepComputeVerticalProfile::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
}

void ONF_StepComputeVerticalProfile::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outProfile, tr("ProfilZ"));
}

void ONF_StepComputeVerticalProfile::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution du profil"),tr("mètres"),0.0001,10000,2, _res );

    postInputConfigDialog->addText(tr("Callage de l'origine (X, Y, Z) :"),"", "");

    CT_ButtonGroup &bg_gridMode = postInputConfigDialog->addButtonGroup(_gridMode);
    postInputConfigDialog->addExcludeValue("", "", tr("Sur la boite englobante de la scène"), bg_gridMode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("Par rapport aux coordonnées suivantes :"), bg_gridMode, 1);

    postInputConfigDialog->addDouble(tr("Coordonnée X :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Y :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _yBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Z :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _zBase);
}

void ONF_StepComputeVerticalProfile::compute()
{
    qDeleteAll(_threadList);
    _threadList.clear();

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            double minX = _xBase;
            double minY = _yBase;
            double minZ = _zBase;

            if (_gridMode == 0)
            {
                minX = scene->minX();
                minY = scene->minY();
                minZ = scene->minZ();
            } else {

                while (minX < scene->minX()) {minX += _res;};
                while (minY < scene->minY()) {minY += _res;};
                while (minZ < scene->minZ()) {minZ += _res;};

                while (minX > scene->minX()) {minX -= _res;};
                while (minY > scene->minY()) {minY -= _res;};
                while (minZ > scene->minZ()) {minZ -= _res;};
            }

            // Declaring the output grids
            CT_Profile<int>* proZ = CT_Profile<int>::createProfileFromSegment(minX, minY, minZ, minX, minY, scene->maxZ(), _res, -1, 0);

            group->addSingularItem(_outProfile, proZ);

            ONF_ComputeVerticalProfileThread* hitsThread = new ONF_ComputeVerticalProfileThread(proZ, scene);
            connect(hitsThread, SIGNAL(progressChanged()), this, SLOT(updateProgress()));
            hitsThread->start();
            _threadList.append(hitsThread);
        }
    }

    int size = _threadList.size();
    for (int i = 0 ; i < size ; ++i)
    {
        _threadList.at(i)->wait();
        updateProgress();
    }

    _mutex.lock();
    qDeleteAll(_threadList);
    _threadList.clear();
    _mutex.unlock();

    setProgress(99.0f);
}

void ONF_StepComputeVerticalProfile::updateProgress()
{
    float progress = 0;

    _mutex.lock();
    int size = _threadList.size();
    for (int i = 0 ; i < size ; ++i)
    {
        progress += _threadList.at(i)->getProgress();
    }
    _mutex.unlock();

    if(size > 0)
    {
        progress /= float(size);
        setProgress(progress);
    }
}

