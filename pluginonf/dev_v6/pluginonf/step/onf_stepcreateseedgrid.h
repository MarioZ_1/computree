/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCREATESEEDGRID_H
#define ONF_STEPCREATESEEDGRID_H

#include "ct_step/abstract/ct_abstractstep.h"                    // The step inherits from ct_abstractstep

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "ct_tools/ct_monitoredqthread.h"

class ONF_StepCreateSeedGrid : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepCreateSeedGrid();

    virtual QString description() const;

    virtual QString detailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    virtual void declareInputModels(CT_StepInModelStructureManager& manager) final;

    virtual void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    virtual void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    virtual void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    virtual void compute() final;

private:

    int  _mode;
    double _offset;

    CT_HandleInResultGroup<>                                                            _inResultItem;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupItem;
    CT_HandleInStdGroup<>                                                               _inGroupItem;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItem;

    CT_HandleInResultGroup<>                                                            _inResultScene;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupScene;
    CT_HandleInStdGroup<>                                                               _inGroupScene;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleInResultGroup<>                                                            _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                                               _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float> >                                         _inDTM;

    CT_HandleInResultGroupCopy<>                                                        _inResultGrid3D;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupGrid3D;
    CT_HandleInStdGroup<>                                                               _inGroupGrid3D;
    CT_HandleInSingularItem<CT_AbstractGrid3D>                                          _inGrid3D;


    CT_HandleOutSingularItem<CT_Grid3D_Sparse<int> >                                    _outGrid3D;

};

#endif // ONF_STEPCREATESEEDGRID_H

