/****************************************************************************
 Copyright (C) 2010-2019 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputedsm.h"

#include "ct_log/ct_logmanager.h"

#define EPSILON 0.000001

ONF_StepComputeDSM::ONF_StepComputeDSM() : SuperClass()
{
    _gridsize   = 0.5;
    _convertNA = true;

    _gridMode = 1;
    _NAMode = 0;
    _naReplaceValue = 0;
    _xBase = 0;
    _yBase = 0;

}

QString ONF_StepComputeDSM::description() const
{
    return tr("Créer MNS (Zmax)");
}

QString ONF_StepComputeDSM::detailledDescription() const
{
    return tr("Cette étape permet de générer un Modèle Numérique de Surface (MNS).<br>"
              "Le MNS est calculé comme un raster Zmax à la <b>résolution</b> spécifiée.");
}

CT_VirtualAbstractStep* ONF_StepComputeDSM::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepComputeDSM();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeDSM::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Points végétation"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Points végétation"));
    manager.addItem(_inGroup, _inArea, tr("Emprise"));
}

void ONF_StepComputeDSM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution du raster :"), "cm", 1, 1000, 0, _gridsize, 100);

    postInputConfigDialog->addTitle("_____________________________________________________________________");
    postInputConfigDialog->addBool(tr("Remplacer les valeurs NA par :"), "", "", _convertNA);
    CT_ButtonGroup &bg_NAMode = postInputConfigDialog->addButtonGroup(_NAMode);
    postInputConfigDialog->addExcludeValue("", "", tr("Min(MNS)"), bg_NAMode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("La valeur ci-dessous"), bg_NAMode, 1);
    postInputConfigDialog->addDouble(tr("Valeur de remplacement des NA :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _naReplaceValue);

    postInputConfigDialog->addTitle("_____________________________________________________________________");
    postInputConfigDialog->addText(tr("Callage du coin (minX, minY) :"),"", "");
    CT_ButtonGroup &bg_gridMode = postInputConfigDialog->addButtonGroup(_gridMode);
    postInputConfigDialog->addExcludeValue("", "", tr("Sur la boite englobante de la scène"), bg_gridMode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("Par rapport aux coordonnées suivantes :"), bg_gridMode, 1);
    postInputConfigDialog->addDouble(tr("Coordonnée X :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Y :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _yBase);
    postInputConfigDialog->addTitle(tr("N.B. : Si une emprise a été fournie, c'est elle qui sera utilisée dans tous les cas."));


}

void ONF_StepComputeDSM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outDSM, tr("MNS"));
}

void ONF_StepComputeDSM::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();

            if (pointCloudIndex->size() > 0)
            {
                const CT_AbstractGeometricalItem *emprise = group->singularItem(_inArea);

                // Creation du raster
                double minX = std::floor(inScene->minX()*100.0) / 100.0;
                double minY = std::floor(inScene->minY()*100.0) / 100.0;
                double maxX = std::ceil(inScene->maxX()*100.0) / 100.0;
                double maxY = std::ceil(inScene->maxY()*100.0) / 100.0;


                if (emprise != nullptr && emprise->hasBoundingBox())
                {
                    Eigen::Vector3d min, max;
                    emprise->boundingBox(min, max);

                    minX = min(0);
                    minY = min(1);
                    maxX = max(0) - EPSILON;
                    maxY = max(1) - EPSILON;

                } else if (_gridMode == 1)
                {
                    minX = (std::floor((inScene->minX() - _xBase) - 1) / _gridsize) * _gridsize + _xBase;
                    minY = (std::floor((inScene->minY() - _yBase) - 1) / _gridsize) * _gridsize + _yBase;

                    while (minX < inScene->minX()) {minX += _gridsize;};
                    while (minY < inScene->minY()) {minY += _gridsize;};

                    while (minX > inScene->minX()) {minX -= _gridsize;};
                    while (minY > inScene->minY()) {minY -= _gridsize;};
                }

                CT_Image2D<float>* mns = CT_Image2D<float>::createImage2DFromXYCoords(minX, minY, maxX, maxY, _gridsize, inScene->minZ(), -9999, -9999);

                // Création MNS (version Zmax)
                CT_PointIterator itP(pointCloudIndex);
                while(itP.hasNext() && !isStopped())
                {
                    const CT_Point &point =itP.next().currentPoint();
                    mns->setMaxValueAtCoords(point(0), point(1), static_cast<float>(point(2)));
                }

                setProgress(50.0f);

                // ajout du raster MNS
                mns->computeMinMax();
                group->addSingularItem(_outDSM, mns);

                if (_convertNA)
                {
                    float minNaVal = mns->dataMin();
                    if (_NAMode == 1) {minNaVal = static_cast<float>(_naReplaceValue);}

                    for (size_t index = 0 ; index < mns->nCells() ; index++)
                    {
                        if (qFuzzyCompare(mns->valueAtIndex(index),mns->NA())) {mns->setValueAtIndex(index, minNaVal);}
                    }
                }
            }
        }
        setProgress(100.0f);
    }
}

