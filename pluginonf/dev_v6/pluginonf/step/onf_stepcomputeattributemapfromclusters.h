/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCOMPUTEATTRIBUTEMAPFROMCLUSTERS_H
#define ONF_STEPCOMPUTEATTRIBUTEMAPFROMCLUSTERS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"

// Inclusion of auto-indexation system

class ONF_StepComputeAttributeMapFromClusters: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepComputeAttributeMapFromClusters();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double                  _naValue;
    double                  _defaultValue;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inMainGrp;
    CT_HandleInSingularItem<CT_Image2D<qint32>>                                         _inClusters;
    CT_HandleInSingularItem<CT_AbstractImage2D, 0, 1>                                   _inAttrib;

    CT_HandleInStdGroup<>                                                               _inGrp;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItem;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::INT32>      _inItemID;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inItemAtt;

    CT_HandleOutSingularItem<CT_Image2D<double>>                                        _outRaster;


};

#endif // ONF_STEPCOMPUTEATTRIBUTEMAPFROMCLUSTERS_H
