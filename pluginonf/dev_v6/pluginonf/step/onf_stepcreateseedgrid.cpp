/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreateseedgrid.h"

#include "tools/onf_computehitsthread.h"

#include <QFileInfo>

ONF_StepCreateSeedGrid::ONF_StepCreateSeedGrid() : SuperClass()
{
    _mode = 0;
    _offset = 1.3;
}

QString ONF_StepCreateSeedGrid::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Create seed voxel grid");
}

QString ONF_StepCreateSeedGrid::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepCreateSeedGrid::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepCreateSeedGrid();
}

void ONF_StepCreateSeedGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    if (_mode == 0 || _mode == 2)
    {
        QString label1 = tr("Items");
        QString label2 = tr("Item");
        if (_mode == 2)
        {
            label1 = tr("Positions 2D");
            label2 = tr("Position 2D");
        }

        manager.addResult(_inResultItem, label1, "", true);
        manager.setZeroOrMoreRootGroup(_inResultItem, _inZeroOrMoreRootGroupItem);
        manager.addGroup(_inZeroOrMoreRootGroupItem, _inGroupItem);
        manager.addItem(_inGroupItem, _inItem, label2);
    }

    if (_mode == 1)
    {
        manager.addResult(_inResultScene, tr("Scene(s)"), "", true);
        manager.setZeroOrMoreRootGroup(_inResultScene, _inZeroOrMoreRootGroupScene);
        manager.addGroup(_inZeroOrMoreRootGroupScene, _inGroupScene);
        manager.addItem(_inGroupScene, _inScene, tr("Scene(s)"));
    }

    if (_mode == 2)
    {
        manager.addResult(_inResultDTM, tr("MNT"), "", true);
        manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
        manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
        manager.addItem(_inGroupDTM, _inDTM, tr("MNT"));
    }

    manager.addResult(_inResultGrid3D, tr("Grille"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultGrid3D, _inZeroOrMoreRootGroupGrid3D);
    manager.addGroup(_inZeroOrMoreRootGroupGrid3D, _inGroupGrid3D);
    manager.addItem(_inGroupGrid3D, _inGrid3D, tr("Grille"));
}

void ONF_StepCreateSeedGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResultGrid3D);
    manager.addItem(_inGroupGrid3D, _outGrid3D, tr("Grille de graines"));
}

void ONF_StepCreateSeedGrid::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    CT_ButtonGroup &bg_mode = preInputConfigDialog->addButtonGroup(_mode);

    preInputConfigDialog->addExcludeValue("", "", tr("Items"), bg_mode, 0);
    preInputConfigDialog->addExcludeValue("", "", tr("Scènes de points"), bg_mode, 1);

    preInputConfigDialog->addExcludeValue("", "", tr("Positions2D + MNT"), bg_mode, 2);

}

void ONF_StepCreateSeedGrid::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    if (_mode == 2)
    {

        postInputConfigDialog->addDouble(tr("Offset :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _offset);
    }
}

void ONF_StepCreateSeedGrid::compute()
{
    CT_Image2D<float> *dtm = nullptr;
    if (_mode == 2)
    {
        for (const CT_Image2D<float>* dtmConst : _inDTM.iterateInputs(_inResultDTM))
        {
            dtm = const_cast<CT_Image2D<float> *>(dtmConst);
        }
    }

    for (CT_StandardItemGroup* group : _inGroupGrid3D.iterateOutputs(_inResultGrid3D))
    {
        for (const CT_AbstractGrid3D* gridIn : group->singularItems(_inGrid3D))
        {
            if (isStopped()) {return;}

            // Declaring the output grids
            CT_Grid3D_Sparse<int>* outGrid = new CT_Grid3D_Sparse<int>(gridIn->minX(), gridIn->minY(), gridIn->minZ(), gridIn->xdim(), gridIn->ydim(), gridIn->zdim(), gridIn->resolution(), -1, -1);

            if (_mode == 0)
            {
                int cpt = 1;
                for (const CT_AbstractSingularItemDrawable* item : _inItem.iterateInputs(_inResultItem))
                {
                    outGrid->setValueAtXYZ(item->centerX(), item->centerY(), item->centerZ(), cpt);
                    ++cpt;
                }
            } else if (_mode == 1)
            {
                int cpt = 1;
                for (const CT_AbstractItemDrawableWithPointCloud* scene : _inScene.iterateInputs(_inResultScene))
                {
                    const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();

                    CT_PointIterator itP(pointCloudIndex);
                    while(itP.hasNext())
                    {
                        const CT_Point &pt = itP.next().currentPoint();
                        outGrid->setValueAtXYZ(pt(0), pt(1), pt(2), cpt);
                    }
                    ++cpt;
                }

            } else if (_mode == 2)
            {
                int cpt = 1;
                for (const CT_AbstractSingularItemDrawable* item : _inItem.iterateInputs(_inResultItem))
                {
                    float z = dtm->valueAtCoords(item->centerX(), item->centerY());
                    if (qFuzzyCompare(z, dtm->NA()))
                    {
                        z = 0;
                    }
                    z += float(_offset);

                    outGrid->setValueAtXYZ(item->centerX(), item->centerY(), double(z), cpt);
                    ++cpt;
                }
            }

            outGrid->computeMinMax();
            group->addSingularItem(_outGrid3D, outGrid);
        }
    }

    setProgress(99);
}

