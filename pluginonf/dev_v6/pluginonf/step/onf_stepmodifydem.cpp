/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmodifydem.h"

#include "ct_math/ct_mathpoint.h"

#include <QtConcurrent/QtConcurrentMap>

#include <QMessageBox>

ONF_StepModifyDEM::ONF_StepModifyDEM() : SuperClass()
{
    m_doc = nullptr;

    setManual(true);
}

QString ONF_StepModifyDEM::description() const
{
    return tr("Modify DEM");
}

QString ONF_StepModifyDEM::detailledDescription() const
{
    return tr("");
}

QString ONF_StepModifyDEM::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepModifyDEM::createNewInstance() const
{
    return new ONF_StepModifyDEM();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepModifyDEM::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("DEM"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInDEM, CT_Image2D<float>::staticGetType(), tr("DEM à modifier"));

    CT_InResultModelGroup *resIMG = createNewInResultModel(_inResIMG, tr("2D Images (optionnel)"), "", true);
    resIMG->setZeroOrMoreRootGroup();
    resIMG->addGroupModel("", _inGrpIMG, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIMG->addItemModel(_inGrpIMG, _inRed, CT_AbstractImage2D::staticGetType(), tr("Red band"));
    resIMG->addItemModel(_inGrpIMG, _inGreen, CT_AbstractImage2D::staticGetType(), tr("Green band"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIMG->addItemModel(_inGrpIMG, _inBlue, CT_AbstractImage2D::staticGetType(), tr("Blue band"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIMG->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
}

void ONF_StepModifyDEM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != nullptr) {
        res->addItemModel(DEF_SearchInGroup, _outDEMModelName, new CT_Image2D<float>(), tr("DEM modifié"));
    }
}

void ONF_StepModifyDEM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    //
}

void ONF_StepModifyDEM::compute()
{
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = nullptr;

    CT_ResultGroup* res_IMG = nullptr;
    _inred = nullptr;
    _ingreen = nullptr;
    _inblue = nullptr;
    if (getInputResults().size() > 1)
    {
        res_IMG = getInputResults().at(1);
        CT_ResultGroupIterator imgIt(res_IMG, this, _inGrpIMG);
        if (imgIt.hasNext())
        {
            CT_StandardItemGroup* grpIMG = (CT_StandardItemGroup*) imgIt.next();

            _inred = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, _inRed);
            _ingreen = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, _inGreen);
            _inblue = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, _inBlue);

            if (_inred != nullptr)
            {
                if (_ingreen == nullptr) {_ingreen = _inred;}
                if (_inblue == nullptr) {_inblue = _inred;}
            }
        }
    }

    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    if (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        _inDEM = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEF_SearchInDEM);

        if (grp != nullptr && _inDEM != nullptr)
        {
            CT_AbstractUndefinedSizePointCloud* mpcir = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            CT_Point point;
            size_t col, lin;
            Eigen::Vector3d cellCenter;

            for (size_t i = 0 ; i < _inDEM->nCells() ; i++)
            {
                float value = _inDEM->valueAtIndex(i);
                if (value != _inDEM->NA())
                {
                    _inDEM->setValueAtIndex(i, value);

                    if (_inDEM->indexToGrid(i, col, lin))
                    {
                        if (_inDEM->getCellCenterCoordinates(i, cellCenter))
                        {
                            point.setValues(cellCenter(0), cellCenter(1), value);
                            mpcir->addPoint(point);
                        }
                    }
                }
            }

            _scene = new CT_Scene("ONF_StepModifyDEM_01", res_out, PS_REPOSITORY->registerUndefinedSizePointCloud(mpcir));
            _scene->updateBoundingBox();

            _modifiedDEM = new CT_Image2D<float>(_outDEMModelName.completeName(), res_out, _inDEM->minX(), _inDEM->minY(), _inDEM->colDim(), _inDEM->linDim(), _inDEM->resolution(), _inDEM->minZ(), -9999, -9999);
            CT_PointIterator itP0(_scene->pointCloudIndex());
            while(itP0.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP0.next().currentPoint();
                CT_PointData &pt = itP0.currentInternalPoint();

                _modifiedDEM->setValueAtCoords(point(0), point(1), pt(2));
            }

            // Début de la partie interactive
            m_doc = nullptr;

            m_status = 0;
            requestManualMode();

            m_status = 1;
            requestManualMode();
            // Fin de la partie interactive

            _modifiedDEM->computeMinMax();
            grp->addSingularItem(_modifiedDEM);

            delete _scene;
        }
    }

}

void ONF_StepModifyDEM::initManualMode()
{
    QColor color(0, 0, 75);
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument(4.0, true, &color);

    m_doc->removeAllItemDrawable();

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionModifyDEM(_scene, _inDEM, _modifiedDEM, _inred, _ingreen, _inblue));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de modification de MNE."), QMessageBox::Ok);
}

void ONF_StepModifyDEM::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;

            quitManualMode();
        }
    }
}
