/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPFILTERELEMENTSBYXYAREA_H
#define ONF_STEPFILTERELEMENTSBYXYAREA_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/abstract/ct_abstractsingularitemdrawable.h"
#include "ct_itemdrawable/abstract/ct_abstractareashape2d.h"

class ONF_StepFilterElementsByXYArea: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFilterElementsByXYArea();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGrpRoot;
    CT_HandleInSingularItem<CT_AbstractAreaShape2D>                                     _inXYArea;
    CT_HandleInStdGroup<>                                                               _inGrp;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItem;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::DOUBLE>     _inXattribute;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::DOUBLE>     _inYattribute;

};

#endif // ONF_STEPFILTERELEMENTSBYXYAREA_H
