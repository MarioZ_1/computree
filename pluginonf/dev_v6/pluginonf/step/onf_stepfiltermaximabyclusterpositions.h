/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPFILTERMAXIMABYCLUSTERPOSITIONS_H
#define ONF_STEPFILTERMAXIMABYCLUSTERPOSITIONS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepFilterMaximaByClusterPositions: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFilterMaximaByClusterPositions();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    QStringList _fileNameSearchRadii;
    QStringList _fileNameExclusionRadii;

    // Declaration of autoRenames Variables (groups or items added to In models copies)

    double getRadius(double height, const QMap<double, double> &radii);

    void readRadii(QString fileName, QMap<double, double> &radii);


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGrp;
    CT_HandleInSingularItem<CT_Image2D<float> >                                         _inImage;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                        _inMaxima;

    CT_HandleInStdGroup<>                                                               _inGrpPos;
    CT_HandleInSingularItem<CT_PointCluster>                                            _inPosition;

    CT_HandleOutSingularItem<CT_Image2D<qint32> >                                       _filteredMaxima;
    CT_HandleOutSingularItem<CT_ItemAttributeList>                                      _attMaximaItem;
    CT_HandleOutStdItemAttribute<qint32>                                                _attMaxima;
    CT_HandleOutStdItemAttribute<quint64>                                               _attClusterID;

};

#endif // ONF_STEPFILTERMAXIMABYCLUSTERPOSITIONS_H
