#ifndef ONF_STEPSTANDARDIZEINTENSITY_H
#define ONF_STEPSTANDARDIZEINTENSITY_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_scanpath.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"

class ONF_StepStandardizeIntensity : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepStandardizeIntensity();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double                  _meanHeight;




};

#endif // ONF_STEPSTANDARDIZEINTENSITY_H
