#ifndef ONF_STEPCREATERASTERMOSAIC_H
#define ONF_STEPCREATERASTERMOSAIC_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"

#include <Eigen/Core>

class ONF_StepCreateRasterMosaic : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;
public:
    ONF_StepCreateRasterMosaic();

    ~ONF_StepCreateRasterMosaic();

    QString description() const;

    QString detailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    virtual void declareInputModels(CT_StepInModelStructureManager& manager) final;

    virtual void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    virtual void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    virtual void compute() final;

private:

    Eigen::Vector2d _min;
    double _resolution;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItemArea;


    CT_HandleInResultGroup<>                                                            _inResultRaster;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupRaster;
    CT_HandleInStdGroup<>                                                               _inGroupRaster;
    CT_HandleInSingularItem<CT_AbstractImage2D>                                         _inRaster;

    CT_HandleOutSingularItem<CT_Image2D<float>>                                         _outRaster;


};

#endif // ONF_STEPCREATERASTERMOSAIC_H
