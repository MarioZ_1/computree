#ifndef ONF_STEPCOMPUTEHILLSHADERASTER_H
#define ONF_STEPCOMPUTEHILLSHADERASTER_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepComputeHillShadeRaster: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepComputeHillShadeRaster();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double          _azimut;
    double          _altitude;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float>>                                          _inDEM;

    CT_HandleOutSingularItem<CT_Image2D<float>>                                         _outHillShade;


};

#endif // ONF_STEPCOMPUTEHILLSHADERASTER_H
