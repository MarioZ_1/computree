#ifndef ONF_STEPCOMPUTERELATIVEINTENSITYATTRIBUTE_H
#define ONF_StepComputeRelativeIntensityAttribute_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"

class ONF_StepComputeRelativeIntensityAttribute : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    struct CandidatePoint {
        CandidatePoint(size_t  pointIndex, double gpsTime, double intensity)
        {
            _pointIndex = pointIndex;
            _gpsTime = gpsTime;
            _intensity = intensity;
            _sumIntensity = 0;
        }

        size_t  _pointIndex;
        double _gpsTime;
        double _intensity;
        double _sumIntensity;
    };

    static bool sortByGPSTime (CandidatePoint* a, CandidatePoint* b) {
        return (a->_gpsTime < b->_gpsTime);
    }

    ONF_StepComputeRelativeIntensityAttribute();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_StdLASPointsAttributesContainer>                         _inLas;

    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<double>>                _outAtt;


};

#endif // ONF_StepComputeRelativeIntensityAttribute_H
