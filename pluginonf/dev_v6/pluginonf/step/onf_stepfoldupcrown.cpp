#include "onf_stepfoldupcrown.h"

ONF_StepFoldUpCrown::ONF_StepFoldUpCrown() : SuperClass()
{
}

QString ONF_StepFoldUpCrown::description() const
{
    return tr("Fold Up Crown");
}

CT_VirtualAbstractStep* ONF_StepFoldUpCrown::createNewInstance() const
{
    return new ONF_StepFoldUpCrown();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFoldUpCrown::declareInputModels(CT_StepInModelStructureManager& manager)
{
    
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
}

void ONF_StepFoldUpCrown::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("FoldUp Scene"));
}

void ONF_StepFoldUpCrown::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* item_inScene = group->singularItem(_inScene);

        if (item_inScene != nullptr)
        {

            const CT_AbstractPointCloudIndex *cloudIndex = item_inScene->pointCloudIndex();
            CT_PointIterator itP(cloudIndex);

            // On Cree un nouveau nuage qui sera le translate
            CT_NMPCIR outputCloud = PS_REPOSITORY->createNewPointCloud(cloudIndex->size());
            CT_MutablePointIterator itPM(outputCloud);

            Eigen::Vector3d apex(0, 0, -std::numeric_limits<double>::max());

            while (itP.hasNext())
            {
                itP.next();
                const CT_Point &point = itP.currentPoint();

                if (point(2) > apex(2))
                {
                    apex = point;
                }
            }

            size_t i = 0;
            itP.toFront();
            // On applique la translation a tous les points du nuage
            while (itP.hasNext() && itPM.hasNext())
            {
                itP.next();
                const CT_Point &point = itP.currentPoint();
                Eigen::Vector3d xyPoint = point;
                xyPoint(0) = apex(0);
                xyPoint(1) = apex(1) + sqrt(pow(apex(0) - point(0), 2) + pow(apex(1) - point(1), 2));
                xyPoint(2) = point(2);

                itPM.next().replaceCurrentPoint(xyPoint);

                // Barre de progression
                setProgress(100.0f*i++ /cloudIndex->size());

                // On regarde si on est en debug mode
                waitForAckIfInDebugMode();
            }

            CT_Scene* itemOut_scene = new CT_Scene(outputCloud);
            itemOut_scene->updateBoundingBox();

            group->addSingularItem(_outScene, itemOut_scene);
        }
    }
}

