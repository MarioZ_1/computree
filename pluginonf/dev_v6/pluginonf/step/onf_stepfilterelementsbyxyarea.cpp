/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.
                         
 Contact : alexandre.piboule@onf.fr
 
 Developers : Alexandre PIBOULE (ONF)
 
 This file is part of PluginONF library.
 
 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfilterelementsbyxyarea.h"

ONF_StepFilterElementsByXYArea::ONF_StepFilterElementsByXYArea() : SuperClass()
{
}

QString ONF_StepFilterElementsByXYArea::description() const
{
    return tr("Garder les Items contenus dans une emprise");
}

QString ONF_StepFilterElementsByXYArea::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepFilterElementsByXYArea::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepFilterElementsByXYArea::createNewInstance() const
{
    return new ONF_StepFilterElementsByXYArea();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFilterElementsByXYArea::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Items à filtrer"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGrpRoot, tr("Groupe Emprise"));
    manager.addItem(_inGrpRoot, _inXYArea, tr("Emprise"));
    manager.addGroup(_inGrpRoot, _inGrp, tr("Groupe à filtrer"));
    manager.addItem(_inGrp, _inItem, tr("XY Item"));
    manager.addItemAttribute(_inItem, _inXattribute, CT_AbstractCategory::DATA_X, tr("X"));
    manager.addItemAttribute(_inItem, _inYattribute, CT_AbstractCategory::DATA_Y, tr("Y"));
}

void ONF_StepFilterElementsByXYArea::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepFilterElementsByXYArea::compute()
{
    
    QList<CT_StandardItemGroup*> groupsToBeRemoved;

    for (CT_StandardItemGroup* rootGrp : _inGrpRoot.iterateOutputs(_inResult))
    {
        const CT_AbstractAreaShape2D* xyArea = rootGrp->singularItem(_inXYArea);

        if (xyArea != nullptr)
        {
            for (const CT_StandardItemGroup* grp : rootGrp->groups(_inGrp))
            {
                if (isStopped()) {return;}

                const CT_AbstractSingularItemDrawable* item = rootGrp->singularItem(_inItem);
                
                if (item != nullptr)
                {
                    double refX = item->centerX();
                    double refY = item->centerY();
                    
                    if (item != nullptr)

                    {
                        const CT_AbstractItemAttribute* attributeX = item->itemAttribute(_inXattribute);
                        const CT_AbstractItemAttribute* attributeY = item->itemAttribute(_inYattribute);
                        
                        if (attributeX != nullptr && attributeY != nullptr)
                        {
                            bool okX, okY;
                            double valX = attributeX->toDouble(item, &okX);
                            double valY = attributeY->toDouble(item, &okY);
                            
                            if (okX && okY)
                            {
                                refX = valX;
                                refY = valY;
                            }
                        }
                    }
                    
                    if (!xyArea->contains(refX, refY))
                    {
                        groupsToBeRemoved.append(const_cast<CT_StandardItemGroup*>(grp));
                    }
                }
            }
        }
    }
    
    QListIterator<CT_StandardItemGroup*> itE(groupsToBeRemoved);
    while(itE.hasNext())
    {
        CT_StandardItemGroup *group = itE.next();
        group->removeFromParent(false);
    }
}

