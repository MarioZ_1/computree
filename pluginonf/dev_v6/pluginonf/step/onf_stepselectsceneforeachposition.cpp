/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepselectsceneforeachposition.h"

#include <QMessageBox>

ONF_StepSelectSceneForEachPosition::ONF_StepSelectSceneForEachPosition() : SuperClass()
{
    _distThrehold = 3.0;
    _interactiveCorrection = true;

    _dataContainer = nullptr;

    setManual(true);

    _m_status = 0;
    _m_doc = nullptr;
}

ONF_StepSelectSceneForEachPosition::~ONF_StepSelectSceneForEachPosition()
{
}

QString ONF_StepSelectSceneForEachPosition::description() const
{
    return tr("Apparier scènes et positions terrain");
}

QString ONF_StepSelectSceneForEachPosition::detailledDescription() const
{
    return tr("To Do");
}

CT_VirtualAbstractStep* ONF_StepSelectSceneForEachPosition::createNewInstance() const
{
    return new ONF_StepSelectSceneForEachPosition();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectSceneForEachPosition::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inPlot = createNewInResultModelForCopy(_inResPlot, tr("Placette"), "", true);
    res_inPlot->setZeroOrMoreRootGroup();
    res_inPlot->addGroupModel("", _inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inPlot->addItemModel(_inGrp, _inRef, CT_Circle2D::staticGetType(), tr("Arbre"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefDbh, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, tr("DBH"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefHeight, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, tr("Height"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefID, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::STRING, tr("IDtree"));
    res_inPlot->addItemAttributeModel(_inRef, _inRefIDplot, QList<QString>() << CT_AbstractCategory::DATA_ID, CT_AbstractCategory::STRING, tr("IDplot"));
    res_inPlot->addItemAttributeModel(_inRef, _inSpecies, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Species"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);

    CT_InResultModelGroup *res_inScene = createNewInResultModel(_inResScene, tr("Scene"), "", true);
    res_inScene->setZeroOrMoreRootGroup();
    res_inScene->addGroupModel("", _inGrpSc, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inScene->addItemModel(_inGrpSc, _inScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene"));

    CT_InResultModelGroup *resultDTM = createNewInResultModel(_inResDTM, tr("MNT"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", _inDTMGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(_inDTMGrp, _inDTM, CT_Image2D<float>::staticGetType(), tr("MNT"));
    resultDTM->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);

}

void ONF_StepSelectSceneForEachPosition::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(_inResPlot);

    if(res != nullptr)
    {
        res->addItemModel(_inGrp, _outSceneModelName, new CT_Scene(), tr("Scene arbre"));
        res->addItemAttributeModel(_outSceneModelName, _outSceneIDClusterAttModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID), tr("IDCluster"));
    }
}

void ONF_StepSelectSceneForEachPosition::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Distance d'appariement maximale :"), "m", 0, std::numeric_limits<double>::max(), 2, _distThrehold);
    postInputConfigDialog->addBool(tr("Correction interactive"), "", "", _interactiveCorrection);
}

void ONF_StepSelectSceneForEachPosition::compute()
{
    setManual(_interactiveCorrection);

    _dataContainer = new ONF_ActionSelectSceneForEachPosition_dataContainer();
    CT_Image2D<float>* dtm = nullptr;

    if (getInputResults().size() > 2)
    {
        CT_ResultGroup* res_inDTM = getInputResults().at(2);
        CT_ResultItemIterator it(res_inDTM, this, _inDTM);
        if (it.hasNext())
        {
            dtm = (CT_Image2D<float>*) it.next();
        }
    }

    _m_status = 0;
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* res_inScene = inResultList.at(1);

    CT_ResultGroupIterator itGrpSc(res_inScene, this, _inGrpSc);
    while (itGrpSc.hasNext())
    {
        const CT_AbstractItemGroup* grp = itGrpSc.next();

        CT_AbstractItemDrawableWithPointCloud* sc = (CT_AbstractItemDrawableWithPointCloud*) grp->firstItemByINModelName(this, _inScene);

        if (sc != nullptr)
        {
            const CT_AbstractPointCloudIndex *pointCloudIndex = sc->pointCloudIndex();

            if (pointCloudIndex->size() > 0)
            {
                Eigen::Vector3d apex;
                apex(2) = -std::numeric_limits<double>::max();

                CT_PointIterator itP(pointCloudIndex);
                while(itP.hasNext())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    if (point(2) > apex(2)) {apex = point;}
                }

                _dataContainer->_scenes.append(sc);
                _dataContainer->_scenesApex.append(apex);
            }
        }
    }

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_positions = outResultList.at(0);

    CT_ResultGroupIterator itGrp(resOut_positions, this, _inGrp);
    while (itGrp.hasNext())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itGrp.next();

        CT_Circle2D* circle = (CT_Circle2D*) grp->firstItemByINModelName(this, _inRef);
        if (circle != nullptr)
        {
            ONF_ActionSelectSceneForEachPosition_treePosition* treePos = new ONF_ActionSelectSceneForEachPosition_treePosition();
            treePos->_base(0) = circle->centerX();
            treePos->_base(1) = circle->centerY();
            treePos->_base(2) = 0;

            if (dtm != nullptr)
            {
                treePos->_base(2) = dtm->valueAtCoords(treePos->_base(0), treePos->_base(1));
                if (treePos->_base(2) == dtm->NA()) {treePos->_base(2) = 0;}
            }

            treePos->_apex(0) = circle->centerX();
            treePos->_apex(1) = circle->centerY();
            treePos->_apex(2) = treePos->_base(2);
            treePos->_hasApex = false;

            CT_AbstractItemAttribute* att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefDbh);
            if (att != nullptr) {treePos->_dbh = att->toFloat(circle, nullptr);}
            if (treePos->_dbh <= 0) {treePos->_dbh = 7.5;}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefHeight);
            if (att != nullptr) {treePos->_height = att->toFloat(circle, nullptr);}
            if (treePos->_height <= 0) {treePos->_height = -1;}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefID);
            if (att != nullptr) {treePos->_idTree = att->toString(circle, nullptr);}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inRefIDplot);
            if (att != nullptr) {treePos->_idPlot = att->toString(circle, nullptr);}

            att = circle->firstItemAttributeByINModelName(resOut_positions, this, _inSpecies);
            if (att != nullptr) {treePos->_species = att->toString(circle, nullptr);}

            _dataContainer->_positions.append(treePos);
            treePos->_grp = grp;
        }
    }

    // Tri par ordre décroissant de diamètre
    qSort(_dataContainer->_positions.begin(), _dataContainer->_positions.end(), ONF_StepSelectSceneForEachPosition::lessThan);

    positionMatching(_dataContainer);

    if (_interactiveCorrection)
    {
        requestManualMode();
        _m_status = 1;
    }

    for (int i = 0 ; i < _dataContainer->_positions.size() ; i++)
    {
        ONF_ActionSelectSceneForEachPosition_treePosition* treePos = _dataContainer->_positions.at(i);

        if (treePos != nullptr && treePos->_grp != nullptr)
        {
            CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
            resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

            // Fusion des scènes associées
            for (int i = 0 ; i < treePos->_scenesIds.size() ; i++)
            {
                int id = treePos->_scenesIds.at(i);
                CT_AbstractItemDrawableWithPointCloud* sc = (CT_AbstractItemDrawableWithPointCloud*) _dataContainer->_scenes.at(id);
                if (sc != nullptr)
                {
                    const CT_AbstractPointCloudIndex *pointCloudIndex = sc->pointCloudIndex();

                    if (pointCloudIndex != nullptr)
                    {
                        CT_PointIterator itP(pointCloudIndex);
                        while(itP.hasNext())
                        {
                            size_t index = itP.next().currentGlobalIndex();
                            resPointCloudIndex->addIndex(index);
                        }
                    }
                }
            }

            if (resPointCloudIndex->size() > 0)
            {
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

                // creation et ajout de la scene
                CT_Scene *outScene = new CT_Scene(_outSceneModelName.completeName(), resOut_positions, PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));

                QString sceneName = QString("%1_%2").arg(treePos->_idPlot).arg(treePos->_idTree);
                outScene->addItemAttribute(new CT_StdItemAttributeT<QString>(_outSceneIDClusterAttModelName.completeName(), CT_AbstractCategory::DATA_ID, resOut_positions, sceneName));

                treePos->_grp->addSingularItem(outScene);

            } else {
                delete resPointCloudIndex;
            }
        }

        delete treePos;
    }

    delete _dataContainer;

    if (_interactiveCorrection)
    {
        qDebug() << "AAAAAA";
        requestManualMode();
        qDebug() << "BBBBBB";

    }
}

void ONF_StepSelectSceneForEachPosition::positionMatching(ONF_ActionSelectSceneForEachPosition_dataContainer* dataContainer)
{
    dataContainer->_matched.resize(dataContainer->_scenesApex.size());
    dataContainer->_matched.fill(false);

    for (int pos = 0 ; pos < dataContainer->_positions.size() ; pos++)
    {
        ONF_ActionSelectSceneForEachPosition_treePosition* treePos = dataContainer->_positions.at(pos);

        //        if (treePos->_height > 0)
        //        {
        //            for (int ap = 0 ; ap < dataContainer->_scenesApex.size() ; ap++)
        //            {
        //                if (!affected[ap])
        //                {
        //                    const Eigen::Vector3d &apex = dataContainer->_scenesApex.at(ap);

        //                    double dist = sqrt(pow(treePos->_base(0) - apex(0), 2) + pow(treePos->_base(1) - apex(1), 2));
        //                    if (dist < _distThrehold)
        //                    {
        //                        double apexHeight = apex(2) - treePos->_base(2);

        //                        double deltaH = std::abs(treePos->_height - apexHeight);
        //                        double hd = treePos->_height / (treePos->_dbh / 100.0);
        //                    }
        //                }
        //            }
        //        } else { // on garde le plus haut
        double maxApexHeight = -std::numeric_limits<double>::max();
        int bestApex = -1;
        for (int ap = 0 ; ap < dataContainer->_scenesApex.size() ; ap++)
        {
            if (!dataContainer->_matched[ap])
            {
                const Eigen::Vector3d &apex = dataContainer->_scenesApex.at(ap);

                double dist = sqrt(pow(treePos->_base(0) - apex(0), 2) + pow(treePos->_base(1) - apex(1), 2));
                if (dist < _distThrehold)
                {
                    double apexHeight = apex(2) - treePos->_base(2);

                    if (apexHeight > maxApexHeight)
                    {
                        maxApexHeight = apexHeight;
                        bestApex = ap;
                    }
                }
            }
        }
        if (bestApex >= 0)
        {
            treePos->addScene(bestApex, dataContainer->_scenesApex.at(bestApex));
            dataContainer->_matched[bestApex] = true;
        }

        //        }
    }

}

void ONF_StepSelectSceneForEachPosition::initManualMode()
{
    if(_m_doc == nullptr)
    {
        // create a new 3D document
        QColor col = Qt::black;
        _m_doc = getGuiContext()->documentManager()->new3DDocument(2.0, false, &col);

        ONF_ActionSelectSceneForEachPosition* action = new ONF_ActionSelectSceneForEachPosition(_dataContainer);

        // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
        _m_doc->setCurrentAction(action, false);
    }

    //    QMessageBox::information(nullptr,
    //                             tr("Mode manuel"),
    //                             tr("Bienvenue dans le mode manuel de cette étape.\n"),
    //                             QMessageBox::Ok);
}

void ONF_StepSelectSceneForEachPosition::useManualMode(bool quit)
{
    if(_m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(_m_status == 1)
    {
        if(!quit)
        {
            _m_doc = nullptr;
            quitManualMode();
        }

    }

}
