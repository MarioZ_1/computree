/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputehitgrid.h"

#include <QFileInfo>

ONF_StepComputeHitGrid::ONF_StepComputeHitGrid() : SuperClass()
{
    _res = 0.5;
    _gridMode = 1;
    _xBase = 0;
    _yBase = 0;
    _zBase = 0.0;
}

QString ONF_StepComputeHitGrid::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Créer grille 3D de densité de points");
}

QString ONF_StepComputeHitGrid::detailledDescription() const
{
    return tr("Cette étape génère une grille 3D à la <b>résolution</b> spécifiée.<br>"
              "Chaque case reçoit le nombre de points de la scène d'entrée qu'elle contient.");
}

CT_VirtualAbstractStep* ONF_StepComputeHitGrid::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepComputeHitGrid();
}

void ONF_StepComputeHitGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
}

void ONF_StepComputeHitGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outGrid, tr("Hits"));
}

void ONF_StepComputeHitGrid::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution de la grille"),tr("meters"),0.0001,10000,3, _res );

    postInputConfigDialog->addText(tr("Callage du coin (minX, minY, minZ) :"),"", "");

    CT_ButtonGroup &bg_gridMode = postInputConfigDialog->addButtonGroup(_gridMode);
    postInputConfigDialog->addExcludeValue("", "", tr("Sur la boite englobante de la scène"), bg_gridMode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("Par rapport aux coordonnées suivantes :"), bg_gridMode, 1);

    postInputConfigDialog->addDouble(tr("Coordonnée X :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Y :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _yBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Z :"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _zBase);
}

void ONF_StepComputeHitGrid::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            double minX = _xBase;
            double minY = _yBase;
            double minZ = _zBase;

            if (_gridMode == 0)
            {
                minX = scene->minX();
                minY = scene->minY();
                minZ = scene->minZ();
            } else {
                minX = (std::floor((scene->minX() - _xBase) - 1) / _res) * _res + _xBase;
                minY = (std::floor((scene->minY() - _yBase) - 1) / _res) * _res + _yBase;
                minZ = (std::floor((scene->minZ() - _zBase) - 1) / _res) * _res + _zBase;

                while (minX < scene->minX()) {minX += _res;};
                while (minY < scene->minY()) {minY += _res;};
                while (minZ < scene->minZ()) {minZ += _res;};

                while (minX > scene->minX()) {minX -= _res;};
                while (minY > scene->minY()) {minY -= _res;};
                while (minZ > scene->minZ()) {minZ -= _res;};
            }

            // Declaring the output grids
            CT_Grid3D_Sparse<int>* hitGrid = CT_Grid3D_Sparse<int>::createGrid3DFromXYZCoords(minX, minY, minZ, scene->maxX(), scene->maxY(), scene->maxZ(), _res, -1, 0);

            CT_PointIterator itP(scene->pointCloudIndex()) ;
            while(itP.hasNext())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t indice;

                if (hitGrid->indexAtXYZ(point(0), point(1), point(2), indice))
                {
                    // Hits Computing
                    hitGrid->addValueAtIndex(indice, 1);
                } else {
                    qDebug() << "Le point n'est pas dans la grille";
                }
            }

            hitGrid->computeMinMax();
            group->addSingularItem(_outGrid, hitGrid);
        }
    }

    setProgress(99);
}

