﻿#ifndef ONF_STEPSELECTCLUSTERSINLOGS_H
#define ONF_STEPSELECTCLUSTERSINLOGS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/abstract/ct_abstractsingularitemdrawable.h"

class CT_AbstractItemGroup;

class ONF_StepSelectClustersInLogs: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepSelectClustersInLogs();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();

    void useManualMode(bool quit = false);

private:

    // Step parameters
    QHash<CT_AbstractItemDrawable*, CT_AbstractItemGroup*>      m_itemDrawableToAdd;
    QList<CT_AbstractItemDrawable*>                             m_validatedItems;
    DocumentInterface                                           *m_doc;
    int                                                         m_status;

    QHash<CT_AbstractItemDrawable*, quint64>                        m_clusterToLog;
    QMultiHash<quint64, CT_AbstractItemDrawable*>                   m_logToCluster;

    void recursiveRemoveGroup(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const;
};

#endif // ONF_STEPSELECTCLUSTERSINLOGS_H
