/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputecumulativeconvexhull.h"

ONF_StepComputeCumulativeConvexHull::ONF_StepComputeCumulativeConvexHull() : SuperClass()
{
    _cumulatedConvexHull = nullptr;
}

QString ONF_StepComputeCumulativeConvexHull::description() const
{
    return tr("Calculer enveloppe convexe cumulée");
}

QString ONF_StepComputeCumulativeConvexHull::detailledDescription() const
{
    return tr("Dans une boucle, créée un enveloppe convexe cumulée de l'ensembles des scènes parcourues. ");
}

QString ONF_StepComputeCumulativeConvexHull::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepComputeCumulativeConvexHull::createNewInstance() const
{
    return new ONF_StepComputeCumulativeConvexHull();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeCumulativeConvexHull::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène(s)"));

    manager.addResult(_inResultCounter, tr("Résultat compteur"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultCounter, _inZeroOrMoreRootGroupCounter);
    manager.addItem(_inZeroOrMoreRootGroupCounter, _inCounter, tr("Compteur"));
}

void ONF_StepComputeCumulativeConvexHull::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResult(_outResult, tr("Convex Hull (cumulative)"));
    manager.setRootGroup(_outResult, _outRootGroup);
    manager.addItem(_outRootGroup, _outConvexHull, tr("Convex Hull"));
}

void ONF_StepComputeCumulativeConvexHull::compute()
{
    QList<Eigen::Vector2d *> allPoints;

    if (_cumulatedConvexHull != nullptr)
    {
        const QVector<Eigen::Vector2d>& vertices = _cumulatedConvexHull->getVertices();
        for (int i = 0 ; i < _cumulatedConvexHull->getVerticesNumber() ; i++)
        {
            const Eigen::Vector2d& vert = vertices.at(i);
            Eigen::Vector2d* vertcpy = new Eigen::Vector2d(vert(0), vert(1));
            allPoints.append(vertcpy);
        }
        delete _cumulatedConvexHull;
        _cumulatedConvexHull = nullptr;
    }

    for (const CT_StandardItemGroup* group : _inGroup.iterateInputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : group->singularItems(_inScene))
        {
            // création de la liste complète des points
            CT_PointIterator itP(scene->pointCloudIndex());
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP.next().currentPoint();
                Eigen::Vector2d *point2D = new Eigen::Vector2d(point(0), point(1));
                allPoints.append(point2D);
            }
        }
    }

    CT_Polygon2DData::orderPointsByXY(allPoints);
    _cumulatedConvexHull = CT_Polygon2DData::createConvexHull(allPoints);

    if (_cumulatedConvexHull != nullptr)
    {
        for(CT_ResultGroup* result : _outResult.iterateOutputs())
        {
            CT_StandardItemGroup* outGrp = new CT_StandardItemGroup();
            result->addRootGroup(_outRootGroup, outGrp);

            CT_Polygon2D* convexHull = new CT_Polygon2D(static_cast<CT_Polygon2DData*>(_cumulatedConvexHull->copy()));
            outGrp->addSingularItem(_outConvexHull, convexHull);

            for (const CT_LoopCounter* counter : _inCounter.iterateInputs(_inResultCounter))
            {
                if (counter->currentTurn() == counter->nTurns())
                {
                    delete _cumulatedConvexHull;
                    _cumulatedConvexHull = nullptr;
                }
                if (counter != nullptr) {break;}
            }

        }
    }

}

