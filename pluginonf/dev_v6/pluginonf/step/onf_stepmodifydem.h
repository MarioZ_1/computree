/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPMODIFYDEM_H
#define ONF_STEPMODIFYDEM_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"

//Inclusion of actions
#include "actions/onf_actionmodifydem.h"

class CT_AbstractItemGroup;

class ONF_StepModifyDEM: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepModifyDEM();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

//    void preProcessCreateOutResultModelListProtected();

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();
    void useManualMode(bool quit);

private:


    // Step parameters

    CT_Scene*                               _scene;
    CT_Image2D<float>*                      _inDEM;
    CT_Image2D<float>*                      _outDEM;
    CT_Image2D<float>*                      _modifiedDEM;
    CT_AbstractImage2D* _inred;
    CT_AbstractImage2D* _ingreen;
    CT_AbstractImage2D* _inblue;

    DocumentInterface                                           *m_doc;
    int                                                         m_status;

};

#endif // ONF_STEPMODIFYDEM_H
