/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepselectcellsingrid3d.h"

#include "actions/onf_actionselectcellsingrid3d.h"

#include <QMessageBox>

ONF_StepSelectCellsInGrid3D::ONF_StepSelectCellsInGrid3D() : SuperClass()
{
    m_doc = nullptr;
    _refGrid = nullptr;
    _boolGrid = nullptr;
    setManual(true);
}

QString ONF_StepSelectCellsInGrid3D::description() const
{
    return tr("Séléctionner une partie de Grille 3D");
}

QString ONF_StepSelectCellsInGrid3D::detailledDescription() const
{
    return tr("Cette étape permet de générer une grille booléenne, représentant une séléction de cellules parmi celles de la grille de référence choisie en entrée.<br>"
              "Elle utilise un actionner, permettant de faire des séléction soit par plans horizontaux 2D, soit directement en 3D.<br>"
              "En sortie elle fournie également une copie de la grille d'entrée pour laquelle toute les cases non sélectionnées sont réinitialisées à la valeur 0. ");
}

QString ONF_StepSelectCellsInGrid3D::URL() const
{
    return tr("http://rdinnovation.onf.fr/projects/plugin-base/wiki/Fr_action_selectcells");
}

CT_VirtualAbstractStep* ONF_StepSelectCellsInGrid3D::createNewInstance() const
{
    return new ONF_StepSelectCellsInGrid3D();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectCellsInGrid3D::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroup *resultInModel = createNewInResultModel(DEF_resultIn,
                                                                  tr("Result"),
                                                                  tr(""),
                                                                  false);
    resultInModel->setZeroOrMoreRootGroup();
    resultInModel->addGroupModel("", DEF_groupIn);
    resultInModel->addItemModel(DEF_groupIn,
                                DEF_itemBaseGrid,
                                CT_AbstractGrid3D::staticGetType(),
                                tr("Grille"));
}

void ONF_StepSelectCellsInGrid3D::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_InAbstractResultModel *resultInModel = getInResultModel(DEF_resultIn);

    CT_OutAbstractResultModel* resultInModelOut = nullptr;
    CT_InAbstractSingularItemModel* baseGridModel = nullptr;
    CT_AbstractSingularItemDrawable* itemPrototype = nullptr;

    // check if model have choice (can be empty if the step want to create a default out model list)
    if(!resultInModel->getPossibilitiesSavedSelected().isEmpty())
        resultInModelOut = (CT_OutAbstractResultModel*)resultInModel->getPossibilitiesSavedSelected().first()->outModel();

    if(resultInModelOut != nullptr)
        baseGridModel = (CT_InAbstractSingularItemModel*)PS_MODELS->searchModel(DEF_itemBaseGrid, resultInModelOut, this);

    if((baseGridModel != nullptr) && !baseGridModel->getPossibilitiesSavedSelected().isEmpty())
        itemPrototype = (CT_AbstractSingularItemDrawable*) ((CT_OutAbstractSingularItemModel*) baseGridModel->getPossibilitiesSavedSelected().first()->outModel())->itemDrawable()->copy(nullptr, nullptr, CT_ResultCopyModeList() << CT_ResultCopyModeList::DontCopyItemDrawable);

    // if we don't have a possibility and we don't create a default out model, there was a problem
    if((itemPrototype == nullptr) && !isCreateDefaultOutModelActive())
        qFatal("Error creating out model in ONF_StepSelectCellsInGrid3D");

    // create the out result
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut,
                                                                  tr("Grille filtrée"),
                                                                  tr("Grille filtrée"));
    resultModel->setRootGroup(DEF_outGroup);
    resultModel->addItemModel(DEF_outGroup, DEF_outBoolGrid, new CT_Grid3D<bool>(), tr("Cases séléctionnées"));
    resultModel->addItemModel(DEF_outGroup, DEF_outFilteredGrid, itemPrototype, tr("Grille filtrée"));
}

void ONF_StepSelectCellsInGrid3D::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    // No parameter dialog for this step
}

void ONF_StepSelectCellsInGrid3D::compute()
{
    m_doc = nullptr;
    m_status = 0;

    CT_ResultGroup *resultIn = getInputResults().first();

    CT_ResultGroup *resultOut = getOutResultList().first();
    CT_OutStdSingularItemModel* filteredGridModel = (CT_OutStdSingularItemModel*)PS_MODELS->searchModelForCreation(DEF_outFilteredGrid, resultOut);

    // create a list of itemdrawable to add in the document
    CT_ResultGroupIterator itG(resultIn, this, DEF_groupIn);

    if (itG.hasNext())
    {
        const CT_AbstractItemGroup *groupIn = itG.next();

        CT_AbstractGrid3D *baseGrid = (CT_AbstractGrid3D*) groupIn->firstItemByINModelName(this, DEF_itemBaseGrid);
        if (baseGrid!=nullptr)
        {
            _boolGrid = new CT_Grid3D<bool>(DEF_outBoolGrid, resultOut,
                                            baseGrid->minX(), baseGrid->minY(), baseGrid->minZ(),
                                            baseGrid->xdim(), baseGrid->ydim(), baseGrid->zdim(),
                                            baseGrid->resolution(), false, false);
            _refGrid = baseGrid;

            CT_AbstractGrid3D *filteredGrid = (CT_AbstractGrid3D*) baseGrid->copy(filteredGridModel, resultOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);

            // request the manual mode
            requestManualMode();

            m_status = 1;
            requestManualMode();

            // Filter output grid, copied from reference grid
            for (size_t i = 0 ; i < _refGrid->nCells() ; i++)
            {
                if (!_boolGrid->valueAtIndex(i))
                {
                    filteredGrid->setValueAtIndexFromDouble(i, 0);
                }
            }

            CT_StandardItemGroup* outGroup = new CT_StandardItemGroup(DEF_outGroup, resultOut);
            outGroup->addSingularItem(_boolGrid);
            outGroup->addSingularItem(filteredGrid);
            resultOut->addGroup(outGroup);
        }
    }

}

void ONF_StepSelectCellsInGrid3D::initManualMode()
{
    if(m_doc == nullptr)
    {
        // create a new 3D document
        m_doc = getGuiContext()->documentManager()->new3DDocument();

        ONF_ActionSelectCellsInGrid3D *action = new ONF_ActionSelectCellsInGrid3D(_refGrid, _boolGrid);

        // check if don't exist in the action manager
        if(!getGuiContext()->actionsManager()->existAction(action->uniqueName()))
        {
            // if not we add it to the manager. The action is deleted by the manager.
            getGuiContext()->actionsManager()->addAction(action);
        }

        // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
        m_doc->setCurrentAction(action);
    }

    m_doc->removeAllItemDrawable();

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette étape de filtrage.\n"
                                                         "Seuls les voxels séléctionés (Rouges) seront conservés.\n"
                                                         "Laisser la souris au-dessus d'un bouton pour avoir des infos."), QMessageBox::Ok);
}

void ONF_StepSelectCellsInGrid3D::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
            // nothing to do, _boolGrid has already been modified in manual mode
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            getGuiContext()->documentManager()->closeDocument(m_doc);
            m_doc = nullptr;

            quitManualMode();
        }
    }
}
