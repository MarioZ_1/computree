/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepdilateboolgrid.h"

#include "tools/onf_computehitsthread.h"

#include <QFileInfo>

ONF_StepDilateBoolGrid::ONF_StepDilateBoolGrid() : SuperClass()
{
    _range = 1;
}

QString ONF_StepDilateBoolGrid::description() const
{
    // Gives the description to print in the GUI
    return tr("Dilatation d'une grille booléenne");
}

QString ONF_StepDilateBoolGrid::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepDilateBoolGrid::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepDilateBoolGrid();
}

void ONF_StepDilateBoolGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Grille"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inGrid, tr("Grille"));
}

void ONF_StepDilateBoolGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outGrid, tr("Grille filtrée"));
}

void ONF_StepDilateBoolGrid::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addInt(tr("Portée de dilatation"),tr("cellules"), 1, std::numeric_limits<int>::max(), _range);
}

void ONF_StepDilateBoolGrid::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Grid3D_Sparse<bool>* gridIn : group->singularItems(_inGrid))
        {
            if (isStopped()) {return;}

            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* outGrid = new CT_Grid3D_Sparse<bool>(gridIn->minX(), gridIn->minY(), gridIn->minZ(), gridIn->xdim(), gridIn->ydim(), gridIn->zdim(), gridIn->resolution(), false, false);

            QList<size_t> list;
            gridIn->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);

                if (gridIn->valueAtIndex(index))
                {
                    int colX, linY, levZ;
                    if (outGrid->indexToGrid(index, colX, linY, levZ))
                    {
                        if (gridIn->value(colX, linY, levZ))
                        {
                            int firstColx = colX - _range;
                            int lastColx  = colX + _range;
                            int firstLiny = linY - _range;
                            int lastLiny  = linY + _range;
                            int firstLinz = levZ - _range;
                            int lastLinz  = levZ + _range;

                            for (int xx = firstColx ; xx <= lastColx ; xx++)
                            {
                                for (int yy = firstLiny ; yy <= lastLiny ; yy++)
                                {
                                    for (int zz = firstLinz ; zz <= lastLinz ; zz++)
                                    {
                                        if (!outGrid->value(xx, yy, zz))
                                        {
                                            outGrid->setValue(xx, yy, zz, true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            outGrid->computeMinMax();
            group->addSingularItem(_outGrid, outGrid);
        }
    }

    setProgress(99);
}

