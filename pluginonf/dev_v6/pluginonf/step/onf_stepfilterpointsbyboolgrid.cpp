/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfilterpointsbyboolgrid.h"
#include "ct_log/ct_logmanager.h"

ONF_StepFilterPointsByBoolGrid::ONF_StepFilterPointsByBoolGrid() : SuperClass()
{
}

QString ONF_StepFilterPointsByBoolGrid::description() const
{
    return tr("Filtrer les points par une Grille Booléenne");
}

QString ONF_StepFilterPointsByBoolGrid::detailledDescription() const
{
    return tr("Cette étape teste pour chaque point des scènes d'entrée s'il est contenu dans une case \"vraie\" de la grille booléenne choisie. "
              "Si oui, le point est conservé. Sinon, il n'est pas conservé.<br>"
              "Plusieures scènes peuvent être traitées avec la même étape.<br>"
              "Chaque scène filtrée est ajoutée au groupe contenant la grille d'entrée."
              "Si le résultat d'entrée contient plusieurs grilles, une scène est produite pour chacune (sur la base du cumul de toutes les scènes d'entrée)");
}

CT_VirtualAbstractStep* ONF_StepFilterPointsByBoolGrid::createNewInstance() const
{
    return new ONF_StepFilterPointsByBoolGrid();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFilterPointsByBoolGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scènes à filtrer"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scènes"));

    manager.addResult(_inResultGrd, tr("Grille(s) de filtrage"));
    manager.setZeroOrMoreRootGroup(_inResultGrd, _inZeroOrMoreRootGroupGrd);
    manager.addGroup(_inZeroOrMoreRootGroupGrd, _inGroupGrd);
    manager.addItem(_inGroupGrd, _inGrid, tr("Grille(s) de filtrage"));
}

void ONF_StepFilterPointsByBoolGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResultGrd);
    manager.addItem(_inGroupGrd, _outScene, tr("Scène filtrée"));
}

void ONF_StepFilterPointsByBoolGrid::compute()
{
    int gridNum = 1;

    for (CT_StandardItemGroup* group : _inGroupGrd.iterateOutputs(_inResultGrd))
    {
        if (isStopped()) {return;}

        const CT_Grid3D_Sparse<bool> *boolGrid = group->singularItem(_inGrid);

        if (boolGrid != nullptr)
        {
            // Indices de la scène filtrée
            CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
            resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

            int sceneNum = 1;

            for (const CT_AbstractItemDrawableWithPointCloud* inScene : _inScene.iterateInputs(_inResult))
            {
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Grille %1, Scène %2:")).arg(gridNum++).arg(sceneNum++));

                CT_PointIterator itP(inScene->pointCloudIndex());
                size_t n_points = itP.size();

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène %1 points...")).arg(n_points));

                size_t i = 0;

                size_t nbOfFilteredPoints = 0;
                while(itP.hasNext())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    double x = point(0);
                    double y = point(1);
                    double z = point(2);

                    if (boolGrid->valueAtXYZ(x, y, z))
                    {
                        resPointCloudIndex->addIndex(itP.cIndex());
                        ++nbOfFilteredPoints;
                    }

                    ++i;
                    if (i % 1000 == 0) {setProgress((float(i) / float(n_points)) * 99.0f);}
                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("...%1 points ont été conservés")).arg(nbOfFilteredPoints));
            }

            if (resPointCloudIndex->size() > 0)
            {
                // creation et ajout de la scene
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);
                CT_Scene *outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));

                outScene->updateBoundingBox();
                group->addSingularItem(_outScene, outScene);
            }
        }
    }
}

