/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPSELECTSCENEFOREACHPOSITION_H
#define ONF_STEPSELECTSCENEFOREACHPOSITION_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_circle2d.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "actions/onf_actionselectsceneforeachposition.h"

class ONF_StepSelectSceneForEachPosition: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepSelectSceneForEachPosition();

    ~ONF_StepSelectSceneForEachPosition();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();
    void useManualMode(bool quit = false);

private:

    // Step parameters
    double              _distThrehold;
    bool              _interactiveCorrection;

    ONF_ActionSelectSceneForEachPosition_dataContainer*     _dataContainer;



    DocumentInterface*      _m_doc;
    int                     _m_status;

    void positionMatching(ONF_ActionSelectSceneForEachPosition_dataContainer *dataContainer);

    static bool lessThan(ONF_ActionSelectSceneForEachPosition_treePosition* i1, ONF_ActionSelectSceneForEachPosition_treePosition* i2)
    {
        if (i1 == nullptr || i2 == nullptr ) {return true;}

        return i1->_dbh > i2->_dbh;
    }

};

#endif // ONF_STEPSELECTSCENEFOREACHPOSITION_H
