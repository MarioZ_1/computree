#ifndef ONF_STEPFILTERMAXIMABYNEIGHBOURHOOD02_H
#define ONF_STEPFILTERMAXIMABYNEIGHBOURHOOD02_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/ct_grid3d_points.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "ct_math/delaunay2d/ct_delaunaytriangulation.h"

class ONF_StepFilterMaximaByNeighbourhood02: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFilterMaximaByNeighbourhood02();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double      _scoreThreshold;
    double      _minRadius;
    double      _maxRadius;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float>>                                          _inImage;
    CT_HandleInSingularItem<CT_Image2D<qint32>>                                         _inMaxima;

    CT_HandleInResultGroup<0,1>                                                         _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                                            _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float>>                                      _inDTM;

    CT_HandleOutSingularItem<CT_Image2D<qint32>>                                        _filteredMaxima;


    // Declaration of autoRenames Variables (groups or items added to In models copies)

    double computeScore(const CT_Image2D<float> *heightImage, CT_DelaunayVertex *baseVertex, CT_DelaunayVertex *neighbourVertex);
};

#endif // ONF_STEPFILTERMAXIMABYNEIGHBOURHOOD02_H
