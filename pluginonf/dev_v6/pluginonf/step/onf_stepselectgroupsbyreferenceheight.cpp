/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepselectgroupsbyreferenceheight.h"

#include <QMessageBox>

ONF_StepSelectGroupsByReferenceHeight::ONF_StepSelectGroupsByReferenceHeight() : SuperClass()
{
    _hRef = 1.3;
}

QString ONF_StepSelectGroupsByReferenceHeight::description() const
{
    return tr("Filter des items dans une tranche de hauteur depuis un MNT");
}

CT_VirtualAbstractStep* ONF_StepSelectGroupsByReferenceHeight::createNewInstance() const
{
    return new ONF_StepSelectGroupsByReferenceHeight();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectGroupsByReferenceHeight::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inScres = createNewInResultModelForCopy(_inScres, tr("Items"));
    res_inScres->setZeroOrMoreRootGroup();
    res_inScres->addGroupModel("", _inScBase, CT_AbstractItemGroup::staticGetType(), tr("Groupe de base"));
    res_inScres->addItemModel(_inScBase, _inDtmValue, CT_ReferencePoint::staticGetType(), tr("Z MNT"));
    res_inScres->addGroupModel(_inScBase, _inGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inScres->addItemModel(_inGroup, _inItem, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
}

void ONF_StepSelectGroupsByReferenceHeight::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    createNewOutResultModelToCopy(_inScres);
}

void ONF_StepSelectGroupsByReferenceHeight::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Hauteur de référence"), "m", 0, std::numeric_limits<double>::max(), 2, _hRef);

}

void ONF_StepSelectGroupsByReferenceHeight::compute()
{
    CT_ResultGroup *resultOut_R = getOutResultList().first();

    QList<CT_AbstractItemGroup*> groupsToRemove;

    CT_ResultGroupIterator itG(resultOut_R, this, _inScBase);
    while(itG.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup *groupOut_G = itG.next();

        if (groupOut_G != nullptr)
        {

            CT_ReferencePoint *dtmRefPoint = (CT_ReferencePoint*) groupOut_G->firstItemByINModelName(this, _inDtmValue);

            if (dtmRefPoint != nullptr)
            {
                float href = dtmRefPoint->centerZ() + _hRef;

                float zmin = std::numeric_limits<float>::max();
                float zmax = -std::numeric_limits<float>::max();

                CT_GroupIterator itG2(groupOut_G, this, _inGroup);
                while(itG2.hasNext() && !isStopped())
                {

                    const CT_AbstractItemGroup *groupOut_G2 = itG2.next();

                    if (groupOut_G2 != nullptr)
                    {
                        CT_AbstractSingularItemDrawable *itemOut_I = groupOut_G2->firstItemByINModelName(this, _inItem);

                        if (itemOut_I != nullptr)
                        {
                            if (itemOut_I->centerZ() < zmin) {zmin = itemOut_I->centerZ();}
                            if (itemOut_I->centerZ() > zmax) {zmax = itemOut_I->centerZ();}
                        }
                    }
                }

                if (zmin > href || zmax < href)
                {
                    groupsToRemove.append((CT_AbstractItemGroup*)groupOut_G);
                }
            }

        }

    }

    // we remove the parent group of all ItemDrawable that must be deleted from the out result
    // and all groups that don't contains a ItemDrawable researched
    QListIterator<CT_AbstractItemGroup*> itE(groupsToRemove);

    while(itE.hasNext())
    {
        CT_AbstractItemGroup *group = itE.next();
        recursiveRemoveGroup(group->parentGroup(), group);
    }

}

void ONF_StepSelectGroupsByReferenceHeight::recursiveRemoveGroup(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const
{
    if(parent != nullptr)
    {
        parent->removeGroup(group);

        if(parent->isEmptyOfGroups())
            recursiveRemoveGroup(parent->parentGroup(), parent);
    }
    else
    {
        ((CT_ResultGroup*)group->result())->removeGroupSomethingInStructure(group);
    }
}

