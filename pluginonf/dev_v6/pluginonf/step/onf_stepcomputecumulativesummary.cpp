/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputecumulativesummary.h"

#include <QFile>
#include <QTextStream>

ONF_StepComputeCumulativeSummary::ONF_StepComputeCumulativeSummary() : SuperClass()
{
    _fileName << "summary.txt";
}

QString ONF_StepComputeCumulativeSummary::description() const
{
    return tr("Export summary of metrics");
}

QString ONF_StepComputeCumulativeSummary::detailledDescription() const
{
    return tr("Compute statistics (sum, mean, min, max, and NA number) for selected attributes. ");
}

QString ONF_StepComputeCumulativeSummary::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepComputeCumulativeSummary::createNewInstance() const
{
    return new ONF_StepComputeCumulativeSummary();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeCumulativeSummary::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Items"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inItem, tr("Item"));
    manager.addItemAttribute(_inItem, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("Attribut"));

    manager.addResult(_inResultCounter, tr("Résultat compteur"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultCounter, _inZeroOrMoreRootGroupCounter);
    manager.addItem(_inZeroOrMoreRootGroupCounter, _inCounter, tr("Compteur"));
}

void ONF_StepComputeCumulativeSummary::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepComputeCumulativeSummary::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addFileChoice(tr("Choose Export file"), CT_FileChoiceButton::OneNewFile, "Text file (*.txt)", _fileName);
}

void ONF_StepComputeCumulativeSummary::compute()
{
    bool last_turn = false;
    bool first_turn = true;
    bool counterok = false;

    for (const CT_LoopCounter* counter : _inCounter.iterateInputs(_inResultCounter))
    {
        counterok = true;
        if (counter->currentTurn() > 1)
        {
            first_turn = false;
        }

        if (counter->currentTurn() == counter->nTurns())
        {
            last_turn = true;
        }
    }

    if (first_turn)
    {
        _groupNumber = 0;
    }

    // parcours des item
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        _groupNumber++;

        for (const CT_AbstractSingularItemDrawable* item : group->singularItems(_inItem))
        {
            if (item != nullptr)
            {
                CT_OutAbstractModel  *itemModel = item->model();

                for (const CT_AbstractItemAttribute* attribute : item->itemAttributes(_inAtt.inModelForSelectedPossibilities(_inResult)))
                {
                    if (attribute != nullptr)
                    {
                        CT_OutAbstractModel* attrModel = attribute->model();

                        if (!_namesItem.contains(attrModel))
                        {
                            _namesItem.insert(attrModel, itemModel->displayableName());
                            _namesAtt.insert(attrModel, attrModel->displayableName());
                        }

                        bool ok;
                        double attVal = attribute->toDouble(item, &ok);

                        if (ok) {_dataMap.insert(attrModel, attVal);}
                    }
                }
            }
        }
    }

    if (_fileName.size() > 0 && _fileName.first() != "" && (!counterok || last_turn))
    {
        QFile file(_fileName.first());

        if(file.open(QFile::WriteOnly))
        {
            QTextStream stream(&file);

            stream << tr("Item\tChamp\tMoyenne\tMin\tMax\tSomme\tnb_Val\tnb_NA\n");

            QMapIterator<CT_OutAbstractModel*, QString > itMap(_namesAtt);

            while (itMap.hasNext())
            {
                itMap.next();
                CT_OutAbstractModel* key = itMap.key();

                QString attName = itMap.value();
                QString itemName = _namesItem.value(key);

                QList<double> values = _dataMap.values(key);

                double sum = 0;
                double mean = 0;
                double max = -std::numeric_limits<double>::max();
                double min = std::numeric_limits<double>::max();
                int cpt = 0;
                int NAcpt = _groupNumber - values.size();

                for (int i = 0 ; i < values.size() ; i++)
                {
                    double val = values.at(i);

                    if (std::isnan(val))
                    {
                        NAcpt++;
                    } else {
                        cpt++;
                        sum += val;
                        if (val > max) {max = val;}
                        if (val < min) {min = val;}
                    }
                }

                if (cpt > 0) {mean = sum / cpt;}

                QString sumStr = QString::number(sum, 'f');
                QString meanStr = QString::number(mean, 'f');
                QString maxStr = QString::number(max, 'f');
                QString minStr = QString::number(min, 'f');

                if (cpt == 0)
                {
                    sumStr = "";
                    meanStr = "";
                    maxStr = "";
                    minStr = "";
                }

                stream << itemName << "\t" << attName << "\t" << meanStr << "\t" << minStr << "\t" << maxStr  << "\t" << sumStr << "\t" << cpt << "\t" << NAcpt << "\n";
            }

            file.close();
        }
    }

}

