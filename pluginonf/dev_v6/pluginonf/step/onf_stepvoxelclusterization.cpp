/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepvoxelclusterization.h"

#include <QFileInfo>

ONF_StepVoxelClusterization::ONF_StepVoxelClusterization() : SuperClass()
{
    _res = 0.1;
    _gridMode = 0;
    _xBase = 0;
    _yBase = 0;
    _zBase = 0;
}

QString ONF_StepVoxelClusterization::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Clusterization selon une grille voxel");
}

QString ONF_StepVoxelClusterization::detailledDescription() const
{
    return tr("TO DO");
}

CT_VirtualAbstractStep* ONF_StepVoxelClusterization::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepVoxelClusterization();
}

void ONF_StepVoxelClusterization::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy * resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scene(s)"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup, CT_StandardItemGroup::staticGetType(), tr("Groupe"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Item à clusteriser"));
}

void ONF_StepVoxelClusterization::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(resultModel != nullptr) {
        resultModel->addGroupModel(DEF_SearchInGroup, m_outClusterGroupModel, new CT_StandardItemGroup(), tr("Cluster (Grp)"));
        resultModel->addItemModel(m_outClusterGroupModel, m_outClusterModel, new CT_PointCluster(), tr("Cluster (Points)"));
    }
}

void ONF_StepVoxelClusterization::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution de la grille"),tr("meters"),0.0001,10000,2, _res );

    postInputConfigDialog->addText(tr("Callage du coin (minX, minY, minZ) :"),"", "");

    CT_ButtonGroup &bg_gridMode = postInputConfigDialog->addButtonGroup(_gridMode);
    postInputConfigDialog->addExcludeValue("", "", tr("Sur la boite englobante de la scène"), bg_gridMode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("Par rapport aux coordonnées suivantes :"), bg_gridMode, 1);

    postInputConfigDialog->addDouble(tr("Coordonnée X :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _xBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Y :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _yBase);
    postInputConfigDialog->addDouble(tr("Coordonnée Z :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _zBase);
}

void ONF_StepVoxelClusterization::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    // iterate over all groups
    while(!isStopped() && it.hasNext())
    {

        CT_StandardItemGroup *group = (CT_StandardItemGroup*)it.next();
        const CT_Scene* scene = (CT_Scene*)group->firstItemByINModelName(this, DEF_SearchInScene);

        if (scene!=nullptr)
        {
            float minX = _xBase;
            float minY = _yBase;
            float minZ = _zBase;

            if (_gridMode == 0)
            {
                minX = scene->minX();
                minY = scene->minY();
                minZ = scene->minZ();
            } else {

                while (minX < scene->minX()) {minX += _res;};
                while (minY < scene->minY()) {minY += _res;};
                while (minZ < scene->minZ()) {minZ += _res;};

                while (minX > scene->minX()) {minX -= _res;};
                while (minY > scene->minY()) {minY -= _res;};
                while (minZ > scene->minZ()) {minZ -= _res;};
            }

            // Declaring the output grids
            CT_Grid3D_Sparse<int>* hitGrid = CT_Grid3D_Sparse<int>::createGrid3DFromXYZCoords(nullptr, nullptr,
                                                                                              minX, minY, minZ,
                                                                                              scene->maxX(), scene->maxY(), scene->maxZ(),
                                                                                              _res, -1, 0);

            QList<CT_PointCluster*> clusters;
            clusters.append(nullptr);
            int clusterNumber = 1;

            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();
            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                int val = hitGrid->valueAtXYZ(point(0), point(1), point(2));

                if (val == 0)
                {
                    val = clusterNumber;
                    hitGrid->setValueAtXYZ(point(0), point(1), point(2), clusterNumber++);
                    clusters.append(new CT_PointCluster(m_outClusterModel.completeName(), outResult));
                }
                clusters.at(val)->addPoint(index);
            }

            for (int i = 1 ; i < clusters.size() ; i++)
            {
                CT_StandardItemGroup* grpCl = new CT_StandardItemGroup(m_outClusterGroupModel.completeName(), outResult);
                group->addGroup(grpCl);
                grpCl->addSingularItem(clusters.at(i));
            }

            delete hitGrid;
        }
    }

    setProgress(99);
}
