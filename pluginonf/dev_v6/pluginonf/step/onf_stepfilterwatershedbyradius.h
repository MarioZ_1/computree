#ifndef ONF_STEPFILTERWATERSHEDBYRADIUS_H
#define ONF_STEPFILTERWATERSHEDBYRADIUS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepFilterWatershedByRadius: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFilterWatershedByRadius();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:    
    double      _radius;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGrp;
    CT_HandleInSingularItem<CT_Image2D<float> >                                         _inImage;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                        _inClusters;

    CT_HandleOutSingularItem<CT_Image2D<qint32> >                                       _outClusters;


};

#endif // ONF_STEPFILTERWATERSHEDBYRADIUS_H
