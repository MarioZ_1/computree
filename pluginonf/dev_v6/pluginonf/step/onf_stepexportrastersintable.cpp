/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepexportrastersintable.h"

#include "ct_log/ct_logmanager.h"

#include "ct_triangulation/ct_delaunayt.h"
#include "ct_triangulation/ct_nodet.h"
#include "ct_triangulation/ct_trianglet.h"

#include "ct_itemdrawable/tools/image2dtools/ct_image2dnaturalneighboursinterpolator.h"

#include <QtConcurrent>
#include <math.h>
#include <stdlib.h>

#include <QTextStream>

ONF_StepExportRastersInTable::ONF_StepExportRastersInTable() : SuperClass()
{
    _noprefix = true;
}

QString ONF_StepExportRastersInTable::description() const
{
    return tr("Exporter multi-raster dans une table");
}

QString ONF_StepExportRastersInTable::detailledDescription() const
{
    return tr("Cette étape permet d'exporter une série de rasters (cohérents) dans une table."
              "La résolution est celle du raster avec la résolution la plus faible."
              "Une emprise peut être fournie pour filtrer les données à exporter.");
}

CT_VirtualAbstractStep* ONF_StepExportRastersInTable::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepExportRastersInTable();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepExportRastersInTable::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Rasters"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inRaster, tr("Rasters"));

    manager.addResult(_inResultFootprint, tr("Emprise (optionnel)"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultFootprint, _inZeroOrMoreRootGroupFootprint);
    manager.addGroup(_inZeroOrMoreRootGroupFootprint, _inGroupFootprint);
    manager.addItem(_inGroupFootprint, _inFootprint, tr("Emprise"));

    manager.addResult(_inResultCounter, tr("Compteur (optionnel - nom adaptatif)"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultCounter, _inZeroOrMoreRootGroupCounter);
    manager.addGroup(_inZeroOrMoreRootGroupCounter, _inGroupCounter);
    manager.addItem(_inGroupCounter, _inCounter, tr("Compteur"));
}

void ONF_StepExportRastersInTable::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addFileChoice(tr("Choix du fichier d'export"), CT_FileChoiceButton::OneNewFile, tr("Fichier texte (*.txt)"), _fileName);
    postInputConfigDialog->addBool("", "", tr("Pas de préfixe (si export adaptatif)"), _noprefix);
}

void ONF_StepExportRastersInTable::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepExportRastersInTable::compute()
{
    Eigen::Vector2d minFootprint, maxFootprint;
    minFootprint(0) = std::numeric_limits<double>::max();
    minFootprint(1) = std::numeric_limits<double>::max();
    maxFootprint(0) = -std::numeric_limits<double>::max();
    maxFootprint(1) = -std::numeric_limits<double>::max();

    QList<const CT_AbstractAreaShape2D*> emprises;

    for (const CT_AbstractAreaShape2D* emprise : _inFootprint.iterateInputs(_inResultFootprint))
    {
        if (isStopped()) {return;}

        emprises.append(emprise);

        Eigen::Vector3d min, max;
        emprise->boundingBox(min, max);

        if (min(0) < minFootprint(0)) {minFootprint(0) = min(0);}
        if (min(1) < minFootprint(1)) {minFootprint(1) = min(1);}
        if (max(0) > maxFootprint(0)) {maxFootprint(0) = max(0);}
        if (max(1) > maxFootprint(1)) {maxFootprint(1) = max(1);}
    }


    QString adaptativeName = "";
    for (const CT_LoopCounter* counter : _inCounter.iterateInputs(_inResultCounter))
    {
        if (isStopped()) {return;}

        adaptativeName = counter->turnName();

        QFileInfo finf(adaptativeName);
        adaptativeName = finf.baseName();

        if (counter != nullptr) {break;}
    }


    double xmin = 0;
    double xmax = 0;
    double ymin = 0;
    double ymax = 0;
    double resolution = 0;

    bool first = true;
    bool empriseDiff = false;
    bool resDiff = false;

    QList<const CT_AbstractImage2D*> rasters;

    for (const CT_AbstractImage2D* raster : _inRaster.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        rasters.append(raster);

        if (first)
        {
            xmin = raster->minX();
            xmax = raster->maxX();
            ymin = raster->minY();
            ymax = raster->maxY();
            resolution = raster->resolution();
            first = false;
        } else {
            if (!qFuzzyCompare(xmin, raster->minX())     ||
                    !qFuzzyCompare(xmax, raster->maxX()) ||
                    !qFuzzyCompare(ymin, raster->minY()) ||
                    !qFuzzyCompare(ymax, raster->maxY()))
            {
                empriseDiff = true;

                if (raster->minX() < xmin) {xmin = raster->minX();}
                if (raster->maxX() > xmax) {xmax = raster->maxX();}
                if (raster->minY() < ymin) {ymin = raster->minY();}
                if (raster->maxY() > ymax) {ymax = raster->maxY();}
            }
            if (!qFuzzyCompare(resolution, raster->resolution()))
            {
                resDiff = true;

                if (raster->resolution() < resolution) {resolution = raster->resolution();}
            }
        }
    }


    double halfRes = resolution / 2.0;

    if (empriseDiff) {PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Tous les rasters n'ont pas la même emprise"));}
    if (resDiff) {PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Tous les rasters n'ont pas la même résolution"));}

    if (!rasters.isEmpty())
    {
        QString exportFileName = _fileName.first();
        if (!adaptativeName.isEmpty())
        {
            QFileInfo fileInfo(_fileName.first());
            if (_noprefix)
            {
                exportFileName = QString("%1/%2.txt").arg(fileInfo.absolutePath()).arg(adaptativeName);
            } else {
                exportFileName = QString("%1/%2_%3.txt").arg(fileInfo.absolutePath()).arg(fileInfo.baseName()).arg(adaptativeName);
            }
        }

        QFile f(exportFileName);
        if (f.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream stream(&f);

            for (int i = 0 ; i < rasters.size() ; i++)
            {
                const CT_AbstractImage2D* raster = rasters.at(i);
                stream << raster->displayableName();
                if (i < (rasters.size() - 1))
                {
                    stream << "\t";
                } else {
                    stream << "\n";
                }
            }

            int nbcells = int(((xmax - xmin) / resolution) * ((ymax - ymin) / resolution));
            int index = 0;
            for (double xx = xmin + halfRes ; xx < xmax ; xx += resolution)
            {
                for (double yy = ymin + halfRes ; yy < ymax ; yy += resolution)
                {
                    ++index;
                    bool keep = true;
                    if (!emprises.isEmpty())
                    {
                        keep= false;
                        if (xx >= minFootprint(0) &&
                            xx <= maxFootprint(0) &&
                            yy >= minFootprint(1) &&
                            yy <= maxFootprint(1))
                        {
                            for (int ff = 0 ; ff < emprises.size() && !keep ; ff++)
                            {
                                if (emprises.at(ff)->contains(xx, yy))
                                {
                                    keep = true;
                                }
                            }
                        }
                    }

                    if (keep)
                    {
                        for (int i = 0 ; i < rasters.size() ; i++)
                        {
                            const CT_AbstractImage2D* raster = rasters.at(i);
                            size_t indexRast;
                            raster->indexAtCoords(xx, yy, indexRast);

                            stream << raster->valueAtIndexAsDouble(indexRast);
                            if (i < (rasters.size() - 1))
                            {
                                stream << "\t";
                            } else {
                                stream << "\n";
                            }
                        }
                    }
                    setProgress(90*int(double(index)/double(nbcells)));
                }
            }

            f.close();
        }

    }
    setProgress(100);

}
