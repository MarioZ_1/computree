#ifndef ONF_STEPCOMPUTESCANDIRECTION_H
#define ONF_STEPCOMPUTESCANDIRECTION_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_scanpath.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"

class ONF_StepComputeScanDirection : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepComputeScanDirection();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_StdLASPointsAttributesContainer>                         _inLAS;

    CT_HandleInResultGroup<>                                            _inResultTraj;
    CT_HandleInStdZeroOrMoreGroup                                      _inZeroOrMoreRootGroupTraj;
    CT_HandleInStdGroup<>                                               _inGroupTraj;
    CT_HandleInSingularItem<CT_ScanPath>                                _inTraj;

    CT_HandleOutSingularItem<CT_PointsAttributesNormal>                                 _outDir;


};

#endif // ONF_STEPCOMPUTESCANDIRECTION_H
