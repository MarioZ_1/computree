#ifndef ONF_STEPKEEPINTERSECTINGITEMS_H
#define ONF_STEPKEEPINTERSECTINGITEMS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

class ONF_StepKeepIntersectingItems : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;
public:
    ONF_StepKeepIntersectingItems();

    ~ONF_StepKeepIntersectingItems();

    QString description() const;

    QString detailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    virtual void declareInputModels(CT_StepInModelStructureManager& manager) final;

    virtual void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    virtual void compute() final;

private:

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItem;

    CT_HandleInResultGroup<>                                                            _inResultFiltering;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupFiltering;
    CT_HandleInStdGroup<>                                                               _inGroupFiltering;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItemFiltering;


    QList<CT_AbstractSingularItemDrawable*>               _ids;

};

#endif // ONF_STEPKEEPINTERSECTINGITEMS_H
