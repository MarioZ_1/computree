/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputeattributemapfromclusters.h"

ONF_StepComputeAttributeMapFromClusters::ONF_StepComputeAttributeMapFromClusters() : SuperClass()
{
    _naValue = -9999;
    _defaultValue = 0;
}

QString ONF_StepComputeAttributeMapFromClusters::description() const
{
    return tr("Mapper attribut par clusters (raster)");
}

QString ONF_StepComputeAttributeMapFromClusters::detailledDescription() const
{
    return tr("Cette étape produit un raster, ou tous les pixels appartenant à un même cluster contiennent la valeur correspondante pour l'attribut correspondant."
              "<br>En entrée, elle nécessite :<br>"
              "- Un raster avec les clusters<br>"
              "- A coté une liste de groupes \"clusters\" contenant un champ IdCluster et l'attribut souhaité<br>"
              "Optionnellement il est possible de fournir également un raster dont les valeurs seront utilisée hors clusters. ");
}

QString ONF_StepComputeAttributeMapFromClusters::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepComputeAttributeMapFromClusters::createNewInstance() const
{
    return new ONF_StepComputeAttributeMapFromClusters();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeAttributeMapFromClusters::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Clusters"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inMainGrp, tr("Groupe"));
    manager.addItem(_inMainGrp, _inClusters, tr("Raster (Clusters)"));
    manager.addItem(_inMainGrp, _inAttrib, tr("Raster (attribut)"));

    manager.addGroup(_inMainGrp, _inGrp, tr("Groupe"));
    manager.addItem(_inGrp, _inItem, tr("Item"));
    manager.addItemAttribute(_inItem, _inItemID, CT_AbstractCategory::DATA_VALUE, tr("IDCluster"));
    manager.addItemAttribute(_inItem, _inItemAtt, CT_AbstractCategory::DATA_VALUE, tr("Attribut"));


}

void ONF_StepComputeAttributeMapFromClusters::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inMainGrp, _outRaster, tr("Carte d'attribut"));
}

void ONF_StepComputeAttributeMapFromClusters::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Valeur manquante dans le raster de sortie"), "", -1e+09, 1e+09, 2, _naValue);
    postInputConfigDialog->addDouble(tr("Valeur par défaut dans le raster de sortie"), "", -1e+09, 1e+09, 2, _defaultValue);
}

void ONF_StepComputeAttributeMapFromClusters::compute()
{
    for (CT_StandardItemGroup* groupMain : _inMainGrp.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<qint32>* clustersIn : groupMain->singularItems(_inClusters))
        {
            if (isStopped()) {return;}

            const CT_AbstractImage2D* attribIn = groupMain->singularItem(_inAttrib);

            CT_Image2D<double>* attMap = new CT_Image2D<double>(clustersIn->minX(), clustersIn->minY(), clustersIn->xdim(), clustersIn->ydim(), clustersIn->resolution(), clustersIn->level(), _naValue, _defaultValue);
            groupMain->addSingularItem(_outRaster, attMap);

            QMap<qint32, double> attValsMap;

            for (const CT_StandardItemGroup* grp : groupMain->groups(_inGrp))
            {
                for (const CT_AbstractSingularItemDrawable* itemWithAttribute : grp->singularItems(_inItem))
                {
                    if (isStopped()) {return;}

                    const CT_AbstractItemAttribute* attId = itemWithAttribute->itemAttribute(_inItemID);
                    const CT_AbstractItemAttribute* attAttribute = itemWithAttribute->itemAttribute(_inItemAtt);


                    qint32 itemID = -1;
                    if (attId != nullptr) {itemID = attId->toInt(itemWithAttribute, nullptr);}

                    double attributeValue = 0;
                    if (attAttribute != nullptr) {attributeValue = attAttribute->toDouble(itemWithAttribute, nullptr);

                        if (itemID >= 0) {attValsMap.insert(itemID, attributeValue);}
                    }
                }
            }

            setProgress(20.0f);

            for (int xx = 0 ; xx < clustersIn->xdim() ; xx++)
            {
                for (int yy = 0 ; yy < clustersIn->ydim() ; yy++)
                {
                    size_t index;
                    if (clustersIn->index(xx, yy, index))
                    {
                        qint32 cluster = clustersIn->value(xx, yy);

                        double attVal = attValsMap.value(cluster, _defaultValue);
                        if (cluster <= 0 && attribIn != nullptr)
                        {
                            attVal = attribIn->valueAtIndexAsDouble(index);
                        }

                        attMap->setValue(xx, yy, attVal);
                    } else {
                        qDebug() << "Problème";
                    }
                }
                setProgress(float(20.0 + (xx / clustersIn->xdim()) * 70.0));
            }

            attMap->computeMinMax();

            setProgress(100.0f);
        }
    }
}

