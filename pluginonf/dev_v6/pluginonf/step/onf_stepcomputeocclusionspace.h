/****************************************************************************

 Copyright (C) 2010-2016 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of Computree version 2.0.

 Computree is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Computree is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Computree.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef ONF_STEPCOMPUTEOCCLUSIONSSPACE_H
#define ONF_STEPCOMPUTEOCCLUSIONSSPACE_H

#include "ct_step/abstract/ct_abstractstep.h"                    // The step inherits from ct_abstractstep

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_scanner.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_itemdrawable/ct_beam.h"
#include "ct_itemdrawable/tools/gridtools/ct_grid3dwootraversalalgorithm.h"

class ONF_StepComputeOcclusionsSpace : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:
    ONF_StepComputeOcclusionsSpace();

    virtual QString description() const;

    QString detailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance() const final;

protected:
    virtual void declareInputModels(CT_StepInModelStructureManager& manager) final;

    virtual void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    virtual void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    virtual void compute() final;

private:

    double          _res;
    double          _distThreshold;
    int             _gridMode;
    double          _xBase;
    double          _yBase;
    double          _zBase;

    int          _xDim;
    int          _yDim;
    int          _zDim;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_Scanner>                                                 _inScanner;

    CT_HandleOutSingularItem<CT_Grid3D_Sparse<int>>                                     _outGrid;


};

#endif // ONF_STEPCOMPUTEOCCLUSIONSSPACE_H
