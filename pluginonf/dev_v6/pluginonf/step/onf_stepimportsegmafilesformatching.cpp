/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepimportsegmafilesformatching.h"
#include "ct_log/ct_logmanager.h"

#include <QFile>
#include <QTextStream>

ONF_StepImportSegmaFilesForMatching::ONF_StepImportSegmaFilesForMatching() : CT_AbstractStepCanBeAddedFirst()
{
}

QString ONF_StepImportSegmaFilesForMatching::description() const
{
    return tr("Fichiers SEGMA : un de ref, un à transformer");
}

QString ONF_StepImportSegmaFilesForMatching::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepImportSegmaFilesForMatching::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepImportSegmaFilesForMatching::createNewInstance() const
{
    return new ONF_StepImportSegmaFilesForMatching();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepImportSegmaFilesForMatching::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    manager.setNotNeedInputResult();
}

void ONF_StepImportSegmaFilesForMatching::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResult(_outResultRef, tr("Positions de référence"));
    manager.setRootGroup(_outResultRef, _outGrpRef, tr("Groupe"));
    manager.addItem(_outGrpRef, _outRef, tr("Position de référence"));
    manager.addItemAttribute(_outRef, _outRefVal, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_HEIGHT), tr("Valeur"));
    manager.addItemAttribute(_outRef, _outRefID, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDsegma"));

    manager.addResult(_outResultTrans, tr("Positions à transformer"));
    manager.setRootGroup(_outResultTrans, _outGrpTrans, tr("Groupe"));
    manager.addItem(_outGrpTrans, _outTrans, tr("Position à transformer"));
    manager.addItemAttribute(_outTrans, _outTransVal, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_HEIGHT), tr("Valeur"));
    manager.addItemAttribute(_outTrans, _outTransID, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDsegma"));
}

void ONF_StepImportSegmaFilesForMatching::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addFileChoice(tr("Fichier SEGMA des positions de référence"), CT_FileChoiceButton::OneExistingFile, "Fichier ascii (*.*)", _refFile);
    postInputConfigDialog->addFileChoice(tr("Fichier SEGMA des positions à transformer"), CT_FileChoiceButton::OneExistingFile, "Fichier ascii (*.*)", _transFile);
}

void ONF_StepImportSegmaFilesForMatching::compute()
{

    for (CT_ResultGroup* resultRef : _outResultRef.iterateOutputs())
    {
        if(QFile::exists(_refFile.first()))
        {
            QFile fRef(_refFile.first());

            if (fRef.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QTextStream stream(&fRef);

                stream.readLine();

                size_t cpt = 1;
                while (!stream.atEnd())
                {
                    QString line = stream.readLine();
                    cpt++;
                    if (!line.isEmpty())
                    {
                        QStringList values = line.split(",");
                        if (values.size() >= 8)
                        {
                            bool okX, okY, okVal;
                            double x = values.at(3).toDouble(&okX);
                            double y = values.at(4).toDouble(&okY);
                            float val = values.at(7).toFloat(&okVal);
                            QString id = values.at(0);

                            if (okX && okY && okVal)
                            {
                                CT_StandardItemGroup* grpRef= new CT_StandardItemGroup();
                                resultRef->addRootGroup(_outGrpRef, grpRef);

                                CT_Point2D* item_ref = new CT_Point2D(new CT_Point2DData(x,y));
                                grpRef->addSingularItem(_outRef, item_ref);

                                item_ref->addItemAttribute(_outRefVal, new CT_StdItemAttributeT<float>(CT_AbstractCategory::DATA_HEIGHT, val));
                                item_ref->addItemAttribute(_outRefID, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, id));

                            } else {
                                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier REF non valide")).arg(cpt));
                            }
                        }
                    }
                }
                fRef.close();
            }
        }
    }

    for (CT_ResultGroup* resultTrans : _outResultTrans.iterateOutputs())
    {
        if(QFile::exists(_transFile.first()))
        {
            QFile fTrans(_transFile.first());

            if (fTrans.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QTextStream stream(&fTrans);

                stream.readLine();

                size_t cpt = 1;
                while (!stream.atEnd())
                {
                    QString line = stream.readLine();
                    if (!line.isEmpty())
                    {
                        QStringList values = line.split(",");
                        if (values.size() >= 8)
                        {
                            bool okX, okY, okVal;
                            double x = values.at(3).toDouble(&okX);
                            double y = values.at(4).toDouble(&okY);
                            float val = values.at(7).toFloat(&okVal);
                            QString id = values.at(0);

                            if (okX && okY && okVal)
                            {
                                CT_StandardItemGroup* grp_grpTrans= new CT_StandardItemGroup();
                                resultTrans->addRootGroup(_outGrpTrans, grp_grpTrans);

                                CT_Point2D* item_trans = new CT_Point2D(new CT_Point2DData(x,y));
                                grp_grpTrans->addSingularItem(_outTrans, item_trans);

                                item_trans->addItemAttribute(_outTransVal, new CT_StdItemAttributeT<float>(CT_AbstractCategory::DATA_HEIGHT, val));
                                item_trans->addItemAttribute(_outTransID, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, id));
                            } else {
                                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier TRANS non valide")).arg(cpt));
                            }
                        }
                    }
                }
                fTrans.close();
            }
        }
    }

}
