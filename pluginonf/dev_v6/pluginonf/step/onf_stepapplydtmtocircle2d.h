/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPAPPLYDTMTOCIRCLE2D_H
#define ONF_STEPAPPLYDTMTOCIRCLE2D_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_circle2d.h"

class ONF_StepApplyDTMToCircle2D : public CT_AbstractStep
{

    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepApplyDTMToCircle2D();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double      _offset;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_Circle2D>                                                _inCircle2D;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inCircle2DAttID;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inCircle2DAttVal;

    CT_HandleOutSingularItem<CT_Circle>                                                 _outcircle3D;
    CT_HandleOutStdItemAttribute<QString>                                               _outcircle3DID;
    CT_HandleOutStdItemAttribute<double>                                                _outcircle3DVal;

    CT_HandleInResultGroup<>                                                            _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                                               _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float>>                                          _inDTM;



};

#endif // ONF_STEPAPPLYDTMTOCIRCLE2D_H
