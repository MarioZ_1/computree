/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputestorktrajectory.h"

#include "tools/onf_computehitsthread.h"

#include <QFileInfo>

ONF_StepComputeStorkTrajectory::ONF_StepComputeStorkTrajectory() : SuperClass()
{
    _range = 1;
}

QString ONF_StepComputeStorkTrajectory::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Calcul des trajectoires de Cigognes");
}

QString ONF_StepComputeStorkTrajectory::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepComputeStorkTrajectory::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepComputeStorkTrajectory();
}

void ONF_StepComputeStorkTrajectory::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Grille"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inGrid, tr("Distances"));
}

void ONF_StepComputeStorkTrajectory::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outTrajectory, tr("Trajectoires"));
}

void ONF_StepComputeStorkTrajectory::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addInt(tr("Portée de dilatation"),tr("cellules"), 1, std::numeric_limits<int>::max(), _range);
}

void ONF_StepComputeStorkTrajectory::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Grid3D<float>* gridIn : group->singularItems(_inGrid))
        {
            if (isStopped()) {return;}

            for (int colx = 0 ; colx < gridIn->xdim() ; colx++)
            {
                for (int liny = 0 ; liny < gridIn->ydim() ; liny++)
                {
                    int levz = gridIn->zdim() - 1;

                    float minDist = gridIn->value(colx, liny, levz);
                    int minxxCoord = colx;
                    int minyyCoord = liny;
                    int minzzCoord = levz;

                    while (minDist > float(gridIn->resolution()))
                    {
                        int minXX = minxxCoord - 1;
                        if (minxxCoord < 0) {minXX = 0;}
                        int minYY = minyyCoord - 1;
                        if (minyyCoord < 0) {minYY = 0;}
                        int minZZ = minzzCoord - 1;
                        if (minzzCoord < 0) {minZZ = 0;}

                        int maxXX = minxxCoord + 1;
                        if (maxXX > gridIn->xdim()) {maxXX = gridIn->xdim();}
                        int maxYY = minyyCoord + 1;
                        if (maxYY > gridIn->ydim()) {maxYY = gridIn->ydim();}
                        int maxZZ = minzzCoord + 1;
                        if (maxZZ > gridIn->zdim()) {maxZZ = gridIn->zdim();}

                        minDist = std::numeric_limits<float>::max();
                        int tmpXX = 0;
                        int tmpYY = 0;
                        int tmpZZ = 0;

                        for (int xx = minXX ; xx <= maxXX ; xx++)
                        {
                            for (int yy = minYY ; yy <= maxYY ; yy++)
                            {
                                for (int zz = minZZ ; zz <= maxZZ ; zz++)
                                {
                                    if (xx != minxxCoord || yy != minyyCoord || zz != minzzCoord)
                                    {
                                        float dist = gridIn->value(xx, yy, zz);
                                        if (!qFuzzyCompare(dist, gridIn->NA()) && dist < minDist)
                                        {
                                            minDist = dist;
                                            tmpXX = xx;
                                            tmpYY = yy;
                                            tmpZZ = zz;
                                        }
                                    }
                                }
                            }
                        }
                        Eigen::Vector3d pt1(gridIn->getCellCenterX(minxxCoord), gridIn->getCellCenterY(minyyCoord), gridIn->getCellCenterZ(minzzCoord));
                        Eigen::Vector3d pt2(gridIn->getCellCenterX(tmpXX), gridIn->getCellCenterY(tmpYY), gridIn->getCellCenterZ(tmpZZ));

                        CT_LineData* linedata = new CT_LineData(pt1, pt2);

                        CT_Line* line = new CT_Line(linedata);

                        CT_StandardItemGroup* grp = new CT_StandardItemGroup();
                        group->addGroup(_outGroup, grp);
                        grp->addSingularItem(_outTrajectory, line);

                        minxxCoord = tmpXX;
                        minyyCoord = tmpYY;
                        minzzCoord = tmpZZ;
                    }
                }
            }

        }
    }

    setProgress(99);
}

