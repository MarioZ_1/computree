/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfitcylinderoncluster.h"

ONF_StepFitCylinderOnCluster::ONF_StepFitCylinderOnCluster() : SuperClass()
{
}

QString ONF_StepFitCylinderOnCluster::description() const
{
    return tr("Ajuster un Cylindre par cluster (en 3D)");
}

QString ONF_StepFitCylinderOnCluster::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepFitCylinderOnCluster::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepFitCylinderOnCluster::createNewInstance() const
{
    return new ONF_StepFitCylinderOnCluster();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFitCylinderOnCluster::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Points"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Points"));
}

void ONF_StepFitCylinderOnCluster::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outCylinder, tr("Scene"));
}

void ONF_StepFitCylinderOnCluster::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* item = group->singularItem(_inScene);

        if (item != nullptr)
        {
            CT_Cylinder* cyl = CT_Cylinder::staticCreate3DCylinderFromPointCloud(*(item->pointCloudIndex()), item->centerCoordinate());

            if (cyl != nullptr)
            {
                group->addSingularItem(_outCylinder, cyl);
            }
        }

    }

}
