/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPMERGEENDTOENDSECTIONS04_H
#define ONF_STEPMERGEENDTOENDSECTIONS04_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/ct_pointcluster.h"

#include "tools/clustersection.h"

#include "ct_shapedata/ct_linedata.h"

class ONF_StepMergeEndToEndSections04 : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepMergeEndToEndSections04();

    QString description() const;

    QString detailledDescription() const;

    QStringList getStepRISCitations() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double _thickness;      /*!< Epaisseur des clusters */
    double _n;             /*!< Nombre de barycentres a considerer aux extremitÃ©s */
    double _mult;          /*!< Facteur multiplicatif de maxDist */
    double _zTolerance;    /*!< Chevauchement en Z tolÃ©rÃ© */
    double _searchDistance; /*!< Distance 3D maximale entre extremitÃ©s de sections Ã  fusionner */

    CT_HandleInResultGroup<>                                                            _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroupSection;
    CT_HandleInStdGroup<>                                                               _inGroupCluster;
    CT_HandleInSingularItem<CT_PointCluster>                                            _inCluster;

    CT_HandleOutResultGroup                                                             _outResult;
    CT_HandleOutStdGroup                                                                _outGroupSection;
    CT_HandleOutStdGroup                                                                _outGroupCluster;
    CT_HandleOutSingularItem<CT_PointCluster>                                           _outCluster;
    CT_HandleOutSingularItem<CT_ReferencePoint>                                         _outRefPoint;

    double distanceFromExtremityToLine(CT_LineData *lineL, double plan_x, double plan_y, double plan_z);

};

#endif // ONF_STEPMERGEENDTOENDSECTIONS04_H
