/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCLASSIFYGROUND_H
#define ONF_STEPCLASSIFYGROUND_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepClassifyGround : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepClassifyGround();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double  _min_density;                       /*!< Densite minimum pour considerer que c'est du sol*/
    double  _gridsize;                          /*!< Taille de  la grille MNT en m*/
    bool    _filterByDensity;
    double  _groundwidth;                         /*!< Epaisseur du sol en m*/
    bool    _filterByNeighourhoud;
    int     _dist;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outSceneVeg;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outSceneGround;

    CT_HandleOutResultGroup                             m_hOutResultDTM;
    CT_HandleOutStdGroup                                m_hOutRootGroupDTM;
    CT_HandleOutSingularItem<CT_Image2D<float> >        m_hOutDTM;
    CT_HandleOutSingularItem<CT_Image2D<int> >          m_hOutDensity;


};

#endif // ONF_STEPCLASSIFYGROUND_H
