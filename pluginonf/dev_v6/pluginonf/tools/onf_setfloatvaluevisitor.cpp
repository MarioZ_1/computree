#include "onf_setfloatvaluevisitor.h"

ONF_SetFloatValueVisitor::ONF_SetFloatValueVisitor(CT_Grid3D<float> *grid, float value)
{
  _grid = grid;
  _value = value;
}

void ONF_SetFloatValueVisitor::visit(const size_t &index, const CT_Beam *beam)
{
    Q_UNUSED(beam);
    _grid->setValueAtIndex(index, _value);
}
