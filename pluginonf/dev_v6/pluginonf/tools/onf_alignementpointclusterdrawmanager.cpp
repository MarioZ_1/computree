#include "onf_alignementpointclusterdrawmanager.h"
#include "painterinterface.h"

#include "ct_itemdrawable/ct_pointcluster.h"
#include "actions/onf_actionaffiliatepointalignementsandfieldinventory.h"

#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_color.h"

#include <QObject>


const QString ONF_AlignementPointClusterDrawManager::INDEX_CONFIG_BARYCENTER_VISIBLE = ONF_AlignementPointClusterDrawManager::staticInitConfigBarycenterVisible();

ONF_AlignementPointClusterDrawManager::ONF_AlignementPointClusterDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithPointCloudDrawManager(drawConfigurationName.isEmpty() ? CT_PointCluster::staticName() : drawConfigurationName)
{
    _lineVisible = true;
    _dataContainer = nullptr;
    _newCluster = false;
    _unmatchedColor = QColor(200,200,200); // Light Grey

}

ONF_AlignementPointClusterDrawManager::~ONF_AlignementPointClusterDrawManager()
{
    _lineVisible = true;
    _dataContainer = nullptr;
    _newCluster = false;
    _unmatchedColor = QColor(200,200,200); // Light Grey

}

void ONF_AlignementPointClusterDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    Q_UNUSED(view);

    const CT_PointCluster &item = dynamic_cast<const CT_PointCluster&>(itemDrawable);

    if(drawConfiguration()->variableValue(INDEX_CONFIG_BOUNDING_SHAPE_VISIBLE).toBool() && item.doesBoundingBoxAppearToBeValid())
    {
        painter.drawPoint(item.minX(), item.minY(), item.minZ());
        painter.drawPoint(item.maxX(), item.minY(), item.minZ());
        painter.drawPoint(item.minX(), item.maxY(), item.minZ());
        painter.drawPoint(item.minX(), item.minY(), item.maxZ());
        painter.drawPoint(item.maxX(), item.maxY(), item.minZ());
        painter.drawPoint(item.minX(), item.maxY(), item.maxZ());
        painter.drawPoint(item.maxX(), item.minY(), item.maxZ());
        painter.drawPoint(item.maxX(), item.maxY(), item.maxZ());

        // B
        painter.drawLine(item.minX(), item.minY(), item.minZ(), item.maxX(), item.minY(), item.minZ());
        painter.drawLine(item.maxX(), item.minY(), item.minZ(), item.maxX(), item.maxY(), item.minZ());
        painter.drawLine(item.maxX(), item.maxY(), item.minZ(), item.minX(), item.maxY(), item.minZ());
        painter.drawLine(item.minX(), item.maxY(), item.minZ(), item.minX(), item.minY(), item.minZ());

        // H
        painter.drawLine(item.minX(), item.minY(), item.maxZ(), item.maxX(), item.minY(), item.maxZ());
        painter.drawLine(item.maxX(), item.minY(), item.maxZ(), item.maxX(), item.maxY(), item.maxZ());
        painter.drawLine(item.maxX(), item.maxY(), item.maxZ(), item.minX(), item.maxY(), item.maxZ());
        painter.drawLine(item.minX(), item.maxY(), item.maxZ(), item.minX(), item.minY(), item.maxZ());

        // G
        painter.drawLine(item.minX(), item.minY(), item.minZ(), item.minX(), item.minY(), item.maxZ());
        painter.drawLine(item.minX(), item.minY(), item.maxZ(), item.minX(), item.maxY(), item.maxZ());
        painter.drawLine(item.minX(), item.maxY(), item.maxZ(), item.minX(), item.maxY(), item.minZ());
        painter.drawLine(item.minX(), item.maxY(), item.minZ(), item.minX(), item.minY(), item.minZ());

        // D
        painter.drawLine(item.maxX(), item.minY(), item.minZ(), item.maxX(), item.minY(), item.maxZ());
        painter.drawLine(item.maxX(), item.minY(), item.maxZ(), item.maxX(), item.maxY(), item.maxZ());
        painter.drawLine(item.maxX(), item.maxY(), item.maxZ(), item.maxX(), item.maxY(), item.minZ());
        painter.drawLine(item.maxX(), item.maxY(), item.minZ(), item.maxX(), item.minY(), item.minZ());
    }

    if(drawConfiguration()->variableValue(INDEX_CONFIG_BARYCENTER_VISIBLE).toBool())
    {
        const CT_PointClusterBarycenter &barycenter = item.getBarycenter();
        painter.drawPoint(barycenter.x(), barycenter.y(), barycenter.z());
    }


    if(drawConfiguration()->variableValue(INDEX_CONFIG_POINTS_VISIBLE).toBool())
    {
        CT_PointAccessor pointAccessor;
        const CT_AbstractPointCloudIndex *pointCloudIndex = item.pointCloudIndex();

        if (_dataContainer != nullptr)
        {
            if (_newCluster)
            {
                CT_PointCluster* itemCast = (CT_PointCluster*) &item;

                int indexCluster = _dataContainer->_alignements.indexOf(itemCast);
                if (indexCluster >= 0)
                {
                    for (int i = 0 ; i < pointCloudIndex->size(); i++)
                    {
                        const size_t index = pointCloudIndex->constIndexAt(i);
                        painter.drawPoint(index);
                    }
                }
            } else
            {
                for (int i = 0 ; i < pointCloudIndex->size(); i++)
                {
                    const size_t index = pointCloudIndex->constIndexAt(i);

                    if (!_dataContainer->_addedAlignements.contains(index) && !_dataContainer->_droppedPointsIds.contains(index))
                    {
                        CT_Point point = pointAccessor.constPointAt(index);
                        painter.drawPoint(index);

                        if (_lineVisible && (i < (pointCloudIndex->size() - 1)))
                        {
                            const size_t nextIndex = pointCloudIndex->constIndexAt(i+1);

                            if (!_dataContainer->_droppedPointsIds.contains(nextIndex) && !_dataContainer->_addedAlignements.contains(nextIndex))
                            {
                                CT_Point nextPoint = pointAccessor.constPointAt(nextIndex);

                                painter.drawLine(point(0), point(1), point(2), nextPoint(0), nextPoint(1), nextPoint(2));
                            }
                        }
                    }
                }
            }
        }
    }
}

void ONF_AlignementPointClusterDrawManager::setLineVisible(bool visible)
{
    _lineVisible = visible;
}

bool ONF_AlignementPointClusterDrawManager::lineVisible()
{
    return _lineVisible;
}

void ONF_AlignementPointClusterDrawManager::setDataContainer(ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer *dataContainer, bool newCluster)
{
    _dataContainer = dataContainer;
    _newCluster = newCluster;
}

CT_ItemDrawableConfiguration ONF_AlignementPointClusterDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(ONF_AlignementPointClusterDrawManager::staticInitConfigBarycenterVisible(), QObject::tr("Barycentre"), CT_ItemDrawableConfiguration::Bool, false );

    return item;
}

// PROTECTED //

QString ONF_AlignementPointClusterDrawManager::staticInitConfigBarycenterVisible()
{
    return "PTCL_BA";
}

