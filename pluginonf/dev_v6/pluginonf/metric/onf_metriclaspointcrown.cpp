/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/onf_metriclaspointcrown.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

#include "ctliblas/tools/las/ct_lasdata.h"

ONF_MetricLASPointCrown::ONF_MetricLASPointCrown() : CT_AbstractMetric_LAS()
{
    declareAttributes();
}

ONF_MetricLASPointCrown::ONF_MetricLASPointCrown(const ONF_MetricLASPointCrown &other) : CT_AbstractMetric_LAS(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
}

QString ONF_MetricLASPointCrown::getShortDescription() const
{
    return tr("Métriques de houppier (LAS H ou LAS Z)");
}

QString ONF_MetricLASPointCrown::getDetailledDescription() const
{
    return tr("Les valeurs suivantes sont calculées :<br>"
              "- X_Apex<br>"
              "- Y_Apex<br>"
              "- Z_Apex<br>"
              "- Pen_1m (Penetration rate in upper 1st meter)<br>"
              "- Pen_2m (Penetration rate in upper 2nd meter)<br>"
              "- Pen_3m (Penetration rate in upper 3rd meter)<br>"
              "- Pen_4m (Penetration rate in upper 4th meter)<br>"
              "- Pen_5m (Penetration rate in upper 5th meter)<br>"
              );
}

ONF_MetricLASPointCrown::Config ONF_MetricLASPointCrown::metricConfiguration() const
{
    return m_configAndResults;
}

void ONF_MetricLASPointCrown::setMetricConfiguration(const ONF_MetricLASPointCrown::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *ONF_MetricLASPointCrown::copy() const
{
    return new ONF_MetricLASPointCrown(*this);
}

void ONF_MetricLASPointCrown::computeMetric()
{
    m_configAndResults._X_Apex.value = std::numeric_limits<double>::quiet_NaN();
    m_configAndResults._Y_Apex.value = std::numeric_limits<double>::quiet_NaN();
    m_configAndResults._Z_Apex.value = -std::numeric_limits<double>::max();
    m_configAndResults._Pen_1m.value = 0;
    m_configAndResults._Pen_2m.value = 0;
    m_configAndResults._Pen_3m.value = 0;
    m_configAndResults._Pen_4m.value = 0;
    m_configAndResults._Pen_5m.value = 0;

    CT_PointIterator itP(pointCloud());
    while(itP.hasNext())
    {
        const CT_Point& point = itP.next().currentPoint();

        if ((plotArea() == NULL) || plotArea()->contains(point(0), point(1)))
        {
            if (point(2) > m_configAndResults._Z_Apex.value)
            {
                m_configAndResults._X_Apex.value = point(0);
                m_configAndResults._Y_Apex.value = point(1);
                m_configAndResults._Z_Apex.value = point(2);
            }
        }                
    }

    double n_FirstInf0 = 0;
    double n_FirstInf1 = 0;
    double n_FirstInf2 = 0;
    double n_FirstInf3 = 0;
    double n_FirstInf4 = 0;
    double n_FirstInf5 = 0;

    CT_PointIterator itP2(pointCloud());
    while(itP2.hasNext())
    {
        const CT_Point& point = itP2.next().currentPoint();

        if ((plotArea() == NULL) || plotArea()->contains(point(0), point(1)))
        {
            CT_LASData lasData;
            size_t index = itP2.currentGlobalIndex();
            if (lasPointCloudIndex()->contains(index))
            {
                size_t lasIndex = lasPointCloudIndex()->indexOf(index);
                lasAttributes()->getLASDataAt(lasIndex, lasData);
            }

            double distZ = m_configAndResults._Z_Apex.value - point(2);

            // calculs pour les taux de pénétration
            if (lasData._Return_Number == 1)
            {
                n_FirstInf0 += 1.0;

                if (distZ >= 1.0)
                {
                    n_FirstInf1 += 1.0;
                }
                if (distZ >= 2.0)
                {
                    n_FirstInf2 += 1.0;
                }
                if (distZ >= 3.0)
                {
                    n_FirstInf3 += 1.0;
                }
                if (distZ >= 4.0)
                {
                    n_FirstInf4 += 1.0;
                }
                if (distZ >= 5.0)
                {
                    n_FirstInf5 += 1.0;
                }
            }

        }
    }

    // Finalisation des calculs de taux de pénétration
    if (n_FirstInf0 > 0)
    {
        m_configAndResults._Pen_1m.value = n_FirstInf1 / n_FirstInf0;
        m_configAndResults._Pen_2m.value = n_FirstInf2 / n_FirstInf0;
        m_configAndResults._Pen_3m.value = n_FirstInf3 / n_FirstInf0;
        m_configAndResults._Pen_4m.value = n_FirstInf4 / n_FirstInf0;
        m_configAndResults._Pen_5m.value = n_FirstInf5 / n_FirstInf0;
    }

    setAttributeValueVaB(m_configAndResults._X_Apex);
    setAttributeValueVaB(m_configAndResults._Y_Apex);
    setAttributeValueVaB(m_configAndResults._Z_Apex);
    setAttributeValueVaB(m_configAndResults._Pen_1m);
    setAttributeValueVaB(m_configAndResults._Pen_2m);
    setAttributeValueVaB(m_configAndResults._Pen_3m);
    setAttributeValueVaB(m_configAndResults._Pen_4m);
    setAttributeValueVaB(m_configAndResults._Pen_5m);
}

void ONF_MetricLASPointCrown::declareAttributes()
{    
    registerAttributeVaB(m_configAndResults._X_Apex, CT_AbstractCategory::DATA_NUMBER, tr("X_Apex"));
    registerAttributeVaB(m_configAndResults._Y_Apex, CT_AbstractCategory::DATA_NUMBER, tr("Y_Apex"));
    registerAttributeVaB(m_configAndResults._Z_Apex, CT_AbstractCategory::DATA_NUMBER, tr("Z_Apex"));
    registerAttributeVaB(m_configAndResults._Pen_1m, CT_AbstractCategory::DATA_NUMBER, tr("Pen_1m"));
    registerAttributeVaB(m_configAndResults._Pen_2m, CT_AbstractCategory::DATA_NUMBER, tr("Pen_2m"));
    registerAttributeVaB(m_configAndResults._Pen_3m, CT_AbstractCategory::DATA_NUMBER, tr("Pen_3m"));
    registerAttributeVaB(m_configAndResults._Pen_4m, CT_AbstractCategory::DATA_NUMBER, tr("Pen_4m"));
    registerAttributeVaB(m_configAndResults._Pen_5m, CT_AbstractCategory::DATA_NUMBER, tr("Pen_5m"));

}

