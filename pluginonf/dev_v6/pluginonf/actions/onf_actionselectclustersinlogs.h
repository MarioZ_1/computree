#ifndef ONF_ACTIONSELECTCLUSTERSINLOGS_H
#define ONF_ACTIONSELECTCLUSTERSINLOGS_H

#include <QRect>

#include "ctlibaction/ct_actions/abstract/ct_abstractactionforgraphicsview.h"

#include "actions/tools/polygonforpicking.h"
#include "actions/tools/rectangleforpicking.h"

/**
 * @brief Action to select items, points, faces, edges, etc...
 *
 * You can use this action in your action if you want. Just call methods :
 *
 * init()
 * mousePressEvent()
 * etc.....
 *
 * in methods of your action.
 *
 * Dont forget to set the selection mode => setSelectionMode method
 */
class ONF_ActionSelectClustersInLogs : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    enum SelectionTool {
        Point = 0,
        Rectangle,
        Polygon
    };



    ONF_ActionSelectClustersInLogs(QHash<CT_AbstractItemDrawable*, quint64> *clusterToLog, QMultiHash<quint64, CT_AbstractItemDrawable*> *logToCluster, QList<CT_AbstractItemDrawable *> *validatedItems);

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);
    bool wheelEvent(QWheelEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    /**
     * @brief change the selection mode for this action. The mode will be set to view
     *        when the mousePressEvent() method is called
     * @return false if we are between mousePressEvent() and mouseReleaseEvent()
     */
    bool setSelectionMode(GraphicsViewInterface::SelectionMode mode);

    /**
     * @brief Set the selection tool to used
     */
    bool setSelectionTool(ONF_ActionSelectClustersInLogs::SelectionTool tool);

    /**
     * @brief Toggle the current selection
     */
    void toggleSelection();

    /**
     * @brief Returns the current selection mode used
     */
    GraphicsViewInterface::SelectionMode selectionMode() const;

    /**
     * @brief Return the current selection mode but only between [NONE;REMOVE_ONE]
     */
    GraphicsViewInterface::SelectionMode selectionModeToBasic(GraphicsViewInterface::SelectionMode mode) const;

    /**
     * @brief Returns the current selection tool used
     */
    ONF_ActionSelectClustersInLogs::SelectionTool selectionTool() const;

private:
    AMKgl::RectangleForPicking                      m_rectangleTools;
    AMKgl::PolygonForPicking                        m_polygonTools;
    int                                             m_status;
    GraphicsViewInterface::SelectionMode            m_selectionMode;
    ONF_ActionSelectClustersInLogs::SelectionTool   m_selectionTool;
    QPoint                                          m_lastMousePos;
    Qt::KeyboardModifiers                           m_keyModifiers;

    QHash<CT_AbstractItemDrawable*, quint64>            *m_clusterToLog;
    QMultiHash<quint64, CT_AbstractItemDrawable*>       *m_logToCluster;

    QList<CT_AbstractItemDrawable*>                 *m_validatedItems;

    QColor                                          m_validationColor;
    QColor                                          m_whiteColor;

    void setKeyModifiers(Qt::KeyboardModifiers m);
    void pick();

private slots:
    void redrawOverlay();

public slots:
    void validate();
    void unvalidate();

signals:
    /**
     * @brief Emitted when the selection mode changed
     */
    void selectionModeChanged(GraphicsViewInterface::SelectionMode mode);
};

#endif // ONF_ACTIONSELECTCLUSTERSINLOGS_H
