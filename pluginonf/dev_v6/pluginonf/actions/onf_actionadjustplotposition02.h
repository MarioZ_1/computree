/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONADJUSTPLOTPOSITION02_H
#define ONF_ACTIONADJUSTPLOTPOSITION02_H

#include "views/actions/onf_actionadjustplotpositionoptions02.h"
#include "ctlibaction/ct_actions/abstract/ct_abstractactionforgraphicsview.h"

#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_result/abstract/ct_abstractresult.h"

#include "tools/onf_adjustplotpositioncylinderdrawmanager.h"
#include "tools/onf_adjustplotpositionimage2ddrawmanager.h"
#include "tools/onf_colorlinearinterpolator.h"

#include "ct_itemdrawable/ct_image2d.h"

#include <QRect>

class ONF_CoRegistration_MonnetMermin2014_TreePosition
{
public:
    ONF_CoRegistration_MonnetMermin2014_TreePosition()
    {
        _x = 0;
        _y = 0;
        _originalX = 0;
        _originalY = 0;
        _zPoint = 0;

        _dbh = 0;
        _height = 0;
        _trueheight = 0;
        _idPlot = "";
        _idTree = "";
        _cyl = nullptr;
        _grp = nullptr;
        _species = "";
        _moved = false;
    }

    ~ONF_CoRegistration_MonnetMermin2014_TreePosition()
    {
        _cyl = nullptr;
        _grp = nullptr;
    }

    double                  _x;
    double                  _y;
    double                  _originalX;
    double                  _originalY;
    double                  _zPoint;
    bool                    _moved;

    float                   _dbh;
    float                   _height;
    float                   _trueheight;
    QString                 _idPlot;
    QString                 _idTree;
    QString                 _species;
    QString                 _comment;
    CT_Cylinder*            _cyl;
    CT_StandardItemGroup*   _grp;
};

class ONF_ActionAdjustPlotPosition02_dataContainer
{
public:
    ONF_ActionAdjustPlotPosition02_dataContainer();

    ~ONF_ActionAdjustPlotPosition02_dataContainer()
    {
        _positions.clear();
        _scenes.clear();
        _LASattributes.clear();
        _heightAttributes.clear();
        _maximaPoints.clear();
        _dtm = nullptr;
        _dsm = nullptr;
        _xPlot = 0.0;
        _yPlot = 0.0;
        _radiusPlot = -1.0;
        _transX = 0.0;
        _transY = 0.0;
        _quality = 0.0;
    }

    QList<ONF_CoRegistration_MonnetMermin2014_TreePosition*>   _positions;
    QList<CT_AbstractItemDrawable*>                     _scenes;
    QList<CT_StdLASPointsAttributesContainer*>          _LASattributes;
    QList<CT_PointsAttributesScalarTemplated<float>*>   _heightAttributes;
    QMap<qint32, Eigen::Vector3d>                       _maximaPoints;
    CT_Image2D<float>*                                  _dsm;
    CT_Image2D<float>*                                  _dtm;

    QString                                             _idPlot;
    double                                              _xPlot;
    double                                              _yPlot;
    double                                              _radiusPlot;

    double                                              _transX;
    double                                              _transY;

    int                                                 _quality;
    QString                                             _comment;

};

class ONF_ActionAdjustPlotPosition02 : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    ONF_ActionAdjustPlotPosition02(ONF_ActionAdjustPlotPosition02_dataContainer* dataContainer);

    ~ONF_ActionAdjustPlotPosition02();

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);
    bool wheelEvent(QWheelEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void draw(GraphicsViewInterface &view, PainterInterface &painter);
    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    CT_AbstractAction* createInstance() const;

public slots:
    void update(double x, double y, bool circles, bool fixedH, double h, bool treeMode, bool mode3d, double radiusFactor, bool apex, double apexSize, bool plot);
    void updateColorization(bool colorizeByIntensity, int min, int max);
    void applyTranslation(bool reset);
    void setGradient(bool intensity, QString name, int min, int max);
    void changeHighlightedNumber(int n);
    void changeSelectionMode(bool modechange);
    void changeQuality(int quality);
    void changeComment(QString comment);


private:

    ONF_ActionAdjustPlotPosition02_dataContainer* _dataContainer;
    QList<CT_AbstractItemDrawable*>               _cylinders;

    ONF_AdjustPlotPositionCylinderDrawManager*          _drawManagerCylinder;
    ONF_AdjustPlotPositionImage2DDrawManager<float>*    _drawManagerDSM;

    ONF_ColorLinearInterpolator                _gradientGrey;
    ONF_ColorLinearInterpolator                _gradientHot;
    ONF_ColorLinearInterpolator                _gradientRainbow;
    ONF_ColorLinearInterpolator                _gradientHSV;

    ONF_ColorLinearInterpolator                _currentZGradient;
    ONF_ColorLinearInterpolator                _currentIGradient;

    QColor                                     _cylinderColor;
    QColor                                     _selectedCylinderColor;
    QColor                                     _selectedApexColor;
    QColor                                     _highlightedCylindersColor;
    QColor                                     _movedCylindersColor;

    QList<QSharedPointer<ObjectsModifierInterface> > _objectsModifierList;

    double _minZ;
    double _maxZ;
    double _rangeZ;
    double _minH;
    double _maxH;
    double _rangeH;
    double _minI;
    double _maxI;
    double _rangeI;
    bool   _colorizeByIntensity;
    bool   _mode3d;
    bool   _apex;
    double _apexSize;
    bool   _drawPlot;


    Qt::MouseButtons                                _buttonsPressed;
    QPoint                                          _lastPos;
    ONF_CoRegistration_MonnetMermin2014_TreePosition*      _selectedPos;
    ONF_CoRegistration_MonnetMermin2014_TreePosition*      _previousSelectedPos;
    Eigen::Vector3d                                 _currentPoint;
    qint32                                          _currentApex;
    bool                                            _movePlot;


    void colorizePoints(ONF_ColorLinearInterpolator &gradient, int min, int max);    
    ONF_CoRegistration_MonnetMermin2014_TreePosition* getPositionFromPoint(Eigen::Vector3d &point);
    ONF_CoRegistration_MonnetMermin2014_TreePosition *getPositionFromDirection(Eigen::Vector3d &origin, Eigen::Vector3d &direction);
    void getApexPositionFromDirection(Eigen::Vector3d &origin, Eigen::Vector3d &direction, Eigen::Vector3d &apex, qint32 &apexID);
    bool getCoordsForMousePosition(QPoint p, double &x, double &y);
    void updateCylinderPosition(ONF_CoRegistration_MonnetMermin2014_TreePosition *pos);
    void updateAllCylindersPosition();


    static bool lessThan(CT_AbstractItemDrawable* i1, CT_AbstractItemDrawable* i2)
    {
        CT_Cylinder* cyl1 = dynamic_cast<CT_Cylinder*>(i1);
        CT_Cylinder* cyl2 = dynamic_cast<CT_Cylinder*>(i2);

        if (cyl1 == nullptr || cyl2 == nullptr ) {return true;}

        return cyl1->getRadius() > cyl2->getRadius();
    }

private slots:
    void redrawOverlay();
    void redrawOverlayAnd3D();


};


#endif // ONF_ACTIONADJUSTPLOTPOSITION02_H
