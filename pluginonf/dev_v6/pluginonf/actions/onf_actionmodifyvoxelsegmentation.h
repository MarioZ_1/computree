/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONMODIFYVOXELSEGMENTATION_H
#define ONF_ACTIONMODIFYVOXELSEGMENTATION_H

#include <QRect>

#include "ctlibaction/ct_actions/abstract/ct_abstractactionforgraphicsview.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_itemdrawable/ct_grid3d_points.h"

/**
 * @brief Action to select items, points, faces, edges, etc...
 *
 * You can use this action in your action if you want. Just call methods :
 *
 * init()
 * mousePressEvent()
 * etc.....
 *
 * in methods of your action.
 *
 * Dont forget to set the selection mode => setSelectionMode method
 */
class ONF_ActionModifyVoxelSegmentation : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    ONF_ActionModifyVoxelSegmentation(CT_AbstractItemDrawableWithPointCloud* scene, CT_Grid3D_Points* pointGrid, CT_Grid3D_Points* topologyGrid, CT_Grid3D_Sparse<int>* outSegmentationGrid, QVector<bool>* validated, QMap<int, QString> *positionLabels);
    ~ONF_ActionModifyVoxelSegmentation();

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();
    void postProcess();

    void updateAllClustersColors();
    void updateColorForOneCluster(int clusterNum);

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    /**
     * @brief change the selection mode for this action. The mode will be set to view
     *        when the mousePressEvent() method is called
     * @return false if we are between mousePressEvent() and mouseReleaseEvent()
     */
    bool setSelectionMode(GraphicsViewInterface::SelectionMode mode);

    GraphicsViewInterface::SelectionMode selectionMode() const;

public slots:
    void setColorA(QColor color);
    void setColorB(QColor color);
    void swapAandB();
    void visibilityChanged();
    void selectPositionA();
    void selectPositionB();
    void affectClusterToA();
    void affectClusterToB();
    void affectClusterToTrash();
    void affectClusterToNewSeed();
    void extend();
    void selectSeed();
    void validatePosition();
    void redraw(bool pointsUpdated);
    void changeLabelA(QString label);

private:
    QRect                                   m_selectionRectangle;
    int                                     m_status;
    GraphicsViewInterface::SelectionMode    m_selectionMode;
    bool                                    m_mousePressed;

    CT_AbstractItemDrawableWithPointCloud*  _scene;
    CT_Grid3D_Points*                       _pointGrid;
    CT_Grid3D_Points*                       _topologyGrid;
    CT_Grid3D_Sparse<int>*                  _outSegmentationGrid;
    const CT_AbstractPointCloudIndex *      _pointCloudIndex;
    QList<size_t>                           _cellsWithPoints;



    QSharedPointer<ObjectsModifierInterface> _objectsModifier;

    QList<QColor>                   _automaticColorList;
    QVector<QColor>                 _positionsBaseColors;
    QVector<bool>                   *_validated;

    QColor                          _colorA;
    QColor                          _colorB;
    QColor                          _colorAvalidated;
    QColor                          _colorTrash;
    QColor                          _validatedColor;

    int                             _positionA;
    int                             _positionB;
    int                             _lastSeedID;
    int                             _colorNum;

    QMap<int, QString>*              _positionLabels;


    GraphicsViewInterface::SelectionMode selectionModeToBasic(GraphicsViewInterface::SelectionMode mode) const;
    void getSelectedCells(QList<size_t> &list);
    void filterPointSelection();
    bool isVisible(int clusterIndex);

};

#endif // ONF_ACTIONMODIFYVOXELSEGMENTATION_H
