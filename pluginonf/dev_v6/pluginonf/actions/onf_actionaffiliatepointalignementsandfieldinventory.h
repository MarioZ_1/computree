/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORY_H
#define ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORY_H


#include "views/actions/onf_actionaffiliatepointalignementsandfieldinventoryoptions.h"
#include "ctlibstdactions/views/actions/ct_actionselectitemdrawablegvoptions.h"
#include "ctlibstdactions/action/tools/ct_actionselecttool.h"

#include "ctlibaction/ct_actions/abstract/ct_abstractactionforgraphicsview.h"

#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_result/abstract/ct_abstractresult.h"
#include "tools/onf_colorlinearinterpolator.h"
#include "tools/onf_alignementpointclusterdrawmanager.h"
#include "ct_itemdrawable/ct_pointcluster.h"
#include "tools/onf_alignementpointclusterdrawmanager.h"

#include <QRect>
#include <QDebug>

class ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition
{
public:
    ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition()
    {
        _base = Eigen::Vector3d(0,0,0);

        _dbh = 0;
        _height = 0;
        _idPlot = "";
        _idTree = "";
        _species = "";
        _grp = nullptr;
        _color = QColor(255, 255, 255);
        _validated = false;
        _rmq = "";
    }

    ~ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition()
    {
        _grp = nullptr;
        _alignementsIds.clear();
    }

    void addAlignement(int sceneId)
    {
        _alignementsIds.append(sceneId);

    }

    Eigen::Vector3d         _base;

    float                   _dbh;
    float                   _height;
    QString                 _idPlot;
    QString                 _idTree;
    QString                 _species;
    CT_StandardItemGroup*   _grp;
    bool                    _validated;
    QString                 _rmq;

    QList<int>              _alignementsIds;

    QColor                  _color;
};

class ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer
{
public:
    ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer();


    ~ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer()
    {
        _positions.clear();
        _alignements.clear();
        _matched.clear();
        _droppedPointsIds.clear();
        _addedAlignements.clear();
        _scenes.clear();
        _scenes2.clear();
        _result = nullptr;
        _sceneVisible = false;
        _sceneVisible2 = false;
    }

    QList<ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition*>   _positions;

    QList<CT_AbstractItemDrawable*>                             _alignements;
    QVector<bool>                                               _matched;
    QVector<bool>                                               _newCluster;
    ONF_AlignementPointClusterDrawManager*                      _drawManager;
    ONF_AlignementPointClusterDrawManager*                      _newClusterDrawManager;
    QList<size_t>                                               _droppedPointsIds;
    QMap<size_t, CT_PointCluster*>                              _addedAlignements;
    CT_AbstractResult*                                          _result;
    QList<CT_AbstractItemDrawableWithPointCloud*>               _scenes;
    QList<CT_AbstractItemDrawableWithPointCloud*>               _scenes2;
    bool                                                        _sceneVisible;
    bool                                                        _sceneVisible2;


};

class ONF_ActionAffiliatePointAlignementsAndFieldInventory : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    ONF_ActionAffiliatePointAlignementsAndFieldInventory(ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer* dataContainer);

    ~ONF_ActionAffiliatePointAlignementsAndFieldInventory();

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);
    bool wheelEvent(QWheelEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void draw(GraphicsViewInterface &view, PainterInterface &painter);
    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    void colorizeAlignements();

public slots:
    void addCluster();
    void removeCluster();
    void initColors();
    void validatePosition();
    void modifyRmq();

private:

    ONF_ActionAffiliatePointAlignementsAndFieldInventory_dataContainer*     _dataContainer;
    CT_ActionSelectItemDrawableGVOptions*                   _selectOption;
    CT_ActionSelectTool*                                    _selectTool;
    bool                                                    _pointMode;


    QColor                                                  _unmatchedColor;
    QList<QColor>                                           _automaticColorList;
    int                                                     _colorNum;


    QColor                                                  _selectedColor;
    QColor                                                  _validatedColor;
    QColor                                                  _validatedAndSelectedColor;

    Qt::MouseButtons                                        _buttonsPressed;
    QPoint                                                  _lastPos;
    ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition*      _selectedPos;
    ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition*      _previousSelectedPos;

    ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition *getPositionFromDirection(Eigen::Vector3d &origin, Eigen::Vector3d &direction);
    bool getCoordsForMousePosition(QPoint p, double &x, double &y);
    void drawCylinder(GraphicsViewInterface &view, PainterInterface &painter, ONF_ActionAffiliatePointAlignementsAndFieldInventory_treePosition* treePos) const;
    QColor nextColor();

private slots:
    void redrawOverlay();
    void redrawOverlayAnd3D();
    void redrawOverlayAnd3D(bool forceRedraw);


};


#endif // ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORY_H
