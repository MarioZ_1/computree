/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_actionselectsceneforeachpositionoptions.h"
#include "ui_onf_actionselectsceneforeachpositionoptions.h"

#include "actions/onf_actionselectsceneforeachposition.h"

#include <QColorDialog>
#include <QMessageBox>


ONF_ActionSelectSceneForEachPositionOptions::ONF_ActionSelectSceneForEachPositionOptions(const ONF_ActionSelectSceneForEachPosition *action) :
    CT_GAbstractActionForGraphicsViewOptions(action),
    ui(new Ui::ONF_ActionSelectSceneForEachPositionOptions())
{
    ui->setupUi(this);

    ui->tb_add->setToolTip(tr("Ajouter le cluster sélectionné au groupe actif [A]"));
    ui->tb_remove->setToolTip(tr("Retirer le cluster sélectionné du groupe actif [Z]"));
    ui->tb_add->setToolTip(tr("Remplacer le groupe actif par le cluster sélectionné [E]"));

    ui->wid_normalMode->setVisible(true);
    ui->wid_segmentMode->setVisible(false);
}

ONF_ActionSelectSceneForEachPositionOptions::~ONF_ActionSelectSceneForEachPositionOptions()
{
    delete ui;
}

void ONF_ActionSelectSceneForEachPositionOptions::setSelectionWidget(QWidget *wid)
{
    ui->wid_segmentMode->layout()->addWidget(wid);
}

void ONF_ActionSelectSceneForEachPositionOptions::clearSelectionWidget(QWidget *wid)
{
    ui->wid_segmentMode->layout()->removeWidget(wid);
}


double ONF_ActionSelectSceneForEachPositionOptions::defaultHeight() const
{
    return ui->dsb_height->value();
}

bool ONF_ActionSelectSceneForEachPositionOptions::circles() const
{
    return ui->cb_circles->isChecked();
}

int ONF_ActionSelectSceneForEachPositionOptions::highlightedNumberOfTrees() const
{
    return ui->sb_highlightedNumber->value();
}

bool ONF_ActionSelectSceneForEachPositionOptions::colorUnmatched() const
{
    return ui->cb_colorUnmatched->isChecked();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_cb_circles_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();

}

void ONF_ActionSelectSceneForEachPositionOptions::on_dsb_height_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    emit parametersChanged();

}

void ONF_ActionSelectSceneForEachPositionOptions::on_sb_highlightedNumber_valueChanged(int arg1)
{
    Q_UNUSED(arg1);
    emit parametersChanged();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_add_clicked()
{
    emit addCluster();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_remove_clicked()
{
    emit removeCluster();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_switch_clicked()
{
    emit switchCluster();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_cb_colorUnmatched_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit updateColorization();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_initColors_clicked()
{
    emit initColors();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_segment_clicked()
{
    setSegmentMode(true);
}

void ONF_ActionSelectSceneForEachPositionOptions::on_tb_finishSegmentMode_clicked()
{
    setSegmentMode(false);
}

void ONF_ActionSelectSceneForEachPositionOptions::setSegmentMode(bool segmentMode)
{

    if (segmentMode)
    {
        ui->wid_segmentMode->setVisible(true);
        ui->wid_normalMode->setVisible(false);
        emit beginSegmentCluster();
    } else {
        ui->wid_segmentMode->setVisible(false);
        ui->wid_normalMode->setVisible(true);
        emit endSegmentCluster();
    }
}

bool ONF_ActionSelectSceneForEachPositionOptions::hideNotSelected() const
{
    return ui->cb_hideNotSelected->isChecked();
}

double ONF_ActionSelectSceneForEachPositionOptions::visibleDist() const
{
    return ui->dsb_visibleRadius->value();
}

bool ONF_ActionSelectSceneForEachPositionOptions::diameters() const
{
    return ui->cb_diameter->isChecked();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_cb_hideNotSelected_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit hideNotSelectedToggled();
}

void ONF_ActionSelectSceneForEachPositionOptions::on_dsb_visibleRadius_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    emit hideNotSelectedToggled();
}


void ONF_ActionSelectSceneForEachPositionOptions::on_cb_diameter_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();
}
