/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORYOPTIONS_H
#define ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORYOPTIONS_H

#include "ctlibaction/ct_actions/view/abstract/ct_gabstractactionforgraphicsviewoptions.h"


class ONF_ActionAffiliatePointAlignementsAndFieldInventory;

namespace Ui {
class ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions;
}

class ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions : public CT_GAbstractActionForGraphicsViewOptions
{
    Q_OBJECT

public:

    explicit ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions(const ONF_ActionAffiliatePointAlignementsAndFieldInventory *action);
    ~ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions();

    void setSelectionWidget(QWidget* wid);
    void clearSelectionWidget(QWidget *wid);

    double defaultHeight() const;
    double spacing() const;
    bool circles() const;
    bool lines() const;
    bool scenes() const;
    bool scenes2() const;
    bool pointMode() const;
    int tolerance() const;

    void setRmq(QString rmq, bool active);
    QString rmq();

private:
    Ui::ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions *ui;


signals:
    void parametersChanged();
    void addCluster();
    void removeCluster();
    void updateColorization();
    void initColors();
    void validate();
    void rmqModified();

private slots:
    void on_cb_circles_toggled(bool checked);
    void on_dsb_height_valueChanged(double arg1);
    void on_tb_add_clicked();
    void on_tb_remove_clicked();
    void on_tb_initColors_clicked();
    void on_dsb_spacing_valueChanged(double arg1);
    void on_pb_validate_clicked();
    void on_cb_lines_stateChanged(int arg1);
    void on_le_rmq_textChanged(const QString &arg1);
    void on_cb_pointMode_toggled(bool checked);
    void on_cb_scene_toggled(bool checked);
    void on_cb_scene2_toggled(bool checked);
};

#endif // ONF_ACTIONAFFILIATEPOINTALIGNEMENTSANDFIELDINVENTORYOPTIONS_H
