/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_actionaffiliatepointalignementsandfieldinventoryoptions.h"
#include "ui_onf_actionaffiliatepointalignementsandfieldinventoryoptions.h"

#include "actions/onf_actionaffiliatepointalignementsandfieldinventory.h"

#include <QColorDialog>
#include <QMessageBox>


ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions(const ONF_ActionAffiliatePointAlignementsAndFieldInventory *action) :
    CT_GAbstractActionForGraphicsViewOptions(action),
    ui(new Ui::ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions())
{
    ui->setupUi(this);

    ui->tb_add->setToolTip(tr("Ajouter l'alignement sélectionné au groupe actif [A]"));
    ui->tb_remove->setToolTip(tr("Retirer l'alignement sélectionné du groupe actif [Z]"));
    ui->pb_validate->setToolTip(tr("Valider/dévalider le groupe actif [E]"));

    ui->wid_normalMode->setVisible(true);
    ui->wid_segmentMode->setVisible(false);
}

ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::~ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions()
{
    delete ui;
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::setSelectionWidget(QWidget *wid)
{
    ui->wid_segmentMode->layout()->addWidget(wid);
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::clearSelectionWidget(QWidget *wid)
{
    ui->wid_segmentMode->layout()->removeWidget(wid);
}


double ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::defaultHeight() const
{
    return ui->dsb_height->value();
}

double ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::spacing() const
{
    return ui->dsb_spacing->value();
}

bool ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::circles() const
{
    return ui->cb_circles->isChecked();
}

bool ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::lines() const
{
    return ui->cb_lines->isChecked();
}

bool ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::scenes() const
{
    return ui->cb_scene->isChecked();
}

bool ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::scenes2() const
{
    return ui->cb_scene2->isChecked();
}

bool ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::pointMode() const
{
    return ui->cb_pointMode->isChecked();
}

int ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::tolerance() const
{
    return ui->sb_tolerance->value();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::setRmq(QString rmq, bool active)
{
    ui->le_rmq->setText(rmq);
    ui->le_rmq->setEnabled(active);
}

QString ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::rmq()
{
    return ui->le_rmq->text();
}



void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_cb_circles_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();

}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_dsb_height_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    emit parametersChanged();

}


void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_tb_add_clicked()
{
    emit addCluster();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_tb_remove_clicked()
{
    emit removeCluster();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_tb_initColors_clicked()
{
    emit initColors();
}



void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_dsb_spacing_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    emit parametersChanged();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_pb_validate_clicked()
{
    emit validate();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_cb_lines_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    emit parametersChanged();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_le_rmq_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    emit rmqModified();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_cb_pointMode_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_cb_scene_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();
}

void ONF_ActionAffiliatePointAlignementsAndFieldInventoryOptions::on_cb_scene2_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit parametersChanged();
}
