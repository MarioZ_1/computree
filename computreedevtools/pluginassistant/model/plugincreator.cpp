#include "model/plugincreator.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QFileInfo>

PluginCreator::PluginCreator(QString directory, QString name, QString code) :
    QObject()
{
    _pluginDirectory = directory;
    _pluginName = name;
    _pluginCode = code;

    _target = QString("plug_%1").arg(_pluginName.toLower());

    _proName = QString("plugin%1").arg(_pluginName.toLower());
    _entryName = QString("%1_PluginEntry").arg(_pluginCode);
    _managerName = QString("%1_PluginManager").arg(_pluginCode);

}

bool PluginCreator::isValidPluginParameters(const QString &directory, const QString &name, const QString &code)
{
    return isValidPluginDirectory(directory) && isValidNameAndCode(name, code);
}

bool PluginCreator::isValidPluginDirectory(const QString &directory)
{
    QFileInfo dirInfo(directory);
    QFileInfo interfaceInfo(QString("%1/../../computreev5/pluginshared/interfaces.h").arg(directory));
    return (dirInfo.isDir() && dirInfo.exists() && interfaceInfo.exists());
}

bool PluginCreator::isValidNameAndCode(const QString &name, const QString &code)
{
    return (name.length() >= 2) && (code.length() >= 2) && (code.length() <= 4);
}

QString PluginCreator::getCodeAndNameFromEntryFile(const QString &fileName, QString &code, QString &name)
{
    QString errors = "";
    QFileInfo fileInfo(fileName);
    if (!fileInfo.exists())
    {
        errors.append(tr("Le répertoire choisi n'existe pas"));
    } else if (isValidPluginDirectory(fileInfo.path()))
    {
        QStringList values = fileInfo.fileName().split("_");

        if (!values.isEmpty())
        {
            code = values.first().toUpper();

            QFile file (fileName);
            if (file.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QTextStream stream(&file);

                while (!stream.atEnd())
                {
                    QString line = stream.readLine();
                    if (line.contains("Q_EXPORT_PLUGIN2"))
                    {
                        QStringList values = line.split(QRegExp("\\W+"), QString::SkipEmptyParts);
                        if (values.size() > 1)
                        {
                            name = values.at(1);
                            if (name.left(5) == "plug_")
                            {
                                name = name.right(name.length() - 5);
                            }
                        }
                    }
                }
                file.close();
            }

            if (!isValidNameAndCode(name, code)) {errors.append(tr("Code / Nom de plugin invalide"));}

        } else {errors.append(tr("Code / Nom de plugin invalide"));}

    }  else {errors.append(tr("Le fichier choisi n'est pas un fichier de plugin valide"));}

    return errors;
}

QString PluginCreator::createPluginFiles()
{
    QString errors = "";

    if (!isValidPluginDirectory(_pluginDirectory)) {return tr("Répertoire de plugin non valide !");}
    QDir dir(_pluginDirectory);


    if (!dir.entryList(QDir::Files).isEmpty())
    {
        return tr("Le répertoire choisi n'est pas vide, veuillez en choisir un autre svp");
    }

    if (!createPluginProject_pro()) {errors.append(tr("Impossible de créer le fichier projet (.pro)\n"));}

    if (!createPluginEntry_h()) {errors.append(tr("Impossible de créer le fichier PluginEntry (.h)\n"));}
    if (!createPluginEntry_cpp()) {errors.append(tr("Impossible de créer le fichier PluginEntry (.cpp)\n"));}

    if (!createPluginManager_h()) {errors.append(tr("Impossible de créer le fichier PluginManager (.h)\n"));}
    if (!createPluginManager_cpp()) {errors.append(tr("Impossible de créer le fichier PluginManager (.cpp)\n"));}

    dir.mkdir("languages");
    dir.mkdir("scripts");

    return errors;
}

bool PluginCreator::createPluginProject_pro()
{            
    QFile proFile(QString("%1/%2.pro").arg(_pluginDirectory).arg(_proName));

    if (proFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&proFile);
        stream.setCodec("UTF-8");        
        stream << "CT_PREFIX = ../../computreev3\n";
        stream << "\n";
        stream << "exists(../../computreev5) {\n";
        stream << "CT_PREFIX = ../../computreev5\n";
        stream << "DEFINES += COMPUTREE_V5\n";
        stream << "}\n";
        stream << "\n";
        stream << "#CHECK_CAN_USE_OPENCV = 1\n";
        stream << "#MUST_USE_OPENCV = 1\n";
        stream << "#CHECK_CAN_USE_GDAL = 1\n";
        stream << "#MUST_USE_USE_GDAL = 1\n";
        stream << "#CHECK_CAN_USE_GSL = 1\n";
        stream << "#MUST_USE_USE_GSL = 1\n";
        stream << "#CHECK_CAN_USE_PCL = 1\n";
        stream << "#MUST_USE_USE_PCL = 1\n";
        stream << "\n";
        stream << "include($${CT_PREFIX}/shared.pri)\n";
        stream << "include($${PLUGIN_SHARED_DIR}/include.pri)\n";
        stream << "\n";
        stream << "#COMPUTREE += ctlibio\n";
        stream << "#COMPUTREE += ctliblas\n";
        stream << "#COMPUTREE += ctlibmetrics\n";
        stream << "#COMPUTREE += ctlibfilters\n";
        stream << "#COMPUTREE += ctlibstdactions\n";
        stream << "\n";
        stream << "include($${CT_PREFIX}/include_ct_library.pri)\n";
        stream << "\n";
        stream << "#QT += concurrent\n";
        stream << "#QT += xml\n";
        stream << "#QT += xmlpatterns\n";
        stream << "#QT += network\n";
        stream << "#QT += sql\n";
        stream << "\n";
        stream << "TARGET = " << _target << "\n";
        stream << "\n";
        stream << "HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \\\n";
        stream << "    " << _entryName.toLower() << ".h \\\n";
        stream << "    " << _managerName.toLower() << ".h\n";
        stream << "SOURCES += \\\n";
        stream << "    " << _entryName.toLower() << ".cpp \\\n";
        stream << "    " << _managerName.toLower() << ".cpp\n";
        stream << "\n";
        stream << "TRANSLATIONS += languages/" << _proName << "_en.ts \\\n";
        stream << "                languages/" << _proName << "_fr.ts\n";


        proFile.close();
        return true;
    }
    return false;
}

bool PluginCreator::createPluginEntry_h()
{
    QFile entryFileh(QString("%1/%2.h").arg(_pluginDirectory).arg(_entryName.toLower()));

    if (entryFileh.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&entryFileh);
        stream.setCodec("UTF-8");

        stream << "#ifndef " << _pluginCode << "_PLUGINENTRY_H\n";
        stream << "#define " << _pluginCode << "_PLUGINENTRY_H\n";
        stream << "\n";
        stream << "#include \"interfaces.h\"\n";
        stream << "\n";
        stream << "class " << _pluginCode << "_PluginManager;\n";
        stream << "\n";
        stream << "class " << _pluginCode << "_PluginEntry : public PluginEntryInterface\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "\n";
        stream << "#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)\n";
        stream << "    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)\n";
        stream << "#endif\n";
        stream << "\n";
        stream << "    Q_INTERFACES(PluginEntryInterface)\n";
        stream << "\n";
        stream << "public:\n";
        stream << "    " << _pluginCode << "_PluginEntry();\n";
        stream << "    ~" << _pluginCode << "_PluginEntry();\n";
        stream << "\n";
        stream << "    QString getVersion() const;\n";       
        stream << "    CT_AbstractStepPlugin* getPlugin() const;\n";
        stream << "\n";
        stream << "private:\n";
        stream << "    " << _pluginCode << "_PluginManager *_pluginManager;\n";
        stream << "};\n";
        stream << "\n";
        stream << "#endif // " << _pluginCode << "_PLUGINENTRY_H";

        entryFileh.close();
        return true;
    }
    return false;
}

bool PluginCreator::createPluginEntry_cpp()
{
    QFile entryFilecpp(QString("%1/%2.cpp").arg(_pluginDirectory).arg(_entryName.toLower()));


    if (entryFilecpp.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&entryFilecpp);
        stream.setCodec("UTF-8");

        stream << "#include \"" << _pluginCode.toLower() << "_pluginentry.h\"\n";
        stream << "#include \"" << _pluginCode.toLower() << "_pluginmanager.h\"\n";
        stream << "\n";
        stream << _pluginCode << "_PluginEntry::" << _pluginCode << "_PluginEntry()\n";
        stream << "{\n";
        stream << "    _pluginManager = new " << _pluginCode << "_PluginManager();\n";
        stream << "}\n";
        stream << "\n";
        stream << _pluginCode << "_PluginEntry::~" << _pluginCode << "_PluginEntry()\n";
        stream << "{\n";
        stream << "    delete _pluginManager;\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _pluginCode << "_PluginEntry::getVersion() const\n";
        stream << "{\n";
        stream << "    return \"1.0\";\n";
        stream << "}\n";
        stream << "\n";                      
        stream << "CT_AbstractStepPlugin* " << _pluginCode << "_PluginEntry::getPlugin() const\n";
        stream << "{\n";
        stream << "    return _pluginManager;\n";
        stream << "}\n";
        stream << "\n";
        stream << "#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)\n";
        stream << "    Q_EXPORT_PLUGIN2(" << _target << ", " << _pluginCode << "_PluginEntry)\n";
        stream << "#endif\n";


        entryFilecpp.close();
        return true;
    }
    return false;
}

bool PluginCreator::createPluginManager_h()
{       
    QFile managerFileh(QString("%1/%2.h").arg(_pluginDirectory).arg(_managerName.toLower()));

    if (managerFileh.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&managerFileh);
        stream.setCodec("UTF-8");

        stream << "#ifndef " << _pluginCode << "_PLUGINMANAGER_H\n";
        stream << "#define " << _pluginCode << "_PLUGINMANAGER_H\n";
        stream << "\n";
        stream << "#include \"ct_abstractstepplugin.h\"\n";
        stream << "\n";
        stream << "class " << _pluginCode << "_PluginManager : public CT_AbstractStepPlugin\n";
        stream << "{\n";
        stream << "public:\n";
        stream << "    " << _pluginCode << "_PluginManager();\n";
        stream << "    ~" << _pluginCode << "_PluginManager();\n";
        stream << "\n";
        stream << "    QString getPluginURL() const {return QString(\"http://rdinnovation.onf.fr/projects/PLUGINS-PROJECT-NAME-HERE/wiki\");}\n";
        stream << "    virtual QString getPluginOfficialName() const;\n";
        stream << "    virtual QString getPluginRISCitation() const;\n";
        stream << "\n";
        stream << "protected:\n";
        stream << "\n";
        stream << "    bool loadGenericsStep();\n";
        stream << "    bool loadOpenFileStep();\n";
        stream << "    bool loadCanBeAddedFirstStep();\n";
        stream << "    bool loadActions();\n";
        stream << "    bool loadExporters();\n";
        stream << "    bool loadReaders();\n";
        stream << "    bool loadMetrics();\n";
        stream << "    bool loadFilters();\n";
        stream << "\n";
        stream << "};\n";
        stream << "\n";
        stream << "#endif // " << _pluginCode << "_PLUGINMANAGER_H\n";

        managerFileh.close();
        return  true;
    }
    return false;

}

bool PluginCreator::createPluginManager_cpp()
{
    QFile managerFilecpp(QString("%1/%2.cpp").arg(_pluginDirectory).arg(_managerName.toLower()));

    if (managerFilecpp.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&managerFilecpp);
        stream.setCodec("UTF-8");

        stream << "#include \"" << _pluginCode.toLower() << "_pluginmanager.h\"\n";
        stream << "#include \"ct_stepseparator.h\"\n";
        stream << "#include \"ct_steploadfileseparator.h\"\n";
        stream << "#include \"ct_stepcanbeaddedfirstseparator.h\"\n";
        stream << "#include \"ct_actions/ct_actionsseparator.h\"\n";
        stream << "#include \"ct_exporter/ct_standardexporterseparator.h\"\n";
        stream << "#include \"ct_reader/ct_standardreaderseparator.h\"\n";
        stream << "#include \"ct_actions/abstract/ct_abstractaction.h\"\n";
        stream << "\n";
        stream << "// Inclure ici les entetes des classes definissant des étapes/actions/exporters ou readers\n";
        stream << "\n";
        stream << _pluginCode << "_PluginManager::" << _pluginCode << "_PluginManager() : CT_AbstractStepPlugin()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << _pluginCode << "_PluginManager::~" << _pluginCode << "_PluginManager()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";

        stream << "QString " << _pluginCode << "_PluginManager::" << "getPluginOfficialName() const\n";
        stream << "{\n";
        stream << "    return \"OfficialName\";\n";
        stream << "}\n";
        stream << "\n";

        stream << "QString " << _pluginCode << "_PluginManager::" << "getPluginRISCitation() const\n";
        stream << "{\n";
        stream << "    return \"TY  - COMP\\n\"\n";
        stream << "           \"TI  - Title\\n\"\n";
        stream << "           \"AU  - Name, forename\\n\"\n";
        stream << "           \"PB  - Organization\\n\"\n";
        stream << "           \"PY  - Year\\n\"\n";
        stream << "           \"UR  - http://url\\n\"\n";
        stream << "           \"ER  - \\n\";\n";
        stream << "}\n";
        stream << "\n";

        stream << "bool " << _pluginCode << "_PluginManager::loadGenericsStep()\n";
        stream << "{\n";
        stream << "    //addNewRastersStep<NOM_CLASSE_ETAPE>(CT_StepsMenu::LP_DEM);\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadOpenFileStep()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadCanBeAddedFirstStep()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadActions()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadExporters()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadReaders()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadMetrics()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _pluginCode << "_PluginManager::loadFilters()\n";
        stream << "{\n";
        stream << "\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";

        managerFilecpp.close();
        return true;
    }
    return false;

}
