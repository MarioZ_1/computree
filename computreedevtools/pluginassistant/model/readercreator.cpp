#include "readercreator.h"

#include <QTextStream>
#include <QDir>

ReaderCreator::ReaderCreator(QString directory, QString name) : QObject()
{
    _directory = directory;
    _name = name;
}

QString ReaderCreator::createReaderFiles()
{
    QString errors = "";

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas', veuillez en choisir un autre svp");
    }

    dir.mkdir("reader");


    if (!create_ReaderFile_h()) {errors.append(tr("Impossible de créer le fichier reader (.h)\n"));}
    if (!create_ReaderFile_cpp()) {errors.append(tr("Impossible de créer le fichier reader (.cpp)\n"));}

    return errors;
}

bool ReaderCreator::create_ReaderFile_h()
{
    QFile file(QString("%1/reader/%2.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";

        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool ReaderCreator::create_ReaderFile_cpp()
{
    QFile file(QString("%1/reader/%2.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"reader/" << _name.toLower() << ".h\"\n";

        file.close();
        return true;
    }
    return false;
}

