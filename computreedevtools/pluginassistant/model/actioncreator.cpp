#include "actioncreator.h"

#include <QTextStream>
#include <QDir>

ActionCreator::ActionCreator(QString directory, QString name) : QObject()
{
    _directory = directory;
    _name = name;
}

QString ActionCreator::createActionFiles()
{
    QString errors = "";

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas', veuillez en choisir un autre svp");
    }

    dir.mkdir("actions");
    dir.mkdir("views");

    QDir dir2(QString("%1/views").arg(_directory));
    dir2.mkdir("actions");

    if (!create_ActionFile_h()) {errors.append(tr("Impossible de créer le fichier action (.h)\n"));}
    if (!create_ActionFile_cpp()) {errors.append(tr("Impossible de créer le fichier action (.cpp)\n"));}
    if (!create_ActionOptionFile_h()) {errors.append(tr("Impossible de créer le fichier actionOption (.h)\n"));}
    if (!create_ActionOptionFile_cpp()) {errors.append(tr("Impossible de créer le fichier actionOption (.cpp)\n"));}
    if (!create_ActionOptionFile_ui()) {errors.append(tr("Impossible de créer le fichier actionOption (.ui)\n"));}

    return errors;
}

bool ActionCreator::create_ActionFile_h()
{
    QFile file(QString("%1/actions/%2.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";
        stream << "\n";
        stream << "#include \"views/actions/" << _name.toLower() << "options.h\"\n";
        stream << "#include \"ct_actions/abstract/ct_abstractactionforgraphicsview.h\"\n";
        stream << "\n";
        stream << "#include <QRect>\n";
        stream << "\n";
        stream << "class " << _name << " : public CT_AbstractActionForGraphicsView\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "public:\n";
        stream << "\n";
        stream << "    " << _name << "();\n";
        stream << "\n";
        stream << "    ~" << _name << "();\n";
        stream << "\n";
        stream << "    QString uniqueName() const;\n";
        stream << "    QString title() const;\n";
        stream << "    QString description() const;\n";
        stream << "    QIcon icon() const;\n";
        stream << "    QString type() const;\n";
        stream << "\n";
        stream << "    void init();\n";
        stream << "\n";
        stream << "    bool mousePressEvent(QMouseEvent *e);\n";
        stream << "    bool mouseMoveEvent(QMouseEvent *e);\n";
        stream << "    bool mouseReleaseEvent(QMouseEvent *e);\n";
        stream << "    bool wheelEvent(QWheelEvent *e);\n";
        stream << "\n";
        stream << "    bool keyPressEvent(QKeyEvent *e);\n";
        stream << "    bool keyReleaseEvent(QKeyEvent *e);\n";
        stream << "\n";
        stream << "    void draw(GraphicsViewInterface &view, PainterInterface &painter);\n";
        stream << "    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);\n";
        stream << "\n";
        stream << "    CT_AbstractAction* copy() const;\n";
        stream << "\n";
        stream << "public slots:\n";
        stream << "    void toDoIfButtonPushed();\n";
        stream << "    void redraw();\n";
        stream << "\n";
        stream << "private:\n";
        stream << "    int         _value;\n";
        stream << "\n";
        stream << "};\n";
        stream << "\n";
        stream << "\n";
        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool ActionCreator::create_ActionFile_cpp()
{
    QFile file(QString("%1/actions/%2.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"actions/" << _name.toLower() << ".h\"\n";
        stream << "#include \"ct_global/ct_context.h\"\n";
        stream << "#include <QMouseEvent>\n";
        stream << "#include <QKeyEvent>\n";
        stream << "#include <QIcon>\n";
        stream << "#include <QPainter>\n";
        stream << "\n";
        stream << "" << _name << "::" << _name << "() : CT_AbstractActionForGraphicsView()\n";
        stream << "{\n";
        stream << "    _value = 0;\n";
        stream << "}\n";
        stream << "\n";
        stream << "" << _name << "::~" << _name << "()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::uniqueName() const\n";
        stream << "{\n";
        stream << "    return \"" << _name << "\";\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::title() const\n";
        stream << "{\n";
        stream << "    return \"Action Exemple\";\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::description() const\n";
        stream << "{\n";
        stream << "    return \"Exemple d'action\";\n";
        stream << "}\n";
        stream << "\n";
        stream << "QIcon " << _name << "::icon() const\n";
        stream << "{\n";
        stream << "    return QIcon(\":/icons/select_rectangular.png\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::type() const\n";
        stream << "{\n";
        stream << "    return CT_AbstractAction::TYPE_INFORMATION;\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::init()\n";
        stream << "{\n";
        stream << "    CT_AbstractActionForGraphicsView::init();\n";
        stream << "\n";
        stream << "    if(nOptions() == 0)\n";
        stream << "    {\n";
        stream << "        // create the option widget if it was not already created\n";
        stream << "        " << _name << "Options *option = new " << _name << "Options(this);\n";
        stream << "\n";
        stream << "        // add the options to the graphics view\n";
        stream << "        graphicsView()->addActionOptions(option);\n";
        stream << "\n";
        stream << "        connect(option, SIGNAL(buttonPushed()), this, SLOT(toDoIfButtonPushed()));\n";
        stream << "        connect(option, SIGNAL(parametersChanged()), this, SLOT(redraw()));\n";
        stream << "\n";
        stream << "        // register the option to the superclass, so the hideOptions and showOptions\n";
        stream << "        // is managed automatically\n";
        stream << "        registerOption(option);\n";
        stream << "\n";
        stream << "        document()->redrawGraphics();\n";
        stream << "    }\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::toDoIfButtonPushed()\n";
        stream << "{\n";
        stream << "    _value++;\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::redraw()\n";
        stream << "{\n";
        stream << "    document()->redrawGraphics();\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::mousePressEvent(QMouseEvent *e)\n";
        stream << "{\n";
        stream << "    if ((e->buttons() & Qt::LeftButton) && (e->modifiers()  & Qt::ControlModifier))\n";
        stream << "    {\n";
        stream << "        _value++;\n";
        stream << "        redraw();\n";
        stream << "        return true;\n";
        stream << "    }\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::mouseMoveEvent(QMouseEvent *e)\n";
        stream << "{\n";
        stream << "    Q_UNUSED(e);\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::mouseReleaseEvent(QMouseEvent *e)\n";
        stream << "{\n";
        stream << "    Q_UNUSED(e);\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::wheelEvent(QWheelEvent *e)\n";
        stream << "{\n";
        stream << "    if (e->modifiers()  & Qt::ControlModifier)\n";
        stream << "    {\n";
        stream << "        if (e->delta()>0)\n";
        stream << "        {\n";
        stream << "            _value++;\n";
        stream << "        }\n";
        stream << "        redraw();\n";
        stream << "        return true;\n";
        stream << "    }\n";
        stream << "\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::keyPressEvent(QKeyEvent *e)\n";
        stream << "{\n";
        stream << "    if((e->key() == Qt::Key_A) && !e->isAutoRepeat())\n";
        stream << "    {\n";
        stream << "        _value++;\n";
        stream << "        redraw();\n";
        stream << "        return true;\n";
        stream << "    }\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::keyReleaseEvent(QKeyEvent *e)\n";
        stream << "{\n";
        stream << "    Q_UNUSED(e);\n";
        stream << "    return false;\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::draw(GraphicsViewInterface &view, PainterInterface &painter)\n";
        stream << "{\n";
        stream << "    Q_UNUSED(view)\n";
        stream << "\n";
        stream << "    " << _name << "Options *option = (" << _name << "Options*)optionAt(0);\n";
        stream << "\n";
        stream << "    painter.save();\n";
        stream << "\n";
        stream << "    if (option->isExemple2Checked())\n";
        stream << "    {\n";
        stream << "        painter.setColor(0,255,0);\n";
        stream << "        painter.drawCircle(0,0,0,2);\n";
        stream << "    }\n";
        stream << "\n";
        stream << "    painter.restore();\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::drawOverlay(GraphicsViewInterface &view, QPainter &painter)\n";
        stream << "{\n";
        stream << "    Q_UNUSED(view)\n";
        stream << "\n";
        stream << "    int y = painter.fontMetrics().height()+2;\n";
        stream << "\n";
        stream << "    painter.save();\n";
        stream << "\n";
        stream << "    painter.setPen(QColor(255,255,255,127));\n";
        stream << "    painter.drawText(2, y, QString(\"Valeur = %1\").arg(_value));\n";
        stream << "\n";
        stream << "    painter.restore();\n";
        stream << "\n";
        stream << "}\n";
        stream << "\n";
        stream << "CT_AbstractAction* " << _name << "::copy() const\n";
        stream << "{\n";
        stream << "    return new " << _name << "();\n";
        stream << "}\n";

        file.close();
        return true;
    }
    return false;
}

bool ActionCreator::create_ActionOptionFile_h()
{
    QFile file(QString("%1/views/actions/%2options.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#ifndef " << _name.toUpper() << "OPTIONS_H\n";
        stream << "#define " << _name.toUpper() << "OPTIONS_H\n";
        stream << "\n";
        stream << "#include \"ct_view/actions/abstract/ct_gabstractactionoptions.h\"\n";
        stream << "\n";
        stream << "class " << _name << ";\n";
        stream << "\n";
        stream << "namespace Ui {\n";
        stream << "class " << _name << "Options;\n";
        stream << "}\n";
        stream << "\n";
        stream << "class " << _name << "Options : public CT_GAbstractActionOptions\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "\n";
        stream << "public:\n";
        stream << "\n";
        stream << "    explicit " << _name << "Options(const " << _name << " *action);\n";
        stream << "    ~" << _name << "Options();\n";
        stream << "\n";
        stream << "    bool isExemple2Checked();\n";
        stream << "\n";
        stream << "private:\n";
        stream << "    Ui::" << _name << "Options *ui;\n";
        stream << "\n";
        stream << "signals:\n";
        stream << "    void parametersChanged();\n";
        stream << "    void buttonPushed();\n";
        stream << "\n";
        stream << "private slots:\n";
        stream << "    void on_pb_exemple_clicked();\n";
        stream << "    void on_cb_exemple_toggled(bool checked);\n";
        stream << "};\n";
        stream << "\n";
        stream << "#endif // " << _name.toUpper() << "OPTIONS_H\n";

        file.close();
        return true;
    }
    return false;}

bool ActionCreator::create_ActionOptionFile_cpp()
{
    QFile file(QString("%1/views/actions/%2options.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"" << _name.toLower() << "options.h\"\n";
        stream << "#include \"ui_" << _name.toLower() << "options.h\"\n";
        stream << "\n";
        stream << "#include \"actions/" << _name.toLower() << ".h\"\n";
        stream << "\n";
        stream << "#include <QColorDialog>\n";
        stream << "\n";
        stream << "" << _name << "Options::" << _name << "Options(const " << _name << " *action) :\n";
        stream << "    CT_GAbstractActionOptions(action),\n";
        stream << "    ui(new Ui::" << _name << "Options())\n";
        stream << "{\n";
        stream << "    ui->setupUi(this);\n";
        stream << "\n";
        stream << "    ui->pb_exemple->setToolTip(\"Add 1 (key A, CTRL-left-clic ou CTRL-wheel)\");\n";
        stream << "    ui->cb_exemple->setToolTip(\"Show green circle\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "" << _name << "Options::~" << _name << "Options()\n";
        stream << "{\n";
        stream << "    delete ui;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "Options::isExemple2Checked()\n";
        stream << "{\n";
        stream << "    return ui->cb_exemple->isChecked();\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "Options::on_pb_exemple_clicked()\n";
        stream << "{\n";
        stream << "    emit buttonPushed();\n";
        stream << "    emit parametersChanged();\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "Options::on_cb_exemple_toggled(bool checked)\n";
        stream << "{\n";
        stream << "    emit parametersChanged();\n";
        stream << "}\n";

        file.close();
        return true;
    }
    return false;}

bool ActionCreator::create_ActionOptionFile_ui()
{
    QFile file(QString("%1/views/actions/%2options.ui").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        stream << "<ui version=\"4.0\">\n";
        stream << " <class>" << _name << "Options</class>\n";
        stream << " <widget class=\"QWidget\" name=\"" << _name << "Options\">\n";
        stream << "  <property name=\"geometry\">\n";
        stream << "   <rect>\n";
        stream << "    <x>0</x>\n";
        stream << "    <y>0</y>\n";
        stream << "    <width>709</width>\n";
        stream << "    <height>50</height>\n";
        stream << "   </rect>\n";
        stream << "  </property>\n";
        stream << "  <property name=\"windowTitle\">\n";
        stream << "   <string>Form</string>\n";
        stream << "  </property>\n";
        stream << "  <layout class=\"QHBoxLayout\" name=\"horizontalLayout\">\n";
        stream << "   <property name=\"leftMargin\">\n";
        stream << "    <number>0</number>\n";
        stream << "   </property>\n";
        stream << "   <property name=\"topMargin\">\n";
        stream << "    <number>0</number>\n";
        stream << "   </property>\n";
        stream << "   <property name=\"rightMargin\">\n";
        stream << "    <number>0</number>\n";
        stream << "   </property>\n";
        stream << "   <property name=\"bottomMargin\">\n";
        stream << "    <number>0</number>\n";
        stream << "   </property>\n";
        stream << "   <item>\n";
        stream << "    <widget class=\"QWidget\" name=\"wd_toolbar\" native=\"true\">\n";
        stream << "     <layout class=\"QVBoxLayout\" name=\"verticalLayout\">\n";
        stream << "      <property name=\"spacing\">\n";
        stream << "       <number>0</number>\n";
        stream << "      </property>\n";
        stream << "      <property name=\"leftMargin\">\n";
        stream << "       <number>0</number>\n";
        stream << "      </property>\n";
        stream << "      <property name=\"topMargin\">\n";
        stream << "       <number>0</number>\n";
        stream << "      </property>\n";
        stream << "      <property name=\"rightMargin\">\n";
        stream << "       <number>0</number>\n";
        stream << "      </property>\n";
        stream << "      <property name=\"bottomMargin\">\n";
        stream << "       <number>0</number>\n";
        stream << "      </property>\n";
        stream << "      <item>\n";
        stream << "       <widget class=\"QWidget\" name=\"widget\" native=\"true\">\n";
        stream << "        <layout class=\"QHBoxLayout\" name=\"horizontalLayout_2\">\n";
        stream << "         <item>\n";
        stream << "          <widget class=\"QPushButton\" name=\"pb_exemple\">\n";
        stream << "           <property name=\"text\">\n";
        stream << "            <string>exemple</string>\n";
        stream << "           </property>\n";
        stream << "          </widget>\n";
        stream << "         </item>\n";
        stream << "         <item>\n";
        stream << "          <widget class=\"QCheckBox\" name=\"cb_exemple\">\n";
        stream << "           <property name=\"text\">\n";
        stream << "            <string>exemple 2</string>\n";
        stream << "           </property>\n";
        stream << "          </widget>\n";
        stream << "         </item>\n";
        stream << "         <item>\n";
        stream << "          <spacer name=\"horizontalSpacer\">\n";
        stream << "           <property name=\"orientation\">\n";
        stream << "            <enum>Qt::Horizontal</enum>\n";
        stream << "           </property>\n";
        stream << "           <property name=\"sizeHint\" stdset=\"0\">\n";
        stream << "            <size>\n";
        stream << "             <width>40</width>\n";
        stream << "             <height>20</height>\n";
        stream << "            </size>\n";
        stream << "           </property>\n";
        stream << "          </spacer>\n";
        stream << "         </item>\n";
        stream << "        </layout>\n";
        stream << "       </widget>\n";
        stream << "      </item>\n";
        stream << "     </layout>\n";
        stream << "    </widget>\n";
        stream << "   </item>\n";
        stream << "  </layout>\n";
        stream << " </widget>\n";
        stream << " <resources/>\n";
        stream << " <connections/>\n";
        stream << "</ui>\n";

        file.close();
        return true;
    }
    return false;
}
