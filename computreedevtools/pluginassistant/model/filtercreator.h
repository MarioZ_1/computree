#ifndef FILTERCCREATOR_H
#define FILTERCCREATOR_H

#include <QObject>

class FilterCreator : public QObject
{
    Q_OBJECT
public:
    explicit FilterCreator(QString directory, QString name);

    QString createFilterFiles();

private:

    QString     _directory;
    QString     _name;


    bool create_FilterFile_h();
    bool create_FilterFile_cpp();

};

#endif // FILTERCCREATOR_H
