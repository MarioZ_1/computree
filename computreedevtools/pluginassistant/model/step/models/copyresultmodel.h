#ifndef COPYRESULTMODEL_H
#define COPYRESULTMODEL_H

#include "model/step/models/abstractcopymodel.h"
#include "model/step/models/inresultmodel.h"

class COPYResultModel : public AbstractCopyModel
{
public:
    COPYResultModel();
    void init(INResultModel *inModel);

    void init(QString alias, QString name, QString desc);

    QString getName();
    virtual AbstractCopyModel::ModelType getModelType() {return AbstractCopyModel::M_Result_COPY;}
    virtual bool isValid();
    virtual QString getAutoRenameName();
    QString getResultName();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "");
    virtual QString getComputeContent(QString resultName = "", QString parentName = "", int indent = 1, bool rootGroup = false);


};

#endif // COPYRESULTMODEL_H
