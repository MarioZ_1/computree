#include "model/step/models/inresultmodel.h"
#include "model/step/models/ingroupmodel.h"
#include "view/step/models/inresultwidget.h"
#include "model/step/tools.h"
#include "assert.h"

INResultModel::INResultModel() : AbstractInModel()
{
    _widget = new INResultWidget(this);
    setText(getPrefixedAlias());
}

bool INResultModel::isValid()
{
    if (rowCount()!=1) {return false;}
    return AbstractInModel::isValid();
}

bool INResultModel::isRecursive()
{
    return ((INResultWidget*) _widget)->isRecursive();
}


bool INResultModel::isZeroOrMoreRootGroup()
{
    return ((INResultWidget*) _widget)->isZeroOrMoreRootGroup();
}

bool INResultModel::isCopyResult()
{
    return ((INResultWidget*) _widget)->isCopyResult();
}

QString INResultModel::getName()
{
    return "resIn_" + getAlias();
}

void INResultModel::getIncludes(QSet<QString> &list)
{
    list.insert("#include \"ct_result/ct_resultgroup.h\"\n");

    if (isCopyResult())
    {
        list.insert("#include \"ct_result/model/inModel/ct_inresultmodelgrouptocopy.h\"\n");
    } else {
        list.insert("#include \"ct_result/model/inModel/ct_inresultmodelgroup.h\"\n");
    }

    getChildrenIncludes(list);
}

QString INResultModel::getCreateInResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    Q_UNUSED(parentDEF);
    Q_UNUSED(resultModelName);
    Q_UNUSED(rootGroup);

    QString result = "";

    QString resultClass = "CT_InResultModelGroup";
    QString resultCreator = "createNewInResultModel";

    if (isCopyResult())
    {
        resultClass = "CT_InResultModelGroupToCopy";
        resultCreator = "createNewInResultModelForCopy";
    }

    result += Tools::getIndentation(1) + resultClass + " *" + getName() + " = " + resultCreator + "(" + getDef();

    if (getDisplayableName() != "" || getDescription() != "" || isRecursive())
    {
        result += ", " + Tools::trs(getDisplayableName());
    }

    if (getDescription() != "" || isRecursive())
    {
        result += ", " + Tools::trs(getDescription());
    }

    if (isRecursive())
    {
        result += ", true";
    }

    result += ");";
    result += "\n";

    INGroupModel* rootGrp = (INGroupModel*) child(0);

    if (isZeroOrMoreRootGroup())
    {
        result += Tools::getIndentation(1) + getName() + "->setZeroOrMoreRootGroup();\n";
        if (rootGrp != NULL)
        {
            result += rootGrp->getCreateInResultModelListProtectedContent("", getName(), false);
        }
    } else {
        if (rootGrp != NULL)
        {
            result += rootGrp->getCreateInResultModelListProtectedContent("", getName(), true);
        }
    }

    return result;
}

QString INResultModel::getComputeContent(QString parentName, int indent, bool rootGroup)
{
    Q_UNUSED(parentName);
    Q_UNUSED(indent);
    Q_UNUSED(rootGroup);

    QString result = "";

    INGroupModel* rootGrp = (INGroupModel*) child(0);

    if (rootGrp != NULL)
    {
        result += rootGrp->getComputeContent(getName(), 1, true);
    }

    return result;
}

