#ifndef INRESULTMODEL_H
#define INRESULTMODEL_H

#include "model/step/models/abstractinmodel.h"

class INResultModel : public AbstractInModel
{
public:
    INResultModel();

    virtual AbstractInModel::ModelType getModelType() {return AbstractInModel::M_Result_IN;}
    virtual bool isValid();

    bool isRecursive();
    bool isZeroOrMoreRootGroup();
    bool isCopyResult();
    QString getName();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateInResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false);
    virtual QString getComputeContent(QString parentName = "", int indent = 1, bool rootGroup = false);

};

#endif // INRESULTMODEL_H
