#ifndef COPYGROUPMODEL_H
#define COPYGROUPMODEL_H

#include "model/step/models/ingroupmodel.h"
#include "model/step/models/abstractcopymodel.h"

class COPYGroupModel : public AbstractCopyModel
{
public:
    COPYGroupModel();
    void init(INGroupModel *inModel);
    void init(QString alias, QString name, QString desc);

    QString getName();
    virtual AbstractCopyModel::ModelType getModelType() {return AbstractCopyModel::M_Group_COPY;}

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "");
    virtual QString getComputeContent(QString resultName, QString parentName = "", int indent = 1, bool rootGroup = false);
};

#endif // COPYGROUPMODEL_H
