#include "model/step/models/copyresultmodel.h"
#include "model/step/models/copygroupmodel.h"
#include "view/step/models/copyresultwidget.h"
#include "model/step/tools.h"
#include "assert.h"

COPYResultModel::COPYResultModel() : AbstractCopyModel()
{
    _widget = new COPYResultWidget(this);
    setText(getPrefixedAlias() + " (copie)");
}


void COPYResultModel::init(QString alias, QString name, QString desc)
{
    ((COPYResultWidget*)_widget)->init(alias, name, desc);
    setText(getPrefixedAlias() + " (copie)");
}

void COPYResultModel::init(INResultModel *inModel)
{
    init(inModel->getAlias(), inModel->getDisplayableName(), inModel->getDescription());
}

QString COPYResultModel::getAutoRenameName()
{
    return "";
}

bool COPYResultModel::isValid()
{
    if (rowCount()!=1) {return false;}
    return AbstractCopyModel::isValid();
}

QString COPYResultModel::getName()
{
    return "resCpy_" + getAlias();
}

void COPYResultModel::getIncludes(QSet<QString> &list)
{
    list.insert("#include \"ct_result/ct_resultgroup.h\"\n");
    list.insert("#include \"ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h\"\n");

    getChildrenIncludes(list);
}

QString COPYResultModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName)
{
    Q_UNUSED(parentDEF);
    Q_UNUSED(resultModelName);

    QString result = "";

    result += Tools::getIndentation(1) + "CT_OutResultModelGroupToCopyPossibilities *" + getName() + " = createNewOutResultModelToCopy" + "(" + getDef()+ ");\n";

    result += Tools::getIndentation(1) + "\n";
    result += Tools::getIndentation(1) + "if (" + getName() + " != NULL)\n";
    result += Tools::getIndentation(1) + "{\n";

    COPYGroupModel* rootGrp = (COPYGroupModel*) child(0);

    if (rootGrp != NULL)
    {
        result += rootGrp->getCreateOutResultModelListProtectedContent("", getName());
    }

    result += Tools::getIndentation(1) + "}\n";

    return result;
}

QString COPYResultModel::getComputeContent(QString resultName, QString parentName, int indent, bool rootGroup)
{
    Q_UNUSED(resultName);
    Q_UNUSED(parentName);
    Q_UNUSED(indent);
    Q_UNUSED(rootGroup);

    QString result = "";

    COPYGroupModel* rootGrp = (COPYGroupModel*) child(0);

    if (rootGrp != NULL && rootGrp->getStatus() != AbstractCopyModel::S_DeletedCopy)
    {
        result += rootGrp->getComputeContent(getName(), getName(), 1, true);
    }

    return result;
}

