#ifndef ABTRACTOUTMODEL_H
#define ABTRACTOUTMODEL_H

#include "qstandarditemmodel.h"
#include "view/step/models/abstractoutwidget.h"
#include "qset.h"

class AbstractOutModel : public QStandardItem
{
public:

    enum ModelType
    {
        M_Result_OUT,
        M_Group_OUT,
        M_Item_OUT
    };

    AbstractOutModel();
    ~AbstractOutModel();

    virtual AbstractOutModel::ModelType getModelType() = 0;
    virtual AbstractOutWidget* getWidget();

    virtual QString getName() = 0;
    virtual QString getDef();
    virtual QString getPrefixedAlias();
    virtual QString getAlias();
    virtual QString getDisplayableName();
    virtual QString getDescription();
    virtual bool isValid();

    virtual QString getOutModelsDefines();

    virtual void getIncludes(QSet<QString> &list) = 0;
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false) = 0;
    virtual QString getComputeContent(QString resultName, QString parentName) = 0;

    virtual void getChildrenIncludes(QSet<QString> &list);
    virtual void getChildrenCreateOutResultModelListProtectedContent(QString &result, QString parentDEF = "", QString resultModelName = "");
    virtual void getChildrenComputeContent(QString &result, QString resultName);

    void onAliasChange();

protected:
    AbstractOutWidget*  _widget;

};

#endif // ABTRACTOUTMODEL_H
