#ifndef INGROUPMODEL_H
#define INGROUPMODEL_H

#include "model/step/models/abstractinmodel.h"

class INGroupModel : public AbstractInModel
{
public:
    INGroupModel();

    QString getName();
    virtual AbstractInModel::ModelType getModelType() {return AbstractInModel::M_Group_IN;}

    QString getChoiceMode();
    QString getFinderMode();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateInResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false);
    virtual QString getComputeContent(QString parentName = "", int indent = 1, bool rootGroup = false);

};

#endif // INGROUPMODEL_H
