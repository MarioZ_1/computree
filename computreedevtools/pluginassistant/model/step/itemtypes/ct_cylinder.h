#ifndef CT_CYLINDER_H
#define CT_CYLINDER_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Cylinder : public AbstractItemType
{
public:
    CT_Cylinder();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_CYLINDER_H
