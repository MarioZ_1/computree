#include "abstractitemtype.h"
#include "model/step/tools.h"

AbstractItemType::AbstractItemType() : QObject()
{
}

QString AbstractItemType::getCreationCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += Tools::getIndentation(indent) + getTypeName() + "* " + name + " = ";
    result += "new " + getTypeName() + "(" + modelName + ", " + resultName + ");\n";
    return result;
}
