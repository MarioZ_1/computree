#ifndef CT_BEAM_H
#define CT_BEAM_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Beam : public AbstractItemType
{
public:
    CT_Beam();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_BEAM_H
