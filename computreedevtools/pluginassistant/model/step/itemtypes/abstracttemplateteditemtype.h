#ifndef ABSTRACTTEMPLATEDITEMTYPE_H
#define ABSTRACTTEMPLATEDITEMTYPE_H

#include "abstractitemtype.h"

#include <QString>
#include <QObject>
#include <QStringList>

class AbstractTemplatedItemType : public AbstractItemType
{
public:
    AbstractTemplatedItemType();

    virtual bool isTemplated() {return true;}
    virtual QStringList getTemplates();
};

#endif // ABSTRACTTEMPLATEDITEMTYPE_H
