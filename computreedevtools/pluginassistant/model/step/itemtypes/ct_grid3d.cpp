#include "ct_grid3d.h"
#include "model/step/tools.h"

CT_Grid3D::CT_Grid3D() : AbstractTemplatedItemType()
{
}

QString CT_Grid3D::getTypeName()
{
    return "CT_Grid3D";
}

QString CT_Grid3D::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Grid3D::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
