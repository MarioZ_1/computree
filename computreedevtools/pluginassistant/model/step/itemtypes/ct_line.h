#ifndef CT_LINE_H
#define CT_LINE_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Line : public AbstractItemType
{
public:
    CT_Line();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_LINE_H
