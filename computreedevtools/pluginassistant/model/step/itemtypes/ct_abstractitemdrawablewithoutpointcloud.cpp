#include "ct_abstractitemdrawablewithoutpointcloud.h"
#include "model/step/tools.h"

CT_AbstractItemDrawableWithoutPointCloud::CT_AbstractItemDrawableWithoutPointCloud() : AbstractItemType()
{
}

QString CT_AbstractItemDrawableWithoutPointCloud::getTypeName()
{
    return "CT_AbstractItemDrawableWithoutPointCloud";
}

QString CT_AbstractItemDrawableWithoutPointCloud::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_AbstractItemDrawableWithoutPointCloud::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    Q_UNUSED(modelName);
    Q_UNUSED(resultName);
    return "";
}
