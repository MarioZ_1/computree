#include "abstracttemplateteditemtype.h"

AbstractTemplatedItemType::AbstractTemplatedItemType() : AbstractItemType()
{
}

QStringList AbstractTemplatedItemType::getTemplates()
{
    QStringList list;
    list.append("int");
    list.append("short");
    list.append("float");
    list.append("double");
    list.append("bool");
    return list;
}
