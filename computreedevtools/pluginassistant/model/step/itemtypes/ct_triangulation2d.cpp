#include "ct_triangulation2d.h"
#include "model/step/tools.h"

CT_Triangulation2D::CT_Triangulation2D() : AbstractItemType()
{
}

QString CT_Triangulation2D::getTypeName()
{
    return "CT_Triangulation2D";
}

QString CT_Triangulation2D::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Triangulation2D::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
