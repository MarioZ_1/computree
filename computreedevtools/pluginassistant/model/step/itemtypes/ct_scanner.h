#ifndef CT_SCANNER_H
#define CT_SCANNER_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Scanner : public AbstractItemType
{
public:
    CT_Scanner();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_SCANNER_H
