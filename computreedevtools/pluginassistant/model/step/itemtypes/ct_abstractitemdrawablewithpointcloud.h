#ifndef CT_ABSTRACTITEMDRAWABLEWITHPOINTCLOUD_H
#define CT_ABSTRACTITEMDRAWABLEWITHPOINTCLOUD_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_AbstractItemDrawableWithPointCloud : public AbstractItemType
{
public:
    CT_AbstractItemDrawableWithPointCloud();

    virtual bool isInstanciable() {return false;}
    virtual QString getTypeName();
    virtual QString getInclude() {return "#include \"ct_itemdrawable/abstract/" + getTypeName().toLower() + ".h\"\n";}

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_ABSTRACTITEMDRAWABLEWITHPOINTCLOUD_H
