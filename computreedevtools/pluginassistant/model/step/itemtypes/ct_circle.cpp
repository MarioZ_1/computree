#include "ct_circle.h"
#include "model/step/tools.h"

CT_Circle::CT_Circle() : AbstractItemType()
{
}

QString CT_Circle::getTypeName()
{
    return "CT_Circle";
}

QString CT_Circle::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Circle::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += Tools::getIndentation(indent) + "CT_CircleData* circleData_" + name + " = new CT_CircleData(QVector3D(0,0,0), QVector3D(0,0,1),10);\n";
    result += Tools::getIndentation(indent) + getTypeName() + "* " + name + " = ";
    result += "new " + getTypeName() + "(" + modelName + ", " + resultName + ", circleData_" + name + ");\n";
    return result;
}
