#ifndef ABSTRACTITEMTYPE_H
#define ABSTRACTITEMTYPE_H

#include <QString>
#include <QObject>
#include <QStringList>

class AbstractItemType : QObject
{
public:
    AbstractItemType();

    virtual bool isInstanciable() {return true;}
    virtual QString getInclude() {return "#include \"ct_itemdrawable/" + getTypeName().toLower() + ".h\"\n";}

    virtual bool isTemplated() {return false;}
    virtual QStringList getTemplates() {return QStringList();}

    virtual QString getTypeName() = 0;
    virtual QString getInCode(int indent, QString name) = 0;
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName) = 0;
    virtual QString getCreationCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // ABSTRACTITEMTYPE_H
