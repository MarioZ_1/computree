#include "ct_scanner.h"
#include "model/step/tools.h"

CT_Scanner::CT_Scanner() : AbstractItemType()
{
}

QString CT_Scanner::getTypeName()
{
    return "CT_Scanner";
}

QString CT_Scanner::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Scanner::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
