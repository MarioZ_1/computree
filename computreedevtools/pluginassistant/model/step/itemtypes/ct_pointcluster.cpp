#include "ct_pointcluster.h"
#include "model/step/tools.h"

CT_PointCluster::CT_PointCluster() : AbstractItemType()
{
}

QString CT_PointCluster::getTypeName()
{
    return "CT_PointCluster";
}

QString CT_PointCluster::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_PointCluster::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
