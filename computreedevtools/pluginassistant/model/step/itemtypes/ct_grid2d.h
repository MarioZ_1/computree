#ifndef CT_GRID2D_H
#define CT_GRID2D_H

#include "model/step/itemtypes/abstracttemplateteditemtype.h"

class CT_Grid2D : public AbstractTemplatedItemType
{
public:
    CT_Grid2D();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_Grid2D_H
