#include "model/step/parameters/parameterfilechoice.h"
#include "model/step/tools.h"

ParameterFileChoice::ParameterFileChoice() : AbstractParameter()
{
    _widget = new WidgetFileChoice(this);
    onAliasChange();
}

void ParameterFileChoice::onAliasChange()
{
    setText(getName() + " (file choice)");
}

QString ParameterFileChoice::getParameterDeclaration()
{
    return Tools::getIndentation(1) + "QStringList" + Tools::getIndentation(1) + widget()->getAlias() + ";" +
           Tools::getIndentation(1) + "/*!< " + widget()->getDescription() + " */\n";
}

QString ParameterFileChoice::getParameterInitialization()
{
       return "";
}

QString ParameterFileChoice::getParameterDialogCommands()
{
    return Tools::getIndentation(1) + "configDialog->addFileChoice("+
            "\"" + widget()->getBeforeText()                        + "\", " +
            "CT_FileChoiceButton::" + widget()->getNeededFileType() + ", " +
            "\"" + widget()->getAfterText()                         + "\", " +
            widget()->getAlias()                                    + ");\n";
}

QString ParameterFileChoice::getParamaterDoc()
{
    return " * \\param " + widget()->getAlias() + " " + widget()->getDescription() + "\n";
}

