#ifndef PARAMETERSCREATOR_H
#define PARAMETERSCREATOR_H

#include <QObject>
#include <QStandardItemModel>

class ParametersCreator : public QObject
{
    Q_OBJECT
public:
    explicit ParametersCreator(QObject *parent = 0);

    inline QStandardItemModel *getStandardItemModel() {return _model;}

    QString getParametersDeclaration();
    QString getParametersInitialization();
    QString getParametersDialogCommands();
    QString getParamatersDoc();

private:
    QStandardItemModel *_model;

};

#endif // PARAMETERSCREATOR_H
