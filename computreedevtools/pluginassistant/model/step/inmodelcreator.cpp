#include "inmodelcreator.h"
#include "model/step/models/inresultmodel.h"
#include "model/step/models/ingroupmodel.h"
#include "model/step/models/initemmodel.h"

#include "model/step/tools.h"


InModelCreator::InModelCreator(QObject *parent) :
    QObject(parent)
{
    _model = new QStandardItemModel();
}

void InModelCreator::getIncludes(QSet<QString> &list)
{
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) root->child(i);
        item->getIncludes(list);
    }
}

QString InModelCreator::getInDefines()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) root->child(i);
        result += item->getInModelsDefines();
        result += "\n";
    }
    return result;
}

QString InModelCreator::getCreateInResultModelListProtectedContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) root->child(i);
        result += item->getCreateInResultModelListProtectedContent();
        result += "\n";
    }
    return result;
}

QString InModelCreator::getResultRecovery()
{
    QString result = "";
    QString result2 = "";
    bool realInResults = false;
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();

    if (count > 0)
    {
        for (int i = 0 ; i < count ; i++)
        {
            INResultModel* item = (INResultModel*) root->child(i);
            if (!item->isCopyResult())
            {
                result2 += Tools::getIndentation(1) + "CT_ResultGroup* " + item->getName() + " = inResultList.at(" + QString("%1").arg(i) + ");\n";
                realInResults = true;
            }
        }
        if (realInResults)
        {
            result += Tools::getIndentation(1) + "QList<CT_ResultGroup*> inResultList = getInputResults();\n";
            result += result2;
        }
    }
    return result;
}

QString InModelCreator::getComputeContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();

    for (int i = 0 ; i < count ; i++)
    {
        INResultModel* item = (INResultModel*) root->child(i);
        if (!item->isCopyResult())
        {
            result += item->getComputeContent();
        }
    }
    return result;
}
