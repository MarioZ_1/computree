#ifndef OUTMODELCREATOR_H
#define OUTMODELCREATOR_H

#include <QObject>
#include <QStandardItemModel>

class OutModelCreator : public QObject
{
    Q_OBJECT
public:
    explicit OutModelCreator(QObject *parent = 0);

    inline QStandardItemModel *getStandardItemModel() {return _model;}


    void getIncludes(QSet<QString> &list);
    QString getOutDefines();
    QString getCreateOutResultModelListProtectedContent();
    QString getResultRecovery(int &nextRank);
    QString getComputeContent();

private:
    QStandardItemModel *_model;

};

#endif // OUTMODELCREATOR_H
