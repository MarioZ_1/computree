#include "outmodelcreator.h"
#include "model/step/models/outresultmodel.h"
#include "model/step/models/outgroupmodel.h"
#include "model/step/models/outitemmodel.h"

#include "model/step/tools.h"

OutModelCreator::OutModelCreator(QObject *parent) :
    QObject(parent)
{
    _model = new QStandardItemModel();
}


void OutModelCreator::getIncludes(QSet<QString> &list)
{
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) root->child(i);
        item->getIncludes(list);
    }
}

QString OutModelCreator::getOutDefines()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) root->child(i);
        result += item->getOutModelsDefines();
        result += "\n";
    }
    return result;
}

QString OutModelCreator::getCreateOutResultModelListProtectedContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) root->child(i);
        result += item->getCreateOutResultModelListProtectedContent();
        result += "\n";
    }
    return result;
}

QString OutModelCreator::getResultRecovery(int &nextRank)
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();

    if (nextRank == 0 && count > 0)
    {
        result += Tools::getIndentation(1) + "QList<CT_ResultGroup*> outResultList = getOutResultList();\n";
    }
    for (int i = 0 ; i < count ; i++)
    {
        OUTResultModel* item = (OUTResultModel*) root->child(i);
        result += Tools::getIndentation(1) + "CT_ResultGroup* " + item->getName() + " = outResultList.at(" + QString("%1").arg(nextRank++) + ");\n";
    }
    return result;
}

QString OutModelCreator::getComputeContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) root->child(i);
        result += item->getComputeContent("", "");
    }
    return result;
}
