#ifndef ACTIONCREATOR_H
#define ACTIONCREATOR_H

#include <QObject>

class ActionCreator : public QObject
{
    Q_OBJECT
public:
    explicit ActionCreator(QString directory, QString name);

    QString createActionFiles();

private:

    QString     _directory;
    QString     _name;


    bool create_ActionFile_h();
    bool create_ActionFile_cpp();
    bool create_ActionOptionFile_h();
    bool create_ActionOptionFile_cpp();
    bool create_ActionOptionFile_ui();

};

#endif // ACTIONCREATOR_H
