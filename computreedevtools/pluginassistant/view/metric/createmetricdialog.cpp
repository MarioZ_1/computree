#include "view/metric/createmetricdialog.h"
#include "ui_createmetricdialog.h"

CreateMetricDialog::CreateMetricDialog(QString code, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateMetricDialog)
{
    ui->setupUi(this);

    ui->lb_begin->setText(QString("%1_Metric").arg(code.toUpper()));
    ui->le_name->setText("");
}

CreateMetricDialog::~CreateMetricDialog()
{
    delete ui;
}

QString CreateMetricDialog::getName()
{
    return QString("%1%2").arg(ui->lb_begin->text()).arg(ui->le_name->text());
}


