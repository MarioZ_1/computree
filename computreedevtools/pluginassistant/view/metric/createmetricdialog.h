#ifndef CREATEMETRICDIALOG_H
#define CREATEMETRICDIALOG_H

#include <QDialog>

namespace Ui {
class CreateMetricDialog;
}

class CreateMetricDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateMetricDialog(QString code, QWidget *parent = 0);
    ~CreateMetricDialog();

    QString getName();

private:
    Ui::CreateMetricDialog *ui;
};

#endif // CREATEMETRICDIALOG_H
