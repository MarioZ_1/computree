#ifndef CREATEPLUGINDIALOG_H
#define CREATEPLUGINDIALOG_H

#include <QDialog>

namespace Ui {
class CreatePluginDialog;
}

class CreatePluginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreatePluginDialog(QString pluginDirectory, QWidget *parent = 0);
    ~CreatePluginDialog();

    QString getDirectory();
    QString getCode();
    QString getName();

private slots:
    void on_code_textChanged(const QString &arg1);
    void on_pb_directory_clicked();

    void on_pb_validate_clicked();

private:
    Ui::CreatePluginDialog *ui;
};

#endif // CREATEPLUGINDIALOG_H
