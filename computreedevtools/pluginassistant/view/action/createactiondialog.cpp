#include "view/action/createactiondialog.h"
#include "ui_createactiondialog.h"

CreateActionDialog::CreateActionDialog(QString code, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateActionDialog)
{
    ui->setupUi(this);

    ui->lb_begin->setText(QString("%1_Action").arg(code.toUpper()));
    ui->le_name->setText("");
}

CreateActionDialog::~CreateActionDialog()
{
    delete ui;
}

QString CreateActionDialog::getName()
{
    return QString("%1%2").arg(ui->lb_begin->text()).arg(ui->le_name->text());
}


