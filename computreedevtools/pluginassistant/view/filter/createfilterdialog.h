#ifndef CREATEFILTERDIALOG_H
#define CREATEFILTERDIALOG_H

#include <QDialog>

namespace Ui {
class CreateFilterDialog;
}

class CreateFilterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateFilterDialog(QString code, QWidget *parent = 0);
    ~CreateFilterDialog();

    QString getName();

private:
    Ui::CreateFilterDialog *ui;
};

#endif // CREATEFILTERDIALOG_H
