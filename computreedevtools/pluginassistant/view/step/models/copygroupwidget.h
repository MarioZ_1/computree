#ifndef INGROUPWIDGET_H
#define INGROUPWIDGET_H


#include "view/step/models/abstractcopywidget.h"

namespace Ui {
    class COPYGroupWidget;
}

class COPYGroupWidget : public AbstractCopyWidget
{
    Q_OBJECT

public:

    enum ChoiceMode
    {
        C_OneIfMultiple,
        C_MultipleIfMultiple,
        C_DontChoose
    };

    enum FinderMode
    {
        F_Obligatory,
        F_Optional
    };

    explicit COPYGroupWidget(AbstractCopyModel* model, QWidget *parent = 0);
    void init(QString alias, QString name, QString desc);
    ~COPYGroupWidget();

    bool isvalid();
    QString getPrefixedAliad();
    QString getAlias();
    QString getDEF();
    QString getDisplayableName();
    QString getDescription();


private slots:
    void on_alias_textChanged(const QString &arg1);

private:
    Ui::COPYGroupWidget *ui;
};

#endif // INRESULTWIDGET_H
