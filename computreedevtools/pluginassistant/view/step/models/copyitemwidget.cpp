#include "copyitemwidget.h"
#include "ui_copyitemwidget.h"
#include "model/step/models/abstractcopymodel.h"
#include "model/step/tools.h"

COPYItemWidget::COPYItemWidget(AbstractCopyModel* model, QWidget *parent) :
    AbstractCopyWidget(model, parent),
    ui(new Ui::COPYItemWidget)
{
    ui->setupUi(this);

    // Ajout des classes d'itemdrawables disponibles

    QMapIterator<QString, AbstractItemType*> it(Tools::ITEMTYPE);
    while (it.hasNext())
    {
        it.next();
        if (it.value()->isInstanciable())
        {
            ui->cb_itemType->addItem(it.key());
        }
    }

    ui->cb_itemType->setCurrentIndex(0);

    setFocusProxy(ui->alias);
}

void COPYItemWidget::init(QString itemType, QString temp, QString alias, QString name, QString desc)
{
    ui->alias->setText(alias);
    ui->alias->setEnabled(false);
    ui->modelName->setText(name);
    ui->modelName->setEnabled(false);
    ui->modelDescription->setPlainText(desc);
    ui->modelDescription->setEnabled(false);

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        ui->cb_itemType->setCurrentIndex(ui->cb_itemType->findText(itemType));
#else
        ui->cb_itemType->setCurrentText(itemType);
#endif

    ui->cb_itemType->setEnabled(false);

    if (temp != "")
    {

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        ui->cb_template->setCurrentIndex(ui->cb_template->findText(temp));
#else
        ui->cb_template->setCurrentText(temp);
#endif

                ui->cb_template->setEnabled(false);
        ui->templateWidget->setVisible(true);
    } else {
        ui->templateWidget->setVisible(false);
    }
}

COPYItemWidget::~COPYItemWidget()
{
    delete ui;
}

bool COPYItemWidget::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString COPYItemWidget::getItemType()
{
    return ui->cb_itemType->currentText();
}

QString COPYItemWidget::getTemplate()
{
    return ui->cb_template->currentText();
}


QString COPYItemWidget::getItemTypeWithTemplate()
{
    QString templ = getTemplate();
    if (templ.size() > 0)
    {
        templ = "<" + templ + ">";
    }

    return ui->cb_itemType->currentText() + templ;
}

QString COPYItemWidget::getPrefixedAliad()
{
    return QString("Item: %1").arg(getAlias());
}

QString COPYItemWidget::getAlias()
{
    return ui->alias->text();
}

QString COPYItemWidget::getDEF()
{
    return QString("DEFin_%1").arg(ui->alias->text());
}

QString COPYItemWidget::getDisplayableName()
{
    return ui->modelName->text();
}

QString COPYItemWidget::getDescription()
{
    return ui->modelDescription->toPlainText().replace("\n","\\n");
}


void COPYItemWidget::on_alias_textChanged(const QString &arg1)
{
    ui->alias->setText(arg1.trimmed());
    _model->onAliasChange();
}

void COPYItemWidget::on_cb_itemType_currentIndexChanged(const QString &arg1)
{
    ui->cb_template->clear();

    AbstractItemType* itemType = Tools::ITEMTYPE.value(arg1, NULL);

    if (itemType != NULL && itemType->isTemplated())
    {
        ui->cb_template->addItems(itemType->getTemplates());
        ui->cb_template->setCurrentIndex(0);
        ui->templateWidget->setVisible(true);
    } else {
        ui->templateWidget->setVisible(false);
    }
}
