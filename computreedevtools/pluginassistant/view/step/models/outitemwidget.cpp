#include "outitemwidget.h"
#include "ui_outitemwidget.h"
#include "model/step/models/abstractoutmodel.h"
#include "model/step/tools.h"

OUTItemWidget::OUTItemWidget(AbstractOutModel* model, QWidget *parent) :
    AbstractOutWidget(model, parent),
    ui(new Ui::OUTItemWidget)
{
    ui->setupUi(this);

    // Ajout des classes d'itemdrawables disponibles
    QMapIterator<QString, AbstractItemType*> it(Tools::ITEMTYPE);
    while (it.hasNext())
    {
        it.next();
        if (it.value()->isInstanciable())
        {
            ui->cb_itemType->addItem(it.key());
        }
    }

    ui->cb_itemType->setCurrentIndex(0);

    setFocusProxy(ui->alias);
}

OUTItemWidget::~OUTItemWidget()
{
    delete ui;
}

bool OUTItemWidget::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString OUTItemWidget::getItemType()
{
    return ui->cb_itemType->currentText();
}

QString OUTItemWidget::getItemTypeWithTemplate()
{
    QString templ = getTemplate();
    if (templ.size() > 0)
    {
        templ = "<" + templ + ">";
    }

    return ui->cb_itemType->currentText() + templ;
}

QString OUTItemWidget::getTemplate()
{
    return ui->cb_template->currentText();
}

QString OUTItemWidget::getPrefixedAliad()
{
    return QString("Item: %1").arg(getAlias());
}

QString OUTItemWidget::getAlias()
{
    return ui->alias->text();
}

QString OUTItemWidget::getDEF()
{
    return QString("DEFout_%1").arg(ui->alias->text());
}

QString OUTItemWidget::getDisplayableName()
{
    return ui->modelName->text();
}

QString OUTItemWidget::getDescription()
{
    return ui->modelDescription->toPlainText().replace("\n","\\n");
}


void OUTItemWidget::on_alias_textChanged(const QString &arg1)
{
    ui->alias->setText(arg1.trimmed());
    _model->onAliasChange();
}

void OUTItemWidget::on_cb_itemType_currentIndexChanged(const QString &arg1)
{
    ui->cb_template->clear();

    AbstractItemType* itemType = Tools::ITEMTYPE.value(arg1, NULL);

    if (itemType != NULL && itemType->isTemplated())
    {
        ui->cb_template->addItems(itemType->getTemplates());
        ui->cb_template->setCurrentIndex(0);
        ui->templateWidget->setVisible(true);
    } else {
        ui->templateWidget->setVisible(false);
    }
}
