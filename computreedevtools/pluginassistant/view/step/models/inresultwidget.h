#ifndef INRESULTWIDGET_H
#define INRESULTWIDGET_H

#include "view/step/models/abstractinwidget.h"

namespace Ui {
    class INResultWidget;
}

class INResultWidget : public AbstractInWidget
{
    Q_OBJECT

public:

    explicit INResultWidget(AbstractInModel* model, QWidget *parent = 0);
    ~INResultWidget();

    bool isvalid();
    QString getPrefixedAliad();
    QString getAlias();
    QString getDEF();
    QString getDisplayableName();
    QString getDescription();
    bool isRecursive();
    bool isZeroOrMoreRootGroup();
    bool isCopyResult();

private slots:
    void on_alias_textChanged(const QString &arg1);

private:
    Ui::INResultWidget *ui;
};

#endif // INRESULTWIDGET_H
