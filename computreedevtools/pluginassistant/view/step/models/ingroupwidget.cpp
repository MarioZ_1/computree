#include "ingroupwidget.h"
#include "ui_ingroupwidget.h"
#include "model/step/models/abstractinmodel.h"

INGroupWidget::INGroupWidget(AbstractInModel* model, QWidget *parent) :
    AbstractInWidget(model, parent),
    ui(new Ui::INGroupWidget)
{
    ui->setupUi(this);

    ui->cb_finderMode->addItem("Obligatory");
    ui->cb_finderMode->addItem("Optional");
    ui->cb_finderMode->setCurrentIndex(0);

    ui->cb_choiceMode->addItem("ChooseMultipleIfMultiple");
    ui->cb_choiceMode->addItem("ChooseOneIfMultiple");
    ui->cb_choiceMode->setCurrentIndex(0);

    setFocusProxy(ui->alias);
}

INGroupWidget::~INGroupWidget()
{
    delete ui;
}


bool INGroupWidget::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString INGroupWidget::getPrefixedAliad()
{
    return QString("Group: %1").arg(getAlias());
}

QString INGroupWidget::getAlias()
{
    return ui->alias->text();
}

QString INGroupWidget::getDEF()
{
    return QString("DEFin_%1").arg(ui->alias->text());
}

QString INGroupWidget::getDisplayableName()
{
    return ui->modelName->text();
}

QString INGroupWidget::getDescription()
{
    return ui->modelDescription->toPlainText().replace("\n","\\n");
}

INGroupWidget::FinderMode INGroupWidget::getFinderMode()
{
    if (ui->cb_finderMode->currentText()=="Obligatory")
    {
        return INGroupWidget::F_Obligatory;
    } else
    {
        return INGroupWidget::F_Optional;
    }
}

INGroupWidget::ChoiceMode INGroupWidget::getChoiceMode()
{
    if (ui->cb_choiceMode->currentText()=="ChooseOneIfMultiple")
    {
        return INGroupWidget::C_OneIfMultiple;
    } else if (ui->cb_choiceMode->currentText()=="ChooseMultipleIfMultiple")
    {
        return INGroupWidget::C_MultipleIfMultiple;
    } else
    {
        return INGroupWidget::C_DontChoose;
    }
}

void INGroupWidget::on_alias_textChanged(const QString &arg1)
{
    ui->alias->setText(arg1.trimmed());
    _model->onAliasChange();
}
