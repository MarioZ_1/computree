#include "inresultwidget.h"
#include "ui_inresultwidget.h"
#include "model/step/models/abstractinmodel.h"

INResultWidget::INResultWidget(AbstractInModel* model, QWidget *parent) :
    AbstractInWidget(model, parent),
    ui(new Ui::INResultWidget)
{
    ui->setupUi(this);
    setFocusProxy(ui->alias);
}

INResultWidget::~INResultWidget()
{
    delete ui;
}

bool INResultWidget::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString INResultWidget::getPrefixedAliad()
{
    return QString("Result: %1").arg(getAlias());
}

QString INResultWidget::getAlias()
{
    return ui->alias->text();
}

QString INResultWidget::getDEF()
{
    return QString("DEFin_%1").arg(ui->alias->text());
}

QString INResultWidget::getDisplayableName()
{
    return ui->modelName->text();
}

QString INResultWidget::getDescription()
{
    return ui->modelDescription->toPlainText().replace("\n","\\n");
}

bool INResultWidget::isRecursive()
{
    return ui->cb_recursive->isChecked();
}

bool INResultWidget::isZeroOrMoreRootGroup()
{
    return ui->cb_zeroOrMoreRoot->isChecked();
}

bool INResultWidget::isCopyResult()
{
    return ui->rb_copyresult->isChecked();
}

void INResultWidget::on_alias_textChanged(const QString &arg1)
{
    ui->alias->setText(arg1.trimmed());
    _model->onAliasChange();
}
