#include "view/reader/createreaderdialog.h"
#include "ui_createreaderdialog.h"

CreateReaderDialog::CreateReaderDialog(QString code, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateReaderDialog)
{
    ui->setupUi(this);

    ui->lb_begin->setText(QString("%1_Reader").arg(code.toUpper()));
    ui->le_name->setText("");
}

CreateReaderDialog::~CreateReaderDialog()
{
    delete ui;
}

QString CreateReaderDialog::getName()
{
    return QString("%1%2").arg(ui->lb_begin->text()).arg(ui->le_name->text());
}


