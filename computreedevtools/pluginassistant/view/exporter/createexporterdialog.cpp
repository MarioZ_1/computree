#include "view/exporter/createexporterdialog.h"
#include "ui_createexporterdialog.h"

CreateExporterDialog::CreateExporterDialog(QString code, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateExporterDialog)
{
    ui->setupUi(this);

    ui->lb_begin->setText(QString("%1_Exporter").arg(code.toUpper()));
    ui->le_name->setText("");
}

CreateExporterDialog::~CreateExporterDialog()
{
    delete ui;
}

QString CreateExporterDialog::getName()
{
    return QString("%1%2").arg(ui->lb_begin->text()).arg(ui->le_name->text());
}


