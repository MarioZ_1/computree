#include "view/item/createitemdialog.h"
#include "ui_createitemdialog.h"

CreateItemDialog::CreateItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateItemDialog)
{
    ui->setupUi(this);
}

CreateItemDialog::~CreateItemDialog()
{
    delete ui;
}
