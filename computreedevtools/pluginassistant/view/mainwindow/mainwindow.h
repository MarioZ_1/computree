#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void writeToLog(QString text);
    void setValidPlugin(bool valid);

    void on_pb_new_clicked();
    void on_pb_open_clicked();

    void on_pb_step_clicked();

    void on_pb_action_clicked();

    void on_pb_exporter_clicked();

    void on_pb_reader_clicked();

    void on_pb_item_clicked();

    void on_pb_metric_clicked();

    void on_pb_filter_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
