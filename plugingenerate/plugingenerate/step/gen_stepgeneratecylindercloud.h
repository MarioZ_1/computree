#ifndef GEN_STEPGENERATECYLINDERCLOUD_H
#define GEN_STEPGENERATECYLINDERCLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCylinder : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateCylinder(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    // Step parameters
    double    _botX;    /*!< Coordonnees du centre de la base du cylindre */
    double    _botY;    /*!< Coordonnees du centre de la base du cylindre */
    double    _botZ;    /*!< Coordonnees du centre de la base du cylindre */
    double    _minAng;
    double    _maxAng;
    double    _height;
    double    _radius;
    double    _resTheta;        /*!< Angle entre deux points consecutifs selon theta */
    double    _resHeight;       /*!< Distance entre deux points consecutifs selon l'axe du cylindre */
    double    _resRadius;       /*!< Resolution sur le rayon */
    double    _noiseTheta;
    double    _noiseHeight;
    double    _noiseRadius;
    bool      _generateFaces;   /*!< Genere les faces du haut et du bas du cylindre ? */
    double    _axisX;
    double    _axisY;
    double    _axisZ;
};

#endif // GEN_STEPGENERATECYLINDERCLOUD_H
