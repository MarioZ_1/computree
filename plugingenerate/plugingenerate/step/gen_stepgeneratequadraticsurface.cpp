#include "step/gen_stepgeneratequadraticsurface.h"

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models
#define DEF_resultOut_resultScene "resultScene"
#define DEF_groupOut_groupScene "groupScene"
#define DEF_itemOut_scene "scene"

#include <assert.h>
#include <limits>
#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

// Constructor : initialization of parameters
GEN_StepGenerateQuadraticSurface::GEN_StepGenerateQuadraticSurface(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _a = 1;
    _b = 1;
    _c = 1;
    _d = 1;
    _e = 1;
    _f = 0;

    _xMin = -10;
    _xMax = 10;
    _yMin = -10;
    _yMax = 10;

    _resX = 0.1;
    _resY = 0.1;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateQuadraticSurface::getStepDescription() const
{
    return tr("Créer une Surface Quadrique de points");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateQuadraticSurface::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateQuadraticSurface(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateQuadraticSurface::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateQuadraticSurface::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Quadratic surface"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateQuadraticSurface::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText("ax2 + bxy + cy2 + dx + ey + f", "", "");
    configDialog->addEmpty();

    configDialog->addText(tr("Parametres"), "", "");
    configDialog->addDouble("a", "", -1e+10, 1e+10, 4, _a, 0);
    configDialog->addDouble("b", "", -1e+10, 1e+10, 4, _b, 0);
    configDialog->addDouble("c", "", -1e+10, 1e+10, 4, _c, 0);
    configDialog->addDouble("d", "", -1e+10, 1e+10, 4, _d, 0);
    configDialog->addDouble("e", "", -1e+10, 1e+10, 4, _e, 0);
    configDialog->addDouble("f", "", -1e+10, 1e+10, 4, _f, 0);

    configDialog->addText(tr("Limites"), "", "");
    configDialog->addDouble("xMin", "", -1e+10, 1e+10, 4, _xMin, 0);
    configDialog->addDouble("xMax", "", -1e+10, 1e+10, 4, _xMax, 0);
    configDialog->addDouble("yMin", "", -1e+10, 1e+10, 4, _yMin, 0);
    configDialog->addDouble("yMax", "", -1e+10, 1e+10, 4, _yMax, 0);

    configDialog->addText(tr("Resolution"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _resX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _resY, 0);
}

void GEN_StepGenerateQuadraticSurface::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    /******************************************************************************
     *      User's Compute
     ******************************************************************************/
    int nbPts = 0;
    int nbPtsTotal = ( (_xMax - _xMin) / _resX ) * ( ( _yMax - _yMin) / _resY );

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction de la surface
    for ( float i = _xMin ; (i < _xMax) && !isStopped() ; i += _resX )
    {
        for ( float j = _yMin ; (j <= _yMax) && !isStopped() ; j += _resY )
        {
            undepositPointCloud->addPoint( Eigen::Vector3d( i, j, _a*i*i + _b*i*j + _c*j*j + _d*i + _e*j + _f ));

            nbPts++;

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(  nbPts * 100 / (float)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        // ------------------------------
        // Create OUT groups and items

        // ----------------------------------------------------------------------------
        // Works on the result corresponding to DEF_resultOut_resultScene
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        // UNCOMMENT Following lines and complete parameters of the item's contructor
        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }
}
