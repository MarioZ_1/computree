#ifndef GEN_STEPGENERATETORUS_H
#define GEN_STEPGENERATETORUS_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateTorus : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateTorus(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private :

    // Step parameters
    double    _centerX;
    double    _centerY;
    double    _centerZ;
    double    _radiusCircle;
    double    _distanceToCenter;
    double    _alphaMin;
    double    _alphaMax;
    double    _betaMin;
    double    _betaMax;
    double    _resAlpha;
    double    _resBeta;
    double    _noiseAlpha;
    double    _noiseBeta;
    double    _noiseRadiusCircle;
    double    _noiseDistanceToCenter;
};

#endif // GEN_STEPGENERATETORUS_H
