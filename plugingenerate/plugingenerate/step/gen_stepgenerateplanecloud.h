#ifndef GEN_STEPGENERATEPLANECLOUD_H
#define GEN_STEPGENERATEPLANECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGeneratePlaneCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    
public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGeneratePlaneCloud(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _height;    /*!< Hauteur du plan */
    double    _botX;    /*!< Coordonnees du point inferieur gauche du plan */
    double    _botY;    /*!< Coordonnees du point inferieur gauche du plan */
    double    _topX;    /*!< Coordonnees du point superieur droit du plan */
    double    _topY;    /*!< Coordonnees du point superieur droit du plan */
    double    _resX;    /*!< Espace entre deux points sur l'axe Ox */
    double    _resY;    /*!< Espace entre deux point selon Oy */
    double    _noiseX;
    double    _noiseY;
    double    _noiseZ;
    double    _axisX;
    double    _axisY;
    double    _axisZ;
};

#endif // GEN_STEPGENERATEPLANECLOUD_H
