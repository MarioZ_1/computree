#include "step/gen_stepgeneratescanner.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/ct_scanner.h"

// Alias for indexing out models
#define DEF_resultOut_resultScanner "GeneratedScanner"
#define DEF_groupOut_groupScanner "ScannerGroup"
#define DEF_itemOut_itemScanner "Scanner"

// Constructor : initialization of parameters
GEN_StepGenerateScanner::GEN_StepGenerateScanner(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _posX = 0;
    _posY = 0;
    _posZ = 0;
    _initTheta = 0;
    _initPhi = 0;
    _hFov = 360;
    _vFov = 120;
    _hRes = 0.036;
    _vRes = 0.036;
    _clockWise = true;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateScanner::getStepDescription() const
{
    return tr("Créer une Position de Scan");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateScanner::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateScanner(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateScanner::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateScanner::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScanner, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupScanner, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScanner, DEF_itemOut_itemScanner, new CT_Scanner(), tr("Generated Scanner"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateScanner::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText(tr("Position"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _posX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _posY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _posZ, 0);
    configDialog->addText(tr("Initial angles"), "", "");
    configDialog->addDouble(tr("Theta"), tr("degres"), 0, 360, 4, _initTheta, 0);
    configDialog->addDouble(tr("Phi"), tr("degres"), 0, 180, 4, _initPhi, 0);
    configDialog->addText(tr("Field of view"), "", "");
    configDialog->addDouble(tr("Theta"), tr("degres"), -1e+10, 1e+10, 4, _hFov, 0);
    configDialog->addDouble(tr("Phi"), tr("degres"), 0.0001, 180, 4, _vFov, 0);
    configDialog->addText(tr("Resolution"), "", "");
    configDialog->addDouble(tr("Theta"), tr("degres"), 0.0001, 1e+10, 4, _hRes, 0);
    configDialog->addDouble(tr("Phi"), tr("degres"), 0.0001, 1e+10, 4, _vRes, 0);
    configDialog->addEmpty();
    configDialog->addDouble(tr("Clockwise"), "", 0, 100, 2, _clockWise, 0);
}

void GEN_StepGenerateScanner::compute()
{
    CT_ResultGroup* resultOut_resultScanner = getOutResultList().first();

    CT_StandardItemGroup* groupOut_groupScanner = new CT_StandardItemGroup(DEF_groupOut_groupScanner, resultOut_resultScanner);

    CT_Scanner* itemOut_itemScanner = new CT_Scanner(DEF_itemOut_itemScanner, resultOut_resultScanner, 0, Eigen::Vector3d(_posX, _posY, _posZ), Eigen::Vector3d(0,0,1), _hFov, _vFov, _hRes, _vRes, _initTheta, _initPhi, _clockWise );

    groupOut_groupScanner->addItemDrawable(itemOut_itemScanner);
    resultOut_resultScanner->addGroup(groupOut_groupScanner);

}
