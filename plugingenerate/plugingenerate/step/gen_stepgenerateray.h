#ifndef GEN_STEPGENERATERAY_H
#define GEN_STEPGENERATERAY_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateRay : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateRay(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _posX;    /*!<  */
    double    _posY;    /*!<  */
    double    _posZ;    /*!<  */
    double    _dirX;    /*!<  */
    double    _dirY;    /*!<  */
    double    _dirZ;    /*!<  */
};

#endif // GEN_STEPGENERATERAY_H
