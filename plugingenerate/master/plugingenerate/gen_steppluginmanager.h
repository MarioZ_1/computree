#ifndef GEN_STEPPLUGINMANAGER_H
#define GEN_STEPPLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class GEN_StepPluginManager : public CT_AbstractStepPlugin
{
public:
    GEN_StepPluginManager();
    ~GEN_StepPluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-generate/wiki");}

    QString getPluginRISCitation() const;
protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();
};

#endif // GEN_STEPPLUGINMANAGER_H
