#ifndef GEN_STEPGENERATESHAPE3D_H
#define GEN_STEPGENERATESHAPE3D_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

/*!
 * \class GEN_StepGenerateShape3D
 * \ingroup Steps_GEN
 * \brief <b>No short description for this step.</b>
 *
 * No detailled description for this step
 *
 * \param _nbCircle 
 * \param _nbCylinder 
 * \param _nbElllipse 
 * \param _nbLine 
 * \param _nbSphere 
 *
 */

class GEN_StepGenerateShape3D: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateShape3D(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    int    _nbCircle;    /*!<  */
    int    _nbCylinder;    /*!<  */
    int    _nbEllipse;    /*!<  */
    int    _nbLine;    /*!<  */
    int    _nbSphere;    /*!<  */


    double  _minx;
    double  _maxx;
    double  _miny;
    double  _maxy;
    double  _minz;
    double  _maxz;
};

#endif // GEN_STEPGENERATESHAPE3D_H
