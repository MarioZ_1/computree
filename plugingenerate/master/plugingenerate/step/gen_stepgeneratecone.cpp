#include "gen_stepgeneratecone.h"

#include <QErrorMessage>

// Initialising the randomiser
#include <ctime>

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models
#define DEF_resultOut_resultScene "resultScene"
#define DEF_groupOut_groupScene "groupScene"
#define DEF_itemOut_scene "scene"

#include <assert.h>
#include <limits>
#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

// Constructor : initialization of parameters
GEN_StepGenerateCone::GEN_StepGenerateCone(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _botX = 0;
    _botY = 0;
    _botZ = 0;
    _height = 10;
    _alpha = 45;
    _resAlpha = 1;
    _resH = 0.5;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateCone::getStepDescription() const
{
    return tr("Créer un Cône de points");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateCone::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateCone(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateCone::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateCone::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Cone"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateCone::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText(tr("Sommet"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _botX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _botY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _botZ, 0);
    configDialog->addText(tr("Resolutions"), "", "");
    configDialog->addDouble(tr("Hauteur"), "", 0.0001, 1e+10, 4, _resH, 0);
    configDialog->addDouble(tr("Rayon"), "", 0.0001, 1e+10, 4, _resAlpha, 0);
    configDialog->addText(tr("Angle du cone"), "", "");
    configDialog->addDouble(tr("Rayon"), "", 0.0001, 1e+10, 4, _alpha, 0);
    configDialog->addText(tr("Hauteur du cone"), "", "");
    configDialog->addDouble(tr("Hauteur"), "", 0.0001, 1e+10, 4, _height, 0);
}

void GEN_StepGenerateCone::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    // Conversion des angles en radians
    _resAlpha *= DEG_TO_RAD;

    size_t nbPts = 0;
    size_t nbPtsAlpha = ceil( 2*M_PI / _resAlpha );
    size_t nbPtsHeight = ceil( _height / _resH );
    size_t nbPtsTotal = nbPtsAlpha*nbPtsHeight;

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction du cote du cylindre vertical qui a pour base 0,0,0
    for ( float i = 0 ; (i <= 2*M_PI) && !isStopped(); i += _resAlpha )
    {
        for ( float j = 0 ; j <= _height ; j += _resH )
        {
            undepositPointCloud->addPoint( Eigen::Vector3d( cos( i ) * tan(_alpha) * j + _botX, sin( i ) * tan(_alpha) * j + _botY, -j + _botZ));
            nbPts++;

            setProgress(  (double)nbPts * 100.0 / (double)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

    // Conversion des angles en degres
    _resAlpha *= RAD_TO_DEG;
}
