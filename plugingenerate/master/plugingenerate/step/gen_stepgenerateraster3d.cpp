#include "step/gen_stepgenerateraster3d.h"

#include <limits>
#include <ctime>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

// Alias for indexing out models
#define DEF_resultOut_resultRaster3DFloat "Generated3DRaster"
#define DEF_groupOut_groupRaster3DFloat "3DRasterGroup"
#define DEF_itemOut_itemRaster3DFloat "3DRasters"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid3d.h"
typedef CT_Grid3D<float> CT_Grid3DFloat;

// Constructor : initialization of parameters
GEN_StepGenerateRaster3DFloat::GEN_StepGenerateRaster3DFloat(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _res = 1;
    _botX = -10;
    _botY = -10;
    _botZ = -10;
    _topX = 10;
    _topY = 10;
    _topZ = 10;
    _valMin = 0;
    _valMax = 100;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRaster3DFloat::getStepDescription() const
{
    return tr("Créer une grille Voxel (3D)");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRaster3DFloat::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateRaster3DFloat(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateRaster3DFloat::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateRaster3DFloat::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster3DFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster3DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster3DFloat, DEF_itemOut_itemRaster3DFloat, new CT_Grid3DFloat(), tr("Generated 3D Grid"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateRaster3DFloat::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Resolution"), "", 0.0001, 1e+10, 4, _res, 0);
    configDialog->addText(tr("Bottom left point"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _botX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _botY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _botZ, 0);
    configDialog->addText(tr("Top right point"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _topX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _topY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _topZ, 0);
    configDialog->addText(tr("Value range"), "", "");
    configDialog->addDouble(tr("Min"), "", -1e+10, 1e+10, 4, _valMin, 0);
    configDialog->addDouble(tr("Max"), "", -1e+10, 1e+10, 4, _valMax, 0);
}

void GEN_StepGenerateRaster3DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster3DFloat = getOutResultList().first();

    // ----------------------------------------------------------------------------

    // UNCOMMENT Following lines and complete parameters of the item's contructor
    CT_Grid3DFloat* itemOut_itemRaster3dFloat = CT_Grid3D<float>::createGrid3DFromXYZCoords(DEF_itemOut_itemRaster3DFloat, resultOut_resultRaster3DFloat, _botX, _botY, _botZ, _topX, _topY, _topZ, _res, -std::numeric_limits<float>::max(), 0);

    // On initialise l'aleatoire
    srand( time(0) );

    size_t nbVoxels = itemOut_itemRaster3dFloat->nCells();

    for ( size_t i = 0 ; (i < nbVoxels) && !isStopped() ; i++ )
    {
        itemOut_itemRaster3dFloat->setValueAtIndex( i, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
    }

    if(!isStopped()) {
        itemOut_itemRaster3dFloat->computeMinMax();

        // Works on the result corresponding to DEF_resultOut_resultRaster3DFloat
        CT_StandardItemGroup* groupOut_groupRaster3DFLoat = new CT_StandardItemGroup(DEF_groupOut_groupRaster3DFloat, resultOut_resultRaster3DFloat);

        groupOut_groupRaster3DFLoat->addItemDrawable(itemOut_itemRaster3dFloat);
        resultOut_resultRaster3DFloat->addGroup(groupOut_groupRaster3DFLoat);
    } else {
        delete itemOut_itemRaster3dFloat;
    }
}
