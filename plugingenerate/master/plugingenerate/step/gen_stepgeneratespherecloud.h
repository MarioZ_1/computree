#ifndef GEN_STEPGENERATESPHERECLOUD_H
#define GEN_STEPGENERATESPHERECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateSphereCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateSphereCloud(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private :

    // Step parameters
    double    _botX;    /*!< Coordonnees du centre de la sphere */
    double    _botY;    /*!< Coordonnees du centre de la sphere */
    double    _botZ;    /*!< Coordonnees du centre de la sphere */
    double    _radius;  /*!< Rayon de la sphere */
    double    _thetaMin;
    double    _thetaMax;
    double    _phiMin;
    double    _phiMax;
    double    _resTheta;
    double    _resPhi;
    double    _noiseTheta;
    double    _noisePhi;
    double    _noiseRadius;
};

#endif // GEN_STEPGENERATESPHERECLOUD_H
