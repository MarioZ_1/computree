#ifndef GEN_STEPGENERATERASTER4DFLOAT_H
#define GEN_STEPGENERATERASTER4DFLOAT_H

#ifdef USE_OPENCV

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateRaster4DFloat  : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateRaster4DFloat(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _resW;    /*!<  */
    double    _resX;    /*!<  */
    double    _resY;    /*!<  */
    double    _resZ;    /*!<  */
    double    _botW;    /*!<  */
    double    _botX;    /*!<  */
    double    _botY;    /*!<  */
    double    _botZ;    /*!<  */
    double    _topW;    /*!<  */
    double    _topX;    /*!<  */
    double    _topY;    /*!<  */
    double    _topZ;    /*!<  */
    double    _valMin;    /*!<  */
    double    _valMax;    /*!<  */
};

#endif

#endif // GEN_STEPGENERATERASTER4DFLOAT_H
