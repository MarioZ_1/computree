#ifndef GEN_STEPGENERATECYLINDERMESH_H
#define GEN_STEPGENERATECYLINDERMESH_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCylinderMesh : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateCylinderMesh();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    // Step parameters
    double _height;
    double _radius;
    int _slices;
    double _x, _y, _z;
};

#endif // GEN_STEPGENERATECYLINDERMESH_H
