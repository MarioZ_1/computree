#ifndef GEN_STEPGENERATESPHERECLOUD_H
#define GEN_STEPGENERATESPHERECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateSphereCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateSphereCloud();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private :

    // Step parameters
    double    _botX;    /*!< Coordonnees du centre de la sphere */
    double    _botY;    /*!< Coordonnees du centre de la sphere */
    double    _botZ;    /*!< Coordonnees du centre de la sphere */
    double    _radius;  /*!< Rayon de la sphere */
    double    _thetaMin;
    double    _thetaMax;
    double    _phiMin;
    double    _phiMax;
    double    _resTheta;
    double    _resPhi;
    double    _noiseTheta;
    double    _noisePhi;
    double    _noiseRadius;
};

#endif // GEN_STEPGENERATESPHERECLOUD_H
