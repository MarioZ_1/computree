#include "gen_stepgenerateraster4d.h"

#ifdef USE_OPENCV

// Using float max

// Initialise random
#include <ctime>

// Affiche des logs

// Include results group

// Alias for indexing out models

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid4d_dense.h"

typedef CT_Grid4D_Dense<float> CT_Grid4DFloat;

GEN_StepGenerateRaster4DFloat::GEN_StepGenerateRaster4DFloat() : CT_AbstractStepCanBeAddedFirst()
{
    _resW = 1;
    _resX = 1;
    _resY = 1;
    _resZ = 1;
    _botW = -5;
    _botX = -5;
    _botY = -5;
    _botZ = -5;
    _topW = 5;
    _topX = 5;
    _topY = 5;
    _topZ = 5;
    _valMin = 0;
    _valMax = 100;
}

QString GEN_StepGenerateRaster4DFloat::description() const
{
    return tr("Créer une grille Voxel (4D)");
}

CT_VirtualAbstractStep* GEN_StepGenerateRaster4DFloat::createNewInstance()
{
    return new GEN_StepGenerateRaster4DFloat();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateRaster4DFloat::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateRaster4DFloat::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster4DFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster4DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster4DFloat, DEF_itemOut_itemRaster4DFloat, new CT_Grid4DFloat(), tr("Generated 4D Grid"));
}

void GEN_StepGenerateRaster4DFloat::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addDouble(tr("Resolution W"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resW, 0);
    postInputConfigDialog->addDouble(tr("Resolution X"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resX, 0);
    postInputConfigDialog->addDouble(tr("Resolution Y"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resY, 0);
    postInputConfigDialog->addDouble(tr("Resolution Z"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resZ, 0);
    postInputConfigDialog->addDouble(tr("Bot W"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botW, 0);
    postInputConfigDialog->addDouble(tr("Bot X"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX, 0);
    postInputConfigDialog->addDouble(tr("Bot Y"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY, 0);
    postInputConfigDialog->addDouble(tr("Bot Z"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ, 0);
    postInputConfigDialog->addDouble(tr("Top W"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topW, 0);
    postInputConfigDialog->addDouble(tr("Top X"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topX, 0);
    postInputConfigDialog->addDouble(tr("Top Y"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topY, 0);
    postInputConfigDialog->addDouble(tr("Top Z"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ, 0);
    postInputConfigDialog->addDouble(tr("Val Min"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMin, 0);
    postInputConfigDialog->addDouble(tr("Val Max"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMax, 0);
}

void GEN_StepGenerateRaster4DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster4DFloat = getOutResultList().first();

    // ----------------------------------------------------------------------------

    // UNCOMMENT Following lines and complete parameters of the item's contructor
    CT_Grid4DFloat* itemOut_itemRaster4dFloat = CT_Grid4D_Dense<float>::createGrid4DFromWXYZCoords(DEF_itemOut_itemRaster4DFloat,
                                                                                             resultOut_resultRaster4DFloat,
                                                                                             _botW, _botX, _botY, _botZ, _topW,
                                                                                             _topX, _topY, _topZ,
                                                                                             _resW,  _resX, _resY, _resZ,
                                                                                             -std::numeric_limits<float>::max(), 0);

    // Initialise random
    srand( time(0) );

    // Fill with random values between min and max
    size_t nbVoxels = itemOut_itemRaster4dFloat->nCells();
    PS_LOG->addMessage( LogInterface::info, LogInterface::step, "Raster avec " + QString::number(nbVoxels) + " cellules" );
    for ( size_t i = 0 ; (i < nbVoxels) && !isStopped() ; i++ )
    {
        itemOut_itemRaster4dFloat->setValueAtIndex( i, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
    }

    if(!isStopped()) {
        // Update min and max
        itemOut_itemRaster4dFloat->computeMinMax();

        // Works on the result corresponding to DEF_resultOut_resultRaster4DFloat
        CT_StandardItemGroup* groupOut_groupRaster4DFLoat = new CT_StandardItemGroup(DEF_groupOut_groupRaster4DFloat, resultOut_resultRaster4DFloat);

        groupOut_groupRaster4DFLoat->addItemDrawable(itemOut_itemRaster4dFloat);
        resultOut_resultRaster4DFloat->addGroup(groupOut_groupRaster4DFLoat);
    } else {
        delete itemOut_itemRaster4dFloat;
    }
}

#endif
