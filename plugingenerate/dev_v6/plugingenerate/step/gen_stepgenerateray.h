#ifndef GEN_STEPGENERATERAY_H
#define GEN_STEPGENERATERAY_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateRay : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateRay();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _posX;    /*!<  */
    double    _posY;    /*!<  */
    double    _posZ;    /*!<  */
    double    _dirX;    /*!<  */
    double    _dirY;    /*!<  */
    double    _dirZ;    /*!<  */
};

#endif // GEN_STEPGENERATERAY_H
