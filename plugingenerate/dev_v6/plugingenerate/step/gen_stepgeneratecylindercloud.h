#ifndef GEN_STEPGENERATECYLINDERCLOUD_H
#define GEN_STEPGENERATECYLINDERCLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCylinder : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateCylinder();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    // Step parameters
    double    _botX;    /*!< Coordonnees du centre de la base du cylindre */
    double    _botY;    /*!< Coordonnees du centre de la base du cylindre */
    double    _botZ;    /*!< Coordonnees du centre de la base du cylindre */
    double    _minAng;
    double    _maxAng;
    double    _height;
    double    _radius;
    double    _resTheta;        /*!< Angle entre deux points consecutifs selon theta */
    double    _resHeight;       /*!< Distance entre deux points consecutifs selon l'axe du cylindre */
    double    _resRadius;       /*!< Resolution sur le rayon */
    double    _noiseTheta;
    double    _noiseHeight;
    double    _noiseRadius;
    bool      _generateFaces;   /*!< Genere les faces du haut et du bas du cylindre ? */
    double    _axisX;
    double    _axisY;
    double    _axisZ;
};

#endif // GEN_STEPGENERATECYLINDERCLOUD_H
