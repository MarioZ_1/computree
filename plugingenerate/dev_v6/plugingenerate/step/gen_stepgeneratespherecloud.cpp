#include "gen_stepgeneratespherecloud.h"

#include <ctime>

// Using the point cloud deposit

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models

#include <assert.h>

#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

GEN_StepGenerateSphereCloud::GEN_StepGenerateSphereCloud() : CT_AbstractStepCanBeAddedFirst()
{
    _botX = 0;
    _botY = 0;
    _botZ = 0;
    _radius = 1;
    _thetaMin = 0;
    _thetaMax = 360;
    _phiMin = 0;
    _phiMax = 360;
    _resTheta = 5;
    _resPhi = 5;
    _noiseTheta = 0;
    _noisePhi = 0;
    _noiseRadius = 0;
}

QString GEN_StepGenerateSphereCloud::description() const
{
    return tr("Créer une Sphère de points");
}

CT_VirtualAbstractStep* GEN_StepGenerateSphereCloud::createNewInstance()
{
    return new GEN_StepGenerateSphereCloud();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateSphereCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateSphereCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Sphere"));
}

void GEN_StepGenerateSphereCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addDouble(tr("Radius"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _radius, 0);
    postInputConfigDialog->addText(tr("Sphere center"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ, 0);
    postInputConfigDialog->addText(tr("Limits"), "", "");
    postInputConfigDialog->addDouble(tr("Minimum theta"), "", 0, 359.9999, 4, _thetaMin, 0);
    postInputConfigDialog->addDouble(tr("Maximum theta"), "", 0, 360, 4, _thetaMax, 0);
    postInputConfigDialog->addDouble(tr("Minimum phi"), "", 0, 179.9999, 4, _phiMin, 0);
    postInputConfigDialog->addDouble(tr("Maximum phi"), "", 0, 180, 4, _phiMax, 0);
    postInputConfigDialog->addText(tr("Resolution"), "", "");
    postInputConfigDialog->addDouble(tr("Theta"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resTheta, 0);
    postInputConfigDialog->addDouble(tr("Phi"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resPhi, 0);
    postInputConfigDialog->addText(tr("Add noise"), "", "");
    postInputConfigDialog->addDouble(tr("Theta"), "", 0, std::numeric_limits<double>::max(), 4, _noiseTheta, 0);
    postInputConfigDialog->addDouble(tr("Phi"), "", 0, std::numeric_limits<double>::max(), 4, _noisePhi, 0);
    postInputConfigDialog->addDouble(tr("Radius"), "", 0, std::numeric_limits<double>::max(), 4, _noiseRadius, 0);
}

void GEN_StepGenerateSphereCloud::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    /******************************************************************************
     *      User's Compute
     ******************************************************************************/
    assert( _thetaMin < _thetaMax );
    assert( _phiMin < _phiMax );

    // Conversion des angles en radians
    _thetaMin *= DEG_TO_RAD;
    _thetaMax *= DEG_TO_RAD;
    _phiMin *= DEG_TO_RAD;
    _phiMax *= DEG_TO_RAD;
    _resTheta *= DEG_TO_RAD;
    _resPhi *= DEG_TO_RAD;

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    size_t nbPts = 0;
    size_t nbPtsTheta = ceil( (_thetaMax - _thetaMin) / _resTheta );
    size_t nbPtsPhi = ceil( (_phiMax - _phiMin) / _resPhi );
    size_t nbPtsTotal = nbPtsPhi * nbPtsTheta;

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction du cote du cylindre
    float valTheta, valPhi, valRadius;
    for ( float i = _thetaMin ; (i < _thetaMax) && !isStopped() ; i += _resTheta )
    {
        for ( float j = _phiMin ; (j <= _phiMax) && !isStopped() ; j += _resPhi )
        {
            // On ajoute un point en tenant compte de la variabilité en epsilone
            valTheta = i - _noiseTheta + ( ((double)rand()/RAND_MAX) * 2 * _noiseTheta );
            valPhi = j - _noisePhi + ( ((double)rand()/RAND_MAX) * 2 * _noisePhi );
            valRadius = _radius - _noiseRadius + ( ((double)rand()/RAND_MAX) * 2 * _noiseRadius );

            undepositPointCloud->addPoint( Eigen::Vector3d(_botX + ( valRadius * sin(valPhi) * cos( valTheta ) ), _botY + ( valRadius * sin(valPhi) * sin( valTheta ) ), _botZ + ( valRadius * cos(valPhi) ) ));

            nbPts++;

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(  (double)nbPts * 100.0 / (double)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        // ------------------------------
        // Create OUT groups and items

        // ----------------------------------------------------------------------------
        // Works on the result corresponding to DEF_resultOut_resultScene
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        // UNCOMMENT Following lines and complete parameters of the item's contructor
        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud );
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

    // Conversion des angles en degres
    _thetaMax *= RAD_TO_DEG;
    _thetaMin *= RAD_TO_DEG;
    _phiMax *= RAD_TO_DEG;
    _phiMin *= RAD_TO_DEG;
    _resTheta *= RAD_TO_DEG;
    _resPhi *= RAD_TO_DEG;
}
