#include "step/gen_stepgenerateraster3d.h"

#include <ctime>

// Alias for indexing out models

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid3d.h"
typedef CT_Grid3D<float> CT_Grid3DFloat;

GEN_StepGenerateRaster3DFloat::GEN_StepGenerateRaster3DFloat() : CT_AbstractStepCanBeAddedFirst()
{
    _res = 1;
    _botX = -10;
    _botY = -10;
    _botZ = -10;
    _topX = 10;
    _topY = 10;
    _topZ = 10;
    _valMin = 0;
    _valMax = 100;
}

QString GEN_StepGenerateRaster3DFloat::description() const
{
    return tr("Créer une grille Voxel (3D)");
}

CT_VirtualAbstractStep* GEN_StepGenerateRaster3DFloat::createNewInstance()
{
    return new GEN_StepGenerateRaster3DFloat();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateRaster3DFloat::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateRaster3DFloat::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster3DFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster3DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster3DFloat, DEF_itemOut_itemRaster3DFloat, new CT_Grid3DFloat(), tr("Generated 3D Grid"));
}

void GEN_StepGenerateRaster3DFloat::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addDouble(tr("Resolution"), "", 0.0001, std::numeric_limits<double>::max(), 4, _res, 0);
    postInputConfigDialog->addText(tr("Bottom left point"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ, 0);
    postInputConfigDialog->addText(tr("Top right point"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ, 0);
    postInputConfigDialog->addText(tr("Value range"), "", "");
    postInputConfigDialog->addDouble(tr("Min"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMin, 0);
    postInputConfigDialog->addDouble(tr("Max"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMax, 0);
}

void GEN_StepGenerateRaster3DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster3DFloat = getOutResultList().first();

    // ----------------------------------------------------------------------------

    // UNCOMMENT Following lines and complete parameters of the item's contructor
    CT_Grid3DFloat* itemOut_itemRaster3dFloat = CT_Grid3D<float>::createGrid3DFromXYZCoords(DEF_itemOut_itemRaster3DFloat, resultOut_resultRaster3DFloat, _botX, _botY, _botZ, _topX, _topY, _topZ, _res, -std::numeric_limits<float>::max(), 0);

    // On initialise l'aleatoire
    srand( time(0) );

    size_t nbVoxels = itemOut_itemRaster3dFloat->nCells();

    for ( size_t i = 0 ; (i < nbVoxels) && !isStopped() ; i++ )
    {
        itemOut_itemRaster3dFloat->setValueAtIndex( i, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
    }

    if(!isStopped()) {
        itemOut_itemRaster3dFloat->computeMinMax();

        // Works on the result corresponding to DEF_resultOut_resultRaster3DFloat
        CT_StandardItemGroup* groupOut_groupRaster3DFLoat = new CT_StandardItemGroup(DEF_groupOut_groupRaster3DFloat, resultOut_resultRaster3DFloat);

        groupOut_groupRaster3DFLoat->addItemDrawable(itemOut_itemRaster3dFloat);
        resultOut_resultRaster3DFloat->addGroup(groupOut_groupRaster3DFLoat);
    } else {
        delete itemOut_itemRaster3dFloat;
    }
}
