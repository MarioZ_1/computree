## Evalutation of SimpleForest QSMs

There has been no destructive AGB data collected to have validation plots.

# Screenshots

* QSM colored by branch order:

![plot](./branchOrder.png)

* QSM colored by growthVolume:

![plot](./growthVolume.png)

* QSM colored by radius:

![plot](./radius.png)

* QSM colored by reverse BranchOrder:

![plot](./reverseBranchOrder.png)

* QSM colored by the angle of cylinder axes to the z axis:

![plot](./verticalHorizontal.png)

# Plots

* Radius predicting the growthLength:

![plot](./PlotGrowthLength.png)

* Evaluating the Pipe Model Theory:

![plot](./PlotPipeModelTheory.png)

* Looking at the stem taper:

![plot](./PlotStemTaper.png)

* Predicting an allometric function from a single tree:

![plot](./PlotTreeAllometry.png)

* Volume distribution for different branch orders:

![plot](./PlotBranchOrderHistogramVolume.png)

* Height Histogram for branches:

![plot](./PlotBranchHistogram.png)

# Data References

> Fieldwork was funded through the Metrology for Earth Observation and Climate project (MetEOC-2), grant number ENV55 within the European Metrology Research Programme (EMRP). The EMRP is jointly funded by the EMRP participating countries within EURAMET and the European Union.

https://royalsocietypublishing.org/doi/10.1098/rsfs.2017.0048
