The clouds's origin: Leipzig Canopy Crane facility financed by the German Centre for Integrative Biodiversity Research (iDiv) Halle-Jena-Leipzig.

The clouds's owner: Systematic Botany and Functional Biodiversity, Institute for Biology, Leipzig University.
