You will have to adapt paths for all scripts.

unzip
GonzalesEtAl_Raw.zip
and apply
cloud for cloud all scripts stored in folder
denoisingScripts
to produce
GonzalesEtAl_butress - lower 5 m of the stem
GonzalesEtAl_fullTreeDenoised - complete stem and branches without leave hits
GonzalesEtAl_leaves - only leave hits
GonzalesEtAl_upperStemBranches - GonzalesEtAl_fullTreeDenoised without GonzalesEtAl_butress

You find those files ziped in this folder as well.

If you have the produced files contained in subfolders, either by running above description or unzipping. you can run the following scripts:
* fullTreeQSM.xsct Imports complete stem and branches in a denoised state. Imports as well leave hits in a second file. Produces two QSMs per tree, one time with only using the denoised cloud and a second time runninge Djikstra modelling with leave cloud attaching to the denoised cloud derived QSM.
* noRootQSM.xsct2 same as fullTreeQSM.xsct, but excludes the lowest 5 meters of the stem. Those are supposed to be mesh modelled.
* modelRootPoisson.xsct2 models the lowest 5 meters of stem with Poisson meshing.
